<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-solutions.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>giai-phap/index.html">Solutions</A></SPAN><SPAN> 
      &gt; </SPAN><span class="CurrentBreadCrumb">Giải pháp Chính phủ</span></SPAN>
      </DIV>
      <DIV>
	<h1>Giải pháp Chính phủ</h1>    
      <table cellspacing="0" cellpadding="0">
          <tr>
            <td><h2>Mô tả</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td width="14%"> </td>
            <td width="86%"><p>Yêu cầu bảo mật khi  truy cập (cục bộ hay từ xa) đến hệ thống mạng công ty hoàn toàn có thể đáp ứng. <strong>Avocent SwitchView® Secure Channel (SC)  desktop KVM</strong>, <strong>DSView  3 Management Software</strong> và <strong>MergePoint® Unity™ KVM over IP</strong> sẽ giúp  chúng ta nâng cao và mở rộng độ bảo mật khi truy cập trong các giải pháp dành  cho chính phủ.</p>
              <p> SwitchView® SC desktop  KVM có 3 mức độ bảo mật (Tamper tape, chassis intrusion và memory lock feature)<br />
                Sử dụng phân quyền đăng  nhập cho người dùng và Common Access Card (CAC), sử dụng chứng thực 2 nhân tố  (2FA)</p>
              <p> Phần mềm quản lý DSView  3 đóng vai trò như một cổng kết nối cho phép người quản trị truy cập và quản lý  từ xa các server được kết nối với MergePoint® Unity™  nhằm cải thiện việc xác thực tương tự như việc  dùng CAC khi đăng nhập cục bộ.  DSView 3  hỗ trợ 4 mức xác thực khi truy cập từ xa đến các server.</p>
              <p> Giải pháp này cho chúng  ta thiết lập độ bảo mật khi truy cập cục bộ trước khi người quản trị có thể  thao tác trên các server trong trung tâm dữ liệu.</p>
            </td>
          </tr>
          <tr>
            <td><h2>Mô hình</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td width="14%"> </td><td><img src="images/government_image.jpg" /></td>
          </tr>
          <tr>
            <td><h2>Lợi ích</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td width="14%"> </td>
            <td width="86%"><ul>
              <li>Mở rộng khả năng  truy cập từ Cục bộ đến Từ xa cho tất cả các thiết bị mạng và server.</li>
              <li>Thiết lập nhiều mức  chứng thực cho người quản trị nhằm tăng độ bảo mật</li>
              <li>Thiết lập chính  sách cho người quản trị quản lý các server từ xa</li>
              <li>Sử dụng thẻ  thông minh/CAC để truy cập từ xa nhằm tăng cường độ bảo mật.</li>
              <li>Với tính năng  Virtual Media, card CAC co thể được sử dụng để chứng thực các server ở xa</li>
            </ul></td>
          </tr>
        </table>
      </DIV></TD></TR></TBODY></TABLE>