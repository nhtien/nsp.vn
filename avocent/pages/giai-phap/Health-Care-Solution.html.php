<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-solutions.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>giai-phap/index.html">Giải pháp</A></SPAN>
      <SPAN> &gt; </SPAN><SPAN class="CurrentBreadCrumb">Giải pháp Y tế</SPAN></SPAN>
     </DIV>
      <DIV>
	<h1>Giải pháp Y tế</h1>      
      
        <table cellspacing="0" cellpadding="0">
          <tr>
            <td><h2>Mô tả</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td width="13%"> </td>
            <td width="87%"><p>Thông  tin hồ sơ của bệnh nhân rất quan trọng và nhạy cảm. Để việc chia sẻ thông tin  hiệu quả từ các bác sĩ đến các sinh viên trong khi truy cập vào cùng một máy  tính sẽ có rất nhiều thông tin rò rỉ nếu không xử lý một cách thích hợp.</p>
              <p> Một  ví dụ trong bệnh viện, các hình ảnh từ một ca phẫu thuật sẽ được trình chiếu ở  các màn hình đặt bên ngoài cho các thực tập sinh theo dõi. Việc này giúp các thực  tập sinh được theo dõi trực tiếp cuộc phẫu thuật để học hỏi kinh nghiệm.</p>
Để  thực hiện việc này, hệ thống Avocent HMX extender truy cập qua IP, cho phép nhiều  người dùng truy cập đến nhiều PC từ nhiều nơi khác nhau để theo dõi. Người dùng  có thể được chia sẻ màn hình với nhiều mức độ khác nhau mà không ảnh hưởng đến  hệ thống bảo mật.</td>
          </tr>
          <tr>
            <td><h2>Mô hình</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td width="13%"> </td><td><img src="images/health_image.jpg" /></td>
          </tr>
          <tr>
            <td><h2>Lợi ích</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td width="13%"> </td>
            <td width="87%"><ul>
              <li>Chia sẻ màn hình  máy tính của người dùng, tăng cường bảo mật bằng cách đặt thêm một lớp bảo mật  vật lý vào hệ thống bảo mật.</li>
              <li>Triển khai linh  hoạt cho các trung tâm dữ liệu và môi trường cluster</li>
              <li>Quản lý tập  trung với các chế độ truy cập, đăng nhập và báo cáo</li>
              <li>Tăng cường trải  nghiệm của người dùng, cho phép người dùng truy cập đến các màn hình được thiết  kế riêng.</li>
            </ul></td>
          </tr>
        </table>
      </DIV></TD></TR></TBODY></TABLE>