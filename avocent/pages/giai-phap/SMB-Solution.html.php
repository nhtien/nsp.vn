<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-solutions.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>giai-phap/index.html">Giải pháp</A></SPAN>
      <SPAN> &gt; </SPAN><SPAN class="CurrentBreadCrumb">Giải pháp SMB</SPAN></SPAN>
      </DIV>
      <DIV>
	<h1>Giải pháp SMB </h1>      
      
        <table cellspacing="0" cellpadding="0">
          <tr>
            <td><h2>Mô tả</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td width="14%"> </td>
            <td ><p>AutoView® Digital KVM  switch giúp bạn truy cập đến các server của bạn từ bất kì đâu, không cần bất cứ  phần mềm tích hợp nào cho việc điều khiển và truy cập server từ xa.</p>
              <p> Người quản trị IT có thể  quản lý hiệu quả việc tiêu thụ điện của các thiết  bị với giải pháp Direct_PDU™ Power Management  thông qua giao diện web giúp cho việc khởi động &ldquo;Bật/Tắt&rdquo; từ xa các server và  switch trở nên dễ dàng.</p>
              <p> Tại trụ sở làm việc, sử  dụng <strong>LCD rack console </strong>để truy cập đến  các server. Thiết bị KVM này giúp bạn kết nối và kiểm soát trựcc tiếp đồng thời  từ 8 đến 16 PC hoặc server thông qua các tổ hợp phím chức năng.</p>
              <p> Giải pháp Avocent® SMB cung  cấp sự linh hoạt, tiết kiệm thời gian nhằm đơn giản hóa việc quản trị theo tiêu  chí quản lý nhiều hơn với ít chi phi đầu tư hơn.</p></td>
          </tr>
          <tr>
            <td><h2>Mô hình</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td> </td><td><img src="images/smb_image.jpg" /></td>
          </tr>
          <tr>
            <td><h2>Lợi ích</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td> </td>
            <td><ul>
              <li>Mở rộng khả năng  truy cập từ Cục bộ đến Từ xa cho tất cả các thiết bị mạng và server.</li>
              <li>Giúp truy cập hiệu  quả và quản lý các hoạt động kinh doanh.</li>
              <li>Giúp quản lý các  văn phòng ở xa hoặc các server của khách hàng để mở rộng cung cấp dịch vụ.</li>
              <li>Tiết kiệm chi  phí và tăng hiệu quả xử lý các vấn đề trên server từ xa mà không cần phải cử  nhân viên đến tận nơi.</li>
              <li>Hợp nhất và quản  lý nhiều thanh phân phối nguồn PDU một cách nhanh chóng.</li>
            </ul></td>
          </tr>
        </table>
      </DIV></TD></TR></TBODY></TABLE>