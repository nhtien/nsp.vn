<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-solutions.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>giai-phap/index.html">Giải pháp</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN class="CurrentBreadCrumb">Giải pháp Viễn thông</SPAN></SPAN>
      </DIV>
      <DIV>
	<h1>Giải pháp Viễn thông</h1>      
      
        <table cellspacing="0" cellpadding="0">
          <tr>
            <td width="14%"><h2>Mô tả</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td> </td>
            <td><p>Mọi trạm cơ sở đều phải  luôn sẵn sàng để truy cập và quản lý, vì trong một vài trường hợp đòi hỏi phải  ngưng hoạt động để xử lý sự cố tuy nhiên việc này phải diễn ra ngắt nhất có thể  nhằm đáp ứng độ cam kết chất lượng dịch vụ (SLA).</p>
              <p> Phần mềm quản lý <strong>DSView 3, Advanced Console Server (ACS)  6000</strong> và <strong>Power Management Solutions  (PM3000 series)</strong> cho chúng ta nhiều lựa chọn để quản lý thiết bị hiệu quả.  Truy cập từ xa đến các thiết bị core switch, thiết bị mạng hoặc reboot thiết bị  đều có thể làm được trên giải pháp serial &amp; PM.             </p>
Việc  triển khai toàn diện pháp trong môi trường Viễn thông sẽ giảm thiểu tối đa thời  gian downtime, sử dụng quỹ thời gian nhân viên tốt hơn và tăng thời gian phản hồi  khi có sự cố. </td>
          </tr>
          <tr>
            <td nowrap="nowrap"><h2>Mô hình</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td> </td><td><img src="images/telco_image.jpg" /></td>
          </tr>
          <tr>
            <td><h2>Lợi ích</h2></td>
            <td> </td>
          </tr>
          <tr>
            <td> </td>
            <td>
              <ul>
                <li>Truy cập từ xa đến  nhiều trạm cơ từ một giao diện điều khiển duy nhất</li>
                <li>Có thể theo dõi  và ghi log các sự kiện truy cập</li>
                <li>Cung cấp nhiều  phương thức kết nối (qua mạng, modem,…) để bảo trì hoặc duy trì hoạt động</li>
                <li>Giảm downtime,  tăng SLA</li>
                <li>Reboot thiết bị  từ xa trong quá trình bảo trì</li>
                <li>Phân quyền người  dùng với quyền hạn tương ứng</li>
              </ul>
           </td>
          </tr>
        </table>
      </DIV></TD></TR></TBODY></TABLE>