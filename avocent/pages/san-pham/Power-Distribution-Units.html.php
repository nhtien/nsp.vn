<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-products.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/index.html">Sản phẩm</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN class="CurrentBreadCrumb">Thanh phấn phối nguồn</SPAN></SPAN>
      </DIV>
      <DIV>
<table style="width: 100%;" class="contentplace" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td class="ProductArticleLeftGap" valign="top">
<div style="margin-bottom:30px;" class="BodyText">
<div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" style="display:inline"><h1 class="ms-rteCustom-ArticleTitle">Thanh phấn phối nguồn</h1>
Kiểm soát trạng thái và đo điện năng sử dụng. Nhận biết năng lực của nguồn điện và giảm thiểu rủi ro do quá tải với thanh nguồn của Avocent. Bộ sản phẩm này bao gồm các loại đo và bật/tắt nguồn, cho phép bạn kiểm soát và đo điện năng sử dụng.
</div>
</div>
<div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartWPQ2" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="ffd26314-791a-473f-b45d-be14cc599e1a" haspers="false" id="WebPartWPQ2" width="100%" class="ms-WPBody" allowdelete="false" style=""><table id="cbqwp" class="cbq-layout-main" cellpadding="0" cellspacing="0"><tbody><tr><td id="column" "%"="" valign="top"><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/PM-100020003000-Rack-PDUs-_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="http://nsp.com.vn/vi/san-pham/ups-a-power-quality/power-management-a-distribution.html?page=shop.product_details&amp;flypage=flypage.tpl&amp;product_id=857&amp;category_id=53" class="prodCEOtitlelink" target="_blank" title="">PM 1000/2000/3000 PDU</a></div>
				<div class="ProdClassBody">PM 1000/2000/3000 bao gồm loại một pha và ba pha hỗ trợ đo điện năng tiêu thụ trên cả thanh nguồn (PM1000), trên từng cổng nguồn (PM2000) hoặc đo và bật tắt điện từng cổng nguồn (PM3000).</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/Direct_PDU-Power-Strip_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Products/PowerDistributionUnits/Pages/AvocentDirectPDUPowerStrip.aspx" class="prodCEOtitlelink" target="_blank" title="">Direct PDU Power Strip</a></div><div class="ProdClassBody">
Direct_PDU cung cấp người quản trị CNTT một phương pháp đơn giản và hiệu quả để quản lý điện từ xa theo yêu cầu hạ tầng CNTT của họ.                
                </div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="footer"></div></td></tr></tbody></table></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
</div>
<div class="EditModePanel">

</div>
</td>
<td class="rightpaneENP" valign="top">
<div></div>
<div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartWPQ3" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="8361cec4-a692-4e1f-8567-0aef7ed55308" haspers="false" id="WebPartWPQ3" width="100%" class="ms-WPBody" allowdelete="false" style=""><!-- BEGIN LivePerson Monitor. --> <script language="javascript"> var lpMTagConfig = {'lpServer' : "server.iad.liveperson.net",'lpNumber' : "10634351",'lpProtocol' : "https"}; function lpAddMonitorTag(src){if(typeof(src)=='undefined'||typeof(src)=='object'){src=lpMTagConfig.lpMTagSrc?lpMTagConfig.lpMTagSrc:'/hcp/html/mTag.js';}if(src.indexOf('http')!=0){src=lpMTagConfig.lpProtocol+"://"+lpMTagConfig.lpServer+src+'?site='+lpMTagConfig.lpNumber;}else{if(src.indexOf('site=')<0){if(src.indexOf('?')<0)src=src+'?';else src=src+'&';src=src+'site='+lpMTagConfig.lpNumber;}};var s=document.createElement('script');s.setAttribute('type','text/javascript');s.setAttribute('charset','iso-8859-1');s.setAttribute('src',src);document.getElementsByTagName('head').item(0).appendChild(s);} if (window.attachEvent) window.attachEvent('onload',lpAddMonitorTag); else window.addEventListener("load",lpAddMonitorTag,false); </script> <!-- END LivePerson Monitor. -->


<!-- BEGIN LivePerson Button Code --> <div id="lpButDivID-1257726285044">
  <div><table class="lpStaticButton" id="lpChatBtnTbl3493566984" style="" border="0" cellpadding="0" cellspacing="0">
    
    <tbody><tr class="lpStaticButtonTR" id="lpStaticButtonTR3493566984">
        
        <td align="center">&nbsp;</td>
        
    </tr>
    <tr id="lpPoweredByTR3493566984" class="lpPoweredBy">
        
        <td align="center">&nbsp;</td>
        
    </tr>
    <tr id="emtStarRatingTR3493566984" class="lpEmtStarRating">

    <td align="center">&nbsp;</td>

</tr>
    
</tbody></table>
</div></div><script type="text/javascript" charset="UTF-8" src="images/san-pham/a_002.js"></script> <!-- END LivePerson Button code --></div></td>
			</tr>
		</tbody></table><div class="ms-PartSpacingVertical"></div></td>
	</tr><tr>
		<td id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_2c0294f9_f1c1_4701_a148_f3424794ba80" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="2c0294f9-f1c1-4701-a148-f3424794ba80" haspers="false" id="WebPartctl00_SPWebPartManager1_g_2c0294f9_f1c1_4701_a148_f3424794ba80" width="100%" class="ms-WPBody" allowdelete="false" allowexport="false" style="">
				  <div id="ctl00_SPWebPartManager1_g_2c0294f9_f1c1_4701_a148_f3424794ba80"></div></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
</div>

</td>
</tr>
</tbody></table>      
      </DIV></TD></TR></TBODY></TABLE>