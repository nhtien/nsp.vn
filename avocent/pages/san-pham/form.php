<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-solutions.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="">Solutions</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN class="CurrentBreadCrumb">Business Need</SPAN></SPAN>
      </DIV>
      <DIV>
      <TABLE style="width: 100%;" class="contentplace" border="0" cellSpacing="0" 
      cellPadding="0">
        <TBODY>
        <TR>
          <TD class="ProductArticleLeftGap" vAlign="top">
            <DIV style="margin-bottom: 30px;" class="BodyText">
            <DIV style="display: inline;" id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField">
            <DIV id="ctl00_cphEnterpriseBody_RichHtmlField1ControlWrapper_RichHtmlField"><SPAN 
            class="ms-rteCustom-ArticleTitle"><strong>Business Need</strong><BR></SPAN></DIV>
            <P>
            <DIV>Whether you need to gain control over unwieldy remote data 
            centers, button down power consumption, gain visibility into asset 
            usage across life cycles or even create real-time virtual centers 
            for capacity planning, our Business Need Solutions are ready to help 
            you Simply Manage anything.</DIV></DIV></DIV>
            <DIV>
            <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
              <TBODY>
              <TR>
                <TD id="MSOZoneCell_WebPartWPQ2" vAlign="top">
                  <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" 
                  TOPLEVEL="">
                    <TBODY>
                    <TR>
                      <TD vAlign="top">
                        <DIV id="WebPartWPQ2" class="ms-WPBody" allowDelete="false" 
                        width="100%" HasPers="false" WebPartID="4b628c9c-d94d-4424-a3d2-916615f75f36">
                        <TABLE id="cbqwp" class="cbq-layout-main" cellSpacing="0" 
                        cellPadding="0">
                          <TBODY>
                          <TR>
                            <TD id="column" vAlign="top" ?%?="">
                              <DIV style="padding-left: 100px;" id="linkitem" 
                              class="BodyText" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"><A 
                              class="SubLinknew" title="" href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Solutions/BusinessNeed/Pages/AvocentSolutionsDataCenterManagement.aspx" 
                              target="">Data Center Management</A>
                              <DIV class="BodyTextLink">Three innovative 
                              solutions to help you deal with the data center 
                              challenges of control, asset identification and 
                              management and branch infrastructure 
                              management.</DIV><BR><BR></DIV>
                              <DIV style="padding-left: 100px;" id="linkitem" 
                              class="BodyText" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"><A 
                              class="SubLinknew" title="" href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Solutions/BusinessNeed/Pages/AvocentSolutionsDataCenterPlanning.aspx" 
                              target="">Data Center Planning</A>
                              <DIV class="BodyTextLink">Emerson Network Power's 
                              Avocent Data Center Planning Solution provides the 
                              real-time insights needed to effectively plan for 
                              IT change down to the rack and device, including 
                              what and where assets are to power 
                              consumption</DIV><BR><BR></DIV>
                              <DIV style="padding-left: 100px;" id="linkitem" 
                              class="BodyText" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"><A 
                              class="SubLinknew" title="" href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Solutions/BusinessNeed/Pages/AvocentSolutionsPowerManagement.aspx" 
                              target="">Power Management</A>
                              <DIV class="BodyTextLink">Emerson Network Power's 
                              Avocent's Power Management Solution provides you 
                              with the tools you need to monitor energy 
                              consumption, costs and trends across all levels 
                              within the data center and remote 
                              locations.</DIV><BR><BR></DIV>
                              <DIV style="padding-left: 100px;" id="linkitem" 
                              class="BodyText" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"><A 
                              class="SubLinknew" title="" href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Solutions/BusinessNeed/Pages/AvocentSolutionsControlRoom.aspx" 
                              target="">Control Room</A>
                              <DIV class="BodyTextLink">Emerson Network Power's 
                              Avocent's Control Room Solution<STRONG> 
                              </STRONG>is designed for desktop administrators 
                              who need to physically separate the computer from 
                              the user without adversely affecting worker 
                              productivity. Deployed over your LAN, it offers 
                              point-to-point, IP-routable extension at gigabit 
                              Ethernet rates. It also supports a comprehensive 
                              range of peripherals including DVI-1 video, CD 
                              audio, USB mass storage, keyboard and mouse and 
                              other USB devices.</DIV><BR><BR></DIV>
                              <DIV style="padding-left: 100px;" id="linkitem" 
                              class="BodyText" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"><A 
                              class="SubLinknew" title="" href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Solutions/BusinessNeed/Pages/AvocentSolutionsDigitalSignage.aspx" 
                              target="">Digital Signage</A>
                              <DIV class="BodyTextLink">Emerson Network Power's 
                              Avocent's Digital Signage Solution is designed to 
                              quickly and easily provide wireless transmission 
                              of live and recorded HD video and audio to 
                              multiple displays such as plasma, LCD and kiosk 
                              stations. </DIV><BR><BR></DIV>
                              <DIV style="padding-left: 100px;" id="linkitem" 
                              class="BodyText" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime"><A 
                              class="SubLinknew" title="" href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Solutions/BusinessNeed/Pages/AvocentSolutionsNetworkInfrastructureManagement.aspx" 
                              target="">Network Infrastructure Management</A>
                              <DIV class="BodyTextLink">Avocent's Network 
                              Infrastructure Management Solution delivers 
                              unmatched serial console and remote access to 
                              multiple system consoles. No matter how spread out 
                              your WAN is. No matter what all you have stuffed 
                              on it.</DIV><BR><BR></DIV>
                              <DIV 
                      id="footer"></DIV></TD></TR></TBODY></TABLE></DIV></TD></TR></TBODY></TABLE></TD></TR>
              </TBODY></TABLE></DIV>
            <DIV class="EditModePanel"></DIV></TD>
          <TD class="rightpaneENP" vAlign="top">
            <DIV>
            <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
              <TBODY>
              <TR>
                <TD id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_fb8f0d32_828b_486d_94bc_671172976f67" 
                vAlign="top">
                  <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" 
                  TOPLEVEL="">
                    <TBODY>
                    <TR>
                      <TD vAlign="top">
                        <DIV id="WebPartctl00_SPWebPartManager1_g_fb8f0d32_828b_486d_94bc_671172976f67" 
                        class="ms-WPBody" allowDelete="false" width="100%" 
                        HasPers="false" WebPartID="fb8f0d32-828b-486d-94bc-671172976f67" 
                        allowExport="false">
                        <DIV id="ctl00_SPWebPartManager1_g_fb8f0d32_828b_486d_94bc_671172976f67"></DIV></DIV></TD></TR></TBODY></TABLE>
                  <DIV class="ms-PartSpacingVertical"></DIV></TD></TR>
              <TR>
                <TD id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_a9ca18c7_0c98_421f_9060_614bd6cf1114" 
                vAlign="top">
                  <TABLE style="width: 155px;" border="0" cellSpacing="0" 
                  cellPadding="0" width="100%" TOPLEVEL="">
                    <TBODY>
                    <TR>
                      <TD vAlign="top">
                        <DIV style="width: 155px; overflow: auto;" id="WebPartctl00_SPWebPartManager1_g_a9ca18c7_0c98_421f_9060_614bd6cf1114" 
                        class="ms-WPBody" allowDelete="false" HasPers="false" 
                        WebPartID="a9ca18c7-0c98-421f-9060-614bd6cf1114">
                        <DIV style="width: 155px;" id="ctl00_SPWebPartManager1_g_a9ca18c7_0c98_421f_9060_614bd6cf1114">
                        <DIV id="ctl00_SPWebPartManager1_g_a9ca18c7_0c98_421f_9060_614bd6cf1114_links" 
                        class="DynamicLinksVertical"><BR></DIV>
                        </DIV></DIV></TD></TR></TBODY></TABLE>
                  <DIV class="ms-PartSpacingVertical"></DIV></TD></TR>
              <TR>
                <TD id="MSOZoneCell_WebPartWPQ3" vAlign="top">&nbsp;</TD></TR>
              </TBODY></TABLE></DIV>
            <DIV>
            <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
              <TBODY>
              <TR>
                <TD id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_3ebe8d84_96f0_4ea2_afd8_6e0ca89f6dd6" 
                vAlign="top">
                  <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" 
                  TOPLEVEL="">
                    <TBODY>
                    <TR>
                      <TD vAlign="top">&nbsp;</TD></TR></TBODY></TABLE></TD></TR>
            </TBODY></TABLE></DIV></TD></TR></TBODY></TABLE></DIV></TD></TR></TBODY></TABLE>