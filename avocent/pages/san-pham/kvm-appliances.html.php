<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-products.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/index.html">Sản phẩm</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN class="CurrentBreadCrumb">Thiết bị KVM</SPAN></SPAN>
      </DIV>
      <DIV>
<table style="width: 100%;" class="contentplace" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td class="ProductArticleLeftGap" valign="top">
<div style="margin-bottom:30px;" class="BodyText">
<div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" style="display:inline"><h1 class="ms-rteCustom-ArticleTitle">Thiết bị KVM</h1>
Tăng cường khả năng quản lý server từ bàn làm việc, tại tủ rack với dòng sản phẩm KVM analog và KVM IP đã được kiểm chứng của Avocent. Avocent đã và đang đi đầu trong lĩnh vực phát triển KVM ngay từ khi mới thành lập, giúp tiết kiệm không gian phòng server và hiệu quả làm việc của nhân viên trong các công ty hàng đầu trên thế giới.
</div>
</div>
<div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartWPQ2" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="8d32baf3-3836-43dd-9707-7f0a72eb481b" haspers="false" id="WebPartWPQ2" width="100%" class="ms-WPBody" allowdelete="false" style=""><table id="cbqwp" class="cbq-layout-main" cellpadding="0" cellspacing="0"><tbody><tr><td id="column" "%"="" valign="top"><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/HMX-Desktop-over-IP-Extender-System_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="san-pham/KVM-Appliances/IP-Appliances.html" class="prodCEOtitlelink" target="" title="">KVM over IP</a></div><div class="ProdClassBody">Loại bỏ hạn chế về khoảng cách với hệ thống KVM IP bằng cách cho phép truy cập và kiểm soát server và các thiết bị mạng trong trung tâm dữ liệu từ xa: trong phòng NOC hoặc từ bất kỳ vị trí nào khác.
               </div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/Amx-Analog-KVM-Appliance_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="san-pham/KVM-Appliances/Analog-KVM-Appliances.html" class="prodCEOtitlelink" target="" title="">KVM analog</a></div>
               <div class="ProdClassBody">
               Đơn giản truy cập server tại tủ rack cho người dùng với công nghệ KVM analog. Một nền tảng vững chắc cho việc quản lý server hiệu quả cùng các tính năng cao cấp như virtual media và chứng thực bằng smart card/CAC.</div></td></tr>

<tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0">
</table></div><div id="footer"></div></td></tr></tbody></table></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
</div>
<div class="EditModePanel">

</div>
</td>
<td class="rightpaneENP" valign="top">
<div></div>
<div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartWPQ3" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="3f97a64a-29f4-4708-ab6d-676ac969bd46" haspers="false" id="WebPartWPQ3" width="100%" class="ms-WPBody" allowdelete="false" style=""><!-- BEGIN LivePerson Monitor. --> <script language="javascript"> var lpMTagConfig = {'lpServer' : "server.iad.liveperson.net",'lpNumber' : "10634351",'lpProtocol' : "https"}; function lpAddMonitorTag(src){if(typeof(src)=='undefined'||typeof(src)=='object'){src=lpMTagConfig.lpMTagSrc?lpMTagConfig.lpMTagSrc:'/hcp/html/mTag.js';}if(src.indexOf('http')!=0){src=lpMTagConfig.lpProtocol+"://"+lpMTagConfig.lpServer+src+'?site='+lpMTagConfig.lpNumber;}else{if(src.indexOf('site=')<0){if(src.indexOf('?')<0)src=src+'?';else src=src+'&';src=src+'site='+lpMTagConfig.lpNumber;}};var s=document.createElement('script');s.setAttribute('type','text/javascript');s.setAttribute('charset','iso-8859-1');s.setAttribute('src',src);document.getElementsByTagName('head').item(0).appendChild(s);} if (window.attachEvent) window.attachEvent('onload',lpAddMonitorTag); else window.addEventListener("load",lpAddMonitorTag,false); </script> <!-- END LivePerson Monitor. -->


<!-- BEGIN LivePerson Button Code --> <div id="lpButDivID-1257726285044">
  <div></div></div><script type="text/javascript" charset="UTF-8" src="images/san-pham/a.js"></script> <!-- END LivePerson Button code --></div></td>
			</tr>
		</tbody></table><div class="ms-PartSpacingVertical"></div></td>
	</tr><tr>
		<td id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_bf7b19b9_0397_4893_9fb4_dd81e5df1bf7" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="bf7b19b9-0397-4893-9fb4-dd81e5df1bf7" haspers="false" id="WebPartctl00_SPWebPartManager1_g_bf7b19b9_0397_4893_9fb4_dd81e5df1bf7" width="100%" class="ms-WPBody" allowdelete="false" allowexport="false" style="">
				  <div id="ctl00_SPWebPartManager1_g_bf7b19b9_0397_4893_9fb4_dd81e5df1bf7"></div></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
</div>

</td>
</tr>
</tbody></table>      
      </DIV></TD></TR></TBODY></TABLE>