<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-products.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN>
      <SPAN>&gt; </SPAN><SPAN class="CurrentBreadCrumb">Sản phẩm</SPAN></SPAN>
      </DIV>
      <DIV>
<table style="width: 100%;" class="contentplace" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td class="ProductArticleLeftGap" valign="top">
<div style="margin-bottom:30px;" class="BodyText">
<div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" style="display:inline"><h1 class="ms-rteCustom-ArticleTitle">Các sản phẩm Avocent cho Cơ sở hạ tầng CNTT của bạn</h1>
Avocent cung cấp giải pháp quản lý hoạt động và hạ tầng trung tâm dữ liệu, giúp khách hàng giảm chi phí và đơn giản hóa môi trường CNTT phức tạp thông qua các thiết bị phần cứng và phần mềm tích hợp, tập trung.</div>
</div>
<div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartWPQ2" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="11942ee1-5290-4f6b-84b8-694997d521eb" haspers="false" id="WebPartWPQ2" width="100%" class="ms-WPBody" allowdelete="false" style=""><table id="cbqwp" class="cbq-layout-main" cellpadding="0" cellspacing="0"><tbody><tr><td id="column" "%"="" valign="top"><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/Data-Center-Management_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="san-pham/Data-Center-Management-Software.html" class="prodCEOtitlelink" target="" title="">	
Phần mềm Quản lý Trung tâm Dữ liệu</a></div><div class="ProdClassBody">
Giao cho đội ngũ nhân viên IT công cụ làm việc hiệu quả từ bất cứ nơi đâu, bất cứ lúc nào bất kể ngày hay đêm, và không quan tâm đến loại phần cứng, hệ điều hành hay tình trạng hệ thống mạng. Phần mềm quản lý của Avocent cho phép hoạch định, truy cập, kiểm soát và báo cáo các tài sản trong trung tâm dữ liệu của bạn.

</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/SwitchView-SC-100200-Secure-Desktop-KVM-Appliance_representa.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="san-pham/kvm-appliances.html" class="prodCEOtitlelink">Thiết bị KVM</a></div><div class="ProdClassBody">Tăng cường khả năng quản lý server từ bàn làm việc, tại tủ rack với dòng sản phẩm KVM analog và KVM IP đã được kiểm chứng của Avocent. Avocent đã và đang đi đầu trong lĩnh vực phát triển KVM ngay từ khi mới thành lập, giúp tiết kiệm không gian phòng server và hiệu quả làm việc của nhân viên trong các công ty hàng đầu trên thế giới.</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/ACS-5000-Advanced-Console-Server_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="san-pham/Serial-Appliances.html" class="prodCEOtitlelink" target="" title="">Thiết bị quản lý serial console</a></div><div class="ProdClassBody">
Truy cập từ xa, bảo mật đến các thiết bị serial console. Sử dụng ACS advanced console servers, các chuyên gia IT và nhân viên phòng NOC có thể, quản lý trung tâm dữ liệu từ xa, bảo mật, bao gồm khả năng kiểm soát từ xa, theo dõi thiết bị hoặc chẩn đoán và xử lý sự cố.
</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/Direct_PDU-Power-Strip_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="san-pham/Power-Distribution-Units.html" class="prodCEOtitlelink" target="" title="">Thanh phấn phối nguồn (PDU)</a></div><div class="ProdClassBody">
Kiểm soát trạng thái và đo điện năng sử dụng. Nhận biết năng lực của nguồn điện và giảm thiểu rủi ro do quá tải với thanh nguồn của Avocent. Bộ sản phẩm này bao gồm các loại đo và bật/tắt nguồn, cho phép bạn kiểm soát và đo điện năng sử dụng.
</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/MergePoint-Service-Processor_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="san-pham/Service-Processor-Managers.html" class="prodCEOtitlelink" target="" title="">Service Processor Managers</a></div><div class="ProdClassBody">
Cho phép quản lý bộ service processor (xử lý dịch vụ) nhúng của server với Avocent Service Processor Managers (SPM). SPM cho phép các tổ chức CNTT quản lý trung tâm dữ liệu một cách thông suốt và giảm chi phí hoạt động bằng cách sử dụng công nghệ service processor có sẵn trong các server.
</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/LCD-Console-Tray_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="san-pham/LCD-Console-Trays.html" class="prodCEOtitlelink" target="" title="">LCD Console Trays</a></div><div class="ProdClassBody">
Dòng sản phẩm LCD Console Trays cung cấp nhiều tính năng giúp đơn giản và dễ dàng truy cập các server, giúp cập nhật phần mềm, giải quyết sự cố và theo dõi hệ thống thuận tiện, rút ngắn thời gian.
</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0">
</table>
 
 </div><div id="footer"></div></td></tr></tbody></table></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
</div>
<div class="EditModePanel">

</div>
</td>
<td class="rightpaneENP" valign="top">&nbsp;</td>
</tr>
</tbody></table>      </DIV></TD></TR></TBODY></TABLE>