<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-products.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/index.html">Sản phẩm</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN class="CurrentBreadCrumb">Thiết bị quản lý Serial Console</SPAN></SPAN>
      </DIV>
      <DIV>
      
<TABLE style="width: 100%;" class="contentplace" border="0" cellSpacing="0" 
      cellPadding="0">
        <TBODY>
        <TR>
          <TD class="ProductArticleLeftGap" vAlign="top">
            <DIV style="margin-bottom: 30px;" class="BodyText">
            <DIV style="display: inline;" id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField">
            <H1 class="ms-rteCustom-ArticleTitle">Thiết bị quản lý Serial Console</H1>
            Truy cập từ xa, bảo mật đến các thiết bị serial console. Sử dụng ACS advanced console servers, các chuyên gia IT và nhân viên phòng NOC có thể, quản lý trung tâm dữ liệu từ xa, bảo mật, bao gồm khả năng kiểm soát từ xa, theo dõi thiết bị hoặc chẩn đoán và xử lý sự cố.</DIV></DIV>
            <DIV>
            <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
              <TBODY>
              <TR>
                <TD id="MSOZoneCell_WebPartWPQ2" vAlign="top">
                  <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" 
                  TOPLEVEL="">
                    <TBODY>
                    <TR>
                      <TD vAlign="top">
                        <DIV id="WebPartWPQ2" class="ms-WPBody" allowDelete="false" 
                        width="100%" HasPers="false" WebPartID="f9e8ac69-f3fd-40e8-a32e-7717155d19af">
                        <TABLE id="cbqwp" class="cbq-layout-main" cellSpacing="0" 
                        cellPadding="0">
                          <TBODY>
                          <TR>
                            <TD id="column" vAlign="top" ?%?="">
                              <DIV id="linkitem" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime">
                              <TABLE border="0" cellSpacing="0" 
                                cellPadding="0"><TBODY>
                                <TR>
                                <TD class="PaddingRight10px" vAlign="top"><IMG 
                                src="images/san-pham/ACS-v6000-Virtual-Advanced_representative.png"></TD>
                                <TD vAlign="top">
                                <DIV class="prodCEOtitlelink"><A class="prodCEOtitlelink" 
                                title="" href="http://www.nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/serial-console-server.html?page=shop.product_details&flypage=flypage.tpl&product_id=777&category_id=68&manufacturer_id=3" 
                                target="_blank">ACS v6000</A></DIV>
                                <DIV class="ProdClassBody">
                                Quản lý máy ảo bằng cách khai thác kết nối serial trực tiếp của máy ảo, công nghệ này nâng cao độ sẵn sàng cho server ảo trong môi trường đám mây phức tạp.
                               </DIV></TD></TR>
                                <TR>
                                <TD style="height: 25px;" 
                                colSpan="2"></TD></TR></TBODY></TABLE></DIV>
                              <DIV id="linkitem" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime">
                              <TABLE border="0" cellSpacing="0" 
                                cellPadding="0"><TBODY>
                                <TR>
                                <TD class="PaddingRight10px" vAlign="top"><IMG 
                                src="images/san-pham/ACS-6000-Advanced-Console-Server_representative.png"></TD>
                                <TD vAlign="top">
                                <DIV class="prodCEOtitlelink"><A class="prodCEOtitlelink" 
                                title="" href="http://www.nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/serial-console-server.html?page=shop.product_details&flypage=flypage.tpl&product_id=754&category_id=68&manufacturer_id=3" 
                                target="">ACS 6000</A></DIV>
                                <DIV class="ProdClassBody">
                                ACS 6000 là dòng thiết bị quản lý serial console thế hệ mới nhất Avocent. ACS 6000 cung cấp khả năng truy cập từ xa, bảo mật đến thiết bị mạng và các server UNIX / LINUX.</DIV></TD></TR>
                                <TR>
                                <TD style="height: 25px;" 
                                colSpan="2"></TD></TR></TBODY></TABLE></DIV>
                              <DIV id="linkitem" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime">
                              <TABLE border="0" cellSpacing="0" 
                                cellPadding="0"><TBODY>
                                <TR>
                                <TD class="PaddingRight10px" vAlign="top"><IMG 
                                src="images/san-pham/ACS-5000-Advanced-Console-Server_representative.png"></TD>
                                <TD vAlign="top">
                                <DIV class="prodCEOtitlelink"><A class="prodCEOtitlelink" 
                                title="" href="http://www.nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/serial-console-server.html?page=shop.product_details&flypage=flypage.tpl&product_id=749&category_id=68&manufacturer_id=3" 
                                target="">ACS 5000</A></DIV>
                                <DIV class="ProdClassBody">
                                ACS 5000 đáp ứng nhu cầu quản lý từ xa cho các trung tâm dữ liệu vừa và lớn. Dãy sản phẩm bao gồm loại 4, 8, 16, 32, 48 cổng với một nguồn cấp điện AC.
                                </DIV></TD></TR>
                                <TR>
                                <TD style="height: 25px;" 
                                colSpan="2"></TD></TR></TBODY></TABLE></DIV>
                              <DIV 
                      id="footer"></DIV></TD></TR></TBODY></TABLE></DIV></TD></TR></TBODY></TABLE></TD></TR>
              </TBODY></TABLE></DIV>
            <DIV class="EditModePanel"></DIV></TD>
          <TD class="rightpaneENP" vAlign="top">
            <DIV>
            <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
              <TBODY>
              <TR>
                <TD id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_4bca4f8e_e9c7_444b_a966_faec36588865" 
                vAlign="top"><DIV class="ms-PartSpacingVertical"></DIV></TD></TR>
              <TR>
                <TD id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_1fbad337_c59f_4243_8024_ead882713f53" 
                vAlign="top">&nbsp;</TD></TR></TBODY></TABLE>
            </DIV>
            <DIV>
            <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
              <TBODY>
              <TR>
                <TD id="MSOZoneCell_WebPartWPQ3" vAlign="top">
                  <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" 
                  TOPLEVEL="">
                    <TBODY>
                    <TR>
                      <TD vAlign="top">
                        <DIV id="WebPartWPQ3" class="ms-WPBody" allowDelete="false" 
                        width="100%" HasPers="false" WebPartID="02a91938-ec89-4bd4-bd8d-b9d2121e7afe"><!-- BEGIN LivePerson Monitor. --> 

<SCRIPT language="javascript"> var lpMTagConfig = {'lpServer' : "server.iad.liveperson.net",'lpNumber' : "10634351",'lpProtocol' : "https"}; function lpAddMonitorTag(src){if(typeof(src)=='undefined'||typeof(src)=='object'){src=lpMTagConfig.lpMTagSrc?lpMTagConfig.lpMTagSrc:'/hcp/html/mTag.js';}if(src.indexOf('http')!=0){src=lpMTagConfig.lpProtocol+"://"+lpMTagConfig.lpServer+src+'?site='+lpMTagConfig.lpNumber;}else{if(src.indexOf('site=')<0){if(src.indexOf('?')<0)src=src+'?';else src=src+'&';src=src+'site='+lpMTagConfig.lpNumber;}};var s=document.createElement('script');s.setAttribute('type','text/javascript');s.setAttribute('charset','iso-8859-1');s.setAttribute('src',src);document.getElementsByTagName('head').item(0).appendChild(s);} if (window.attachEvent) window.attachEvent('onload',lpAddMonitorTag); else window.addEventListener("load",lpAddMonitorTag,false); </SCRIPT>
                         <!-- END LivePerson Monitor. --><!-- BEGIN LivePerson Button Code --> 

                        <DIV id="lpButDivID-1257726285044"></DIV>
                        <SCRIPT type="text/javascript" charset="UTF-8" src="images/san-pham/10634351.js"></SCRIPT>
                         
                  <!-- END LivePerson Button code --></DIV></TD></TR></TBODY></TABLE>
                  <DIV class="ms-PartSpacingVertical"></DIV></TD></TR>
              <TR>
                <TD id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_5d23883c_a4eb_4629_985a_485d27b65739" 
                vAlign="top">
                  <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" 
                  TOPLEVEL="">
                    <TBODY>
                    <TR>
                      <TD vAlign="top">
                        <DIV id="WebPartctl00_SPWebPartManager1_g_5d23883c_a4eb_4629_985a_485d27b65739" 
                        class="ms-WPBody" allowDelete="false" width="100%" 
                        HasPers="false" WebPartID="5d23883c-a4eb-4629-985a-485d27b65739" 
                        allowExport="false">
                        <DIV id="ctl00_SPWebPartManager1_g_5d23883c_a4eb_4629_985a_485d27b65739"></DIV></DIV></TD></TR></TBODY></TABLE></TD></TR>
            </TBODY></TABLE></DIV></TD></TR></TBODY></TABLE>      </DIV></TD></TR></TBODY></TABLE>