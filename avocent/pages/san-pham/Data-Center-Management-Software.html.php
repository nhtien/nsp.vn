<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-products.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/index.html">Sản phẩm</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN class="CurrentBreadCrumb">Phần mềm quản lý Trung tâm Dữ liệu</SPAN></SPAN>
      </DIV>
      <DIV>
      
<table style="width: 100%;" class="contentplace" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td class="ProductArticleLeftGap" valign="top">
<div style="margin-bottom:30px;" class="BodyText">
<div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" style="display:inline"><h1 class="ms-rteCustom-ArticleTitle">Phần mềm quản lý Trung tâm Dữ liệu</h1>
Giao cho đội ngũ nhân viên IT công cụ làm việc hiệu quả từ bất cứ nơi đâu, bất cứ lúc nào bất kể ngày hay đêm, và không quan tâm đến loại phần cứng, hệ điều hành hay tình trạng hệ thống mạng. Phần mềm quản lý của Avocent cho phép hoạch định, truy cập, kiểm soát và báo cáo các tài sản trong trung tâm dữ liệu của bạn.</div>
</div>
<div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartWPQ2" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="7fda058f-04bd-4550-ab45-6e9f8d6b81e2" haspers="false" id="WebPartWPQ2" width="100%" class="ms-WPBody" allowdelete="false" style=""><table id="cbqwp" class="cbq-layout-main" cellpadding="0" cellspacing="0"><tbody><tr><td id="column" "%"="" valign="top"><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/Avocent-Data-Center-Planner_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="http://www.nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/data-center-planning-a-management-softwares.html?page=shop.product_details&amp;flypage=flypage.tpl&amp;product_id=806&amp;category_id=69" class="prodCEOtitlelink" target="_blank" title="">Avocent Data Center Planner</a></div><div class="ProdClassBody">Phiên bản mới của phần mềm Avocent MergePoint Infrastructure Explorer, phần mềm hoạch định và quản lý năng lực trung tâm dữ liệu mạnh mẽ này giúp các tổ chức khả năng theo dõi và báo cáo linh hoạt để quản lý hiệu quả trung tâm dữ liệu của họ.</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0">
</table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/dsv3_150.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="http://www.nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/data-center-planning-a-management-softwares.html?page=shop.product_details&amp;flypage=flypage.tpl&amp;product_id=801&amp;category_id=69&amp;manufacturer_id=3" class="prodCEOtitlelink" target="_blank" title="">Phần mềm Quản lý DSView 3</a></div>
<div class="ProdClassBody">Phần mềm DSView 3 cung cấp tầm nhìn toàn diện cho tất cả server, service processor, thiết bị mạng và thanh phân phối nguồn trong toàn bộ trung tâm dữ liệu.</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/dsvpm_150.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="http://www.nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/data-center-planning-a-management-softwares.html?page=shop.product_details&amp;flypage=flypage.tpl&amp;product_id=805&amp;category_id=69&amp;manufacturer_id=3" class="prodCEOtitlelink" target="_blank" title="">Phần mềm Quản lý Điện năng DSView 3</a></div><div class="ProdClassBody">
Phần mềm DSView 3 Power Manager thêm khả năng theo dõi điện năng và khả năng báo cáo các truy cập và kiểm soát của phần mềm DSView 3.
</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="footer"></div></td></tr></tbody></table></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
</div>
<div class="EditModePanel">

</div>
</td>
<td class="rightpaneENP" valign="top">
<div></div>
<div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartWPQ3" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top">&nbsp;</td>
			</tr>
		</tbody></table><div class="ms-PartSpacingVertical"></div></td>
	</tr><tr>
		<td id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_3e9bd514_bf24_456c_8ff6_80c3da11cfa6" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top">&nbsp;</td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
</div>

</td>
</tr>
</tbody></table>      </DIV></TD></TR></TBODY></TABLE>