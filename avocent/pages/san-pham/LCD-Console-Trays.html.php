<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-products.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/index.html">Sản phẩm</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN class="CurrentBreadCrumb">LCD Console</SPAN></SPAN>
      </DIV>
      <DIV>
      
<TABLE style="width: 100%;" class="contentplace" border="0" cellSpacing="0" 
      cellPadding="0">
        <TBODY>
        <TR>
          <TD class="ProductArticleLeftGap" vAlign="top">
            <DIV style="margin-bottom: 30px;" class="BodyText">
            <DIV style="display: inline;" id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField">
            <H1 class="ms-rteCustom-ArticleTitle">LCD Console</H1>
            Dòng sản phẩm LCD Console Trays cung cấp nhiều tính năng giúp đơn giản và dễ dàng truy cập các server, giúp cập nhật phần mềm, giải quyết sự cố và theo dõi hệ thống thuận tiện, rút ngắn thời gian. </DIV></DIV>
            <DIV>
            <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
              <TBODY>
              <TR>
                <TD id="MSOZoneCell_WebPartWPQ2" vAlign="top">
                  <TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" 
                  TOPLEVEL="">
                    <TBODY>
                    <TR>
                      <TD vAlign="top">
                        <DIV id="WebPartWPQ2" class="ms-WPBody" allowDelete="false" 
                        width="100%" HasPers="false" WebPartID="1aea21cd-c801-4d53-ac32-2c15358a599d">
                        <TABLE id="cbqwp" class="cbq-layout-main" cellSpacing="0" 
                        cellPadding="0">
                          <TBODY>
                          <TR>
                            <TD id="column" vAlign="top" ?%?="">
                              <DIV id="linkitem" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime">
                              <TABLE border="0" cellSpacing="0" 
                                cellPadding="0"><TBODY>
                                <TR>
                                <TD class="PaddingRight10px" vAlign="top"><IMG 
                                src="images/san-pham/LCD-Console-Tray_representative.png"></TD>
                                <TD vAlign="top">
                                <DIV class="prodCEOtitlelink"><a class="prodCEOtitlelink" 
                                title="" href="http://www.nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/lcd-console-trays/lcd-console-trays.html?page=shop.product_details&amp;flypage=flypage.tpl&amp;product_id=837&amp;category_id=56&amp;manufacturer_id=3" 
                                target="_blank">LCD Console</a></DIV>
                                <DIV class="ProdClassBody"><SPAN style='font:/normal "times new roman"; text-transform: none; text-indent: 0px; letter-spacing: normal; word-spacing: 0px; white-space: normal; border-collapse: separate; font-size-adjust: none; font-stretch: normal;' 
                                class="Apple-style-span"><SPAN style="text-align: left; line-height: 16px; font-family: arial, verdana, sans-serif; font-size: 12px;" 
                                class="Apple-style-span">LCD console cho phép truy cập nhiều server từ một giao diện duy nhất ngay tại tủ rack.
</SPAN></SPAN></DIV></TD></TR>
                                <TR>
                                <TD style="height: 25px;" 
                                colSpan="2"></TD></TR></TBODY></TABLE></DIV>
                              <DIV id="linkitem" xmlns:dt="urn:schemas-microsoft-com:datatypes" 
                              xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime">
                              <TABLE border="0" cellSpacing="0" 
                                cellPadding="0"><TBODY>
                                <TR>
                                <TD class="PaddingRight10px" vAlign="top"><IMG 
                                src="images/san-pham/LCD-Console-Tray_representative.png"></TD>
                                <TD vAlign="top">
                                <DIV class="prodCEOtitlelink"><a class="prodCEOtitlelink" 
                                title="" href="http://www.nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/lcd-console-trays/lcd-console-with-kvm-switch.html?page=shop.product_details&amp;flypage=flypage.tpl&amp;product_id=840&amp;category_id=57&amp;manufacturer_id=3"  target="_blank">LCD Console tích hợp KVM</a></DIV>
                                <DIV class="ProdClassBody">Thiết bị LCD console tích hợp KVM tạo ra giải pháp truy cập đơn giản cho SMB và môi trường phòng server.</DIV></TD></TR>
                                <TR>
                                <TD style="height: 25px;" 
                                colSpan="2"></TD></TR></TBODY></TABLE></DIV>
                              <DIV 
                      id="footer"></DIV></TD></TR></TBODY></TABLE></DIV></TD></TR></TBODY></TABLE></TD></TR>
              </TBODY></TABLE></DIV>
            <DIV class="EditModePanel"></DIV></TD>
          <TD class="rightpaneENP" vAlign="top">&nbsp;</TD></TR></TBODY></TABLE>      </DIV></TD></TR></TBODY></TABLE>