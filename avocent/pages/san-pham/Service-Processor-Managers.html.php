<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-products.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/index.html">Sản phẩm</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN class="CurrentBreadCrumb">MergePoint Service Processor Manage</SPAN></SPAN>
      </DIV>
      <DIV>
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
                    <tbody><tr>
                 
                     <td style="padding-right: 10px;" valign="top" width="600px">  
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td style="width: 90%;" valign="top">
                                    <div style="padding-left:10px; padding-top:10px;">
                                        <span class="detailheading">
                                            <div id="ctl00_PlaceHolderMain_RichHtmlField7__ControlWrapper_RichHtmlField" style="display:inline">MergePoint Service Processor Manager</div>
                                        </span>
                                    </div>
                                    <div class="BodyText" style="margin-left:10px;">
                                      <div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" style="display:inline"><h2>Rack-level management of embedded service processors</h2></div>
                                    </div>
                                </td>
                                <td class="ImageRotator" style="padding-right: 7px; padding-top: 10px;" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_7c8de7af_644b_44b3_b3a6_30b2cde0176d" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="7c8de7af-644b-44b3-b3a6-30b2cde0176d" haspers="false" id="WebPartctl00_SPWebPartManager1_g_7c8de7af_644b_44b3_b3a6_30b2cde0176d" width="100%" class="ms-WPBody" allowdelete="false" style=""><div id="ctl00_SPWebPartManager1_g_7c8de7af_644b_44b3_b3a6_30b2cde0176d">
					<table style="border-collapse: collapse;" border="0" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td id="ctl00_SPWebPartManager1_g_7c8de7af_644b_44b3_b3a6_30b2cde0176d_smallImageTD"></td><td style="height: 170px; width: 180px;" align="center" valign="middle"><img id="ctl00_SPWebPartManager1_g_7c8de7af_644b_44b3_b3a6_30b2cde0176d_imageNormal" src="images/san-pham/MergePoint-Service-Processor_1_small.png" style="width: 170px; border-width: 0px;"></td><td><input name="ctl00$SPWebPartManager1$g_7c8de7af_644b_44b3_b3a6_30b2cde0176d$hiddenLarge" id="ctl00_SPWebPartManager1_g_7c8de7af_644b_44b3_b3a6_30b2cde0176d_hiddenLarge" value="/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial Appliances/MergePoint Service Processor/MergePoint-Service-Processor_1_large.png" type="hidden"><input name="ctl00$SPWebPartManager1$g_7c8de7af_644b_44b3_b3a6_30b2cde0176d$hiddenimageNormal" id="ctl00_SPWebPartManager1_g_7c8de7af_644b_44b3_b3a6_30b2cde0176d_hiddenimageNormal" value="/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial Appliances/MergePoint Service Processor/MergePoint-Service-Processor_1_small.png" type="hidden"><input name="ctl00$SPWebPartManager1$g_7c8de7af_644b_44b3_b3a6_30b2cde0176d$hiddenSmall" id="ctl00_SPWebPartManager1_g_7c8de7af_644b_44b3_b3a6_30b2cde0176d_hiddenSmall" value="/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial Appliances/MergePoint Service Processor/MergePoint-Service-Processor_1_thumbnail.png" type="hidden"><input name="ctl00$SPWebPartManager1$g_7c8de7af_644b_44b3_b3a6_30b2cde0176d$hiddenNormalAlt" id="ctl00_SPWebPartManager1_g_7c8de7af_644b_44b3_b3a6_30b2cde0176d_hiddenNormalAlt" type="hidden"></td><td class="ClickContainer" style="width: 55px; padding-top: 2px; padding-left: 5px;" align="center" valign="top">&nbsp;</td><td valign="top">&nbsp;</td>
						</tr>
					</tbody></table>
				</div></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
                                </td>
                                
                            </tr>
                        </tbody></table>                                        
                    <div style="margin-bottom: 30px;" class="BodyText">
                        <div id="ctl00_PlaceHolderMain_RichHtmlField2__ControlWrapper_RichHtmlField" style="display:inline"><p><br>Avocent MergePoint<sup>®</sup>
 Service Processor Managers (SPMs) allow IT organizations to streamline 
data center management and reduce operational costs by leveraging the 
service processor technologies already present in their servers. </p>
<p>MergePoint appliances proactively monitor and maintain server health,
 enabling faster troubleshooting and problem resolution that ensures 
servers are up and running 24/7.</p></div>
                    </div>
                    <div>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="2d2d01e3-0b58-4a08-ad66-0185de3e2865" haspers="false" id="WebPartctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865" width="100%" class="ms-WPBody" allowdelete="false" allowexport="false" style=""><div id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865">
	<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_tabs"><div class="innercontent" style="height:450px;width:600px;"><div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_tab0">
			<a name="Overview"></a><h3>Overview</h3><p><strong>Features and Benefits</strong></p><ul><li><strong>Single Console for Multiple SProcessor Typeservice</strong>&nbsp; – Consolidated server management regardless of the type of embedded management technologies in use </li><li><strong>IPMI Provisioning</strong> – Faster, easier IPMI deployment </li><li><strong>Embedded DHCP Server</strong> – Reduces cost of IP management on the system management network </li><li><strong>Service Processor Auto Discovery</strong> – Better use of already deployed server management capabilities </li><li><strong>Integration with DSView<sup>®</sup> 3 Management Software</strong> – Provides a single interface for management of the entire IT infrastructure </li><li><strong>Support for Industry Standard Communications</strong> – SMASH 2.0, WS-Man improves automation and integration with other management tools </li><li><strong>SoL, Power Control, Hardware Monitoring and Alerts</strong> – Complete, full-featured, remote server management from a single interface </li><li><strong>Event and Alert Management</strong> – Enables proactive response to server issues </li><li><strong>Group Execution of Commands</strong> – Control the server infrastructure as a whole</li></ul>
		</div><div class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_tab1">
			<a name="Model Comparison"></a><h3>Model Comparison</h3><table class="ms-rteTable-Emerson" summary=""><tbody><tr class="ms-rteTableHeaderRow-Emerson"><td class="ms-rteTableHeaderOddCol-Emerson">SPM&nbsp;</td><td class="ms-rteTableHeaderEvenCol-Emerson">Power&nbsp;</td><td class="ms-rteTableHeaderOddCol-Emerson">Network Ports&nbsp;</td><td class="ms-rteTableHeaderEvenCol-Emerson">Device Ports&nbsp;</td><td class="ms-rteTableHeaderOddCol-Emerson">Key Features&nbsp;</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson"><span></span><br>SP5324<br></td><td class="ms-rteTableEvenCol-Emerson"><span></span><br>Single/Dual AC<br></td><td class="ms-rteTableOddCol-Emerson"><span></span><br>2<br></td><td class="ms-rteTableEvenCol-Emerson"><span></span><br>24<br></td><td class="ms-rteTableOddCol-Emerson"><span></span><br>Target application IP address consolidation<br></td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson"><span></span><br>SP5340<br></td><td class="ms-rteTableEvenCol-Emerson"><span></span><br>Single/Dual AC<br></td><td class="ms-rteTableOddCol-Emerson"><span></span><br>2<br></td><td class="ms-rteTableEvenCol-Emerson"><span></span><br>40<br></td><td class="ms-rteTableOddCol-Emerson"><span></span><br>Target application IP address consolidation<br></td></tr></tbody></table><p><br></p>
		</div><div class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_tab2">
			<a name="Photos"></a><h3>Photos</h3><p></p><table style="width: 100%; float: left; height: 1%;" class="ms-rteTable-Emerson" align="left"><tbody><tr class="ms-rteTableHeaderRow-Emerson"><td class="ms-rteTableHeaderOddCol-Emerson" colspan="2" valign="center">Product Images</td></tr><tr class="ms-rteTableOddRow-Emerson"><td style="border-bottom: 1px solid rgb(171, 171, 171); width: 160px;" class="ms-rteTableOddCol-Emerson" valign="top"><a href="http://www.emersonnetworkpower.com/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial%20Appliances/MergePoint%20Service%20Processor/SP5324_b_415.jpg" target="_blank"><img alt="View large image" src="images/san-pham/SP5324_b_415_thumb.jpg" border="0"></a></td><td style="border-bottom: 1px solid rgb(171, 171, 171); width: 70%;" class="ms-rteTableEvenCol-Emerson" valign="top"><a href="http://www.emersonnetworkpower.com/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial%20Appliances/MergePoint%20Service%20Processor/SP5324_b_415.jpg" target="_blank"><span class="class=ms-rteCustom-HeadlineLink">SP 5324 - Back</span></a></td></tr><tr class="ms-rteTableEvenRow-Emerson"><td style="border-bottom: 1px solid rgb(171, 171, 171); width: 160px;" class="ms-rteTableOddCol-Emerson" valign="top"><a href="http://www.emersonnetworkpower.com/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial%20Appliances/MergePoint%20Service%20Processor/SP5324_a_415.jpg" target="_blank"><img alt="View large image" src="images/san-pham/SP5324_a_415_thumb.jpg" border="0"></a></td><td style="border-bottom: 1px solid rgb(171, 171, 171); width: 70%;" class="ms-rteTableEvenCol-Emerson" valign="top"><a href="http://www.emersonnetworkpower.com/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial%20Appliances/MergePoint%20Service%20Processor/SP5324_a_415.jpg" target="_blank"><span class="class=ms-rteCustom-HeadlineLink">SP 5324 - Front</span></a></td></tr><tr class="ms-rteTableOddRow-Emerson"><td style="border-bottom: 1px solid rgb(171, 171, 171); width: 160px;" class="ms-rteTableOddCol-Emerson" valign="top"><a href="http://www.emersonnetworkpower.com/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial%20Appliances/MergePoint%20Service%20Processor/SP5340_b_415.jpg" target="_blank"><img alt="View large image" src="images/san-pham/SP5340_b_415_thumb.jpg" border="0"></a></td><td style="border-bottom: 1px solid rgb(171, 171, 171); width: 70%;" class="ms-rteTableEvenCol-Emerson" valign="top"><a href="http://www.emersonnetworkpower.com/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial%20Appliances/MergePoint%20Service%20Processor/SP5340_b_415.jpg" target="_blank"><span class="class=ms-rteCustom-HeadlineLink">SP 5340 - Back</span></a></td></tr><tr class="ms-rteTableEvenRow-Emerson"><td style="border-bottom: 1px solid rgb(171, 171, 171); width: 160px;" class="ms-rteTableOddCol-Emerson" valign="top"><a href="http://www.emersonnetworkpower.com/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial%20Appliances/MergePoint%20Service%20Processor/SP5340_a_415.jpg" target="_blank"><img alt="View large image" src="images/san-pham/SP5340_a_415_thumb.jpg" border="0"></a></td><td style="border-bottom: 1px solid rgb(171, 171, 171); width: 70%;" class="ms-rteTableEvenCol-Emerson" valign="top"><a href="http://www.emersonnetworkpower.com/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial%20Appliances/MergePoint%20Service%20Processor/SP5340_a_415.jpg" target="_blank"><span class="class=ms-rteCustom-HeadlineLink">SP 5340 - Front</span></a></td></tr></tbody></table><p></p>
		</div><div class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_tab3">
			<a name="Tech Specs"></a><h3>Tech Specs</span><table style="width: 100%;" class="ms-rteTable-Emerson" summary=""><tbody><tr class="ms-rteTableHeaderRow-Emerson"><td class="ms-rteTableHeaderOddCol-Emerson"><strong>Mechanical</strong></td><td class="ms-rteTableHeaderEvenCol-Emerson"><strong>5324/5340 </strong></td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Height</td><td class="ms-rteTableEvenCol-Emerson">1.75 in. (4.45 cm)</td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Width</td><td class="ms-rteTableEvenCol-Emerson">17 in. (43.18 cm)</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Depth</td><td class="ms-rteTableEvenCol-Emerson">12 in. (30 cm)</td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Weight</td><td class="ms-rteTableEvenCol-Emerson">9 lbs. (4 kg)</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson" colspan="2"><strong>Environmental</strong></td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Operating Temperature</td><td class="ms-rteTableEvenCol-Emerson">50° to 122°F (10° to 50°C)</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Storage Temperature</td><td class="ms-rteTableEvenCol-Emerson">–40° to 185°F (–40° to 85°C)</td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Humidity</td><td class="ms-rteTableEvenCol-Emerson">5% to 90% non-condensing</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson" colspan="2"><strong>Power</strong></td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Operating Voltage</td><td class="ms-rteTableEvenCol-Emerson">100–240 VAC</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Input Power</td><td class="ms-rteTableEvenCol-Emerson">260W</td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson" colspan="2"><strong>Device Support</strong></td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Support SP List</td><td class="ms-rteTableEvenCol-Emerson">• IPMI 1.5 (incl. non-standard SoL support) <br>• IPMI 2.0 <br>• HP integrated Lights Out (iLO, iLO2) <br>• Dell Remote Access Cards (DRAC III, 4, 5) <br>• Dell M1000e Blade Chassis Controller <br>• Dell iDRAC (Blades) <br>• FTS iRMC <br>• IBM RSA (RSA II) <br>• IBM BladeCenter <br>• Sun ALOM <br>• Sun ILOM <br></td></tr><tr class="ms-rteTableEvenRow-Emerson"><td style="height: 24px;" class="ms-rteTableOddCol-Emerson" colspan="2"><strong>Hardware Specifications</strong></td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Interfaces</td><td class="ms-rteTableEvenCol-Emerson">24/40
 x 10/100BT RJ-45 server Ethernet interfaces on RJ-45 1 RS-232 serial 
console on RJ-45 1 RS-232 serial port on RJ-45 for power manager or 
external modem 1 10/100/1000BT primary user Ethernet on RJ-45 1 10/100BT
 secondary user Ethernet on RJ-45 (with failover support for primary 
interface)</td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson" colspan="2"><strong>Standards</strong></td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson">Approved Agency</td><td class="ms-rteTableEvenCol-Emerson">CSA <br>FCC <br>C-tick <br>VCCI <br>CE <br>(CE) <br></td></tr></tbody></table>
		</div><div class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_tab4">
			<a name="Ordering Details"></a><h3>Ordering Details</h3><table style="width: 100%;" class="ms-rteTable-Emerson" summary=""><tbody><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson" colspan="2"><strong>MODEL</strong></td></tr><tr class="ms-rteTableHeaderRow-Emerson"><td class="ms-rteTableHeaderOddCol-Emerson"><strong>Description</strong></td><td class="ms-rteTableHeaderOddCol-Emerson"><strong>Part Number</strong></td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson">MergePoint SP5324 service processor manager SAC, 24-port, single AC power</td><td class="ms-rteTableEvenCol-Emerson">MGP5324SAC-xxx</td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson">MergePoint SP5340 service processor manager SAC, 40-port, single AC power</td><td class="ms-rteTableEvenCol-Emerson">MGP5340SAC-xxx</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson">MergePoint SP5324 service processor manager DAC, 24-port, dual AC power</td><td class="ms-rteTableEvenCol-Emerson">MGP5324DAC-xxx</td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson">MergePoint SP5340 service processor manager DAC, 40-port, dual AC power</td><td class="ms-rteTableEvenCol-Emerson">MGP5340DAC-xxx</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson" colspan="2">&nbsp;*Note: Actual part number ends with country-specific code (example -001 for North America)</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson" colspan="2">&nbsp;</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson" colspan="2"><strong>SERVICE OPTIONS</strong></td></tr><tr class="ms-rteTableHeaderRow-Emerson"><td class="ms-rteTableHeaderOddCol-Emerson"><strong>Description</strong></td><td class="ms-rteTableHeaderOddCol-Emerson"><strong>Part Number</strong></td></tr><tr class="ms-rteTableEvenRow-Emerson"><td class="ms-rteTableOddCol-Emerson">1 year advanced replacement for MergePoint 53xx series service processor managers</td><td class="ms-rteTableEvenCol-Emerson">SCNT-PLUS-MP53XX</td></tr><tr class="ms-rteTableOddRow-Emerson"><td class="ms-rteTableOddCol-Emerson">1 year 24/7 technical support and advanced replacement for MergePoint 53xx series service processor managers</td><td class="ms-rteTableEvenCol-Emerson">SCNT-PREM-MP53XX</td></tr></tbody></table>
		</div><div class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_tab5">
			<a name="Downloads"></a><h3>Downloads</h3><div>
	<div>
		
<table border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td style="padding-right: 10px;">
    <img alt="" src="images/san-pham/btn_SalesLiterature.gif" height="24px" width="24px">
</td>
<td class="ProductsDownload">Brochures and Data Sheets
</td>
</tr>

<tr>
<td></td>
<td>

    <table border="0" cellpadding="0" cellspacing="0">
    <tbody><tr>
    <td class="ProductsDownloadLinks" style="height: 20px;">
        <a id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_ctl59_ctl03_repeaterDownloadSource_ctl00_repeaterDownLoadText_ctl00_HyperLink1" class="ProductsDownloadLinks" href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Documents/DataSheets/01/MGP-DS-EN.pdf" target="_blank">Service Processor Managers Data Sheet</a>    
    </td>
    </tr>
    </tbody></table>
    
</td>
</tr><tr><td>&nbsp;</td></tr>
</tbody></table>

<table border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td style="padding-right: 10px;">
    <img alt="" src="images/san-pham/btn_products.gif" height="24px" width="24px">
</td>
<td class="ProductsDownload">Firmware
</td>
</tr>

<tr>
<td></td>
<td>

    <table border="0" cellpadding="0" cellspacing="0">
    <tbody><tr>
    <td class="ProductsDownloadLinks" style="height: 20px;">
        <a id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_ctl59_ctl03_repeaterDownloadSource_ctl01_repeaterDownLoadText_ctl00_HyperLink1" class="ProductsDownloadLinks" href="http://www.avocent.com/Support_Firmware/MergePoint_SPM/MergePoint_5300_Service_Processor_Manager.aspx" target="_blank">SPM 5300 Firmware Updates</a>    
    </td>
    </tr>
    </tbody></table>
    
    <table border="0" cellpadding="0" cellspacing="0">
    <tbody><tr>
    <td class="ProductsDownloadLinks" style="height: 20px;">
        <a id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_ctl59_ctl03_repeaterDownloadSource_ctl01_repeaterDownLoadText_ctl01_HyperLink1" class="ProductsDownloadLinks" href="http://www.avocent.com/Support_Firmware/MergePoint_SPM/MergePoint_5324_5340_Service_Processor_Managers.aspx" target="_blank">SPM 5324/5340 Firmware Updates</a>    
    </td>
    </tr>
    </tbody></table>
    
</td>
</tr><tr><td>&nbsp;</td></tr>
</tbody></table>

<table border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td style="padding-right: 10px;">
    <img alt="" src="images/san-pham/Manual_alt.gif" height="24px" width="24px">
</td>
<td class="ProductsDownload">Manual
</td>
</tr>

<tr>
<td></td>
<td>

    <table border="0" cellpadding="0" cellspacing="0">
    <tbody><tr>
    <td class="ProductsDownloadLinks" style="height: 20px;">
        <a id="ctl00_SPWebPartManager1_g_2d2d01e3_0b58_4a08_ad66_0185de3e2865_ctl59_ctl03_repeaterDownloadSource_ctl02_repeaterDownLoadText_ctl00_HyperLink1" class="ProductsDownloadLinks" href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Documents/Manuals/01/590989501C.pdf" target="_blank">Service Processor Managers Installer/User Guide</a>    
    </td>
    </tr>
    </tbody></table>
    
</td>
</tr><tr><td>&nbsp;</td></tr>
</tbody></table>



	</div><input name="ctl00$SPWebPartManager1$g_2d2d01e3_0b58_4a08_ad66_0185de3e2865$ctl59$ctl02" value="Downloads" type="hidden">
</div>
		</div></div>
	</div>
</div></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
                    </div>
                    <div>
                        
                    </div>
                    <div>
                        
                    </div>
                    <div class="EditModePanel">
                        
</div>
</td>
<td class="rightpaneENP" valign="top">                    
                    <div></div>
                </td>
                    </tr>
                    </tbody></table>      
      </DIV></TD></TR></TBODY></TABLE>