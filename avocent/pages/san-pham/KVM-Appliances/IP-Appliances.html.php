<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-products.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/index.html">Sản phẩm</A></SPAN><SPAN> 
      &gt; </SPAN> <span><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/kvm-appliances.html">Thiết bị KVM</a> &gt; </span><SPAN class="CurrentBreadCrumb">KVM IP</SPAN></SPAN>
      </DIV>
      <DIV>
<table style="width: 100%;" class="contentplace" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td class="ProductArticleLeftGap" valign="top">
<div style="margin-bottom:30px;" class="BodyText">
<div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" style="display:inline">
<h1 class="ms-rteCustom-ArticleTitle">KVM IP</h1>
<p>Loại bỏ hạn chế về khoảng cách với hệ thống KVM over IP bằng cách cho phép truy cập và kiểm soát các server từ xa và thiết bị mạng khác từ trung tâm dữ liệu, phòng NOC hoặc từ bất kỳ vị trí nào khác.</p></div>
</div>
<div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartWPQ2" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="e70a782e-372b-48d5-a44e-4b12a8c3039d" haspers="false" id="WebPartWPQ2" width="100%" class="ms-WPBody" allowdelete="false" style=""><table id="cbqwp" class="cbq-layout-main" cellpadding="0" cellspacing="0"><tbody><tr><td id="column" "%"="" valign="top"><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/MergePoint-Unity-KVM-over-IP-and-Serial-Console-Appliance_re.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="http://nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/ip-kvm/cat-5-kvm-switch.html?page=shop.product_details&flypage=flypage.tpl&product_id=826&category_id=73" class="prodCEOtitlelink" target="_blank" title="">MergePoint Unity</a></div><div class="ProdClassBody">MergePoint Unity switch bao gồm KVM over IP và công nghệ quản lý serial console. Khả năng quản lý tập trung nhiều thiết bị với phần mềm DSView 3.</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/AutoView-Appliances-Digital_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="http://nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/ip-kvm/cat-5-kvm-switch.html?page=shop.product_details&flypage=flypage.tpl&product_id=851&category_id=73" class="prodCEOtitlelink" target="_blank" title="">AutoView IP</a></div><div class="ProdClassBody">Thiết bị AutoView KVM IP cung cấp truy cập tới server từ bất cứ đâu qua giao diện Web.</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/SwitchView-Digital-KVM-Appliance.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="http://nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/ip-kvm/kvm-switch.html?page=shop.product_details&flypage=flypage.tpl&product_id=855&category_id=72" class="prodCEOtitlelink" target="_blank" title="">SwitchView IP</a></div><div class="ProdClassBody">Hiệu quả chi phí, thiết bị KVM digital cung cấp truy cập và điều khiển server.</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="footer"></div></td></tr></tbody></table></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
</div>
<div class="EditModePanel">

</div>
</td>
<td class="rightpaneENP" valign="top">&nbsp;</td>
</tr>
</tbody></table>      
      </DIV></TD></TR></TBODY></TABLE>