<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-products.jpg" 
      width="760" height="130"></DIV>
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/index.html">Sản phẩm</A></SPAN><SPAN> 
      &gt; </SPAN> <span><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/kvm-appliances.html">Thiết bị KVM</a> &gt; </span><SPAN class="CurrentBreadCrumb">KVM Analog</SPAN></SPAN>
      </DIV>
      <DIV>
<table style="width: 100%;" class="contentplace" border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td class="ProductArticleLeftGap" valign="top">
<div style="margin-bottom:30px;" class="BodyText">
<div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" style="display:inline"><h1 class="ms-rteCustom-ArticleTitle">KVM Analog</h1>
<p>Đơn giản truy cập vào tủ rack tại local cho người dùng với công nghệ KVM Analog. Một nền tảng vững chắc cho việc quản lý các nhóm lớn hơn của thiết bị cũng như cho phép các tính năng tiên tiến như chuyển mạch ma trận, virtual media và chứng thực bằng smar card/CAC.</p></div>
</div>
<div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartWPQ2" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="5448bcdc-0126-4845-9341-5cd88fb3b407" haspers="false" id="WebPartWPQ2" width="100%" class="ms-WPBody" allowdelete="false" style=""><table id="cbqwp" class="cbq-layout-main" cellpadding="0" cellspacing="0"><tbody><tr><td id="column" "%"="" valign="top"><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/AutoView-Analog-KVM-Appliance_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="http://nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/analog-kvm/cat-5-kvm-switch.html?page=shop.product_details&flypage=flypage.tpl&product_id=789&category_id=70" class="prodCEOtitlelink" target="_blank" title="">AutoView</a></div><div class="ProdClassBody">Lắp đặt cho môi trường phòng server của SMB, thiết bị KVM analog được sử dụng để kiểm soát và truy cập server của bạn. Bao gồm những tính năng cao cấp như virtual media và chứng thực smart cart/CAC.</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="linkitem" xmlns:ddwrt="http://schemas.microsoft.com/WebParts/v2/DataView/runtime" xmlns:dt="urn:schemas-microsoft-com:datatypes"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td class="PaddingRight10px" valign="top"><img src="images/san-pham/SwitchView-Analog-KVM-Appliances_representative.png"></td><td valign="top"><div class="prodCEOtitlelink"><a href="http://nsp.com.vn/vi/san-pham/server-room-a-dc-infrastructure-management/analog-kvm/kvm-switch.html?page=shop.product_details&flypage=flypage.tpl&product_id=794&category_id=55" class="prodCEOtitlelink" target="_blank" title="">SwitchView</a></div><div class="ProdClassBody">KVM analog SwitchView cung cấp nhiều người dùng, giải pháp hiệu quả chi phí, kết nối PS/2 và/hoặc USB và thiết bị đích trong phòng server SMB.</div></td></tr><tr><td colspan="2" style="height: 25px;"></td></tr></tbody></table></div><div id="footer"></div></td></tr></tbody></table></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
</div>
<div class="EditModePanel">

</div>
</td>
<td class="rightpaneENP" valign="top">&nbsp;</td>
</tr>
</tbody></table>      
      </DIV></TD></TR></TBODY></TABLE>