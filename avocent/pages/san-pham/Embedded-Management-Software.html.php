<TABLE style="width: 100%;" border="0" cellSpacing="0" cellPadding="0">
  <TBODY>
  <TR>
    <TD class="leftpane2" vAlign="top">
      <?php require(PATH_LIB.DS."left-menu.php"); ?>
      </TD>
    <TD id="contentarea2" vAlign="top">
      <DIV class="Pagetopimage2"><IMG title="Avocent" alt="Avocent" src="images/avocent-products.jpg" 
      width="760" height="130"></DIV>
      
      <DIV id="breadcrumsection"><SPAN id="ctl00_PlaceHolderMain_ctl01_siteMapPath"><SPAN><A class="NodeBreadCrumb" 
      title="Emerson Network Power's Avocent brand offers open, secure, modular solutions that enable customers to simply manage IT complexity." 
      href="<?php echo LIVE_SITE?>">Avocent</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN><A class="NodeBreadCrumb" href="<?php echo LIVE_SITE?>san-pham/index.html">Sản phẩm</A></SPAN><SPAN> 
      &gt; </SPAN><SPAN class="CurrentBreadCrumb">MergePoint Embedded Management Software (EMS)</SPAN></SPAN>
      </DIV>
      <DIV>
      
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
                    <tbody><tr>
                 
                     <td style="padding-right: 10px;" valign="top" width="600px">  
                     
                <div class="ProductImage">
                <table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td style="width: 90%;" valign="top">
                                    <div style="padding-left:10px; padding-top:10px;">
                                        <span class="detailheading">
                                            <div id="ctl00_PlaceHolderMain_RichHtmlField7__ControlWrapper_RichHtmlField" style="display:inline">MergePoint Embedded Management Software (EMS)</div>
                                        </span>
                                    </div>
                                    <div class="BodyText" style="margin-left:10px;">
                                        <div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" style="display:inline"><h2>Remote access and device management for baseboard service processors</h2></div>
                                    </div>
                                </td>
                                <td class="ImageRotator" style="padding-right: 7px; padding-top: 10px;" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="90e20548-14a1-4525-8306-c8b4d72b15bb" haspers="false" id="WebPartctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb" width="100%" class="ms-WPBody" allowdelete="false" style=""><div id="ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb">
					<table style="border-collapse: collapse;" border="0" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td id="ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb_smallImageTD"></td><td style="height: 170px; width: 180px;" align="center" valign="middle"><img id="ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb_imageNormal" src="images/san-pham/MergePoint-Embedded-Management_1_small.png" style="width: 170px; border-width: 0px;"></td><td><input name="ctl00$SPWebPartManager1$g_90e20548_14a1_4525_8306_c8b4d72b15bb$hiddenLarge" id="ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb_hiddenLarge" type="hidden"><input name="ctl00$SPWebPartManager1$g_90e20548_14a1_4525_8306_c8b4d72b15bb$hiddenimageNormal" id="ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb_hiddenimageNormal" value="/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial Appliances/MergePoint Embedded Management/MergePoint-Embedded-Management_1_small.png" type="hidden"><input name="ctl00$SPWebPartManager1$g_90e20548_14a1_4525_8306_c8b4d72b15bb$hiddenSmall" id="ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb_hiddenSmall" value="/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial Appliances/MergePoint Embedded Management/MergePoint-Embedded-Management_1_thumbnail.png" type="hidden"><input name="ctl00$SPWebPartManager1$g_90e20548_14a1_4525_8306_c8b4d72b15bb$hiddenNormalAlt" id="ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb_hiddenNormalAlt" type="hidden"></td><td class="ClickContainer" style="width: 55px; padding-top: 2px; padding-left: 5px;" align="center" valign="top"><span id="ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb_divZoom"><a id="ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb_hypZoom" href="javascript:ViewZoom('ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb_hiddenLarge');" style="display: none;">Click to Zoom</a></span></td><td valign="top"><table style="border-collapse: collapse; margin-left: 5px;" border="0" cellpadding="0" cellspacing="0">
								<tbody><tr>
									<td style="height: 30px; width: 30px;"><a id="ctl00_SPWebPartManager1_g_90e20548_14a1_4525_8306_c8b4d72b15bb_hyperlinkID0" title="MergePoint Embedded Management Software &amp;#40;EMS&amp;#41;" class="OrangeBrd" onmouseover="hovernormalimage('/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial Appliances/MergePoint Embedded Management/MergePoint-Embedded-Management_1_small.png')" onmouseout="mouseoutnormalimage()" onclick="LinkClick(this);" href="javascript:ViewNormalImage('/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial%20Appliances/MergePoint%20Embedded%20Management/MergePoint-Embedded-Management_1_small.png','','/en-US/Products/InfrastructureManagement/Monitoring-Control/Hardware/SerialAppliances/PublishingImages/Serial%20Appliances/MergePoint%20Embedded%20Management/MergePoint-Embedded-Management_1_thumbnail.png','MergePoint%20Embedded%20Management%20Software%20(EMS)');"><img class="thumbImage" src="images/san-pham/MergePoint-Embedded-Management_1_thumbnail.png" style="height: 25px; width: 25px; border-width: 0px;"></a></td>
								</tr>
							</tbody></table></td>
						</tr>
					</tbody></table>
				</div></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
                                </td>
                                
                            </tr>
                        </tbody></table></div>
                    <div style="margin-bottom: 30px;" class="BodyText">
                        <div id="ctl00_PlaceHolderMain_RichHtmlField2__ControlWrapper_RichHtmlField" style="display:inline"><p><br>Avocent MergePoint<sup>®</sup>
 EMS allows original equipment manufacturers (OEM) and original design 
manufacturers (ODM) the ability to implement powerful, remote access and
 device management functionality on platforms designed to support 
baseboard service processors.</p>
<p>MergePoint EMS supports the latest industry standards such as IPMI 
2.0, DCMI, PMBus and Avocent's widely-adopted, embedded KVM and virtual 
media technologies.</p>
<p>Customers benefit from a mature solution that can be quickly 
integrated with their platform portfolio, reducing bill of material 
(BOM) costs and time to market (TTM) while increasing product 
differentiation.</p></div>
                    </div>
                    <div>
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td id="MSOZoneCell_WebPartctl00_SPWebPartManager1_g_de8e1570_a639_4b83_9a77_8dfd7a516773" valign="top"><table toplevel="" border="0" cellpadding="0" cellspacing="0" width="100%">
			<tbody><tr>
				<td valign="top"><div webpartid="de8e1570-a639-4b83-9a77-8dfd7a516773" haspers="false" id="WebPartctl00_SPWebPartManager1_g_de8e1570_a639_4b83_9a77_8dfd7a516773" width="100%" class="ms-WPBody" allowdelete="false" allowexport="false" style=""><div id="ctl00_SPWebPartManager1_g_de8e1570_a639_4b83_9a77_8dfd7a516773">
	<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" id="ctl00_SPWebPartManager1_g_de8e1570_a639_4b83_9a77_8dfd7a516773_tabs"><div class="innercontent" style="height:450px;"><div class="ui-tabs-panel ui-widget-content ui-corner-bottom" id="ctl00_SPWebPartManager1_g_de8e1570_a639_4b83_9a77_8dfd7a516773_tab0">
			<a name="Overview"></a><h3>Overview</h3><p><strong>Features and Benefits</strong></p><ul><li><strong>High-Performance KVM</strong> - Capture, compress, encrypt and transmit KVM streams </li><li><strong>Seamless KVM and Virtual Media</strong> - Sessions are maintained during power cycling or when the server is turned off, with no requirement to restart sessions </li><li><strong>Silicon Independence</strong> - Easy to extend and port to multiple silicon cores including PowerPC<sup>®</sup>, ARM7/9, StrongARM/XScale, H8S, SH4 and MIPS, with ports for various operating systems including Linux<sup>®</sup> </li><li><strong>Powerful, Integrated Console</strong> - Embedded web server simplifies administration </li><li><strong>Fast Time-to-Revenue</strong> - An off-the-shelf design that includes robust development tools for easy integration and extension<br></li></ul>
		</div><div class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="ctl00_SPWebPartManager1_g_de8e1570_a639_4b83_9a77_8dfd7a516773_tab1">
			<a name="SDK"></a><h3>SDK</h3><p><strong>Application Development Package</strong></p><p>MergePoint
 EMS Advanced Development Package (ADP) is a software development kit 
(SDK) that decreases time to market for the OEMs and ODMs by 
facilitating the server platform customization and addition of 
OEM-specific features. ADP allows OEMs and ODMs to architect and 
implement differentiating manageability features, while minimizing the 
engineering development time needed to enable standard manageability 
functions.</p><p>ADP is delivered in the form of a customized release 
targeted at, and optimized for, an application-specific integrated 
circuit (ASIC) platform, but the programming interface and most features
 are common across platforms. This enables engineers to easily move from
 one platform to another, leveraging the expertise acquired and 
increasing engineering efficiencies. MergePoint EMS ADP for a given 
platform is a complete SDK and includes firmware libraries, complete 
build environment, development utilities and documentation.</p>
		</div><div class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="ctl00_SPWebPartManager1_g_de8e1570_a639_4b83_9a77_8dfd7a516773_tab2">
			<a name="Professional Services"></a><h3>Professional Services</h3><p><strong>Avocent Professional Services</strong></p><p>Avocent's
 Professional Services Team provides a full range of services that 
include implementation services, customization services to meet your 
specific needs, training and custom service offerings. Our services team
 brings a wealth of experience developing embedded software for some of 
the world's largest server and device manufacturers.</p>
		</div><div class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide" id="ctl00_SPWebPartManager1_g_de8e1570_a639_4b83_9a77_8dfd7a516773_tab3">
			<a name="Downloads"></a><h3>Downloads</h3><div>
	<div>
		
<table border="0" cellpadding="0" cellspacing="0">
<tbody><tr>
<td style="padding-right: 10px;">
    <img alt="" src="images/san-pham/btn_SalesLiterature.gif" height="24px" width="24px">
</td>
<td class="ProductsDownload">Brochures and Data Sheets
</td>
</tr>

<tr>
<td></td>
<td>

    <table border="0" cellpadding="0" cellspacing="0">
    <tbody><tr>
    <td class="ProductsDownloadLinks" style="height: 20px;">
        <a id="ctl00_SPWebPartManager1_g_de8e1570_a639_4b83_9a77_8dfd7a516773_ctl31_ctl03_repeaterDownloadSource_ctl00_repeaterDownLoadText_ctl00_HyperLink1" class="ProductsDownloadLinks" href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Documents/DataSheets/01/MPEMS-DS-EN.pdf" target="_blank">EMS Data Sheet</a>    
    </td>
    </tr>
    </tbody></table>
    
    <table border="0" cellpadding="0" cellspacing="0">
    <tbody><tr>
    <td class="ProductsDownloadLinks" style="height: 20px;">
        <a id="ctl00_SPWebPartManager1_g_de8e1570_a639_4b83_9a77_8dfd7a516773_ctl31_ctl03_repeaterDownloadSource_ctl00_repeaterDownLoadText_ctl01_HyperLink1" class="ProductsDownloadLinks" href="http://www.emersonnetworkpower.com/en-US/Brands/Avocent/Documents/DataSheets/01/MPADP-DS-EN.pdf" target="_blank">EMS ADP Data Sheet</a>    
    </td>
    </tr>
    </tbody></table>
    
</td>
</tr><tr><td>&nbsp;</td></tr>
</tbody></table>



	</div><input name="ctl00$SPWebPartManager1$g_de8e1570_a639_4b83_9a77_8dfd7a516773$ctl31$ctl02" value="Downloads" type="hidden">
</div>
		</div></div>
	</div>
</div></div></td>
			</tr>
		</tbody></table></td>
	</tr>
</tbody></table>
                    </div>
                    <div>
                        
                    </div>
                    <div>
                        
                    </div>
                    <div class="EditModePanel">
                        
</div>
</td>
<td class="rightpaneENP" valign="top">&nbsp;</td>
                    </tr>
                    </tbody></table>      </DIV></TD></TR></TBODY></TABLE>