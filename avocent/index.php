<?php

include('defined.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<base href="<?php echo LIVE_SITE?>" />
<link href="css/general.css" type="text/css" rel="stylesheet" />
<link href="css/core.css" type="text/css" rel="stylesheet" />
<link href="css/styles.css" type="text/css" rel="stylesheet" />
<title>Avocent</title>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16757079-10']);
  _gaq.push(['_setDomainName', '.nsp.com.vn']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
<body>
<div id="a-primary">
	<div id="a-page">
    	<div class="a-top">
        	<div class="logo">
            	<a href="" class="left-logo"><img src="images/logo.gif" border="0" vspace="10" /></a>
                <a href="http://nsp.com.vn" class="right-logo"><img src="images/logo-nsp-avocent.png" border="0" vspace="10" /></a>
            </div>
            <div class="menu"><?php include('lib/menu.php')?></div>
            <div style="clear:both"></div>
        </div><!-- top -->
        <div class="a-content">
          <div class="inner-content">
        	<?php
			$page = 'index.html';
			$path_info = $_SERVER['PATH_INFO']; //var_dump($_SERVER);exit;
			if(!isset($path_info)) {
				$path_info = $_SERVER['ORIG_PATH_INFO'];
			}//var_dump($path_info);exit;
			if( !$path_info ){
				$path_info = $_SERVER['REQUEST_URI'];
				$path_info = str_replace('/avocent/','/',$path_info);
			}
			
			//var_dump($_SERVER['REQUEST_URI']);exit;
			if(preg_match('/.php/',$path_info))
				return;
			$explode = explode('/',$path_info);
			array_shift($explode);
			if(count($explode)) $page = implode('/',$explode);
			//if($explode[0]) $page = $explode[0];
			if(!$page) $page = 'index.html';
			$page .= '.php';
			include('pages/'.$page);
			?>
            </div>
        </div>
        <?php include('lib/footer.php')?>
    </div><!-- page-->
</div><!-- primary-->

</body>
</html>
