﻿function EmailSubmit(url) {
    var pageurl = url + "?Title=" + document.title + "&Link=" + document.URL;
    var newwindow = window.open(pageurl, "_blank", "width=525,height=400");
    return false;
}
    function clearFields(formdata) {
            if(!formdata || !formdata.elements) return;
            
            for(var i=0,k=formdata.elements.length;i<k;i++)
            {            
             if(typeof(formdata.elements[i].type)!='string')
                 continue;
                 
                switch(formdata.elements[i].type.toLowerCase()) {
                    case 'text':
                    case 'textarea':
                    case 'password':
                    formdata.elements[i].value='';
                break;
                    case 'radio':
                    case 'checkbox':
                    formdata.elements[i].checked=false;
                break;
                    case 'select-one':
                    case 'select-multiple':
                    formdata.elements[i].selectedIndex=0;
                break;
            }
          }
 }

function ActivateTab(tabindex,activeTabIndex)
{
	var tabvalue = document.getElementById("HdnTabInformation").value;
	var tabs = tabvalue.split('~');
	var tabToActivate;
	
	for(var counter=0;counter<tabs.length;counter++)
	{
		var tab = tabs[counter].split(':');
		if(tab.length==2)
		{
			if(tab[0]==tabindex)
			{
				tabToActivate = tab[1];
				break;
			}
		}
	}
	
	var tabDiv = document.getElementById(tabToActivate);


	$(document).ready(function(){
	$(tabDiv).tabs('select', activeTabIndex); 
	});
}



function toLeft(id) {

    document.getElementById(id).scrollLeft = 0
    return false;

}

function scrollDivLeft(id) {

    /* clearTimeout(timerRight) 
    document.getElementById(id).scrollLeft+=scrollStep
    timerRight=setTimeout("scrollDivLeft('"+id+"')",3)*/
    document.getElementById(id).scrollLeft += 90;
    return false;

}

function scrollDivRight(id) {

    /*clearTimeout(timerLeft)
    document.getElementById(id).scrollLeft-=scrollStep
    timerLeft=setTimeout("scrollDivRight('"+id+"')",3)*/

    document.getElementById(id).scrollLeft -= 90;
    return false;
}

function toRight(id) {

    document.getElementById(id).scrollLeft = document.getElementById(id).scrollWidth
    return false;
}