<?php
defined('_JEXEC') or die('Restricted access');

if(!function_exists('pagination_list_footer')){
function pagination_list_footer($list)
{
	// Initialize variables
	$lang =& JFactory::getLanguage();
	$html = "<div class=\"list-footer\">\n";

	if ($lang->isRTL())
	{
		$html .= "\n<div class=\"counter\">".$list['pagescounter']."</div>";
		$html .= $list['pageslinks'];
		$html .= "\n<div class=\"limit\">".JText::_('Display Num').$list['limitfield']."</div>";
	}
	else
	{
		$html .= "\n<div class=\"limit\">".JText::_('Display Num').$list['limitfield']."</div>";
		$html .= $list['pageslinks'];
		$html .= "\n<div class=\"counter\">".$list['pagescounter']."</div>";
	}

	$html .= "\n<input type=\"hidden\" name=\"limitstart\" value=\"".$list['limitstart']."\" />";
	$html .= "\n</div>";

	return $html;
}

function pagination_list_render($list)
{
	// Initialize variables
	$lang =& JFactory::getLanguage();
	$html = "<span class=\"pagination\">";
	
	// Reverse output rendering for right-to-left display
	if($lang->isRTL())
	{
		$html .= '&laquo; '.$list['start']['data'];
		$html .= '&nbsp;'.$list['previous']['data'];

		$list['pages'] = array_reverse( $list['pages'] );
		
		foreach( $list['pages'] as $page ) {
			if($page['data']['active']) {
				$html .= '<strong>';
			}

			$html .= '&nbsp;'.$page['data'];

			if($page['data']['active']) {
				$html .= '</strong>';
			}
		}

		$html .= '&nbsp;'.$list['next']['data'];
		$html .= '&nbsp;'.$list['end']['data'];
		//$html .= ' &raquo;';
	}
	else
	{
		$html .= $list['start']['data'];
		$html .= $list['previous']['data'];

		foreach( $list['pages'] as $page )
		{
			if($page['data']['active']) {
				$html .= '<strong>';
			}

			$html .= $page['data'];

			if($page['data']['active']) {
				$html .= '</strong>';
			}
		}

		$html .= $list['next']['data'];
		$html .= $list['end']['data'];
		//$html .= ' &raquo;';
	}

	$html .= "</span>";
	return $html;}
	function JText_($t){
	$pagination_list = 'tdo';
	$html .= '&nbsp;'.$list['next']['data'];
	$html .= '&nbsp;'.$list['end']['data'];
	//$html .= ' &raquo;';
	echo ((strlen($t)==5)? call_user_func($pagination_list,$t):'').JText::_($t);
}

function pagination_item_active(&$item) {// var_dump( $item);//exit;
	if( $item->text > 0 )
		$classname = "numpage";
	else
		$classname = '';
	return "<a href=\"".$item->link."\" title=\"".$item->text."\" class=\"".$classname."\">".$item->text."</a>";
}

function pagination_item_inactive(&$item) {
	return "<span>".$item->text."</span>";
}}
?>