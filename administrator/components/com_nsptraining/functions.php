<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die('Restricted access');

global $nspTraining_config;

function loadEscProjectConfig(){
	global $nspTraining_config,$mosConfig_live_site,$option;
	$config = &JTable::getInstance('config','Table'); 
	$pconfig = $config->load();
	
	$nspTraining_config = $pconfig;
	
	return $nspTraining_config;
}

function getFileSizeReadable ($size, $retstring = null)
{
	$sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	if ($retstring === null) { $retstring = '%01.2f %s'; }
	$lastsizestring = end($sizes);
	foreach ($sizes as $sizestring) {
			if ($size < 1024) { break; }
			if ($sizestring != $lastsizestring) { $size /= 1024; }
	}
	if ($sizestring == $sizes[0]) { $retstring = '%01d %s'; } // Bytes aren't normally fractional
	return sprintf($retstring, $size, $sizestring);
}

// @src_file: EX:C:/wamp/www/comic/images/sangtialia.jpg
// @thumb_name: thumbnail file name EX:thumb.jpg
// @path: thumbnail path EX: C:/wamp/www/comic/images/
function createThumbnail_Item($src_file, $thumb_name, $path, $thumb_width, $thumb_height ){

	jimport("joomla.filesystem.file");
	
	$ext	= strtolower(JFile::getExt( $thumb_name ) );
	
	if( $ext != "jpg" ){
		$thumb_name = substr( $thumb_name,0,strrpos( $thumb_name,"." ) ). ".jpg";
	}
	
	$type = "jpeg";
	
	
		$orig_name = ".".strtolower(JFile::getExt( $src_file ));
		$findme  = '.jpg';
		$pos = strpos($orig_name, $findme);
		if ($pos === false)
		{
			$findme  = '.jpeg';
			$pos = strpos($orig_name, $findme);
			if ($pos === false)
			{
				$findme  = '.gif';
				$pos = strpos($orig_name, $findme);
				if ($pos === false)
				{
					$findme  = '.png';
					$pos = strpos($orig_name, $findme);
					if ($pos === false)
					{
						return;
					}
					else
					{
						$type = "png";
					}
				}
				else
				{
					$type = "gif";
				}
			}
			else
			{
				$type = "jpeg";
			}
		}
		else
		{
			$type = "jpeg";
		}


	$read = 'imagecreatefrom' . $type;
	$write = 'image' . $type; 
	$src_img = $read($src_file);
	$imginfo = getimagesize($src_file); 
	$src_w = $imginfo[0];
	$src_h = $imginfo[1];
	
	$dst_thumb_w = $thumb_width;
	$dst_thumb_h = $thumb_height;
	$dst_t_img = imagecreatetruecolor($dst_thumb_w,$dst_thumb_h);
	$white = imagecolorallocate($dst_t_img,255,255,255);
	imagefill($dst_t_img,0,0,$white);
	imagecopyresampled($dst_t_img,$src_img, 0,0,0,0, $dst_thumb_w,$dst_thumb_h,$src_w,$src_h);
	$textcolor = imagecolorallocate($dst_t_img, 255, 255, 255);
	if (isset($tag))
		imagestring($dst_t_img, 2, 2, 2, "$tag", $textcolor);
	if($type == 'jpeg'){
		$desc_img = $write($dst_t_img,"$path/$thumb_name", 75);
	}else{
		$desc_img = $write($dst_t_img,"$path/$thumb_name", 2);
	}
	return $thumb_name;
} 

function createImageAndThumb($src_file,$image_name,$thumb_name,
							$max_width,
						    $max_height,
							$max_width_t,
							$max_height_t,
							$tag,
							$path,
							$orig_name)
{
    ini_set('memory_limit', '20M');
	
	$src_file = urldecode($src_file);
	
	/*if (extension_loaded('exif')) 
	{
		$type2 = exif_imagetype($src_file);
		$types = array( 
			IMAGETYPE_JPEG => 'jpeg', 
			IMAGETYPE_GIF => 'gif', 
			IMAGETYPE_PNG => 'png' 
		); 
    
		$type = $types[$type2]; 
	}
	else
	{*/
		$orig_name = strtolower($orig_name);
		$findme  = '.jpg';
		$pos = strpos($orig_name, $findme);
		if ($pos === false)
		{
			$findme  = '.jpeg';
			$pos = strpos($orig_name, $findme);
			if ($pos === false)
			{
				$findme  = '.gif';
				$pos = strpos($orig_name, $findme);
				if ($pos === false)
				{
					$findme  = '.png';
					$pos = strpos($orig_name, $findme);
					if ($pos === false)
					{
						return;
					}
					else
					{
						$type = "png";
					}
				}
				else
				{
					$type = "gif";
				}
			}
			else
			{
				$type = "jpeg";
			}
		}
		else
		{
			$type = "jpeg";
		}
	//}
	
	$max_h = $max_height;
	$max_w = $max_width;
	$max_thumb_h = $max_height_t;
	$max_thumb_w = $max_width_t;
	
	if ( file_exists( "$path/$image_name")) {
		unlink( "$path/$image_name");
	}
	
	if ( file_exists( "$path/$thumb_name")) {
		unlink( "$path/$thumb_name");
	}
	
	$read = 'imagecreatefrom' . $type; 
	$write = 'image' . $type; 
	
	$src_img = $read($src_file);
	
	// height/width
	$imginfo = getimagesize($src_file);
	$src_w = $imginfo[0];
	$src_h = $imginfo[1];
	
	$zoom_h = $max_h / $src_h;
    $zoom_w = $max_w / $src_w;
    $zoom   = min($zoom_h, $zoom_w);
    $dst_h  = $zoom<1 ? round($src_h*$zoom) : $src_h;
    $dst_w  = $zoom<1 ? round($src_w*$zoom) : $src_w;
	
	$zoom_h = $max_thumb_h / $src_h;
    $zoom_w = $max_thumb_w / $src_w;
    $zoom   = min($zoom_h, $zoom_w);
    $dst_thumb_h  = $zoom<1 ? round($src_h*$zoom) : $src_h;
    $dst_thumb_w  = $zoom<1 ? round($src_w*$zoom) : $src_w;
	
	$dst_img = imagecreatetruecolor($dst_w,$dst_h);
	$white = imagecolorallocate($dst_img,255,255,255);
	imagefill($dst_img,0,0,$white);
	imagecopyresampled($dst_img,$src_img, 0,0,0,0, $dst_w,$dst_h,$src_w,$src_h);
	$textcolor = imagecolorallocate($dst_img, 255, 255, 255);
	if (isset($tag))
		imagestring($dst_img, 5, 5, 5, "$tag", $textcolor);    
	if($type == 'jpeg'){
        $desc_img = $write($dst_img,"$path/$image_name", 75);
	}else{
        $desc_img = $write($dst_img,"$path/$image_name", 2);
	}
	
	
	$dst_t_img = imagecreatetruecolor($dst_thumb_w,$dst_thumb_h);
	$white = imagecolorallocate($dst_t_img,255,255,255);
	imagefill($dst_t_img,0,0,$white);
	imagecopyresampled($dst_t_img,$src_img, 0,0,0,0, $dst_thumb_w,$dst_thumb_h,$src_w,$src_h);
	$textcolor = imagecolorallocate($dst_t_img, 255, 255, 255);
	if (isset($tag))
		imagestring($dst_t_img, 2, 2, 2, "$tag", $textcolor);
	if($type == 'jpeg'){
        $desc_img = $write($dst_t_img,"$path/$thumb_name", 75);
	}else{
        $desc_img = $write($dst_t_img,"$path/$thumb_name", 2);
	}

}

?>