<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Course {
	function showCourse( &$rows, &$pageNav, $option, &$lists ) {
		
		$user =& JFactory::getUser();
		$ordering = ($lists['order'] == 'cr.id' || $lists['order'] == 'cr.name' || $lists['order'] == 'cr.ordering');
		//echo $lists['order'];
		JHTML::_('behavior.tooltip');
		$tmpl = JRequest::getVar('tmpl');
		$popup = ($tmpl=="component" ? true : false);
		?>
		<form action="?option=com_nsptraining" method="post" name="adminForm">

		<table width="100%">
		<tr>
			<td align="left">
				<?php echo JText::_( 'Filter (Name)' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button></td>
			<td align="center" nowrap="nowrap">
				<?php echo JText::_( 'Category Filter' ); ?>:<?php
				echo $lists['category'];
				?></td>
			<td align="center" nowrap="nowrap">
				</td>	
			<td align="right" nowrap="nowrap"><?php echo JText::_( 'State Filter' ); ?>:
				<?php
				echo $lists['state'];
				?></td>
		</tr>
		</table>

		<div id="tablecell">
			<table class="adminlist">
			<thead>
				<tr>
					<th width="5" nowrap="nowrap"><?php echo JText::_( 'NUM' ); ?></th>
					<th width="20" nowrap="nowrap"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th width="1%" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'ID', 'cr.id', @$lists['order_Dir'], @$lists['order'], 'list_courses' ); ?></th>
					<th nowrap="nowrap" class="title"><?php echo JHTML::_('grid.sort', 'Name', 'cr.name', @$lists['order_Dir'], @$lists['order'], 'list_courses' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Category Name', 'category_name', @$lists['order_Dir'], @$lists['order'], 'list_courses' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Author', 'author', @$lists['order_Dir'], @$lists['order'], 'list_courses' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Date', 'cr.created_date', @$lists['order_Dir'], @$lists['order'], 'list_courses' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Published', 'cr.published', @$lists['order_Dir'], @$lists['order'], 'list_courses' ); ?></th>
					<th colspan="3" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'Ordering', 'cr.ordering', @$lists['order_Dir'],      @$lists['order'], 'list_courses' ); ?><?php echo JHTML::_('grid.order', $rows, 'filesave.png', 'save_course_order' ); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $pageNav->getListFooter(); ?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0; 
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];

				$link 		= 'index.php?option=com_nsptraining&task=edit_course&cid[]='. $row->id ;

				$checked 	= JHTML::_('grid.checkedout', $row, $i );
				$published 	= JHTML::_('grid.published', $row, $i );
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td><?php echo $pageNav->getRowOffset( $i ); ?></td>
					<td><?php echo $checked; ?></td>
					<td align="center"><?php echo $row->id; ?></td>
					<td>
					<?php if( $popup){
						echo "<a href='#' onClick=\"window.parent.escProjectsProject('".$row->id."','".$row->name."')\">".$row->name."</a>";
					}else{?>
						<a href="<?php echo JRoute::_( $link ); ?>" title="<?php echo JText::_( 'Edit Project' ); ?>"><?php echo $row->name; ?></a>
					<?php }?>
					</td>
					<td width="1%" align="center"><?php echo $row->category_name;?></td>
					<td align="left"><?php echo $row->author;?></td>
					<td align="center"><?php echo date("F d, Y",strtotime($row->created_date));?></td>
					<td align="center"><?php echo $published;?></td>
					<td width="1%" align="center"><?php  echo $pageNav->orderUpIcon( $i, ($row->book_id == @$rows[$i-1]->book_id) , 'orderup_course', 'Move Up', $ordering); ?></td>
					<td width="1%" align="center"><?php echo $pageNav->orderDownIcon( $i, $n, ($row->book_id == @$rows[$i+1]->book_id), 'orderdown_course', 'Move Down', $ordering ); ?></td>
					<td width="1%" align="center" nowrap="nowrap"><?php $disabled = $ordering ? '' : 'disabled="disabled"'; ?><input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" <?php echo $disabled; ?> class="text_area" style="text-align: center" /></td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>
		</div>

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="list_courses" />
		<input type="hidden" name="section" value="manage_course" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="" />
		</form>
		<?php
	}
	
	function editCourse( &$row, &$lists ) {
		global $option,$nspTraining_config;
		if (JRequest::getCmd('task') == 'add_course') {
			$row->published = '1';
			$row->category_id = '';
			$row->description = '';
			$row->ordering = '999';
		}
		JRequest::setVar( 'hidemainmenu', 1 );

		$editor =& JFactory::getEditor();
		
		jimport('joomla.filter.output');
		JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES );
		
		// Build the component's submenu
		jimport('joomla.html.pane');
		$pane =& JPane::getInstance('sliders');
		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.modal','a.modal-button');
		?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel_project') {
			submitform( pressbutton );
			return;
		}
		else if(pressbutton == 'save_project' || pressbutton == 'apply_project' ) {
			if(form.category_id.value == ''){
				alert('Select a category');
				return false;
			}else if( form.name.value==''){
				alert('input name');
				return false;
			}
			submitform( pressbutton );
		}else{
			submitform( pressbutton );
		}
	}
</script>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
<table class="adminform">
	<tr>
		<td class="key"><?php echo JText::_( 'Course ID' ); ?></td>
		<td><?php echo $row->id ; ?></td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Name' ); ?></span></td>
		<td>
			<input type="text" name="name" value="<?php echo $row->name;?>" size="60" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText :: _('Published'); ?></td>
		<td><?php echo JHTML::_( 'select.booleanlist', 'published', 'class="inputbox"', $row->published ); ?></td>
	</tr>
	<tr>
		<td class="key"><span class="editlinktip hasTip" title="<?php echo JText::_( 'Category' );?>::<?php echo JText::_( 'Category Page Description' ); ?>"><?php echo JText::_( 'Categories' ); ?></span></td>
		<td><?php echo $lists['categories']; ?></td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Title' ); ?></td>
		<td>
			<input type="text" name="title" value="<?php echo $row->title;?>" size="40" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Price' ); ?></td>
		<td>
			<input type="text" name="price" value="<?php echo $row->price;?>" size="" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Time' ); ?></td>
		<td>
			<input type="text" name="time" value="<?php echo $row->time;?>" size="" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'URL' ); ?></td>
		<td>
			<input type="text" name="url" value="<?php echo $row->url;?>" size="" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Thumbnail File' ); ?></td>
		<td><input name="thumbnail" type="file" /></td>
	</tr>
	<tr>
		<td class="key"><span class="editlinktip hasTip" title="<?php echo JText::_( 'Ordering' );?>::<?php echo JText::_( 'Order Page Description' ); ?>"><?php echo JText::_( 'Ordering' ); ?></span></td>
		<td><input name="ordering" type="text" class="text_area" id="ordering" value="<?php echo $row->ordering; ?>" /></td>
	</tr>
	<tr>
		<td class="key" style="text-align:left"><span class="key" style="text-align:left"><?php echo JText::_( 'Short Description' ); ?></span>:</td>
		<td><textarea name="short_des" rows="5" cols="30"><?php echo $row->short_des;?></textarea></td>
	</tr>
	<tr>
		<td class="key" style="text-align:left"><span class="key" style="text-align:left"><?php echo JText::_( 'Long Description' ); ?></span>:</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2"><?php echo $editor->display( 'description', $row->description, '60%', '300', '60', '20' ) ; ?></td>
	</tr>
	

</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="cid[]" value="<?php echo $row->id; ?>" />
</form>
		<?php
	}
}
?>
