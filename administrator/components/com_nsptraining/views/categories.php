<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class CategoryManager {
	function showCategories( &$rows, $nb, &$pageNav, $option, &$lists, &$children ) {
		$user =& JFactory::getUser();
		$ordering = ($lists['order'] == 'c.ordering');
		JHTML::_('behavior.tooltip');
		?>
<form action="index.php?option=com_nsptraining" method="post" name="adminForm">
	<table>
		<tr>
			<td align="left" width="100%">
				<?php echo JText::_( 'Filter (Category Name)' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?>
				</button>
			</td>
			<td nowrap="nowrap">
				<?php
				echo $lists['state'];
				?>
			</td>
		</tr>
	</table>
	<div id="tablecell">
		<table class="adminlist">
			<thead>
				<tr>
					<th width="5">
						<?php echo JText::_( 'NUM' ); ?></th>
					<th width="20">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th width="1%" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'ID', 'c.id', @$lists['order_Dir'], @$lists['order'], 'list_categories' ); ?></th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', 'Category Name', 'c.name', @$lists['order_Dir'], @$lists['order'], 'list_categories' ); ?>					</th>
						<th align="center">
						<?php echo JHTML::_('grid.sort', 'Description', 'c.description', @$lists['order_Dir'], @$lists['order'], 'list_categories' ); ?>					</th>
						<th align="center">
						<?php echo JHTML::_('grid.sort', 'Items', 'numoptions', @$lists['order_Dir'], @$lists['order'], 'list_categories' ); ?>					</th>
					<th align="center">
						<?php echo JHTML::_('grid.sort', 'Published', 'c.published', @$lists['order_Dir'], @$lists['order'], 'list_categories' ); ?>					</th>
					
					<th colspan="3" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort', 'Ordering', 'c.ordering', @$lists['order_Dir'], @$lists['order'], 'list_categories' ); ?><?php echo JHTML::_('grid.order', $rows, 'filesave.png', 'save_category_order' ); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="10"><?php echo $pageNav->getListFooter(); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php
		CategoryManager::recurseCategories($option, 0, 0, $children,$pageNav,0,$nb,$option,$ordering);
		?>
			</tbody>
		</table>
	</div>

	<input type="hidden" name="option" value="<?php echo $option;?>" />
	<input type="hidden" name="task" value="list_categories" />
	<input type="hidden" name="section" value="manage_categories" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="" />
</form>
<?php
	}

	function editCategory( &$row, &$lists, &$children ) {
		global $option;
		if (JRequest::getCmd('task') == 'add_category') {
			//Default values for new books
			$row->category_name = 'New Category';
			$row->description = '';
			$row->published = 1;
			$row->ordering = 0;
			
		}
		JRequest::setVar( 'hidemainmenu', 1 );
		$editor =& JFactory::getEditor();
		jimport('joomla.filter.output');
		JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES );
		jimport('joomla.html.pane');
		$pane =& JPane::getInstance('sliders');
		JHTML::_('behavior.tooltip');
?>
		
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel_category') {
			submitform( pressbutton );
			return;
		}
		// do field validation
		if (form.name.value == "") {
			alert( "<?php echo JText::_( 'Category must have a Name', true ); ?>" );
		} else {
			submitform( pressbutton );
		}
	}
</script>
<br>
<form action="index.php" method="post" name="adminForm">
<table width="100%" border="0" cellspacing="5" cellpadding="0" class="adminform">
	<tr>
		<td valign="top">
			<table class="admintable">

				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Category Name' ); ?></label></td>
					<td><input class="inputbox" type="text" name="name" id="title" size="60" value="<?php echo $row->name; ?>" />
					</td>
				</tr>
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Preview Image' ); ?></label></td>
					<td><?php echo $lists['preview_image']; ?>
					</td>
				</tr>
				<tr>
					<td class="key" valign="top"><?php echo JText::_( 'Description' ); ?></td>
					<td><?php echo $editor->display( 'description', $row->description, '500', '300', '60', '20', false ) ; ?></td>
				</tr>
				<tr>
					<td class="key"><?php echo JText :: _('Published'); ?></td>
					<td><?php echo JHTML::_( 'select.booleanlist', 'published', 'class="inputbox"', $row->published ); ?></td>
			    </tr>				
			</table>

	    <input type="hidden" name="task" value="" />
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="cid[]" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="ordering" value="<?php echo $row->ordering; ?>" />
	  </td>
  </tr>
</table>
</form>

<?php
	}
	
	function recurseCategories($option, $id, $level, $children,$pageNav,$num,$nb,$option,$ordering) {
	if (@$children[$id]) {
		$n = 0; 
		$total = count($children[$id]);
		foreach ($children[$id] as $row) {
			$link = 'index.php?option=com_nsptraining&task=edit_category&cid[]='. $row->id ;
			?>
					<td><?php echo $num+1; ?></td>
					<td align="center"><input type="checkbox" id="cb<?php echo $num;?>" name="cid[]" value="<?php echo $row->id; ?>" onclick="isChecked(this.checked);" /></td>
					<td><?php echo $row->id; ?></td>
			<?php 
				$text ="";
				for($i=1;$i<$level;$i++)
					$text .= "&nbsp;&nbsp;&nbsp;&nbsp;";
				if ($level > 0)
					$text .= "&nbsp;&nbsp;&nbsp;&nbsp;<sup>L</sup>&nbsp;";
				$text .=$row->name;
			?>
			<td>
				<a href="<?php echo JRoute::_( $link ); ?>" title="<?php echo JText::_( 'Edit Adcategory' ); ?>">
						<?php echo $text; ?></a>			
			</td>
			<td align="center"><?php echo $row->description;?></td>
			<td align="center"><?php echo $row->numoptions; ?></td>
			<td align="center"><?php $published = JHTML::_('grid.published', $row, $num ); 
			echo $published; ?></td>
			<td align="right">
			<?php
			$ordering=true;
			 echo $pageNav->orderUpIcon( $num, true,'orderup_category','move down',$ordering); ?>
			</td>
			<td align="left">
			<?php echo $pageNav->orderDownIcon( $num,true, ($n < $total -1 ),'orderdown_category','move up',$ordering); ?>
			</td>
			<td align="center" colspan="2">
			<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="text_area" style="text-align: center" />			</td>
			</tr>
			<?php
			$num++;
			$num = CategoryManager::recurseCategories($option, $row->id, $level+1, $children,$pageNav,$num,$nb,$option,$ordering);
			$n++;
			}
		}
		return $num;
	}
		function selectCategories($id, $level, $children,$catid,$nodisplaycatid,$multiple=0,$catsid="") {
		if (@$children[$id]) {
			foreach ($children[$id] as $row) {
				if ($row->id != $nodisplaycatid) {
					if ((($multiple == 0)&&($row->id != $catid))
						||
						(($multiple == 1)&&(strpos($catsid, ",$row->id,") === false)))
						echo "<option value='".$row->id."'>".$level.$row->name ."</option>";
					else
						echo "<option value='".$row->id."' selected>".$level.$row->name ."</option>";
					
					CategoryManager::selectCategories($row->id, $level.$row->name ." >> ", $children,$catid,$nodisplaycatid,$multiple,$catsid);
				}
			}
		}
	}//selectCategories
}
?>
