<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport('joomla.html.pane');
$pane =& JPane::getInstance('tabs');
/* OR
//$pane =& JPane::getInstance('sliders');
*/
	echo $pane->startPane("menu-pane");
		echo $pane->startPanel('License FAQ', "license"); 

	?>
		body of tab
	<?php
	echo $pane->endPanel();
			echo $pane->startPanel('License FAQ', "license2"); 

	?>
		body of tab
	<?php
	echo $pane->endPanel();

echo $pane->endPane();


class Config {
	function Configuration( $config, $lists ) {
	global $option;
	JHTML::_('behavior.tooltip');
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		submitform( pressbutton );
	}
</script>
<form action="index.php?option=com_nsptraining&task=configuration" method="post" name="adminForm">
	<table width="100%" class="adminform">
		<tr valign="middle">
			<td width="50%" valign="top" nowrap="nowrap">
				<fieldset class="adminform">
				<legend><?php echo JText::_( 'Contact Infomation Settings' ); ?></legend>
					<table class="admintable">
					  <tr>
						<td class="key"><?php echo JText::_('Contact Name');?></td>
						<td><input type="text" value="<?php echo $config->contact_name;?>" name="contact_name" id="" size="35" /></td>
					  </tr>
					  
					  <tr>
						<td class="key"><?php echo JText::_('Contact Phone');?></td>
						<td><input type="text" value="<?php echo $config->contact_phone;?>" name="contact_phone" id="" size="35" /></td>
					  </tr>
					  
					  <tr>
						<td class="key"><?php echo JText::_('Contact Email');?></td>
						<td><input type="text" value="<?php echo $config->contact_email;?>" name="contact_email" id="" size="65" /><br />(Ex: ex@yourdomain.com ; ex2@yourdomain.com)</td>
					  </tr>
					  
					  <tr>
						<td class="key"><?php echo JText::_('Contact CC Email');?></td>
						<td><input type="text" value="<?php echo $config->contact_cc_email;?>" name="contact_cc_email" id="" size="65" /><br />(Ex: ex@yourdomain.com ; ex2@yourdomain.com)</td>
					  </tr>

					  <tr>
						<td class="key"><?php echo JText::_('Address');?></td>
						<td><input type="text" value="<?php echo $config->contact_address;?>" name="contact_address" id="" size="35" />
						
						</td>
					  </tr>
					  
					  <tr>
						<td class="key"><?php echo JText::_('Send Registration to Email');?></td>
						<td><?php echo JHTML::_('select.booleanlist','send_reg_to_mail','class=""',$config->send_reg_to_mail);?></td>
					  </tr>
					  
					</table>
				</fieldset>
				<br/>
				<fieldset class="adminform">
					<legend><?php echo JText::_( 'Display Settings' ); ?></legend>
					<table class="admintable">
					
					  <tr>
						<td class="key"><span class="hasTip" title="Number of item per a row"><?php echo JText::_('Num of Item Per Row');?></span></td>
						<td><input type="text" value="<?php echo $config->num_item_per_row;?>" name="num_item_per_row" id="thumbnail_width" /></td>
					  </tr>

					  <tr>
						<td class="key"><?php echo JText::_('Num of Item Per Page');?></td>
						<td><input type="text" value="<?php echo $config->num_item_per_page;?>" name="num_item_per_page" id="thumbnail_height" /></td>
					  </tr>
						
					</table> 
				</fieldset></td>
		    <td width="50%" valign="top" nowrap="nowrap"><fieldset class="adminform">
					<legend><?php echo JText::_( 'Others Setting' ); ?></legend>
					
				</fieldset>
				
					</td>
		</tr>
		<tr valign="middle">
		  <td colspan="2" valign="top" nowrap="nowrap"></td>
	  </tr>
	</table>
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="" />
</form>
<?php
	}
}
?>
