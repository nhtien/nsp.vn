<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class RegcourseManager {
	function showRegcourse( &$rows, &$pageNav, $option, &$lists ) {
		$user =& JFactory::getUser();
		$ordering = ($lists['order'] == 'rc.id' || $lists['order'] == 'rc.name' || $lists['order'] == 'rc.ordering');
		//echo $lists['order'];
		JHTML::_('behavior.tooltip');
		$tmpl = JRequest::getVar('tmpl');
		$popup = ($tmpl=="component" ? true : false);
		?>
		<form action="?option=com_nsptraining" method="post" name="adminForm">

		<table width="100%">
		<tr>
			<td align="left">
				<?php echo JText::_( 'Filter (Course Name)' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button></td>
			<td align="center" nowrap="nowrap">
				</td>
			<td align="center" nowrap="nowrap">
				</td>	
			<td align="right" nowrap="nowrap"><?php echo JText::_( 'State Filter' ); ?>:
				<?php
				echo $lists['state'];
				?></td>
		</tr>
		</table>

		<div id="tablecell">
			<table class="adminlist">
			<thead>
				<tr>
					<th width="5" nowrap="nowrap"><?php echo JText::_( 'NUM' ); ?></th>
					<th width="20" nowrap="nowrap"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th width="1%" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'ID', 'p.id', @$lists['order_Dir'], @$lists['order'], 'list_regcourse' ); ?></th>
					<th nowrap="nowrap" class="title"><?php echo JHTML::_('grid.sort', 'Title', 'rc.name', @$lists['order_Dir'], @$lists['order'], 'list_regcourse' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Create By', 'author', @$lists['order_Dir'], @$lists['order'], 'list_regcourse' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Added Date', 'rc.created_date', @$lists['order_Dir'], @$lists['order'], 'list_regcourse' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Published', 'rc.published', @$lists['order_Dir'], @$lists['order'], 'list_regcourse' ); ?></th>
					<th colspan="3" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'Ordering', 'rc.ordering', @$lists['order_Dir'],      @$lists['order'], 'list_regcourse' ); ?><?php echo JHTML::_('grid.order', $rows, 'filesave.png', 'save_regcourse_order' ); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $pageNav->getListFooter(); ?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0; 
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];

				$link 		= 'index.php?option=com_nsptraining&task=edit_regcourse&cid[]='. $row->id ;

				$checked 	= JHTML::_('grid.checkedout', $row, $i );
				$published 	= JHTML::_('grid.published', $row, $i );
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td><?php echo $pageNav->getRowOffset( $i ); ?></td>
					<td><?php echo $checked; ?></td>
					<td align="center"><?php echo $row->id; ?></td>
					<td>
						<a href="<?php echo JRoute::_( $link ); ?>" title="<?php echo JText::_( 'Edit Item' ); ?>"><?php echo $row->name; ?></a>
					</td>
					<td width="1%" align="center"><?php echo $row->author;?></td>
					<td align="left"><?php echo $row->created_date;?></td>
					<td align="center"><?php echo $published;?></td>
					<td width="1%" align="center"><?php  echo $pageNav->orderUpIcon( $i, ($row->book_id == @$rows[$i-1]->book_id) , 'orderdown_regcourse', 'Move Up', $ordering); ?></td>
					<td width="1%" align="center"><?php echo $pageNav->orderDownIcon( $i, $n, ($row->book_id == @$rows[$i+1]->book_id), 'orderup_regcourse', 'Move Down', $ordering ); ?></td>
					<td width="1%" align="center" nowrap="nowrap"><?php $disabled = $ordering ? '' : 'disabled="disabled"'; ?><input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" <?php echo $disabled; ?> class="text_area" style="text-align: center" /></td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>
		</div>

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="list_regcourse" />
		<input type="hidden" name="section" value="manage_regcourse" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="" />
		</form>
		<?php
	}
	
	function editRegcourse( &$row, &$lists ) {
		global $option;
		if (JRequest::getCmd('task') == 'add_regcourse') {
			$row->published = '1';
			$row->category_id = '';
			$row->description = '';
			$row->ordering = '0';
		}
		JRequest::setVar( 'hidemainmenu', 1 );

		$editor =& JFactory::getEditor();
		
		jimport('joomla.filter.output');
		JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES );
		
		// Build the component's submenu
		jimport('joomla.html.pane');
		$pane =& JPane::getInstance('sliders');
		JHTML::_('behavior.tooltip');
		?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel_regcourse') {
			submitform( pressbutton );
			return;
		}
		else if(pressbutton == 'save_regcourse' || pressbutton == 'apply_regcourse' ) {
			if(form.name.value == ''){
				alert('Input title');
				return false;
			}
			submitform( pressbutton );
		}else{
			submitform( pressbutton );
		}
	}
	
</script>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
<table class="admintable" width="100%">
	<tr>
		<td valign="top">
<table class="adminform">
	<tr>
		<td class="key"><?php echo JText::_( 'Item ID' ); ?></td>
		<td><?php echo $row->id ; ?></td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Name' ); ?></span></td>
		<td>
			<input type="text" name="name" value="<?php echo $row->name;?>" size="40" />
		</td>
	</tr>
	
	<tr>
		<td class="key"><?php echo JText::_( 'Phone' ); ?></span></td>
		<td>
			<input type="text" name="phone" value="<?php echo $row->phone;?>" size="40" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Mobile Phone' ); ?></span></td>
		<td>
			<input type="text" name="mobile" value="<?php echo $row->mobile;?>" size="40" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Fax' ); ?></span></td>
		<td>
			<input type="text" name="fax" value="<?php echo $row->fax;?>" size="40" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Address' ); ?></span></td>
		<td>
			<input type="text" name="address" value="<?php echo $row->address;?>" size="40" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Email' ); ?></span></td>
		<td>
			<input type="text" name="mail" value="<?php echo $row->mail;?>" size="40" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Partner Type' ); ?></span></td>
		<td>
			<input type="text" name="partner_type" value="<?php echo $row->partner_type;?>" size="40" readonly="" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Location' ); ?></span></td>
		<td>
			<input type="text" name="location" value="<?php echo $row->location;?>" size="40" readonly="" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Total Attending' ); ?></span></td>
		<td>
			<input type="text" name="total_attending" value="<?php echo $row->total_attending;?>" size="40" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText :: _('Published'); ?></td>
		<td><?php echo JHTML::_( 'select.booleanlist', 'published', 'class="inputbox"', $row->published ); ?></td>
	</tr>
	<tr>
		<td class="key"><span class="editlinktip hasTip" title="<?php echo JText::_( 'Ordering' );?>::<?php echo JText::_( 'Order Description' ); ?>"><?php echo JText::_( 'Ordering' ); ?></span></td>
		<td><input name="ordering" type="text" class="text_area" id="ordering" value="<?php echo $row->ordering; ?>" /></td>
	</tr>
</table>
		</td>
		<td valign="top">
		</td>
	</tr>
</table>

		<input type="hidden" name="task" value="add_regcourse" />
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="cid[]" value="<?php echo $row->id; ?>" />
</form>
		<?php
	}
}
?>
