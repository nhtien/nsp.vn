<?php
/*
 * @package Joomla 1.5
 * @copyright Copyright (C) 2005 Open Source Matters. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 *
 * @component flippingcomic book
 * @copyright Copyright (C) Nghe Nguyen Xuan
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.filesystem.folder' );

function com_uninstall()
{
	$db			=& JFactory::getDBO();
	$db_prefix 	= $db->getPrefix();
	//$folder[1][0]	=	'images' . DS . 'flippingcomic' . DS.'pdf_files'.DS ;
	//$folder[0][1]	= 	JPATH_ROOT . DS .  $folder[0][0];
	$folder[1][0]	=	'images' . DS . 'flippingcomic' . DS ;
	$folder[1][1]	= 	JPATH_ROOT . DS .  $folder[1][0];
	$message = '';
	$error	 = array();
	foreach ($folder as $key => $value)
	{
		if (JFolder::delete( $value[1]))
		{
			$message .= '<p><b><span style="color:#009933">Folder</span> ' . $value[0] 
					   .' <span style="color:#009933">successfully deleted!</span></b></p>';
			$error[] = 0;
		}	 
		else
		{
			$message .= '<p><b><span style="color:#CC0033">Folder</span> ' . $value[0]
					   .' <span style="color:#CC0033">deletion failed!</span></b> Please delete it manually</p>';
			$error[] = 1;
		}
	}
	$message = '<p><b><span style="color:#009933">Folder</span> ' . $folder[1][0] 
					   .' <span style="color:#009933">still exists!</span></b> Please delete it manually, if you want.</p>';
					   
					   
$sql = "DROP TABLE #__flippingcomic_books ";					   
$db  -> setQuery($sql);
$db  -> query();

$sql = "DROP TABLE #__flippingcomic_category ";					   
$db  -> setQuery($sql);
$db  -> query();

$sql = "DROP TABLE #__flippingcomic_chapters ";					   
$db  -> setQuery($sql);
$db  -> query();

$sql = "DROP TABLE #__flippingcomic_pages ";					   
$db  -> setQuery($sql);
$db  -> query();

$sql = "DROP TABLE #__flippingcomic_config ";					   
$db  -> setQuery($sql);
$db  -> query();

$sql = "DROP TABLE #__flippingcomic_readers ";					   
$db  -> setQuery($sql);
$db  -> query();

$message .="<p>Delete Datebase successfull!</p>";

echo $message;
}

?>