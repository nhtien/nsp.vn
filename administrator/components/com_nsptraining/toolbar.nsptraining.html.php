<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class TOOLBAR_NSPTraining {

	function _CONFIGURATION() {
		JToolBarHelper::title(  JText::_( 'NSP Training' ) .': <small> : '.JText::_( 'Configuration' ).'</small>' );
		JToolBarHelper::save( 'save_configuration', 'Save' );
		JToolBarHelper::apply( 'apply_configuration', 'Apply' );
		JToolBarHelper::cancel( 'cancel_configuration', 'Cancel' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

/* categories */
	function _CATEGORY_MANAGER() {
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		JToolBarHelper::title(  JText::_( 'NSP Training') .': <small> : '.JText::_( 'Category Manager' ).'</small>' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList( '', 'remove_category', 'Delete' );
		JToolBarHelper::editListX( 'edit_category', 'Edit' );
		JToolBarHelper::addNewX( 'add_category', 'New' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

	function _EDIT_CATEGORY( $categoryid ) {
			$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
			JArrayHelper::toInteger($cid, array(0));
	
			$text = ( $cid[0] ? JText::_( 'Edit Category' ) : JText::_( 'New Category' ) );
	
			JToolBarHelper::title(  JText::_( 'NSP Training' ).': <small> : '.JText::_( 'Category Manager' ).' : <small>[ ' . $text.' ]</small></small>' );
			JToolBarHelper::save( 'save_category', 'Save' );
			JToolBarHelper::apply( 'apply_category', 'Apply' );
			if ($cid[0]) {
				// for existing items the button is renamed `close`
				JToolBarHelper::cancel( 'cancel_category', 'Close' );
			} else {
				JToolBarHelper::cancel( 'cancel_category', 'Cancel' );
			}
			JToolBarHelper::help( '../help.html', true );
		}
/* Course */
	function _EDIT_COURSE( $bookid ) {
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$text = ( $cid[0] ? JText::_( 'Edit Software' ) : JText::_( 'New Software' ) );

		JToolBarHelper::title(  JText::_( 'NSP Training' ).': <small> : '.JText::_( 'Courses Manager' ).' : <small>[ ' . $text.' ]</small></small>' );
		JToolBarHelper::save( 'save_course', 'Save' );
		JToolBarHelper::apply( 'apply_course', 'Apply' );
		if ($cid[0]) {
			// for existing items the button is renamed `close`
			JToolBarHelper::cancel( 'cancel_course', 'Close' );
		} else {
			JToolBarHelper::cancel( 'cancel_course', 'Cancel' );
		}
		JToolBarHelper::help( '../help.html', true );
	}
	
	function _COURSE_MANAGER() {
		JToolBarHelper::title(  JText::_( 'NSP Training') .': <small> : '.JText::_( 'Courses Manager' ).'</small>' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList( '', 'remove_course', 'Delete' );
		JToolBarHelper::editListX( 'edit_course', 'Edit' );
		JToolBarHelper::addNewX( 'add_course', 'New' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}
/* Register course */
	function _REGCOURSE_MANAGER() {
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		JToolBarHelper::title(  JText::_( 'NSP Training') .': <small> : '.JText::_( 'Register Course Manager' ).'</small>' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList( '', 'remove_regcourse', 'Delete' );
		//JToolBarHelper::editListX( 'edit_regcourse', 'Edit' );
		//JToolBarHelper::addNewX( 'add_regcourse', 'New' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

	function _EDIT_REGCOURSE( ) {
			$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
			JArrayHelper::toInteger($cid, array(0));
	
			$text = ( $cid[0] ? JText::_( 'Edit Register Course' ) : JText::_( 'New File' ) );
	
			JToolBarHelper::title(  JText::_( 'NSP Training' ).': <small> : '.JText::_( 'Register Course Manager' ).' : <small>[ ' . $text.' ]</small></small>' );
			JToolBarHelper::save( 'save_regcourse', 'Save' );
			JToolBarHelper::apply( 'apply_regcourse', 'Apply' );
			if ($cid[0]) {
				// for existing items the button is renamed `close`
				JToolBarHelper::cancel( 'cancel_regcourse', 'Close' );
			} else {
				JToolBarHelper::cancel( 'cancel_regcourse', 'Cancel' );
			}
			JToolBarHelper::help( '../help.html', true );
		}

	function _DEFAULT() {
		JToolBarHelper::title(  JText::_( 'NSP Training Component' ) );
		JToolBarHelper::help( '../help.html', true );
	}
}
?>