<?php
/***************************************************************************
*  @EcsProjec Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2009 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			October 2009
***************************************************************************/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.filesystem.folder' ); 
jimport( 'joomla.filesystem.file' );

class escProjectDownload
{	
	function download($file, $currentLink , $paramsTmpl) {
			
		global $mainframe;
		$directLink 	= $file->directlink;// Direct Link 0 or 1
		$absOrRelFile	= $file->filename;// Relative Path or Absolute Path
		// NO FILES FOUND (abs file)
		$error = false;
		$error = preg_match("/escError/i", $absOrRelFile);
		
		if ($error) {
			$msg = JText::_('Error while downloading file');
			$mainframe->redirect($currentLink, $msg);
		} else {
			
			// Get extensions
			$extension = JFile::getExt($absOrRelFile);
			
			// Get Mime from params ( ext --> mime)
			
			$allowedMimeType = escProjectDownload::getMimeType($extension, $paramsTmpl['allowed_file_types']);
			$disallowedMimeType = escProjectDownload::getMimeType($extension, $paramsTmpl['disallowed_file_types']);
			
			$allowedMimeType = true;
			
			// NO MIME FOUND
			$errorAllowed 		= false;// !!! IF YES - Disallow Downloading
			$errorDisallowed 	= false;// !!! IF YES - Allow Downloading
			
			$errorAllowed 		= preg_match("/escError/i", $allowedMimeType);
			$errorDisallowed	= preg_match("/escError/i", $disallowedMimeType);
			
			if ($errorAllowed) {
				$msg = JText::_('Error while downloading file (Mime Type not found)');
				$mainframe->redirect($currentLink, $msg);
			} else if (!$errorDisallowed) {
				$msg = JText::_('Error while downloading file (Disallowed Mime Type)');
				$mainframe->redirect($currentLink, $msg);
			
			} else {
				if ($directLink == 1) {
					
					$addHit	= escProjectDownload::_hit( $file->id ); 
					$mainframe->redirect ($absOrRelFile);
					exit;
				} else {
				
					// Clears file status cache
					clearstatcache();
					
					// HIT Statistics
					$addHit	= escProjectDownload::_hit( $file );

					// USER Statistics
				
					$fileWithoutPath	= basename($absOrRelFile);
					$fileSize 			= filesize($absOrRelFile);
					$mimeType			= '';
					$memeType			= $allowedMimeType;
					// Clean the output buffer
					ob_end_clean();
					
					header("Cache-Control: public, must-revalidate");
					header('Cache-Control: pre-check=0, post-check=0, max-age=0');
					header("Pragma: no-cache");
					header("Expires: 0"); 
					header("Content-Description: File Transfer");
					header("Expires: Sat, 30 Dec 1990 07:07:07 GMT");
					header("Content-Type: " . (string)$mimeType);
					header("Content-Length: ". (string)$fileSize);
					header('Content-Disposition: attachment; filename="'.$fileWithoutPath.'"');
					header("Content-Transfer-Encoding: binary\n");
					
					@readfile($absOrRelFile);
					exit;
				}
			}
			
		}
		return false;
	
	}
	
	function _hit( $file ) {
	
		$db = &JFactory::getDBO();
		$sql = "UPDATE #__nsptraining_software SET num_download = num_download+1 WHERE id=".$file->software_id;
		$db->setQuery( $sql );
		@$db->query(); 

		global $mainframe;
		$table = & JTable::getInstance('file', 'Table');
		$table->hit($file->id);
		return true;
	}
	
	function getMimeType($extension, $params) {
		
		$regex_one		= '/({\s*)(.*?)(})/si';
		$regex_all		= '/{\s*.*?}/si';
		$matches 		= array();
		$count_matches	= preg_match_all($regex_all,$params,$matches,PREG_OFFSET_CAPTURE | PREG_PATTERN_ORDER);

		$returnMime = '';
		
		for($i = 0; $i < $count_matches; $i++) {
			
			$escDownload	= $matches[0][$i][0];
			preg_match($regex_one,$escDownload,$escDownloadParts);
			$values_replace = array ("/^'/", "/'$/", "/^&#39;/", "/&#39;$/", "/<br \/>/");
			$values = explode("=", $escDownloadParts[2], 2);	
			
			foreach ($values_replace as $key2 => $values2) {
				$values = preg_replace($values2, '', $values);
			}

			// Return mime if extension call it
			if ($extension == $values[0]) {
				$returnMime = $values[1];
			}
		}

		if ($returnMime != '') {
			return $returnMime;
		} else {
			return "escErrorNoMimeFound";
		}
	}
	

}
?>