<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableCategory extends JTable {

	var $id				= null;
	var $name			= null;
	var $description	= null;
	var $thumbnail		= null;
	var $published		= null;
	var $ordering		= null;
	 
	function __construct( &$_db ) {
		parent::__construct( '#__nsptraining_categories', 'id', $_db );
	}

	function check() {
		return true;
	}
	
	function getList( $published = null, $limit = 500, $sort = "c.name ASC" ,$filter = array() ){
		$db = &JFactory::getDBO();
		$where = array() ;
		$where[] = "1";
		if( $published ){
			$where[] = "published=$published";
		}
		while( list( $k,$v) = each( $filter ) ){
			$where[] = $v;
		}
		$where = "WHERE ".implode(" AND " ,$where);
		
		$orderby = " ORDER BY ".$sort;
		$limit = " LIMIT 0,$limit";
		
		$sql = "SELECT * FROM #__nsptraining_categories AS c ".$where.$orderby.$limit;
		$db->setQuery( $sql ); 
		$rows = $db->loadObjectList();
		return $rows;
	}


	function getSubCategories( $id = null , $published = null ){
		if( !$id ) $id = $this->id;
		
		if( !$id ) $id = 0;
		
		$db = &JFactory::getDBO();
		
		if( $published ) $where = " AND published=$published";
		$sql = "SELECT id,parent FROM #__nsptraining_categories WHERE 1 ".$where;
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		
		$cid = array();
		foreach( $rows as $r){
			$cid[ $r->id ] = $r->parent;
		}
		$result = array(); 
		$result[] = $id;
		$this->_getParent($cid, $result);
		
		return $result;
		
	}
	
	function _getParent( $cid = array() , &$result ){
		
		while( list( $k, $v) = each( $cid ) ){
			
			while( list($k1, $v1) = each( $result ) ){
				if( $v1 == $v && !in_array($k,$result) ){ 
					$result[] = $k;
				}
			}
			reset( $result );
		}//while
	}//function
	
	function getSelectBox( $name, $attribs = null,  $selected = null, $select_id = null ){
	
		$children = $this->_getTree();
		$category_option = array();
		$category_option[] = JHTML::_('select.option','',' - Select category - ');
		$this->selectCategories(0,"Root >> ",$children,@$this->parent,@$this->id , $category_option); 
		
		$selectBox = JHTML::_( 'select.genericList', $category_option, $name,$attribs,"value", "text", $selected, $select_id);
		return $selectBox;
	
	}
	
	function _getTree( $published = null ){
		if( $published ) 
			$where = "c.published = $published";
		else
			$where = "1";
		$db = &JFactory::getDBO();
		$db->setQuery( "SELECT c.* FROM #__nsptraining_categories as c ".
						 "WHERE $where ORDER BY c.parent,c.name");
		$rows = $db->loadObjectList();
		$children = array();
		// first pass - collect children
		foreach ($rows as $v ) {
			$pt 	= $v->parent;
			$list 	= @$children[$pt] ? $children[$pt] : array();
			array_push( $list, $v );
			$children[$pt] = $list;
		} //var_dump( $children );exit;
		return $children;
	}
	
	function selectCategories($id, $level, $children,$catid,$nodisplaycatid, &$category_option = array(), $multiple=0,$catsid="" ) {
		if (@$children[$id]) {
			foreach ($children[$id] as $row) {
				if ($row->id != $nodisplaycatid) {
					if ((($multiple == 0)&&($row->id != $catid))
						||
						(($multiple == 1)&&(strpos($catsid, ",$row->id,") === false)))
						$category_option[] = JHTML::_('select.option',$row->id, $level.$row->name);
					else
						$category_option[] = JHTML::_('select.option',$row->id, $level.$row->name);
					
					$this->selectCategories($row->id, $level.$row->name ." >> ", $children,$catid,$nodisplaycatid,$category_option,$multiple,$catsid);
				}
			}
		}
	}//selectCategories
}
?>
