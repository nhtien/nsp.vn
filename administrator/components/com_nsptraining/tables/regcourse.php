<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableRegcourse extends JTable {

	var $id					= null;
	var $course_id			= null;
	var $name		 		= null;
	var $location			= null;
	var $total_attending	= null;
	var $position			= null;
	var $phone				= null;
	var $mobile 			= null;
	var $mail				= null;
	var $company			= null;
	var $partner_type		= null;
	var $address			= null;
	var $fax				= null;
	var $create_by			= null;
	var $created_date		= null;
	var $published			= null;
	var $ordering			= null;

	function __construct( &$_db ) {
		parent::__construct( '#__nsptraining_regcourse', 'id', $_db );
	}

	function check() {
		return true;
	}
}
?>