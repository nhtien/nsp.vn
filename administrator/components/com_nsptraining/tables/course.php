<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined('_JEXEC') or die();

class TableCourse extends JTable {

	var $id					= null;
	var $category_id		= null;
	var $name 				= null;
	var $title				= null;
	var $price				= null;
	var $time				= null;
	var $short_des			= null;
	var $description		= null;
	
	var $url				= null;
	
	var $preview_img		= null;
	var $created_date		= null;
	var $create_by			= null;
	var $published			= null;
	var $ordering			= null;

	function __construct( &$_db ) {
		parent::__construct( '#__nsptraining_courses', 'id', $_db );
	}

	function check() {
		return true;
	}

	function getList( $published = null, $limit = 500, $sort = "mf.name ASC" ,$filter = array() ){
		$db = &JFactory::getDBO();
		$where = array() ;
		$where[] = "1";
		if( $published ){
			$where[] = "published=$published";
		}
		while( list( $k,$v) = each( $filter ) ){
			$where[] = $v;
		}
		$where = "WHERE ".implode(" AND " ,$where);
		
		$orderby = " ORDER BY ".$sort;
		$limit = " LIMIT 0,$limit";
		
		$sql = "SELECT * FROM #__nsptraining_manufacture AS mf ".$where.$orderby.$limit;
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		return $rows;
	}
	
}
?>