<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined('_JEXEC') or die();

jimport( 'joomla.application.component.controller' );

$lang = & JFactory::getLanguage();
$lang->load('escmobile', JPATH_COMPONENT);

class NspController extends JController 
{

	function __construct( $default = array())
	{
		parent::__construct( $default );
		
		//some tasks about configuration
		$this->registerTask( 'apply_configuration', 'save_configuration' );
		
		//some tasks about categories
		$this->registerTask( 'add_category' , 'edit_category' );
		$this->registerTask( 'apply_category', 'save_category');
		$this->registerTask( 'orderup_category', 'reorder_categories' );
		$this->registerTask( 'orderdown_category', 'reorder_categories' );

		//some tasks about course
		$this->registerTask( 'add_course' , 'edit_course' );
		$this->registerTask( 'apply_course', 'save_course');
		$this->registerTask( 'orderup_course', 'reorder_course' );
		$this->registerTask( 'orderdown_course', 'reorder_course' );
		
		//some tasks about regcourse
		$this->registerTask( 'add_regcourse' , 'edit_regcourse' );
		$this->registerTask( 'apply_regcourse', 'save_regcourse');
		$this->registerTask( 'orderup_regcourse', 'reorder_regcourse' );
		$this->registerTask( 'orderdown_regcourse', 'reorder_regcourse' );
		
		//some tasks about main
		$this->registerTask( 'main', 'showMain' );
		//some tasks about publish and unpublish	
		$this->registerTask( 'publish', 'publish');
		$this->registerTask( 'unpublish', 'publish');	
		
	}
	function publish() {
		global $mainframe;

		$db 	=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid		= JRequest::getVar( 'cid', array(), '', 'array' );
		$publish	= ( $this->_task == 'publish' ? 1 : 0 );
		$option		= JRequest::getCmd( 'option', 'com_nsptraining', '', 'string' );

		JArrayHelper::toInteger($cid);

		if (count( $cid ) < 1) {
			$action = $publish ? 'publish' : 'unpublish';
			JError::raiseError(500, JText::_( 'Select an item to '.$action, true ) );
		}
		$cids = implode( ',', $cid );

		switch (JRequest::getVar( 'section', '', 'post', 'string' )) {
			case 'manage_course':
				$query = 'UPDATE #__nsptraining_courses'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN ( '. $cids .' )'
				;
				$db->setQuery( $query );
				if (!$db->query()) {
					JError::raiseError(500, $db->getErrorMsg() );
				}

				if (count( $cid ) == 1) {
					$row =& JTable::getInstance('course', 'Table');
					$row->checkin( $cid[0] );
				}
				$link = 'index.php?option=com_nsptraining&task=list_courses';
				$this->setRedirect($link);
				break;
				
			case 'manage_categories':
				$query = 'UPDATE #__nsptraining_categories'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN ( '. $cids .' )'
				;
				$db->setQuery( $query );
				if (!$db->query()) {
					JError::raiseError(500, $db->getErrorMsg() );
				}

				if (count( $cid ) == 1) {
					$row =& JTable::getInstance('category', 'Table');
					$row->checkin( $cid[0] );
				}
				$link = 'index.php?option=com_nsptraining&task=list_categories';
				$this->setRedirect($link);
				break;
				
			case 'manage_regcourse':
			$query = 'UPDATE #__nsptraining_regcourse'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN ( '. $cids .' )'
				;
				$db->setQuery( $query );
				if (!$db->query()) {
					JError::raiseError(500, $db->getErrorMsg() );
				}

				if (count( $cid ) == 1) {
					$row =& JTable::getInstance('category', 'Table');
					$row->checkin( $cid[0] );
				}
				$link = 'index.php?option=com_nsptraining&task=list_regcourse';
				$this->setRedirect($link);
				break;
		}
	}

	function showMain() {
		require_once( JPATH_COMPONENT.DS.'views'.DS.'main.php' );
		Main::showMain();
	}
	
/* some function about configuaration */
	
	function configuration() {
		global $nspTraining_config;
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'configuration.php' );
		Config::Configuration($nspTraining_config, $lists);
	}
	
	function save_configuration() {
		global $option, $task, $mainframe;
		
		$post = JRequest::get( 'post' );
		$config = &JTable::getInstance('config','Table');
		$config->save( $post );
		switch ($this->_task) {
			case 'apply_configuration':
				$msg = JText::_( 'Configuration saved' );
				$link = 'index.php?option=com_nsptraining&task=configuration';
			break;
			case 'save_configuration':
			default:
				$msg = JText::_( 'Configuration saved' );
				$link = 'index.php?option=com_nsptraining&task=main';
			break;
		}
		$mainframe->redirect($link, $msg);
	}
	
	function cancel_configuration() {
		require_once( JPATH_COMPONENT.DS.'views'.DS.'main.php' );
		Main::showMain();
	}
/* some function about categries */

	function list_categories() {
		global $mainframe, $option;

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order_category",		'filter_order',		'c.ordering',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir_category",	'filter_order_Dir',	'',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state_category",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search_category",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit_category', 'limit', 100 );
		
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'limitstart_category', 'limitstart', 0, 'int' );
		$where = array();

		if ( $filter_state )
		{
			if ( $filter_state == 'P' )
			{
				$where[] = 'c.published = 1';
			}
			else if ($filter_state == 'U' )
			{
				$where[] = 'c.published = 0';
			}
		}
		if ($search)
		{
			$where[] = 'LOWER(c.name) LIKE '.$db->Quote('%'.$search.'%');
		}

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
		
		$query = 'SELECT COUNT(c.id)'
		. ' FROM #__nsptraining_categories AS c'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult();

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT c.*, COUNT(b.id) AS numoptions'
		. ' FROM #__nsptraining_categories as c'
		. ' LEFT JOIN #__nsptraining_courses AS b ON b.category_id = c.id'
		. $where
		. ' GROUP BY c.id'
		. $orderby
		;
			
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$rows = $db->loadObjectList();
		$children = array();
		// first pass - collect children
		foreach ($rows as $v ) {
			$pt 	= $v->parent;
			$list 	= @$children[$pt] ? $children[$pt] : array();
			array_push( $list, $v );
			$children[$pt] = $list;
		}
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}

		// state filter
		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;
		

		require_once( JPATH_COMPONENT.DS.'views'.DS.'categories.php' );
		CategoryManager::showCategories( $rows, count($rows), $pageNav, $option, $lists, $children  );
	}
	function edit_category() {
		$db		=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid 	= JRequest::getVar( 'cid', array(0), '', 'array' );
		$option = JRequest::getCmd( 'option');
		$uid 	= (int) @$cid[0];

		$row =& JTable::getInstance('category', 'Table');
		$row->load( $uid );
		$database = &JFactory::getDBO();
		$database->setQuery( "SELECT c.* FROM #__nsptraining_categories as c ".
						 "WHERE 1 ORDER BY c.parent,c.ordering");
		$rows = $database->loadObjectList();
		$children = array();
		// first pass - collect children
		foreach ($rows as $v ) {
			$pt 	= $v->parent;
			$list 	= @$children[$pt] ? $children[$pt] : array();
			array_push( $list, $v );
			$children[$pt] = $list;
		}
		
		$directory = "/components/com_nsptraining/images/categories/";
		$javascript = '';
		$lists['preview_image'] = JHTML::_('list.images', 'thumbnail', $row->thumbnail, $javascript, $directory, "bmp|gif|jpg|png"  );
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'categories.php' );
		CategoryManager::editCategory( $row, $lists , $children);
	}
	function save_category() {
		global $mainframe;
		$db =& JFactory::getDBO();
		$row =& JTable::getInstance('category', 'Table');
		$post = JRequest::get( 'post' );
		$link = "index.php?option=com_nsptraining&task=edit_category&cid[]=".$post['id'];
		$post['name'] = JRequest::getVar('name', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		if( $post['name'] == ''){
			$this->setRedirect( $link , "Please Input Data!");
			return;
		}
		
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		$isNew = ($row->id == 0);
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}

		switch ($this->_task)
		{
			case 'apply_category':
				$msg = JText::_( 'Category saved' );
				$link = 'index.php?option=com_nsptraining&task=edit_category&cid[]='. $row->id .'';
				break;

			case 'save_category':
			default:
				$msg = JText::_( 'Category saved' );
				$link = 'index.php?option=com_nsptraining&task=list_categories';
				break;
		}
		$this->reorder_categories();
		$mainframe->redirect($link, $msg);
	}
	
	function remove_category() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );
		JArrayHelper::toInteger($cid);
		$strCid = implode(',',$cid);
		$db->setQuery("SELECT * FROM #__nsptraining_categories WHERE id NOT IN ($strCid) AND parent IN ($strCid)");
		if($db->loadResult()){
			echo "<script> alert('".JText::_('Please select child categories!')."'); window.history.go(-1); </script>\n";
			exit;
		}

		for ($i=0, $n=count($cid); $i < $n; $i++) {
			$category =& JTable::getInstance('category', 'Table');
			if (!$category->delete( $cid[$i] )) {
				$msg .= $category->getError();
			} else {
				$query = 'DELETE FROM #__nsptraining_courses WHERE category_id = '.(int) $cid[$i];
				$db->setQuery( $query );
				if (!$db->query()) {
					return JError::raiseWarning( 500, $db->getError() );
				}
			}
		}
		$msg .= JText::_( 'Category(s) was deleted' );
		$mainframe->redirect('index.php?option=com_nsptraining&task=list_categories', $msg);
	}

	function reorder_categories() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$this->setRedirect( 'index.php?option=com_nsptraining&task=list_categories' );

		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$task	= $this->getTask();
		$inc	= ($task == 'orderup_category' ? -1 : 1);

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		$row =& JTable::getInstance('category', 'Table');
		$row->load( (int) $cid[0] );
		$row->move( $inc );
		$row->reorder();
	}
	function save_category_order() {
		global $mainframe;
		$db			= & JFactory::getDBO();
		$cid		= JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$order		= JRequest::getVar( 'order', array (0), 'post', 'array' );
		$total		= count( $cid );
		
		JArrayHelper::toInteger($cid, array(0));
		JArrayHelper::toInteger($order, array(0));

		$row =& JTable::getInstance('category', 'Table');
		for( $i=0; $i < $total; $i++ ) {
			$row->load( (int) $cid[$i] );
			if ($row->ordering != $order[$i]) $row->ordering = $order[$i];
			$row->store();
		}
		$row->reorder();
		
		$link = 'index.php?option=com_nsptraining&task=list_categories';
		$this->setRedirect($link);
	}
	function cancel_category() {
		global $option;
		$this->setRedirect( 'index.php?option='. $option .'&task=list_categories' );
	}

	
/* some funtion about software */
	function list_courses() {
		global $mainframe, $option;

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order_courses",		'filter_order',		'cr.ordering',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir_courses",	'filter_order_Dir',	'',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state_courses",		'filter_state',		'',		'word' );
		$filter_category		= $mainframe->getUserStateFromRequest( "$option.filter_courses_cate",		'filter_category',		'-1',		'string' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search_project",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'limitstart_courses', 'limitstart', 0, 'int' );

		$where = array();

		if ( $filter_state )
		{
			if ( $filter_state == 'P' )
			{
				$where[] = 'cr.published = 1';
			}
			else if ($filter_state == 'U' )
			{
				$where[] = 'cr.published = 0';
			}
		}
		if ( (int)$filter_category >= 1) {
			$categoryTable = &JTable::getInstance('category','Table');
			$cid = $categoryTable->getSubCategories( $filter_category ); 
			$cid = implode(",",$cid);

			$where[] = 'cr.category_id IN ( ' . $cid . ')';
		}
		if ($search)
		{
			$where[] = 'LOWER(cr.name) LIKE '.$db->Quote('%'.$search.'%');
		}

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;

		$query = 'SELECT COUNT(cr.id)'
		. ' FROM #__nsptraining_courses AS cr'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult(); //var_dump( $db->getQuery());

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT cr.*,  c.id as category_id, c.name as category_name, u.name AS author'
		. ' FROM #__nsptraining_courses AS cr'
		. ' LEFT JOIN #__nsptraining_categories AS c ON cr.category_id = c.id'
		. ' LEFT JOIN #__users AS u ON u.id = cr.create_by'
		. $where
		. $orderby
		;
		; 
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$rows = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
		$category = &JTable::getInstance('category','Table');
		$lists['category']		 = $category->getSelectBox( 'filter_category','onchange="this.form.submit();"', $filter_category);

		
		// state filter
		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;

		require_once( JPATH_COMPONENT.DS.'views'.DS.'course.php' );
		Course::showCourse( $rows, $pageNav, $option, $lists );
	}
	function edit_course() {
		global $mosConfig_absolute_path;
		$db		=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid 	= JRequest::getVar( 'cid', array(0), '', 'array' );
		$id		= JRequest::getVar( 'id', $cid[0], '', 'int' );
		
		$option = JRequest::getCmd( 'option');
		$uid 	= (int) @$cid[0];

		$row =& JTable::getInstance('course', 'Table');
		$row->load( $uid );

		
		if ($row->publish_down == null ) {
			$row->publish_down =  JText::_('Never');
		} 	
		
		$category = &JTable::getInstance('category','Table');
		$lists['categories'] = $category->getSelectBox( 'category_id','',$row->category_id);
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'course.php' );
		Course::editCourse( $row, $lists );
	}

	function save_course() 
	{
		global $mainframe, $nspTraining_config, $option; 
		$db =& JFactory::getDBO();
		$user = &JFactory::getUser();
		$row =& JTable::getInstance('course', 'Table');
		$post = JRequest::get( 'post' );
		$link = 'index.php?option=com_nsptraining&task=edit_course&cid[]='. $post['id'] .'';
		$post['description']= JRequest::getVar('description', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		$post['name'] = JRequest::getVar('name', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		
		/* upload thumbnail */		
		jimport("joomla.filesystem.file");
		$thumbnail = JRequest::getVar('thumbnail','','files','array');
		$filename =  $thumbnail['name'];
		$ext	= strtolower(JFile::getExt( $filename ) );
		$fillter = array("png","gif","jpg");
		if( $filename && !in_array($ext, $fillter) ){
			$mainframe->redirect( $link, JText::_('File is not support') );
			return;
		}else if( $filename ){ 
			$dir_file = JPATH_SITE .DS."components".DS. $option .DS. $nspTraining_config->path_thumb; 
			$tmp_dest 	 = $dir_file.$filename;
			$tmp_src 	 = $thumbnail['tmp_name'];
			if(file_exists($tmp_dest)){
				$name = JFile::stripExt( $filename );
				$filename = uniqid(substr( $name,0,20)."_" ).".".$ext;
				$tmp_dest 	= $dir_file.$filename;
			}
			JFile::upload($tmp_src, $tmp_dest);
			$post['preview_img'] = $filename;
			/* remove old thumbnail file */
			$row->load( $post['id']); 
			if( $row->id && $row->preview_img){
				$oldFile = $dir_file.$row->preview_img;
				@unlink( $oldFile );
			}
		}
		/* end upload */
		
		if( $post['name'] == '' || $post['category_id'] == ''){
			$this->setRedirect( $link , "Please Input Data!");
			return;
		}
		if( !$post['id'] ){
			$post['created_date'] = date("Y-m-d H:i:s");
			$post['create_by'] = $user->id;
		}
		
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		$isNew = ($row->id == 0);
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}

		switch ($this->_task)
		{
			case 'apply_course':
				$msg = JText::_( 'Course saved' );
				$link = 'index.php?option=com_nsptraining&task=edit_course&cid[]='. $row->id .'';
				break;

			case 'save_course':
			default:
				$msg = JText::_( 'Course saved' );
				$link = 'index.php?option=com_nsptraining&task=list_courses';
				$this->reorder_course();
				break;
		}
		
		$mainframe->redirect($link, $msg);
	}

	function reorder_course() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$this->setRedirect( 'index.php?option=com_nsptraining&task=list_courses' );

		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$task	= $this->getTask();
		$inc	= ($task == 'orderup_course' ? -1 : 1);

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		$row =& JTable::getInstance('course', 'Table');
		$row->load( (int) $cid[0] );
		$row->move( $inc );
		$row->reorder();
	}
	
	function save_course_order() {
		global $mainframe;
		$db			= & JFactory::getDBO();
		$cid		= JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$order		= JRequest::getVar( 'order', array (0), 'post', 'array' );
		$total		= count( $cid );
		
		JArrayHelper::toInteger($cid, array(0));
		JArrayHelper::toInteger($order, array(0));

		$row =& JTable::getInstance('course', 'Table');
		for( $i=0; $i < $total; $i++ ) {
			$row->load( (int) $cid[$i] );
			if ($row->ordering != $order[$i]) $row->ordering = $order[$i];
			$row->store();
		}
		$row->reorder();
		
		$link = 'index.php?option=com_nsptraining&task=list_courses';
		$this->setRedirect($link);
	}
	
	function remove_course() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );
		JArrayHelper::toInteger($cid);
		for ($i=0, $n=count($cid); $i < $n; $i++) {
			$row =& JTable::getInstance('course', 'Table');
			$row->load( $cid[$i] );
			$row->delete();
		}
		$msg .= JText::_( 'Course(s) was deleted' );
		$mainframe->redirect('index.php?option=com_nsptraining&task=list_courses', $msg);
	}

	function cancel_course() {
		global $option;
		$this->setRedirect( 'index.php?option='. $option .'&task=list_courses' );
	}
/* register course */
	function list_regcourse() {
		global $mainframe, $option;

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order_regcourse",		'filter_order',		'rc.ordering',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir_regcourse",	'filter_order_Dir',	'',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state_regcourse",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search_regcourse",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit_regcourse', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'limitstart_regcourse', 'limitstart', 0, 'int' );

		$where = array();

		if ( $filter_state )
		{
			if ( $filter_state == 'P' )
			{
				$where[] = 'rc.published = 1';
			}
			else if ($filter_state == 'U' )
			{
				$where[] = 'rc.published = 0';
			}
		}
		if ($search)
		{
			$where[] = 'LOWER(rc.name) LIKE '.$db->Quote('%'.$search.'%');
		}

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;

		$query = 'SELECT COUNT(rc.id)'
		. ' FROM #__nsptraining_regcourse AS rc'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult(); 

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT rc.*, u.name AS author'
		. ' FROM #__nsptraining_regcourse AS rc'
		//. ' INNER JOIN #__nsptraining_categories AS c ON p.category_id = c.id'
		. ' LEFT JOIN #__users AS u ON u.id = rc.create_by'
		. $where
		. ' GROUP BY rc.id'
		. $orderby
		;
		; 
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit ); 
		$rows = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
	
		// state filter
		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;

		require_once( JPATH_COMPONENT.DS.'views'.DS.'regcourse.php' );
		RegcourseManager::showRegcourse( $rows, $pageNav, $option, $lists );
	}
	function edit_regcourse() {
		global $mosConfig_absolute_path;
		$db		=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid 	= JRequest::getVar( 'cid', array(0), '', 'array' );
		$id		= JRequest::getVar( 'id', $cid[0], '', 'int' );
		
		$option = JRequest::getCmd( 'option');
		$uid 	= (int) @$cid[0];

		$row =& JTable::getInstance('regcourse', 'Table');
		$row->load( $uid );

		
		if ($row->publish_down == null ) {
			$row->publish_down =  JText::_('Never');
		} 	
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'regcourse.php' );
		regcourseManager::editRegcourse( $row, $lists );
	}

	function save_regcourse() {
		global $mainframe, $nspTraining_config, $option;
		$db =& JFactory::getDBO();
		$user = &JFactory::getUser();
		$row =& JTable::getInstance('regcourse', 'Table');
		$post = JRequest::get( 'post' );
		$link = 'index.php?option=com_nsptraining&task=edit_regcourse&cid[]='. $post['id'] .'';
		
		
		$post['name'] = JRequest::getVar('name', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		
		if( $post['name'] == '' ){
			$this->setRedirect( $link , "Please Input Data!");
			return;
		}
		if( !$post['id']){
			$post['created_date'] = date("Y-m-d H:i:s");
			$post['create_by'] = $user->id;
		}
		
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		$isNew = ($row->id == 0);
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}

		switch ($this->_task)
		{
			case 'apply_regcourse':
				$msg = JText::_( 'Item saved' );
				$link = 'index.php?option=com_nsptraining&task=edit_regcourse&cid[]='. $row->id .'';
				break;

			case 'save_regcourse':
			default:
				$msg = JText::_( 'Item saved' );
				$link = 'index.php?option=com_nsptraining&task=list_regcourse';
				break;
		}
		$this->reorder_regcourse();
		$mainframe->redirect($link, $msg);
	}

	function reorder_regcourse() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$this->setRedirect( 'index.php?option=com_nsptraining&task=list_regcourse' );

		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$task	= $this->getTask();
		$inc	= ($task == 'orderup_platform' ? -1 : 1);

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		$row =& JTable::getInstance('regcourse', 'Table');
		$row->load( (int) $cid[0] );
		$row->move( $inc );
		$row->reorder();
	}
	
	function save_regcourse_order() {
		global $mainframe;
		$db			= & JFactory::getDBO();
		$cid		= JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$order		= JRequest::getVar( 'order', array (0), 'post', 'array' );
		$total		= count( $cid );
		
		JArrayHelper::toInteger($cid, array(0));
		JArrayHelper::toInteger($order, array(0));

		$row =& JTable::getInstance('regcourse', 'Table');
		for( $i=0; $i < $total; $i++ ) {
			$row->load( (int) $cid[$i] );
			if ($row->ordering != $order[$i]) $row->ordering = $order[$i];
			$row->store();
		}
		$row->reorder();
		
		$link = 'index.php?option=com_nsptraining&task=list_regcourse';
		$this->setRedirect($link);
	}
	
	function remove_regcourse() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );
		JArrayHelper::toInteger($cid);
		for ($i=0, $n=count($cid); $i < $n; $i++) {
			$row =& JTable::getInstance('regcourse', 'Table');
			$row->load( $cid[$i] );
			$row->delete();
		}
		$msg .= JText::_( 'Item(s) was deleted' );
		$mainframe->redirect('index.php?option=com_nsptraining&task=list_regcourse', $msg);
	}

	function cancel_regcourse() {
		global $option;
		$this->setRedirect( 'index.php?option='. $option .'&task=list_regcourse' );
	}
	

}

?>