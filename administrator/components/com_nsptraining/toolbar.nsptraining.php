<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( JApplicationHelper::getPath( 'toolbar_html' ) );

switch ($task) {
	case 'configuration':
		TOOLBAR_NSPTraining::_CONFIGURATION();
		break;
/* categories */
	case 'list_categories':
		TOOLBAR_NSPTraining::_CATEGORY_MANAGER();
		break;	
		
	case 'edit_category':
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		TOOLBAR_NSPTraining::_EDIT_CATEGORY( $cid[0] );
		break;	
	case 'add_category'  :
		$id = JRequest::getVar( 'id', 0, '', 'int' );
		TOOLBAR_NSPTraining::_EDIT_CATEGORY( $id );
		break;
/* course */
	case 'list_courses':
		TOOLBAR_NSPTraining::_COURSE_MANAGER();
		break;
	case 'edit_course':
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		TOOLBAR_NSPTraining::_EDIT_COURSE( $cid[0] );
		break;
	case 'add_course'  :
		$id = JRequest::getVar( 'id', 0, '', 'int' );
		TOOLBAR_NSPTraining::_EDIT_COURSE( $id );
		break;
/* register course */
	case 'list_regcourse':
		TOOLBAR_NSPTraining::_REGCOURSE_MANAGER();
		break;	
		
	case 'edit_regcourse':
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		TOOLBAR_NSPTraining::_EDIT_REGCOURSE( $cid[0] );
		break;	
	case 'add_regcourse'  :
		$id = JRequest::getVar( 'id', 0, '', 'int' );
		TOOLBAR_NSPTraining::_EDIT_REGCOURSE( $id );
		break;

	default:
		TOOLBAR_NSPTraining::_DEFAULT();
		break;
}
?>