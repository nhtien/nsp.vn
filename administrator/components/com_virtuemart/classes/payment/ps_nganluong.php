<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
/**
*
* @version $Id: ps_paypal.php 1095 2007-12-19 20:19:16Z soeren_nb $
* @package VirtueMart
* @subpackage payment
* @copyright Copyright (C) 2004-2007 soeren - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/

/**
* This class implements the configuration panel for nganluong
* @copyright https://www.nganluong.vn
* 
*/
class ps_nganluong {

    var $classname = "ps_nganluong";
    var $payment_code = "NGANLUONG";
    
    /**
    * Show all configuration parameters for this payment method
    * @returns boolean False when the Payment method has no configration
    */
    function show_configuration() {
        global $VM_LANG;
        #var_dump($VM_LANG);die();
        $db = new ps_DB();
        
        /** Read current Configuration ***/
        #var_dump(CLASSPATH ."payment/".$this->classname.".cfg.php");die();
        include_once(CLASSPATH ."payment/".$this->classname.".cfg.php");
    ?>
    <table class="adminform" width="auto">
    	<!-- Email đăng ký tài khoản tại NGANLUONG.vn-->
       	<tr class="row0">
        <td><strong><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NGANLUONG_EMAIL') ?></strong></td>
            <td>
                <input type="text" name="NGANLUONG_EMAIL" class="inputbox" value="<?php  echo NGANLUONG_EMAIL ?>" />            </td>
            <td><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NGANLUONG_EMAIL_EXPLAIN') ?>            </td>
        </tr>
        
        <!-- Mã merchant -->
        <tr class="row1">
        <td><strong><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NGANLUONG_MERCHANT_SITE_CODE') ?></strong></td>
            <td>
                <input type="text" name="NGANLUONG_MERCHANT_SITE_CODE" class="inputbox" value="<?php  echo NGANLUONG_MERCHANT_SITE_CODE ?>" />            </td>
            <td><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NGANLUONG_MERCHANT_SITE_CODE_EXPLAIN') ?>            </td>
        </tr>
        
        <!-- Mật khẩu API -->
        <tr class="row0">
        <td><strong><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NGANLUONG_SECURE_PASS') ?></strong></td>
            <td>
                <input type="password" name="NGANLUONG_SECURE_PASS" class="inputbox" value="<?php  echo NGANLUONG_SECURE_PASS ?>" />            </td>
            <td><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NGANLUONG_SECURE_PASS_EXPLAIN') ?></td>
       </tr> 
             
       <!-- Địa chỉ trang checkout của NgânLượng.vn -->
       <tr class="row1">
       <td><strong><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NGANLUONG_CHECKOUT_URL') ?></strong></td>
       		<td>
            	<input type="text" name="NGANLUONG_CHECKOUT_URL" class="inputbox" value="<?php  echo NGANLUONG_CHECKOUT_URL ?>" size="45" />          </td>
            <td><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NGANLUONG_CHECKOUT_URL_EXPLAIN') ?></td>
       </tr> 
            
       <!-- Địa chỉ trả về sau khi thanh toán thành công tại NgânLượng.vn -->
       <tr class="row0">
       <td><strong><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NGANLUONG_RETURN_URL') ?></strong></td>
       		<td>
            	<input type="text" name="NGANLUONG_RETURN_URL" class="inputbox" value="<?php  echo NGANLUONG_RETURN_URL ?>" size="45" />          </td>
            <td><?php echo $VM_LANG->_('PHPSHOP_ADMIN_CFG_NGANLUONG_RETURN_URL_EXPLAIN') ?></td>
       </tr> 
            
        
      </table>
<?php
    }
    
    function has_configuration() {
      // return false if there's no configuration
      return true;
   }
   
  /**
	* Returns the "is_writeable" status of the configuration file
	* @param void
	* @returns boolean True when the configuration file is writeable, false when not
	*/
   function configfile_writeable() {
      return is_writeable( CLASSPATH."payment/".$this->classname.".cfg.php" );
   }
   
  /**
	* Returns the "is_readable" status of the configuration file
	* @param void
	* @returns boolean True when the configuration file is writeable, false when not
	*/
   function configfile_readable() {
      return is_readable( CLASSPATH."payment/".$this->classname.".cfg.php" );
   }
   
  /**
	* Writes the configuration file for this payment method
	* @param array An array of objects
	* @returns boolean True when writing was successful
	*/
   function write_configuration( &$d ) {
   	  $d['NGANLUONG_PAYMENT_METHOD_ID'] = intval( vmGet( $_REQUEST, "payment_method_id" ));
   
   	  $my_config_array = array(
                              "NGANLUONG_PAYMENT_METHOD_ID" => $d['NGANLUONG_PAYMENT_METHOD_ID'],
                              "NGANLUONG_EMAIL" => $d['NGANLUONG_EMAIL'],
                              "NGANLUONG_MERCHANT_SITE_CODE" => $d['NGANLUONG_MERCHANT_SITE_CODE'],
                              "NGANLUONG_SECURE_PASS" => $d['NGANLUONG_SECURE_PASS'],
                              "NGANLUONG_CHECKOUT_URL" => $d['NGANLUONG_CHECKOUT_URL'],
                              "NGANLUONG_RETURN_URL" => $d['NGANLUONG_RETURN_URL'],
                            );
      $config = "<?php\n";
      $config .= "if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); \n\n";
      foreach( $my_config_array as $key => $value ) {
        $config .= "define ('$key', '$value');\n";
      }
      
      $config .= "?>";
  
      if ($fp = fopen(CLASSPATH ."payment/".$this->classname.".cfg.php", "w")) {
          fputs($fp, $config, strlen($config));
          fclose ($fp);
          return true;
     }
     else
        return false;
   }
   
  /**************************************************************************
  ** name: process_payment()
  ** returns: 
  ***************************************************************************/
   function process_payment($order_number, $order_total, &$d) {
        return true;
    }
   
}
