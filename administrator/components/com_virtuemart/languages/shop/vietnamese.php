<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
*
* @package VirtueMart
* @subpackage languages
* @copyright Copyright (C) 2004-2008 soeren - All rights reserved.
* @translator soeren
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
global $VM_LANG;
$langvars = array (
	'CHARSET' => 'UTF-8',
	'PHPSHOP_BROWSE_LBL' => 'Browse',
	'PHPSHOP_FLYPAGE_LBL' => 'Xem chi tiết',
	'PHPSHOP_PRODUCT_FORM_EDIT_PRODUCT' => 'Sửa sản phẩm này',
	'PHPSHOP_DOWNLOADS_START' => 'Bắt đầu tải về',
	'PHPSHOP_DOWNLOADS_INFO' => 'Vui lòng nhập vào mã tải về từ email của bạn và click vào nút \'Bắt đầu tải về\'.',
	'PHPSHOP_WAITING_LIST_MESSAGE' => 'Please enter your e-mail address below to be notified when this product comes back in stock. 
                                        We will not share, rent, sell or use this e-mail address cho any other purpose other than to 
                                        tell you when the product is back in stock.<br /><br />Thank you!',
	'PHPSHOP_WAITING_LIST_THANKS' => 'Thanks for waiting! <br />We will let you know as soon as we get our inventory.',
	'PHPSHOP_WAITING_LIST_NOTIFY_ME' => 'Notify Me!',
	'PHPSHOP_SEARCH_ALL_CATEGORIES' => 'Tìm kiếm tất cả chủ đề',
	'PHPSHOP_SEARCH_ALL_PRODINFO' => 'Tìm kiếm tất cả thông tin',
	'PHPSHOP_SEARCH_PRODNAME' => 'Tên Sản phẩm',
	'PHPSHOP_SEARCH_MANU_VENDOR' => 'Nhà sản xuất/Người bán hàng',
	'PHPSHOP_SEARCH_DESCRIPTION' => 'Mô tả sản phẩm',
	'PHPSHOP_SEARCH_AND' => 'AND',
	'PHPSHOP_SEARCH_NOT' => 'NOT',
	'PHPSHOP_SEARCH_TEXT1' => 'Dùng hộp chọn cho phép bạn chọn nhóm sản phẩm để giới hạn kết quả tìm. hộp chọn thứ hai cho phép bạn tìm các thông tin như tên hay mô tả sản phẩm,... ',
	'PHPSHOP_SEARCH_TEXT2' => ' Dùng toán tử AND hoặc NOT để lọc lấy kết quả chính xác hơn. Dùng AND thì cả hai từ khóa sẽ xuất hiện trong kết quả, dùng NOT thì từ khóa thứ nhất sẽ xuất hiện trong kết quả, từ khóa thứ 2 sẽ không xuất hiện. ',
	'PHPSHOP_CONTINUE_SHOPPING' => 'Tiếp tục mua hàng',
	'PHPSHOP_AVAILABLE_IMAGES' => 'Available Images cho',
	'PHPSHOP_BACK_TO_DETAILS' => 'Quay lại trang chi tiết sản phẩm',
	'PHPSHOP_IMAGE_NOT_FOUND' => 'Ảnh không tìm thấy!',
	'PHPSHOP_PARAMETER_SEARCH_TEXT1' => 'Bạn muốn tìm kiếm sản phẩm theo các thông số kỹ thuật?<br />Bạn có thể sử dụng mẫu đã được chuẩn bị:',
	'PHPSHOP_PARAMETER_SEARCH_NO_PRODUCT_TYPE' => 'Chúng tôi xin lỗi. Không tìm thấy bất kì chủ đề nào.',
	'PHPSHOP_PARAMETER_SEARCH_BAD_PRODUCT_TYPE' => 'Xin lỗi. Không có bất kỳ sản phẩm nào theo tên này.',
	'PHPSHOP_PARAMETER_SEARCH_IS_LIKE' => 'Is Like',
	'PHPSHOP_PARAMETER_SEARCH_IS_NOT_LIKE' => 'Is NOT Like',
	'PHPSHOP_PARAMETER_SEARCH_FULLTEXT' => 'Tìm kiếm đầy đủ',
	'PHPSHOP_PARAMETER_SEARCH_FIND_IN_SET_ALL' => 'Chọn tất cả',
	'PHPSHOP_PARAMETER_SEARCH_FIND_IN_SET_ANY' => 'Chọn bất kì',
	'PHPSHOP_PARAMETER_SEARCH_RESET_FORM' => 'Reset Form',
	'PHPSHOP_PRODUCT_NOT_FOUND' => 'Xin lỗi, những sản phẩm mà bạn yêu cầu không được tìm thấy!',
	'PHPSHOP_PRODUCT_PACKAGING1' => 'Number {unit}s in packaging:',
	'PHPSHOP_PRODUCT_PACKAGING2' => 'Number {unit}s in box:',
	'PHPSHOP_CART_PRICE_PER_UNIT' => 'Giá',
	'VM_PRODUCT_ENQUIRY_LBL' => 'Gửi câu hỏi về sản phẩm này',
	'VM_RECOMMEND_FORM_LBL' => 'Giới thiệu sản phẩm này đến bạn bè',
	'PHPSHOP_EMPTY_YOUR_CART' => 'Giỏ hàng của bạn rổng',
	'VM_RETURN_TO_PRODUCT' => 'Quay lại trang sản phẩm',
	'EMPTY_CATEGORY' => 'Không có sản phẩm nào trong chủ đề này.',
	'ENQUIRY' => 'Enquiry',
	'NAME_PROMPT' => 'Tên của bạn',
	'EMAIL_PROMPT' => 'Địa chỉ E-mail',
	'MESSAGE_PROMPT' => 'Nội dung tin nhắn',
	'SEND_BUTTON' => ' Gửi ',
	'THANK_MESSAGE' => 'Cám ơn vì đã gửi yêu cầu, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất',
	'PROMPT_CLOSE' => 'Close',
	'VM_RECOVER_CART_REPLACE' => 'Replace Cart with Saved Cart',
	'VM_RECOVER_CART_MERGE' => 'Add Saved Cart to Current Cart',
	'VM_RECOVER_CART_DELETE' => 'Delete Saved Cart',
	'VM_EMPTY_YOUR_CART_TIP' => 'Clear the cart of all contents',
	'VM_SAVED_CART_TITLE' => 'Saved Cart',
	'VM_SAVED_CART_RETURN' => 'Return'
); $VM_LANG->initModule( 'shop', $langvars );
?>