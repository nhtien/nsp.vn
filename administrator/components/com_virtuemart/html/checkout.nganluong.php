<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

mm_showMyFileName( __FILE__ );
require_once( CLASSPATH . 'NL_checkout.php');
require_once( CLASSPATH."payment/ps_nganluong.cfg.php");

#Kiểm tra xem là lấy thông tin trả về hay gửi thông tin đi
if(isset($_GET['secure_code']) && !empty($_GET['secure_code'])) #Nhận thông tin
{
	#Thông tin giao dịch
	$transaction_info = @$_GET['transaction_info'];
	#Mã sản phẩm, mã đơn hàng, giỏ hàng giao dịch
	$order_code = @$_GET['order_code'];
	#Tổng số tiền thanh toán
	$price = @$_GET['price'];
	#Mã giao dịch thanh toán tại nganluong.vn
	$payment_id = @$_GET['payment_id'];
	#Loại giao dịch tại nganluong.vn (1=thanh toán ngay, 2=thanh toán tạm giữ)
	$payment_type = @$_GET['payment_type'];
	#Thông tin chi tiết về lỗi trong quá trình thanh toán
	$error_text = @$_GET['error_text'];
	#Lấy mã kiểm tra tính hợp lệ của đầu vào
	$secure_code = @$_GET['secure_code'];
	#Xử lý đầu vào
	$nl = new NL_Checkout();
	$check = $nl->verifyPaymentUrl($transaction_info, $order_code, $price, $payment_id, $payment_type, $error_text, $secure_code);
	if($check === false)
	{
		#Tham số gửi về không hợp lệ, có sự can thiệp từ bên ngoài
		echo 'Kết quả thanh toán không hợp lệ';
	}
	else
	{
		if($error_text != '')
		{
			#Có lỗi trong quá trình thanh toán
			echo 'Có lỗi: '.$error_text.'! Hãy thực hiện lại!';
		}
		else
		{
			#Thanh toán thành công
			echo 'Thanh toán thành công!';
		}
	}
}
else #Gửi thông tin
{
	#Thông tin giao dịch
	$transaction_info = @$_POST['customer_note'];
	#Mã sản phẩm, mã đơn hàng, mã giỏ hàng
	$order_code = 'DH:'.date('d-m-Y',time());
	#Tổng số tiền thanh toán
	$price = @$_POST['price'];
	#Thông tin giao dịch đến nganluong.vn
	$nl = new NL_Checkout(NGANLUONG_CHECKOUT_URL,NGANLUONG_MERCHANT_SITE_CODE,NGANLUONG_SECURE_PASS);
	$url = $nl->buildCheckoutUrl(NGANLUONG_RETURN_URL, NGANLUONG_EMAIL, $transaction_info, $order_code, $price);	
	vmRedirect( $url );
	exit();
}
?>