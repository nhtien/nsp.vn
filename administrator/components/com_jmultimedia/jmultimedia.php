<?php
/**
 * Media Controller for JMultimedia Component
 * 
 * @package  			Joomla
 * @subpackage 	JMultimedia Suite
 * @link 				http://3den.org
 * @license		GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Submenu
/*TODO add uniComments
$link = 'index.php?option=com_unicomments&section=com_jmultimedia';
JSubMenuHelper::addEntry(JText::_('Comments'), $link);
*/

//  Import denVideo or Error
$denpath = JPATH_SITE.DS.'plugins'.DS.'content'.DS.'denvideo.php';
if( !is_file($denpath) ){
	$msg = JText::_( 'denVideo is not installed' );
	$mainframe->enqueueMessage( $msg, 'error' ); 
	return;
}
require_once( $denpath );

// Define paths
define('URI_ASSETS', 
	JURI::base().'../components/com_jmultimedia/assets/' );
define('PATH_HELPERS', 
	JPATH_COMPONENT_SITE.DS.'helpers'.DS );
	
// Imports
require_once( JPATH_COMPONENT.DS.'controller.php' );

// Create the controller
$controller    = new JMultimediaController();

// Perform the Request task
$controller->execute( JRequest::getVar('task') );
$controller->redirect();
