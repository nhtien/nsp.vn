<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableJob extends JTable {

	var $id						= null;
	var	$job_title				= null;
	var $number					= null; // so luong
	var $job_short_desc			= null;
	var $job_description		= null;
	var $job_skills				= null; // ky nang yeu cau
	var $location				= null; // noi lam viec
	var $profile				= null; // yeu cau ho so
	var $experience				= null; // kinh nghiem lam viec
	var $mini_edu_level			= null; // bang cap toi  thieu
	var $gender					= null; // yeu cau gioi tinh
	var $job_type				= null; // loai hinh lam viec / hinh thuc lam viec
	var $salary					= null; // muc luong
	var $regimes				= null; // che do
	var $probationary			= null; // thoi gian thu viec
	var $end_date				= null;
	
	var $mail_admin				= null; // Email nhan thong bao khi co ung vien nop hs
	var $published				= null;
	var $post_date				= null;
	var $author					= null; // nguoi tao
	var $ordering				= null;
	
  	function __construct( &$_db ) {
		parent::__construct( '#__nsp_job', 'id', $_db );
	}

	function check() {
		return true;
	}
	
}
?>
