<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableWebComments extends JTable {

	var $id						= null;
	var	$name					= null;
	var $email					= null; 
	var $message				= null;
	var $date					= null;
	
  	function __construct( &$_db ) {
		parent::__construct( '#__nsp_webcomments', 'id', $_db );
	}

	function check() {
		return true;
	}
	
}
?>
