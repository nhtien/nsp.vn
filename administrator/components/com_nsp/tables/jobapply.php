<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableJobApply extends JTable {

	var $id					= null;
	var $job_id				= null;
	var	$position			= null;
	var $expected_salary	= null;
	
	/* private infomations */
	var $name				= null;
	var $gender				= null;
	var $birthday_day		= null;
	var $birthday_month		= null;
	var $birthday_year		= null;
	var $birth_place		= null;
	var $email				= null;
	var $address			= null; // D/C thuong tru
	var $address2			= null; // D/C lien lac
	var $phone				= null;
	var $mobilephone		= null;
	var $avatar				= null;
	var $picture			= null;
	var $attachment			= null;
	
	/* learning */
	var $learning_time			= null; // tg dao tao
	var $learing_names			= null; // truong/don vi dao tao
	var $learning_subjects		= null; // chuyen nganh
	var $learning_qualifications = null; // bang cap
	
	/* training */
	var $training_time			= null;
	var $training_names			= null;
	var $training_subjects		= null;
	var $training_qualifications= null;
	
	/* foreign language */
	var $foreign_lang			= null;
	var $foreign_lang_level		= null;
	var $foreign_lang_school	= null;
	var $foreign_skills			= null; //listen - talk - read - write
	
	/* other skills */
	var $computure_software		= null; // su dung cac phan may tinh su dung duoc
	var $office_machine			= null; // cac may moc van phong su dung duoc
	var $other_skills			= null;
	
	/* experience work */
	var $exp_time				= null; // thoi gian
	var $exp_company_name		= null; // ten don vi
	var $exp_activity			= null; // nganh hoat dong
	var $exp_company_type		= null; // loai hinh hoat dong
	
	var $exp_phone_fax			= null;
	var $exp_position			= null; // chuc danh
	var $exp_description		= null; // mo ta cong viec
	var $exp_time_type			= null; // loai hinh
	var $exp_boss				= null; // cap tren truc tiep
	var $exp_loan				= null; // thu nhap
	var $exp_discontinue		= null; // l� do thoi viec
	
	/* NSP reference */
	var $reference				= null;
	var $ref_name				= null;
	var $ref_position			= null;
	var $ref_part				= null;
	
	/* other reference */
	var $reference1				= null;
	var $ref_name1				= null;
	var $ref_position1			= null;
	var $ref_part1				= null;
	
	/* other reference 1-2 */
	var $reference1_detail		= null;
	var $reference2_detail		= null;
	
	var $strength				= null;
	var $weakness				= null;
	
	var $publish				= null;
	var $post_date				= null;
	var $is_read				= null;
	var $read_by				= null;
	
  	function __construct( &$_db ) {
		parent::__construct( '#__nsp_jobs', 'id', $_db );
	}
	function delete(){
		jimport('joomla.filesystem.file');
		global $nsp_config;
		if( $this->avatar){
			$avatar = $nsp_config->path_avatar.DS.$this->avatar;
			JFile::delete($avatar);
		}
		if( $this->picture ){
			$picture  = $nsp_config->path_piture.DS.$this->picture ;
			JFile::delete($picture);
		}
		
		if( $this->attachment ){
			$attachment  = $nsp_config->path_attachment.DS.$this->attachment ;
			JFile::delete($attachment);
		}
		
		parent::delete();
	}

	function check() {
		return true;
	}
	function checkout(){
		$user = &JFactory::getUser();
		$db = &JFactory::getDBO();
		$sql = "UPDATE #__nsp_jobs SET is_read = 1, read_by = $user->id WHERE id = ". $this->id;
		$db->setQuery( $sql ); 
		$db->query();
	}
	
	function put_data_to_property( $array, $pname){
		$arr = array();
		if( is_array( $array) ){
			while( list($k,$v) = each( $array)){
				$text = str_replace("\n","",$v);
				$arr[] = $text;
			}
		}
		$this->$pname = implode("\n",$arr);
		return $this->$pname;
	}
	
	function get_data_to_obj( $str){
		$array = explode("\n",$str);
		return $array;
	}
	
	function binData( $row ){ 
		$obj = new StdClass();
		$obj = $row;
		include(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_nsp'.DS.'common.php');

		$arr_input_data = array(
				'learning_time','learing_names','learning_subjects','learning_qualifications',
				'training_time','training_names','training_subjects','training_qualifications',
				'foreign_lang','foreign_lang_level','foreign_lang_school', 'foreign_skills',
				'exp_time','exp_company_name','exp_activity','exp_phone_fax','exp_company_type','exp_position','exp_description','exp_time_type','exp_boss','exp_total_staff','exp_loan','exp_discontinue',
				'reference','ref_name','ref_position','ref_part',
				'reference1','ref_name1','ref_position1','ref_part1'
			
		);
		while(list($k,$v)=each($arr_input_data)){
			$obj->$v = TableJobApply::get_data_to_obj($row->$v);
		}
		
		$obj->birthday = $row->birthday_day.'/'.$row->birthday_month.'/'.$row->birthday_year;
		$obj->str_gender = $row->gender == 1 ? 'Nam' : 'N&#7919;';
		/* learning */
		$learning = array(); 
		for($i = 0 ; $i < count($obj->learning_time) ; $i++ ){
			$o = new StdClass();
			$o->learning_time 			= $obj->learning_time[$i];
			$o->learing_names 			= $obj->learing_names[$i];
			$o->learning_subjects		= $obj->learning_subjects[$i];
			$o->learning_qualifications	= $obj->learning_qualifications[$i];
			$learning[] = $o;
		} 
		$obj->learning = $learning; 
		
		/* training */
		$training = array(); 
		for($i = 0 ; $i < count($obj->training_time) ; $i++ ){
			$o = new StdClass();
			$o->training_time 			= $obj->training_time[$i];
			$o->training_names 			= $obj->training_names[$i];
			$o->training_subjects		= $obj->training_subjects[$i];
			$o->training_qualifications	= $obj->training_qualifications[$i];
			$training[] = $o;
		} 
		$obj->training = $training;
		
		/* foreign language */
		$languages = array();  
		for($i = 0 ; $i < count($obj->foreign_lang) ; $i++ ){
			$o = new StdClass();
			$o->foreign_lang 			= $obj->foreign_lang[$i];
			$o->foreign_lang_level		= $obj->foreign_lang_level[$i];
			$o->foreign_lang_school		= $obj->foreign_lang_school[$i];
			
			$array = explode(";",$obj->foreign_skills[$i]); //var_dump($array);exit;
			$array_lable = array("Nghe","N&oacute;i","&#272;&#7885;c","Vi&#7871;t");
			$str_foreign_skills = '';
			for( $j=0 ; $j<4;$j++){
				
				$str_foreign_skills .= '&nbsp;<input type="checkbox" name="lang_skills" style="border:0" disabled="disabled" value="1"'.($array[$j]==1?'checked="checked"':'').' />&nbsp;'.$array_lable[$j];
			}
			
			$o->str_foreign_skills		=  $str_foreign_skills;
			$languages[] = $o;
		} 
		$obj->languages = $languages; 
		/* experience */
		$experience = array(); //var_dump($obj->exp_time);exit;
		for($i = 0 ; $i < count($obj->exp_time) ; $i++ ){
			$o = new StdClass();
			$o->exp_time 				= $obj->exp_time[$i];
			$o->exp_company_name		= $obj->exp_company_name[$i];
			$o->exp_activity			= $obj->exp_activity[$i];
			$o->exp_phone_fax			= $obj->exp_phone_fax[$i];
			$o->exp_company_type		= $exp_company_type[$obj->exp_company_type[$i]];
			$o->exp_position			= $obj->exp_position[$i];
			$o->exp_description			= $obj->exp_description[$i];
			$o->exp_time_type			= $exp_time_type[$obj->exp_time_type[$i]];
			$o->exp_boss				= $obj->exp_boss[$i];
			$o->exp_total_staff			= $obj->exp_total_staff[$i];
			$o->exp_loan				= $obj->exp_loan[$i];
			$o->exp_discontinue			= $obj->exp_discontinue[$i];
			$experience[] = $o;
		} 
		$obj->experience = $experience; 
		
		/* reference */
		$reference = array(); 
		for($i = 0 ; $i < count($obj->reference) ; $i++ ){
			$o = new StdClass();
			$o->reference 			= $obj->reference[$i];
			$o->ref_name 			= $obj->ref_name[$i];
			$o->ref_position		= $obj->ref_position[$i];
			$o->ref_part			= $obj->ref_part[$i];
			$reference[] = $o;
		} 
		$obj->reference = $reference;
		
		/* reference1 */
		$reference1 = array(); 
		for($i = 0 ; $i < count($obj->reference1) ; $i++ ){
			$o = new StdClass();
			$o->reference1 			= $obj->reference1[$i];
			$o->ref_name1 			= $obj->ref_name1[$i];
			$o->ref_position1		= $obj->ref_position1[$i];
			$o->ref_part1			= $obj->ref_part1[$i];
			$reference1[] = $o;
		} 
		$obj->reference1 = $reference1;

		return $obj;
	}
	
}
?>
