<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class JobsManager {
	function showJobs( &$rows, $nb, &$pageNav, $option, &$lists, &$children ) {
		$user =& JFactory::getUser();
		$ordering = ($lists['order'] == 'j.ordering');
		JHTML::_('behavior.tooltip');
		?>
<form action="index.php?option=com_nsp" method="post" name="adminForm">
	<table>
		<tr>
			<td align="left" width="100%">
				<?php echo JText::_( 'Filter (Job Title)' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?>
				</button>
			</td>
			<td nowrap="nowrap">
				<?php
				echo $lists['state'];
				?>
			</td>
		</tr>
	</table>
	<div id="tablecell">
		<table class="adminlist">
			<thead>
				<tr>
					<th width="5">
						<?php echo JText::_( 'NUM' ); ?></th>
					<th width="20">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th width="1%" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'ID', 'c.id', @$lists['order_Dir'], @$lists['order'], 'list_jobs' ); ?></th>
					<th class="title">
						<?php echo JHTML::_('grid.sort', 'Job Title', 'j.job_title', @$lists['order_Dir'], @$lists['order'], 'list_jobs' ); ?></th>
					<th align="center">
						<?php echo JHTML::_('grid.sort', 'Number', 'j.number', @$lists['order_Dir'], @$lists['order'], 'list_jobs' ); ?></th>
					<th align="center"><?php echo JHTML::_('grid.sort', 'Items', 'numoptions', @$lists['order_Dir'], @$lists['order'], 'list_jobs' ); ?></th>
					<th align="center"><?php echo JHTML::_('grid.sort', 'Created Date', 'j.post_date', @$lists['order_Dir'], @$lists['order'], 'list_jobs' ); ?></th>
					<th align="center">
						<?php echo JHTML::_('grid.sort', 'Published', 'j.published', @$lists['order_Dir'], @$lists['order'], 'list_jobs' ); ?></th>
					
					<th colspan="3" nowrap="nowrap">
						<?php echo JHTML::_('grid.sort', 'Ordering', 'j.ordering', @$lists['order_Dir'], @$lists['order'], 'list_jobs' ); ?><?php echo JHTML::_('grid.order', $rows, 'filesave.png', 'save_job_order' ); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="10"><?php echo $pageNav->getListFooter(); ?></td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0; 
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];

				$link 		= 'index.php?option=com_nsp&task=edit_job&cid[]='. $row->id;
				$items_link = "index.php?option=com_nsp&task=list_jobapply&job_id=".$row->id;

				$checked 	= JHTML::_('grid.checkedout', $row, $i );
				$published 	= JHTML::_('grid.published', $row, $i );
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td><?php echo $pageNav->getRowOffset( $i ); ?></td>
					<td><?php echo $checked; ?></td>
					<td align="center"><?php echo $row->id; ?></td>
					<td>
						<a href="<?php echo JRoute::_( $link ); ?>" title=""><?php echo( $row->job_title); ?></a>
					</td>
					<td align="center"><?php echo $row->number; ?></td>
					<td align="center"><a href="<?php echo $items_link?>" title="View All Profiles">
						<?php echo $row->numoptions;?>
						<img src="components/com_nsp/images/icon-arrow.gif" width="11" height="9" />
						</a>
					</td>
					<td align="center"><?php echo  date("d/m/Y", strtotime($row->post_date));  ?></td>
					<td align="center"><?php echo  $published  ?></td>
					
<td width="1%" align="center"><?php  echo $pageNav->orderUpIcon( $i, ($row->id1 == @$rows[$i-1]->id1) , 'orderup_job', 'Move Up', $ordering); ?></td>
					<td width="1%" align="center"><?php echo $pageNav->orderDownIcon( $i, $n, ($row->id1 == @$rows[$i+1]->id1), 'orderdown_job', 'Move Down', $ordering ); ?></td>
					<td width="1%" align="center" nowrap="nowrap"><?php $disabled = $ordering ? '' : 'disabled="disabled"'; ?><input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" <?php echo $disabled; ?> class="text_area" style="text-align: center" /></td>					
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
		</table>
	</div>

	<input type="hidden" name="option" value="<?php echo $option;?>" />
	<input type="hidden" name="task" value="list_jobs" />
	<input type="hidden" name="section" value="manage_job" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="" />
</form>
<?php
	}

	function editJob( &$row, &$lists ) {
		global $option;
		if (JRequest::getCmd('task') == 'add_job') {
			//Default values for new books
			$row->category_name = 'New Job';
			$row->description = '';
			$row->published = 1;
			$row->ordering = 999;
			$row->gender = 'Nam/Nữ';
			$row->number = 5;
			$row->published = 1;
			$row->location = "359 Võ Văn Tần Q.3 Tp.Hồ Chí Minh";
			$row->job_type = 'Nhân viên chính thức';
			$row->salary	= 'Thỏa thuận';
			$row->probationary = 'Nhận việc ngay';
			$row->profile = '- Được mua BHXH, BHYT khi ký hợp đồng chính thức.<br />- Được tổ chức đi du lịch và thưởng định kỳ theo lịch trình của công ty<br />- Được học hỏi các kỹ thuật, kỹ   năng mới.';
			$row->experience = '1 năm';
			
		}
		JRequest::setVar( 'hidemainmenu', 1 );
		$editor =& JFactory::getEditor();
		jimport('joomla.filter.output');
		JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES );
		jimport('joomla.html.pane');
		$pane =& JPane::getInstance('sliders');
		JHTML::_('behavior.tooltip');
?>
		
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel_job') {
			submitform( pressbutton );
			return;
		}
		// do field validation
		if (form.name.value == "") {
			alert( "<?php echo JText::_( 'Job must have a Name', true ); ?>" );
		} else {
			submitform( pressbutton );
		}
	}
</script>
<br>
<form action="index.php" method="post" name="adminForm">
<table width="100%" border="0" cellspacing="5" cellpadding="0" class="adminform">
	<tr>
		<td valign="top">
			<table class="admintable" width="100%">
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Job ID' ); ?></label></td>
					<td><?php echo $row->id; ?>&nbsp;</td>
				</tr>
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Job Title' ); ?></label></td>
					<td><input class="inputbox" type="text" name="job_title" id="job_title" size="60" value="<?php echo $row->job_title; ?>" /></td>
				</tr>
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Number' ); ?></label></td>
					<td><input class="inputbox" type="text" name="number" id="number" size="20" value="<?php echo $row->number; ?>" /></td>
				</tr>
				<tr>
					<td class="key"><?php echo JText :: _('Published'); ?></td>
					<td><?php echo JHTML::_( 'select.booleanlist', 'published', 'class="inputbox"', $row->published ); ?></td>
			    </tr>
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Location' ); ?></label></td>
					<td><input class="inputbox" type="text" name="location" id="location" size="60" value="<?php echo $row->location; ?>" /></td>
				</tr>
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Experience' ); ?></label></td>
					<td><input class="inputbox" type="text" name="experience" id="experience" size="60" value="<?php echo $row->experience; ?>" /></td>
				</tr>
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Minimum education level:' ); ?></label></td>
					<td><input class="inputbox" type="text" name="mini_edu_level" id="mini_edu_level" size="60" value="<?php echo $row->mini_edu_level; ?>" /></td>
				</tr>
				
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Gender' ); ?></label></td>
					<td><input class="inputbox" type="text" name="gender" id="gender" size="60" value="<?php echo $row->gender; ?>" /></td>
				</tr>
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Job Type' ); ?></label></td>
					<td><input class="inputbox" type="text" name="job_type" id="job_type" size="60" value="<?php echo $row->job_type; ?>" /></td>
				</tr>
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Salary' ); ?></label></td>
					<td><input class="inputbox" type="text" name="salary" id="salary" size="60" value="<?php echo $row->salary; ?>" /></td>
				</tr>
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Trial Time' ); ?></label></td>
					<td><input class="inputbox" type="text" name="probationary" id="probationary" size="60" value="<?php echo $row->probationary; ?>" /></td>
				</tr>
				<tr>
					<td class="key"><label for="title"><?php echo JText::_( 'Expiration Date' ); ?></label></td>
					<td><?php echo JHTML::calendar($row->end_date,'end_date' ,'end_date' ,'%Y-%m-%d %H:%I:%S',$attribs); ?></td>
				</tr>
				</tr>
				<tr>
					<td class="key" valign="top"><label for="title"><?php echo JText::_( 'Alert Email' ); ?></label></td>
					<td><input class="inputbox" type="text" name="mail_admin" id="mail_admin" size="60" value="<?php echo $row->mail_admin; ?>" /></td>
				</tr>
				
				<tr>
					<td class="key" valign="top"><?php echo JText::_( 'Job Short Description' ); ?></td>
					<td><?php echo $editor->display( 'job_short_desc', $row->job_short_desc, '550', '200', '60', '20', true ) ; ?></td>
				</tr>
				
				<tr>
					<td class="key" valign="top"><?php echo JText::_( 'Job Description' ); ?></td>
					<td><?php echo $editor->display( 'job_description', $row->job_description, '550', '200', '60', '20', true ) ; ?></td>
				</tr>
				
				<tr>
					<td class="key" valign="top"><?php echo JText::_( 'Job Skills' ); ?></td>
					<td><?php echo $editor->display( 'job_skills', $row->job_skills, '550', '200', '60', '20', true ) ; ?></td>
				</tr>
				<tr>
					<td class="key" valign="top"><?php echo JText::_( 'Profile' ); ?></td>
					<td><?php echo $editor->display( 'profile', $row->profile, '550', '200', '60', '20', true ) ; ?></td>
				</tr>
				<tr>
					<td class="key" valign="top"><?php echo JText::_( 'Regimes' ); ?></td>
					<td><?php echo $editor->display( 'regimes', $row->regimes, '550', '200', '60', '20', true ) ; ?></td>
				</tr>
			</table>

	    <input type="hidden" name="task" value="" />
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="cid[]" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="ordering" value="<?php echo $row->ordering; ?>" />
	  </td>
  </tr>
</table>
</form>
<?php 
}
}
?>
