<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Config {
	function Configuration( $config, $lists ) {
	global $option;
	JHTML::_('behavior.tooltip');
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		submitform( pressbutton );
	}
</script>
<form action="index.php?option=com_nsp&task=configuration" method="post" name="adminForm">
	<table width="100%" class="adminform">
		<tr valign="middle">
			<td width="50%" valign="top" nowrap="nowrap">
				<fieldset class="adminform">
				<legend><?php echo JText::_( 'Genaral Settings' ); ?></legend>
					<table class="admintable">
					  <tr>
						<td class="key"><?php echo JText::_('Send Email to Admin');?></td>
						<td><?php echo JHTML::_('select.booleanlist','job_sendmail2admin','class=""',$config->job_sendmail2admin,'Yes','No');?></td>
					  </tr>
					  <tr>
						<td class="key"><?php echo JText::_('Send Email to');?></td>
						<td><input type="text" value="<?php echo $config->job_mailto;?>" name="job_mailto" id="" size="55" /></td>
					  </tr>
					  <tr>
						<td class="key"><?php echo JText::_('CC Email');?></td>
						<td><input type="text" value="<?php echo $config->job_mailcc;?>" name="job_mailcc" id="" size="55" /></td>
					  </tr>
					  <tr>
						<td class="key"><?php echo JText::_('Email Subject');?></td>
						<td><input type="text" value="<?php echo $config->job_mail_subject;?>" name="job_mail_subject" id="" size="35" /></td>
					  </tr>
					  
					  <tr>
						<td class="key"><?php echo JText::_('Send Email to Applier');?></td>
						<td><?php echo JHTML::_('select.booleanList','job_sendmail2applier','class="inputbox"',$config->job_sendmail2applier,'Yes','No');?></td>
					  </tr>
					  

					  
					  
					</table>
				</fieldset>
				<br/>
				<fieldset class="adminform">
					<legend><?php echo JText::_( 'Display Settings' ); ?></legend>
					<table class="admintable">
					
					  <tr>
						<td class="key"><span class="hasTip" title="Number of item per a row"><?php echo JText::_('Num of Item Per Row');?></span></td>
						<td><input type="text" value="<?php echo $config->num_item_per_row;?>" name="num_item_per_row" id="thumbnail_width" /></td>
					  </tr>

					  <tr>
						<td class="key"><?php echo JText::_('Num of Item Per Page');?></td>
						<td><input type="text" value="<?php echo $config->num_item_per_page;?>" name="num_item_per_page" id="thumbnail_height" /></td>
					  </tr>
						
					</table> 
				</fieldset></td>
		    <td width="50%" valign="top" nowrap="nowrap"><fieldset class="adminform">
					<legend><?php echo JText::_( 'Others Setting' ); ?></legend>
					
				</fieldset>
				
					</td>
		</tr>
		<tr valign="middle">
		  <td colspan="2" valign="top" nowrap="nowrap"></td>
	  </tr>
	</table>
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="" />
</form>
<?php
	}
}
?>
