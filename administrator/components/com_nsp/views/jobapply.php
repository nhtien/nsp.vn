<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class JobApply {
	function showLists( &$rows, &$pageNav, $option, &$lists ) {
		$user =& JFactory::getUser();
		$ordering = ($lists['order'] == 'p.id' || $lists['order'] == 'p.name' || $lists['order'] == 'p.ordering');
		//echo $lists['order'];
		JHTML::_('behavior.tooltip');
		$tmpl = JRequest::getVar('tmpl');
		$popup = ($tmpl=="component" ? true : false);
		?>
		<form action="?option=com_nsp" method="post" name="adminForm">

		<table width="100%">
		<tr>
			<td align="left">
				<?php echo JText::_( 'Filter (User Name)' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button></td>
			<td align="center" nowrap="nowrap">
				</td>
			<td align="center" nowrap="nowrap">
				</td>	
			<td align="right" nowrap="nowrap">
				<?php
				echo JText::_( 'Filter (Job)' ) .' : '.$lists['job_id'];
				echo '&nbsp;&nbsp;'.JText::_( 'Status Filter' ) .' : '.$lists['state'];
				?></td>
		</tr>
		</table>

		<div id="tablecell">
			<table class="adminlist">
			<thead>
				<tr>
					<th width="5" nowrap="nowrap"><?php echo JText::_( 'NUM' ); ?></th>
					<th width="20" nowrap="nowrap"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th width="1%" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'ID', 'p.id', @$lists['order_Dir'], @$lists['order'], 'list_jobapply' ); ?></th>
					<th nowrap="nowrap" class="title"><?php echo JHTML::_('grid.sort', 'Name', 'p.name', @$lists['order_Dir'], @$lists['order'], 'list_jobapply' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Position', 'p.position', @$lists['order_Dir'], @$lists['order'], 'list_jobapply' ); ?></th>
					<th><?php echo JHTML::_('grid.sort', 'Gender', 'p.gender', @$lists['order_Dir'], @$lists['order'], 'list_jobapply' ); ?></th>
					<th><?php echo JHTML::_('grid.sort', 'Ages', 'p.birthday_year', @$lists['order_Dir'], @$lists['order'], 'list_jobapply' ); ?></th>
					<th><?php echo JHTML::_('grid.sort', 'Email', 'p.email', @$lists['order_Dir'], @$lists['order'], 'list_jobapply' ); ?></th>

					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Submitted on', 'p.post_date', @$lists['order_Dir'], @$lists['order'], 'list_jobapply' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Status', 'p.is_read', @$lists['order_Dir'], @$lists['order'], 'list_jobapply' ); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $pageNav->getListFooter(); ?>					
					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0; 
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];

				$link 		= 'index.php?option=com_nsp&task=jobapply_detail&cid[]='. $row->id ;

				$checked 	= JHTML::_('grid.checkedout', $row, $i );
				$status 	=  $row->is_read==1 ? "Processed" : "Pending";
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td><?php echo $pageNav->getRowOffset( $i ); ?></td>
					<td><?php echo $checked; ?></td>
					<td align="center"><?php echo $row->id; ?></td>
					<td>
						<a href="<?php echo JRoute::_( $link ); ?>" title=""><?php echo $row->name; ?></a>
					</td>
					<td align="center"><?php echo $row->position; ?></td>
					<td align="center"><?php echo $row->gender == 1 ? "M" : "F";?></td>
					<td align="center"><?php echo  date("Y") - $row->birthday_year  ?></td>
					<td><a href="mailto:<?php echo $row->email ?>"><?php echo $row->email?></a></td>
					<td align="left"><?php echo date("d/m/Y",strtotime($row->post_date));?></td>
					<td align="center"><?php echo $status;?></td>
					
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>
		</div>

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="list_jobapply" />
		<input type="hidden" name="section" value="list_jobapply" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="DESC" />
		</form>
		<?php
	}
	
	function detail( &$row, &$lists ) {
		global $option;
		JHTML::_('behavior.modal', 'a.modal-button');
		?>
<form action="?option=com_nsp" method="post" name="adminForm">

	<div class="jobapply">
	<?php if( JRequest::getVar('tmpl') == 'component'){?>
		<a href="javascript:void(0);" onclick="window.print();">Print</a>
	<?php }else{
		echo '<a href="index.php?option=com_nsp&task=jobapply_detail&cid[]='.$row->id.'&tmpl=component" target="_blank">Print</a>';
	}
	?>
		<h2>THÔNG TIN ỨNG VIÊN</h2>
		<?php if( $row->avatar):?><a class="modal-button" href="<?php echo JURI::root().'documents/jobs/pictures/'.$row->picture;?>" rel="{handler: 'image', size: {x: 200, y: 150}, overlayOpacity: 0.7, classWindow: 'escdownloadpreviewwindow', classOverlay: 'escdownloadpreviewoverlay'}" >
			<img src="<?php echo JURI::root().'documents/jobs/avatars/'.$row->avatar?>" title="<?php echo $row->name?>" align="preview image" border="0" hspace="15" vspace="15" />&nbsp;<br />
			Xem ảnh đầy đủ</a>
		<?php endif;?><br />
		<?php if( $row->attachment):?>
			<a href="<?php echo JURI::root().'documents/jobs/attachments/'.$row->attachment?>">Tải file đính kèm</a>
		<?php endif;?>
		<P>
		<table width="auto" border="0" cellspacing="0" cellpadding="2" class="">
		  <tr>
			<td><label class="">Ngày nộp đơn xin việc:</label></td>
			<td><?php echo date("d/m/Y", strtotime($row->post_date));?></td>
		  </tr>
		  <tr>
		    <td><label class="">Chức danh dự tuyển:</label></td>
			<td> <?php echo $row->position ?></td>
		  </tr>
		  <tr>
		    <td><label class="">Mức lương mong muốn:</label></td>
			<td> <?php echo $row->expected_salary ?></td>
		  </tr>
		 </table>
		
		</P>
		<h3>Thông tin cá nhân</h3>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl">
  <tr>
    <td>Họ và Tên: <br /><?php echo $row->name?></td>
    <td>Ngày sinh: <?php echo $row->birthday; ?><br />
      Nơi sinh: <?php echo $row->birth_place?></td>
    <td>Giới tính: <?php echo $row->str_gender?></td>
  </tr>
  <tr>
    <td>Địa chỉ thường trú : </td>
    <td><?php echo $row->address?></td>
    <td>Điện thoại: <?php echo $row->phone?> </td>
  </tr>
  <tr>
    <td>Địa chỉ liên lạc: </td>
    <td><?php echo $row->address2?></td>
    <td>Điện thoại: <?php echo $row->mobilephone?></td>
  </tr>
</table>
<h3>Quá trình học tập</h3>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl">
  <tr class="title">
    <td>Thời gian( từ...đến...) </td>
    <td>Trường/đơn vị đào tạo </td>
    <td>Chuyên ngành </td>
    <td>Bằng cấp </td>
  </tr>
  <?php foreach( $row->learning as $r):?>
  <tr>
    <td><?php echo $r->learning_time ?>&nbsp;</td>
    <td><?php echo $r->learing_names ?>&nbsp;</td>
    <td><?php echo $r->learning_subjects ?>&nbsp;</td>
    <td><?php echo $r->learning_qualifications ?>&nbsp;</td>
  </tr>
  <?php endforeach;?>
</table>
<h3>Các khóa đào tạo <span class="">(Nghiệp vụ, chuyên môn...)</span></h3>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl">
  <tr class="title">
    <td>Thời gian( từ...đến...) </td>
    <td>Trường/đơn vị đào tạo </td>
    <td>Chuyên ngành </td>
    <td>Bằng cấp </td>
  </tr>
  <?php foreach( $row->training as $r):?>
  <tr>
    <td><?php echo $r->training_time ?>&nbsp;</td>
    <td><?php echo $r->training_names ?>&nbsp;</td>
    <td><?php echo $r->training_subjects ?>&nbsp;</td>
    <td><?php echo $r->training_qualifications ?>&nbsp;</td>
  </tr>
  <?php endforeach;?>
</table>
<h3>Trình độ ngoại ngữ</h3>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl">
  <tr class="title">
    <td>Ngoại ngữ </td>
    <td>Trình độ</td>
    <td>Nơi cấp</td>
    <td>Khả năng </td>
  </tr>
  <?php foreach( $row->languages as $r):?>
  <tr>
    <td><?php echo $r->foreign_lang ?>&nbsp;</td>
    <td><?php echo $r->foreign_lang_level ?>&nbsp;</td>
    <td><?php echo $r->foreign_lang_school ?>&nbsp;</td>
    <td><?php echo $r->str_foreign_skills ?>&nbsp;</td>
  </tr>
  <?php endforeach;?>
</table>

<h3>Kỹ năng</h3>
	<p>	<strong>Nêu các phần mềm vi tính biết sử dụng thành thạo:</strong><br />
	<?php echo $row->computure_software; ?>
	</p>
	<p>
	<strong>Máy móc văn phòng: các máy móc văn phòng biết sử dụng:</strong><br />
	<?php echo $row->office_machine; ?>
	</p>
	<p>
	<label class="lb"><strong>Các kỹ năng khác:</strong></label><br />
	<?php echo $row->other_skills; ?>
	</p>
	
	<h3>Kinh nghiệm làm việc</h3>
	
	<?php
	$i = 1;
	 foreach( $row->experience as $r):?>
	<table cellpadding="0" cellspacing="0" border="0" class="tbl" width="100%">
	  <tr>
	    <td><label class="lb"><?php echo $i?>/ Thời gian (từ...đến...)</label> <?php echo $r->exp_time?></td>
		<td><label class="lb">Tên đơn vị:</label> <?php echo $r->exp_company_name?></td>
		<td><label class="lb">Ngành hoạt động:</label> <?php echo $r->exp_activity?></td>
	  </tr>
	  <tr>
	    <td><label class="lb"></label></td>
		<td><label class="lb">Loại hình: </label><?php echo $r->exp_company_type?></td>
		<td><label class="lb">Điên thoại/Fax liên lạc: </label><?php echo $r->exp_phone_fax?></td>
	  </tr>
	  <tr>
	    <td><label class="lb">Chức danh:</label> <?php echo $r->exp_position?></td>
		<td><label class="lb">Mô tả ngắn gọn công việc: </label> <?php echo $r->exp_description?></td>
		<td><label class="lb">Loại hình: </label><?php echo $r->exp_time_type?></td>
	  </tr>
	  <tr>
	    <td><label class="lb">Thu nhập:</label> <?php echo $r->exp_loan?></td>
		<td><label class="lb">Tên và chức vụ cấp quản lý trực tiếp: </label><?php echo $r->exp_boss?></td>
		<td><label class="lb">Số nhân viên phụ trách:</label> <?php echo $r->exp_total_staff?></td>
	  </tr>
	  <tr>
	    <td colspan="3"><label class="lb">Lý do thôi việc:</label> <?php echo $r->exp_discontinue?></td>
	  </tr>
	 </table><br />
	  <?php $i++; endforeach;?>
	  
	<h3>Các thông tin khác</h3>
	<p>Cho biết về người quen (họ hàng, bạn bè,...) đang làm việc tại NSP:</p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl">
	  <tr class="title">
		<td width="25%">Quan hệ </td>
		<td width="25%">họ tên</td>
		<td width="25%">Chức vụ</td>
		<td width="25%">Bộ phận </td>
	  </tr>
	  <?php foreach( $row->reference as $r):?>
	  <tr>
		<td><?php echo $r->reference ?>&nbsp;</td>
		<td><?php echo $r->ref_name ?>&nbsp;</td>
		<td><?php echo $r->ref_position ?>&nbsp;</td>
		<td><?php echo $r->ref_part ?>&nbsp;</td>
	  </tr>
	  <?php endforeach;?>
	</table>
	
	<p>Cho biết về người quen (họ hàng, bạn bè,...) đang làm việc tại công ty khác:</p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl">
	  <tr class="title">
		<td width="25%">Quan hệ </td>
		<td width="25%">họ tên</td>
		<td width="25%">Chức vụ</td>
		<td width="25%">Bộ phận </td>
	  </tr>
	  <?php foreach( $row->reference1 as $r):?>
	  <tr>
		<td><?php echo $r->reference1 ?>&nbsp;</td>
		<td><?php echo $r->ref_name1 ?>&nbsp;</td>
		<td><?php echo $r->ref_position1 ?>&nbsp;</td>
		<td><?php echo $r->ref_part1 ?>&nbsp;</td>
	  </tr>
	  <?php endforeach;?>
	</table>
	<p>Nêu tên, chức vụ, nơi công tác của 2 người (không phải người thân) biết rõ về quá trình của bạn để NSP tham khảo nếu cần thiết.</p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl">
	  <tr class="">
		<td><label class="lb">1.</label> </td>
		<td><label class="lb">2.</label></td>
	  </tr>
	  <tr>
		<td width="50%"><?php echo $row->reference1_detail?></td>
		<td width="50%"><?php echo $row->reference2_detail?> </td>
	  </tr>
	 </table>
	 
	<p>Tự nhập xét về những điểm mạnh và điểm yếu.</p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl">
	  <tr class="">
		<td><label class="lb">Điểm mạnh</label> </td>
		<td><label class="lb">Điểm yếu</label></td>
	  </tr>
	  <tr>
		<td width="50%"><?php echo $row->strength?></td>
		<td width="50%"><?php echo $row->weakness?> </td>
	  </tr>
	 </table>
	</div>

		<input type="hidden" name="task" value="" />
		</form>
		<?php
	}
}
?>
