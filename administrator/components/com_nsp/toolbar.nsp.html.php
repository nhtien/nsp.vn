<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class TOOLBAR_nsp {

	function _CONFIGURATION() {
		JToolBarHelper::title(  JText::_( 'NSP Component' ) .': <small> : '.JText::_( 'Configuration' ).'</small>' );
		JToolBarHelper::save( 'save_configuration', 'Save' );
		JToolBarHelper::apply( 'apply_configuration', 'Apply' );
		JToolBarHelper::cancel( 'cancel_configuration', 'Cancel' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

/* categories */
	function _JOBS_MANAGER() {
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		JToolBarHelper::title(  JText::_( 'NSP Component') .': <small> : '.JText::_( 'Jobs Manager' ).'</small>' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList( '', 'remove_job', 'Delete' );
		JToolBarHelper::editListX( 'edit_job', 'Edit' );
		JToolBarHelper::addNewX( 'add_job', 'New' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

	function _EDIT_JOB( $categoryid ) {
			$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
			JArrayHelper::toInteger($cid, array(0));
	
			$text = ( $cid[0] ? JText::_( 'Edit Job' ) : JText::_( 'New Job' ) );
	
			JToolBarHelper::title(  JText::_( 'NSP Component' ).': <small> : '.JText::_( 'Jobs Manager' ).' : <small>[ ' . $text.' ]</small></small>' );
			JToolBarHelper::save( 'save_job', 'Save' );
			JToolBarHelper::apply( 'apply_job', 'Apply' );
			if ($cid[0]) {
				// for existing items the button is renamed `close`
				JToolBarHelper::cancel( 'cancel_job', 'Close' );
			} else {
				JToolBarHelper::cancel( 'cancel_job', 'Cancel' );
			}
			JToolBarHelper::help( '../help.html', true );
		}
/* reporting */
	function _JOBAPPLY(){
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		JToolBarHelper::title(  JText::_( 'NSP Component') .': <small> : '.JText::_( 'Jobs Manager' ).'</small>' );
		JToolBarHelper::customX( 'jobapply_detail', 'preview.png', 'preview.png', 'Review' ,true );
		JToolBarHelper::deleteList( '', 'remove_jobapply', 'Delete' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}
	function _JOBAPPLY_DETAIL(){
		JToolBarHelper::title(  JText::_( 'NSP Component') .': <small> : '.JText::_( 'Jobs Manager' ).'</small>' );
		JToolBarHelper::customX( 'list_jobapply', 'back.png', 'back.png', 'Back' ,false );
		JToolBarHelper::help( '../help.html', true );
	}
	
	function _DEFAULT() {
		JToolBarHelper::title(  JText::_( 'NSP Component Manager' ) );
		JToolBarHelper::help( '../help.html', true );
	}
}
?>