<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined('_JEXEC') or die();

jimport( 'joomla.application.component.controller' );

$lang = & JFactory::getLanguage();
$lang->load('escmobile', JPATH_COMPONENT);

class NspController extends JController {

	function __construct( $default = array())
	{
		parent::__construct( $default );
		
		//some tasks about configuration
		$this->registerTask( 'apply_configuration', 'save_configuration' );
		
		//some tasks about categories
		$this->registerTask( 'add_job' , 'edit_job' );
		$this->registerTask( 'apply_job', 'save_job');
		$this->registerTask( 'orderup_job', 'reorder_job' );
		$this->registerTask( 'orderdown_job', 'reorder_job' );

		//some tasks about main
		$this->registerTask( 'main', 'showMain' );
		//some tasks about publish and unpublish	
		$this->registerTask( 'publish', 'publish');
		$this->registerTask( 'unpublish', 'publish');	
		
	}
	function publish() {
		global $mainframe;

		$db 	=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid		= JRequest::getVar( 'cid', array(), '', 'array' );
		$publish	= ( $this->_task == 'publish' ? 1 : 0 );
		$option		= JRequest::getCmd( 'option', 'com_nsp', '', 'string' );

		JArrayHelper::toInteger($cid);

		if (count( $cid ) < 1) {
			$action = $publish ? 'publish' : 'unpublish';
			JError::raiseError(500, JText::_( 'Select an item to '.$action, true ) );
		}
		$cids = implode( ',', $cid );

		switch (JRequest::getVar( 'section', '', 'post', 'string' )) {
			case 'manage_job':
				$query = 'UPDATE #__nsp_job'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN ( '. $cids .' )'
				;
				$db->setQuery( $query );
				if (!$db->query()) {
					JError::raiseError(500, $db->getErrorMsg() );
				}

				if (count( $cid ) == 1) {
					$row =& JTable::getInstance('job', 'Table');
					$row->checkin( $cid[0] );
				}
				$link = 'index.php?option=com_nsp&task=list_jobs';
				$this->setRedirect($link);
				break;
				
		}
	}

	function showMain() {
		require_once( JPATH_COMPONENT.DS.'views'.DS.'main.php' );
		Main::showMain();
	}
	
/* some function about configuaration */
	
	function configuration() {
		global $nsp_config;
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'configuration.php' );
		Config::Configuration($nsp_config, $lists);
	}
	
	function save_configuration() {
		global $option, $task, $mainframe;
		
		$post = JRequest::get( 'post' );
		$config = &JTable::getInstance('config','Table');
		$config->save( $post );
		switch ($this->_task) {
			case 'apply_configuration':
				$msg = JText::_( 'Configuration saved' );
				$link = 'index.php?option=com_nsp&task=configuration';
			break;
			case 'save_configuration':
			default:
				$msg = JText::_( 'Configuration saved' );
				$link = 'index.php?option=com_nsp&task=main';
			break;
		}
		$mainframe->redirect($link, $msg);
	}
	
	function cancel_configuration() {
		require_once( JPATH_COMPONENT.DS.'views'.DS.'main.php' );
		Main::showMain();
	}
/* some function about categries */
	function list_jobs() {
		global $mainframe, $option;

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order_jobs",		'filter_order',		'j.ordering',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir_jobs",	'filter_order_Dir',	'',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state_jobs",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search_jobs",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit_jobs', 'limit', 100 );
		
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'limitstart_jobs', 'limitstart', 0, 'int' );
		$where = array();

		if ( $filter_state )
		{
			if ( $filter_state == 'P' )
			{
				$where[] = 'j.published = 1';
			}
			else if ($filter_state == 'U' )
			{
				$where[] = 'j.published = 0';
			}
		}
		if ($search)
		{
			$where[] = 'LOWER(j.name) LIKE '.$db->Quote('%'.$search.'%');
		}

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;
		
		$query = 'SELECT COUNT(j.id)'
		. ' FROM #__nsp_job AS j'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult();

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT j.*, COUNT(js.id) AS numoptions'
		. ' FROM #__nsp_job as j'
		. ' LEFT JOIN #__nsp_jobs AS js ON js.job_id = j.id'
		. $where
		. ' GROUP BY j.id'
		. $orderby
		;
			
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$rows = $db->loadObjectList();

		// state filter
		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;
		

		require_once( JPATH_COMPONENT.DS.'views'.DS.'job.php' );
		JobsManager::showJobs( $rows, count($rows), $pageNav, $option, $lists, $children  );
	}
	function edit_job() {
		$db		=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid 	= JRequest::getVar( 'cid', array(0), '', 'array' );
		$option = JRequest::getCmd( 'option');
		$uid 	= (int) @$cid[0];

		$row =& JTable::getInstance('job', 'Table');
		$row->load( $uid );
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'job.php' );
		JobsManager::editJob( $row, $lists );
	}
	function save_job() {
		global $mainframe;
		$db =& JFactory::getDBO();
		$user = &JFactory::getUser();
		$row =& JTable::getInstance('job', 'Table');
		$post = JRequest::get( 'post' ); 
		$link = "index.php?option=com_nsp&task=edit_job&cid[]=".$post['id'];
		$post['job_title'] = JRequest::getVar('job_title', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		if( !$post['id'] ){
			$post['post_date'] = date("Y-m-d H:i:s");
			$post['author'] = $user->id;
		}
			
		if( $post['job_title'] == ''){
			$this->setRedirect( $link , "Please Input Data!");
			return;
		}
		$array_p = array('job_short_desc','job_description','job_skills','profile','regimes');
		while(list($k,$v) = each( $array_p)){
			$post[$v] = JRequest::getVar($v,'','POST','string',JREQUEST_ALLOWRAW);
		}
		
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		$isNew = ($row->id == 0);
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}

		switch ($this->_task)
		{
			case 'apply_job':
				$msg = JText::_( 'Job saved' );
				$link = 'index.php?option=com_nsp&task=edit_job&cid[]='. $row->id .'';
				break;

			case 'save_job':
			default:
				$msg = JText::_( 'Job saved' );
				$link = 'index.php?option=com_nsp&task=list_jobs';
				break;
		}
		if( !$row->id )
			$this->reorder_job();
		$mainframe->redirect($link, $msg);
	}
	
	function remove_job() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );

		for ($i=0, $n=count($cid); $i < $n; $i++) {
			$category =& JTable::getInstance('job', 'Table');
			$category->delete( $cid[$i] );
		}
		$msg .= JText::_( 'Job(s) was deleted' );
		$mainframe->redirect('index.php?option=com_nsp&task=list_jobs', $msg);
	}

	function reorder_job() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$this->setRedirect( 'index.php?option=com_nsp&task=list_jobs' );

		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$task	= $this->getTask();
		$inc	= ($task == 'orderup_job' ? -1 : 1);

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		$row =& JTable::getInstance('job', 'Table');
		$row->load( (int) $cid[0] );
		$row->move( $inc );
		$row->reorder();
	}
	function save_job_order() {
		global $mainframe;
		$db			= & JFactory::getDBO();
		$cid		= JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$order		= JRequest::getVar( 'order', array (0), 'post', 'array' );
		$total		= count( $cid );
		
		JArrayHelper::toInteger($cid, array(0));
		JArrayHelper::toInteger($order, array(0));

		$row =& JTable::getInstance('job', 'Table');
		for( $i=0; $i < $total; $i++ ) {
			$row->load( (int) $cid[$i] );
			if ($row->ordering != $order[$i]) $row->ordering = $order[$i];
			$row->store();
		}
		$row->reorder();
		
		$link = 'index.php?option=com_nsp&task=list_jobs';
		$this->setRedirect($link);
	}
	function cancel_job() {
		global $option;
		$this->setRedirect( 'index.php?option='. $option .'&task=list_jobs' );
	}

	
/* jobs apply */
	function list_jobapply(){
		global $mainframe, $option;

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order_jobappy",		'filter_order',		'p.post_date',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir_jobappy",	'filter_order_Dir',	'DESC',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.state_jobappy", 'filter_state',	'',	'string' );
		
		$job_id				= $mainframe->getUserStateFromRequest( "$option.jobid_jobappy", 'job_id',	'',	'string' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search_jobappy", 'search',	'',	'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit_jobappy', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'limitstart_jobappy', 'limitstart', 0, 'int' );
		
		$where = array(); 

		if ( $filter_state == 1 || $filter_state == 0  )
		{
			if ( $filter_state == '1' )
			{
				$where[] = 'p.is_read = 1';
			}
			else if ($filter_state == '0' )
			{
				$where[] = 'p.is_read = 0';
			}
		}
		if ($search)
		{
			$where[] = 'LOWER(p.name) LIKE '.$db->Quote('%'.$search.'%');
		}
		
		if( $job_id ){
			$where[] = "p.job_id = $job_id";
		}

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;

		$query = 'SELECT COUNT(p.id)'
		. ' FROM #__nsp_jobs AS p'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult(); 

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT p.*, u.username AS author'
		. ' FROM #__nsp_jobs AS p'
		. ' LEFT JOIN #__users AS u ON u.id = p.read_by'
		. $where
		. ' GROUP BY p.id'
		. $orderby
		;
		; 
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );  //echo $db->getQuery();
		$rows = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		/*
		$jrows = array();
		$table = &JTable::getInstance('jobapply','Table');
		foreach( $rows as $r){
			$jrows[] = $table->binData( $r );
		}
		*/
		// state filter
		$state_option[] = JHTML::_('select.option','',' select status ');
		$state_option[] = JHTML::_('select.option','0',' pending ');
		$state_option[] = JHTML::_('select.option','1',' processed ');
		$lists['state']	= JHTML::_('select.genericList', $state_option, 'filter_state','class="inputbox" size="1" onchange="this.form.submit()"','value','text',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;
		
		// job id 
		$sql = "SELECT * FROM #__nsp_job WHERE published = 1 ORDER BY ordering";
		$db->setQuery( $sql );
		$rows_job = $db->loadObjectList();
		$job_option[] = JHTML::_('select.option','','- Select a job -');
		foreach( $rows_job as $r){
			$job_option[] = JHTML::_('select.option',$r->id, $r->job_title);
		}
		$lists['job_id'] = JHTML::_('select.genericList', $job_option, 'job_id', 'class="inputbox" onchange="this.form.submit()"','value','text', $job_id);
		require_once( JPATH_COMPONENT.DS.'views'.DS.'jobapply.php' );
		JobApply::showLists( $rows, $pageNav, $option, $lists );	
	}
	
	function jobapply_detail(){
		$cid = JRequest::getVar('cid',array(0),'','array');
		$row = &JTable::getInstance('jobapply','Table'); 
		$row->load( $cid[0] );
		if( $row->is_read==0){
			$row->checkout();
		}
		$data = $row->binData($row);
		require_once( JPATH_COMPONENT.DS.'views'.DS.'jobapply.php' );
		JobApply::detail( $data, $lists );	
	}
	function remove_jobapply(){
		global $mainframe;
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );
		JArrayHelper::toInteger($cid);
		for ($i=0, $n=count($cid); $i < $n; $i++) {
			$row =& JTable::getInstance('jobapply', 'Table');
			$row->load( $cid[$i] );
			$row->delete();
		}
		$msg .= JText::_( 'Job(s) was deleted' );
		$mainframe->redirect('index.php?option=com_nsp&task=list_jobapply', $msg);
	}
	

}

?>