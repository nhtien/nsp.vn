<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( JApplicationHelper::getPath( 'toolbar_html' ) );

switch ($task) {
	case 'configuration':
		TOOLBAR_nsp::_CONFIGURATION();
		break;
/* categories */
	case 'list_jobs':
		TOOLBAR_nsp::_JOBS_MANAGER();
		break;	
		
	case 'edit_job':
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		TOOLBAR_nsp::_EDIT_JOB( $cid[0] );
		break;	
	case 'add_job'  :
		$id = JRequest::getVar( 'id', 0, '', 'int' );
		TOOLBAR_nsp::_EDIT_JOB( $id );
		break;

/* reporting */
	case 'list_jobapply':
		TOOLBAR_nsp::_JOBAPPLY();
		break;	
	case 'jobapply_detail':
		TOOLBAR_nsp::_JOBAPPLY_DETAIL();
		break;
		

	default:
		TOOLBAR_nsp::_DEFAULT();
		break;
}
?>