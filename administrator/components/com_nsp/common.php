<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die('Restricted access');

$exp_company_type = array(
							'DNNN' => 'DN nhà nước',
							'DNTN' => 'DN tư nhân/tnhh',
							'DNNG' => 'DN nước ngoài',
							'DNLD' => 'DN liên doanh'
							);
							
$exp_time_type = array(
							'FUL' => 'Toàn thời gian',
							'PAR' => 'Bán thời gian',
							'PRA' => 'Thực tập',
							'COL' => 'Cộng tác viên'
							);
$lang_skills = array(
							'LST' => 'Nghe',
							'TAL' => 'Nói',
							'REA' => 'Đọc',
							'WRI' => 'Viết'
							);
?>