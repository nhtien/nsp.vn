<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die('Restricted access');

$array_order_status = array(
			'0'=>'Chưa xem',
			'1'=>'Đang xử lý',
			'2'=>'Đã giao hàng'
			);
			
?>