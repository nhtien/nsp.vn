<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Order {
	function show( &$rows, &$pageNav, $option, &$lists ) {
		require(JPATH_COMPONENT.DS.'common.php');
		?>
		<form action="?option=com_nspvoucher" method="post" name="adminForm">

		<table width="100%">
		<tr>
			<td align="left">
				<?php echo JText::_( 'Filter (name)' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button></td>
			<td align="center" nowrap="nowrap">
				</td>
			<td align="center" nowrap="nowrap">
            Trạng thái:<?php echo $lists['order_status'] ?>
				</td>	
			<td align="right" nowrap="nowrap"></td>
		</tr>
		</table>

		<div id="tablecell">
			<table class="adminlist">
			<thead>
				<tr>
					<th width="5" nowrap="nowrap"><?php echo JText::_( 'NUM' ); ?></th>
					<th width="20" nowrap="nowrap"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th width="1%" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'ID', 'p.id', @$lists['order_Dir'], @$lists['order'], 'list_orders' ); ?></th>
					<th nowrap="nowrap" class="title"><?php echo JHTML::_('grid.sort', 'Tên KH', 'o.name', @$lists['order_Dir'], @$lists['order'], 'list_orders' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Điện thoại', 'o.phone', @$lists['order_Dir'], @$lists['order'], 'list_orders' ); ?></th>
                    <th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Công ty', 'o.company_name', @$lists['order_Dir'], @$lists['order'], 'list_orders' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Ngày lập', 'o.created', @$lists['order_Dir'], @$lists['order'], 'list_orders' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Trị giá', 'o.total', @$lists['order_Dir'], @$lists['order'], 'list_orders' ); ?></th>
                    <th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Trạng thái', 'o.status', @$lists['order_Dir'], @$lists['order'], 'list_orders' ); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $pageNav->getListFooter(); ?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0; 
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];
				$checked 	= JHTML::_('grid.checkedout', $row, $i );
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td><?php echo $pageNav->getRowOffset( $i ); ?></td>
					<td><?php echo $checked; ?></td>
					<td align="center"><?php echo $row->id; ?></td>
					<td>
						<a href="index.php?option=com_nspvoucher&task=edit_order&cid[]=<?php echo $row->id?>"><?php echo $row->name; ?></a>
					</td>
                    <td align="center"><?php echo $row->phone ?></td>
                    <td align="center"><?php echo $row->company_name ?></td>
                    <td align="left"><?php echo $row->created;?></td>
					<td align="center"><?php echo number_format($row->total,0,',','.');?> đ</td>
					
					<td align="center"><?php echo $array_order_status[$row->status];?></td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>
		</div>

		<input type="hidden" name="option" value="com_nspvoucher" />
		<input type="hidden" name="task" value="list_orders" />
		<input type="hidden" name="section" value="manage_checked" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="" />
		</form>
		<?php
	
	}
	function edit( &$row, &$lists ) {
		global $option,$nspTraining_config;
		?>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
<table class="" width="auto">
	<tr>
		<td class="key"><?php echo JText::_( 'ID' ); ?></td>
		<td><?php echo $row->id ; ?></td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Name' ); ?></td>
		<td><?php echo $row->name;?></td>
	</tr>
	<tr>
		<td class="key">Mã số thành  viên</td>
		<td><?php echo $row->user_id;?></td>
	</tr>
	<tr>
		<td class="key">Số tiền hiện có</td>
		<td><?php echo vpriceFormat($row->user_crvalue);?></td>
	</tr>
	<tr>
		<td class="key">Địa chỉ</td>
		<td><?php echo $row->address;?></td>
	</tr>
	<tr>
		<td class="key">Điện thoại</td>
		<td><?php echo $row->phone;?></td>
	</tr>
	<tr>
		<td class="key">Email</td>
		<td><?php echo $row->email;?></td>
	</tr>
	<tr>
		<td class="key">Tên công ty</td>
		<td><?php echo $row->company_name;?></td>
	</tr>
	<tr>
		<td class="key">Ngày lập đơn hàng</td>
		<td><?php echo $row->created;?></td>
	</tr>
	<tr>
		<td class="key">Ghi chứ</td>
		<td><?php echo $row->note;?></td>
	</tr>
	<tr>
		<td class="key">Địa chỉ</td>
		<td><?php echo $row->address;?></td>
	</tr>
	<tr>
		<td class="key">Trạng thái đơn hàng</td>
		<td>
		<?php echo $lists['order_status']?>
        </td>
	</tr>
	<tr>
		<td class="key">Tổng giá</td>
		<td>
			<?php echo vpriceFormat($row->total)?>
		</td>
	</tr>
</table>
<h4>Sản phẩm</h4>
<table class="list" width="100%" border="1" cellpadding="5" cellspacing="0">
<tr class="title">
  <td align="center"><strong>Tên sản phẩm</strong></td>
  <td align="center"><strong>Giá sản phẩm</strong></td>
  <td align="center"><strong>Số lượng</strong></td>
  <td align="center"><strong>Thành tiền</strong></td>
 </tr>

<?php
$products = $lists['products'];
$totalPrice = '';
if(!count($products))
{
	echo '<tr><td colspan="5">&nbsp;</td></tr>';
}
else
{
	foreach( $products as $p){
	$subTotalPrice = '';
	if($p->product_price){
		$subTotalPrice = $p->quantity * $p->product_price;
		$totalPrice += $subTotalPrice;
	}else{
		$totalPrice = '';
	}
	?>
	<tr>
	  <td><?php echo $p->product_name?></td>
	  <td nowrap="nowrap"><?php echo vpriceFormat($p->product_price)?></td>
	  <td nowrap="nowrap">
		<?php echo $p->quantity?>
	  </td>
	  <td nowrap="nowrap"><?php echo vpriceFormat($subTotalPrice)?></td>
	 </tr>
	<?php
	}
}
?>
</table>
<p><strong><?php echo 'Tổng giá trị đơn hàng: '.vpriceFormat($totalPrice)?></strong></p>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="option" value="com_nspvoucher" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="cid[]" value="<?php echo $row->id; ?>" />
</form>
		<?php
	}
}
?>
