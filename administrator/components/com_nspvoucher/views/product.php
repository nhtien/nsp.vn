<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Product {
	function show( &$rows, &$pageNav, $option, &$lists ) {
		$user =& JFactory::getUser();
		$ordering = ($lists['order'] == 'p.id' || $lists['order'] == 'p.name' || $lists['order'] == 'p.ordering');
		//echo $lists['order'];
		JHTML::_('behavior.tooltip');
		?>
		<form action="index.php?option=com_nspvoucher&task=list_products" method="post" name="adminForm">

		<table width="100%">
		<tr>
			<td align="left">
				<?php echo JText::_( 'Filter (Name)' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button></td>
			<td align="center" nowrap="nowrap">
				<?php echo JText::_( 'Category Filter' ); ?>:<?php
				echo $lists['category'];
				?></td>
			<td align="center" nowrap="nowrap">
				</td>	
			<td align="right" nowrap="nowrap"><?php echo JText::_( 'State Filter' ); ?>:
				<?php
				echo $lists['state'];
				?></td>
		</tr>
		</table>

		<div id="tablecell">
			<table class="adminlist">
			<thead>
				<tr>
					<th width="5" nowrap="nowrap"><?php echo JText::_( 'NUM' ); ?></th>
					<th width="20" nowrap="nowrap"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th width="1%" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'ID', 'cr.id', @$lists['order_Dir'], @$lists['order'], 'list_products' ); ?></th>
					<th nowrap="nowrap" class="title"><?php echo JHTML::_('grid.sort', 'Name', 'p.name', @$lists['order_Dir'], @$lists['order'], 'list_products' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Price', 'p.price', @$lists['order_Dir'], @$lists['order'], 'list_products' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Author', 'author', @$lists['order_Dir'], @$lists['order'], 'list_products' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Date', 'cr.created_date', @$lists['order_Dir'], @$lists['order'], 'list_products' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Published', 'p.published', @$lists['order_Dir'], @$lists['order'], 'list_products' ); ?></th>
					<th colspan="3" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'Ordering', 'p.ordering', @$lists['order_Dir'],      @$lists['order'], 'list_products' ); ?><?php echo JHTML::_('grid.order', $rows, 'filesave.png', 'save_course_order' ); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $pageNav->getListFooter(); ?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0; 
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];

				$link 		= 'index.php?option=com_nspvoucher&task=edit_product&cid[]='. $row->id ;

				$checked 	= JHTML::_('grid.checkedout', $row, $i );
				$published 	= JHTML::_('grid.published', $row, $i );
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td><?php echo $pageNav->getRowOffset( $i ); ?></td>
					<td><?php echo $checked; ?></td>
					<td align="center"><?php echo $row->id; ?></td>
					<td>
					<a href="<?php echo JRoute::_( $link ); ?>" title="<?php echo JText::_( 'Edit Product' ); ?>"><?php echo $row->name; ?></a>
					</td>
					<td width="1%" align="center"><?php echo number_format($row->price,0,',','.');?></td>
					<td align="left"><?php echo $row->author;?></td>
					<td align="center"><?php echo date("F d, Y",strtotime($row->created));?></td>
					<td align="center"><?php echo $published;?></td>
					<td width="1%" align="center"><?php  echo $pageNav->orderUpIcon( $i, ($row->book_id == @$rows[$i-1]->book_id) , 'orderup_product', 'Move Up', $ordering); ?></td>
					<td width="1%" align="center"><?php echo $pageNav->orderDownIcon( $i, $n, ($row->book_id == @$rows[$i+1]->book_id), 'orderdown_product', 'Move Down', $ordering ); ?></td>
					<td width="1%" align="center" nowrap="nowrap"><?php $disabled = $ordering ? '' : 'disabled="disabled"'; ?><input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" <?php echo $disabled; ?> class="text_area" style="text-align: center" /></td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>
		</div>

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="list_products" />
		<input type="hidden" name="section" value="manage_product" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="" />
		</form>
		<?php
	}
	
	function edit( &$row, &$lists ) {
		global $option,$nspTraining_config;
		if (JRequest::getCmd('task') == 'add_product') {
			$row->published = '1';
			$row->category_id = '';
			$row->description = '';
			$row->ordering = '999';
		}
		JRequest::setVar( 'hidemainmenu', 1 );

		$editor =& JFactory::getEditor();
		
		jimport('joomla.filter.output');
		JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES );
		
		// Build the component's submenu
		jimport('joomla.html.pane');
		$pane =& JPane::getInstance('sliders');
		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.modal','a.modal-button');
		?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel_product') {
			submitform( pressbutton );
			return;
		}
		else if(pressbutton == 'save_product' || pressbutton == 'apply_product' ) {
			if( form.name.value==''){
				alert('Nhập tên sản phẩm');
				return false;
			}
			submitform( pressbutton );
		}else{
			submitform( pressbutton );
		}
	}
</script>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
<table class="adminform">
	<tr>
		<td class="key"><?php echo JText::_( 'Product ID' ); ?></td>
		<td><?php echo $row->id ; ?></td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Name' ); ?></span></td>
		<td>
			<input type="text" name="name" value="<?php echo $row->name;?>" size="60" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText :: _('Published'); ?></td>
		<td><?php echo JHTML::_( 'select.booleanlist', 'published', 'class="inputbox"', $row->published ); ?></td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Price' ); ?></td>
		<td>
			<input type="text" name="price" value="<?php echo $row->price;?>" size="" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText::_( 'Thumbnail File' ); ?></td>
		<td>
        <div><img src="<?php echo JURI::root().'documents/nspvoucher/products/'.$row->thumb?>" /></div>
        <input name="thumbnail" type="file" />
        </td>
	</tr>
	<tr>
		<td class="key"><span class="editlinktip hasTip" title="<?php echo JText::_( 'Ordering' );?>::<?php echo JText::_( 'Order Page Description' ); ?>"><?php echo JText::_( 'Ordering' ); ?></span></td>
		<td><input name="ordering" type="text" class="text_area" id="ordering" value="<?php echo $row->ordering; ?>" /></td>
	</tr>
	<tr>
		<td class="key" style="text-align:left"><span class="key" style="text-align:left"><?php echo JText::_( 'Short Description' ); ?></span>:</td>
		<td><textarea name="short_desc" rows="5" cols="50"><?php echo $row->short_desc;?></textarea></td>
	</tr>
	<tr>
		<td class="key" style="text-align:left"><span class="key" style="text-align:left"><?php echo JText::_( 'Long Description' ); ?></span>:</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="2"><?php echo $editor->display( 'description', $row->description, '60%', '300', '60', '20' ) ; ?></td>
	</tr>
	

</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="cid[]" value="<?php echo $row->id; ?>" />
</form>
		<?php
	}
}
?>
