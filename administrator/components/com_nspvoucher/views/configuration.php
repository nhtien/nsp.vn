<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );


class Config {
	function Configuration( $config, $lists ) {
	global $option;
	JHTML::_('behavior.tooltip');
	$editor = &JFactory::getEditor();
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		submitform( pressbutton );
	}
</script>
<form action="index.php?option=com_nspvoucher&task=configuration" method="post" name="adminForm">
	<table width="100%" class="adminform">
		<tr valign="middle">
			<td width="50%" valign="top" nowrap="nowrap">
				<fieldset class="adminform">
				<legend><?php echo JText::_( 'General Settings' ); ?></legend>
					<table class="admintable">
					  <tr>
						<td class="key"><?php echo JText::_('Meta Title');?></td>
						<td><input type="text" value="<?php echo $config->meta_title;?>" name="meta_title" id="" size="120" /></td>
					  </tr>
					  <tr>
						<td class="key"><?php echo JText::_('Meta Keyword');?></td>
						<td><input type="text" value="<?php echo $config->meta_key;?>" name="meta_key" id="" size="120" /></td>
					  </tr>
					  <tr>
						<td class="key"><?php echo JText::_('Meta Desc');?></td>
						<td><input type="text" value="<?php echo $config->meta_desc;?>" name="meta_desc" id="" size="120" /></td>
					  </tr>
					  <tr>
						<td class="key">Gửi mail khi KH mua SP</td>
						<td><?php echo JHTML::_('select.booleanlist','sendmail2admin','class=""',$config->sendmail2admin);?></td>
					  </tr>
					  <tr>
						<td class="key">Gửi email đến</td>
						<td><input type="text" value="<?php echo $config->sendmailto;?>" name="sendmailto" id="" size="60" /></td>
					  </tr>
					  <tr>
						<td class="key">Nội dung chương trình</td>
						<td><?php echo $editor->display( 'content_text', $config->content_text, '100%', '300', '60', '20', false ) ;?></td>
					  </tr>
					</table>
				</fieldset>
                
                </td>
		</tr>
		<tr valign="middle">
		  <td colspan="2" valign="top" nowrap="nowrap"></td>
	  </tr>
	</table>
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="" />
</form>
<?php
	}
}
?>
