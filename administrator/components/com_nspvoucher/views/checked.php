<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class VChecked {
	function show( &$rows, &$pageNav, $option, &$lists ) {
		?>
		<form action="?option=com_nspvoucher" method="post" name="adminForm">

		<table width="100%">
		<tr>
			<td align="left">
				<?php echo JText::_( 'Filter (Code)' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button></td>
			<td align="center" nowrap="nowrap">
				</td>
			<td align="center" nowrap="nowrap">
				</td>	
			<td align="right" nowrap="nowrap"></td>
		</tr>
		</table>

		<div id="tablecell">
			<table class="adminlist">
			<thead>
				<tr>
					<th width="5" nowrap="nowrap"><?php echo JText::_( 'NUM' ); ?></th>
					<th width="20" nowrap="nowrap"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th width="1%" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'ID', 'p.id', @$lists['order_Dir'], @$lists['order'], 'list_checked' ); ?></th>
					<th nowrap="nowrap" class="title"><?php echo JHTML::_('grid.sort', 'Mã phiếu', 'vr.voucher_code', @$lists['order_Dir'], @$lists['order'], 'list_checked' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Giá trị', 'vr.voucher_value', @$lists['order_Dir'], @$lists['order'], 'list_checked' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Ngày nạp', 'vr.created', @$lists['order_Dir'], @$lists['order'], 'list_checked' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Nạp bởi', 'rc.username', @$lists['order_Dir'], @$lists['order'], 'list_checked' ); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $pageNav->getListFooter(); ?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0; 
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];
				$checked 	= JHTML::_('grid.checkedout', $row, $i );
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td><?php echo $pageNav->getRowOffset( $i ); ?></td>
					<td><?php echo $checked; ?></td>
					<td align="center"><?php echo $row->id; ?></td>
					<td>
						<?php echo $row->voucher_code; ?></a>
					</td>
					<td align="center"><?php echo number_format($row->voucher_value,0,',','.');?> đ</td>
					<td align="left"><?php echo $row->created;?></td>
					<td align="center"><?php echo $row->username;?></td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>
		</div>

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="list_checked" />
		<input type="hidden" name="section" value="manage_checked" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="" />
		</form>
		<?php
	
	}
}
?>
