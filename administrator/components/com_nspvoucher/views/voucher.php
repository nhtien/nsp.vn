<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Voucher {
	function show( &$rows, &$pageNav, $option, &$lists ) {
		
		$user =& JFactory::getUser();
		$ordering = ($lists['order'] == 'cr.id' || $lists['order'] == 'cr.name' || $lists['order'] == 'cr.ordering');
		//echo $lists['order'];
		JHTML::_('behavior.tooltip');
		$tmpl = JRequest::getVar('tmpl');
		$popup = ($tmpl=="component" ? true : false);
		?>
		<form action="?option=com_nspvoucher" method="post" name="adminForm">

		<table width="100%">
		<tr>
			<td align="left">
				<?php echo JText::_( 'Filter (Voucher)' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button></td>
			<td align="center" nowrap="nowrap">
				</td>
			<td align="center" nowrap="nowrap">
				</td>	
			<td align="right" nowrap="nowrap"><?php echo JText::_( 'State Filter' ); ?>:
				<?php
				echo $lists['state'];
				?></td>
		</tr>
		</table>

		<div id="tablecell">
			<table class="adminlist">
			<thead>
				<tr>
					<th width="5" nowrap="nowrap"><?php echo JText::_( 'NUM' ); ?></th>
					<th width="20" nowrap="nowrap"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
					<th width="1%" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'ID', 'cr.id', @$lists['order_Dir'], @$lists['order'], 'list_vouchers' ); ?></th>
					<th nowrap="nowrap" class="title"><?php echo JHTML::_('grid.sort', 'Mã số phiếu', 'v.code', @$lists['order_Dir'], @$lists['order'], 'list_vouchers' ); ?></th>
                    <th nowrap="nowrap" class="title"><?php echo JHTML::_('grid.sort', 'Giá trị phiếu', 'v.value', @$lists['order_Dir'], @$lists['order'], 'list_vouchers' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Số hóa đơn', 'category_name', @$lists['order_Dir'], @$lists['order'], 'list_vouchers' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Phiếu đã sử dụng', 'v.checked', @$lists['order_Dir'], @$lists['order'], 'list_vouchers' ); ?></th>
                    <th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Người nhập phiếu', 'author', @$lists['order_Dir'], @$lists['order'], 'list_vouchers' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Ngày nhập', 'v.created', @$lists['order_Dir'], @$lists['order'], 'list_vouchers' ); ?></th>
					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Published', 'v.published', @$lists['order_Dir'], @$lists['order'], 'list_vouchers' ); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $pageNav->getListFooter(); ?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0; 
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];

				$link 		= 'index.php?option=com_nspvoucher&task=edit_voucher&cid[]='. $row->id ;

				$checked 	= JHTML::_('grid.checkedout', $row, $i );
				$published 	= JHTML::_('grid.published', $row, $i );
				
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td><?php echo $pageNav->getRowOffset( $i ); ?></td>
					<td><?php echo $checked; ?></td>
					<td align="center"><?php echo $row->id; ?></td>
					<td>
						<a href="<?php echo JRoute::_( $link ); ?>" title="<?php echo JText::_( 'Edit' ); ?>"><?php echo $row->code; ?></a>
					</td>
                    <td align="center"><?php echo number_format($row->value,0,',','.');?></td>
					<td align="center"><?php echo $row->invoice_num;?></td>
                    <td align="center"><?php echo $row->checked ? '<img src="images/tick.png" border="0" />' : '';?></td>
					<td align="left"><?php echo $row->author;?></td>
					<td align="center"><?php echo date("d/m/Y",strtotime($row->created));?></td>
					<td align="center"><?php echo $published;?></td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>
		</div>

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="list_vouchers" />
		<input type="hidden" name="section" value="manage_voucher" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="" />
		</form>
		<?php
	}
	
	function edit( &$row, &$lists ) 
	{
		JHTML::_('behavior.modal', '.modal-button');
		global $option,$nspTraining_config;
		if (JRequest::getCmd('task') == 'add_voucher') {
			$row->published = '1';
			$row->description = '';
		}
		JRequest::setVar( 'hidemainmenu', 1 );

		$editor =& JFactory::getEditor();
		
		jimport('joomla.filter.output');
		JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES );
		
		// Build the component's submenu
		jimport('joomla.html.pane');
		$pane =& JPane::getInstance('sliders');
		JHTML::_('behavior.tooltip');
		JHTML::_('behavior.modal','a.modal-button');
		?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel_project') {
			submitform( pressbutton );
			return;
		}
		else if(pressbutton == 'save_project' || pressbutton == 'apply_project' ) {
			if(form.category_id.value == ''){
				alert('Select a category');
				return false;
			}else if( form.name.value==''){
				alert('input name');
				return false;
			}
			submitform( pressbutton );
		}else{
			submitform( pressbutton );
		}
	}
</script>
<form action="index.php" method="post" name="adminForm" enctype="multipart/form-data">
<table class="adminform">
	<tr>
		<td class="key">ID</td>
		<td><?php echo $row->id ; ?></td>
	</tr>
	<tr>
		<td class="key">Số hóa đơn</span></td>
		<td>
			<input type="text" name="invoice_num" value="<?php echo $row->invoice_num;?>" size="60" />
		</td>
	</tr>
	<tr>
		<td class="key"><?php echo JText :: _('Published'); ?></td>
		<td><?php echo JHTML::_( 'select.booleanlist', 'published', 'class="inputbox"', $row->published ); ?></td>
	</tr>
	<tr>
		<td class="key">Mã số phiếu</td>
		<td>
			<input type="text" name="code" value="<?php echo $row->code;?>" size="60" /> <a class="modal-button" href="" rel="{handler: 'iframe', size: {x: 300, y: 150}}" onclick="this.href='index.php?option=com_nspvoucher&task=generate&tmpl=component&src='+document.adminForm.invoice_num.value;"><button type="button" class="button">Generate</button></a>
		</td>
	</tr>
	<tr>
		<td class="key">Trị giá</td>
		<td>
			<?php echo $lists['values'] ?>
		</td>
	</tr>
	<tr>
		<td class="key">Mô tả</td>
		<td><textarea name="description" rows="5" cols="30"><?php echo $row->description;?></textarea></td>
	</tr>
	

</table>

		<input type="hidden" name="task" value="save_voucher" />
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
		<input type="hidden" name="cid[]" value="<?php echo $row->id; ?>" />
</form>
<script language="javascript">
function selectGenerateCode(code){document.adminForm.code.value=code;}
</script>
		<?php
	}
}
?>
