<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableVoucher_user_ref extends JTable {

	var $id				= null;
	var $voucher_id		= null; // mã số voucher
	var $voucher_code	= null; // số voucher
	var $voucher_value	= null;
	var $user_id		= null;
	var $username		= null;
	var $created		= null;
	 
	function __construct( &$_db ) {
		parent::__construct( '#__nspvoucher_user_ref', 'id', $_db );
	}

}
?>
