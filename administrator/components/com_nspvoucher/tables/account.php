<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableAccount extends JTable {

	var $user_id		= null;
	var $total_value	= null;
	 
	function __construct( &$_db ) {
		parent::__construct( '#__nspvoucher_account', 'user_id', $_db );
	}

}
?>
