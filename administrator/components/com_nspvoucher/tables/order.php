<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableOrder extends JTable {

	var $id				= null;
	var $user_id		= null;
	var $user_crvalue	= null; // số tiền hiện có trong tài khoản của thành viên
	var $total			= null; // tổng giá trị đơn hàng
	// thông tin khách hàng
	var $name			= null;
	var $email			= null;
	var $phone			= null;
	var $address		= null;
	var $company_name	= null;
	var $created		= null;
	var $status			= null;
	var $note			= null;
	 
	function __construct( &$_db ) {
		parent::__construct( '#__nspvoucher_orders', 'id', $_db );
	}

}
?>
