<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TablePromotion extends JTable {

	var $id				= null;
	var $title			= null;
	var $url			= null;
	var $short_desc		= null;
	var $description	= null;
	var $thumb			= null;
	var $created		= null;
	var $created_by		= null;
	var $published		= null;
	var $ordering		= null;
	 
	function __construct( &$_db ) {
		parent::__construct( '#__nspvoucher_promotions', 'id', $_db );
	}

}
?>
