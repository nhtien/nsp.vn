<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined('_JEXEC') or die();

class TableConfig
{

	var $meta_title				= null;
	var $meta_key				= null;
	var $meta_desc				= null;
	var $sendmail2admin			= null;
	var $sendmailto				= null;
	
	var $content_text 			= null; // nọi dung chuong trình

	function load()
	{
		$db = &JFactory::getDBO();
		
		$query = "SELECT name, value FROM #__nspvoucher_config";
		$db->setQuery($query);
		$db->query() or die($db->stderr());
		$rows = $db->loadObjectList();

		foreach ( $rows as $row ) {
			eval ('$config->' . $row->name . " = '" . $row->value . "';");
		}
		return $config;
	}
	
	function save( $data ){
		$db = &JFactory::getDBO();
		$object = get_object_vars( $this );
		$config = new stdClass();
		
		while( list( $k,$v) = each( $object ) ){
			$config->$k = $data[$k]; 
		}
		$query = array();
		while( list($k,$v)=each( $config) ){
			$query[] = "UPDATE #__nspvoucher_config SET value='".$v."' WHERE name='".$k."'";
		}
		
		foreach ($query as $query_string) {
			$db->setQuery($query_string);
			$db->query() or die($db->stderr());
		}
		return $config;
		
	}

	function check() {
		return true;
	}
}
?>