<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableVoucher extends JTable {

	var $id				= null;
	var $invoice_num	= null; // số hóa đơn
	var $code			= null;
	var $value			= null;
	var $created		= null;
	var $create_by		= null;
	var $description	= null;
	var $published		= null;
	var $checked		= null; // thẻ đã sử dụng
	 
	function __construct( &$_db ) {
		parent::__construct( '#__nspvoucher', 'id', $_db );
	}

}
?>
