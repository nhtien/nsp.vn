<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableOrder_product extends JTable {

	var $id				= null;
	var $order_id		= null;
	var $product_id		= null;
	var $product_name	= null;
	var $product_price	= null;
	var $quantity		= null;
	var $created		= null;

	 
	function __construct( &$_db ) {
		parent::__construct( '#__nspvoucher_order_products', 'id', $_db );
	}

}
?>
