<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( JApplicationHelper::getPath( 'toolbar_html' ) );

switch ($task) {
	case 'configuration':
		TOOLBAR_NSPVoucher::_CONFIGURATION();
		break;
/* voucher */
	case 'list_vouchers':
		TOOLBAR_NSPVoucher::_VOUCHERS_MANAGER();
		break;	
		
	case 'edit_voucher':
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		TOOLBAR_NSPVoucher::_EDIT_VOUCHER( $cid[0] );
		break;	
	case 'add_voucher'  :
		$id = JRequest::getVar( 'id', 0, '', 'int' );
		TOOLBAR_NSPVoucher::_EDIT_VOUCHER( $id );
		break;
/* Product */
	case 'list_products':
		TOOLBAR_NSPVoucher::_PRODUCTS_MANAGER();
		break;	
		
	case 'edit_product':
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		TOOLBAR_NSPVoucher::_EDIT_PRODUCT( $cid[0] );
		break;	
	case 'add_product'  :
		$id = JRequest::getVar( 'id', 0, '', 'int' );
		TOOLBAR_NSPVoucher::_EDIT_PRODUCT( $id );
		break;
/* Promotion */
	case 'list_promotions':
		TOOLBAR_NSPVoucher::_PROMOTION_MANAGER();
		break;	
		
	case 'edit_promotion':
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		TOOLBAR_NSPVoucher::_EDIT_PROMOTION( $cid[0] );
		break;	
	case 'add_promotion'  :
		$id = JRequest::getVar( 'id', 0, '', 'int' );
		TOOLBAR_NSPVoucher::_EDIT_PROMOTION( $id );
		break;
		
/* order */
	case 'list_orders':
		TOOLBAR_NSPVoucher::_ORDERS_MANAGER();
		break;	
		
	case 'edit_order':
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		TOOLBAR_NSPVoucher::_EDIT_ORDER( $cid[0] );
		break;	
	case 'add_product'  :
		$id = JRequest::getVar( 'id', 0, '', 'int' );
		TOOLBAR_NSPVoucher::_EDIT_ORDER( $id );
		break;
	default:
		TOOLBAR_NSPVoucher::_DEFAULT();
		break;
/* Checked */
	case 'list_checked':
		TOOLBAR_NSPVoucher::_LIST_CHECKED();
		break;	

}
?>