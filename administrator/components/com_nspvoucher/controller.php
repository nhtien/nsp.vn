<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined('_JEXEC') or die();

jimport( 'joomla.application.component.controller' );

$lang = & JFactory::getLanguage();
$lang->load('nspvoucher', JPATH_COMPONENT);

class NspVoucherController extends JController 
{

	function __construct( $default = array())
	{
		parent::__construct( $default );
		
		//some tasks about configuration
		$this->registerTask( 'apply_configuration', 'save_configuration' );
		
		//some tasks about voucher
		$this->registerTask( 'add_voucher' , 'edit_voucher' );
		$this->registerTask( 'apply_voucher', 'save_voucher');
		//some tasks about product
		$this->registerTask( 'add_product' , 'edit_product' );
		$this->registerTask( 'apply_product', 'save_product');
		//some tasks about promotion
		$this->registerTask( 'add_promotion' , 'edit_promotion' );
		$this->registerTask( 'apply_promotion', 'save_promotion');
		
		$this->registerTask( 'apply_order', 'save_order');
		//some tasks about main
		$this->registerTask( 'main', 'showMain' );
		//some tasks about publish and unpublish	
		$this->registerTask( 'publish', 'publish');
		$this->registerTask( 'unpublish', 'publish');	
		
	}
	function publish() {
		global $mainframe;

		$db 	=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid		= JRequest::getVar( 'cid', array(), '', 'array' );
		$publish	= ( $this->_task == 'publish' ? 1 : 0 );
		$option		= JRequest::getCmd( 'option', 'com_nspvoucher', '', 'string' );

		JArrayHelper::toInteger($cid);

		if (count( $cid ) < 1) {
			$action = $publish ? 'publish' : 'unpublish';
			JError::raiseError(500, JText::_( 'Select an item to '.$action, true ) );
		}
		$cids = implode( ',', $cid );

		switch (JRequest::getVar( 'section', '', 'post', 'string' )) {
			case 'manage_voucher':
				$query = 'UPDATE #__nspvoucher'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN ( '. $cids .' )'
				;
				$db->setQuery( $query );
				if (!$db->query()) {
					JError::raiseError(500, $db->getErrorMsg() );
				}

				$link = 'index.php?option=com_nspvoucher&task=list_vouchers';
				$this->setRedirect($link);
				break;
			case 'manage_product':
				$query = 'UPDATE #__nspvoucher_products'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN ( '. $cids .' )'
				;
				$db->setQuery( $query );
				if (!$db->query()) {
					JError::raiseError(500, $db->getErrorMsg() );
				}

				$link = 'index.php?option=com_nspvoucher&task=list_products';
				$this->setRedirect($link);
				break;
				
			case 'manage_promotion':
				$query = 'UPDATE #__nspvoucher_promotions'
				. ' SET published = ' . (int) $publish
				. ' WHERE id IN ( '. $cids .' )'
				;
				$db->setQuery( $query );
				if (!$db->query()) {
					JError::raiseError(500, $db->getErrorMsg() );
				}

				$link = 'index.php?option=com_nspvoucher&task=list_promotions';
				$this->setRedirect($link);
				break;
				
		}
	}

	function showMain() {
		require_once( JPATH_COMPONENT.DS.'views'.DS.'main.php' );
		Main::showMain();
	}
	
/* some function about configuaration */
	
	function configuration() {
		global $nspVoucher_config;
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'configuration.php' );
		Config::Configuration($nspVoucher_config, $lists);
	}
	
	function save_configuration() {
		global $option, $task, $mainframe;
		
		$post = JRequest::get( 'post' );
		$post['content_text'] = JRequest::getVar('content_text', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		$config = &JTable::getInstance('config','Table');
		$config->save( $post );
		switch ($this->_task) {
			case 'apply_configuration':
				$msg = JText::_( 'Configuration saved' );
				$link = 'index.php?option=com_nspvoucher&task=configuration';
			break;
			case 'save_configuration':
			default:
				$msg = JText::_( 'Configuration saved' );
				$link = 'index.php?option=com_nspvoucher&task=main';
			break;
		}
		$mainframe->redirect($link, $msg);
	}
	
	function cancel_configuration() {
		require_once( JPATH_COMPONENT.DS.'views'.DS.'main.php' );
		Main::showMain();
	}
	
/* some funtion about voucher */
	function list_vouchers() {
		global $mainframe, $option;

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order_voucher",		'filter_order',		'v.created',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir_voucher",	'filter_order_Dir',	'',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state_voucher",		'filter_state',		'',		'word' );
		$filter_voucher		= $mainframe->getUserStateFromRequest( "$option.filter_voucher_cate",		'filter_voucher',		'-1',		'string' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search_project",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'limitstart_voucher', 'limitstart', 0, 'int' );

		$where = array();

		if ( $filter_state )
		{
			if ( $filter_state == 'P' )
			{
				$where[] = 'v.published = 1';
			}
			else if ($filter_state == 'U' )
			{
				$where[] = 'v.published = 0';
			}
		}
		if ($search)
		{
			$where[] = '(LOWER(v.code) LIKE '.$db->Quote('%'.$search.'%').' OR LOWER(v.invoice_num) LIKE '.$db->Quote('%'.$search.'%') .')';
		}

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;

		$query = 'SELECT COUNT(v.id)'
		. ' FROM #__nspvoucher AS v'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult();

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT v.*, u.name AS author'
		. ' FROM #__nspvoucher AS v'
		. ' LEFT JOIN #__users AS u ON u.id = v.create_by'
		. $where
		. $orderby
		;
		; 
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$rows = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
		// state filter
		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;

		require_once( JPATH_COMPONENT.DS.'views'.DS.'voucher.php' );
		Voucher::show( $rows, $pageNav, $option, $lists );
	}
	function edit_voucher() {
		global $mosConfig_absolute_path;
		$db		=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid 	= JRequest::getVar( 'cid', array(0), '', 'array' );
		$id		= JRequest::getVar( 'id', $cid[0], '', 'int' );
		
		$option = JRequest::getCmd( 'option');
		$uid 	= (int) @$cid[0];

		$row =& JTable::getInstance('voucher', 'Table');
		$row->load( $uid );

		
		if ($row->publish_down == null ) {
			$row->publish_down =  JText::_('Never');
		}
		
		$voption[] = JHTML::_('select.option','100000','100.000 đ');
		$voption[] = JHTML::_('select.option','200000','200.000 đ');
		$voption[] = JHTML::_('select.option','300000','300.000 đ');
		$voption[] = JHTML::_('select.option','500000','500.000 đ');
		
		$lists['values']	= JHTML::_('select.genericList', $voption, 'value','class="inputbox"','value','text',  $row->value );
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'voucher.php' );
		Voucher::edit( $row, $lists );
	}

	function save_voucher() 
	{
		global $mainframe,$nspVoucher_config, $option; 
		$db =& JFactory::getDBO();
		$user = &JFactory::getUser();
		$row =& JTable::getInstance('voucher', 'Table');
		$post = JRequest::get( 'post' );
		$link = 'index.php?option=com_nspvoucher&task=edit_voucher&cid[]='. $post['id'] .'';
		$post['description']	= JRequest::getVar('description', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		$post['code'] 			= strtoupper(JRequest::getVar('code', '', 'POST', 'string', JREQUEST_ALLOWRAW));
		$post['invoice_num'] 	= JRequest::getVar('invoice_num', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		
		$post['code'] = str_replace(' ','-',$post['code']);
		
		if( $post['code'] == '' || $post['value'] == ''){
			$this->setRedirect( $link , "Please Input Data!");
			return;
		}
		if( !$post['id'] ){
			$post['created'] = date("Y-m-d H:i:s");
			$post['create_by'] = $user->id;
		}
		
		
		$sql = 'SELECT * FROM #__nspvoucher where (code = '.$db->Quote($post['code']).' OR invoice_num = '.$db->Quote($post['invoice_num']).') AND id <> '.$post['id'];
		$db->setQuery($sql);
		$row2 = $db->loadObject();
		if($row2){
			$this->setRedirect( $link , "Mã số phiếu hoặc số hóa đơn này đã tồn tại, vui lòng nhập vào số khác", 'error');
			return;
		}
		
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		$isNew = ($row->id == 0);
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}

		switch ($this->_task)
		{
			case 'apply_voucher':
				$msg = JText::_( 'Voucher saved' );
				$link = 'index.php?option=com_nspvoucher&task=edit_voucher&cid[]='. $row->id .'';
				break;

			case 'save_voucher':
			default:
				$msg = JText::_( 'Vocher saved' );
				$link = 'index.php?option=com_nspvoucher&task=list_vouchers';
				break;
		}
		
		$mainframe->redirect($link, $msg);
	}

	function remove_voucher() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );
		JArrayHelper::toInteger($cid);
		for ($i=0, $n=count($cid); $i < $n; $i++) {
			$row =& JTable::getInstance('voucher', 'Table');
			$row->load( $cid[$i] );
			$row->delete();
		}
		$msg .= JText::_( 'Voucher(s) was deleted' );
		$mainframe->redirect('index.php?option=com_nspvoucher&task=list_vouchers', $msg);
	}

	function cancel_voucher() {
		global $option;
		$this->setRedirect( 'index.php?option='. $option .'&task=list_vouchers' );
	}
	
/* Danh sách các voucher đã sử dụng */
	function list_checked() {
		global $mainframe, $option;

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order_checked",		'filter_order',		'vr.created',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir_checked",	'filter_order_Dir',	'DESC',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state_checked",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search_checked",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit_regcourse', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'limitstart_regcourse', 'limitstart', 0, 'int' );

		$where = array();

		if ($search)
		{
			$where[] = 'LOWER(vr.voucher_code) LIKE '.$db->Quote('%'.$search.'%');
		}

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;

		$query = 'SELECT COUNT(vr.id)'
		. ' FROM #__nspvoucher_user_ref AS vr'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult(); 

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT vr.*'
		. ' FROM #__nspvoucher_user_ref AS vr'
		. ' LEFT JOIN #__users AS u ON u.id = vr.user_id'
		. $where
		. ' GROUP BY vr.id'
		. $orderby
		;
		; 
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit ); 
		$rows = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
	
		// state filter
		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;

		require_once( JPATH_COMPONENT.DS.'views'.DS.'checked.php' );
		VChecked::show( $rows, $pageNav, $option, $lists );
	}
	
/* Danh sách các đơn hàng */
	function list_orders() {
		global $mainframe, $option;

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order_orders",		'filter_order',		'o.created',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir_orders",	'filter_order_Dir',	'DESC',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state_orders",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search_orders",			'search',			'',		'string' );
		$status				= $mainframe->getUserStateFromRequest( "$option.filter_status",			'status',			'',		'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit_orders', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'limitstart_orders', 'limitstart', 0, 'int' );

		$where = array();

		if ($search)
		{
			$where[] = 'LOWER(o.name) LIKE '.$db->Quote('%'.$search.'%');
		}
		if ($status == '0' || $status)
		{
			$where[] = 'o.status='.$status;
		}

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;

		$query = 'SELECT COUNT(o.id)'
		. ' FROM #__nspvoucher_orders AS o'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult(); 

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT o.*'
		. ' FROM #__nspvoucher_orders AS o'
		. ' LEFT JOIN #__users AS u ON u.id = o.user_id'
		. $where
		. ' GROUP BY o.id'
		. $orderby
		;
		; 
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit ); 
		$rows = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
		require(JPATH_COMPONENT.DS.'common.php');
		$option = array();
		$option[] = JHTML::_('select.option','','- Trạng thái đơn hàng -');
		while(list($k,$v) = each($array_order_status)){
			$option[] = JHTML::_('select.option',$k,$v);
		}
		$lists['order_status'] = JHTML::_('select.genericlist', $option, 'status', 'class="inputbox" onchange="this.form.submit();"','value','text',$status);
	
		// state filter
		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;

		require_once( JPATH_COMPONENT.DS.'views'.DS.'order.php' );
		Order::show( $rows, $pageNav, $option, $lists );
	}
	
	function edit_order() {
		global $mosConfig_absolute_path;
		$db		=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid 	= JRequest::getVar( 'cid', array(0), '', 'array' );
		$id		= JRequest::getVar( 'id', $cid[0], '', 'int' );
		
		$option = JRequest::getCmd( 'option');
		$uid 	= (int) @$cid[0];

		$row =& JTable::getInstance('order', 'Table');
		$row->load( $uid );
		
		$sql = 'SELECT * FROM #__nspvoucher_order_products where order_id = '.$row->id;
		$db->setQuery($sql);
		$lists['products'] = $db->loadObjectList();
		
		require(JPATH_COMPONENT.DS.'common.php');
		$option = array();
		while(list($k,$v) = each($array_order_status)){
			$option[] = JHTML::_('select.option',$k,$v);
		}
		$lists['order_status'] = JHTML::_('select.genericlist', $option, 'status', 'class="inputbox"','value','text',$row->status);

		
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'order.php' );
		Order::edit( $row, $lists );
	}
	function save_order() 
	{
		global $mainframe,$nspVoucher_config, $option; 
		$db =& JFactory::getDBO();
		$user = &JFactory::getUser();
		$row =& JTable::getInstance('order', 'Table');
		$post = JRequest::get( 'post' );
		$link = 'index.php?option=com_nspvoucher&task=edit_order&cid[]='. $post['id'] .'';

		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		$isNew = ($row->id == 0);
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}

		switch ($this->_task)
		{
			case 'apply_order':
				$msg = JText::_( 'Order saved' );
				$link = 'index.php?option=com_nspvoucher&task=edit_order&cid[]='. $row->id .'';
				break;

			case 'save_order':
			default:
				$msg = JText::_( 'Order saved' );
				$link = 'index.php?option=com_nspvoucher&task=list_orders';
				break;
		}
		
		$mainframe->redirect($link, $msg);
	}
	function cancel_order() {
		global $option;
		$this->setRedirect( 'index.php?option='. $option .'&task=list_orders' );
	}
	
/* some funtion about products */
	function list_products() {
		global $mainframe, $option;

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order_products",		'filter_order',		'p.created',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir_products",	'filter_order_Dir',	'DESC',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state_products",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search_product",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'limitstart_product', 'limitstart', 0, 'int' );

		$where = array();

		if ( $filter_state )
		{
			if ( $filter_state == 'P' )
			{
				$where[] = 'p.published = 1';
			}
			else if ($filter_state == 'U' )
			{
				$where[] = 'p.published = 0';
			}
		}
		if ($search)
		{
			$where[] = 'LOWER(p.name) LIKE '.$db->Quote('%'.$search.'%');
		}

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;

		$query = 'SELECT COUNT(cr.id)'
		. ' FROM #__nspvoucher_products AS p'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult();

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT p.*, u.name AS author'
		. ' FROM #__nspvoucher_products AS p'
		. ' LEFT JOIN #__users AS u ON u.id = p.created_by'
		. $where
		. $orderby
		;
		; 
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$rows = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
		// state filter
		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;

		require_once( JPATH_COMPONENT.DS.'views'.DS.'product.php' );
		Product::show( $rows, $pageNav, $option, $lists );
	}
	function edit_product() {
		global $mosConfig_absolute_path;
		$db		=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid 	= JRequest::getVar( 'cid', array(0), '', 'array' );
		$id		= JRequest::getVar( 'id', $cid[0], '', 'int' );
		
		$option = JRequest::getCmd( 'option');
		$uid 	= (int) @$cid[0];

		$row =& JTable::getInstance('product', 'Table');
		$row->load( $uid );

		
		if ($row->publish_down == null ) {
			$row->publish_down =  JText::_('Never');
		} 	
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'product.php' );
		Product::edit( $row, $lists );
	}

	function save_product() 
	{
		global $mainframe, $nspVoucher_config, $option; 
		$db =& JFactory::getDBO();
		$user = &JFactory::getUser();
		$row =& JTable::getInstance('product', 'Table');
		$post = JRequest::get( 'post' );
		$link = 'index.php?option=com_nspvoucher&task=edit_product&cid[]='. $post['id'] .'';
		$post['description']= JRequest::getVar('description', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		$post['name'] = JRequest::getVar('name', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		
		if( $post['name'] == ''){
			$this->setRedirect( $link , "Please Input Data!");
			return;
		}
		
		/* upload thumbnail */		
		jimport("joomla.filesystem.file");
		$thumbnail = JRequest::getVar('thumbnail','','files','array'); 
		$filename =  $thumbnail['name'];
		$ext	= strtolower(JFile::getExt( $filename ) );
		$fillter = array("png","gif","jpg");
		if( $filename && !in_array($ext, $fillter) ){
			$mainframe->redirect( $link, JText::_('File is not support') );
			return;
		}else if( $filename ){ 
			$dir_file = JPATH_SITE .DS."documents".DS.'nspvoucher'.DS.'products';
			$tmp_dest 	 = $dir_file.DS.$filename;
			$tmp_src 	 = $thumbnail['tmp_name'];
			if(file_exists($tmp_dest)){
				$name = JFile::stripExt( $filename );
				$filename = uniqid(substr( $name,0,40)."_" ).".".$ext;
				$tmp_dest 	= $dir_file.DS.$filename;
			}
			//JFile::upload($tmp_src, $tmp_dest);
			$post['fullimage'] = $filename;
			$post['thumb'] = 't_'.$filename;
			createImageAndThumb($tmp_src, $filename, 't_'.$filename, 400, 400, 120, 120, '', $dir_file, $filename);
			//@unlink($tmp_dest);
			//var_dump($tmp_dest);exit;
			/* remove old full file */
			if($post['id']){
				$row2 =& JTable::getInstance('product', 'Table');
				$row2->load( $post['id']); 
				if( $row2->id && $row2->fullimage){
					@unlink(  $dir_file.DS.$row2->fullimage );
					@unlink(  $dir_file.DS.$row2->thumb );
				}
			}
		}
		/* end upload */
		
		if( !$post['id'] ){
			$post['created'] = date("Y-m-d H:i:s");
			$post['created_by'] = $user->id;
		}//var_dump($row);exit;
		
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		$isNew = ($row->id == 0);
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}

		switch ($this->_task)
		{
			case 'apply_product':
				$msg = JText::_( 'Product saved' );
				$link = 'index.php?option=com_nspvoucher&task=edit_product&cid[]='. $row->id .'';
				break;

			case 'save_product':
			default:
				$msg = JText::_( 'Product saved' );
				$link = 'index.php?option=com_nspvoucher&task=list_products';
				$this->reorder_product();
				break;
		}
		
		$mainframe->redirect($link, $msg);
	}

	function reorder_product() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$this->setRedirect( 'index.php?option=com_nspvoucher&task=list_products' );

		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$task	= $this->getTask();
		$inc	= ($task == 'orderup_product' ? -1 : 1);

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		$row =& JTable::getInstance('product', 'Table');
		$row->load( (int) $cid[0] );
		$row->move( $inc );
		$row->reorder();
	}
	
	function save_product_order() {
		global $mainframe;
		$db			= & JFactory::getDBO();
		$cid		= JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$order		= JRequest::getVar( 'order', array (0), 'post', 'array' );
		$total		= count( $cid );
		
		JArrayHelper::toInteger($cid, array(0));
		JArrayHelper::toInteger($order, array(0));

		$row =& JTable::getInstance('product', 'Table');
		for( $i=0; $i < $total; $i++ ) {
			$row->load( (int) $cid[$i] );
			if ($row->ordering != $order[$i]) $row->ordering = $order[$i];
			$row->store();
		}
		$row->reorder();
		
		$link = 'index.php?option=com_nspvoucher&task=list_products';
		$this->setRedirect($link);
	}
	
	function remove_product() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );
		JArrayHelper::toInteger($cid);
		$dir_file = JPATH_SITE .DS."documents".DS.'nspvoucher'.DS.'products';
		for ($i=0, $n=count($cid); $i < $n; $i++) {
			$row =& JTable::getInstance('product', 'Table');
			$row->load( $cid[$i] );
			if($row->thumb){
				@unlink(  $dir_file.DS.$row->fullimage );
				@unlink(  $dir_file.DS.$row->thumb );
			}
			$row->delete();
		}
		$msg .= JText::_( 'Product(s) was deleted' );
		$mainframe->redirect('index.php?option=com_nspvoucher&task=list_products', $msg);
	}

	function cancel_product() {
		global $option;
		$this->setRedirect( 'index.php?option='. $option .'&task=list_products' );
	}
	
/* some funtion about PROMOTIONS */
	function list_promotions() {
		global $mainframe, $option;

		$db					=& JFactory::getDBO();
		$filter_order		= $mainframe->getUserStateFromRequest( "$option.filter_order_promotions",		'filter_order',		'p.created',	'cmd' );
		$filter_order_Dir	= $mainframe->getUserStateFromRequest( "$option.filter_order_Dir_promotions",	'filter_order_Dir',	'DESC',		'word' );
		$filter_state		= $mainframe->getUserStateFromRequest( "$option.filter_state_promotions",		'filter_state',		'',		'word' );
		$search				= $mainframe->getUserStateFromRequest( "$option.search_promotion",			'search',			'',		'string' );
		$search				= JString::strtolower( $search );

		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $mainframe->getUserStateFromRequest( $option.'limitstart_promotion', 'limitstart', 0, 'int' );

		$where = array();

		if ( $filter_state )
		{
			if ( $filter_state == 'P' )
			{
				$where[] = 'p.published = 1';
			}
			else if ($filter_state == 'U' )
			{
				$where[] = 'p.published = 0';
			}
		}
		if ($search)
		{
			$where[] = 'LOWER(p.name) LIKE '.$db->Quote('%'.$search.'%');
		}

		$where 		= ( count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '' );
		$orderby 	= ' ORDER BY '. $filter_order .' '. $filter_order_Dir;

		$query = 'SELECT COUNT(p.id)'
		. ' FROM #__nspvoucher_promotions AS p'
		. $where
		;
		$db->setQuery( $query );
		$total = $db->loadResult();

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit );

		$query = 'SELECT p.*, u.name AS author'
		. ' FROM #__nspvoucher_promotions AS p'
		. ' LEFT JOIN #__users AS u ON u.id = p.created_by'
		. $where
		. $orderby
		;
		; 
		$db->setQuery( $query, $pageNav->limitstart, $pageNav->limit );
		$rows = $db->loadObjectList();
		
		if ($db->getErrorNum())
		{
			echo $db->stderr();
			return false;
		}
		
		// state filter
		$lists['state']	= JHTML::_('grid.state',  $filter_state );

		// table ordering
		$lists['order_Dir']	= $filter_order_Dir;
		$lists['order']		= $filter_order;

		// search filter
		$lists['search']= $search;

		require_once( JPATH_COMPONENT.DS.'views'.DS.'promotion.php' );
		Promotion::show( $rows, $pageNav, $option, $lists );
	}
	function edit_promotion() {
		global $mosConfig_absolute_path;
		$db		=& JFactory::getDBO();
		$user 	=& JFactory::getUser();

		$cid 	= JRequest::getVar( 'cid', array(0), '', 'array' );
		$id		= JRequest::getVar( 'id', $cid[0], '', 'int' );
		
		$option = JRequest::getCmd( 'option');
		$uid 	= (int) @$cid[0];

		$row =& JTable::getInstance('promotion', 'Table');
		$row->load( $uid );

		
		if ($row->publish_down == null ) {
			$row->publish_down =  JText::_('Never');
		} 	
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'promotion.php' );
		Promotion::edit( $row, $lists );
	}

	function save_promotion() 
	{
		global $mainframe, $nspVoucher_config, $option; 
		$db =& JFactory::getDBO();
		$user = &JFactory::getUser();
		$row =& JTable::getInstance('promotion', 'Table');
		$post = JRequest::get( 'post' );
		$link = 'index.php?option=com_nspvoucher&task=edit_promotion&cid[]='. $post['id'] .'';
		$post['description']= JRequest::getVar('description', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		$post['title'] = JRequest::getVar('title', '', 'POST', 'string', JREQUEST_ALLOWRAW);
		
		if( $post['title'] == ''){
			$this->setRedirect( $link , "Please Input Data!");
			return;
		}
		
		/* upload thumbnail */		
		jimport("joomla.filesystem.file");
		$thumbnail = JRequest::getVar('thumbnail','','files','array'); 
		$filename =  $thumbnail['name'];
		$ext	= strtolower(JFile::getExt( $filename ) );
		$fillter = array("png","gif","jpg");
		if( $filename && !in_array($ext, $fillter) ){
			$mainframe->redirect( $link, JText::_('File is not support') );
			return;
		}else if( $filename ){ 
			$dir_file = JPATH_SITE .DS."documents".DS.'nspvoucher'.DS.'promotions';
			$tmp_dest 	 = $dir_file.DS.$filename;
			$tmp_src 	 = $thumbnail['tmp_name'];
			if(file_exists($tmp_dest)){
				$name = JFile::stripExt( $filename );
				$filename = uniqid(substr( $name,0,40)."_" ).".".$ext;
				$tmp_dest 	= $dir_file.DS.$filename;
			}
			JFile::upload($tmp_src, $tmp_dest);
			$post['thumb'] = $filename;
			/* remove old full file */
			if($post['id']){
				$row2 =& JTable::getInstance('promotion', 'Table');
				$row2->load( $post['id']); 
				if( $row2->id && $row2->thumb){
					@unlink(  $dir_file.DS.$row2->thumb );
				}
			}
		}
		/* end upload */
		
		if( !$post['id'] ){
			$post['created'] = date("Y-m-d H:i:s");
			$post['created_by'] = $user->id;
		}//var_dump($row);exit;
		
		if (!$row->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		$isNew = ($row->id == 0);
		if (!$row->check()) {
			JError::raiseError(500, $row->getError() );
		}
		if (!$row->store()) {
			JError::raiseError(500, $row->getError() );
		}

		switch ($this->_task)
		{
			case 'apply_promotion':
				$msg = JText::_( 'Promotion saved' );
				$link = 'index.php?option=com_nspvoucher&task=edit_promotion&cid[]='. $row->id .'';
				break;

			case 'save_promotion':
			default:
				$msg = JText::_( 'Promotion saved' );
				$link = 'index.php?option=com_nspvoucher&task=list_promotions';
				$this->reorder_promotion();
				break;
		}
		
		$mainframe->redirect($link, $msg);
	}

	function reorder_promotion() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$this->setRedirect( 'index.php?option=com_nspvoucher&task=list_promotions' );

		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$task	= $this->getTask();
		$inc	= ($task == 'orderup_promotion' ? -1 : 1);

		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}

		$row =& JTable::getInstance('promotion', 'Table');
		$row->load( (int) $cid[0] );
		$row->move( $inc );
		$row->reorder();
	}
	
	function save_promotion_order() {
		global $mainframe;
		$db			= & JFactory::getDBO();
		$cid		= JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$order		= JRequest::getVar( 'order', array (0), 'post', 'array' );
		$total		= count( $cid );
		
		JArrayHelper::toInteger($cid, array(0));
		JArrayHelper::toInteger($order, array(0));

		$row =& JTable::getInstance('promotion', 'Table');
		for( $i=0; $i < $total; $i++ ) {
			$row->load( (int) $cid[$i] );
			if ($row->ordering != $order[$i]) $row->ordering = $order[$i];
			$row->store();
		}
		$row->reorder();
		
		$link = 'index.php?option=com_nspvoucher&task=list_promotions';
		$this->setRedirect($link);
	}
	
	function remove_promotion() {
		global $mainframe;
		$db		=& JFactory::getDBO();
		$cid	= JRequest::getVar( 'cid', array(), '', 'array' );
		JArrayHelper::toInteger($cid);
		$dir_file = JPATH_SITE .DS."documents".DS.'nspvoucher'.DS.'promotions';
		for ($i=0, $n=count($cid); $i < $n; $i++) {
			$row =& JTable::getInstance('promotion', 'Table');
			$row->load( $cid[$i] );
			if($row->thumb){
				@unlink(  $dir_file.DS.$row->fullimage );
				@unlink(  $dir_file.DS.$row->thumb );
			}
			$row->delete();
		}
		$msg .= JText::_( 'Product(s) was deleted' );
		$mainframe->redirect('index.php?option=com_nspvoucher&task=list_promotions', $msg);
	}

	function cancel_promotion() {
		global $option;
		$this->setRedirect( 'index.php?option='. $option .'&task=list_promotions' );
	}

// END PROMOTION	
	function generate(){
		$code = JRequest::getVar('src');
		$code = md5($code);
		$code = strtoupper(substr($code, 0,7));
		require(JPATH_COMPONENT.DS.'helpers'.DS.'generate.php');
		
	}

}

?>