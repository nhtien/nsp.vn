<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
$db =& JFactory::getDBO();
$sql = "SELECT id FROM #__components WHERE name = 'flippingcomic'";
$db->setQuery($sql);
$comid = $db->loadObject();

$query="CREATE TABLE `#__flippingcomic_books` (
  `id` int(11) NOT NULL auto_increment,
  `category_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `alias` text NOT NULL,
  `description` text NOT NULL,
  `preview_image` varchar(255) NOT NULL,
  `background_image` varchar(255) NOT NULL,
  `flash_width` int(4) NOT NULL,
  `flash_height` int(4) NOT NULL,
  `book_width` int(4) NOT NULL,
  `book_height` int(4) NOT NULL,
  `page_background_color` varchar(10) NOT NULL,
  `rotation` varchar(4) NOT NULL,
  `flip_area` int(4) NOT NULL,
  `static_shadows_depth` int(3) NOT NULL,
  `dynamic_shadows_depth` int(3) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(6) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_by_alias` varchar(225) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `reader_numbers` int(11) NOT NULL,
  `access` int(1) NOT NULL,
  `background_color` varchar(10) NOT NULL,
  `always_opened` tinyint(1) NOT NULL,
  `first_page` int(4) NOT NULL,
  `scale_content` tinyint(1) NOT NULL,
  `show_navigation` tinyint(1) NOT NULL,
  `show_description` tinyint(1) NOT NULL,
  `show_book_title` tinyint(1) NOT NULL,
  `show_back_button` tinyint(1) NOT NULL,
  `show_pages_list` tinyint(1) NOT NULL,
  `show_pages_description` tinyint(1) NOT NULL,
  `show_slide_show_button` tinyint(4) NOT NULL,
  `slide_show` tinyint(1) NOT NULL,
  `slide_show_display_duration` int(8) NOT NULL,
  `slide_show_loop` tinyint(1) NOT NULL,
  `page_flip_delay` int(6) NOT NULL,
  `flip_corner_on_load` tinyint(1) NOT NULL,
  `open_book_in` int(4) NOT NULL,
  `new_window_height` int(4) NOT NULL,
  `new_window_width` int(4) NOT NULL,
  `zoom_image_size_autodetect` tinyint(1) NOT NULL default '0',
  `checked_out_time` int(11) NOT NULL default '0',
  `checked_out` int(11) NOT NULL,
  `emailIcon` tinyint(1) NOT NULL,
  `printIcon` tinyint(1) NOT NULL,
  `pdf_file` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;";
$db->setQuery( $query );
$db->query();

$query="CREATE TABLE  `#__flippingcomic_voucher` (
  `id` int(9) NOT NULL auto_increment,
  `category_name` varchar(255) NOT NULL,
  `preview_image` varchar(255) NOT NULL,
  `description` text character set utf8 collate utf8_bin NOT NULL,
  `published` tinyint(1) NOT NULL default '1',
  `ordering` int(6) NOT NULL default '0',
  `checked_out_time` int(11) NOT NULL,
  `checked_out` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;";
$db->setQuery( $query );
$db->query();

$query="CREATE TABLE `#__flippingcomic_chapters` (
  `id` int(11) NOT NULL auto_increment,
  `book_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `chapter_name` text NOT NULL,
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` int(6) NOT NULL,
  `checked_out_time` int(11) NOT NULL,
  `checked_out` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;";
$db->setQuery( $query );
$db->query();

$query="CREATE TABLE `#__flippingcomic_config` (
  `id` int(9) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0";
$db->setQuery( $query );
$db->query();
$query = " INSERT INTO `#__flippingcomic_config` (`id`, `name`, `value`, `type`) VALUES
	(1, 'flipOnClick', '0', ''),
	(2, 'moveSpeed', '0', ''),
	(3, 'closeSpeed', '0', ''),
	(4, 'flipSound', 'components/com_flippingcomic/sounds/glossy_magazine.mp3', ''),
	(5, 'loadOnDemand', '0', ''),
	(6, 'cachePages', '0', ''),
	(7, 'cacheSize', '0', ''),
	(8, 'preloaderType', 'None', ''),
	(9, 'book_list_title', '', ''),
	(10, 'bookListStyle', 'images_title', ''),
	(11, 'style', 'images', ''),
	(12, 'columns', '1', ''),
	(13, 'allowPagesUnload', '1', ''),
	(14, 'flashCookie', '0', ''),
	(15, 'gotoSpeed', '0', ''),
	(16, 'showUnderlyingPages', '0', ''),
	(17, 'printIcon', '1', ''),
	(18, 'emailIcon', '1', '');";
$db->setQuery( $query );
$db->query();


$query="CREATE TABLE `#__flippingcomic_pages` (
  `id` int(11) NOT NULL auto_increment,
  `file` varchar(255) NOT NULL,
  `chapter_id` int(4) NOT NULL,
  `book_id` int(4) NOT NULL default '0',
  `category_id` int(4) NOT NULL default '0',
  `description` text NOT NULL,
  `ordering` int(11) NOT NULL default '0',
  `published` tinyint(1) NOT NULL default '1',
  `link_url` text NOT NULL,
  `link_url_target` int(1) NOT NULL default '4',
  `link_window_height` int(4) NOT NULL default '480',
  `link_window_width` int(4) NOT NULL default '640',
  `zoom_url` text NOT NULL,
  `zoom_url_target` int(1) NOT NULL default '3',
  `zoom_window_height` int(4) NOT NULL default '480',
  `zoom_window_width` int(4) NOT NULL default '640',
  `checked_out` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;";
$db->setQuery( $query );
$db->query();

$query="CREATE TABLE `#__flippingcomic_readers` (
  `id` int(4) NOT NULL auto_increment,
  `user_id` int(4) NOT NULL,
  `book_id` int(4) NOT NULL,
  `reader_numbers` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;";
$db->setQuery( $query );
$db->query();

$sql ="INSERT INTO `#__components` VALUES 
	(NULL,'Configuration', '',0,$comid->id, 'option=com_flippingcomic&task=configuration', 'Configuration', 'com_flippingcomic',0,'',0,'', 1),
	(NULL,'Categories Manager', '',0,$comid->id, 'option=com_flippingcomic&task=manage_categories', 'Categories Manager', 'com_flippingcomic', 1,'',0,'', 1),
	(NULL,'Book manager', '',0,$comid->id, 'option=com_flippingcomic&task=book_manager', 'Book manager', 'com_flippingcomic', 2,'',0,'', 1),
	(NULL,'chapters Manager', '',0,$comid->id, 'option=com_flippingcomic&task=manage_chapters', 'chapters Manager', 'com_flippingcomic', 3,'',0,'', 1),
	(NULL,'page Manager', '',0,$comid->id, 'option=com_flippingcomic&task=page_manager', 'page Manager', 'com_flippingcomic', 4,'',0,'', 1) ,
	(NULL,'File manager', '',0,$comid->id, 'option=com_flippingcomic&task=file_manager', 'File manager', 'com_flippingcomic', 5,'',0,'', 1) ,
	(NULL,'batch add pages', '',0,$comid->id, 'option=com_flippingcomic&task=batch_add_pages', 'batch_add_pages', 'com_flippingcomic', 6,'',0,'', 1);";
$db->setQuery( $sql );
$db->query();

if (!JFile::exists(JPATH_SITE.'/images/flippingcomic/none_book.png'))
{

//Extract images
		$archivename = JPATH_SITE.DS.'components'.DS.'com_flippingcomic'.DS.'flippingcomic.zip';

		// Temporary folder to extract the archive into
		$tmpdir = JPATH_SITE.DS.'images'.DS;

		// Clean the paths to use for archive extraction
		$extractdir = JPath::clean($tmpdir);
		$archivename = JPath::clean($archivename);

		// do the unpacking of the archive
		$result = JArchive::extract( $archivename, $extractdir);

		if ( $result === false ) {
		echo $archivename."<br>";
		echo $extractdir."<br>";
		echo "error extract";
		die();
		}


$imagesdir=JPATH_SITE.DS.'images'.DS.'flippingcomic';
if (!$ftp['enabled'] && JPath::isOwner($imagesdir) && !JPath::setPermissions($imagesdir, '0777','0777')) {
			JError::raiseNotice('SOME_ERROR_CODE', 'Could not make media images dir writable');
}

if (!$ftp['enabled'] && JPath::isOwner($imagesdir.DS.'fippingcomic') && !JPath::setPermissions($imagesdir.DS.'fippingcomic', '0777','0777')) {
			JError::raiseNotice('SOME_ERROR_CODE', 'Could not make avatars dir writable');
}


}

?>