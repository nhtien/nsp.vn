<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class TOOLBAR_NSPVoucher {

	function _CONFIGURATION() {
		JToolBarHelper::title(  JText::_( 'NSP Voucher' ) .': <small> : '.JText::_( 'Configuration' ).'</small>' );
		JToolBarHelper::save( 'save_configuration', 'Save' );
		JToolBarHelper::apply( 'apply_configuration', 'Apply' );
		JToolBarHelper::cancel( 'cancel_configuration', 'Cancel' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

/* voucher */
	function _VOUCHERS_MANAGER() {
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));
		JToolBarHelper::title(  JText::_( 'NSP Voucher') .': <small> : '.JText::_( 'Voucher Manager' ).'</small>' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList( '', 'remove_voucher', 'Delete' );
		JToolBarHelper::editListX( 'edit_voucher', 'Edit' );
		JToolBarHelper::addNewX( 'add_voucher', 'New' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

	function _EDIT_VOUCHER( $categoryid ) {
			$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
			JArrayHelper::toInteger($cid, array(0));
	
			$text = ( $cid[0] ? JText::_( 'Edit Voucher' ) : JText::_( 'New Voucher' ) );
	
			JToolBarHelper::title(  JText::_( 'NSP Voucher' ).': <small> : '.JText::_( 'Voucher Manager' ).' : <small>[ ' . $text.' ]</small></small>' );
			JToolBarHelper::save( 'save_voucher', 'Save' );
			JToolBarHelper::apply( 'apply_voucher', 'Apply' );
			if ($cid[0]) {
				// for existing items the button is renamed `close`
				JToolBarHelper::cancel( 'cancel_voucher', 'Close' );
			} else {
				JToolBarHelper::cancel( 'cancel_voucher', 'Cancel' );
			}
			JToolBarHelper::help( '../help.html', true );
		}
/* product */
	function _PRODUCTS_MANAGER() {
		JToolBarHelper::title(  JText::_( 'NSP Voucher') .': <small> : '.JText::_( 'Product Manager' ).'</small>' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList( '', 'remove_product', 'Delete' );
		JToolBarHelper::editListX( 'edit_product', 'Edit' );
		JToolBarHelper::addNewX( 'add_product', 'New' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

	function _EDIT_PRODUCT( $bookid ) {
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$text = ( $cid[0] ? JText::_( 'Edit Product' ) : JText::_( 'New Product' ) );

		JToolBarHelper::title(  JText::_( 'NSP Voucher' ).': <small> : '.JText::_( 'Product Manager' ).' : <small>[ ' . $text.' ]</small></small>' );
		JToolBarHelper::save( 'save_product', 'Save' );
		JToolBarHelper::apply( 'apply_product', 'Apply' );
		if ($cid[0]) {
			// for existing items the button is renamed `close`
			JToolBarHelper::cancel( 'cancel_product', 'Close' );
		} else {
			JToolBarHelper::cancel( 'cancel_product', 'Cancel' );
		}
		JToolBarHelper::help( '../help.html', true );
	}
/* PROMTION */
	function _PROMOTION_MANAGER() {
		JToolBarHelper::title(  JText::_( 'NSP Voucher') .': <small> : '.JText::_( 'Promotion Manager' ).'</small>' );
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList( '', 'remove_promotion', 'Delete' );
		JToolBarHelper::editListX( 'edit_promotion', 'Edit' );
		JToolBarHelper::addNewX( 'add_promotion', 'New' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

	function _EDIT_PROMOTION( $bookid ) {
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$text = ( $cid[0] ? JText::_( 'Edit Promotion' ) : JText::_( 'New Promotion' ) );

		JToolBarHelper::title(  JText::_( 'NSP Voucher' ).': <small> : '.JText::_( 'Promotion Manager' ).' : <small>[ ' . $text.' ]</small></small>' );
		JToolBarHelper::save( 'save_promotion', 'Save' );
		JToolBarHelper::apply( 'apply_promotion', 'Apply' );
		if ($cid[0]) {
			// for existing items the button is renamed `close`
			JToolBarHelper::cancel( 'cancel_promotion', 'Close' );
		} else {
			JToolBarHelper::cancel( 'cancel_promotion', 'Cancel' );
		}
		JToolBarHelper::help( '../help.html', true );
	}

/* order */
	function _ORDERS_MANAGER() {
		JToolBarHelper::title(  JText::_( 'NSP Voucher') .': <small> : '.JText::_( 'Order Manager' ).'</small>' );
		JToolBarHelper::editListX( 'edit_order', 'View' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

	function _EDIT_ORDER( $bookid ) {
		$cid = JRequest::getVar( 'cid', array(0), '', 'array' );
		JArrayHelper::toInteger($cid, array(0));

		$text = ( $cid[0] ? JText::_( 'Edit Order' ) : JText::_( 'New Order' ) );

		JToolBarHelper::title(  JText::_( 'NSP Voucher' ).': <small> : '.JText::_( 'Order Manager' ).' : <small>[ ' . $text.' ]</small></small>' );
		JToolBarHelper::save( 'save_order', 'Save' );
		JToolBarHelper::apply( 'apply_order', 'Apply' );
		if ($cid[0]) {
			// for existing items the button is renamed `close`
			JToolBarHelper::cancel( 'cancel_order', 'Close' );
		} else {
			JToolBarHelper::cancel( 'cancel_order', 'Cancel' );
		}
		JToolBarHelper::help( '../help.html', true );
	}
	
	function _LIST_CHECKED(){
		JToolBarHelper::title(  JText::_( 'NSP Voucher' ).': <small> : '.JText::_( 'Voucher Manager' ).' : <small>[ ' . $text.' ]</small></small>' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
	}

	function _DEFAULT() {
		JToolBarHelper::title(  JText::_( 'NSP Voucher Component' ) );
		JToolBarHelper::help( '../help.html', true );
	}
}
?>