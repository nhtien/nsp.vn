<?php
/***************************************************************************
*  @NSP Export/Import Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			October 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Config {
	function Configuration( $config, $lists ) {
	global $option;
	JHTML::_('behavior.tooltip');
?>
<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		submitform( pressbutton );
	}
</script>
<form action="index.php?option=com_nspeximpro&task=configuration" method="post" name="adminForm">
	<table width="100%" class="adminform">
		<tr valign="middle">
			<td width="50%" valign="top" nowrap="nowrap">
				<fieldset class="adminform">
				<legend><?php echo JText::_( 'General Settings' ); ?></legend>
					<table class="admintable">
					  <tr>
						<td class="key"><?php echo JText::_('Separator Charapter');?></td>
						<td><input type="text" value="<?php echo $config->separator_char;?>" name="separator_char" id="" size="10" /></td>
					  </tr>
					  
					</table>
				</fieldset>
				<br/>
				</td>
		    <td width="50%" valign="top" nowrap="nowrap">
					</td>
		</tr>
		<tr valign="middle">
		  <td colspan="2" valign="top" nowrap="nowrap"></td>
	  </tr>
	</table>
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_('form.token');?>
</form>
<?php
	}
}
?>
