<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Main {
	function showMain()	{
		jimport('joomla.html.pane');
		$pane =& JPane::getInstance('sliders');
		global $option;
?>
<?php
$rootLink="index.php?option=".$option."&task=";

$imgscr = "components/".$option."/images/";

?>
<link href="components/<?php echo $option;?>/assets/admin.style.css" type="text/css" rel="stylesheet" />
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	  <td valign="top" width="50%">
	  <table width="100%" border="0" cellpadding="3" class="admin-table-panel">
        <tr>
          <td><div class="border">
		  <a href="<?php echo $rootLink;?>configuration" title="Configuration">
		  <img src="<?php echo $imgscr;?>cpanel.png" class="icon-image" onmouseover="this.className='icon-hover'" onmouseout="this.className='icon-mouseout'"/>
		  </a>
		  <div ><?php echo JText::_('Settings');?></div>
		  </div>
		  </td>
          <td>
		  <div class="border"><a href="<?php echo $rootLink;?>import" title="Import Products">
		  <img src="<?php echo $imgscr;?>import.png" class="icon-image" onmouseover="this.className='icon-hover'" onmouseout="this.className='icon-mouseout'"/>
		  </a>
		  <div><?php echo JText::_('Import Products');?></div>
		  </div>
		  </td>
          <td>
		  <div class="border"><a href="<?php echo $rootLink;?>export" title="Export Products">
		  <img src="<?php echo $imgscr;?>export.png" class="icon-image" onmouseover="this.className='icon-hover'" onmouseout="this.className='icon-mouseout'"/>
		  </a>
		  <div><?php echo JText::_('Export Products');?></div>
		  </div>
		  </td>
        </tr>

		
        <tr>
		
          <td>
			  <div class="border"><a href="<?php echo $rootLink;?>removeimg" title="Remove Product Images">
			  <img src="<?php echo $imgscr;?>module.png" class="icon-image" onmouseover="this.className='icon-hover'" onmouseout="this.className='icon-mouseout'"/>
			  </a>
			  <div><?php echo JText::_('Remove Product Images');?></div>
			  </div>
		  </td>

          <td>
		  </td>
		  
          <td>
		  </td>
        </tr>

		
      </table></td>
		<td valign="top">
	<div><img src="<?php echo $imgscr;?>logo_nsp.jpg" /></div><br/>
<?php 

echo $pane->startPane("menu-pane");
echo $pane->startPanel('License FAQ', "license"); 
?>
<div style="padding:5px">
	<strong>This product is made by Sang Tran Thanh(Date October 2010)</strong><br />
	Full name: Sang Tran Thanh<br />
	Country: Vietnam<br />
	Emaill Address: sangtialia@gmail.com<br />
	Tel:0908317568<br />
</div>
<?php
echo $pane->endPanel();
echo $pane->startPanel('Support / Contacts', "support"); 
?>
<div style="padding:5px">
	<strong>Contacts</strong><br />
	Technical support: <a href="mailto:sangtialia@gmail.com">sangtialia@gmail.com</a>
</div>
<?php
echo $pane->endPanel();
echo $pane->startPanel('Copyright', "copyrights"); 
?>
<div style="padding:5px">
	Copyright &copy; 2010 .<br />All rights reserved.
</div>
<div><img src="<?php echo $imgscr;?>logo_nsp.jpg" /></div><br/>
<?php
echo $pane->endPanel();
echo $pane->endPane();
?>
		</td>
	</tr>
</table>
<?php
	}
}
?>
