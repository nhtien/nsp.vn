<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Export {
	function showForm( &$rows, $option, &$lists ) {
		
		?>
		<form action="?option=com_nspeximpro" method="post" name="adminForm">
		<table class="adminForm">
		  <tr>
			<td><?php echo JText::_('Product Status')?></td>
			<td><?php echo $lists['product_status']?></td>
			</tr>
			<tr>
			<td><?php echo JText::_('Choose Manufacture')?></td>
			<td><?php echo $lists['manufacturer'];?></td>
		</tr>
			<tr>
			<td><?php echo JText::_('Export to')?></td>
			<td><?php echo $lists['export']?></td>
		</tr>
		</table>
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="save_export" />
		<?php echo JHTML::_('form.token');?>
		</form>
		
		<?php
	}
	
}
?>
