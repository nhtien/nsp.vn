<?php
/***************************************************************************
*  @NSP Export/Import Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			October 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class Import {
	function showForm( &$rows, $option, &$lists ) {
		
		?>
		<form action="?option=com_nspeximpro" method="post" name="adminForm" enctype="multipart/form-data">
		<p>Upload CSV File to Import Products</p>
		<table class="adminForm">
		  <tr>
			<td><?php echo JText::_('CSV, XLS File')?></td>
			<td><input type="file" name="file" /></td>
			</tr>
			<tr>
			<td><?php echo JText::_('Update Product Table');?></td>
			<td><input type="checkbox" value="1" name="update_product_tbl" /></td>
		</tr>
		</table>
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="save_import" />
		<?php echo JHTML::_('form.token');?>
		</form>
		
		<?php
	}
	function result(&$rows, $option, &$lists){
		?>
		<form action="index.php" method="post" name="adminForm">
		<table width="100%" cellpadding="0" border="0">
		<tr>
		<td width="60%">
			<table class="adminlist" width="auto">
			  <tr>
				<td><?php echo JText::_('File Name')?>:</td>
				<td><?php echo $lists['filename']?></td>
			  </tr>
			  <tr>
				<td><?php echo JText::_('File Size')?>:</td>
				<td><?php echo $lists['filesize']?></td>
			  </tr>
			  <tr>
				<td><?php echo JText::_('Total Row')?>:</td>
				<td><?php echo $lists['total_rows']?></td>
			  </tr>
			  <tr>
				<td><?php echo JText::_('Total Import Row Sucessful')?>:</td>
				<td><?php echo $lists['total_row_import']?></td>
			  </tr>
			  <tr>
				<td><?php echo JText::_('Bad Rows')?>:</td>
				<td><?php echo count($lists['bad_rows']);?></td>
			  </tr>
			  <tr>
				<td><?php echo JText::_('Total Update Price')?>:</td>
				<td><?php echo count($lists['update_price']);?></td>
				</tr>
			  <tr>
				<td><?php echo JText::_('Total Add New Price')?>:</td>
				<td><?php echo count($lists['new_price'])?></td>
				</tr>
			  <tr>
				<td><?php echo JText::_('Total Removed Price')?>:</td>
				<td><?php echo count($lists['remove_price'])?></td>
			  </tr>
			  <tr>
				<td><?php echo JText::_('Total The Price Not Change')?>:</td>
				<td><?php echo count($lists['not_change_price'])?></td>
			  </tr>
			</table>
		</td>
		<td>&nbsp;</td>
		</tr>
		</table>
		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="import" />
		</form>
		<?php
	if( count($lists['bad_rows']) > 0):
	?>
		<p><strong><?php echo JText::_('Row\'s not Import')?></strong></p>
		<table class="adminlist">
		<thead>
		  <tr>
		  	<td>#</td>
			<td>Product ID</td>
			<td>Product SKU</td>
			<td>Product Name</td>
			<td>Product Price</td>
			<td>Product Currency</td>
			<td>Product Unit</td>
		  </tr>
		  </thead>
		<?php
		$i=1;
		foreach( $lists['bad_rows'] as $item):?>
		  <tr>
		  	<td><?php echo $i++;?></td>
			<td><?php echo $item->product_id?></td>
			<td><?php echo $item->product_sku?></td>
			<td><?php echo $item->product_name?></td>
			<td><?php echo $item->product_price?></td>
			<td><?php echo $item->product_currency?></td>
			<td><?php echo $item->product_unit?></td>
		  </tr>
		<?php endforeach; ?>
		</table>	
		<?php endif;?>
		
		<?php
	if( count($lists['remove_price']) > 0):
	?>
		<p><strong><?php echo JText::_('Remove Product Price')?></strong></p>
		<table class="adminlist">
		<thead>
		  <tr>
		  	<td>#</td>
			<td>Product ID</td>
			<td>Product SKU</td>
			<td>Product Name</td>
			<td>Product Price</td>
			<td>Product Currency</td>
			<td>Product Unit</td>
		  </tr>
		  </thead>
		<?php
		$i=1;
		foreach( $lists['remove_price'] as $item):?>
		  <tr>
		  	<td><?php echo $i++;?></td>
			<td><?php echo $item->product_id?></td>
			<td><?php echo $item->product_sku?></td>
			<td><?php echo $item->product_name?></td>
			<td><?php echo $item->product_price?></td>
			<td><?php echo $item->product_currency?></td>
			<td><?php echo $item->product_unit?></td>
		  </tr>
		<?php endforeach; ?>
		</table>	
		<?php endif;?>
		
		
		<?php
	if( count($lists['new_price']) > 0):
	?>
		<p><strong><?php echo JText::_('Add New Product Price')?></strong></p>
		<table class="adminlist">
		<thead>
		  <tr>
		  	<td>#</td>
			<td>Product ID</td>
			<td>Product SKU</td>
			<td>Product Name</td>
			<td>Product Price</td>
			<td>Product Currency</td>
			<td>Product Unit</td>
		  </tr>
		  </thead>
		<?php
		$i=1;
		foreach( $lists['new_price'] as $item):?>
		  <tr>
		  	<td><?php echo $i++;?></td>
			<td><?php echo $item->product_id?></td>
			<td><?php echo $item->product_sku?></td>
			<td><?php echo $item->product_name?></td>
			<td><?php echo $item->product_price?></td>
			<td><?php echo $item->product_currency?></td>
			<td><?php echo $item->product_unit?></td>
		  </tr>
		<?php endforeach; ?>
		</table>	
		<?php endif;?>
		
		<?php
	if( count($lists['update_price']) > 0):
	?>
		<p><strong><?php echo JText::_('Update Product Price')?></strong></p>
		<table class="adminlist">
		<thead>
		  <tr>
		  	<td>#</td>
			<td>Product ID</td>
			<td>Product SKU</td>
			<td>Product Name</td>
			<td>Product Price</td>
			<td>Product Currency</td>
			<td>Product Unit</td>
		  </tr>
		  </thead>
		<?php
		$i=1;
		foreach( $lists['update_price'] as $item):?>
		  <tr>
		  	<td><?php echo $i++;?></td>
			<td><?php echo $item->product_id?></td>
			<td><?php echo $item->product_sku?></td>
			<td><?php echo $item->product_name?></td>
			<td><?php echo $item->product_price?></td>
			<td><?php echo $item->product_currency?></td>
			<td><?php echo $item->product_unit?></td>
		  </tr>
		<?php endforeach; ?>
		</table>	
		<?php endif;?>

	<?php
	}
	
}
?>
