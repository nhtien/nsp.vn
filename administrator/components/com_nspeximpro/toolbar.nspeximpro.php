<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( JApplicationHelper::getPath( 'toolbar_html' ) );

switch ($task) {
	case 'configuration':
		TOOLBAR_NSPEximpro::_CONFIGURATION();
		break;
	case 'export':
		TOOLBAR_NSPEximpro::_EXPORT();
		break;	
		
	case 'import':
		TOOLBAR_NSPEximpro::_IMPORT();
		break;	
	case 'save_import':
		TOOLBAR_NSPEximpro::_SAVE_IMPORT();
		break;	

	default:
		TOOLBAR_NSPEximpro::_DEFAULT();
		break;
}
?>