<?php
/***************************************************************************
*  @NSP Export/Import Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			October 2010
***************************************************************************/
defined('_JEXEC') or die();

class TableImport_history extends JTable {

	var $id					= null;
	var $user_id			= null;
	var $username	 		= null;
	var $import_date		= null;
	var $import_file		= null;
	var $filesize			= null;
	var $total_rows			= null;
	var $bad_rows			= null;
	var $total_row_import	= null;
	var $update_price		= null;
	var $new_price			= null;
	var $remove_price		= null;
	var $not_change_price	= null;
	var $update_product_tbl	= null;
	

	function __construct( &$_db ) {
		parent::__construct( '#__nspeximpro_import_history', 'id', $_db );
	}

	function check() {
		return true;
	}
}
?>