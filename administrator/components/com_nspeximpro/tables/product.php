<?php
/***************************************************************************
*  @NSP Export/Import Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			October 2010
***************************************************************************/
// no direct access
defined('_JEXEC') or die();

class TableProduct extends JTable {

	var $product_id			= null;
	var $product_sku		= null;
	var $product_name		= null;
	var $product_unit		= null;

	function __construct( &$_db ) {
		parent::__construct( '#__vm_product', 'Product_id', $_db );
	}

	function check_exists( $id=null,$sku=null) {
		$db = &JFactory::getDBO();
		$product_id = $id ? $id : $this->product_id;
		$product_sku = $sku ? $sku : $this->$product_sku;
		$sql = "SELECT * FROM #__vm_product WHERE product_id=$product_id AND product_sku='$product_sku'";
		$db->setQuery($sql);
		if( $db->loadResult()) return true;
		return false;
	}
	function update( $obj, $update_product = 0 ){
		$db = &JFactory::getDBO();
		//$db->setUTF();
		$shopper_group_id = 5;
		$r=$obj;
		$datenow = time();
		// update product table
		if($update_product){
			$sql = "UPDATE #__vm_product SET product_name='".$r->product_name."', product_unit='".$r->product_unit."' WHERE product_id = $r->product_id AND product_sku = '$r->product_sku'";
			$db->setQuery($sql);
			$db->query();
		}
		// update price
		if( $r->product_price > 0){
			$sql = "SELECT product_price FROM #__vm_product_price WHERE product_id = $r->product_id AND shopper_group_id = $shopper_group_id";
			$db->setQuery($sql);
			$product_price = $db->loadResult();
			if( $product_price ){
				if( $product_price <> $r->product_price){// modify price
					$sql_2 = "UPDATE #__vm_product_price SET product_price = $r->product_price, product_currency='$r->product_currency',mdate=$datenow WHERE product_id = $r->product_id AND shopper_group_id = $shopper_group_id";
					$act = 1;// update price
				}else{
					// the price not change
					$act = 4;// price not change
				}
			}else{
				$sql_2 = "INSERT INTO #__vm_product_price(product_id,product_price,product_currency, shopper_group_id, product_price_vdate, product_price_edate, cdate, mdate) VALUES($r->product_id, $r->product_price,'$r->product_currency', $shopper_group_id,0,0,$datenow,$datenow)";
				$act = 2;// Add New price
			}
		}elseif($r->product_price <= 0){ // remove price
			$sql = "SELECT * FROM #__vm_product_price WHERE product_id = $r->product_id AND shopper_group_id = $shopper_group_id";
			$db->setQuery($sql);
			if( $db->loadResult() ){
				$sql_2="DELETE FROM #__vm_product_price WHERE product_id = $r->product_id AND shopper_group_id = $shopper_group_id";
				$act = 3;// remove price
			}
		}
		
		if( $sql_2){
			$db->setQuery( $sql_2 );
			$db->query();
		}
		return $act;
		
	}

	
}
?>