<?php
/***************************************************************************
*  @NSP Export/Import Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			October 2010 
***************************************************************************/
// no direct access
defined('_JEXEC') or die();

jimport( 'joomla.application.component.controller' );

$lang = & JFactory::getLanguage();
$lang->load('nspeximpro', JPATH_COMPONENT);

class NspController extends JController {

	function __construct( $default = array())
	{
		parent::__construct( $default );
		
		//some tasks about configuration
		$this->registerTask( 'apply_configuration', 'save_configuration' );
		
	}

	function main() {
		require_once( JPATH_COMPONENT.DS.'views'.DS.'main.php' );
		Main::showMain();
	}
	
/* some function about configuaration */
	
	function configuration() {
		global $nspEximPro_config;
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'configuration.php' );
		Config::Configuration($nspEximPro_config, $lists);
	}
	
	function save_configuration() {
		global $option, $task, $mainframe;
		
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		$post = JRequest::get( 'post' );
		$config = &JTable::getInstance('config','Table');
		$config->save( $post );
		switch ($this->_task) {
			case 'apply_configuration':
				$msg = JText::_( 'Configuration saved' );
				$link = 'index.php?option=com_nspeximpro&task=configuration';
			break;
			case 'save_configuration':
			default:
				$msg = JText::_( 'Configuration saved' );
				$link = 'index.php?option=com_nspeximpro&task=main';
			break;
		}
		$mainframe->redirect($link, $msg);
	}
	
	function cancel_configuration() {
		require_once( JPATH_COMPONENT.DS.'views'.DS.'main.php' );
		Main::showMain();
	}

	function export(){
		global $option;
		$db = &JFactory::getDBO();
		$sql = "SELECT manufacturer_id, mf_name FROM #__vm_manufacturer WHERE 1 ORDER BY mf_name";
		$db->setQuery( $sql );
		$manufacture = $db->loadObjectList();
		foreach($manufacture as $m){
			$mf_option[] = JHTML::_('select.option',$m->manufacturer_id, $m->mf_name);
		}
		$lists['manufacturer'] = JHTML::_('select.genericList', $mf_option,'manufacturer_id[]','class="inputbox" multiple="multiple" size="10"','value','text',null);
		
		$product_status[] = JHTML::_("select.option","","ALL");
		$product_status[] = JHTML::_("select.option","Y","Published");
		$product_status[] = JHTML::_("select.option","N","UnPublished");
		
		$lists['product_status'] = JHTML::_('select.radiolist',$product_status,'product_publish','class="inputbox"','value','text','Y');
		
		$ex_option[] = JHTML::_('select.option','csv','CSV File');
		$ex_option[] = JHTML::_('select.option','xls','XLS File');
		
		$lists['export'] = JHTML::_('select.radiolist', $ex_option, 'export2', 'class="inputbox"','value','text','csv');
		
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'export.php' );
		Export::showForm($rows, $option, $lists);
		
	}
	function save_export(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		global $mainframe, $nspEximPro_config;
		$db = &JFactory::getDBO();
		$datenow = &JFactory::getDate();
		$user = &JFactory::getUser();
		if( $user->id <>62){
			$mainframe->redirect("index.php?option=com_nspeximpro","You Can't Export Database. Please Contact Super Administrator.","error");
		}
		$post = JRequest::get('post');
		$shopper_default = 5;
		$mf_arr = array();
		
		if(is_string($post['manufacturer_id'])){
			$where[] = "pref.manufacturer_id = ".$post['manufacturer_id'];
			$mf_arr[] = $post['manufacturer_id'];
		}elseif( is_array($post['manufacturer_id'])){
			$where[] = "pref.manufacturer_id IN(".implode(",",$post['manufacturer_id']).")";
			$mf_arr = $post['manufacturer_id'];
		}
		if( $post['product_publish']){
			$where[] = "p.product_publish='".$post['product_publish']."'";
		}
		$where = ' WHERE '.implode(" AND ", $where);
		$orderby = " ORDER BY p.product_parent_id,p.product_name";
		$having = ' HAVING total_parent = 0';
		
		$sql = "SELECT p.product_id, p.product_parent_id, p.product_sku, p.product_name, p.product_s_desc, p.product_thumb_image,p.product_full_image, p.product_publish, p.product_weight, p.product_weight_uom, p.product_length, p.product_width, p.product_height, p.product_lwh_uom, p.product_special, p.product_unit, p.product_tax_id,
		pr.product_price, pr.product_currency,
		(SELECT COUNT(*) FROM #__vm_product AS p2 WHERE p2.product_parent_id=p.product_id) AS total_parent
					FROM #__vm_product AS p
					LEFT JOIN #__vm_product_price AS pr ON pr.product_id= p.product_id AND pr.shopper_group_id = $shopper_default
					LEFT JOIN #__vm_product_mf_xref AS pref ON pref.product_id = p.product_id
					".$where . $having . $orderby;
		$db->setQuery($sql);
		$rows = $db->loadObjectList();// echo $db->getQuery();exit; 
		$data = array();
		foreach( $rows as $r){
			$o = new StdClass();
			if(!$r->product_currency) $r->product_currency='USD';
			$r->product_price = number_format($r->product_price, 2, null, '');
			$o = $r;
			$data[] = $o;
		}
		
		$br = $nspEximPro_config->separator_char;
		require(JPATH_COMPONENT.DS.'common.php');
		$product_fields = $product_field_arr;
			
		$db->setQuery("SELECT * FROM #__vm_manufacturer WHERE manufacturer_id IN (".implode(",",$mf_arr).")" );
		$row_mf = $db->loadObjectList();
		foreach( $row_mf as $rf){
			$str_mf .= trim($rf->mf_name).'_';
			$mfArrName[] = $rf->mf_name;
		}
		$str_mf = str_replace(" ","_",$str_mf);
		$filename = "NSP_Web_Price_List_".$str_mf.$user->id.'_'.$datenow->toFormat("%d_%m_%Y");
		
		$exFolder = $nspEximPro_config->export_folder;
		jimport('joomla.filesystem.path');
		jimport('joomla.filesystem.file');
		if( !JFolder::exists($exFolder)){
			JFolder::create($exFolder,'0755');
		}
		
		switch( JRequest::getVar('export2','csv') ){
			case 'csv': // CSV File
				$file_extension = 'csv';
				$buffer = implode($br,$product_fields)."\n";
					
				foreach( $data as $r){
					$buffer .= 
						$r->product_id . $br . 
						$r->product_sku . $br . '"'.
						str_replace('"','&quot;',$r->product_name).'"' . $br . 
						$r->product_price . $br .
						$r->product_currency . $br .
						$r->product_unit . $br .
						$r->product_parent_id .
						"\n";
				}
				$filename .= '.'.$file_extension;
				$file = $exFolder.DS.$filename;
				JFile::write( $file, $buffer);
				break;
			case 'xls': // XLS File
				$file_extension = 'xlsx';
				$filename .= '.'.$file_extension;
				$file = $exFolder.DS.$filename;
				require_once(JPATH_COMPONENT.DS.'classes'.DS.'PHPExcel.php');
				require_once (JPATH_COMPONENT.DS.'classes'.DS.'PHPExcel'.DS.'IOFactory.php');
				$objPHPExcel = new PHPExcel();
				
				// Set properties
				$objPHPExcel->getProperties()->setCreator("Tran Thanh Sang")
											 ->setLastModifiedBy("Tran Thanh Sang")
											 ->setTitle("NSP WEB Export Document")
											 ->setSubject("")
											 ->setDescription("")
											 ->setKeywords("")
											 ->setCategory("");
											 
				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->getActiveSheet()->setTitle($mfArrName[0]);
				
				$startRIndex = $xlsStartRIndex; // chi so index cua dong dau tien trong bang excel
				$alpharicArr = $xlsAlpharicArr;
				
				$startAlpharic = $alpharicArr[0];
				$numColumns = count($alpharicArr);
				$endAlpharic = $alpharicArr[$numColumns-1];
				
				$objPHPExcel->getActiveSheet()->mergeCells('A1:'.$endAlpharic.'1');
				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Bảng cập nhật dữ liệu sản phẩm Website NSP.COM.VN');
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Nguồn xuất file: nsp.com.vn');
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Ngày xuất: '.date("d-m-Y"));
				$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Sản phẩm: '.implode(", ",$mfArrName));
				
				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
				//$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
				$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB('FFFF0000');
				//set sheet title
				$i = 0;
				$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$startRIndex, $xlsTitleArray[0]);
				$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$startRIndex, $xlsTitleArray[1]);
				$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$startRIndex, $xlsTitleArray[2]);
				$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$startRIndex, $xlsTitleArray[3]);
				$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$startRIndex, $xlsTitleArray[4]);
				$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$startRIndex, $xlsTitleArray[5]);
				$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$startRIndex, $xlsTitleArray[6]);
				$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$startRIndex, $xlsTitleArray[7]);
				
				$row_index = $startRIndex+1;
				$odernum = 1;
				$maxLenPName = 40;
				foreach( $rows as $r){
					$i = 0;
					$maxLenPName = strlen($r->product_name) > $maxLenPName ? strlen($r->product_name) : $maxLenPName;
					
					$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$row_index, $odernum++);
					$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$row_index, $r->product_id);
					$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$row_index, $r->product_sku);
					$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$row_index, $r->product_name);
					$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$row_index, $r->product_price);
					$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$row_index, $r->product_currency);
					$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$row_index, $r->product_unit);
					$objPHPExcel->getActiveSheet()->setCellValue($alpharicArr[$i++].$row_index, $r->product_parent_id);
					$row_index++;
				}
				$maxLenPName = $maxLenPName > 75 ? 75 : $maxLenPName;
				// Set column widths
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth($maxLenPName);

				// Set fonts
				$objPHPExcel->getActiveSheet()->getStyle("A1:$endAlpharic".$row_index)->getFont()->setName('Arial');
				$objPHPExcel->getActiveSheet()->getStyle("A2:$endAlpharic".$row_index)->getFont()->setSize(10);
				//$objPHPExcel->getActiveSheet()->getStyle('A3:G3')->getFont()->setBold(true);
				
				//set title format
				$objPHPExcel->getActiveSheet()->getStyle('A'.$startRIndex.':'.$endAlpharic.$startRIndex)->applyFromArray(
						array(
							'font'    => array(
								'bold'      => true
							),
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
							),
							'fill' => array(
								'type'       => PHPExcel_Style_Fill::FILL_SOLID,
								'startcolor' => array(
									'argb' => 'FFd7d4d4'
								)
							)
						)
				);
				//set border
				$styleThinBlackBorderOutline = array(
					'borders' => array(
						'outline' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('argb' => 'FF000000'),
						),
						'inside' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('argb' => 'FF000000'),
						),

					),
				);
				//set background
				for( $i = $startRIndex+2 ; $i < $row_index ; $i+=2){
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':'.$endAlpharic.$i)->applyFromArray(array('fill'=>array('type'=>PHPExcel_Style_Fill::FILL_SOLID,'startcolor'=>array('argb'=>'FFd1f6f6'))));
				}

				$objPHPExcel->getActiveSheet()->getStyle('A'.$startRIndex.':'.$endAlpharic.($row_index - 1))->applyFromArray($styleThinBlackBorderOutline);
				// set alignment
				$objPHPExcel->getActiveSheet()->getStyle("A$startRIndex:C$row_index")->applyFromArray(array('alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
								
				$objPHPExcel->getActiveSheet()->getStyle("E$startRIndex:H$row_index")->applyFromArray(array('alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)));
				
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save($file);						 
				//var_dump(count($objPHPExcel));exit;
				break;
		}
		//var_dump( $buffer);//exit;
		
		$msg[] = "Export Products completed.";
		$msg[] = "File: ".$filename;
		$msg[] = "Total Rows: ".count( $rows );
		$msg[] = '<a href="'.$nspEximPro_config->url_export_folder.'/'.$filename.'">Download &raquo;</a>';
		$msg = implode("<br />",$msg);
		
		$mainframe->redirect("index.php?option=com_nspeximpro&task=export", $msg);
	}
	
	function import(){
		global $option, $mainframe;
		require_once( JPATH_COMPONENT.DS.'views'.DS.'import.php' );
		Import::showForm($rows, $option, $lists);
	}
	function save_import(){
		JRequest::checkToken() or jexit( 'Invalid Token' );
		global $option, $mainframe, $nspEximPro_config;
		$user 		= &JFactory::getUser();
		$datenow 	= &JFactory::getDate();
		$post		= JRequest::get('post');
		$link = "index.php?option=com_nspeximpro";
		if( $user->id <>62){
			$mainframe->redirect($link,"You Can't Import Database. Please Contact Super Administrator.","error");
		}
		jimport('joomla.filesystem.file');
		$file = JRequest::getVar( 'file', '', 'files', 'array' );
		$filesize = getFileSizeReadable($file['size']);
		
		$ext = strtolower( substr($file['name'],strrpos($file['name'],".")+1) );
		if( $ext <> "csv" && $ext <> "xls" ){ 
			//$mainframe->redirect( $link,"File Format Is Not Support.","error");
		}
		if( !JFolder::exists($nspEximPro_config->import_folder)){
			JFolder::create($exFolder,'0755');
		}

		$tmp_src 	= $file['tmp_name'];
		$tmp_dest 	= $nspEximPro_config->import_folder.DS.$file['name'];
		JFile::upload($tmp_src, $tmp_dest);
		require(JPATH_COMPONENT.DS.'common.php');
		$field_arr = $product_field_arr; 
		
		switch( $ext ){
			case 'csv':
				$handle = fopen($tmp_dest, "r");
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
					$num = count($field_arr);
					$obj = new StdClass();
					for ($j=0; $j < $num; $j++) {
						$obj->$field_arr[$j] = $data[$j];
					}
					$row_result[] = $obj;
				}
				fclose($handle);
				unset($row_result[0]);
				break;
			case 'xls':
				require_once(JPATH_COMPONENT.DS.'classes'.DS.'PHPExcel.php');
				require_once(JPATH_COMPONENT.DS.'classes'.DS.'PHPExcel'.DS.'IOFactory.php');
				//jimport('classes.PHPExcel.php','administrator','components.com_nspeximpro');

				$objReader = new PHPExcel_Reader_Excel5();
				$objReader->setReadDataOnly(true);
				$objPHPExcel = $objReader->load($tmp_dest);
				
				$sIndex = $xlsStartRIndex;
				$alphArr = $xlsAlpharicArr;
				
				// kiem tra tieu de xls
				$NSP_Format = true;
				$i=0;
				while($NSP_Format && $i < count($xlsTitleArray) ){
					$fieldName = $objPHPExcel->getActiveSheet()->getCell($alphArr[$i].$sIndex)->getValue(); //echo $fieldName.'-'.$xlsTitleArray[$i].'<br/>';
					$NSP_Format = $fieldName <> $xlsTitleArray[$i] ? false : true;
					$i++;
				}
				if( !$NSP_Format ){
					$mainframe->redirect($link,"Excel Format is wrong.","error");
				}
				// end
				$sIndex++;
				$PN = $objPHPExcel->getActiveSheet()->getCell($alphArr[2].$sIndex)->getValue(); //get first part number
				//var_dump( $PN);
				while($PN){
					$num = count($field_arr);
					$obj = new StdClass();
					for ($j=0; $j < $num; $j++) {
						$obj->$field_arr[$j] = $objPHPExcel->getActiveSheet()->getCell($alphArr[$j+1].$sIndex)->getValue();
					}
					$row_result[] = $obj;
					$sIndex++;
					$PN = $objPHPExcel->getActiveSheet()->getCell($alphArr[2].$sIndex)->getValue();;
				}
				
				//$objPHPExcel->setActiveSheetIndex(0);
				//$value = $objPHPExcel->getActiveSheet()->setCellValue('C6','');
				//var_dump( $row_result[0] );exit;

			break;
			}
		
		
		
		$p = &JTable::getInstance('product','Table');
		
		
		$total_row=0;
		foreach( $row_result as $r){ 
		
			$r->product_id 		 = (int)$r->product_id;
			$r->product_sku 	 = strtoupper(trim($r->product_sku));
			$r->product_price 	 = (float)$r->product_price;
			$r->product_currency = $r->product_currency ? strtoupper(trim($r->product_currency)) : 'USD' ;
			
			if($p->check_exists($r->product_id,$r->product_sku)){
				$total_row++;
				$act = $p->update($r, $post['update_product_tbl']);
				switch( $act ){
					case 1:
						$update_price[] = $r;
						break;
					case 2:
						$new_price[] = $r;
						break;
					case 3:
						$remove_price[] = $r;
						break;
					case 4:
						$not_change_price[] = $r;
				}
			}else{
				// row not import
				$bad_row[] = $r;
			}
		}
		$lists['update_price'] 	= $update_price;
		$lists['new_price'] 	= $new_price;
		$lists['remove_price'] 	= $remove_price;
		$lists['not_change_price'] 	= $not_change_price;
		$lists['total_rows'] 	= count($row_result);
		$lists['total_row_import'] = $total_row;
		$lists['bad_rows'] 		= $bad_row;
		$lists['filename']		= $file['name'];
		$lists['filesize'] 		= $filesize;

		/*
		$file_content = JFile::read($tmp_dest) ;
		$data = explode("\n",$file_content);
		foreach( $data as $r){
			$obj = new StdClass();
			$tmp_arr = explode($nspEximPro_config->separator_char,$r);
			$i=0;
			foreach( $field_arr as $a){
				$obj->$a = $tmp_arr[$i];
				$i++;
			}
			$row_result[] = $obj;
		}
		var_dump( $row_result[0]);exit;
		*/
		
		// Insert into Import History
		$history = &JTable::getInstance('import_history','Table');
		$history->import_date 	= $datenow->toMySQL();
		$history->import_file 	= $file['name'];
		$history->user_id 		= $user->id;
		$history->username 		= $user->name;
		$history->total_rows 	= $lists['total_rows'];
		$history->bad_rows 		= $lists['bad_rows'];
		$history->total_row_import = $lists['total_row_import'];
		$history->update_price	= count($update_price);
		$history->new_price		= count( $new_price );
		$history->remove_price	= count($remove_price);
		$history->not_change_price = count($lists['not_change_price']);
		$history->filesize		= $filesize;
		$history->update_product_tbl = $post['update_product_tbl'];
		if( !$history->store()){
			JError::raiseError(500, $row->getError() );
		}
		// end
		
		$objList = serialize($lists);
		$session	 =& JFactory::getSession();
		$session->set('nspeximprolists', $objList);
		$mainframe->redirect('index.php?option=com_nspeximpro&task=import_result',"Import Completed.");
		

	}
	function import_result(){
		global $option, $mainframe;
		$session	 =& JFactory::getSession();
		$lists = unserialize($session->get('nspeximprolists'));
		
		require_once( JPATH_COMPONENT.DS.'views'.DS.'import.php' );
		Import::result($rows, $option, $lists);
	}
	
	function removeimg(){
		$user = &JFactory::getUser();
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		global $mainframe, $path,$move_dir;
		$path = JPATH_ROOT.DS.'documents'.DS.'vm_images'.DS.'product'.DS; // product image folder of virtuemart
		$link = "index.php?option=com_nspeximpro&task=removeimg";
		if(!is_dir($path))
			$path = JPATH_ROOT.DS.'components'.DS.'com_virtuemart'.DS.'shop_image'.DS.'product'.DS;
		
		$db = &JFactory::getDBO();
		 
		$sql = "SELECT * FROM #__vm_product ORDER BY product_full_image ASC";
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		 
		$db_images = array();
		
		foreach ( $rows as $row ) {
			if( $row->product_full_image)
				$db_images[] = strtolower($row->product_full_image);
				
			if( $row->product_thumb_image)
				$db_images[] = strtolower($row->product_thumb_image);
		}
		//var_dump( );exit;
		$move_dir = $path.'removed'.DS.$user->username.'_'.date("Y_m_d").DS;
		JFolder::create($move_dir,0775);
		JFolder::create($move_dir.'resized'.DS,0775);
		
		//$exclude = array(".","..",".svn","index.html","index.htm","___1noimage.gif");
		
		//$product_files	= JFolder::files($path, '.', false, false, $exclude);
		//$resize_file	= JFolder::files($path.'resized', '.', false, false, $exclude);
		
		//var_dump( $resize_file );exit; 



		function remove_file( $dir, $db_files, $subname = ''){
			global $path, $move_dir;
			$dir_current = opendir( $dir );
			$total_files = 0;
			while( $file = readdir($dir_current) )
			{ 
				$filename 	= $file;
				$file 		= ( $subname.$file ); 
				$exclude 	= array(".","..",".svn","index.html","index.htm","___1noimage.gif");
				if ( !in_array($filename,$exclude) and !is_dir($path.$file) ){
					if( !in_array( strtolower($file), $db_files) ){
						$src_file = $path.$file;
						JFile::copy($src_file,$move_dir.$file);
						JFile::delete($src_file);
						$file_removed_html .= $file."<br>";
						$total_files++;
					}
					
				}
			}
			echo '<br/><br/><h3>Files were remove ('.$total_files.' files) - '.$dir.'</h3>'.$file_removed_html;
		}


	remove_file($path, $db_images,'');
	remove_file($path.'resized'.DS, $db_images,'resized/');
	}

}
?>