<?php
/***************************************************************************
*  @NSP Export/Import Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			October 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die('Restricted access');

global $nspEximPro_config;

$product_field_arr = array( 
	"product_id",
	"product_sku",
	"product_name",
	"product_price",
	"product_currency",
	"product_unit",
	"product_parent_id"
);
$xlsTitleArray = array('#','Product Id','Part Number','Product Name','Price','Currency','Unit','Parent ID');
$xlsStartRIndex = 5; // chi so index cua dong dau tien trong bang excel

$xlsAlpharicArr = array(
					0 =>'A',//#
					1 => 'B', //Product ID
					2 => 'C', // P/N
					3 => 'D',// Product Name
					4 => 'E', // Price
					5 => 'F',//Current
					6 => 'G',//Unit
					7 => 'H'//Parent ID
					);

?>