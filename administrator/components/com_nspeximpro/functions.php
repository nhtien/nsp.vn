<?php
/***************************************************************************
*  @NSP Export/Import Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			October 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die('Restricted access');

global $nspEximPro_config;

function loadEscProjectConfig(){
	global $nspEximPro_config, $mosConfig_live_site, $option;
	$config = &JTable::getInstance('config','Table'); 
	$pconfig = $config->load();
	$pconfig->url_export_folder = JURI::root().'documents/export';
	//$pconfig->import_folder = JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_nspeximpro'.DS.'files'.DS.'import';
	
	$pconfig->export_folder = JPATH_ROOT.DS.'documents'.DS.'export';
	$pconfig->import_folder = JPATH_ROOT.DS.'documents'.DS.'import';
	
	$nspEximPro_config = $pconfig;
	
	return $nspEximPro_config;
}

function getFileSizeReadable ($size, $retstring = null)
{
	$sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	if ($retstring === null) { $retstring = '%01.2f %s'; }
	$lastsizestring = end($sizes);
	foreach ($sizes as $sizestring) {
			if ($size < 1024) { break; }
			if ($sizestring != $lastsizestring) { $size /= 1024; }
	}
	if ($sizestring == $sizes[0]) { $retstring = '%01d %s'; } // Bytes aren't normally fractional
	return sprintf($retstring, $size, $sizestring);
}


?>