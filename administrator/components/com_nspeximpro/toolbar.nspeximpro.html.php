<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class TOOLBAR_NSPEximpro {

	function _CONFIGURATION() {
		JToolBarHelper::title(  JText::_( 'NSP Export/Import Product' ) .': <small> : '.JText::_( 'Configuration' ).'</small>' );
		JToolBarHelper::save( 'save_configuration', 'Save' );
		JToolBarHelper::apply( 'apply_configuration', 'Apply' );
		JToolBarHelper::cancel( 'cancel_configuration', 'Cancel' );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false );
		JToolBarHelper::help( '../help.html', true );
	}

	function _EXPORT() {
		JToolBarHelper::title(  JText::_( 'Export Products' ) );
		JToolBarHelper::customX( 'save_export', 'apply.png','apply.png', 'Export', false );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false);
		JToolBarHelper::help( '../help.html', true );
	}

	function _IMPORT() {
		JToolBarHelper::title(  JText::_( 'Import Products' ) );
		JToolBarHelper::customX( 'save_import', 'apply.png','apply.png', 'Import', false );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false);
		JToolBarHelper::help( '../help.html', true );
	}
	
	function _SAVE_IMPORT(){
		JToolBarHelper::title(  JText::_( 'Export Products' ) );
		JToolBarHelper::customX( 'main', 'back.png', 'back.png', 'Panel' ,false);
		JToolBarHelper::help( '../help.html', true );
	}

	function _DEFAULT() {
		JToolBarHelper::title(  JText::_( 'NSP Export/Import Product Component' ) );
		JToolBarHelper::help( '../help.html', true );
	}
}
?>