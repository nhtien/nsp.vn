<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

global $nspEximPro_config;

JTable::addIncludePath( JPATH_COMPONENT.DS.'tables' );


require_once( JPATH_COMPONENT.DS.'functions.php' );

loadEscProjectConfig();

require_once( JPATH_COMPONENT.DS.'controller.php' );

// Set the table directory

$controller = new NspController( array('default_task' => 'main') );

$controller->execute( JRequest::getCmd( 'task' ) );
$controller->redirect();
?>