<?php
/**
 * All Streams Helper of JMultimedia Component
 * @package			Joomla
 * @subpackage	JMultimedia
 * @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 */

class comCommentsHelper{
	
	/**
	 * Get section and return Table name 
	 * 
	 * @static
	 * @return string Table name
	 * @param string $section component name
	 */
	function section2table($section){
		$tbl = str_replace('com_', '#__', $section );
		
		return $tbl; 
	}

	/**
	 * Get page section 
	 * 
	 * @static
	 * @return string component name
	 */
	function getSection(){
		static $section;
		if(empty($section)){
			$section = JRequest::getVar( 'section', 'com_content' );
		}
		return $section;	
	}
	
	/**
	 * Create HTML select list with conteiners
	 * 
	 * @static
	 * @return void
	 * @param string $tbl
	 * @param string $name [optional]
	 */
	function selectList($tbl, $name='cotid', $selected=0, $attr=null ){
		$db		=& JFactory::getDBO();
		$query = 'SELECT id AS value, title AS text'
			. ' FROM ' . $tbl
//			. ' WHERE id IN (SELECT cotid FROM #__comments WHERE section='
			. ' ORDER BY text';
		$db->setQuery($query);
		
		$options = $db->loadObjectList();		
		array_unshift( $options, array('value'=>0, 'text'=>JText::_('Select Item')) );
		return JHTML::_('select.genericlist', $options, $name, 
			$attr, 	//Additional list attributes
			'value',//The value key in the associative arrays or objects, normally value
			'text', 	//The text key in the associative arrays or objects, normally text
			$selected,//Key value of the currently selected option; default is null
			null, 	//List ID, default is null
			false );	//Translate text using JText; default is false
	}
}