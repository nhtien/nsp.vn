<?php 
/**
 * Display media form, follow weblinks model 
 */
defined('_JEXEC') or die('Restricted access'); 

// ToolBar
$text = ($this->item->id)? 
	JText::_( 'Edit' ):	
		JText::_( 'New' ); 		

JToolBarHelper::title(  'Comment: <small><small>['. $text.' ]</small></small>' );
//JToolBarHelper::apply();
JToolBarHelper::save();
JToolBarHelper::cancel();	
JToolBarHelper::help( 'comments', true );
?>

<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton) {
		var form = document.adminForm;
		
		if (pressbutton == 'cancel') {
			submitform( pressbutton );
			return;
		}
		
		// Comment
		if (form.comment.value == ""){
			alert( "<?php echo JText::_( JText::_('You must comment something.') ); ?>" );
			return;
		} 
		
		// Content
		if (form.cotid.value == "0"){
			alert( "<?php echo JText::_( 'You must select a content', true ); ?>" );
			return;
		} 
		
		submitform( pressbutton );
	}
	
	
</script>

<style type="text/css">
	table.paramlist td.paramlist_key {
		width: 92px;
		text-align: left;
		height: 30px;
	}
	
	
	table.admintable{
		width: 100%;
	}
</style>

<form action="<?php echo $this->action; ?>" method="post" name="adminForm" id="adminForm">
	
	<!-- Properties -->
	<div class="col width-60">
	<fieldset class="adminform">

		<table class="admintable" width="100%">
		<!-- Title -->
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'Title' ); ?>:
				</label>
			</td>
			<td>
				<input class="inputbox" size="64" type="text" name="title" id="title" maxlength="250" value="<?php echo $this->item->title;?>" />
			</td>
		</tr>

		<!-- Comment -->
		<tr>
			<td valign="center" align="right" class="key">
				<label for="comment">
					<?php echo JText::_( 'comment' ); ?>: 
				</label>
			</td>
			<td>
				<textarea class="inputbox" cols="45" rows="10" name="comment" id="comment"><?php echo $this->item->comment; ?></textarea>
			</td>
		</tr>
	</table>		
	</fieldset>
	
	<fieldset class="adminform">
		<legend><?php echo JText::_('Optional Params'); ?></legend>
		<table class="admintable">			
<!-- Optional Params -->		
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'Author' ); ?>:
				</label>
			</td>
			<td>
				<input class="inputbox" size="64" type="text" name="author" id="author" maxlength="250" value="<?php echo $this->params->get('author');?>" />
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'Email' ); ?>:
				</label>
			</td>
			<td>
				<input class="inputbox" size="64" type="text" name="email" id="email" maxlength="250" value="<?php echo $this->params->get('email');?>" />
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'Site' ); ?>:
				</label>
			</td>
			<td>
				<input class="inputbox" size="64" type="text" name="site" id="site" maxlength="250" value="<?php echo $this->params->get('site');?>" />
			</td>
		</tr>		
		<tr>
			<td width="100" align="right" class="key">
				<label for="author">
					<?php echo JText::_( 'More Params' ); ?>:
				</label>
			</td>
			<td>
				<textarea name="params" id="params"  cols="45" rows="5" title="Add morer field, like: author=Algum\n email=den@mail.com" ><?php echo $this->item->params; ?></textarea>
			</td>
		</tr>	
		</table>
	</fieldset>
	</div>	
	
	<!-- detail -->
	<div class="col width-40">
	<fieldset class="adminform">
		
		<table class-"admintable">		
		<tr>
			<td valign="top" align="right" class="key">
				<label><?php echo JText::_( 'Accepted' ); ?>:</label>
			</td>
			<td>
				<?php echo $this->lists['published']; ?>
			</td>
		</tr>
		<tr>
			<td valign="top" align="right" class="key">
				<label><?php echo JText::_( 'Container' ); ?>:</label>
			</td>
			<td>
				<?php echo $this->lists['contents']; ?>
			</td>
		</tr>			
		</table>
		
		<?php if($this->item->id) { ?><br />
		<table style="border: 1px dashed silver; padding: 5px; margin-bottom: 10px;" width="100%">
			<tbody>
			<tr>
				<td>
					<strong>Comment ID:</strong>
				</td>
				<td><?php echo $this->item->id; ?></td>
			</tr>
			<tr>
				<td><strong>Hits:</strong></td>
				<td><?php echo $this->item->hits; ?></td>
			</tr>
			<tr>
				<td><strong class="createdate">Added Date:</strong></td>
				<td><?php echo $this->item->added; ?></td>
			</tr>
			</tbody>
		</table>		
		<?php } ?>
		
	</fieldset>	
	</div>		
	


	<div class="clr"></div>	
	<input type="hidden" name="added" value="<?php echo $this->item->added; ?>" />
	<input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />	
	<input type="hidden" name="userid" value="<?php echo $this->item->userid ?>" />
	<input type="hidden" name="uid" value="<?php echo $this->item->userid ?>" />	
	<input type="hidden" name="option" value="com_comments" />
	<input type="hidden" name="c" value="comment" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="section" value="<?php echo $this->item->section; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
