<?php
/**
 * Comments View for ... Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license        GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.view' );

/**
 * Comments View
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class CommentsViewComment extends JView
{
    /**
     * Hellos view display method
     * @return void
     **/
    function display($tpl = null)
    {
		global $mainframe, $option;
		
		$db		=& JFactory::getDBO();
		$uri 	=& JFactory::getURI();
		$user 	=& JFactory::getUser();
		$model	=& $this->getModel();

		$lists = array();
		
		//get the data
		$item		=& $this->get('Data');

		// fail if checked out not by 'me'
		if ( $model->isCheckedOut( $user->get('id') ) ) {
			$msg = JText::sprintf( 'DESCBEINGEDITTED', JText::_( 'item' ), $item->title );
			$mainframe->redirect( 'index.php?option='. $option, $msg );
		}
		
		// is new?
		if( !$item->id ){
			$item->section =  comCommentsHelper::getSection();
		}

		$tbl = comCommentsHelper::section2table($item->section );
		// build the html select list for ordering
		$lists['contents'] = comCommentsHelper::selectList( $tbl, 'cotid', $item->cotid );
		
		// build the html select list
		$lists['published'] 	= JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $item->published );

		//clean weblink data
		JFilterOutput::objectHTMLSafe( $item, ENT_QUOTES, 'comment' );
		
		// parameters
		$params = new JParameter($item->params);
		
		// Action
		$action = JRoute::_('index.php?option='.$option);
		
		// Assign References
		$this->assignRef('action',	$action);
		$this->assignRef('lists', 	$lists);
		$this->assignRef('item',	$item);
		$this->assignRef('params', $params);
		
		parent::display($tpl);
	}
	
	
}