<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
	// Set Vars
	$section_name = substr( $this->section, 4 );
	
	// Set toolbar items for the page
	JToolBarHelper::title( JText::_( 'Comments Manager' ).': <small><small>[ '. $section_name .' ]</small></small>' , 'generic.png' );
	JToolBarHelper::publishList();
	JToolBarHelper::unpublishList();
	JToolBarHelper::deleteList( JText::_('VALIDDELETEITEMS') );
	JToolBarHelper::editListX();
	JToolBarHelper::addNewX('edit');
	//JToolBarHelper::preferences('com_jpress_comments', '360', '550', JText::_('Configuration') );
	JToolBarHelper::custom('confPlugin', 'config.png', '', JText::_('Configuration'), false);
	JToolBarHelper::help( 'comments', true );
?>

<form action="<?php echo $this->action; ?>" method="post" name="adminForm">
	<table>
	<tr>
		<td align="left" width="100%">
			<?php echo JText::_( 'Filter' ); ?>:
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
			<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
			<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_catid').value='0';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button>
		</td>
		<td nowrap="nowrap">
			<?php
				echo $this->lists['contents'];
				echo $this->lists['state'];
			?>
		</td>
	</tr>
	</table>
	
	<!-- Admin list -->
	<table class="adminlist">
	<thead>
		<tr>
			<th width="5">
				<?php echo JText::_( 'NUM' ); ?>
			</th>
			<th width="20">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
			</th>
			<th class="title" >
				<?php echo JHTML::_('grid.sort',  'Comment', 'a.comment', $this->lists['order_Dir'], $this->lists['order'] );	 ?>
			</th>
			<th width="5%" nowrap="nowrap">
				<?php echo JHTML::_('grid.sort',  'Accepted', 'a.published', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th>
			<th width="8%" nowrap="nowrap">
				<?php echo JHTML::_('grid.sort',  'Date', 'a.added', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th>
			<th width="15%" >
				<?php echo JHTML::_('grid.sort',  'Content Item', 'container', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th>
			<th width="15%" >
				<?php echo JHTML::_('grid.sort',  'Author', 'author', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th>
			<th width="5%">
				<?php echo JHTML::_('grid.sort',  'Rating', 'a.hits', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th>
			<th width="1%" nowrap="nowrap">
				<?php echo JHTML::_('grid.sort',  'ID', 'a.id', $this->lists['order_Dir'], $this->lists['order'] ); ?>
			</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="9">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>
	<tbody>
	<?php
	$k = 0;
	for ($i=0, $n=count( $this->items ); $i < $n; $i++):
		$row = &$this->items[$i];
		$params = new JParameter($row->params);
		
		// Section Item
		$link['cotid'] 	= JRoute::_( 'index.php?option=com_comments&section='.$this->section.'&cotid='. $row->cotid );
		$link['user'] 	= JRoute::_( 'index.php?option=com_users&view=user&task=edit&cid[]='. $row->userid );		
		$link['edit'] 	= JRoute::_( 'index.php?option=com_comments&task=edit&view=comment&id='. $row->id );		
		
		// Author 
		if(trim($row->author) == ''){
			$row->author = $params->get('author', JText::_('Guest') );
		}
				
		$date  = JFactory::getDate($row->added);// TODO clean date
		$row->added = $date->toFormat(JText::_('DATE_FORMAT_LC4'));
		
		$row->comment = substr( $row->comment, 0, 200 ).'...   ';
		
		$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		$published 	= JHTML::_('grid.published', $row, $i );

		?>
		<tr class="<?php echo 'row'.$k; ?>">
			<td>
				<?php echo $this->pagination->getRowOffset( $i ); ?>
			</td>
			<td>
				<?php echo $checked; ?>
			</td>
			<td>
			<?php
				if (  JTable::isCheckedOut($this->user->get ('id'), $row->checked_out ) ) {
					echo $row->comment;
				} else {
			?>
				<span class="editlinktip hasTip" title="<?php echo JText::_( 'Edit Media' ) .'::'. $row->title; ?>">
					<a href="<?php echo $link['edit']; ?>">
						<?php echo $row->comment; ?>
					</a>
				</span>
			<?php
				}
			?>
			</td>
			<td align="center">
				<?php echo $published;?>
			</td>
			<td class="order">
				<?php echo $row->added; ?>
			</td>
			<td>
				<span class="editlinktip hasTip" title="<?php echo JText::_( 'Filter by Item' ) .'::'. $row->content_id; ?>">
					<a href="<?php echo $link['cotid']; ?>" ><?php echo $row->container; ?></a>
				<span>
			</td>
			<td>
				<span class="editlinktip hasTip" title="<?php echo JText::_( 'Edit User' ) .'::'. $row->userid; ?>">
					<a href="<?php echo $link['user']; ?>" ><?php echo $row->author; ?></a>
				<span>
			</td>
			<td align="center">
				<?php echo $row->hits; ?>
			</td>
			<td align="center">
				<?php echo $row->id; ?>
			</td>
		</tr>
	<?php
			$k = 1 - $k;
		endfor;
	?>
	</tbody>
	</table>


	<input type="hidden" id="option" name="option" value="com_comments" />
	<input type="hidden" id="c" name="c" value="comment" />
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="boxchecked" name="boxchecked" value="0" />
	<input type="hidden" id="filter_order" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" id="filter_order_Dir" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<input type="hidden" id="section" name="section" value="<?php echo $this->section; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
