<?php
/**
 * Comments View for ... Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license        GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport( 'joomla.application.component.view' );

/**
 * Comments View
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class CommentsViewComments extends JView
{
    /**
     * Hellos view display method
     * @return void
     **/
    function display($tpl = null)
    {
		global $mainframe, $option;

		// Define
		$db		=& JFactory::getDBO();
		$uri	=& JFactory::getURI();
		
		$section 	=& comCommentsHelper::getSection()	;

		// Get data from the model
		$items		=& $this->get( 'Data');
		$total		=& $this->get( 'Total');
		$pagination =& $this->get( 'Pagination' );

		// build lists
		$lists =& $this->_buildLists(); 
		
		// Action
		$action = JRoute::_('index.php?option='.$option);
		
		// Assign References
		$this->assignRef('action',	$action);
		$this->assignRef('user',		JFactory::getUser());
		$this->assignRef('lists',		$lists);
		$this->assignRef('items',		$items);
		$this->assignRef('pagination',	$pagination);	
		$this->assignRef('section',	$section);
	
		// Display
		parent::display($tpl);
    }
	

	/**
	 * Build sort list
	 * 
	 * @access private
	 * @return 
	 */
	function &_buildLists(){
		global $mainframe, $option;
				
		// Request data
		
		$filter_state		= $mainframe->getUserStateFromRequest( $option.'filter_state',		'filter_state',		'',				'word' );
		$filter_cotid	= $mainframe->getUserStateFromRequest( $option.'cotid',		'cotid',		0,				'int' );
		$filter_order		= $mainframe->getUserStateFromRequest( $option.'filter_order',		'filter_order',		'a.date',	'cmd' );
		$filter_order_Dir = $mainframe->getUserStateFromRequest( $option.'filter_order_Dir',	'filter_order_Dir',	'',				'word' );
		
		$search				= $mainframe->getUserStateFromRequest( $option.'search',			'search',			'',				'string' );
		$search				= JString::strtolower( $search );
		$section 	=& comCommentsHelper::getSection()	;
		// declare lists
		$lists = array();
		
		// state filter
		$lists['state']	= JHTML::_('grid.state',  $filter_state );
		
		// container
		$attr['onchange'] = 'document.adminForm.submit();';
		$tbl = comCommentsHelper::section2table($section );
		$lists['contents'] = comCommentsHelper::selectList( $tbl, 'cotid', $filter_cotid, $attr );
			
		// table ordering
		$lists['order_Dir'] = $filter_order_Dir;
		$lists['order'] = $filter_order;

		// search filter
		$lists['search']= $search;
			
		return $lists;
	}

	
}