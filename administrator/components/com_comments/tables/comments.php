<?php
/**
 * Media Table for JMultimedia Component 
 * @package		Joomla
 * @subpackage	JMultimedia Suite
 * @license	GNU/GPL, see LICENSE.php
 * @link http://3den.org
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

/**
* Weblink Table class
*
* @package		Joomla
* @subpackage	Weblinks
* @since 1.0
*/
class TableComments extends JTable
{
	var $id = null;
	var $ip = null;
	var $cotid = null;
	var $userid = null;
	var $section = null;
	var $target = null;	
		
	var $title = null;
	var $comment= null;
	var $added = null;
	
	var $hits = 0;
	var $checked_out = 0;
	var $checked_out_time = 0;
	var $published = null;
	var $params = null;
	
	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 * @since 1.0
	 */
	function __construct(& $db) {
			parent::__construct('#__comments', 'id', $db);	
	}

	/**
	 * Overloaded check method to ensure data integrity
	 *
	 * @access public
	 * @return boolean True on success
	 * @since 1.0
	 */
	function check(){ 
		// check for valid name 
		if (trim($this->comment) == '') {
			$this->setError(JText::_('ROW_WAS_NOT_FOUND'));
			return false;
		}
				
		// check for valid name 
		if (trim($this->section) == '') {
			$this->setError(JText::_('ARTICLE_ADD_DISABLED') );
			return false;
		}
		
		// check for valid name 
		if( !$this->cotid ) {
			$this->setError(JText::_('Your Comment must contain a Content.'));
			return false;
		}
		
		return true;
	}

	
	/**
	 * Overloaded blindmethod
	 * 
	 * @todo improvements 
	 * @param array
	 * @return boolean	True on success
	 */
	function bind($data){
		// try to Blind data
		if( !parent::bind($data) ){
			return false;
		}
		
		$params = new JParameter( $this->params );
			
		// Set date
		$datenow =& JFactory::getDate($this->added);
		$this->added = $datenow->toMySQL();
		
		//Fix uid
		if(empty($this->userid)){
			$user	=& JFactory::getUser();
			$this->userid=$user->get('id');
			$params->set('author', $user->get('name') );
		}
		
		// set author param
		if( isset($data['author']) ){
			$params->set('author', $data['author'] );
		}
			
		// set conteiner param
		if( isset($data['conteiner']) ){
			$params->set('conteinter', $data['conteiner'] );
		}
		
		// set email param
		if( isset($data['email']) ){
			$params->set('email', $data['email'] );
		}
		
		// set url param
		if( isset($data['site']) ){
			$params->set('site', $data['site'] );
		}
			
		$this->params = $params->toString();
		return true;
	}
}
