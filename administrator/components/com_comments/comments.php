<?php
/**
 * Media Controller for JMultimedia Component
 * 
 * @package  			Joomla
 * @subpackage 	JMultimedia Suite
 * @link 				http://3den.org
 * @license		GNU/GPL
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//check plugin
jimport('joomla.plugin.helper');
if( !JPluginHelper::isEnabled('system', 'comments') ){
	JError::raiseWarning( 123, JText::_('This component requires the COMMENTS plugin'));
}

// Language
$language =& JFactory::getLanguage();
$language->load('com_comments', JPATH_SITE);

// Require the base controller
require_once( JPATH_COMPONENT.DS.'controller.php' );

// Require Helpers
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'comments.php' );

// Create the controller
$controller   = new CommentsController( );

// Perform the Request task
$controller->execute( JRequest::getVar('task') );
$controller->redirect();
