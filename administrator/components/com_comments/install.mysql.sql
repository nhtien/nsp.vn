-- phpMyAdmin SQL Dump
-- version 2.11.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 03, 2008 at 09:27 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.4
-- --------------------------------------------------------

--
-- Table structure for table `#__comments`
--

CREATE TABLE IF NOT EXISTS `#__comments` (
  `id` int(11) NOT NULL auto_increment,
  `ip` varchar(23) collate utf8_unicode_ci NOT NULL,
  `cotid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `section` varchar(255) collate utf8_unicode_ci NOT NULL,  
  `target` text collate utf8_unicode_ci NOT NULL,
 
  `title` varchar(255) collate utf8_unicode_ci NOT NULL,
  `comment` text collate utf8_unicode_ci NOT NULL,
  `added` datetime NOT NULL,
  `rank` int(11) NOT NULL,
 
  `published` tinyint(1) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `hits` int(11) NOT NULL,
  `params` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `userid` (`userid`),
  KEY `container` (`cotid`,`section`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `#__jpress_comments`
--


