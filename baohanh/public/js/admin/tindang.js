
$(document).ready(function(e) {
	/*khai báo base_url */
		var base_url=$("#base_url").val();
	/*end khai báo base url*/
	/*Tỉnh thành*/
	$("#tinhthanh_tindang").change(function (evt) {
		evt.preventDefault();
		var idtinhthanh=$(this).val();
		$.ajax({
			type: "POST",
			data: "idtinhthanh=" + idtinhthanh,
			cache: false,
			url:base_url+"nguoidung/quanlytindang/getquan/",
			dataType: "json",
			success: function(json){
				//$("#cat").empty().append(json);
				json = json || {};
				$("#idquan").empty();
				$.each(json, function(key, value) {
					$("#idquan").append(value);	
				});
			}
		});
	})
	/*end tỉnh thành*/
	
	
	/*loại danh mục*/
	$("#loaidanhmuc_tindang").change(function (evt) {
		evt.preventDefault();
		var idloaidanhmuc=$(this).val();
		$.ajax({
			type: "POST",
			data: "idloaidanhmuc=" + idloaidanhmuc,
			cache: false,
			url:base_url+"nguoidung/quanlytindang/getdanhmuc/",
			dataType: "json",
			success: function(json){
				//$("#cat").empty().append(json);
				json = json || {};
				$("#iddanhmuc").empty();
				$.each(json, function(key, value) {
					$("#iddanhmuc").append(value);	
				});
			}
		});
	})
	/*end loại danh mục*/
	
	
	// button đang tin mới\
	$("#form-dangtin").validate({ 
			rules: {
				ten_tindang:{
					required: true,
					minlength: 20,
					maxlength: 150
				}
			},
			messages: {
				ten_tindang:{
					required: "Bắt buộc nhập",
					minlength: "Tối thiểu là 20 kí tự",
					maxlength: "Tối đa 150 kí tự"
				}
			}
		});
		
		
		/*$("#form-dangtin").live("submit",function(e){
			e.preventDefault();
			var idloaitindang = $("#idloaitindang").val();
			var idloaidanhmuc = $("#idloaidanhmuc").val();
			var iddanhmuc = $("#iddanhmuc").val();
			var idtinhthanh = $("#tinhthanh_tindang").val();
			var idquan = $("#idquan").val();
			var ten_tindang = $("#ten_tindang").val();
			var noidung_tindang = $("#noidung_tindang").val();
			var idkieutindang = $("#idkieutindang").val();
			var thoigiantontai_tindang = $("#thoigiantontai_tindang").val();
			$.post(base_url+"nguoidung/quanlytindang/check_dangtin",
			{
				 idloaitindang:idloaitindang,
				 idloaidanhmuc:idloaidanhmuc,
				 iddanhmuc:iddanhmuc,
				 idtinhthanh:idtinhthanh,
				 idquan:idquan,
				 idkieutindang:idkieutindang,
				 ten_tindang:ten_tindang,
				 noidung_tindang:noidung_tindang,
				 thoigiantontai_tindang:thoigiantontai_tindang
			 
			},
			function(msg) 
			{
			 //alert(msg.result);
				 if(msg.result==1)
				 {
					$("#error").html(msg.messages);
					$("#error").css("color","#09c");
                    $('html,body').animate({ scrollTop: $('.box').offset().top }, 'slow');
				 }
				 else
				 {
					$("#error").html(msg.messages);
                    $('html,body').animate({ scrollTop: $('.box').offset().top }, 'slow');
				 }
					//alert(response);
				},'json'); //end function (response)
		});*/
	/*end đăng tin mới*/
	
	/*lam moi tin dang*/
	//Click the button event!
		$(".wrapper-body-left a#lammoi-tindang").click(function(e){
			e.preventDefault();
			var idTinDang=$(this).attr("href");
			$("#idTinDang").val(idTinDang);
			$.post(base_url+"nguoidung/quanlytindang/lammoitindang/"+idTinDang+"");
			//centering with css
			centerPopup_lammoi();
			//load popup
			loadPopup_lammoi();
			
		});
		//Click out event!
		$("#backgroundPopup").live("click",function(){
			disablePopup_lammoi();
		});
		//btn lam moi
		$(".btn-lammoi").click(function(e){
			e.preventDefault();
			thoigiantontai_tindang=$("#thoigiantontai_tindang").val();
			$.post(base_url+'nguoidung/quanlytindang/check_lammoitindang',
			{
				 thoigiantontai_tindang:thoigiantontai_tindang
			},
			function(msg)
			{
				if(msg.result==1)
				 {
					alert(msg.messages);
					window.location = base_url+'quan-ly-tin-dang';
				 }
			},'json'); //end function (response)
		})
	/*end lam moi tin dang*/
	
});
