$(document).ready(function(e) {
	/*datepicker*/
	$("#datepicker" ).datepicker({
	  changeMonth: true,
	  changeYear: true,
	  yearRange: "1930:2020"
	});
	$("#datepicker1" ).datepicker({
	  changeMonth: true,
	  changeYear: true,
	  yearRange: "1930:2020"
	});
/*end datepiker*/
	var URL=$("#base_url").val();
	//datatables
	oTable = $('.datatables').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		'iDisplayLength': 20
	});
	//end datatables
	//tab jquery
	$('.vtabs a').tabs();
	//end tab jquery
});
/*function save*/
function submitbutton(action){
	$("#t").val(action);
	$("#adminForm").submit();
}

function submitOrdering(id,stt,action){
	$("#stt").val(stt);
	$("#t").val(id);
	var URL=$("#base_url").val();
	$('#from-admin').attr('action', URL+'admin/'+action);
	$('#from-admin').submit();
}

function submitOrderingSub(id,stt,catid,action){
	$("#catid").val(catid);
	$("#stt").val(stt);
	$("#t").val(id);
	var URL=$("#base_url").val();
	$('#from-admin').attr('action', URL+'admin/'+action);
	$('#from-admin').submit();
}
/*end function save*/
/*fancybox*/
$("a[rel=example_group]").fancybox({
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'titlePosition' 	: 'over',
			'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
				return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
			}
		});
/*end fancybox*/
$.fn.tabs = function() {
	var selector = this;
	
	this.each(function() {
		var obj = $(this); 
		
		$(obj.attr('href')).hide();
		
		$(obj).click(function() {
			$(selector).removeClass('selected');
			
			$(selector).each(function(i, element) {
				$($(element).attr('href')).hide();
			});
			
			$(this).addClass('selected');
			
			$($(this).attr('href')).show();
			
			return false;
		});
	});

	$(this).show();
	
	$(this).first().click();
};

/*checkbox delete tat ca*/
	function toggle(source){
		checkboxes=document.getElementsByName('delete[]');
		for(var i=0,n=checkboxes.length;i<n;i++){
		checkboxes[i].checked=source.checked;
		}
	}
/*end checkbox delete tat ca*/

/*checkbox add product typcial*/
	function toggle_add(source){
		checkboxes=document.getElementsByName('add[]');
		for(var i=0,n=checkboxes.length;i<n;i++){
		checkboxes[i].checked=source.checked;
		}
	}
/*end add product typcial*/
