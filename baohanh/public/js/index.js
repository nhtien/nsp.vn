
jQuery(document).ready(function(e) {
/*Biến base_url được lấy từ trang chủ main.php - dùng chung cho tất cả các trang js*/
	var base_url=$("#base_url").val();
/*end Biến base_url được lấy từ trang chủ main.php - dùng chung cho tất cả các trang js*/

/*scrolto top*/
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('#back-top').fadeIn();
		} else {
			$('#back-top').fadeOut();
		}
	}); 
	
	$('#back-top').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});
/*end scrollto top*/


//tìm kiếm
	$("#form-timkiem").live("submit",function(e){
		e.preventDefault();
		var searchstring=$(".searchstring").val();
		$.post(base_url+"search/index",
		{
			searchstring:searchstring
		},
		function(msg){
		$("#content .wrapper").html(msg)});
	})
//end tìm kiếm
//email
	function validateEmail(email) 
	{
		var re = /\S+@\S+\.\S+/;
		return re.test(email);
	}
	$("#form-email").live("submit",function(e){
		e.preventDefault();
		var name_email=$("#name_email").val();
		if(!validateEmail(name_email))
		{
			alert("Email không đúng định dạng");
			return false;
		}
		$.post(base_url+"email",
		{
			name_email:name_email
		},
		function(msg){
			alert("Thông tin của bạn đã được gửi thành công. Cảm ơn bạn đã tham gia!");
		})
	})
//end email
//tìm kiếm
	$("#form-limit-use").live("submit",function(e){
		e.preventDefault();
		var limit_use_string=$(".limit_use_string").val();
		var mid=$(".mid").val();
		$.post(base_url+"check_limit_use/index",
		{
			limit_use_string:limit_use_string,
			mid:mid
		},
		function(msg){
		$(".body-limit-use").html(msg)});
	})
//end tìm kiếm
});

/*checkbox delete tat ca*/
	function toggle(source){
		checkboxes=document.getElementsByName('delete[]');
		for(var i=0,n=checkboxes.length;i<n;i++){
		checkboxes[i].checked=source.checked;
		}
	}
/*end checkbox delete tat ca*/
