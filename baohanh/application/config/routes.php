<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
/*cau hinh trong admin*/
$route['administrator'] = "administrator/index";
$route['administrator/check_admin_login'] = "administrator/check_admin_login";
$route['admin/thoat'] = "admin/thoat";
$route['admin/(:any)'] = "admin/$1";
/*end cai hinh trong admin*/
$route['languages/index'] = "languages/index";
$route['languages/index/(:any)'] = "languages/index/$1";
/*cấu hình router để rewrite*/
$route['product/download-file/(:any)/(:any)'] = "product/download_file_home/$1/$2";
$route['search/index'] = "search/index";
/** product */

$route['(?i)products'] = "product/index";
$route['(?i)san-pham'] = "product/index";
$route['(?i)san_pham'] = "product/index";
$route['(:any)/(:num)/(:num)/(:any)'] = "product/products/$1/$2/$3/$4";
$route['(:num)/(:any)'] = "product/productsCategories/$1/$2";
$route['product/export-file/(:num)'] = "product/export_file/$1";
/*solution*/
$route['(?i)solution/(:num)/(:any)'] = "solution/solutions/$1/$2";
$route['(?i)solution'] = "solution/index";
$route['(?i)giai-phap'] = "solution/index";
$route['(?i)giai_phap'] = "solution/index";
$route['(?i)giai_phap/(:num)/(:any)'] = "solution/solutions/$1/$2";
$route['(?i)giai-phap/(:num)/(:any)'] = "solution/solutions/$1/$2";
/*warranty check*/
$route['(?i)tra-cuu-han-su-dung'] = "limit_use/index";
$route['(?i)warranty-check'] = "limit_use/index";
$route['(?i)check_limit_use/index'] = "check_limit_use/index";
/*contact*/
$route['(?i)contact'] = "contact/index";
$route['(?i)contact/check_sentmail'] = "contact/check_sentmail";
$route['(?i)lien-he'] = "contact/index";
$route['(?i)lien_he'] = "contact/index";
/*articles*/
$route['(:any)'] = "article/index/$1";
$route['(:any)/(:any)'] = "article/index/$1/$2";
/*recived email*/
$route['email'] = "email/index";
/********* end thiết lập cho nguoi dung ********************/
$route['sitemap\.xml'] = "sitemap";
$route['default_controller'] = "main";
$route['404_override'] = '';
/* End of file routes.php */
/* Location: ./application/config/routes.php */