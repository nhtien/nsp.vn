<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isset($_SESSION))@session_start();
class Application extends CI_Controller{ 
   public $_data = array(); 
   function __construct(){ 
        parent::__construct();
/*		$this->_data['title']=$this->config->item("title_index");
		$this->_data['description']=$this->config->item("description");
		$this->_data['keyword']=$this->config->item("keyword");*/
		/*hepler*/
			$this->load->helper("getalias");
			/*models application*/
				$this->load->Model("m_application");
			/*get config*/
				$getConfig=$this->m_application->getConfig();
				foreach($getConfig as $config)
				{
					if($config->name=='site_meta_keywords')
					{
						$this->_data['keywords']=$config->value;
					}
					if($config->name=='site_meta_description')
					{
						$this->_data['description']=$config->value;
					}
					if($config->name=='site_title')
					{
						$this->_data['title']=$config->value;
					}
				}	
			/*load helper languages*/
			if($this->check_languages()=='_en')
			{
				$this->load->helper("languages_en");
			}
			else
			{
				$this->load->helper("languages_vi");
			}
			
     }
	 
	 /*check languages*/
		public function check_languages(){
			if($this->session->userdata("languages"))
			{
				if($this->session->userdata("languages")=='vi')
				{
					$languages='';
				}
				else
				{
					$languages='_en';
				}
				return $languages;
			}
			else
			{
				$languages=$this->m_application->check_languages();
				return $languages;
			}
		}
	/*end check languages*/
}
?>