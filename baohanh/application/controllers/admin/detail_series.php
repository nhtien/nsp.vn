<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isset($_SESSION))@session_start();
require_once(APPPATH . 'controllers/admin/getalias.php');
class Detail_series extends Getalias{
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
		parent::__construct();
		$this->_data['detail_series']=1;
		//$this->_data['detail_series_parent']=1;	
		$this->_data['limit_use_default']=1;	
		/*hepler*/
			$this->load->helper("url");
			$this->load->helper("getalias");		
		/*Load model*/
			$this->load->Model("admin/detail_series/m_detail_series");
			$this->_gallery_url = base_url()."public/images";
	}
	
	public function index($limitid)
	{
		//sent data
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/detail_series/main';
		//get data
			$this->_data['listDetailSeries']=$this->m_detail_series->listDetailSeries($limitid);
			$this->_data['getLimitUse']=$this->m_detail_series->getLimitUse($limitid);
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function add_detail_series($limitid)
	{
		//sent data
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['limitid']=$limitid;
			$this->_data['template']='admin/bodyright/detail_series/addDetail_Series';
		//get data
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function check_add_detail_series($idLimitUse)
	{					
		//sent data
			$this->_data['title']=$this->config->item("title_index");
			$action=$this->input->post('t');
		//get data
			$name_detail_series=$this->input->post('name_detail_series');
			$number_detail_series=$this->input->post('number_detail_series');
			$enable_detail_series=$this->input->post('default_detail_series');
			$data=array(
			'idDetailSeries'   => 'NULL',
			'limitid' => $idLimitUse,
			'name_detail_series' => $name_detail_series,
			'number_detail_series' => $number_detail_series,		
			'enable_detail_series' =>$enable_detail_series
			);
			$this->m_detail_series->addDetail_Series($data);
			if($action=='save')
			{
				$this->_data['template']='admin/bodyright/detail_series/addDetail_Series';
			}
			else
			{
				$this->_data['listDetailSeries']=$this->m_detail_series->listDetailSeries($idLimitUse);
				$this->_data['getLimitUse']=$this->m_detail_series->getLimitUse($idLimitUse);
				$this->_data['template']='admin/bodyright/detail_series/main';
			}
			$this->_data['limitid']=$idLimitUse;
			$this->_data['messages']='Thêm series thành công.';
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function edit_detail_series($idDetailSeries,$idLimitUse)
	{
		//sent data
			
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/detail_series/editDetail_Series';
		//get data
			$this->_data['limitid']=$idLimitUse;
			$this->_data['getDetailSeries']=$this->m_detail_series->getDetailSeries($idDetailSeries);
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function check_edit_detail_series($idLimitUse,$idDetailSeries)
	{
			$this->_data['title']=$this->config->item("title_index");
			$action=$this->input->post('t');
			//get data
			$name_detail_series=$this->input->post('name_detail_series');
			$number_detail_series=$this->input->post('number_detail_series');
			$enable_detail_series=$this->input->post('default_detail_series');
			$data=array(
			'limitid' => $idLimitUse,
			'name_detail_series' => $name_detail_series,
			'number_detail_series' => $number_detail_series,		
			'enable_detail_series' =>$enable_detail_series
			);
			$this->m_detail_series->editDetail_Series($idDetailSeries,$data);
			if($action=='save')
			{	$this->_data['limitid']=$idLimitUse;
				$this->_data['getDetailSeries']=$this->m_detail_series->getDetailSeries($idDetailSeries);
				$this->_data['template']='admin/bodyright/detail_series/addDetail_Series';
			}
			else
			{
				$this->_data['getLimitUse']=$this->m_detail_series->getLimitUse($idLimitUse);
				$this->_data['listDetailSeries']=$this->m_detail_series->listDetailSeries($idLimitUse);
				$this->_data['template']='admin/bodyright/detail_series/main';
			}
			$this->_data['messages']='Sữa series thành công.';
			
			$this->load->view('admin/main',$this->_data);
		
	}
	public function enable($enable,$id,$limitid)
	{
		//sent data
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/detail_series/main';
		//get data
			$this->m_detail_series->enable($enable,$id);
			$this->_data['listDetailSeries']=$this->m_detail_series->listDetailSeries($limitid);
			$this->_data['getLimitUse']=$this->m_detail_series->getLimitUse($limitid);
			$this->load->view('admin/main',$this->_data);
		
	}
	
		public function removeDetail_Series($idLimitUse)
		{
			if(isset($_POST["btnDeleteall"]))
			{
				if(empty($_POST['delete']))
				redirect('admin/detail_series/index/'.$idLimitUse.'');
				foreach($_POST['delete'] as $id)
				{
					$this->m_detail_series->removedetail_series($id);
				}
				redirect('admin/detail_series/index/'.$idLimitUse.'');
			}
		}
}