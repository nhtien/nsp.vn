<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isset($_SESSION))@session_start();
require_once(APPPATH . 'controllers/admin/getalias.php');
class Products_manufacture extends Getalias{
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
		parent::__construct();
		$this->_data['limit_use_default']=1;	
		$this->_data['products_manufacture']=1;	
		/*hepler*/
			$this->load->helper("url");
			$this->load->helper("getalias");		
		/*Load model*/
			$this->load->Model("admin/m_products_manufacture");
			$this->_gallery_url = base_url()."public/images";
	}
	
	public function index()
	{
		//sent data
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/products_manufacture/main';
		//get data
			$this->_data['listProductsManufacture']=$this->m_products_manufacture->listProductsManufacture();
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function add_products_manufacture()
	{
		//sent data
			
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/products_manufacture/addProductsManufacture';
		//get data
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function check_add_products_manufacture()
	{					
		//sent data
			$this->_data['title']=$this->config->item("title_index");
			$action=$this->input->post('t');
		//get data
			$name_products_manufacture=$this->input->post('name_products_manufacture');
			$name_en_products_manufacture=$this->input->post('name_en_products_manufacture');
			$description_products_manufacture=$this->input->post('description_products_manufacture');
			$description_en_products_manufacture=$this->input->post('description_en_products_manufacture');
			$enable_products_manufacture=$this->input->post('enable_products_manufacture');
			$meta_title_products_manufacture=$this->input->post('meta_title_products_manufacture');
			$meta_key_products_manufacture=$this->input->post('meta_key_products_manufacture');
			$meta_desc_products_manufacture=$this->input->post('meta_desc_products_manufacture');
			$ordering_products_manufacture=$this->input->post('ordering_products_manufacture');
			$data=array(
			'idProductsManufacture'   => 'NULL',
			'name_products_manufacture' => $name_products_manufacture,
			'name_en_products_manufacture' => $name_en_products_manufacture,
			'alias_products_manufacture' => getAlias($name_products_manufacture),
			'alias_en_products_manufacture' => getAlias($name_en_products_manufacture),
			'description_products_manufacture' => $description_products_manufacture,
			'description_en_products_manufacture' => $description_en_products_manufacture,
			'enable_products_manufacture' => $enable_products_manufacture,
			'ordering_products_manufacture' => $ordering_products_manufacture,		
			'meta_title_products_manufacture' =>$meta_title_products_manufacture,
			'meta_key_products_manufacture' =>$meta_key_products_manufacture,
			'meta_desc_products_manufacture' =>$meta_desc_products_manufacture
			);
			if($action=='save')
			{
				$this->_data['template']='admin/bodyright/products_manufacture/addProductsManufacture';
			}
			else
			{
				$this->_data['template']='admin/bodyright/products_manufacture/main';
			}
			$this->_data['messages']='Thêm products_manufacture thành công.';
			$this->m_products_manufacture->addProductsManufacture($data);
			$this->_data['listProductsManufacture']=$this->m_products_manufacture->listProductsManufacture();
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function edit_products_manufacture($idProductsManufacture)
	{
		//sent data
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/products_manufacture/editProductsManufacture';
		//get data
			$this->_data['getProductsManufacture']=$this->m_products_manufacture->getProductsManufacture($idProductsManufacture);
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function check_edit_products_manufacture($idProductsManufacture)
	{
			$this->_data['title']=$this->config->item("title_index");
			$action=$this->input->post('t');
			//get data
			$name_products_manufacture=$this->input->post('name_products_manufacture');
			$name_en_products_manufacture=$this->input->post('name_en_products_manufacture');
			$description_products_manufacture=$this->input->post('description_products_manufacture');
			$description_en_products_manufacture=$this->input->post('description_en_products_manufacture');
			$enable_products_manufacture=$this->input->post('enable_products_manufacture');
			$meta_title_products_manufacture=$this->input->post('meta_title_products_manufacture');
			$meta_key_products_manufacture=$this->input->post('meta_key_products_manufacture');
			$meta_desc_products_manufacture=$this->input->post('meta_desc_products_manufacture');
			$ordering_products_manufacture=$this->input->post('ordering_products_manufacture');
			$data=array(
			'name_products_manufacture' => $name_products_manufacture,
			'name_en_products_manufacture' => $name_en_products_manufacture,
			'alias_products_manufacture' => getAlias($name_products_manufacture),
			'alias_en_products_manufacture' => getAlias($name_en_products_manufacture),
			'description_products_manufacture' => $description_products_manufacture,
			'description_en_products_manufacture' => $description_en_products_manufacture,
			'enable_products_manufacture' => $enable_products_manufacture,
			'ordering_products_manufacture' => $ordering_products_manufacture,		
			'meta_title_products_manufacture' =>$meta_title_products_manufacture,
			'meta_key_products_manufacture' =>$meta_key_products_manufacture,
			'meta_desc_products_manufacture' =>$meta_desc_products_manufacture
			);
			if($action=='save')
			{
				$this->_data['template']='admin/bodyright/products_manufacture/editProductsManufacture';
			}
			else
			{
				$this->_data['template']='admin/bodyright/products_manufacture/main';
			}
			$this->_data['messages']='Sữa products_manufacture thành công.';
			$this->m_products_manufacture->editProductsManufacture($idProductsManufacture,$data);
			$this->_data['listProductsManufacture']=$this->m_products_manufacture->listProductsManufacture();
			
			$this->_data['getProductsManufacture']=$this->m_products_manufacture->getProductsManufacture($idProductsManufacture);
			$this->load->view('admin/main',$this->_data);
		
	}
	//enable products_manufacture
	public function enable($enable,$id)
	{
		//sent data
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/products_manufacture/main';
		//get data
			$this->m_products_manufacture->enable($enable,$id);
			$this->_data['listProductsManufacture']=$this->m_products_manufacture->listProductsManufacture();
			$this->load->view('admin/main',$this->_data);
		
	}
	//end enable products_manufacture
	/*Delete products_manufacture*/
		public function removeProductsManufacture()
		{
			if(isset($_POST["btnDeleteall"]))
			{
				if(empty($_POST['delete']))
				redirect('admin/products_manufacture');
				foreach($_POST['delete'] as $id)
				{
					//remove don hang
					$this->m_products_manufacture->removeProductsManufacture($id);
				}
				redirect("admin/products_manufacture");
			}
		}
	/*end delete products_manufacture*/
}