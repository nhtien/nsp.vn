<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isset($_SESSION))@session_start();
require_once(APPPATH . 'controllers/admin/getalias.php');
class Limit_use extends Getalias{
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
		parent::__construct();
			
		$this->_data['limit_use_default']=1;	
		/*hepler*/
			$this->load->helper("url");
			$this->load->helper("getalias");		
		/*Load model*/
			$this->load->Model("admin/limit_use/m_limit_use");
			$this->_gallery_url = base_url()."public/images";
	}
	
	public function index()
	{
		//sent data
			$this->_data['list']=1;
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/main';
			//get data
			$this->_data['listLimitUse']=$this->m_limit_use->listLimitUse();
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function add_limit_use()
	{
		//sent data
			$this->_data['list']=1;
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/addLimitUse';
			$this->_data['listManufacture']=$this->m_limit_use->listManufacture();
			//get data
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function check_add_limit_use()
	{					
		//sent data
			$this->_data['list']=1;
			$this->_data['title']=$this->config->item("title_index");
			$action=$this->input->post('t');
			//get data
			$company_limit_use=$this->input->post('company_limit_use');
			$mid=$this->input->post('mid');
			$oid=$this->input->post('oid');
			$code_limit_use=$this->input->post('code_limit_use');
			$name_limit_use=$this->input->post('name_limit_use');
			
			$date_start_limit_use=strtotime($this->input->post('date_start_limit_use'));
			$date_start_limit_use=date('Y-m-d',$date_start_limit_use);
			$date_end_limit_use=strtotime($this->input->post('date_end_limit_use'));
			$date_end_limit_use=date('Y-m-d',$date_end_limit_use);
			$description_limit_use=$this->input->post('description_limit_use');
			
			$enable_limit_use=$this->input->post('enable_limit_use');
			$data=array(
			'idLimitUse'   => 'NULL',
			'oid' => $oid,
			'mid' => $mid,
			'company_limit_use' => $company_limit_use,
			'code_limit_use' => $code_limit_use,
			'name_limit_use' => $name_limit_use,
			
			'description_limit_use' => $description_limit_use,
			
			'date_start_limit_use' => $date_start_limit_use,
			'date_end_limit_use' => $date_end_limit_use,
			'enable_limit_use' => $enable_limit_use
			);
			if($action=='save')
			{
				$this->_data['template']='admin/bodyright/addLimitUse';
			}
			else
			{
				$this->_data['template']='admin/bodyright/main';
			}
			$this->_data['messages']='Thêm hạn sử dụng thành công.';
			
			$this->m_limit_use->addLimitUse($data);
			$this->_data['listLimitUse']=$this->m_limit_use->listLimitUse();
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function edit_limit_use($idLimitUse)
	{
		//sent data
			$this->_data['list']=1;
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/editLimitUse';
			//get data
			$this->_data['getLimitUse']=$this->m_limit_use->getLimitUse($idLimitUse);
			$this->_data['listManufacture']=$this->m_limit_use->listManufacture();
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function check_edit_limit_use($idLimitUse)
	{
			$this->_data['list']=1;
			$this->_data['title']=$this->config->item("title_index");
			$action=$this->input->post('t');
			//get data
			
			$company_limit_use=$this->input->post('company_limit_use');
			$mid=$this->input->post('mid');
			$oid=$this->input->post('oid');
			$code_limit_use=$this->input->post('code_limit_use');
			$name_limit_use=$this->input->post('name_limit_use');
			
			$date_start_limit_use=strtotime($this->input->post('date_start_limit_use'));
			$date_start_limit_use=date('Y-m-d',$date_start_limit_use);
			$date_end_limit_use=strtotime($this->input->post('date_end_limit_use'));
			$date_end_limit_use=date('Y-m-d',$date_end_limit_use);
			$description_limit_use=$this->input->post('description_limit_use');
			
			$enable_limit_use=$this->input->post('enable_limit_use');
			$data=array(
			'oid' => $oid,
			'mid' => $mid,
			'company_limit_use' => $company_limit_use,
			'code_limit_use' => $code_limit_use,
			'name_limit_use' => $name_limit_use,
			
			'description_limit_use' => $description_limit_use,
			
			'date_start_limit_use' => $date_start_limit_use,
			'date_end_limit_use' => $date_end_limit_use,
			'enable_limit_use' => $enable_limit_use
			);
			$this->m_limit_use->editLimitUse($idLimitUse,$data);
			if($action=='save')
			{
				$this->_data['getLimitUse']=$this->m_limit_use->getLimitUse($idLimitUse);
				$this->_data['listManufacture']=$this->m_limit_use->listManufacture();
				$this->_data['template']='admin/bodyright/editLimitUse';
			}
			else
			{
				$this->_data['template']='admin/bodyright/main';
			}
			$this->_data['messages']='Sữa hạn sử dụng thành công.';
			
			$this->_data['listLimitUse']=$this->m_limit_use->listLimitUse();
			
			$this->_data['getLimitUse']=$this->m_limit_use->getLimitUse($idLimitUse);
			$this->load->view('admin/main',$this->_data);
		
	}
	//enable limit_use
	public function enable($enable,$id)
	{
		//sent data
			$this->_data['list']=1;
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/main';
			//get data
			$this->m_limit_use->enable($enable,$id);
			
			$this->_data['listLimitUse']=$this->m_limit_use->listLimitUse();
			$this->load->view('admin/main',$this->_data);
		
	}
	//end enable limit_use
	/*Delete hạn sử dụng*/
		public function removeLimitUse()
		{
			if(isset($_POST["btnDeleteall"]))
			{
				if(empty($_POST['delete']))
				redirect('admin/limit_use');
				foreach($_POST['delete'] as $id)
				{
					//remove don hang
					$this->m_limit_use->removeLimitUse($id);
				}
				redirect("admin/limit_use");
			}
		}
	/*end delete hạn sử dụng*/
	
	
	public function import()
	{
		//sent data
			$this->_data['import']=1;	
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/import';
			
			
			//get data
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function export()
	{
		//sent data
			$this->_data['export']=1;	
			$this->_data['title']=$this->config->item("title_index");
			$this->_data['template']='admin/bodyright/export';
			
			$this->_data['listManufacture']=$this->m_limit_use->listManufacture();
			//get data
			$this->load->view('admin/main',$this->_data);
		
	}
	
	public function check_import()
	{
		$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
		if( $ext !== 'xls' && $ext !== 'xlsx')
		{
				$this->_data['messages']="File không đúng định dạng!";
		}
		else
		{
			$this->_path = realpath(APPPATH. "../public/import");
			move_uploaded_file($_FILES["file"]["tmp_name"],$this->_path."/".$_FILES["file"]["name"]);
			$this->load->helper('excel_reader/excel_reader.php'); // Thư viện xử lý
				$data = new Spreadsheet_Excel_Reader($this->_path."/".$_FILES['file']['name'],false,"UTF-8"); // Đọc file excel, hỗ trợ Unicode UTF-8
				$rowsnum = $data->rowcount($sheet_index=0); // Số hàng của sheet
				$colsnum =  $data->colcount($sheet_index=0);  //Số cột của sheet
				$count=1;
				for($i=3;$i<=$rowsnum;$i++) // Duyệt từng hàng, bắt đầu lấy dữ liệu từ hàng 2
				{
					//echo $data->val($i,4).'<br>';
					
					/*----------------------------------------------
					/* Lưu dữ liệu vào DB
					/*---------------------------------------------*/
					list($d,$m,$y)=explode('/',$data->val($i,3));
					$mk=mktime(0,0,0,$d,$m,$y);
					$date_start=date('Y-m-d',$mk);
					list($d,$m,$y)=explode('/',$data->val($i,11));
					$mk=mktime(0,0,0,$d,$m,$y);
					$date_end=date('Y-m-d',$mk);
					//select limitUse manufacture
					$getProductsManufacture=$this->m_limit_use->getProductsManufacture($data->val($i,6));
					mysql_query("insert into pt_limit_use values('NULL',
					'".$data->val($i,2)."',
					'".$getProductsManufacture[0]->idProductsManufacture."',
					'".$data->val($i,4)."',
					'".$data->val($i,5)."',
					'".$data->val($i,7)."',
					'".$date_start."',
					'".$date_end."',
					'".$data->val($i,12)."',
					'1'
					)");
					//insert in detail series
					$limitid=mysql_insert_id();
					//explode string series
					$string_series=explode("\n",$data->val($i,9));
					for($j=0; $j<count($string_series); $j++)
					{
						mysql_query("insert into pt_detail_series values('NULL',
						'".$limitid."',
						'".$string_series[$j]."',
						'".$data->val($i,10)."',
						'1'
						)");
					}
					
				}
				$this->_data['messages']="Import file thành công!";
		}
		$this->_data['import']=1;	
		$this->_data['title']=$this->config->item("title_index");
		$this->_data['template']='admin/bodyright/import';
		//get data
		$this->load->view('admin/main',$this->_data);
		
	}
	
	
	public function check_export()
	{
		$mid=$this->input->post("mid");
		$date_start_limit_use=$this->input->post("date_start_limit_use");
		$date_end_limit_use=$this->input->post("date_end_limit_use");
		$listLimitUse=$this->m_limit_use->listExport($mid,$date_start_limit_use,$date_end_limit_use);
		//export
			$this->load->library("Classes/PHPExcel");
			$date=date("d/m/Y");
			list($d,$m,$y)=explode('/',$date);
			$objPHPExcel = new PHPExcel();
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'HTML');
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$this->_path = realpath(APPPATH. "../public/export");
			$objPHPExcel = $objReader->load($this->_path."/mau_export_excel.xls");
			$objPHPExcel->getActiveSheet()
				->setCellValue('M1', 'Ngày xuất : '.$d.' Tháng '.$m.' Năm '.$y.'');
				$i=3;
				$stt=1;
			$objPHPExcel->getActiveSheet()
				->setCellValue('N1', ' Từ ngày bảo hành : '.$date_start_limit_use.'');
				$i=3;
				$stt=1;
			$objPHPExcel->getActiveSheet()
				->setCellValue('O1', ' Đến ngày hết hạn : '.$date_end_limit_use.'');
				$i=3;
				$stt=1;
				$styleArray = array(
				'font'  => array(
				'bold'  => false
				));
				$styleArrayParent = array(
				'font'  => array(
				'bold'  => false
				));
				foreach($listLimitUse as $limitUse)
				{ 
					
						$listSeries="";
						$stt=0;
						foreach($limitUse->idLimitUses as $serie)
						{
							if($stt==count($limitUse->idLimitUses)-1)
							{
								$listSeries.=$serie->name_detail_series;
							}
							else
							{
								$listSeries.=$serie->name_detail_series."\n";
							}
							$stt++;
						}
						$objPHPExcel->getActiveSheet()->getStyle('A')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('B')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('C')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('D')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('E')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('F')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('G')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('H')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('I')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('J')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('K')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('L')->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('I')->getAlignment()->setWrapText(true);
						$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $limitUse->date_start_limit_use)
							->setCellValue('B'.$i, $limitUse->oid)
							->setCellValue('C'.$i, $limitUse->date_start_limit_use)
							->setCellValue('D'.$i, $limitUse->company_limit_use)
							->setCellValue('E'.$i, $limitUse->code_limit_use)
							->setCellValue('F'.$i, $limitUse->name_products_manufacture)
							->setCellValue('G'.$i, $limitUse->name_limit_use)
							->setCellValue('H'.$i, count($limitUse->idLimitUses))
							->setCellValue('I'.$i, $listSeries)
							->setCellValue('J'.$i, $limitUse->idLimitUses[0]->number_detail_series)
							->setCellValue('K'.$i, $limitUse->date_end_limit_use)
							->setCellValue('L'.$i, $limitUse->description_limit_use);
							$i++;
						$stt++;	
				}
			// Rename sheet
			$objPHPExcel->getActiveSheet()->setTitle('Danh sách bảo hành');
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			// Redirect output to a client's web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="listlimitUse.xls"');
			header('Cache-Control: max-age=0');
			
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		//end export
			$this->load->view('admin/main',$this->_data);
		
	}
	
	/*download file default*/
	public function download_file_default(){
		
		//load the download helper
		$this->load->helper('download');
		//set the textfile's content 
		$name_file="import_default.xls";
		$data = file_get_contents("./public/import/import_default/".$name_file.""); // Read the file's contents
		//use this function to force the session/browser to download the created file
		force_download($name_file, $data);
	}
	/*end download file default*/
	
	
	
}