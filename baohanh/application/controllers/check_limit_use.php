<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isset($_SESSION))@session_start();
require_once(APPPATH . 'controllers/application.php');
class Check_limit_use extends Application{
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
		parent::__construct();
		/*hepler*/
			$this->load->helper("url");
		/*session*/
			$this->load->library('session');
		/*Load model*/
			//tin hot
			$this->load->Model("body/check_limit_use/m_check_limit_use");
		
		
	}
	public function index()
	{
		$limit_use_string=$this->input->post("limit_use_string");
		$mid=$this->input->post("mid");
		$this->_data['listLimitUse']=$this->m_check_limit_use->listLimitUse($limit_use_string,$mid);
		$this->load->view('body/limit_use/getLimitUse',$this->_data);
	}
}
