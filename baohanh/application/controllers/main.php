<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!isset($_SESSION))@session_start();
require_once(APPPATH . 'controllers/application.php');
class Main extends Application{
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
		parent::__construct();
		/*data menu*/
			
		/*hepler*/
			$this->load->helper("url");
		/*session*/
			$this->load->library('session');
			
		/*Load model*/
		$this->_data['home']=1;
		$this->_data['nothome']=1;
		
		
	}
	public function index()
	{
		
		/*check_maintenace*/
		$this->load->Model("body/m_application");
		$getConfig=$this->m_application->getConfig();
		foreach($getConfig as $config)
		{
		 if($config->name=='siteoffline'){
			 $siteoffline=$config->value;
		 }
		 if($config->name=='offlinemsg'){
			 $offlinemsg=$config->value;
		 }
		}
		if($siteoffline==1)
		{
			$this->_data['error']=$offlinemsg;
			$this->load->view('maintenance',$this->_data);
		}
		else
		{
			$this->_data['listProductsManufacture']=$this->m_application->listProductsManufacture();
			$this->_data['template']='body/main';
			$this->load->view('index',$this->_data);
		}
		/*end check_maintenace*/	
	}
}
