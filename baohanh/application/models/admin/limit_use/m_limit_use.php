<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Author: Jorge Torres

 * Description: Login model class

 */

class M_limit_use extends CI_Model{

    function __construct(){

        parent::__construct();

		$this->load->database();

			$this->load->helper("url");

			$this->_gallery_path = realpath(APPPATH. "../public/images/products/".date("m_Y"));

    }

    /*list limit_use*/

		public function listLimitUse(){

		$limit_use_list=$this->db->query('select * from pt_limit_use a, pt_products_manufacture b where a.mid=b.idProductsManufacture order by idLimitUse');

		$listSeries = array();

		foreach ($limit_use_list->result() as $series){

			$option_list = $this->db->get_where('pt_detail_series', array('limitid'=>$series->idLimitUse));

			$series->idLimitUses = $option_list->result();

			$listMenusParent[] = $series;

  		}

		$listSeries = $limit_use_list->result();

		return $listSeries;

    }

	/*end list limit_use*/

	/*get đơn limit_use */

		public function getLimitUse($idLimitUse){

			$querylimit_use=$this->db->query(

			'select *

			from pt_limit_use

			where idLimitUse='.$idLimitUse.'');

			$getLimitUse=$querylimit_use->result();

			

			return $getLimitUse;

		}

	/*end đơn limit_use */

	

	/*get đơn limit_use */

		public function listManufacture(){

			$listManufacture=$this->db->query(

			'select *

			from pt_products_manufacture');

			$listManufacture=$listManufacture->result();

			return $listManufacture;

		}

	/*end đơn limit_use */

	

	/*add limit_use */

		public function addLimitUse($data){

			$this->db->insert('pt_limit_use', $data);

		}

	/*end add limit_use */

	

	/*add limit_use import */

		public function addLimitUseImport($data){

			$this->db->query($data);

		}

	/*end add limit_use import */

	

	/*edit limit_use */

		public function editLimitUse($idLimitUse,$data){

			$this->db->where("idLimitUse",$idLimitUse);

			$this->db->update('pt_limit_use', $data);

		}

	/*end edit limit_use */

	

	/*remove limit_use*/

	public function removeLimitUse($idLimitUse)



	{
		$this->db->where("limitid",$idLimitUse);

		$this->db->delete("pt_detail_series");
		
		$this->db->where("idLimitUse",$idLimitUse);

		$this->db->delete("pt_limit_use");

	}

	/*end remove limit_use*/



	/*enable limit_use */

		public function enable($status,$id){

			if($status==0)

			$status=1;

			else

			$status=0;

			$qr = $this->db->query("UPDATE pt_limit_use SET enable_limit_use=".$status." WHERE idLimitUse=".$id."" );

		}

	/*end enable limit_use */

	

	

	/*list import */

		public function getProductsManufacture($name_products_manufacture){

			$queryimport=$this->db->query("select idProductsManufacture from pt_products_manufacture where name_products_manufacture='".$name_products_manufacture."'");

			$getProductsManufacture=$queryimport->result();

			return $getProductsManufacture;

		}

	/*edit list import */
	
	
	/*list export */

		public function listExport($mid=false,$date_start_limit_use=false,$date_end_limit_use=false){
			
			$where ="";
			if($mid!=false)
			{
				$where.="and mid=".$mid." ";
			}
			if($date_start_limit_use!=false)
			{
				list($d,$m,$y)=explode('/',$date_start_limit_use);
				$mk=mktime(0,0,0,$d,$m,$y);
				$date_start=date('Y-m-d',$mk);
				$where.="and date_start_limit_use='".$date_start."' ";
			}
			
			if($date_end_limit_use!=false)
			{
				$where.="and date_end_limit_use=".$date_end_limit_use." ";
			}

			$limit_use_list=$this->db->query('select * from pt_limit_use a, pt_products_manufacture b where a.mid=b.idProductsManufacture '.$where.' order by idLimitUse');

			$listSeries = array();
	
			foreach ($limit_use_list->result() as $series){
	
				$option_list = $this->db->get_where('pt_detail_series', array('limitid'=>$series->idLimitUse));
	
				$series->idLimitUses = $option_list->result();
	
				$listMenusParent[] = $series;
	
			}
	
			$listSeries = $limit_use_list->result();
	
			return $listSeries;

		}

	/*edit list export */

	

	

}


