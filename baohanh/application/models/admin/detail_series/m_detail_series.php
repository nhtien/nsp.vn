<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
class M_detail_series extends CI_Model{
	public $languages; 
    function __construct(){
        parent::__construct();
		$this->load->database();
			$this->load->helper("url");
			//load library session
			$this->load->library('session');
			//check languages
			if($this->session->userdata("languages"))
			{
				$languages=$this->session->userdata("languages");
			}
    }
	
	/*list detail_series parent*/
		public function listDetailSeriesParent(){
		$detail_series_list=$this->db->get('pt_detail_series_parent');
		$listDetailSeriesParent= $detail_series_list->result();
		return $listDetailSeriesParent;
    }
	/*end list detail_series parent*/
	
    /*list detail_series*/
		public function limitid(){
		$this->db->order_by("ordering_detail_series", "asc");
		$detail_series_list=$this->db->get('pt_detail_series');
		$detail_series = array();
		foreach ($detail_series_list->result() as $DetailSeries){
			$this->db->order_by("ordering_detail_series_sub", "asc");
			$option_list = $this->db->get_where('pt_detail_series_sub', array('iddetail_series'=>$DetailSeries->idDetailSeries));
			$DetailSeries->idDetailSeries = $option_list->result();
			$detail_series[] = $DetailSeries;
  		}
		return $detail_series;
    }
	/*end list detail_series*/
	
	/*list detail_series*/
		public function listDetailSeries($limitid){
		$detail_series_list=$this->db->query('select * 
		from pt_detail_series a, pt_limit_use b
		where a.limitid=b.idLimitUse and a.limitid='.$limitid.'');
		$listDetailSeries = $detail_series_list->result();
		return $listDetailSeries;
    }
	/*end list detail_series*/
	
	/*list detail_series*/
		public function getLimitUse($idLimitUse){
		$getLimitUse=$this->db->query('select * 
		from pt_limit_use
		where idLimitUse='.$idLimitUse.'');
		$getLimitUse = $getLimitUse->result();
		return $getLimitUse;
    }
	/*end list detail_series*/
	
	
	/*list detail_series sub*/
		public function listDetailSeriesub(){
			$queryDetailSeriesub=$this->db->query('select a.*,b.title_detail_series_sub from pt_detail_series a,pt_detail_series_sub b where a.iddetail_series=b.idDetailSeries order by ordering_detail_series_sub asc');
			$listDetailSeriesub=$queryDetailSeriesub->result();
			return $listDetailSeriesub;
		}
	/*end list detail_series sub*/
	
	/*get detail_series*/
		public function getDetailSeries($idDetailSeries){
			$queryDetailSeries=$this->db->query(
			'select *
			from pt_detail_series a, pt_limit_use b
			where idDetailSeries='.$idDetailSeries.' and a.limitid=b.idLimitUse');
			$getDetailSeries=$queryDetailSeries->result();
			
			return $getDetailSeries;
		}
	/*end get detail_series*/
	
	/*thêm add detail_series*/
		public function addDetail_Series($data){
			$this->db->insert('pt_detail_series', $data);
		}
	/*end add detail_series */
	/* edit detail_series*/
		public function editDetail_Series($idDetailSeries,$data){
			$this->db->where("idDetailSeries",$idDetailSeries);
			$this->db->update('pt_detail_series', $data);
		}
	/*end edit detail_series*/
	
	/*remove detail_series*/
	public function removeDetail_Series($idDetailSeries)

	{
		$this->db->where("idDetailSeries",$idDetailSeries);
		$this->db->delete("pt_detail_series");
	}
	/*end remove detail_series*/

	/*enable detail_series*/
		public function enable($status,$id){
			if($status==0)
			$status=1;
			else
			$status=0;
			$qr = $this->db->query("UPDATE pt_detail_series SET enable_detail_series=".$status." WHERE idDetailSeries=".$id."" );
		}
	/*end enable detail_series*/
	
	/*enable detail_series sub*/
		public function enable_sub($status,$id){
			if($status==0)
			$status=1;
			else
			$status=0;
			$qr = $this->db->query("UPDATE pt_detail_series_sub SET enable_detail_series_sub=".$status." WHERE idDetailSeriesSub=".$id."" );
		}
	/*end enable detail_series sub*/
	
	/*check_ordering*/
		public function check_ordering($idDetailSeries,$data){
			$this->db->where("idDetailSeries",$idDetailSeries);
			$this->db->update('pt_detail_series', $data);
		}
	/*end check_ordering*/
	/*get record next ordering*/
		public function getOrderingPrevious($ordering_detail_series){
			$queryOrderingPrevious=$this->db->query(
			'select *
			from pt_detail_series
			where ordering_detail_series='.($ordering_detail_series+1).'');
			$getOrderingPrevious=$queryOrderingPrevious->result();
			return $getOrderingPrevious;
		}
	/*end get record next ordering*/
	
	/*get record next ordering*/
		public function getOrderingNext($ordering_detail_series){
			$queryOrderingNext=$this->db->query(
			'select *
			from pt_detail_series
			where ordering_detail_series='.($ordering_detail_series-1).'');
			$getOrderingNext=$queryOrderingNext->result();
			return $getOrderingNext;
		}
	/*end get record next ordering*/

	
}
?>
