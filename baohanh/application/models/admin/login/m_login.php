<?php
if(!isset($_SESSION))@session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
class M_login extends CI_Model{
    function __construct(){
        parent::__construct();
		$this->load->database();
		$this->load->library('session');
    }
	/*check number login*/
	public function check_number_login()
	{
		$ip = getenv("REMOTE_ADDR");
		$this->db->where("ip",$ip); 
		$query=$this->db->get("pt_limitlogin");
		$listLimitLogin=$query->result();
		if($listLimitLogin)
		return $listLimitLogin[0]->numberlogin;
	}
	public function getIp()
	{
		$ip = getenv("REMOTE_ADDR");
		$this->db->where("ip",$ip); 
		$query=$this->db->get("pt_limitlogin");
		$listLimitLogin=$query->result();
		return $listLimitLogin;
	}
	public function deleteIp()
	{
		$ip = getenv("REMOTE_ADDR");
		$this->db->where("ip",$ip); 
		$query=$this->db->delete("pt_limitlogin");
	}
	/*end check number login*/
	
	/* check admin login */
    public function check_admin_login($username,$password)
	{
		$this->db->where("username_users",$username); 
		$this->db->where("password_users",md5($password));
		$this->db->where("gid",1);
		$query=$this->db->get("pt_users");
		if($query->num_rows == 1)
		{
			//Bỏ limit login
			$this->deleteIp();
			// lấy thông tin người đăng nhập
			$row = $query->row();
			$data = array(
					'__idUsers__' => $row->idUsers,
					'__name__' => $row->name_users,
					'__gid__' => $row->gid,
					'validated' => true
					);
			$this->session->set_userdata($data);
			return true;
			exit;
		}
		else
		{
			$numberlogin=1;
			$date=date("Y-m-d H:i:s");
			$ip = getenv("REMOTE_ADDR");
			$query=$this->db->get_where("pt_limitlogin",array("ip"=>$ip));
			$listLimitLogin=$query->result();
			if(count($listLimitLogin)<1)
			{
				
				$data = array(
					'idLimitLogin' =>'null',
					'ip' =>$ip,
					'numberlogin' =>1,
					'time_first' =>$date,
					'time_last' =>$date
				);
				$this->db->insert("pt_limitlogin",$data);
			}
			else
			{
				$numberlogin=$listLimitLogin[0]->numberlogin + 1;
				$data = array(
					'ip' =>$ip,
					'numberlogin' =>1,
					'time_first' =>$date,
					'time_last' =>$date
				);
				$this->db->where("ip",$ip);
				$this->db->update("pt_limitlogin",$data);
				$query=mysql_query("update pt_limitlogin set numberlogin = '".$numberlogin."', time_last = '".$date."' where ip='".$ip."'");
				
			}
			echo json_encode(array("result"=>0,"numberlogin"=>$numberlogin));
			exit;
		}
	}
	
	function addLog($data){
		$this->db->insert("pt_logs",$data);
	}
	
	public function selectIdUsers($username,$password)
	{
		$this->db->where("username_users",$username); 
		$this->db->where("password_users",md5($password));
		$this->db->where("gid",1);
		$query=$this->db->get("pt_users");
		return $query->result();
	}
}
?>