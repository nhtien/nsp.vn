<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
class M_application extends CI_Model{
    function __construct(){
        parent::__construct();
		$this->load->database();
		$this->load->helper("getalias");
		$this->load->library('session');
    }
	/*check languages*/
		public function check_languages(){
			if($this->session->userdata("languages"))
			{
				if($this->session->userdata("languages")=='vi')
				{
					$languages='';
				}
				else
				{
					$languages='_en';
				}
				return $languages;
			}
			else
			{
				$queryLanguages=$this->db->query('select 
				code_languages
				from pt_languages
				where default_languages=1 and enable_languages=1');
				$getLanguages=$queryLanguages->result();
				return $getLanguages[0]->code_languages;	
			}	
		}
	/*end check languages*/
	/*List article about us*/
		public function getCopyright(){
			$languages=$this->check_languages();
			$queryCopyright=$this->db->query('select html_content_blocks, html_content'.$languages.'_blocks as html_content_blocks
			from pt_blocks
			where idBlocks=5');
			$getCopyright=$queryCopyright->result();
			return $getCopyright;
		}
	/*end List article about us*/
		public function getConfig(){
			$queryconfig=$this->db->query('select value, name
			from pt_config');
			$listConfig=$queryconfig->result();
			return $listConfig;
		}
		
	/*list products_manufacture */
		public function listProductsManufacture(){
			$queryproducts_manufacture=$this->db->query('select * from pt_products_manufacture  order by idProductsManufacture DESC');
			$listProductsManufacture=$queryproducts_manufacture->result();
			return $listProductsManufacture;
		}
	/*edit list products_manufacture */
}
?>