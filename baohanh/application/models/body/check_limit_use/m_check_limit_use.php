<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Author: Jorge Torres

 * Description: Login model class

 */
require_once(APPPATH . 'models/m_application.php');
class M_check_limit_use extends M_application{

    function __construct(){

        parent::__construct();

		$this->load->database();

    }
	
	// get list limit use
	function listLimitUse($limit_use_string,$mid){
		$where="c.idProductsManufacture=a.mid and a.idLimitUse=b.limitid ";
		if($mid!=0){
			$where.="and a.mid=".$mid." ";
		}
		$option_list = $this->db->query("select *
		from pt_limit_use a, pt_detail_series b, pt_products_manufacture c
		where  ".$where." and b.name_detail_series like '%".$limit_use_string."%'");
		$listLimitUse = $option_list->result();
		return  $listLimitUse;
	} 

}

?>