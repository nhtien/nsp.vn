<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
require_once(APPPATH . 'models/m_application.php');
class M_limit_use extends M_application{
    function __construct(){
        parent::__construct();
		$this->load->database();
			$this->load->helper("url");
			$this->_gallery_path = realpath(APPPATH. "../public/images/products/".date("m_Y"));
    }
    /*list limit_use*/
		public function listLimitUse(){
		$limit_use_list=$this->db->query('select
		meta_title_menus as title,
		meta_key_menus as keywords,
		meta_desc_menus as description,
		name_limit_use, name_limit_use as name_limit_use,
		description_limit_use, description_limit_use as description_limit_use
		from pt_limit_use a, pt_menus b
		where b.idMenus=80');
		$listLimitUse = $limit_use_list->result();
		return $listLimitUse;
    }

	
}
?>
