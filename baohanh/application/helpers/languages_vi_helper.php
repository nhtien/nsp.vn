<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/*



|--------------------------------------------------------------------------



| File and Directory Modes



|--------------------------------------------------------------------------



|



| These prefs are used when checking and setting modes when working



| with the file system.  The defaults are fine on servers with proper



| security, but you may wish (or even need) to change the values in



| certain environments (Apache running a separate process for each



| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should



| always be used to set the mode correctly.



|



*/





/*limit_use*/

define('slogan','Hệ thống tra cứu hạn sử dụng của sản phẩm');



define('limit_use','Nhập số Series');



define('number_detail_series','TGBH (Tháng)');



define('manufacture','NSX');



define('name_products','Tên sản phẩm');



define('description_products','Mô tả');



define('date_start_limit','Ngày bắt đầu');



define('date_end_limit','Hạn bảo hành');



define('check_use','Tra cứu');



define('result','Kết quả tìm thấy');

