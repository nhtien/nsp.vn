<!DOCTYPE html>


<html lang="en">


<head>


	<meta charset="utf-8">


	<title><?php echo $title;?></title>


    <!--Chèn file CSS--> 


    


		<link href="<?php echo (CSS_ADMIN."style.css")?>" type="text/css" rel="stylesheet" />


        


        <link href="<?php echo (CSS_ADMIN."tuyendung_page.css")?>" type="text/css" rel="stylesheet" />


        


    	<link href="<?php echo (CSS_ADMIN."jquery-ui-1.10.3.custom.css")?>" type="text/css" rel="stylesheet" />


        


        


		<link href="<?php echo (CSS_ADMIN."colors-fresh.min.css")?>" type="text/css" rel="stylesheet" />


        <link href="<?php echo (CSS_ADMIN."load-styles.css")?>" type="text/css" rel="stylesheet" />


        


        <link href="<?php echo (CSS_ADMIN."demo_table_jui.css")?>" type="text/css" rel="stylesheet" />


        <link href="<?php echo (CSS."jquery.fancybox-1.3.4.css")?>" type="text/css" rel="stylesheet" />


        


        <link rel="shortcut icon" href="<?php echo URL;?>puclic/images/favicon.ico" type="image/x-icon" />


		<link rel="icon" href="<?php echo URL;?>public/images/favicon.ico" type="image/x-icon" />


    <!--END Chèn file CSS--> 


    <!--Chèn file JS--> 


	<script language="javascript" type="text/javascript" src="<?php echo JS;?>jquery/jquery.min.js"></script>


    <script language="javascript" type="text/javascript" src="<?php echo JS?>jquery/jquery.validate.min.js"></script>


    <script language="javascript" type="text/javascript" src="<?php echo JS;?>jquery/jquery.dataTables.js"></script>


    <script language="javascript" type="text/javascript" src="<?php echo JS;?>jquery/jquery.fancybox-1.3.4.pack.js"></script>


     <script language="javascript" type="text/javascript" src="<?php echo JS;?>jquery/jquery-ui-1.10.3.custom.js"></script>


   


    <script language="javascript" type="text/javascript" src="<?php echo JS_ADMIN;?>index.js"></script>


    <script type="text/javascript">


	/*fancybox*/


		$("a[rel=example_group]").fancybox({


			'transitionIn'		: 'none',


			'transitionOut'		: 'none',


			'titlePosition' 	: 'over',


			'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {


				return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';


			}


		});


	</script>


    <!--END Chèn file JS--> 


</head>


<!--Biến URL gởi qua file js-->


	<input type="hidden" id="base_url" value="<?php echo URL;?>">


<!--end Biến URL gởi qua file js-->


<body>


<div id="backgroundPopup"></div>


    <!--Nội dung wrapper-->


    <div id="wpwrap">


    	<!--Nội dung menu left-->


            <div id="adminmenuwrap">


                <?php $this->load->view('admin/menuleft/main');?>


            </div>


        <!--END Nội dung menu left-->


        <!--Nội dung body right-->


           <div id="wpcontent">


                <!--Tiêu đề-->


                    <div id="wpadminbar" class="nojq nojs" role="navigation">


                        <div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Top navigation toolbar." tabindex="0">


                        	<ul>


                            	<li><a href="<?php echo URL;?>" target="_blank">Trang chủ</a></li>


                            	<li><?php ?></li>


                            	<li><a href="<?php echo URL;?>admin/thoat">Thoát</a></li>


                            </ul>


                        </div>


                    </div>


                <!--end tiêu đề-->


                <!--Nội dung right-->


                <div id="wpbody"> 


                    <div id="wpbody-content">


                       <?php $this->load->view($template);?>


                    </div>


                </div><!-- wpbody -->


            </div><!-- wpcontent -->


            <div class="clear"></div>


        <!--END Nội dung body right-->


    </div>


    <!--END Nội dung wrapper-->


    <div id="back-top"><a href="#header"><span></span></a></div>


</body>


</html>