<h1>Thêm hạn sử dụng</h1>

<div class="box">

    <div class="heading">

      <h1 style="float:left"><img src="<?php echo IMAGES_ADMIN;?>/order.png" alt="">nhóm sản phẩm</h1>

      <div style="float:right; padding-top:7px;">

      <a class="button" id="save" onclick="javascript:submitbutton('save');">Lưu lại</a>&nbsp;&nbsp;

      <a class="button" id="save_close" onclick="javascript:submitbutton('save_close');">Lưu lại và đóng</a>&nbsp;&nbsp;

      <a class="button" href="<?php echo $_SERVER['HTTP_REFERER']; ?>"> << Quay lại</a>

      </div>

    </div>

    <div class="connamet">

      <div class="vtabs">

          <a href="#tab-order" class="selected">Thông tin hạn sử dụng </a>

          <a href="#tab-payment">English</a>

      </div>

      <div id="tab-order" class="vtabs-connamet">

      	<form method="post" action="<?php echo URL;?>admin/limit_use/check_add_limit_use" enctype="multipart/form-data" class="origin_setting" name="adminForm" id="adminForm" method="post" accept-charset="utf-8" novalidate="novalidate">

                <table class="table_admin">

                    <tbody>

                        <tr>

                            <td colspan="2"><span style="color:#0C9;"><?php if(isset($messages)) echo $messages;?></span></td>

                        </tr>
                        
                        <tr class="alt-row">

                            <td>Số hóa đơn</td> 

                            <td><input name="oid" type="text" autofocus="autofocus"  /></td>

                        </tr>
                        
                        <tr class="alt-row">

                            <td>Nhà sản xuất:</td>

                            <td><select name="mid">

                            <?php

                            foreach($listManufacture as $manufacture)

                            {

                                ?>

                                <option value="<?php echo $manufacture->idProductsManufacture;?>"><?php echo $manufacture->name_products_manufacture;?></option>

                                <?php

                            }

                            ?>

                            </select></td>

                        </tr>
                        
                        
                        
                        <tr class="alt-row">

                            <td>Công ty</td> 

                            <td><input name="company_limit_use" type="text"  /></td>

                        </tr>

						<tr class="alt-row">

                            <td>Code P/N</td> 

                            <td><input name="code_limit_use" type="text"    /></td>

                        </tr>

                        <tr class="alt-row">

                            <td>Tên sản phẩm:</td> 

                            <td><input name="name_limit_use" type="text"    /></td>

                        </tr>
                        
                        <tr class="alt-row">

                            <td>Ngày bảo hành:</td> 

                            <td><input id="datepicker" name="date_start_limit_use" type="text"     /></td>

                        </tr>

                        <tr class="alt-row">

                            <td>Hạn sử dụng:</td> 

                            <td><input id="datepicker1" name="date_end_limit_use" type="text"     /></td>

                        </tr>

                        <tr>

                            <td>

                                <div class="input-group">

                                    <span class="input-group-addon">Nội dung (*) </span></div></td><td>

                                    <textarea class="form-control" name="description_limit_use" cols="2" rows="3" id="description_limit_use" title="Không được bỏ trống" ></textarea>

                            </td>

                        </tr>

                        <tr>

                        	<td>Bật</td>

                            <td><input name="enable_limit_use" type="radio" value="1" class="inputbox" checked="checked">&nbsp;Có &nbsp;&nbsp;<input name="enable_limit_use" type="radio" value="0" class="inputbox">&nbsp;Không</td>

                        </tr>

                    </tbody>

                </table>
                <input type="hidden" name="t" value="save" id="t">

            </form>

      </div>

    </div>

</div>

<script language="javascript" type="text/javascript" src="<?php echo JS;?>editor/ckeditor.js"></script>

<script>

		CKEDITOR.replace( 'description_en_limit_use',

		{

			filebrowserBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html',

			filebrowserImageBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html?type=Images',

			filebrowserFlashBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html?type=Flash',

			filebrowserUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',

			filebrowserImageUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

			filebrowserFlashUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

		});

		CKEDITOR.replace( 'description_limit_use',

		{

			filebrowserBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html',

			filebrowserImageBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html?type=Images',

			filebrowserFlashBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html?type=Flash',

			filebrowserUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',

			filebrowserImageUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

			filebrowserFlashUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

		});

		$(document).ready(function(e) {

			$('.vtabs a').tabs();

		});

</script>

<script type="text/javascript">

$(document).ready(function(e) {
	 /*function check_date(startDateStr, endDateStr) {
		var sDate = new Date(startDateStr),
			eDate = new Date(endDateStr);
		if(sDate > eDate) {
			alert("Ngày bắt đầu không được lớn hơn ngày kết thúc!");
			return false;
		}

	};
	$("#adminForm").submit(function(e){
		return check_date($("#datepicker").val(), $("#datepicker").val());
	})*/
    /*dang ky*/

		$("#adminForm").validate({ 

			rules: {

				name_limit_use:{

					required: true

				},

				description_limit_use:{

					required: true

				},
				date_start_limit_use:{

					required: true

				},
				date_end_limit_use:{

					required: true
				}


			},

			messages: {

				name_limit_use:{

					required: "Bắt buộc nhập"

				},

				description_limit_use:{

					required: "Bắt buộc nhập"

				},
				date_start_limit_use:{

					required: "Bắt buộc nhập"

				},
				date_end_limit_use:{

					required: "Bắt buộc nhập"

				}

			}

		});

	/*end đăng ký*/

});

</script>
