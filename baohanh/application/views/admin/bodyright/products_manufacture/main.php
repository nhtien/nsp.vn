	<h1>Danh sách nhà sản xuất</h1>
    <form action="<?php echo base_url();?>admin/products_manufacture/removeProductsmanufacture" method="post" id="from-tindang">
        <table class="TableGrid datatables" cellpadding="0" cellspacing="0" border="0" id="tbl_danhsachhoadon">
        
            <thead>
                <tr>
                    <th width="5%">Stt</th>
                    <th width="10%">Tên nhà sản xuất</th>
                    <th width="10%">Trạng thái</th>
                    <th width="15%">Thao tác<br />
                        <input type="checkbox" onclick="toggle(this)" />&nbsp;<font color="#000000">Chọn tất cả</font>
                        <button type="submit" class="btn" name="btnDeleteall">
                            <span>Xóa</span>
                        </button>
                        <a class="them_products_manufacture" href="<?php echo base_url();?>admin/products_manufacture/add_products_manufacture">Thêm nhà sản xuất</a>
                    </th>
                </tr>
            </thead>
            <tbody>
			<?php
            $stt=1;
            foreach ($listProductsManufacture as $row)
            { 
            ?>
            <tr class="odd <?php if($stt%2!=0)echo 'tr_two';?>">
                <td  width="5%"><?php echo $stt?></td>
                <td  width="10%" ><?php echo $row->name_products_manufacture;?>
                </td>
                
                <td  width="10%" >
				<a title="Duyệt tuyển dụng" href="<?php echo base_url();?>admin/products_manufacture/enable/<?php echo $row->enable_products_manufacture?>/<?php echo $row->idProductsManufacture?>"
				<?php if($row->enable_products_manufacture==1) echo 'class="daduyet"'; else echo 'class="chuaduyet"';?> 
                id="status">
				<?php 
					if($row->enable_products_manufacture==1)
					{
						
						echo 'Đã duyệt';
					}
					else
					echo 'Chưa duyệt';
				?></a><br>
                </td>
                <td width="15%">
                    <a id="edit-hoadon" href="<?php echo URL;?>admin/products_manufacture/edit_products_manufacture/<?php echo $row->idProductsManufacture;?>">[&nbsp;Sửa&nbsp;]</a>
                    <input type="checkbox" name="delete[]" value="<?php echo $row->idProductsManufacture;?>" />   
                </td>
             </tr>
				<?php
                $stt++;
            }
          ?>
          </tbody>
    </table>
    </form>
    <div style="clear:both;"></div>
    