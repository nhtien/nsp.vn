<h1>Sữa nhà sản xuất</h1>

<div class="box">

    <div class="heading">

      <h1 style="float:left"><img src="<?php echo IMAGES_ADMIN;?>/order.png" alt="">nhà sản xuất</h1>

      <div style="float:right; padding-top:7px;">

      <a class="button" id="save" onclick="javascript:submitbutton('save');">Lưu lại</a>&nbsp;&nbsp;

      <a class="button" id="save_close" onclick="javascript:submitbutton('save_close');">Lưu lại và đóng</a>&nbsp;&nbsp;

      <!--<a class="button" href="<?php echo $_SERVER['HTTP_REFERER']; ?>"> << Quay lại</a>-->

      </div>

    </div>

    <div class="connamet">

      <div class="vtabs">

          <a href="#tab-order" class="selected">Thông tin nhà sản xuất </a>

          <a href="#tab-payment">English</a>

      </div>

      <div id="tab-order" class="vtabs-connamet">

      	<form method="post" action="<?php echo URL;?>admin/products_manufacture/check_edit_products_manufacture/<?php echo $getProductsManufacture[0]->idProductsManufacture;?>" enctype="multipart/form-data" class="origin_setting" name="adminForm" id="adminForm" method="post" accept-charset="utf-8" novalidate="novalidate">

                <table class="table_admin">

                    <tbody>

                        <tr>

                            <td><span style="color:#0C9;"><?php if(isset($messages)) echo $messages;?></span></td>

                        </tr>

                        <tr class="alt-row">

                            <td>Tên nhà sản xuất:</td> 

                            <td><input name="name_products_manufacture" type="text" autofocus value="<?php echo $getProductsManufacture[0]->name_products_manufacture;?>"    /></td>

                        </tr>

                        <tr>

                            <td>

                                <div class="input-group">

                                    <span class="input-group-addon">Nội dung (*) </span></div></td><td>

                                    <textarea class="form-control" name="description_products_manufacture" cols="2" rows="3" id="description_products_manufacture" title="Không được bỏ trống" ><?php echo $getProductsManufacture[0]->description_products_manufacture;?></textarea>

                            </td>

                        </tr>

                        <tr class="alt-row">

                            <td>Thứ tự:</td> 

                            <td><input name="ordering_products_manufacture" type="text" value="<?php echo $getProductsManufacture[0]->ordering_products_manufacture;?>"     /></td>

                        </tr>

                        <tr class="alt-row">

                            <td>Meta Title:</td> 

                            <td><input name="meta_title_products_manufacture" type="text" value="<?php echo $getProductsManufacture[0]->meta_title_products_manufacture;?>"    /></td>

                        </tr>

                        <tr class="alt-row">

                            <td>Meta Keywords:</td> 

                            <td><input name="meta_key_products_manufacture" type="text"  value="<?php echo $getProductsManufacture[0]->meta_key_products_manufacture;?>"    /></td>

                        </tr>

                        <tr class="alt-row">

                            <td>Meta Description:</td> 

                            <td><input name="meta_desc_products_manufacture" type="text" value="<?php echo $getProductsManufacture[0]->meta_desc_products_manufacture;?>"     /></td>

                        </tr>

                        <tr>

                        	<td>Bật</td>

                            <td><input name="enable_products_manufacture" type="radio" value="1" class="inputbox" <?php if($getProductsManufacture[0]->enable_products_manufacture==1) echo 'checked="checked"'?>>&nbsp;Có &nbsp;&nbsp;<input name="enable_products_manufacture" type="radio" value="0" class="inputbox" <?php if($getProductsManufacture[0]->enable_products_manufacture==0) echo 'checked="checked"'?>>&nbsp;Không</td>

                        </tr>

                    </tbody>

                </table>

      </div>

      <div id="tab-payment" class="vtabs-content" style="display: none;">

                <table>

                    <tbody>

                        <tr>

                            <td><span style="color:#0C9;"><?php if(isset($messages)) echo $messages;?></span></td>

                        </tr>

                        <tr class="alt-row">

                            <td>Tên nhà sản xuất:</td> 

                            <td><input name="name_en_products_manufacture" type="text" autofocus value="<?php echo $getProductsManufacture[0]->name_en_products_manufacture;?>"    /></td>

                        </tr>

                        <tr>

                            <td>

                                <div class="input-group">

                                    <span class="input-group-addon">Nội dung (*) </span></div></td><td>

                                    <textarea class="form-control" name="description_en_products_manufacture" cols="2" rows="3" id="description_en_products_manufacture" title="Không được bỏ trống" ><?php echo $getProductsManufacture[0]->description_en_products_manufacture;?></textarea>

                            </td>

                        </tr>

                    </tbody>

                </table>

                <input type="hidden" name="t" value="save" id="t">

            </form>

      </div>

    </div>

</div>

<script language="javascript" type="text/javascript" src="<?php echo JS;?>editor/ckeditor.js"></script>

<script>

		CKEDITOR.replace( 'description_en_products_manufacture',

		{

			filebrowserBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html',

			filebrowserImageBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html?type=Images',

			filebrowserFlashBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html?type=Flash',

			filebrowserUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',

			filebrowserImageUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

			filebrowserFlashUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

		});

		CKEDITOR.replace( 'description_products_manufacture',

		{

			filebrowserBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html',

			filebrowserImageBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html?type=Images',

			filebrowserFlashBrowseUrl : '<?php echo JS;?>ckfinder/ckfinder.html?type=Flash',

			filebrowserUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',

			filebrowserImageUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

			filebrowserFlashUploadUrl : '<?php echo JS;?>ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

		});

		$(document).ready(function(e) {

			$('.vtabs a').tabs();

		});

</script>

<script type="text/javascript">

$(document).ready(function(e) {

    /*dang ky*/

		$("#adminForm").validate({ 

			rules: {

				name_products_manufacture:{

					required: true

				},

				description_products_manufacture:{

					required: true

				},

				ordering_products_manufacture:{

					required: true,

					number:true

				}

			},

			messages: {

				name_products_manufacture:{

					required: "Bắt buộc nhập"

				},

				description_products_manufacture:{

					required: "Bắt buộc nhập"

				},

				ordering_products_manufacture:{

					required: "Bắt buộc nhập",

					number: "Phải là số"

				}

			}

		});

	/*end đăng ký*/

});

</script>

