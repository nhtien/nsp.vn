	<h1>Danh sách hạn sử dụng</h1>

    <form action="<?php echo base_url();?>admin/limit_use/removeLimitUse" method="post" id="from-admin">

        <table class="TableGrid datatables" cellpadding="0" cellspacing="0" border="0" id="tbl_danhsachhoadon">

        

            <thead>

                <tr>

                    <th width="5%">Stt</th>
                    
                    <th width="10%">Số HD</th>
                    
                    <th width="10%">P/N</th>

                    <th width="15%">Công ty</th>
                    
                    <th width="5%">NSX</th>

                    <th width="20%">Tên sản phẩm</th>

                    <th width="10%">Ngày bảo hành</th>
                    
                    <th width="10%">Hạn bảo hành</th>
                    
                    <th width="5%">SL</th>

                    <th width="10%">Trạng thái</th>

                    <th width="15%">Thao tác<br />

                        <input type="checkbox" onclick="toggle(this)" />&nbsp;<font color="#000000">Chọn tất cả</font>

                        <button type="submit" class="btn" name="btnDeleteall">

                            <span>Xóa</span>

                        </button>
						<br />
                        <a class="them_limit_use" href="<?php echo base_url();?>admin/limit_use/add_limit_use">Thêm hạn sử dụng</a>

                    </th>

                </tr>

            </thead>

            <tbody>

			<?php

            $stt=0;

            foreach ($listLimitUse as $row)

            { 

            ?>

            <tr class="odd <?php if($stt%2!=0)echo 'tr_two';?>">

                <td  width="5%"><?php echo $stt+1?></td>
                
                <td  width="10%" ><?php echo $row->oid;?></td>
                
                <td  width="10%" ><?php echo $row->code_limit_use;?></td>

                <td  width="20%" ><?php echo $row->company_limit_use;?></td>

                <td  width="5%" ><?php echo $row->name_products_manufacture;?></td>
                
                <td  width="25%" ><?php echo $row->name_limit_use;?></td>

                <td  width="10%" ><?php echo date("d-m-Y", strtotime($row->date_start_limit_use));?></td>   
                
                <td  width="10%" ><?php echo date("d-m-Y", strtotime($row->date_end_limit_use));?></td> 
                
                <td  width="10%" ><?php echo count($row->idLimitUses);?>&nbsp;<a href="<?php echo URL;?>admin/detail_series/index/<?php echo $row->idLimitUse;?>"><img src="<?php echo IMAGES;?>go_items.png" /></a></td>             

                <td  width="10%" >

				<a title="Duyệt tuyển dụng" href="<?php echo base_url();?>admin/limit_use/enable/<?php echo $row->enable_limit_use?>/<?php echo $row->idLimitUse?>"

				<?php if($row->enable_limit_use==1) echo 'class="daduyet"'; else echo 'class="chuaduyet"';?> 

                id="status">

				<?php 

					if($row->enable_limit_use==1)

					{

						

						echo 'Đã bật';

					}

					else

					echo 'Đã tắt';

				?></a><br>

                </td>

                <td width="15%">

                    <a id="edit-hoadon" href="<?php echo URL;?>admin/limit_use/edit_limit_use/<?php echo $row->idLimitUse;?>">[&nbsp;Sửa&nbsp;]</a>

                    <input type="checkbox" name="delete[]" value="<?php echo $row->idLimitUse;?>" />   

                </td>

             </tr>
             <?php
			$stt++;
          }
          ?>
          </tbody>

    </table>

    <!--data id -->

    <input type="hidden" name="t" value="" id="t">

    <!--data stt -->

    <input type="hidden" name="stt" value="" id="stt">

    </form>

    <div style="clear:both;"></div>

    