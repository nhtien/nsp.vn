<!--Giới hạn số lần đăng nhập -->
<?php
	if($numberlogin > 5)
	{
		redirect(base_url().'admin/limitlogin');
	}
?>
<!--end Giới hạn số lần đăng nhập -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $title;?></title>
    <!--Chèn file CSS--> 
    	<link href="<?php echo (CSS_ADMIN."/style1.css")?>" type="text/css" rel="stylesheet" />
        <link rel="shortcut icon" href="<?php echo URL;?>/puclic/images/favicon.ico" type="image/x-icon" />
		<link rel="icon" href="<?php echo URL;?>/public/images/favicon.ico" type="image/x-icon" />
    <!--END Chèn file CSS--> 
    <!--Chèn file JS--> 
		<script language="javascript" type="text/javascript" src="<?php echo (JS.'/jquery/jquery.min.js');?>"></script>
    <!--END Chèn file JS--> 
</head>
<!--Biến URL gởi qua file js-->
	<input type="hidden" id="base_url" value="<?php echo base_url();?>">
<!--end Biến URL gởi qua file js-->
<body>
    <!--Nội dung wrapper-->
    <!--<div id="wrapper">

		<div id="wrappertop"></div>



		<!--<div id="wrappermiddle">

<div style="padding-bottom:5px; padding-left:80px; color:#F00;"><span id="error" style="display:none;">Tên đăng nhập không tồn tại</span></div>

			<h2>Đăng nhập hệ thống</h2>

			

			<div id="username_input">



				<div id="username_inputleft"></div>

				

				<div id="username_inputmiddle">

				

					<input type="text" name="username" class="username" id="url" placeholder="Email">

					<img id="url_user" src="<?php echo IMAGES_ADMIN;?>/mailicon.png" alt="">

				

				</div>



				<div id="username_inputright"></div>



			</div>



			<div id="password_input">



				<div id="password_inputleft"></div>



				<div id="password_inputmiddle">

				

					<input type="password" name="pass" placeholder="Mật khẩu"  class="pass" id="url" value="Password" >

					<img id="url_password" src="<?php echo IMAGES_ADMIN;?>/passicon.png" alt="">

				

				</div>



				<div id="password_inputright"></div>



			</div>



			<div id="submit">

				

				<a id="login-admin"><input type="image" src="<?php echo IMAGES_ADMIN;?>/submit_hover.png"  value="Đăng nhập"></a>

				

			</div>



		</div>



		<div id="wrapperbottom"></div>

		

		

	</div>-->
    <div class="login-register">
        <section id="content-login">
            <form action="" id="form-login">
                <h1>Đăng nhập</h1>
                <span id="error" style="color:#F00; opacity:0;">Tên đăng nhập hay mật khẩu không tồn tại!</span>
                <div>
                    <input type="text"  name="email_user" placeholder="Tên đăng nhập..." required id="username_user" class="username_user" />
                </div>
                <div>
                    <input type="password" placeholder="Mật khẩu..." required id="pass_user" class="pass_user" />
                </div>
                <div>
                    <input type="submit" value="Đăng nhập" id="login-admin"  />
                </div>
            </form><!-- form -->
        </section><!-- content -->
     </div>
<script type="text/javascript">
	$(document).ready(function(e) {

			$("#login-admin").click(function(e){

				e.preventDefault();

				var username = $(".username_user").val();

				var pass = $(".pass_user").val();

				//alert(base_url);

				$.post("<?php echo URL;?>administrator/check_admin_login",

				{

					 username:username,

					 pass:pass

					 

				},

				function(msg) 

				{ 

					 if(msg==true)

					 {

						window.location = "<?php echo URL;?>administrator";

					 }

					 else

					 {

						if(msg.numberlogin>5)

						{

							alert("Tài khoản của bạn đã bị khóa, xin vui lòng liên hệ ban quản trị");

							window.location = base_url+"admin/limitlogin";

						}

						else if(msg.numberlogin>4)

						{

							alert("Bạn đã đăng nhập sai 4 lần, hãy kiểm tra lại mật khẩu trước khi tiến hành đăng nhập lần tiếp theo\n Đừng nên đăng nhập sai quá 5 lần không thì tài khoản của bạn sẽ bị khóa");

						}

						else if(msg.numberlogin>3)

						{

							alert("Bạn đã đăng nhập sai 3 lần, hãy kiểm tra lại mật khẩu trước khi tiến hành đăng nhập lần tiếp theo\n Đừng nên đăng nhập sai quá 5 lần không thì tài khoản của bạn sẽ bị khóa");

						}

						else {
							$("#error").animate({
							'margin-left':'-100px',
							'opacity':'1'
							},2000);
							$("#error").animate({
								'margin-left':'100px',
								'opacity':'0'
							},2000);
							$("#username_user").val("").fadeIn('fast');
							$("#pass_user").val("").fadeIn('fast');
							document.getElementById("username_user").focus();
						}

					 }

				},'json'); //end function (response)		

			});    

	});

	</script>

</body>



</html>