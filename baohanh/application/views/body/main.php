<style>
select {
color: #000;
}
textarea, input[type="text"], input[type="password"], input[type="file"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="url"], select {
background-color: #fff;
color: #333;
}
.widefat, div.updated, div.error, .wrap .add-new-h2, textarea, input[type="text"], input[type="password"], input[type="file"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="url"], select, .tablenav .tablenav-pages a, .tablenav-pages span.current, #titlediv #title, .postbox, #postcustomstuff table, #postcustomstuff input, #postcustomstuff textarea, .imgedit-menu div, .plugin-update-tr .update-message, #poststuff .inside .the-tagcloud, .login form, #login_error, .login .message, #menu-management .menu-edit, .nav-menus-php .list-container, .menu-item-handle, .link-to-original, .nav-menus-php .major-publishing-actions .form-invalid, .press-this #message, #TB_window, .tbtitle, .highlight, .feature-filter, #widget-list .widget-top, .editwidget .widget-inside {
-webkit-border-radius: 3px;
border-radius: 3px;
border-width: 1px;
border-style: solid;
}
textarea, input, select {
margin: 1px;
padding: 3px;
}
td, textarea, input, select, button {
font-family: inherit;
font-size: inherit;
font-weight: inherit;
}
textarea, input[type="text"], input[type="password"], input[type="file"], input[type="email"], input[type="number"], input[type="search"], input[type="tel"], input[type="url"], select {
border-color: #dfdfdf;
}
</style>
<div id="body-product">
    <!--main-products-->
    <div class="hero-area" id="content-left">
        <div class="wrapper">
        	<div class="body">
			<fieldset>
                <legend><h3 class="red">Bạn hãy nhập số series vào khung bên dưới</h3></legend>
                <form action="" method="post" id="form-limit-use">
                    	<!--Chọn nhà sản xuất
						<select name="mid" class="mid">
                        	<option value="0">--Chọn--</option>
                            <?php
                            foreach($listProductsManufacture as $manufacture)
                            {
                                ?>
                                <option value="<?php echo $manufacture->idProductsManufacture;?>"><?php echo $manufacture->name_products_manufacture;?></option>
                                <?php
                            }
                            ?>
                            </select>
                            </br>-->
                    	<input name="limit_use_string" type="text" id="limit_use_string" class="limit_use_string" autofocus="autofocus" placeholder="<?php echo limit_use;?>" />
                        <button type="submit" class="button"><?php echo check_use;?></button>
                    </form>
                    <div class="body-limit-use">
                
                </div>
            </fieldset>
            	<!--<div class="title">
					<div>
                        <h3>Bạn hãy nhập số series vào khung bên dưới</h3>
					</div>
                </div>-->
                <div class="form-look">
                	<!--<form action="" method="post" id="form-limit-use">
                    	<!--Chọn nhà sản xuất
						<select name="mid" class="mid">
                        	<option value="0">--Chọn--</option>
                            <?php
                            foreach($listProductsManufacture as $manufacture)
                            {
                                ?>
                                <option value="<?php echo $manufacture->idProductsManufacture;?>"><?php echo $manufacture->name_products_manufacture;?></option>
                                <?php
                            }
                            ?>
                            </select>
                            </br>
                    	<input name="limit_use_string" type="text" id="limit_use_string" class="limit_use_string" autofocus="autofocus" placeholder="<?php echo limit_use;?>" />
                        <button type="submit" class="button"><?php echo check_use;?></button>
                    </form>-->
                </div>
                
            </div>
        </div>
    </div>
    <!--end main-products-->
    <div class="clear"></div>
    <!--end our services-->
</div>
<script type="text/javascript">
$(document).ready(function(e) {
    /*dang ky*/
		$("#form-limit-use").validate({ 
			rules: {
				limit_use_string:{
					required: true
				}
				
			},
			messages: {
				limit_use_string:{
					required: "Bắt buộc nhập"
				}
			}
		});
	/*end đăng ký*/
});
</script>
