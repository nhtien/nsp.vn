<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class File
{
	
	function __construct(){
	}
	/*
	get Extension
	*/
	function ext($file)
	{
		$chunks = explode('.', $file);
		$chunksCount = count($chunks) - 1;

		if($chunksCount > 0) {
			return strtolower($chunks[$chunksCount]);
		}
		
		return false;
	}
	
	/**
	 * Strips the last extension off a file name
	 * @return string The file name without the extension
	 * @param string $file The file name
	 * 
	 */
	function stripExt($file) {
		return preg_replace('#\.[^.]*$#', '', $file);
	}
	
	/*
	Returns the name, sans any path
	param string $file File path
	@return string filename
	*/
	function getName($file) {
		$slash = strrpos($file, DS);
		if ($slash !== false) {
			return substr($file, $slash + 1);
		} else {
			return $file;
		}
	}
	
	function upload($src, $dest)
	{
		global $config;
		
		$dest = Path::clean($dest);
		$basedir = dirname($dest);
		
		if (!file_exists($basedir))
		{
			import('folder');
			Folder::create($basedir);
		}

		if(move_uploaded_file($src, $dest))
		{
			return true;
		}
		elseif($config->ftp_enable)
		{
			import('ftp');
			$ftp = &FTP::getInstance();
			$path_ftp = Path::clean(str_replace(PATH_BASE, $config->ftp_root, $dest), '/');
			return $ftp->put($path_ftp, $src);
		}
		
		return false;
	}
	/*
	@string/array $file 
	*/
	function delete( $file )
	{
		global $config;
		
		if (is_array($file)) 
		{
			$files = $file;
		} 
		else 
		{
			$files[] = $file;
		}
		$total = 0;
		foreach($files as $f){
			if( file_exists( $f ) )
			{
				if ( @unlink($f) )
					$total++;
				elseif($config->ftp_enable)
				{
					import('ftp');
					$ftp = &FTP::getInstance();
					$path_ftp = Path::clean(str_replace(PATH_BASE, $config->ftp_root, $f), '/');
					return $ftp->delete($path_ftp);
				}
			}
		}
		
		if($total) return true;
		return false;
	}
	
	/*
	Kiểm tra định dạng file
	@ string: jpg|gif|bmp|....
	*/
	function checkExt( $stringExt , $file )
	{
		$ext 	= File::ext($file);
		$arrExt = explode("|",$stringExt);
		if(in_array($ext, $arrExt))
		{
			return true;
		}
		return false;
	}
	
	function makeSafe($file) {
		$regex = array('#(\.){2,}#', '#[^A-Za-z0-9\.\_\- ]#', '#^\.#');
		return preg_replace($regex, '',  String::removeUnicode($file));
	}
	// return url of icon
	/*
	@string file name
	@size = 16 - 24 - 32
	*/
	function getFileIcon( $filename, $size = 16)
	{
		$fileIcons = array(
			"pdf"	=> "pdf.gif",
			"zip"	=> "zip.gif",
			"rar"	=> "rar.gif",
			"doc"	=> "doc.gif",
			"docx"	=> "doc.gif",
			"xls"	=> "xls.gif",
			"xlsx"	=> "xls.gif",
			"txt"	=> "txt.gif",
			"exe"	=> "exe.gif",
			"jar"	=> "jar.gif",
			"ttf"	=> "ttf.gif",
			"jpg"	=> "img.png",
			"png"	=> "img.png",
			"gif"	=> "img.png",
			"other"	=> "file.png"
			);
			
		$ext = File::ext( $filename);
		
		switch( $size )
		{
			case 24:
				$folder = 'icon24x24';
				break;
			case 32:
				$folder = 'icon32x32';
				break;
			case 48:
				$folder = 'icon48x48';
				break;
			default:
				$folder = 'icon16x16';
				break;
		}
		
		$src = PATH_SITE_IMAGES . DS . 'icons' . DS . 'file' . DS . $folder . DS . $ext.'.png' ; //echo $src;exit;
		if(file_exists($src ) ){
			$icon_url = URI::getRoot().'images/icons/file/'.$folder.'/'.$ext . '.png';
		}else{
			$icon_url = URI::getRoot().'images/icons/file/'.$folder.'/default.png';
		}
		
		return $icon_url;
	
	}
	
	function copy($src, $dest, $path = null)
	{
		global $config;
		
		if ($path)
		{
			$src = $path.DS.$src;
			$dest = $path.DS.$dest;
		}
		
		//Check src path
		if (!is_readable($src))
		{
			Session::setMsg('File::copy: Cannot find or read file'. ": $src",1);
			return false;
		}
		
		if (!file_exists(dirname($dest)))
		{
			import('folder');
			Folder::create(dirname($dest));
		}
		
		if (@copy($src, $dest)) 
		{
			return true;
		}
		elseif($config->ftp_enable)
		{
			import('ftp');
			$ftp = &FTP::getInstance();
			$path_ftp = Path::clean(str_replace(PATH_BASE, $config->ftp_root, $dest), '/');
			return $ftp->put($path_ftp, $src);
		}
		
		Session::setMsg('Copy failed',1);
		return false;
		
		

	}
	
	function stringSizeReadable ($size, $retstring = null)
	{
        $sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        if ($retstring === null) { $retstring = '%01.2f %s'; }
        $lastsizestring = end($sizes);
        foreach ($sizes as $sizestring) {
                if ($size < 1024) { break; }
                if ($sizestring != $lastsizestring) { $size /= 1024; }
        }
        if ($sizestring == $sizes[0]) { $retstring = '%01d %s'; } // Bytes aren't normally fractional
        return sprintf($retstring, $size, $sizestring);
	}
	
}

?>