<?php 
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Trần Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/
// danh sách chưa chọn

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

global $user;
Module::loadCss('examiner','photo.list');
if(!isset($ncol)) 		$ncol 		= 2;
if(!isset($t_width)) 	$t_width 	= 90;
if(!isset($t_height))	$t_height 	= 90;

$percent = floor(100 / $ncol);
?>
<table cellpadding="0" cellspacing="5" border="0" class="photoList2" width="100%">
<tr>
<?php
$i = 1;
foreach( $rows as $r )
{
	$link = get_img_url('photo/'.$r->filename, 728, 550, true);
	$src = get_img_url('photo/'.$r->filename, $t_width, $t_height, true, true);
	$imgsrc = '<img src="'.$src.'" alt="'.htmlspecialchars($r->name).'" class="imglink" />';
	$imgsrc = '<a href="javascript:void(0);" src="'.$link.'" title="'.htmlspecialchars($r->name).'" pid="'.$r->id.'" class="imgpreviewthumb">'.$imgsrc.'</a>';
	?>	
	<td  valign="middle" align="center" id="itemphoto<?php echo $r->id?>" width="<?php echo $percent?>%">
        <div class="item" id="itemthumb_<?php echo $r->id?>">
            <div class="thumb"><?php echo $imgsrc ?></div>
        </div>
	</td>
    <?php
	if( $i % $ncol == 0 ) echo '</tr><tr><td colspan="'.$ncol.'"><hr size="1" /></td></tr><tr>';
	$i++;
}
?>
</tr>
<tr><?php for($i=0; $i<$ncol; $i++) echo '<td></td>';?></tr>
</table>
<script language="javascript">
$(document).ready(function(){
	$('.imgpreviewthumb').click(function(){
		$('.modal-mark').attr('pid',$(this).attr('pid'));
		var src = $(this).attr('src');
		$('#preview img').attr('src',src);
		$('#preview img').hide(200);
		setTimeout("shwPreview()",500);
		$('.radiomark').each(function(){
			if($(this).attr('checked')) $(this).attr('checked', false);
		});
	});
});

function shwPreview(){
	$('#preview img').show(700);
}
</script>
