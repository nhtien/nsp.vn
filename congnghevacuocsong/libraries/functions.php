<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

function import( $filename )
{
	if( is_array($filename ) )
	{
		foreach( $filename as $f)
		{
			$path = PATH_SITE_LIBRARIES.DS.'import'.DS.str_replace(".",DS,$f).'.php';
			if( file_exists($path))
			{
				require_once( $path );
			}
		}
	}
	else
	{
		$path = PATH_SITE_LIBRARIES.DS.'import'.DS.str_replace(".",DS,$filename).'.php';
		if( file_exists($path))
		{
			require_once( $path );
		}
	}
}

function get_img_url($filename, $width = 0, $height = 0, $pathonly = true, $ftsize = false)
{
	//$url = URI::getRoot($pathonly).'image/';
	//$url .= (int)$width.'/'.(int)$height.'/'.$filename;
	$url = URI::getRoot($pathonly).'get_image.php?w='.$width.'&h='.$height.'&fn='.$filename;
	if($ftsize)	$url .= '&ftsize=1';
	return $url;
}

function endcodeImgPath( $path )
{
	$a = explode('/',$path);
	$a = array_reverse($a);
	$a = implode('/',$a);
	return urlencode(base64_encode(base64_encode( $a )));
}




?>