<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class SEF
{
	var $lang	= null;
	
	var $modAliasArr = array(
			'article' => 'bai-viet'
		);

	
	function __construct()
	{
		$this->lang		= array('vi','en');
	}
	function seo( $real_link, $pathonly = true )
	{
		if(preg_match('#http://#',$real_link) || preg_match('#https://#',$real_link) || $real_link == 'index.php' || substr($real_link,0,1) == '/')
			return $real_link;
			
		if($config->multilang)
			$langurl = $config->lang.'/';
		
		$mcmn = Module::getCmn('menu');
		
		$pos =  strpos($real_link,"?") +1;
		
		$link = substr( $real_link, $pos );
		$root = substr($real_link,0,$pos);
		
		$array = explode("&",$link);
		$request	= array();
		for( $i = 0 ; $i < count( $array ) ; $i++)
		{
			$t = explode( "=",$array[$i]);
			$request[$t[0]] = $t[1];
		}
		
		$query_string = array();
		while( list($k,$v) = each( $request ) )
		{
			if( $k != 'm' && $k != 't' && $k != 'mid' )
			{
				$query_string[] = $k.'='.$v;
			}
		}
		$query_string = count( $query_string) ? '?'.implode("&", $query_string) : '';
		if( array_key_exists('mid', $request) )
		{
			$item = $mcmn->getItem( $request['mid'] );
			if( $item->alias )
			{
				
				$vir_link = URI::getBase($pathonly).$langurl.$item->alias.'.html';
				
				return $vir_link;
			}
			
		} 
		$_module	= $request['m'];
		$_task		= $request['t'];
		
		/* Module SEO*/
		
		$path_seo = PATH_SITE_LIBRARIES.DS.'common'.DS.'sef'.DS.$_module.'.seo.php';
		if(file_exists($path_seo))
		{
			require_once($path_seo);
			$className = 'SEO_'.$_module;
			$mseo = new $className();
			$vir_link = $mseo->seo($_task, $request, $pathonly);
			if($vir_link) 
				return $vir_link;
		}
		
		$vir_link = URI::getBase($pathonly).$_module.'/'. ($_task ? $_task : 'home') .'.html'.$query_string;
		return $vir_link;
	}
	function analyticColon($string, $symbol = ':')
	{
		$pos = strrpos($string, $symbol);
		if($pos){
			$result[0] = substr($string,0,$pos);
			$result[1] = substr($string, $pos+1 ); // alias
			
		}else
		{
			$result[0] = $string;
			$result[1] = '';
		}
		return $result;
	}
	
	function analyticDash($string, $symbol = '-')
	{
		$string = str_replace('.html','', strtolower($string));
		$result = explode($symbol, $string);
		$result = array_reverse($result);
		
		return $result;
	}

	function deseo()
	{
		global $module, $task, $menuID, $database, $config;
		$cmn = Module::getCmn('menu');
		
		$path_info = @$_SERVER['PATH_INFO'];
		if(!$path_info && @$_SERVER['ORIG_PATH_INFO'])
			$path_info = $_SERVER['ORIG_PATH_INFO'];
		if(!$path_info) $path_info = $_SERVER['REDIRECT_URL'];
		if($path_info && substr($path_info,0,19) == '/congnghevacuocsong')
			$path_info = str_replace('/congnghevacuocsong','',$path_info);
		
		if(preg_match('/.php/',$path_info))
			return;
		$explode = explode('/',$path_info);
		array_shift($explode);
		if(!count($explode) || $explode[0] == '')
		{
			return;
		}
		if( in_array($explode[0], $this->lang) )
		{
			Request::setVar('lg',$explode[0]);
			$row_config = Table::_('config');
			$row_config->set_language($config);
			array_shift($explode);
		}
		$malias = $explode[0];
		$item = '';
		if( $malias && !@$explode[1] )
		{
			$malias = str_replace('.html','',$malias);
			$filter = 'alias=\''.$malias.'\'';
			$sql = "SELECT * FROM #__menus_items WHERE ".$filter;
			$database->setQuery( $sql );
			$item = $database->loadObject();
			if($config->lang != $config->site_default_lang)
			{
				$filter2 = ' alias_'.$config->lang.'=\''.$malias.'\'';
				$sql = "SELECT * FROM #__menus_items WHERE ".$filter2;
				$database->setQuery( $sql );
				$row = $database->loadObject();
				if($row) $item = $row;
			}
			
			if( @$item->id )
			{
				Request::setVar('mid', $item->id, 'get');
				$menuID = $item->id;
				$module = $item->module;
				$task= $item->task;
			}
		}
		if(@$item->params)
		{
			$xml 	= Module::_getPathTaskParams($item->module, $item->task);
			$p 		= new Parameter($item->params, $xml, false);
			$urlparams = $p->get('paramsArrayURL');
			if($urlparams)
			{
				while(list($k,$v) = each($urlparams)){
					Request::setVar($k,$v,'get');
				}
				
			}
		}
		
		if( !$item)
		{
			$module 	= $explode[0];
			$task 		= preg_replace('/.html/','',$explode[1]);
			
			while(list($k,$v) = each($this->modAliasArr))
				if($v == $module) $module = $k;

			$path_seo = PATH_SITE_LIBRARIES.DS.'common'.DS.'sef'.DS.$module.'.seo.php';
			if(file_exists($path_seo))
			{
				require_once($path_seo);
				$className = 'SEO_'.$module;
				$mseo = new $className();
				$vir_link = $mseo->deseo($explode);
			}
		
		}
		SEF::__homepage();
	}
	// home page
	// set menuID of homepage
	// return boolean homepage
	function __homepage()
	{	
		global $module, $task, $menuID, $config;
		
		$query_string = $_SERVER['QUERY_STRING'] ;
		if(  ($query_string == '' || $query_string == 'lg='.$config->lang ) && $module=='home' )
		{
			$cmn = Module::getCmn('menu');
			$default = $cmn->getDefault();
			if( $default->id )
			{
				$_REQUEST['mid'] = $default->id;
				$menuID = $default->id;
				$module = $default->module;
				$task = $default->task ? $default->task : 'home';
			}
			
			$array = explode("&", $default->link);
	
			for( $i = 0; $i<count( $array) ; $i++)
			{
				$t = explode("=",$array[$i]);
				if( $t[0] == 'm' )
				{
					$module = $t[1];
				}
				elseif( $t[0] == 't' )
				{
					$task = $t[1];
				}else{
					$_REQUEST[$t[0]] = $t[1];
				}
			}
			return true;
		}//if
		
		return false;
	}
}

?>