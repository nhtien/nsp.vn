<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

?>
<h1 class="modheading2"><?php echo sprintf(Language::_('MSG_SEARCH_RESULT'), $total ) ?></h1>
<?php echo $content ?>
<?php echo $pagination->getSitePagesLinks()?>
