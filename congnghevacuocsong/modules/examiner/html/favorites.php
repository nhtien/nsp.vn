<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

$soption[]	= HTML::option('0' , Language::_('SORT_LASTEST'));
$soption[]	= HTML::option('1' , Language::_('SORT_HITS'));
$soption[]	= HTML::option('2' , Language::_('SORT_PHOTO_NAME')); 
$lists['sort'] = HTML::select($soption, 'sort' , 'class="inputbox" onchange="this.form.submit();"','' , Request::getVar('sort'));

?>
<h1 class="modheading2"><span><?php echo Language::_('My Favorites') ?></span></h1>
<div class="sortfrm">
	<form action="" method="get">
    	<?php echo Language::_('Sort') .' '.$lists['sort']?>
    </form>
</div>
<?php if($category->description) echo $category->description?>
<?php echo $content ?>
<?php if($pagination) echo $pagination->getSitePagesLinks()?>
