<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

$selected = Request::getVar('selected','-1');
$level = Request::getVar('level');

?>
<p>Khu vực dành riêng cho ban giám khảo (BGK), nếu bạn không phải là BGK, vui lòng không truy cập vào khu vực này!</p>
<ul class="heading">
<li<?php if($level == 1) echo ' class="selected"'; ?>><a href="<?php echo URI::seo('index.php?m=examiner&t=home&level=1')?>">Vòng 1</a></li>
<li<?php if($level == 2) echo ' class="selected"'; ?>><a href="<?php echo URI::seo('index.php?m=examiner&t=home&level=2')?>">Vòng 2</a></li>
<li<?php if($level == 3) echo ' class="selected"'; ?>><a href="<?php echo URI::seo('index.php?m=examiner&t=home&level=3')?>">Vòng 3</a></li>
</ul>
<br class="break" />
<?php if($level == '2' || $level == '3')
	echo 'Trang web đang xây dựng, vui lòng truy cập sau.';
	else{?>
<ul class="title">
	<li<?php if($selected == '1') echo ' class="selected"'; ?>><a href="<?php echo URI::seo('index.php?m=examiner&t=home&level=1&selected=1')?>">Ảnh bạn đã chọn</a></li>
    <li<?php if($selected == '0') echo ' class="selected"'; ?>><a href="<?php echo URI::seo('index.php?m=examiner&t=home&level=1&selected=0')?>">Ảnh bạn không chọn</a></li>
    <li<?php if($selected == '-1') echo ' class="selected"'; ?>><a href="<?php echo URI::seo('index.php?m=examiner&t=home&level=1&selected=-1')?>">Ảnh bạn chưa chọn</a></li>
    <li<?php if($selected == '2') echo ' class="selected"'; ?>><a href="<?php echo URI::seo('index.php?m=examiner&t=home&level=1&selected=2')?>">Ảnh được chọn vào vòng 1</a></li>
</ul>
<br class="break" />&nbsp;
<h1 class="modheading2"><span><?php echo $title?></span></h1>
<?php echo $content?>
<?php echo $pagination->getSitePagesLinks()?>

<?php } ?>