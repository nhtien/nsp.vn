<?php 
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Trần Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

?>
<h2 class="modheading">Ý kiến học viên</h2>
<div class="b-comment">
<ul>
<?php
foreach($rows as $r){?>
<li>
	<h5><?php echo $r->name?></h5>
    <div class="info"><i>(<?php echo $r->course?>)</i></div>
    <p>"<?php echo $r->description?>"</p>
</li>
<?php } ?>
</ul>
</div>
<br class="break" />
<?php echo $pagination->getSitePagesLinks()?>
