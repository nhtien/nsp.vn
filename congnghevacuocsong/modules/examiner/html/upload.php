<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

?>
<h1 class="modheading">Các ảnh bạn đã gửi dự thi</h1>
<?php if(count($photos)):?>
<table width="100%" class="list" border="0" cellspacing="0" cellpadding="0">
  <tr class="title">
  	<td>STT</td>
    <td>Ảnh</td>
    <td>Tên ảnh</td>
    <td>Ngày gửi</td>
    <td>Xóa</td>
  </tr>
  <?php 
  $i = 1;
  foreach($photos as $r){
		$src = get_img_url('photo/'.$r->filename, 70, 70, true, true);
		$imgsrc = '<a href="'.$r->photo_url.'"><img src="'.$src.'" alt="'.htmlspecialchars($r->name).'" class="imglink" /></a>';
	  ?>
  <tr>
    <td align="center"><?php echo $i++ ?></td>
    <td align="center"><?php echo $imgsrc?></td>
    <td>
    	<form action="index.php" method="post">
        	<input type="text" name="name" value="<?php echo $r->name?>" id="<?php echo $r->id?>" size="35" disabled="disabled" class="disabled" />
           <input type="hidden" name="m" value="photo" />
           <input type="hidden" name="t" value="edittitle" />
           <input type="hidden" name="pid" value="<?php echo $r->id?>"  />
           <?php echo HTML::_('form.token')?>
        <input type="button" title="Sửa" id="editbt<?php echo $r->id?>" pid="<?php echo $r->id?>" class="imglink edittitlebt" value="" />
        <input type="submit" title="Lưu" id="savebt<?php echo $r->id?>" class="imglink savetitlebt" value="" />
        <input type="button" title="Thoát" id="cancelbt<?php echo $r->id?>" pid="<?php echo $r->id?>"  class="imglink canceltitlebt" value="" />
         </form>
   	</td>
    <td align=""><?php echo Date::toFormat($r->created, Language::_('DATE_FORMAT_LC2'));?><br /><a href="<?php echo $r->photo_url?>" class="readmore">Xem ảnh &raquo;</a></td>
    <td align="center">
    	<form action="index.php" method="post" onsubmit="return confirmD();">
           <input type="image" src="images/delete.png" alt="<?php echo Language::_('Delete')?>" class="imglink" />
           <input type="hidden" name="m" value="photo" />
           <input type="hidden" name="t" value="delete" />
           <input type="hidden" name="pid" value="<?php echo $r->id?>"  />
           <?php echo HTML::_('form.token')?>
        </form>
    </td>
  </tr>
  <?php } ?>
</table>
<?php endif;?>
<a name="upload"></a>
<br />
<fieldset style="background:#EFEFEF">
<legend><h3>TẢI LÊN ẢNH MỚI&nbsp;</h3></legend>
Số lượng ảnh tối đa cho phép bạn tải lên: <b><?php echo $pconfig->max_user_upload?></b><br />
Bạn đã tải lên: <b><?php echo count($photos)?>/<?php echo $pconfig->max_user_upload?></b>
<p class="note">Vui lòng nhập thông tin vào mẫu bên dưới để gửi ảnh dự thi của bạn</p>
<form action="index.php" method="post" name="uploadfrm" class="validateform" enctype="multipart/form-data">
<table width="auto" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td>Tên ảnh (*)</td>
    <td><input type="text" name="name" value="" class="inputbox required" size="30" maxlength="50" /></td>
  </tr>
  <tr>
    <td>Tập tin ảnh (*)</td>
    <td><input type="file" name="file" class="required" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" value="Gửi ảnh" class="button submitfrm" /></td>
  </tr>
</table>
<input type="hidden" name="m" value="photo" />
<input type="hidden" name="t" value="save_upload" />
<input type="hidden" name="catid" value="1" />
<?php echo HTML::_('form.token')?>
</form>
<p id="loadingimg" style="display:none;"><img src="images/loading.gif" border="0" align="absmiddle" /> Đang tải...</p>
<br />
	<h5 style="text-decoration:underline;">Ảnh tải lên phải đảm bảo các thông số:</h5>
    <p style="line-height:1.7; font-style:italic;">
    - Định dạng files là JPG hoặc JPEG<br />
    - Kích thước tối thiểu <?php echo $pconfig->min_upload_w?> pixel và tối đa <?php echo $pconfig->max_upload_w?> pixel<br />
    - Độ phân giải: 72dpi - 300 dpi<br />
    - Dung lượng tối đa: <?php echo $pconfig->max_upload_size?>MB
    </p>
</fieldset>
<script language="javascript">
function confirmD(){
	if(confirm('Bạn có muốn xóa ảnh này không?'))
		return true;
	return false;
}
$(document).ready(function(){
	$('input.submitfrm').click(function(){
		var f = document.uploadfrm;
		if(f.name.value != '' && f.file.value != '')
			$('#loadingimg').show();
	});
	
	$('.edittitlebt').click(function(){
		var pid = $(this).attr('pid');
		$('input#'+pid).attr('disabled', null);
		$('input#'+pid).removeClass('disabled');
		$('input#'+pid).addClass('inputbox');
		$(this).hide();
		$('#savebt'+pid).show();
		$('#cancelbt'+pid).show();
	});
	$('.canceltitlebt').click(function(){
		var pid = $(this).attr('pid');
		$('input#'+pid).attr('disabled', 'disabled');
		$('input#'+pid).addClass('disabled');
		$('input#'+pid).removeClass('inputbox');
		$(this).hide();
		$('#savebt'+pid).hide();
		$('#editbt'+pid).show();
	});
	
});

</script>
<style type="text/css">
input.disabled{
	border:none;
	background:none;
}
.edittitlebt{
	background:url(images/edit.png) no-repeat;
	border:none;
	width:20px;
	height:20px;
	cursor:pointer;
}
.savetitlebt{
	background:url(images/save.png) no-repeat;
	border:none;
	width:20px;
	height:20px;
	cursor:pointer;
	display:none;
}
.canceltitlebt{
	background:url(images/cancel.png) no-repeat;
	border:none;
	width:20px;
	height:20px;
	cursor:pointer;
	display:none;
}

</style>
