<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

$width = 950;
$height = 950;

$src = get_img_url('photo/'.$row->filename, $width, $height);
$imgsrc = '<img src="'.$src.'" alt="'.htmlspecialchars($row->name).'" />';

$i = 0;
foreach($lists['slideshow'] as $r){
	if($r->id == $row->id){
		$nextp = $lists['slideshow'][$i+1];
		$prevp = $lists['slideshow'][$i-1];
		break;
	}
	$i++;
}
if(!$nextp) $nextp = $lists['slideshow'][$i-1];
if(!$prevp) $prevp = $lists['slideshow'][0];
//var_dump($lists['slideshow']);exit;
?>
<link href="<?php echo LIVE_THEME?>modules/photo/css/shadowbox.css" rel="stylesheet" type="text/css" media="screen,print">
<script language="JavaScript" type="text/javascript" src="<?php echo LIVE_THEME?>modules/photo/js/shadowbox.js"></script>
<?php include(dirname(__FILE__).DS.'includes'.DS.'lv2.header.php');?>
<div class="detail">
	<h1 class="modheading"><?php echo $row->name?></h1>
    <div class="marks">
    	<div style="float:left; width:35%">
        	<h3>BẢNG ĐIỂM CHI TIẾT</h3>
    		<table width="auto" border="0" cellspacing="0" cellpadding="3">
			<?php 
            $i = 1;
            foreach($examiner as $e){?>
               <tr>
                <td align="left" nowrap="nowrap">Giám khảo <?php echo $i++; if($e->uid == $user->get('id')) echo '(tôi)'?></td>
                <td><div style="background:#<?php echo $e->color?>; width:100px; height:20px;">&nbsp;</div></td>
                <td align="center"><?php echo ($e->mark ? $e->mark .' điểm' : '-') ?></td>
              </tr>
            <?php
            }
			?> </table>
		</div>
        <?php if($econfig->lv1_enable):?>
        <div style="float:right; width:60%; border:1px dashed #999; background:#f2f2f2; padding:10px; height:150px;">
        	<h3>MỜI BẠN CHO ĐIỂM ẢNH NÀY:</h3>
            <p class="note">Đánh chọn từ 1 đến 5, sau đó click "Chấm điểm"</p>
            <form action="index.php" method="post">
            <?php for($i = 1 ; $i <= 5 ; $i++)
                echo '<input type="radio" name="mark" value="'.$i.'" />'.$i.' điểm &nbsp;';
                ?>
                <input type="submit" value="Chấm điểm" class="button" />
                <input type="hidden" name="m" value="examiner" />
                <input type="hidden" name="t" value="lv2_mark" />
                <input type="hidden" name="pid" value="<?php echo $row->id?>" />
            </form>
        </div>
        <?php endif;?>
    </div>
    <br class="break" />
    <hr />
	<div class="preview">
    	<?php echo $imgsrc ?>
        <ul class="slideShow clearfix">
            <li class="prev"><a href="<?php echo $prevp->photo_url?>#photo" title="View Previous Photo">Trước</a></li>
            <li class="next"><a href="<?php echo $nextp->photo_url?>#photo" title="View Next Photo">Sau</a></li>
            <li class="btnSlide"><a href="<?php echo $src?>" title="<?php echo $row->name?>" rel="shadowbox[process]">Trình chiếu</a></li>
        </ul>
        <div class="break" style="height:5px;">&nbsp;</div>
    </div>
    <?php
    foreach($lists['slideshow'] as $r){?>     
    <a style="display:none" href="<?php echo get_img_url('photo/'.$r->filename, $width, $height);?>" title="<?php echo $r->name?>" rel="shadowbox[process]"></a>
    <?php } ?>
    
	<?php
	if($lists['slideshow']){?>
    	<div class="morephoto">
    		<h3>Ảnh xem thêm</h3>
    		<?php echo $lists['morephoto'] ?>
        </div>  
  	<?php } ?>     
</div>
<script language="javascript">
$(document).ready(function()
{

	Shadowbox.init({
		fadeDuration: 0.1,
		continuous: true,
		slideshowDelay: 3
	});
	
	
		

});
</script>
