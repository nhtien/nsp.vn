<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

echo AHTML::_('tableList.startForm');
echo AHTML::_('tableList.top', $lists['searchbox'] . ' ' . $lists['status'] ); 
echo AHtml::_('tableList.start');

$headHtml = array();

$headHtml[] = AHTML::cbcheckall( count($rows));
$headHtml[] = '#';
$headHtml[] = AHTML::_('tableList.sort',Language::_('Category Name'),'c.name',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.sort',Language::_('Photo'),'total_photo',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.sort',Language::_('Enable'),'c.enable',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.orderTitle', count($rows), 'c.ordering',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.sort',Language::_('Category ID'),'c.id',$lists['orderField'],$lists['orderDir'] );

echo AHTML::_('tableList.head',$headHtml );

$i = 0;
foreach($rows as $r )
{
	$ord = AHTML::getHtmlOrder($pagination, $i, count($rows), $r->ordering, ($r->parent == @$rows[$i-1]->parent) , ($r->parent == @$rows[$i+1]->parent) ,($lists['orderField']=='c.ordering' && $lists['orderDir'] == 'ASC' ) );
	$bodyHtml = array();
	
	$bodyHtml[] = array(AHTML::checkboxlist2($r->id, $i ) , 'align="center"');
	$bodyHtml[] = array($pagination->getRowOffset($i), 'align="center"');
	
	$bodyHtml[] = '<a href="index.php?m=photo&t=edit_cat&cid[]='.$r->id.'">'.$r->treename.'</a>';
	$bodyHtml[] = array($r->total_article.'<a href="index.php?m=photo&t=home&catid='.$r->id.'"> &nbsp; <img src="images/go_items.png" /></a>','align="center"');
	$bodyHtml[] = array(AHTML::enable($r->enable, $i),'align="center"');
	$bodyHtml[] = array( $ord, 'align="center" nowrap="nowrap"');
	$bodyHtml[] = array($r->id, 'align="center"');
	
	echo AHTML::_('tableList.body', $bodyHtml, $i );
	$i++;
}

echo AHtml::_('tableList.end');

echo $pagination->getAdminFooterList();
$hiddenList = array(
	'm' 		=> 'photo',
	't'			=> 'categories',
	'boxchecked'=> 0,
	'order' 	=> '',
	'page' 		=> '',
	'section'	=> 'category',
	'order_field'	=> $lists['orderField'],
	'order_dir'		=> $lists['orderDir']
);

echo AHTML::_('tableList.endForm', $hiddenList);

?>
