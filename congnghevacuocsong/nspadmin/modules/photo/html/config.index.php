<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

?>
<form action="index.php" class="adminForm" method="post" name="adminForm">
<table class="adminForm" cellspacing="0">
  <tr>
    <td class="label"><?php echo Language::_('Thumb Width')?> (px) </td>
    <td><input type="text" name="thumb_width" value="<?php echo $row->thumb_width?>" class="inputbox" size="10" maxlength="255" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Thumb Height')?> (px)</td>
    <td><input type="text" name="thumb_height" value="<?php echo $row->thumb_height?>" class="inputbox" size="10" maxlength="255" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Full Width')?> (px) </td>
    <td><input type="text" name="full_width" value="<?php echo $row->full_width?>" class="inputbox" size="10" maxlength="255" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Full Height')?> (px)</td>
    <td><input type="text" name="full_height" value="<?php echo $row->full_height?>" class="inputbox" size="10" maxlength="255" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Min Upload Width')?> (px) </td>
    <td><input type="text" name="min_upload_w" value="<?php echo $row->min_upload_w?>" class="inputbox" size="10" maxlength="255" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Min Upload Height')?> (px)</td>
    <td><input type="text" name="min_upload_h" value="<?php echo $row->min_upload_h?>" class="inputbox" size="10" maxlength="255" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Max Upload Width')?> (px) </td>
    <td><input type="text" name="max_upload_w" value="<?php echo $row->max_upload_w?>" class="inputbox" size="10" maxlength="255" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Max Upload Height')?> (px)</td>
    <td><input type="text" name="max_upload_h" value="<?php echo $row->max_upload_h?>" class="inputbox" size="10" maxlength="255" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Items Per Row'); ?></td>
    <td><input type="text" name="item_per_row" value="<?php echo $row->item_per_row; ?>" class="inputbox" size="10" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Items Row'); ?></td>
    <td><input type="text" name="item_row" value="<?php echo $row->item_row; ?>" class="inputbox" size="10" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Allow User Upload'); ?></td>
    <td><?php echo HTML::booleanlist('allow_user_upload','class="inputbox"',$row->allow_user_upload);?></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Max User Upload'); ?> (photo)</td>
    <td><input type="text" name="max_user_upload" value="<?php echo $row->max_user_upload; ?>" class="inputbox" size="10" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Max Size Upload'); ?> (Mb)</td>
    <td><input type="text" name="max_upload_size" value="<?php echo $row->max_upload_size; ?>" class="inputbox" size="10" maxlength="3" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Upload file type'); ?></td>
    <td><input type="text" name="upload_file_ext" value="<?php echo $row->upload_file_ext; ?>" class="inputbox" size="30" maxlength="50" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Auto Enable Photo')?></td>
    <td><?php echo HTML::booleanlist('auto_enable_upload','class="inputbox"',$row->auto_enable_upload);?></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Send mail to Admin')?></td>
    <td><?php echo HTML::booleanlist('sendemail2admin','class="inputbox"',$row->sendemail2admin);?></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Mail Admin')?></td>
    <td><input type="text" name="mailadmin" value="<?php echo $row->mailadmin?>" class="inputbox" size="50" maxlength="255" /></td>
  </tr>
</table>
<input type="hidden" name="m" value="photo" />
<input type="hidden" name="c" value="config" />
<input type="hidden" name="t" value="" />
</form>