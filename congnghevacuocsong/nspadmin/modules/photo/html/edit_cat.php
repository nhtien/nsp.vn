<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Trần Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

$tabsArr['tabs1'] = Language::_('General Information') ;
import('editor');
?>
<form action="index.php" class="adminForm" method="post" name="adminForm">
<?php 
echo $tabs->startTabs( $tabsArr );
echo $tabs->startTab();
?>

<table class="adminForm" cellspacing="0">
  <tr>
    <td class="label"><?php echo Language::_('Category ID')?></td>
    <td><?php echo $cat->id?></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Category Name')?></td>
    <td><input type="text" name="name" value="<?php echo $cat->name?>" class="inputbox" size="70" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Alias')?> </td>
    <td><input type="text" name="alias" value="<?php echo $cat->alias?>" class="inputbox" size="70" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Enable')?> </td>
    <td><?php echo HTML::booleanlist('enable','class="inputbox"',$cat->enable);?></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Description')?> </td>
    <td> 
		<?php echo Editor::show('description', $cat->description,'100%','500','20','120', true);?>
	
	</td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Meta Title')?> </td>
    <td><input type="text" name="meta_title" value="<?php echo $cat->meta_title; ?>" class="inputbox" size="120" maxlength="255" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Meta Keywords')?> </td>
    <td><input type="text" name="meta_key" value="<?php echo $cat->meta_key; ?>" class="inputbox" size="120" maxlength="400" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Meta Description')?> </td>
    <td><input type="text" name="meta_desc" value="<?php echo $cat->meta_desc; ?>" class="inputbox" size="120" maxlength="400" /></td>
  </tr>
</table>
<?php
echo $tabs->endTab();
echo $tabs->endTabs();
?>

<input type="hidden" name="m" value="photo" />
<input type="hidden" name="t" value="" />
<input type="hidden" name="id" value="<?php echo $cat->id?>" />
</form>