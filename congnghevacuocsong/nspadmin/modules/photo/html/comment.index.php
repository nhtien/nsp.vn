<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

echo AHTML::_('tableList.startForm');
echo AHTML::_('tableList.top', $lists['searchbox'] . ' ' . $lists['status'] ); 
echo AHtml::_('tableList.start');

$headHtml = array();

$headHtml[] = AHTML::cbcheckall( count($rows));
$headHtml[] = '#';
$headHtml[] = AHTML::_('tableList.sort',Language::_('Name'),'c.name',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.sort',Language::_('Comment'),'c.description',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.sort',Language::_('Enable'),'c.enable',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.orderTitle', count($rows), 'c.ordering',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.sort',Language::_('ID'),'c.id',$lists['orderField'],$lists['orderDir'] );

echo AHTML::_('tableList.head',$headHtml );

$i = 0;
foreach($rows as $r )
{
	$ord = AHTML::getHtmlOrder($pagination, $i, count($rows), $r->ordering, 1 , 1 ,($lists['orderField']=='c.ordering' && $lists['orderDir'] == 'ASC' ) );
	$bodyHtml = array();
	
	$bodyHtml[] = array(AHTML::checkboxlist2($r->id, $i ) , 'align="center"');
	$bodyHtml[] = array($pagination->getRowOffset($i), 'align="center"');
	
	$bodyHtml[] = '<a href="index.php?m=course&t=edit&c=comment&cid[]='.$r->id.'">'.$r->name.'</a>';
	$bodyHtml[] = array($r->description, '');
	$bodyHtml[] = array(AHTML::enable($r->enable, $i),'align="center"');
	$bodyHtml[] = array( $ord, 'align="center" nowrap="nowrap"');
	$bodyHtml[] = array($r->id, 'align="center"');
	
	echo AHTML::_('tableList.body', $bodyHtml, $i );
	$i++;
}

echo AHtml::_('tableList.end');

echo $pagination->getAdminFooterList();
$hiddenList = array(
	'm' 		=> 'course',
	't'			=> 'home',
	'c'			=> 'comment',
	'boxchecked'=> 0,
	'order' 	=> '',
	'page' 		=> '',
	'section'	=> 'home',
	'order_field'	=> $lists['orderField'],
	'order_dir'		=> $lists['orderDir']
);

echo AHTML::_('tableList.endForm', $hiddenList);

?>
