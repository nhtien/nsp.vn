<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Trần Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

$tabsArr['tabs1'] = Language::_('General Information') ; 
?>
<script language="javascript">
function submitbutton(t) 
{
	var f = document.adminForm;
	if (t == 'cancel') 	{submitform(t);	return;	}

	if (f.name.value == "") 
	{
		alert( "<?php echo Language::_('Photo must have a Title');?>." );
		f.name.focus();
		return false;
	}
	if( f.catid.value==''){
		alert( "<?php echo Language::_('You must select a Category'); ?>." );
		 f.catid.focus();
		return false;
	}
	
	submitform(t);
}
</script>

<form action="index.php" class="adminForm" method="post" name="adminForm" enctype="multipart/form-data">
<?php 
echo $tabs->startTabs( $tabsArr );
echo $tabs->startTab();
?>

<table class="adminForm" cellspacing="0">
  <tr>
    <td class="label"><?php echo Language::_('Photo ID'); ?></td>
    <td><?php echo $photo->id ?></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Course Name'); ?></td>
    <td><input type="text" name="name" value="<?php echo htmlspecialchars($photo->name); ?>" class="inputbox" size="80" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Alias')?> </td>
    <td><input type="text" name="alias" value="<?php echo $photo->alias; ?>" class="inputbox" size="80" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Category')?> </td>
    <td><?php echo $categories?></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Enable')?> </td>
    <td><?php echo HTML::booleanlist('enable','class="inputbox"',$photo->enable);?></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Image')?></td>
	<td> 
		<input name="file" type="file" class="inputbox" />
        <p class="note">Ảnh sẽ được tự động điều chỉnh kích thước trên website</p>
        <?php if($photo->filename){
			echo '<p>Size: '.$photo->f_width.' x '.$photo->f_height.' px</p>';
			echo '<img src="'.get_img_url('photo/'.$photo->filename, $pconfig->full_width, $pconfig->full_height).'" />';
		}?>
	</td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Short Description')?> </td>
    <td><textarea name="short_desc" class="inputbox" rows="8" cols="80"><?php echo $photo->short_desc; ?></textarea></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Description')?> </td>
    <td> 
		<?php echo Editor::show('description', $photo->description,'100%','500','20','120', true);?>
	
	</td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Meta Title')?> </td>
    <td><input type="text" name="meta_title" value="<?php echo $photo->meta_title; ?>" class="inputbox" size="120" maxlength="255" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Meta Keywords')?> </td>
    <td><input type="text" name="meta_key" value="<?php echo $photo->meta_key; ?>" class="inputbox" size="120" maxlength="400" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Meta Description')?> </td>
    <td><input type="text" name="meta_desc" value="<?php echo $photo->meta_desc; ?>" class="inputbox" size="120" maxlength="400" /></td>
  </tr>
</table>
<?php
echo $tabs->endTab();
echo $tabs->endTabs()
?>

<input type="hidden" name="m" value="photo" />
<input type="hidden" name="t" value="save" />
<input type="hidden" name="id" value="<?php echo $photo->id?>" />
</form>