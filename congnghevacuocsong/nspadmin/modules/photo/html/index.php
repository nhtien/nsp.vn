<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

$tpl = Request::getVar('tpl');
if( $tpl=='m')
{
$functionName = Request::getVar('fc','selectArticle');
?>
<div class="mbt_bar">
	<button class="button" onclick="_selectArticle();"><?php echo Language::_('Select')?></button>&nbsp;
	<button class="button" onclick="window.parent.document.getElementById('sbox-window').close();"><?php echo Language::_('Cancel')?></button>
</div>
<script language="javascript">
function _selectArticle()
{
	cbName = 'cb';
	var n = <?php echo count($rows)?>;
	var frm = document.adminForm;
	var cids = new Array();
	var pnams = new Array();
	var j = 0;
	for (i=0; i < n; i++) {
		cb = eval( 'frm.' + cbName + '' + i );
		if (cb.checked == true) {
			cids[j] = cb.value;
			pnams[j] = cb.title;
			j++;
		}
	}
	

	window.parent.<?php echo $functionName;?>(cids,pnams);
	window.parent.document.getElementById('sbox-window').close();
}
</script>
<?php
} // if

echo AHTML::_('tableList.startForm');
echo AHTML::_('tableList.top', $lists['searchbox'] . $lists['categories'] . ' ' . $lists['status'] ); 
echo AHtml::_('tableList.start');

$headHtml = array();

$headHtml[] = AHTML::cbcheckall( count($rows));
$headHtml[] = '#';
$headHtml[] = AHTML::_('tableList.sort', Language::_('Title') ,'p.name',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.sort', Language::_('Image'), 'filename', $lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.sort', Language::_('File Name'),'filename',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.sort', Language::_('Category Name'),'category_name',$lists['orderField'],$lists['orderDir'] ); 
$headHtml[] = AHTML::_('tableList.sort',Language::_('Created Date'),'p.created',$lists['orderField'],$lists['orderDir'] );
$headHtml[] = AHTML::_('tableList.sort', Language::_('Hits'),'p.hits',$lists['orderField'],$lists['orderDir'] );
if( $tpl!='m'){
	$headHtml[] = AHTML::_('tableList.orderTitle', count($rows), 'p.catid,p.ordering',$lists['orderField'],$lists['orderDir'] );
	$headHtml[] = AHTML::_('tableList.sort', Language::_('Enable'),'p.enable',$lists['orderField'],$lists['orderDir'] );
}
$headHtml[] = AHTML::_('tableList.sort', Language::_('ID'),'p.id',$lists['orderField'],$lists['orderDir'] );

echo AHTML::_('tableList.head',$headHtml );

for($i = 0 ; $i < count($rows) ; $i++ )
{
	$r = $rows[$i];
	$ord = AHTML::getHtmlOrder($pagination, $i, count($rows), $r->ordering, ($r->catid == @$rows[$i-1]->catid) , ($r->catid == @$rows[$i+1]->catid) ,($lists['orderField']=='p.catid,p.ordering' && $lists['orderDir'] == 'ASC' ) );
	
	if( $tpl == 'm')
		$courseTitle = $r->name;
	else
		$courseTitle = '<a href="index.php?m=photo&t=edit&cid[]='.$r->id.'">'.$r->name.'</a>';
	if($r->filename){
		$image = '<img src="'.get_img_url('photo/'.$r->filename, 70, 70, true, true).'" />';
	}else
		$image = '';
		
	$bodyHtml = array();
	
	$bodyHtml[] = array(AHTML::checkboxlist2($r->id, $i ) , 'align="center"');
	$bodyHtml[] = array($pagination->getRowOffset($i), 'align="center"');
	$bodyHtml[] = $courseTitle;
	$bodyHtml[] = array($image, 'align="center"');
	$bodyHtml[] = substr($r->filename,0, 40);
	$bodyHtml[] = $r->category_name;
	$bodyHtml[] = array( Date::mySQL2String($r->created), 'align="center"');
	$bodyHtml[] = array($r->hits, 'align="center"');
	if( $tpl!='m'){
		$bodyHtml[] = array( $ord, 'align="center" nowrap="nowrap"' );
		$bodyHtml[] = array( AHTML::enable($r->enable, $i), 'align="center"');
	}
	$bodyHtml[] = array($r->id, 'align="center"');
	
	echo AHTML::_('tableList.body', $bodyHtml, $i );
}
echo AHtml::_('tableList.end');
echo $pagination->getAdminFooterList();

$hiddenList = array(
	'm' 			=> 'photo',
	't'				=> '',
	'boxchecked'	=> 0,
	'order' 		=> '',
	'page' 			=> '',
	'section'		=> 'photo',
	'tpl'			=> $tpl,
	'order_field'	=> $lists['orderField'],
	'order_dir'		=> $lists['orderDir']
);

echo AHTML::_('tableList.endForm', $hiddenList);

?>
