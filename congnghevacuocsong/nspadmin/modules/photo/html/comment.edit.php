<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Trần Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

$tabsArr['tabs1'] = Language::_('General Information') ;

?>
<form action="index.php" class="adminForm" method="post" name="adminForm" enctype="multipart/form-data">
<?php 
echo $tabs->startTabs( $tabsArr );
echo $tabs->startTab();
?>

<table class="adminForm" cellspacing="0">
  <tr>
    <td class="label"><?php echo Language::_('ID')?></td>
    <td><?php echo $row->id?></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Name')?></td>
    <td><input type="text" name="name" value="<?php echo $row->name?>" class="inputbox" size="70" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Course')?> </td>
    <td><input type="text" name="course" value="<?php echo $row->course?>" class="inputbox" size="70" /></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Enable')?> </td>
    <td><?php echo HTML::booleanlist('enable','class="inputbox"',$row->enable);?></td>
  </tr>
  <tr>
    <td class="label"><?php echo Language::_('Comment')?> </td>
    <td><textarea name="description" class="inputbox" rows="8" cols="70"><?php echo $row->description?></textarea></td>
  </tr>
</table>
<?php
echo $tabs->endTab();
echo $tabs->endTabs();
?>

<input type="hidden" name="m" value="course" />
<input type="hidden" name="c" value="comment" />
<input type="hidden" name="t" value="" />
<input type="hidden" name="id" value="<?php echo $row->id?>" />
</form>