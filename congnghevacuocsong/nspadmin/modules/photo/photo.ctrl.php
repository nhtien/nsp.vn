<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class PhotoCtrl extends Controller
{

	function __construct()
	{
		parent::__construct();
	}
	
	function home()
	{
		global $session, $module;
		import('pagination');
		import('date');
		$db = new Database;
		
		$keyword 	= trim(Session::getStateFromRequest('photo.keyword','search','','photo'));
		$category 	= Session::getStateFromRequest('photo.filter_catid','catid','','photo');
		$status 	= Session::getStateFromRequest('photo.filter_status','status','','photo');
		
		$orderField	= Session::getStateFromRequest('photo.order_field','order_field','p.created','photo');
		$orderDir	= Session::getStateFromRequest('photo.order_dir','order_dir','DESC','photo');
		
		$where = array();
		$where[] = '1';
		if( $keyword )
		{
			$where[] = 'p.name LIKE \'%'.$keyword.'%\'';
		}
		
		if( $category )
		{
			$where[] = 'p.catid = '.(int)$category;
		}
		
		if( $status=='1' || $status=='0' )
		{
			$where[] = 'p.enable='.(int)$status;
		}
		
		$where = ' WHERE '.implode(" AND ", $where);
		
		if( $orderField )
		{
			$orderby 	= ' ORDER BY '. $orderField .' '. $orderDir;
		}
		$groupby = ' GROUP BY p.id';
		$sql = "SELECT p.*, c.name AS category_name
					FROM #__photo as p 
					LEFT JOIN #__photo_categories  as c ON c.id = p.catid
					".$where.$groupby.$orderby;
		$db->setQuery($sql);
		$total = count($db->loadObjectList());
		
		$limit = Session::getStateFromRequest('limit', 'limit', 20, 'photo');
		$pagination = new Pagination( $total, $limit, true); 
		
		$limit = ' LIMIT '.$pagination->get('limitstart',0).','. $pagination->get('limit');
		$db->setQuery($sql.$limit);
		$rows = $db->loadObjectList();
		
		/* filter category */
		$db->setQuery( 'SELECT * FROM #__photo_categories ORDER BY name');
		$rows_cat = $db->loadObjectList();
		$lists['categories'] = HTML::_('tree.selectBox', $rows_cat,'catid','class="inputbox" onchange="this.form.submit();"', 'id','name', $category, '', Language::_('photo CATEGORY'));
		
		$lists['status'] = HTML::_('form.statusList', 'status',$status);
		
		
		$lists['searchbox'] = HTML::_('form.searchBox', $keyword);
		$lists['orderField']= $orderField;
		$lists['orderDir'] 	= $orderDir;
		
		$tmpl = new Template();
		$tmpl->set('rows', $rows);
		$tmpl->set('lists', $lists);
		$tmpl->set('pagination', $pagination);
		$tmpl->display();
		
	}
	/* category */
	function categories()
	{
		global $session, $module;
		import('pagination');
		$db = new Database;
		
		$orderField	= Session::getStateFromRequest('cat.order_field','order_field','c.ordering','photo');
		$orderDir	= Session::getStateFromRequest('cat.order_dir','order_dir','ASC','photo');
		$limit 		= Session::getStateFromRequest('categories.limit', 'limit', 20, 'photo');
		$keyword 	= trim(Session::getStateFromRequest('cat.keyword','search','','photo'));
		$status 	= Session::getStateFromRequest('cat.filter_status','status','','photo');
		
		$where[] = 1;
		if($keyword)
		{
			$where[] = '(c.name LIKE \'%'.$keyword.'%\' OR c.id =\'%'.$keyword.'%\')';
		}
		if( $status == '0' || $status == '1' )
		{
			$where[] = 'c.enable = '.(int)$status;
		}
		$where = ' WHERE '.implode(" AND ",$where);

		$groupby = ' GROUP BY c.id';
		if( $orderField )
		{
			$orderby 	= ' ORDER BY c.parent, '. $orderField .' '. $orderDir;
		}
		
		$sql = "SELECT c.*, count(a.id) AS total_article 
					FROM #__photo_categories as c 
					LEFT JOIN #__photo  as a ON a.catid = c.id" 
					.$where
					.$groupby
					.$orderby;
					
		$db->setQuery($sql);
		$rows =$db->loadObjectList();
		$total = count($rows); 
		
		$pagination = new Pagination( $total, $limit, true);
		$rows = HTML::_('tree.categoriesList', $rows, $pagination->get('limit'), $pagination->get('limitstart',0) );
		
		$lists['orderField'] 	= $orderField;
		$lists['orderDir'] 		= $orderDir;
		$lists['searchbox'] 	= HTML::_('form.searchBox', $keyword);
		$lists['status'] 		= HTML::_('form.statusList', 'status',$status);
		
		$tmpl = new Template();
		$tmpl->set('rows', $rows);
		$tmpl->set('pagination', $pagination);
		$tmpl->set('lists', $lists);
		$tmpl->display();
	}
	
	function edit_cat()
	{
		global $module, $session, $database, $language;
		import('tabs');
		$tabs= new Tabs('cattabs');
		$cid = Request::getVar('cid');
		$id = $cid[0];
		
		if( $id ){
			$cat = &Table::_('photo_category');
			$cat->load($id);
		}else{
			$tmpdata = Session::unserialize_post2obj('cat.tmp',null , $module);
			if( $tmpdata  ){
				$cat = ($tmpdata);
			}
		}
		
		if( $cat->parent ) $whereand = ' AND id <> '.$cat->id;
		$sql = "SELECT * FROM #__photo_categories WHERE enable = 1 ".$whereand;
		$rows = $database->loadObjectList($sql);
		
		$parent = HTML::_('tree.selectBox', $rows ,'parent','class="inputbox"', 'id', 'name', $cat->parent);
		
		$tmpl = new Template();
		$tmpl->set('cat', $cat);
		$tmpl->set('parent', $parent);
		$tmpl->set('tabs', $tabs);
		$tmpl->set('language', $language);
		$tmpl->display();
	}
	
	function save_cat()
	{
		global $database, $session, $module, $task, $language;
		$cat 	= Table::_('photo_category');
		$post 	= Request::get('post');
		
		$post['name'] 		= Request::getString('name');
		$post['alias'] 		= String::toAlias($post['alias']);
		$post['alias'] 		= $post['alias'] ? $post['alias'] : String::toAlias($post['name']);
		$post['description'] = Request::getString('description');
		
		$post['meta_title']	= String::toMetaKeyWords_Desc($post['meta_title']);
		$post['meta_key']	= String::toMetaKeyWords_Desc($post['meta_key']);
		$post['meta_desc']	= String::toMetaKeyWords_Desc($post['meta_desc']);
		
		$link_add 	= "index.php?m=photo&t=add_cat";
		$link_cat	= "index.php?m=photo&t=categories";
		
		if( !$post['name'] )
		{
			Session::serialize_post2obj('cat.tmp',$post, $module);
			$this->redirect($link_add, Language::_('MSG_INPUT_NOT_NULL'),2);
		}
		
		$cat->bind($post);
		
		Session::unregister('cat.tmp', $module);
		$isNew = $cat->isNew();
		
		if( $isNew )
		{
			$cat->ordering = $cat->getNextOrder('parent = '.(int) $cat->parent);
		}
		
		if( !$isNew )
		{
			$msg = Language::_('MSG_UPDATE_SUCCESSFUL');
			$cat->update();
		}
		else if( $cat->save() )
		{
			$msg = Language::_('MSG_SAVE_SUCCESSFUL');
		}
		else
		{
			$msg = Language::_('MSG_SAVE_UNSUCCESSFUL');
		}
		
		if($task=='apply_cat')
		{
			$link_cat = 'index.php?m=photo&t=edit_cat&cid[]='.$cat->id;
		}
		
		$this->redirect($link_cat, $msg);
		
	}
	
	function cancel_cat()
	{
		$this->redirect("index.php?m=photo&t=categories");
	}
	
	function delete_cat()
	{
		global $database;
		$row = Table::_('photo_category');
		$cid = Request::getVar('cid');
		$link = "index.php?m=photo&t=categories";
		$count = 0;
		for($i=0;$i < count($cid);$i++)
		{
			$row->load( $cid[$i]);
			if( $row->checkDel())
			{
				$count++;
				$row->delete($cid[$i]);
			}else
			{
				Session::setMsg( sprintf(Language::_('ERROR_CANT_DELETE_CATEGORY'),$row->name,$row->id) ,2);
			}
		}
		if( $count )
		{
			$msg = Language::_('MSG_DELETE_SUCCESSFUL') .' '. sprintf(Language::_('MSG_DELETE_ARTICLE_CATEGORY'), $count);
		}
	
		$this->redirect($link, $msg );
		return true;
	}
	
	
	/* Photo */
	
	function edit()
	{
		global $module, $database, $language;
		import( array('editor','tabs') );
		$pconfig = Module::loadConfig();
		$tabs = new Tabs('articleTabs');
		$cid = Request::getVar('cid'); 
		$id = $cid[0];
		
		if( $id ){
			$photo = Table::_('photo');
			$photo->load($id);
		}else{
			$tmpdata = Session::unserialize_post2obj('photo.tmp',null , $module);
			if( $tmpdata  )
			{
				$photo = ($tmpdata);
			}
		}
		
		$sql = "SELECT * FROM #__photo_categories WHERE 1 ORDER BY parent, name";
		$rows = $database->loadObjectList($sql);
		if(!$photo->catid)	$photo->catid	= Request::getVar('catid','','post');
		
		$categories = HTML::_('tree.selectBox', $rows ,'catid','class="inputbox"', 'id', 'name', $photo->catid);
		
		$tmpl = new Template();
		$tmpl->set('photo', $photo);
		$tmpl->set('pconfig', $pconfig);
		$tmpl->set('categories', $categories);
		$tmpl->set('language', $language);
		$tmpl->set('tabs', $tabs);
		$tmpl->display();
	}
	
	function save()
	{
		global $database, $session, $module, $task, $user, $language;
		$photo = Table::_('photo');
		$pconfig = Module::loadConfig();
		$post = Request::get('post');
		import('image');
		
		$post['name'] 	= Request::getString('name');
		$post['alias'] 	= String::toAlias($post['alias']);
		$post['alias'] 	= $post['alias'] ? $post['alias'] : String::toAlias($post['name']);
		$post['meta_title']	= String::toMetaKeyWords_Desc($post['meta_title']);
		$post['meta_key']	= String::toMetaKeyWords_Desc($post['meta_key']);
		$post['meta_desc']	= String::toMetaKeyWords_Desc($post['meta_desc']);
		
		$post['short_desc']		= Request::getString('short_desc');
		$post['description'] 	= Request::getString('description');
		$link_edit 		= "index.php?m=photo&t=edit";
		$link_article	= "index.php?m=photo";
		
		if( !$post['name'] || !$post['catid'] )
		{
			if( !$post['id'] )
				Session::serialize_post2obj('photo.tmp',$post, $module);
				
			$this->redirect($link_edit, Language::_('MSG_INPUT_NOT_NULL'),2);
		}
		
		if(!$post['id'])
		{
			import('date');
			$datenow = new Date;
			$post['created'] = $datenow->toMySQL();
			$post['created_by'] = $user->id;
		}else
		{
			$photo->load( $post['id']);
		}
		
		/* upload thumb */
		$file = $_FILES['file'];
		
		if( $file['name'] )
		{
			if( $result = Image::upload($file, $pconfig->path_file, NULL, $pconfig->max_upload_w, $pconfig->max_upload_h))
			{
				@File::delete( $pconfig->path_file.DS.$photo->filename );
				$post['filename'] = $result['name'];
				$info = getimagesize($pconfig->path_file.DS.$result['name']);
				$post['f_width'] 	= $info[0];
				$post['f_height'] 	= $info[1];
			}
		}
		
		$photo->bind($post);
		
		Session::unregister('photo.tmp', $module);
		$isNew = $photo->isNew();
		if( $isNew )
		{
			$photo->ordering = $photo->getNextOrder('catid='.(int)$photo->catid );
		}
		
		if( !$isNew )
		{
			$msg = Language::_('MSG_UPDATE_SUCCESSFUL');
			$photo->update();
		}
		else if( $photo->save() )
		{
			$msg = Language::_('MSG_SAVE_SUCCESSFUL');
		}else
		{
			$msg = Language::_('MSG_SAVE_UNSUCCESSFUL');
		}
		if($task=='apply')
		{
			$link_article = 'index.php?m=photo&t=edit&cid[]='.$photo->id;
		}
		
		$this->redirect($link_article, $msg);
	}
	
	function delete()
	{
		global $db;
		import('file');
		$pconfig = Module::loadConfig();
		$row = Table::_('photo');
		$cid = Request::getVar('cid');
		$link = "index.php?m=photo";
		for	($i=0;$i < count($cid);$i++)
		{
			$row->load($cid[$i]);
			if($row->filename){
				$thumb_src = $pconfig->path_file.DS.$row->filename;
				if(file_exists($thumb_src)) File::delete( $thumb_src );
			}
			$row->delete();
			
		}
		$this->redirect($link, Language::_('MSG_DELETE_SUCCESSFUL') );
		return true;
	}
	
	
	function cancel()
	{
		$this->redirect("index.php?m=photo");
	}
	
	function enable()
	{
		global $task, $database;
		$sect = Request::getVar('section');
		$enable = $task == 'enable' ? 1 : 0;
		$cid = Request::getVar('cid'); 
		$cids = implode( ',', $cid );
		switch( $sect ){
			case 'category':
				$link = 'index.php?m=photo&t=categories';
				$sql = "UPDATE #__photo_categories SET enable = $enable WHERE id IN ($cids)";
				break;
			default:
				$link = 'index.php?m=photo';
				$sql = "UPDATE #__photo SET enable = $enable WHERE id IN ($cids)";
				break;
		}
		
		$database->setQuery( $sql );
		$database->query();
		$this->redirect($link, Language::_('MSG_UPDATE_SUCCESSFUL') );
	}
	function saveorder()
	{
		$cid = Request::getVar('cid');
		$order = Request::getInt('order',0);
		$cid = (int) $cid[0];
		
		$section = Request::getVar('section','');
		switch( $section )
		{
			case 'category':
				$row =& Table::_('photo_category');
				$row->load( $cid );
				$link = 'index.php?m=photo&t=categories';
				$where = 'parent='.(int)$row->parent ;
				break;
			default:
				$row =& Table::_('photo');
				$row->load( $cid );
				$link = 'index.php?m=photo';
				$where = 'catid = '.(int)$row->catid ;
				break;
		}
		
		if($row->saveorder($order) )
		{
			$msg = Language::_('MSG_SAVE_ORDER_SUCCESSFUL');
			$row->reorder( $where );
		}
		else{
			$msg = Language::_('MSG_UPDATE_UNSUCCESSFUL');
		}
		$this->redirect($link, $msg );
		
	}
	
	function saveorders()
	{
		global $database;
		$section = Request::getVar('section','');
		
		switch( $section )
		{
			case 'category';
				$row = Table::_('photo_category');
				$row->saveorders();
				
				$sql = 'SELECT DISTINCT parent FROM #__photo_categories ORDER BY parent';
				$database->setQuery( $sql );
				$orders = $database->loadObjectList();
				foreach( $orders as $r)
				{
					$row->reorder("parent = ".(int)$r->parent);
				}
				
				Session::set('cat.order_field','c.ordering','photo');
				Session::set('cat.order_dir', 'ASC','photo');
				$link = 'index.php?m=photo&t=categories';
				break;
			default:
				$row =& Table::_('photo');
				$row->saveorders();
				$sql = 'SELECT DISTINCT catid FROM #__photo ORDER BY catid';
				$database->setQuery( $sql );
				$orders = $database->loadObjectList();
				foreach( $orders as $r)
				{
					$row->reorder("catid = ".(int)$r->catid);
				}

				Session::set('photo.order_field','p.catid,p.ordering','photo');
				Session::set('photo.order_dir','ASC','photo');
				$link = 'index.php?m=photo';
				break;
		}
			
		
		$this->redirect($link, Language::_('MSG_SAVE_ORDER_SUCCESSFUL') );
	}
	
	
	function move()
	{
		global $task;
		$cid = Request::getVar('cid');
		$cid = (int) $cid[0];
		$inc	= ($task == 'moveup' ? -1 : 1);
		$section = Request::getVar('section','');

		switch( $section )
		{
			case 'category';
				$row =& Table::_('photo_category');
				$row->load( $cid );
				$link = 'index.php?m=photo&t=categories';
				$where = "parent = ".(int)$row->parent;
				break;
			default:
				$row =& Table::_('photo');
				$row->load( $cid );
				$link = 'index.php?m=photo';
				$where= 'catid = '.(int)$row->catid ;
				break;
		}
		
		
		
		$row->move( $inc, $where );
		$row->reorder($where);
		$this->redirect( $link );
	}
	
	
}
	
?>