<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class CourseCommentCtrl extends Controller
{

	function __construct()
	{
		parent::__construct();
	}
	
	function home()
	{
		global $session, $module;
		import('pagination');
		$db = new Database;
		
		$orderField	= Session::getStateFromRequest('comment.order_field','order_field','c.ordering','course');
		$orderDir	= Session::getStateFromRequest('comment.order_dir','order_dir','ASC','course');
		$limit 		= Session::getStateFromRequest('categories.limit', 'limit', 20, 'course');
		$keyword 	= trim(Session::getStateFromRequest('comment.keyword','search','','course'));
		$status 	= Session::getStateFromRequest('comment.filter_status','status','','course');
		
		$where[] = 1;
		if($keyword)
		{
			$where[] = '(c.name LIKE \'%'.$keyword.'%\' OR c.id =\'%'.$keyword.'%\')';
		}
		if( $status == '0' || $status == '1' )
		{
			$where[] = 'c.enable = '.(int)$status;
		}
		$where = ' WHERE '.implode(" AND ",$where);

		//$groupby = ' GROUP BY t.id';
		if( $orderField )
		{
			$orderby 	= ' ORDER BY '. $orderField .' '. $orderDir;
		}
		
		$sql = "SELECT c.*
					FROM #__course_comment as c 
					" 
					.$where
					.$orderby;
					
		$db->setQuery($sql);
		$rows =$db->loadObjectList();
		$total = count($rows); 
		
		$pagination = new Pagination( $total, $limit, true);
		
		$limit = ' LIMIT '.$pagination->get('limitstart',0).','. $pagination->get('limit');
		$db->setQuery($sql.$limit);
		$rows = $db->loadObjectList();
		
		$lists['orderField'] 	= $orderField;
		$lists['orderDir'] 		= $orderDir;
		$lists['searchbox'] 	= HTML::_('form.searchBox', $keyword);
		$lists['status'] 		= HTML::_('form.statusList', 'status',$status);
		
		$tmpl = new Template();
		$tmpl->set('rows', $rows);
		$tmpl->set('pagination', $pagination);
		$tmpl->set('lists', $lists);
		$tmpl->display();
	}
	
	function edit()
	{
		global $module, $session, $database, $language;
		$cconfig = Module::loadConfig();
		import('tabs');
		$tabs= new Tabs('cattabs');
		$cid = Request::getVar('cid');
		$id = $cid[0];
		
		if( $id ){
			$row = &Table::_('course_comment');
			$row->load($id);
		}else{
			$tmpdata = Session::unserialize_post2obj('comment.tmp',null , $module);
			if( $tmpdata  ){
				$row = ($tmpdata);
			}
		}

		$tmpl = new Template();
		$tmpl->set('row', $row);
		$tmpl->set('tabs', $tabs);
		$tmpl->set('cconfig', $cconfig);
		$tmpl->display();
	}
	
	function save()
	{
		global $database, $session, $module, $task, $language;
		import('image');
		$row 	= Table::_('course_comment');
		$post 	= Request::get('post');
		$cconfig = Module::loadConfig();
		
		$post['name'] 		= Request::getString('name');
		$post['alias'] 		= String::toAlias($post['alias']);
		$post['alias'] 		= $post['alias'] ? $post['alias'] : String::toAlias($post['name']);
		$post['description'] = Request::getString('description');
		
		$post['meta_title']	= String::toMetaKeyWords_Desc($post['meta_title']);
		$post['meta_key']	= String::toMetaKeyWords_Desc($post['meta_key']);
		$post['meta_desc']	= String::toMetaKeyWords_Desc($post['meta_desc']);
		
		$link_add 	= "index.php?m=course&c=comment&t=add";
		$link		= "index.php?m=course&c=comment";
		
		if( !$post['name'] )
		{
			Session::serialize_post2obj('comment.tmp',$post, $module);
			$this->redirect($link_add, Language::_('MSG_INPUT_NOT_NULL'),2);
		}
		
		
		$row->bind($post);
		
		Session::unregister('comment.tmp', $module);
		$isNew = $row->isNew();
		
		if( $isNew )
		{
			$row->ordering = $row->getNextOrder('');
		}
		
		if( !$isNew )
		{
			$msg = Language::_('MSG_UPDATE_SUCCESSFUL');
			$row->update();
		}
		else if( $row->save() )
		{
			$msg = Language::_('MSG_SAVE_SUCCESSFUL');
		}
		else
		{
			$msg = Language::_('MSG_SAVE_UNSUCCESSFUL');
		}
		
		if($task=='apply')
		{
			$link = 'index.php?m=course&t=edit&c=comment&cid[]='.$row->id;
		}
		
		$this->redirect($link, $msg);
		
	}
	
	function cancel()
	{
		$this->redirect("index.php?m=course&c=comment");
	}
	
	function delete()
	{
		global $database;
		import('file');
		$row = Table::_('course_comment');
		$cconfig = Module::loadConfig();
		$cid = Request::getVar('cid');
		$link = "index.php?m=course&c=comment";
		$count = 0;
		for($i=0;$i < count($cid);$i++)
		{
			$row->load($cid[$i]);
			if($row->thumb){
				$thumb_src = $cconfig->path_teacher.DS.$row->thumb;
				if(file_exists($thumb_src)) File::delete( $thumb_src );
			}
			$row->delete();
		}
		if( $count )
		{
			$msg = Language::_('MSG_DELETE_SUCCESSFUL') ;
		}
	
		$this->redirect($link, $msg );
		return true;
	}
	
	
	function saveorder()
	{
		$cid = Request::getVar('cid');
		$order = Request::getInt('order',0);
		$cid = (int) $cid[0];
		
		$section = Request::getVar('section','');
		switch( $section )
		{
			default:
				$row =& Table::_('course_comment');
				$row->load( $cid );
				$link = 'index.php?m=course&c=comment';
				$where = '';
				break;
		}
		
		if($row->saveorder($order) )
		{
			$msg = Language::_('MSG_SAVE_ORDER_SUCCESSFUL');
			$row->reorder( $where );
		}
		else{
			$msg = Language::_('MSG_UPDATE_UNSUCCESSFUL');
		}
		$this->redirect($link, $msg );
		
	}
	
	function saveorders()
	{
		global $database;
		$section = Request::getVar('section','');
		
		switch( $section )
		{
			default:
				$row =& Table::_('course_comment');
				$row->saveorders();
				$row->reorder('');

				Session::set('comment.order_field','c.ordering','course');
				Session::set('comment.order_dir','ASC','course');
				$link = 'index.php?m=course&c=comment';
				break;
		}
			
		
		$this->redirect($link, Language::_('MSG_SAVE_ORDER_SUCCESSFUL') );
	}
	
	
	function move()
	{
		global $task;
		$cid = Request::getVar('cid');
		$cid = (int) $cid[0];
		$inc	= ($task == 'moveup' ? -1 : 1);
		$section = Request::getVar('section','');

		switch( $section )
		{
			default:
				$row =& Table::_('course_comment');
				$row->load( $cid );
				$link = 'index.php?m=course&c=comment';
				$where= '' ;
				break;
		}
		
		
		
		$row->move( $inc, $where );
		$row->reorder($where);
		$this->redirect( $link );
	}
	
	function enable()
	{
		global $task, $database;
		$sect = Request::getVar('section');
		$enable = $task == 'enable' ? 1 : 0;
		$cid = Request::getVar('cid'); 
		$cids = implode( ',', $cid );
		switch( $sect ){
			default:
				$link = 'index.php?m=course&c=comment';
				$sql = "UPDATE #__course_comment SET enable = $enable WHERE id IN ($cids)";
				break;
		}
		
		$database->setQuery( $sql );
		$database->query();
		$this->redirect($link, Language::_('MSG_UPDATE_SUCCESSFUL') );
	}
}
	
?>