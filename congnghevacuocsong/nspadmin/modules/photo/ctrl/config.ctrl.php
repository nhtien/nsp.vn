<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class PhotoConfigCtrl extends Controller
{

	function __construct()
	{
		parent::__construct();
	}
	
	function home()
	{
		$row = Table::_('photo_config');
		$row = $row->load();
		
		$tmpl = new Template();
		$tmpl->set('row', $row);
		$tmpl->display();
		
	}
	
	function save()
	{
		global $task;
		$post = Request::get('post');
		$row = Table::_('photo_config');

		$row->save($post);
		
		if($task=='apply')
			$link = 'index.php?m=photo&c=config';
		else
			$link = 'index.php?m=photo';
			
		$this->redirect($link, Language::_('MSG_UPDATE_SUCCESSFUL') );
	}
	
	function cancel()
	{
		$this->redirect("index.php?m=photo");
	}
	
}
	
?>