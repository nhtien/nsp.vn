User-agent: *

Disallow: /admin/

Disallow: /administrator/

Disallow: /backup/

Disallow: /blocks/

Disallow: /cache/

Disallow: /images/

Disallow: /libraries/

Disallow: /modules/

Disallow: /options/

Disallow: /theme/

Disallow: /tmp/

Disallow: /uploads/

Disallow: /nspadmin/

