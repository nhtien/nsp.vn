<?php
require_once('lib/captcha.class.php');
require_once('lib/mysql.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="EATON, EATON UPS 9PX" />
<meta name="description" content="GIẢI PHÁP BẢO VỆ VÀ TIẾT KIỆM NĂNG LƯỢNG CHO TRUNG TÂM DỮ LIỆU VỪA VÀ NHỎ" />
<title>GIẢI PHÁP BẢO VỆ VÀ TIẾT KIỆM NĂNG LƯỢNG CHO TRUNG TÂM DỮ LIỆU VỪA VÀ NHỎ</title>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16757079-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<style type="text/css">
.primary{
	width:750px;
	margin:0 auto;
	border:1px solid #999;
	padding:10px;
}
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	line-height:1.5;
}
table.frm td.label{
	text-align:right;
	padding-right:3px;
}
li{margin-bottom:7px;}
</style>
<script language="javascript">
function checkfrm(){
	if(document.frmsend.company.value==''){
		alert('Nhập vào tên công ty');
		document.frmsend.company.focus();
		return false;
	}
	if(document.frmsend.address.value==''){
		alert('Nhập vào địa chỉ công ty');
		document.frmsend.address.focus();
		return false;
	}
	if(document.frmsend.name.value==''){
		alert('Nhập vào tên của bạn');
		document.frmsend.name.focus();
		return false;
	}
	if(document.frmsend.email.value==''){
		alert('Nhập vào địa chỉ email của bạn');
		document.frmsend.email.focus();
		return false;
	}
	if(document.frmsend.phone.value==''){
		alert('Nhập vào số điện thoại liên hệ của bạn');
		document.frmsend.phone.focus();
		return false;
	}
	return true;
}
</script>

</head>

<body>
<div class="primary">
    <div class="logo">
        <a href="http://www.nsp.com.vn" target="_blank"><img src="http://nsp.com.vn/email-marketing/eaton/08-2011/Eaton-T8_07.jpg" width="180" height="67" border="0" align="right" /></a>
        <a href="http://www.eaton.com"><img src="http://nsp.com.vn/email-marketing/eaton/08-2011/Eaton-T8_09.jpg" border="0" width="121" height="67" align="left"></a>
    
    
    </div>

<br style="clear:both" /><br />
<div style="background:#dcf4f6; padding:15px 0 15px 5px;">
  <h2 style="margin:0; text-align:center; font-size:18px;"><span style="color:#666">Đăng ký tham dự hội thảo</span><br />
  <span style="color:#dcc713">GIẢI PHÁP BẢO VỆ VÀ TIẾT KIỆM NĂNG LƯỢNG <br />
  CHO TRUNG TÂM DỮ LIỆU VỪA VÀ NHỎ</span></h2>
<div style="clear:both; font-size:1px;"></div>
</div>
<?php
if($_REQUEST['send']):?>
<div style="border-top:3px solid #0a21ae;border-bottom:3px solid #0a21ae; padding:10px 5px; margin-top:5px; background:#CFF; font-size:16px; font-weight:bold; color:#0a21ae">Thông tin đăng ký của bạn đã gửi thành công.</div>
<?php endif?>
<div style="height:10px;">&nbsp;</div>
<div style="float:left; width:49%;">
    <h4>CHƯƠNG TRÌNH:</h4>
    <table width="100%" border="0" cellspacing="0" cellpadding="5" style="font-family:Arial, Helvetica, sans-serif; font-size:10pt;">
      <tr>
        <td>&nbsp;</td>
        <td align="left" valign="top">09:00  - 09:30&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&#272;&oacute;n kh&aacute;ch.</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="left" valign="top" nowrap="nowrap">09:30 - 09:50&nbsp;&nbsp;&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">C&ocirc;ng b&#7889; NSP ph&acirc;n ph&#7889;i &#273;&#7897;c quy&#7873;n  EATON UPS 9PX.</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="left" valign="top">09:50 - 10:40&nbsp;&nbsp;&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">Gi&#7843;i ph&aacute;p b&#7843;o v&#7879; v&agrave; ti&#7871;t ki&#7879;m &#273;i&#7879;n  n&#259;ng&nbsp;cho  trung t&acirc;m d&#7919; li&#7879;u v&#7915;a v&agrave; nh&#7887;.</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="left" valign="top">10:40 - 10:55&nbsp;&nbsp;&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">Ngh&#7881; gi&#7843;i lao.</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="left" valign="top">10:55 - 11:15&nbsp;&nbsp;&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">Ch&#432;&#417;ng tr&igrave;nh ca nh&#7841;c  th&#7875; lo&#7841;i Acoustic.</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="left" valign="top">11:15 - 11:40&nbsp;&nbsp;&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">Tr&igrave;nh di&#7877;n sản phẩm EATON UPS 9PX.</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="left" valign="top">11:40  - 12:00&nbsp;&nbsp;&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">Ch&#432;&#417;ng tr&igrave;nh khuy&#7871;n m&atilde;i v&agrave; r&uacute;t th&#259;m tr&uacute;ng th&#432;&#7903;ng.</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td align="left" valign="top">12:00 - 13:00&nbsp;&nbsp;&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">Ti&#7879;c tr&#432;a.</td>
      </tr>
    </table>
    <p>&nbsp;</p>
</div>
<div style="float:right; width:49%; border:1px solid #CCC;">
<form action="send.php" method="post" name="frmsend" onsubmit="return checkfrm();">
<div style="background:#dcf4f6; padding:10px 0 10px 5px;"><strong>Quý khách vui lòng điền đầy đủ các thông tin sau đây:</strong></div>
<i>&nbsp;Các trường có dấu (*) là bắt buộc nhập dữ liệu</i>
<table width="auto" border="0" cellspacing="0" cellpadding="5" class="frm">
  <tr>
    <td class="label">Tên công ty (*)</td>
    <td><input type="text" name="company" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Địa chỉ công ty (*)</td>
    <td><input type="text" name="address" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Tên người tham dự (*)</td>
    <td><input type="text" name="name" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ email (*)</td>
    <td><input type="text" name="email" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Điện thoại liên hệ (*)</td>
    <td><input type="text" name="phone" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Website</td>
    <td><input type="text" name="website" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Mã bảo vệ (*)</td>
    <td><?php 
	$captcha = new NSPCaptcha();
	echo $captcha->htmlCaptcha();
	?>
	</td>
  </tr>
  <tr>
    <td class="label">&nbsp;</td>
    <td><input type="submit" name="submit" value="Đăng ký" class="button" /></td>
  </tr>
</table>
</form>
</div>
<div style="clear:both;">&nbsp;</div>
</div>

</div>
</body>
</html>
