<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Training giai phap ip Camera ACTi</title>
<style type="text/css">
.primary{
	width:750px;
	margin:0 auto;
	border:1px solid #999;
	padding:10px;
}
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	line-height:1.5;
}
table.frm td.label{
	text-align:right;
	padding-right:3px;
}
li{margin-bottom:7px;}
</style>
<script language="javascript">
function checkfrm(){
	if(document.frmsend.company.value==''){
		alert('Nhập vào tên công ty');
		document.frmsend.company.focus();
		return false;
	}
	if(document.frmsend.name.value==''){
		alert('Nhập vào tên của bạn');
		document.frmsend.name.focus();
		return false;
	}
	if(document.frmsend.email.value==''){
		alert('Nhập vào địa chỉ email của bạn');
		document.frmsend.email.focus();
		return false;
	}
	if(document.frmsend.phone.value==''){
		alert('Nhập vào số điện thoại liên hệ của bạn');
		document.frmsend.phone.focus();
		return false;
	}
	return true;
}
</script>
</head>

<body>
<div class="primary">
    <div class="logo">
        <a href="http://www.nsp.com.vn" target="_blank"><img src="images/logo.png" border="0" style="float:left" /></a> 
        <a href="http://acti.com" target="_blank"><img src="images/logo-ACTi.png" border="0" style="float:right" /></a>
    </div>

<br style="clear:both" /><br /><br /><br />
<div style="background:#dcf4f6; padding:15px 0 15px 5px;">
  <h2 style="margin:0; float:left; font-size:18px;">Training "Giải pháp IP  Camera ACTi" <br />Tham dự & nhận ngay Coupon</h2>
<div style="float:right; margin-top:-50px"><img src="images/Coupon-ACTi.png" border="0" style="margin-right:3px;" /></div>
<div style="clear:both; font-size:1px;"></div>
</div>
<?php
if($_REQUEST['send']):?>
<div style="border-top:2px solid #0C0;border-bottom:2px solid #0C0; padding:5px; margin-top:5px; background:#CFF; font-size:15px; color:#0C0">Thông tin đăng ký của bạn đã gửi thành công.</div>
<?php endif?>
<div style="height:10px;">&nbsp;</div>
<div style="float:left; width:49%;">
    <h4>Lợi ích cho các doanh nghiệp tham dự Training</h4>
    <ol>
      <li>Thêm lựa chọn mới cho thị trường IP Camera tại VN.</li>
      <li>Nắm được xu hướng hệ thống giám       sát CCTV trên thế giới hiện nay.</li>
      <li>Nhận biết các công nghệ camera       IP Megapixel thế hệ mới nhất.</li>
      <li>Trao đổi trực tiếp với chuyên       gia camera từ hãng.</li>
      <li>Coupon&nbsp; ACTi  <strong>trị       giá 50 USD (tương đương&nbsp; 1,029,000 vnđ )</strong>        cho các doanh nghiệp đăng ký tham dự training. </li>
    </ol>
    <p>&nbsp;</p>
</div>
<div style="float:right; width:49%; border:1px solid #CCC;">
<form action="send.php" method="post" name="frmsend" onsubmit="return checkfrm();">
<div style="background:#dcf4f6; padding:10px 0 10px 5px;"><strong>Quý khách vui lòng điền đầy đủ các thông tin sau đây:</strong></div>
<i>&nbsp;Các trường có dấu (*) là bắt buộc nhập dữ liệu</i>
<table width="auto" border="0" cellspacing="0" cellpadding="5" class="frm">
  <tr>
    <td class="label">Tên công ty (*)</td>
    <td><input type="text" name="company" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Tên người tham dự (*)</td>
    <td><input type="text" name="name" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ email (*)</td>
    <td><input type="text" name="email" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Điện thoại liên hệ (*)</td>
    <td><input type="text" name="phone" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Website</td>
    <td><input type="text" name="website" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Sở thích</td>
    <td><input type="text" name="sothich" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">&nbsp;</td>
    <td><input type="submit" name="submit" value="Đăng ký" class="button" /></td>
  </tr>
</table>
</form>
</div>
<div style="clear:both;">&nbsp;</div>
</div>

</div>
</body>
</html>
