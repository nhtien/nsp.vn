<?php
require_once('lib/captcha.class.php');
require_once('lib/mysql.php');


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Đăng ký tham dự hội thảo Trải nghiệm giải pháp Giám sát hình ảnh toàn diện</title>
<style type="text/css">
.primary{
	width:648px;
	margin:0 auto;
	border:1px solid #999;
	background:#eeeeee;
}
.primary > .inner{
	margin:15px;
}
.primary .logo{
	margin:0 50px;
}
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	line-height:1.5;
}
table.frm{
	font-size:11px;
}
table.frm td.label{
	text-align:right;
	padding-right:3px;
}
li{margin-bottom:7px;}

table.frm .inputbox{
	width:150px;
}
td.h{
	font-weight:bold;
	color:#bf1e2e;
	font-size:13px;
}
.button{
	background:url(images/bt.png) no-repeat;
	width:90px;
	height:30px;
	border:none;
}
</style>
<script language="javascript">
function checkfrm(){
	if(document.frmsend.company.value==''){
		alert('Nhập vào tên công ty');
		document.frmsend.company.focus();
		return false;
	}
	if(document.frmsend.name.value==''){
		alert('Nhập vào tên của bạn');
		document.frmsend.name.focus();
		return false;
	}
	if(document.frmsend.email.value==''){
		alert('Nhập vào địa chỉ email của bạn');
		document.frmsend.email.focus();
		return false;
	}
	if(document.frmsend.phone.value==''){
		alert('Nhập vào số điện thoại liên hệ của bạn');
		document.frmsend.phone.focus();
		return false;
	}
	
	document.getElementById('txt').style.display='block'; 
	document.frmsend.submit.style.display='none'
	return true;
}
</script>
</head>

<body>
<div class="primary">
  <div class="inner">
    <div class="logo">
        <a href="http://www.nsp.com.vn" target="_blank"><img src="images/logo-NSP.png" border="0" style="float:left" /></a> 
        <a href="http://acti.com" target="_blank"><img src="images/logo-ACTi.png" border="0" style="float:right" /></a>
    </div>
<br /><br />&nbsp;
<div>
	<img src="images/banner.jpg" border="0" alt="Trải nghiệm giải pháp Giám sát hình ảnh toàn diện" />
</div>
<?php
if(isset($_REQUEST['send']) ):
//include_once("send2.php");
?>
<div style="border-top:2px solid #0C0;border-bottom:2px solid #0C0; padding:5px; margin-top:5px; background:#CFF; font-size:15px; color:#0C0">Thông tin đăng ký của bạn đã gửi thành công.</div>
<?php endif?>
<div style="height:10px;">&nbsp;</div>
<div style="float:left; width:49%;">
    <h4 style="color:#bf1e2e;">NỘI DUNG CHƯƠNG TRÌNH</h4>
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
      <tr>
        <td width="34%" class="h">9:00 – 9:30</td>
        <td width="66%">Đón khách</td>
      </tr>
      <tr>
        <td class="h">9:30 – 10:40</td>
        <td>Giới thiệu các giải pháp giám sát hình ảnh mới</td>
      </tr>
      <tr>
        <td class="h">10:40 – 11:00</td>
        <td>Tiệc trà </td>
      </tr>
      <tr>
        <td class="h">11:00 – 11:40</td>
        <td>Giới thiệu công cụ hỗ trợ dự án &amp; chương trình khuyến mãi</td>
      </tr>
      <tr>
        <td class="h">11:45 – 12:00</td>
        <td>Rút thăm trúng thưởng</td>
      </tr>
      <tr>
        <td class="h">12:00 – 13:00</td>
        <td>Tiệc trưa</td>
      </tr>
    </table>
<p><img src="images/bottom-qua-tang.jpg" /></p>
</div>
<div style="float:right; width:49%; border:1px solid #CCC;">
<form action="send.php" method="post" name="frmsend" onsubmit="return checkfrm();">
<div style="background:#bf1e2e; padding:10px 0 10px 5px; font-size:11px; color:#FFF; text-align:center"><strong>Quý khách vui lòng điền đầy đủ thông tin dưới đây:</strong></div>
<p><i>&nbsp;Các trường có dấu (*) là bắt buộc nhập dữ liệu</i></p>
<table width="auto" border="0" cellspacing="0" cellpadding="5" class="frm">
  <tr>
    <td class="label">Tên công ty (*)</td>
    <td><input type="text" name="company" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Tên người tham dự (*)</td>
    <td><input type="text" name="name" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ email (*)</td>
    <td><input type="text" name="email" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Điện thoại liên hệ (*)</td>
    <td><input type="text" name="phone" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ</td>
    <td><input type="text" name="address" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Website</td>
    <td><input type="text" name="website" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">&nbsp;</td>
    <td><input type="submit" name="submit" value="&nbsp;" class="button" /> <p style="display:none;" id="txt">Đang xử lý, vui lòng chờ...</p></td>
  </tr>
</table>
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
<div style="clear:both;">&nbsp;</div>
</div>
</div>

</div>
</body>
</html>
