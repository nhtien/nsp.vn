<?php
require_once('lib/captcha.class.php');
require_once('lib/mysql.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Avocent - Đăng ký nhận eNewsletter</title>
<meta name="keywords" content="Avocent; Nhan Sinh Phuc" />
<meta name="description" content="Avocent - đăng ký nhận email hàng tháng, eNewsletter"  />
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-26358512-1']);
	_gaq.push(['_trackPageview']);
	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>

<style type="text/css">
.primary{
	width:650px;
	margin:0 auto;
	border:1px solid #999;
	padding:10px;
	background:#f2f2f2;
}
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	line-height:1.5;
}
table.frm td.label{
	text-align:right;
	padding-right:3px;
}
li{margin-bottom:7px;}
</style>
<script language="javascript">
function checkfrm(){
	if(document.frmsend.address.value==''){
		alert('Nhập vào địa chỉ công ty');
		document.frmsend.address.focus();
		return false;
	}
	if(document.frmsend.name.value==''){
		alert('Nhập vào tên của bạn');
		document.frmsend.name.focus();
		return false;
	}
	if(document.frmsend.email.value==''){
		alert('Nhập vào địa chỉ email của bạn');
		document.frmsend.email.focus();
		return false;
	}
	if(document.frmsend.phone.value==''){
		alert('Nhập vào số điện thoại liên hệ của bạn');
		document.frmsend.phone.focus();
		return false;
	}
	return true;
}
</script>

</head>

<body>
<div class="primary">
    <div class="logo">
    <img src="images/header.jpg" />
    </div>

<br style="clear:both" /><br />
<div style="background:#081738; padding:15px 0 15px 5px;">
  <h2 style="margin:0; text-align:center; font-size:18px; color:#FFF">ĐĂNG KÝ NHẬN BẢN TIN</h2>
<div style="clear:both; font-size:1px;"></div>
</div>
<?php
if($_REQUEST['send']):?>
<div style="border-top:2px solid #0C0;border-bottom:2px solid #0C0; padding:5px; margin-top:5px; background:#CFF; font-size:15px; color:#0C0">Thông tin đăng ký của bạn đã gửi thành công.</div>
<?php endif?>
<div style="height:10px;">&nbsp;</div>
<div style=" border:1px solid #CCC;">
<form action="send.php" method="post" name="frmsend" onsubmit="return checkfrm();">
<div style="background:#dcf4f6; padding:10px 0 10px 5px;"><strong>Quý khách vui lòng điền đầy đủ các thông tin sau đây:</strong></div>
<i>&nbsp;Các trường có dấu (*) là bắt buộc nhập dữ liệu</i>
<table width="auto" border="0" cellspacing="0" cellpadding="5" class="frm">
  <tr>
    <td class="label" nowrap="nowrap">Họ Tên (*)</td>
    <td><input type="text" name="name" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ email (*)</td>
    <td><input type="text" name="email" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Điện thoại liên hệ (*)</td>
    <td><input type="text" name="phone" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Tên công ty</td>
    <td><input type="text" name="company" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Website</td>
    <td><input type="text" name="website" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Mã bảo vệ (*)</td>
    <td><?php 
	$captcha = new NSPCaptcha();
	echo $captcha->htmlCaptcha();
	?>
	</td>
  </tr>
  <tr>
    <td class="label">&nbsp;</td>
    <td><input type="submit" name="submit" value="Đăng ký" class="button" /></td>
  </tr>
</table>
</form>
</div>
<div style="clear:both;">&nbsp;</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td style="padding:0.14in 0.14in 0.14in 0.14in" bgcolor="#868686">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        	  <tr>
        	    <td width="27%" valign="middle" align="left"><a href="http://nsp.com.vn"><img src="http://nsp.com.vn/logo/logo-132x45.png" border="0" width="132" height="45"></a></td>
        	    <td width="73%"><p style="font-size:9pt; color:#FFF">Công ty TNHH  TM-DV Tin học Nhân Sinh Phúc<br>
          Địa chỉ: 359  Võ Văn Tần, Phường 5, Quận 3, TP. Hồ Chí Minh<br>
          Điện thoại:  +84 8 3834 2108<br>
          Fax: +84 8  3834 2109</p></td>
      	    </tr>
      	  </table>
      </tr>
</table>
</div>

</div>
</body>
</html>
