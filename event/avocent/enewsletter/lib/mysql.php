<?php

class Database
{
	var $db_host          = "";
	
	var $db_name          = "";
	
	var $db_username      = "";
	
	var $db_password      = "";
	
	var $serverconn		  = 0;
	
	var $db_conn		  = 0;
	
	var $sql			  ='';
	
	function __construct( $options = null )
	{
		$this->db_mysql();
		$this->db_connect();
		$this->db_select();
	}
	
	function db_mysql()
	{
		$this->db_host     = 'localhost';
		$this->db_name     = 'nsp_db';
		$this->db_username = 'root';
		$this->db_password = '';
	}
	
	function db_connect()
	{
		$this->serverconn = mysql_connect($this->db_host,$this->db_username,$this->db_password, true);
		if(!$this->serverconn) die ("Could not connect to MySQL");
		mysql_query("SET NAMES 'utf8'"); 
	}
	
	function db_select()
	{
		$this->db_conn = mysql_select_db($this->db_name,$this->serverconn) or die("Could not connect to database");
	}
	
	function setQuery($sql)
	{
		global $config, $debug;
		$this->sql = $sql;
		if( $config->debug)
			$debug->_( $this->getQuery() );
	}
	function getQuery(){
		return $this->sql;
	}
	
	function loadObject($sql = null)
	{
		if(!$sql) $sql = $this->sql;
		$result=mysql_query($sql,$this->serverconn );
		if(!$result) die ("Could not execute SQL: ".$this->_showError( $sql) );
		$row = mysql_fetch_object( $result );
		return $row;
	}
	
	function loadObjectList($sql = '')
	{
		if($sql=='') $sql = $this->sql;
		if($sql)
		{
			$result = mysql_query($sql,$this->serverconn );
			if(!$result) die ("Could not execute SQL: ".$this->_showError( $sql) );
			$rows = array();
			while( $row = mysql_fetch_object($result) )
			{
				 $rows[] = $row;
			}
			return $rows;
		}
	}
	
	function loadResult($sql = '')
	{
		if($sql=='') $sql = $this->sql;
		$result=mysql_query($sql,$this->serverconn ); 
		if(!$result) die ("Could not execute SQL: ".$this->_showError( $sql) );
		$rows = array();
		$row = mysql_num_rows($result);
		return $row;
	}
	
	function query($sql = '')
	{
		if( $sql == '' ) $sql = $this->sql;
		$result = mysql_query($sql,$this->serverconn); 
		if ( !$result )
			die("Could not execute SQL: ". $this->_showError( $sql) );
		return true;

	}
	
	function quote( $text, $escaped = true )
	{
		return '\''.($escaped ? $this->escapeString( $text ) : $text).'\'';
	}
	
	function escapeString( $text )
	{
		return mysql_real_escape_string( $text );
	}
	
	function _showError( $string )
	{
		global $config;
		$error = '';
		//if( $config->debug)
		{
			$error =  mysql_error($this->serverconn)."\n<br /><br/>";
			$error .= $string;
		}
		
		return $error;
	}
	
	// Other Languages
	/*
	@field array field name
	@pre_tbl string table prefix EX: a.
	$sql_lang = Database::SQLOtherLang(array('title','description'), 'b.')
	*/
	
	function SQLOtherLang( $field , $pre_tbl = '' )
	{
		global $language, $config;
		
		$sql_lang = '';
		
		if( $config->site_default_lang != $language->lang )
		{
			foreach( $field as $l )
			{
				$field_name = $pre_tbl.$l.'_'. $config->lang ;
				
				$sql_lang[]= "CASE WHEN $field_name <> '' THEN $field_name ELSE $pre_tbl$l END '$l'"  ;
			}
		}
		else
		{
			foreach( $field as $l )
			{
				$sql_lang[] = $pre_tbl.$l ;
			}
		}
		
		$sql_lang = implode(",", $sql_lang);
		
		return $sql_lang;
	}
	
}
	

?>
