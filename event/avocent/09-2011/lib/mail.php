<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class Mail
{
	var $to 			= null;
	var $subject		= null;
	var $message 		= null;
	var $cc 			= null;
	var $bcc	 		= null;
	var $mailfrom		= null;
	var $fromname		= null;
	
	function __construct()
	{
		global $config;
		$this->mailfrom = $config->mailfrom;
		$this->fromname = $config->fromname;
	}
	
	function setSubject($string)
	{
		$this->subject = $string;
	}
	
	function setSender($mailfrom)
	{
		$this->mailfrom = $mailfrom;
	}
	
	function addRecipient($recipient)
	{
		if( is_array($recipient) )
		{
			$to = implode(",",$recipient);
		}
		else
		{
			$to = $recipient;
		}
		$this->to = $to;
	}
	
	function addCc( $cc )
	{
		$this->cc = $cc;
	}
	
	function addBcc( $bcc )
	{
		$this->bcc = $bcc;
	}
	
	function setBody($content)
	{
		$this->message = $content;
	}
	
	function setFromName( $name )
	{
		$this->fromname = $name;
	}
	
	function send()
	{
		$headers  = "MIME-Version: 1.0\r\n"; 
		$headers .= "Content-type: text/html; charset=utf-8\r\n"; 	
		$headers .= "From: $this->fromname  <".$this->mailfrom.">\r\n"; 
		if( $this->cc )
		{
			$headers .= 'Cc: ' . $this->cc . "\r\n";
		}
		
		if( $this->bcc )
		{
			$headers .= 'Bcc: ' . $this->bcc . "\r\n";
		}

		$result = mail($this->to,$this->subject, $this->message, $headers); 
		
		if( $result )
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
}

?>