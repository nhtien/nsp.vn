<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fluke Networks - Hội thảo Tương lai của phân tích mạng</title>
<style type="text/css">
.primary{
	width:850px;
	margin:0 auto;
	border:1px solid #999;
	padding:10px;
}
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	line-height:1.3;
	color:#666;
}
table.frm td.label{
	text-align:right;
	padding-right:3px;
}
table tr td{vertical-align:top; text-align:left;}
li{margin-bottom:7px;}
ul.list{margin:0 0 0 10px; padding-left:5px}
ul.list li{padding:7px 0 8px 5px; margin:0}
</style>
<script language="javascript">
function checkfrm(){
	if(document.frmsend.company.value==''){
		alert('Nhập vào tên công ty');
		document.frmsend.company.focus();
		return false;
	}
	if(document.frmsend.name.value==''){
		alert('Nhập vào tên của bạn');
		document.frmsend.name.focus();
		return false;
	}
	if(document.frmsend.email.value==''){
		alert('Nhập vào địa chỉ email của bạn');
		document.frmsend.email.focus();
		return false;
	}
	if(document.frmsend.phone.value==''){
		alert('Nhập vào số điện thoại liên hệ của bạn');
		document.frmsend.phone.focus();
		return false;
	}
	return true;
}
</script>
</head>

<body>
<div class="primary">
<div align="center" style="background:#000000;"><img src="images/Invitation-Fnet_02.png" /></div>
  <?php
if($_REQUEST['send']):?>
<div align="center" style="border-top:2px solid #175288;border-bottom:2px solid #175288; padding:5px; margin-top:5px; background:#175288; font-size:15px; color:#fdc300"><strong>Thông tin đăng ký của bạn đã gửi thành công. Chúng tôi sẽ liên lạc với bạn trong thời gian sớm nhất.</strong></div>
<?php endif?>

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="43%" valign="top"><img src="images/Thong-tin-tren-web_05.png" hspace="0" vspace="10" /></td>
      <td width="57%" style="padding-top:10px;">
      
        <table border="0" width="100%" cellpadding="0" cellspacing="0"><tr><td>
        <p align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:15pt"><b>HỘI THẢO<br>
        <font color="#175288">Tương lai của phân tích mạng<br>
      <font style="font-size:12pt">Tầm nhìn toàn diện trên hệ thống mạng<br />
      không dây và có dây</font></font></b>
      </p>
      </td><td style="vertical-align:middle"><img src="http://nsp.com.vn/email-marketing/fnet/06-2011/logo-as.jpg" width="104" height="98" align="right"></td></tr></table>

      
      
      <p style="font-family:Arial, Helvetica, sans-serif; font-size:10pt;" align="justify">Trong thế giới ngày càng phát triển hiện nay, chúng ta đang ở trong một môi trường mà các công nghệ luôn được thay đổi với tốc độ đáng kinh ngạc. Liệu vấn đề về hiệu suất mạng có &quot;<strong>nên</strong>&quot; được quan tâm đúng mức, các kỹ sư mạng có thể giải quyết các vấn đề một cách nhanh chóng?</p>
      <p style="font-family:Arial, Helvetica, sans-serif; font-size:10pt;" align="justify">Song song với việc mổ xẻ các thách thức mà nhà quản lý CNTT đang phải đối đầu, Fluke Networks cũng tiết lộ thông tin về sản phẩm chiến lược đang được công bố cùng lúc trên toàn thế giới: OptiView<sup>&reg;</sup> XG Network Analysis Tablet - Thiết bị dạng bảng của kỹ sư mạng với những tùy chỉnh chuyên dụng để tự động phân tích mạng và ứng dụng trong việc triển khai và xử lý sự cố của các công nghệ mới. Hỗ trợ các công nghệ 10GbE, ảo hóa, 802.11n và phân tích các ứng dụng trung tâm. Hệ thống hỗ trợ xử lý sự cố độc đáo của thiết bị dựa trên những phân tích chủ động, phân tích đường dẫn và cung cấp các hướng dẫn chuyên môn để xác định nguyên nhân sâu xa của vấn đề.</p>
          </td>
    </tr>
  </table>
  

      <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
          <td width="31%" valign="top">
          </td>
          <td width="69%">

          </td>
        </tr>
      </table><br>
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
          <td width="52%">
          <font color="#175288"><strong>Đến với buổi hội thảo, chúng tôi sẽ cùng bạn trao đổi các vấn đề:</strong></font>
          <ul style="font-family:Arial, Helvetica, sans-serif; font-size:10pt;" class="list">
            <li>Nhận diện hệ thống mạng thông qua các kết nối không dây và có dây.</li>
            <li>Xác định các kết nối chậm thông qua phân tích lộ trình ứng dụng.</li>
            <li>Phá vỡ rào cản phân tích mạng 10G</li>
            <li>Hiệu suất mạng không tốt. Do máy chủ hay ứng dụng hay mạng?</li>
            <li>Làm thế nào để xử lý các sự cố</li>
            <li>Phân tích các gói tin trở nên dễ dàng hơn.</li>
            <li>Phân tích mạng 802.11n đến tận gói tin và phổ Wi-Fi</li>
          </ul></td>
          <td width="48%" style="padding-left:30px;">
          <font color="#175288"><strong>Chương trình hội thảo:</strong></font>
          <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td width="12%">08:30</td>
              <td width="88%">Đón khách</td>
            </tr>
            <tr>
              <td>09:00</td>
              <td>Giới thiệu Optiview<sup>®</sup> XG - Tầm nhìn 			toàn diện trong hệ thống mạng không 			dây và có dây.</td>
            </tr>
            <tr>
              <td>09:30</td>
              <td>Nhận diện toàn bộ hệ thống thông 				qua kết nối mạng không dây và có dây</td>
            </tr>
            <tr>
              <td>09:45</td>
              <td>Theo dõi toàn diện hiệu suất hệ thống</td>
            </tr>
            <tr>
              <td>10:00</td>
              <td>Xác định kết nối chậm với tính năng phân 			tích lộ trình ứng dụng</td>
            </tr>
            <tr>
              <td>10:15</td>
              <td>Xử lý sự cố với các hướng dẫn chuyên môn</td>
            </tr>
            <tr>
              <td>10:30</td>
              <td>Tiệc trà</td>
            </tr>
            <tr>
              <td>10:45</td>
              <td>Phân tích gói tin dễ dàng với ClearSight 			Analyser</td>
            </tr>
            <tr>
              <td>11:15</td>
              <td>Network Time Machine - Ghi và lưu trữ các 			giao tiếp mạng để phân tích tính hợp lệ của 			thông tin</td>
            </tr>
            <tr>
              <td>11:35</td>
              <td>Phân tích mạng 802.11n đến tận các gói tin 			và phổ <br />
              Wi-Fi</td>
            </tr>
            <tr>
              <td>11:55</td>
              <td>Hỏi đáp</td>
            </tr>
            <tr>
              <td>12:00</td>
              <td>Kết thúc chương trình</td>
            </tr>
          </table></td>
        </tr>
      </table>
<div style="height:5px;">&nbsp;</div>
<form action="send.php" method="post" name="frmsend" onsubmit="return checkfrm();" style="padding:0; margin:0">

  <div style="border:1px solid #CCC;">
<div style="background:#DFDFDF; padding:10px 0 10px 5px;">
<strong>Đăng ký tham gia hội thảo ngay, bạn sẽ được tham gia chương  trình rút thăm trúng thưởng  thiết bị <font color="#175288">OptiView® XG Network  Analysis Tablet</font> (<font color="#175288">Model OPVXG-10G</font>) trị giá tương đương <font color="#175288">38.995 USD</font>. </strong><a href="index2.php" target="_blank"><em>Xem chi tiết tại đây</em></a>
</div>
<div style="float:left; width:48%; margin-top:10px; border-right:1px solid #CCC">
  <i>&nbsp;Các trường có dấu (*) là bắt buộc nhập dữ liệu</i>
  <table width="auto" border="0" cellspacing="0" cellpadding="5" class="frm">
  <tr>
    <td class="label">Tên công ty (*)</td>
    <td><input type="text" name="company" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Tên người tham dự (*)</td>
    <td><input type="text" name="name" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Địa điểm (*)</td>
    <td><select name="positon"><option value="TpHCM">Tp.Hồ Chí Minh</option><option value="Hanoi">Hà Nội</option></select></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ email (*)</td>
    <td><input type="text" name="email" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Điện thoại liên hệ (*)</td>
    <td><input type="text" name="phone" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Địa chỉ nhận thư mời (*)</td>
    <td><input type="text" name="address" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Website</td>
    <td><input type="text" name="website" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Sở thích</td>
    <td><input type="text" name="sothich" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">&nbsp;</td>
    <td><input type="submit" name="submit" value="Đăng ký" class="button" /></td>
  </tr>
</table>

</div>

<div style="float:right; width:45%; padding-top:10px;">
<font color="#175288"><strong>Hồ Chí Minh</strong></font><br>
            Thời gian: 8h30, ngày 13 tháng 7 năm 2011<br>
            Địa điểm:&nbsp;  Diamond Hall A, Tầng trệt, khách sạn <br />
        Sofitel Saigon Plaza, 17 Lê Duẩn, Quận 1, Tp. Hồ Chí Minh
        <p style="font-family:Arial, Helvetica, sans-serif; font-size:10pt;"><font color="#175288"><strong>Hà Nội</strong></font><br>
              Thời gian: 8h30, ngày 14 tháng 7 năm 2011<br>
              Địa điểm:  &nbsp;Phòng Lotus – Tầng trệt, Khách sạn Hà Nội Daewoo, <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;360 Kim Mã, Quận Ba Đình, Hà Nội</p>
<p style="font-family:Arial, Helvetica, sans-serif; font-size:10pt;"><font color="#175288"><strong>Người trình bày:</strong></font> <br>
            <strong>Ông Eric Lie</strong><br>
          Giám đốc kinh doanh khu vực Đông Nam Á</p>
        <p style="color:#175288;">&nbsp;</p>
</div>

</div>
</form><div style="clear:both; font-size:5px; height:7px;">&nbsp;</div>
<hr style="clear:both" />
<p style="font-family:Arial, Helvetica, sans-serif; font-size:8pt;"><em>Để biết thêm chi tiết, 
            xin vui lòng liên hệ:<br>
            Hồ Chí Minh: 
thangphan@nsp.com.vn hoặc 093 758 5016<br>
Hà Nội: thunguyen@nsp.com.vn hoặc 097 4346 000</em>
          </p>

</div>
</div>
</body>
</html>
