<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fluke Networks - Chương trình rút thăm trúng thưởng OptiView® XG World Tour</title>
<style type="text/css">
.primary{
	width:850px;
	margin:0 auto;
	border:1px solid #999;
	padding:10px;
}
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	line-height:1.3;
	color:#666;
}
table.frm td.label{
	text-align:right;
	padding-right:3px;
}
table tr td{vertical-align:top; text-align:left;}
li{margin-bottom:7px;}
ul.list{margin:0 0 0 10px; padding-left:5px}
ul.list li{padding:7px 0 8px 5px; margin:0}
</style>
<script language="javascript">
function checkfrm(){
	if(document.frmsend.company.value==''){
		alert('Nhập vào tên công ty');
		document.frmsend.company.focus();
		return false;
	}
	if(document.frmsend.name.value==''){
		alert('Nhập vào tên của bạn');
		document.frmsend.name.focus();
		return false;
	}
	if(document.frmsend.email.value==''){
		alert('Nhập vào địa chỉ email của bạn');
		document.frmsend.email.focus();
		return false;
	}
	if(document.frmsend.phone.value==''){
		alert('Nhập vào số điện thoại liên hệ của bạn');
		document.frmsend.phone.focus();
		return false;
	}
	return true;
}
</script>
</head>

<body>
<div class="primary">
<div align="center" style="background:#000000;"><img src="images/Invitation-Fnet_02.png" /></div>
<p align="center" style="font-size:15pt; color:#175288; font-weight:bold">Fluke Networks<br />
Chương trình rút thăm trúng thưởng  OptiView® XG World Tour<br />
<font style="font-size:12pt; color:#333">Nội quy chính thức</font></p>
<p>Bằng việc tham gia vào chương  trình bốc thăm trúng thưởng (sau đây gọi tắt là chương trình) này, bạn được hiểu  rằng nếu bạn là người<strong> trúng giải</strong> thì phần thưởng sẽ là của công ty mà bạn đang làm việc.</p>
<ol>
  <li>Mô tả chương trình:  Fluke Networks (sau đây gọi là nhà tổ chức) sẽ tặng một phần thưởng cho người  thắng cuộc bằng việc bốc thăm ngẫu nhiên để lựa chọn trong số các người đăng ký  tham dự chương trình. Chương trình bắt đầu từ lúc 12:00:01 AM Pacific Time (PT)  ngày 14/06/2011 và kết thúc vào cuối ngày 02/09/2011 (sau đây gọi là thời gian  hiệu lực của chương trình).</li>
  <li>Tư cách tham dự: Người  tham dự phải là các cá nhân trên 18 tuổi với tư cách là nhân viên phụ trách quản  trị, xử lý sự cố mạng, hiệu suất của các ứng dụng trên mạng và hiện đang là  cư dân hợp pháp tại Úc, Áo, Bỉ, Bun-ga-ri, Canada, Chi Lê, Trung Quốc, Cộng hòa  Séc, Đan Mạch, Estonia, Phần Lan, Pháp , Đức, Hy Lạp, Hồng Kông, Hung-ga-ri,  Ai-len, Ấn Độ, Is-ra-el, Ý, Nhật, Hàn Quốc, Lat-vi-a, Liechtenstein, Lithuania,  Luxembourg, Mal-ta, Mê-xi-cô, Hà Lan, Niu Di-lân, Na Uy, Ba Lan, Bồ Đào Nha,  Puerto Rico, Ru-ma-ni, Ả Rập Saudi, Singapore, Cộng hòa Sovakia, Slovenia, Tây  Ban Nha, Thụy Điển, Thụy Sĩ, Đài Loan, Thái Lan, Mỹ, Anh, Các tiểu vương quốc  A-rập thống nhất và Việt Nam. Nhân viên, người quản lý, hoặc người đại diện của  Fluke Networks và các thành viên liên quan hoặc họ hàng của họ, hay các đại lý  quảng cáo, xúc tiến thương mại có liên quan trong quá trình thiết kế, quảng bá  sản phẩm đều không được tham gia chương trình này.</li>
  <li>Cách tham dự chương  trình: Tham dự vào bất kỳ một buổi hội thảo giới thiệu sản phẩm OptiView XG  chính thức nào của hãng Fluke Networks trong thời gian hiệu lực của chương  trình. Việc hoàn thành phiếu khảo sát tại buổi hội thảo mà bạn tham gia sẽ được  coi là bạn đã tham dự chương trình. Chỉ giới hạn một phiếu cho một người. Để biết  thêm thông tin về thời gian tổ chức các buổi hội thảo, vui lòng xem thêm tại địa  chỉ sau: <a href="http://www.flukenetworks.com/xgtour" title="www.flukenetworks.com/xgtour">www.flukenetworks.com/xgtour</a></li>
  <li>Giá trị của phần thưởng:  Mỗi người thắng cuộc của chương trình sẽ được nhận một thiết bị phân tích mạng  dạng bảng OptiView XG (Model OPVXG-10G) có giá trị khoảng $38,995 USD. Số lượng  bốc thăm người thắng cuộc của chương trình phụ thuộc vào số lượng người tham dự  chương trình. Ví dụ, nếu số lượng người tham dự là 1.000.000 (1 triệu) người  thì số lượng người thắng cuộc sẽ là 1/1 triệu (một trên một triệu). </li>
  <li>Trao thưởng: Việc  rút thăm trúng thưởng sẽ được tổ chức vào ngày 15/09/2011. Người trúng thưởng sẽ  được liên hệ sau đó trong 5 (năm) ngày làm việc bằng điện thoại hay email để  yêu cầu hoàn thành một cách tự nguyện một bản khai có ký tên và gửi lại cho  Fluke Networks trong vòng 10 (mười) ngày. Người thắng cuộc còn được yêu cầu gửi  bản copy của hộ chiếu, bằng lái xe, giấy khai sinh hoặc các giấy tờ tùy thân  khác để chứng minh độ tuổi và tư cách công dân tại Việt Nam. Người trúng thưởng  còn phải chứng minh mình là nhân viên của một công ty và có giấy ủy quyền của  công ty đó đứng ra nhận phần thưởng cho công ty của mình. Trong trường hợp nhà  tổ chức không nhận được bản khai và các giấy tờ liên quan khác thì người trúng  thưởng sẽ bị loại và nhà tổ chức sẽ rút thăm lại để lựa chọn một người khác  thay thế. NGƯỜI  THAM DỰ PHẢI HÔI ĐỦ ĐIỀU KIÊN THAM DỰ VÀ CÁC THÔNG TIN TRONG BẢN KHAI PHẢI ĐƯỢC  BAN TỔ CHỨC XÁC NHẬN TRƯỚC KHI ĐƯỢC CÔNG NHẬN LÀ NGƯỜI THẮNG CUÔC</li>
  <li>Quy định chung:  Trong trường hợp không thể trao thưởng vì lí do bất khả kháng, nhà tổ chức có  quyền thay thế phần thưởng khác có giá trị tương đương hoặc lớn hơn cho người  thắng cuộc. Phần thưởng sẽ không được quy đổi ra tiền hoặc các thay thế khác.  Trong các trường hợp nằm ngoài khả năng kiểm soát của nhà tổ chức (bao gồm  nhưng không giới hạn việc thiết bị bị nhiễm virus), nhà tổ chức có quyền hủy bỏ  hay thay đổi chương trình mà không phải chịu bất kỳ một nghĩa vụ pháp lý nào với  người tham dự chương trình. Nhà tổ chức không chịu trách nhiệm cho những trường  hợp như thất lạc, trễ, sai địa chỉ, sai thông tin cá nhân. Các bản khai không đầy  đủ, không chính xác, giả mạo hoặc được tạo ra do các chương trình tự động thì  không được chấp nhận.<br />
    Tất cả người tham dự phải tuân thủ đầy đủ các quy định của chính phủ. Bất kỳ  người nào cố tình lừa gạt hoặc trong bằng bất kỳ hình thức nào làm xáo trộn  chương trình và không tuân thủ các quy định của chính phủ thì không đủ điều kiện  nhận giải thưởng và có thể bị truy tố trước pháp luật.<br />
    Khi tham gia chương trình, bạn cho phép Fluke Networks quyền, trừ trường hợp bị  pháp luật cấm, sử dụng tên, chân dung của bạn, hình ảnh, địa chỉ (thành phố và  tiểu bang), địa chỉ e-mail, điện thoại, thông tin tiểu sử, và chữ viết cho quảng  cáo và mục đích quảng cáo trong việc thúc đẩy hoặc công bố hình ảnh Fluke  Networks, các sản phẩm hoặc dịch vụ , mà không bồi thường, trừ khi luật pháp  yêu cầu. Người tham dự không có quyền phê duyệt, không được bồi thường, và không được yêu cầu bồi thường khi có phát sinh từ việc sử dụng, thay đổi, hoặc sử  dụng các thông tin tổng hợp của bạn như tên, hình ảnh, chân dung, địa chỉ  (thành phố và tiểu bang), địa chỉ thư điện tử, thông tin tiểu sử.<br />
    NGƯỜI TRÚNG GIẢI PHẢI CHỊU TẤT CẢ CÁC KHOẢN THUẾ PHÁT SINH NẾU CÓ KHI NHẬN GIẢI.</li>
  <li>Ban tổ chức không chịu  trách nhiệm về thiệt hại cho hệ thống máy tính của khách hàng (bao gồm nhưng không  giới hạn các hư hỏng của máy chủ hoặc bị mất, bị trì hoãn hoặc bị hỏng dữ  liệu hoặc sự cố khác) do trực tiếp hoặc gián tiếp khi khách hàng &nbsp;tham gia  vào Chương trình của Fluke hoặc tải về thông tin có liên quan đến chương trình.  Ban tổ chức có quyền sửa đổi hoặc hủy bỏ chương trình trong trường hợp trang  web được sử dụng để phục vụ cho chương trình rút thăm bị lỗi.Thông tin của  khách hàng tham dự được xử lý tại Hoa Kỳ và phù hợp với pháp luật riêng tư Mỹ.</li>
  <li>Thông tin về người  trúng giải: Gửi phong bì đến địa chỉ XG World Tour Sweepstakes Winner Information,  PO Box 12055, Scottsdale, AZ 85267-205. Yêu cầu phải được đóng dấu bưu điện chậm  nhất là 15/09/2011.</li>
  <li>Đơn vị tổ chức:  Fluke Networks, Everett, WA 98206-9090</li>
</ol>
<p>&nbsp;</p>
<p>Liên kết đến trang nguồn từ flukenetworks.com: <a href="http://www.flukenetworks.com/content/optiview%C2%AE-xg-world-tour-sweepstakes-rules">http://www.flukenetworks.com/content/optiview%C2%AE-xg-world-tour-sweepstakes-rules</a></p>
<hr style="clear:both" />

<p style="font-family:Arial, Helvetica, sans-serif; font-size:8pt;"><em>Để biết thêm chi tiết, 
            xin vui lòng liên hệ:<br>
            Hồ Chí Minh: 
thangphan@nsp.com.vn hoặc 093 758 5016<br>
Hà Nội: thunguyen@nsp.com.vn hoặc 097 4346 000</em>
          </p>

</div>
</div>
</body>
</html>
