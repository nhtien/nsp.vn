<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chương trình khuyến mãi FNET 2016</title>
<link type="text/css" href="css.css" rel="stylesheet" />
</head>

<body>
<div class="primary">
	<div class="item item-1">
    	<a href="http://goo.gl/p0AnsD" target="_blank"><img src="images/DSX-5000.png" alt="DTX-5000"></a>
        <div class="tooltip">
        <ul>
       		<li>Đo kiểm và chứng nhận hệ thống cáp đồng Cat. 6A và Class FA.</li>
        <li>Phân tích các kết quả đo và tạo ra các báo cáo chuyên nghiệp bằng phần mềm LinkWare™</li>
        <li>Hiển thị dưới dạng đồ họa các nguyên nhân gây ra lỗi như crosstalk, return loss và shield</li>
        </ul>

        </div>
    </div>
	<div class="item item-2">
    	<img src="images/multimode.png" alt="DTX-5000">
        <div class="tooltip">
        <ul>
<li>Kiểm tra chiều dài cáp quang Multimode/ Singlemode</li>
<li>Xác định các vị trí bị uốn cong quá mức, các mối hàn có độ suy hao lớn, các điểm đứt gãy…</li>
<li>Xác nhận khả năng kết nối toàn tuyến bằng cách phân tích kết nối quang</li>
<li>Xác định các nguồn của tỷ lệ bit-error gây ra bởi bề mặt đầu nối bị nhiễm bẩn hoặc kết nối kém</li>
</ul>
        </div>
    </div>
	<div class="item item-3">
    	<img src="images/apdung.png" alt="Thời gian áp dụng">
    </div>
</div>
<script language="javascript" src="jquery.min.js"></script>
<script language="javascript">
</script>
</body>
</html>
