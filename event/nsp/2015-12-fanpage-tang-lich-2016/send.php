<?php
define( '_EXEC', 1 );

define( 'DS', DIRECTORY_SEPARATOR );
error_reporting(E_ALL ^ E_NOTICE);
include('lib/mail.php');
include('lib/captcha.php');

$post = $_POST; 
$content = 'Có một khách hàng đăng ký khóa học ACTi:<br />';
$content .= 'Công ty: '.$post['company'].'<br />';
$content .= 'Tên: '.$post['name'].'<br />';
$content .= 'Email: '.$post['email'].'<br />';
$content .= 'Điện thoại: '.$post['phone'].'<br />';
$content .= 'Website: '.$post['website'].'<br />';
$content .= 'Địa chỉ: '.$post['address'].'<br />';

require_once('lib/phpxls/PHPExcel.php');
require_once('lib/phpxls/PHPExcel'.DS.'IOFactory.php');

$src_xls = dirname(__FILE__).DS.'DSKH.xls';

$objReader = new PHPExcel_Reader_Excel5();
$objPHPExcel = $objReader->load($src_xls);
$objPHPExcel->setActiveSheetIndex(0);

$index = 3;
$name = $objPHPExcel->getActiveSheet()->getCell('B'.$index)->getValue();
while($name){
	$index++;
	$name = $objPHPExcel->getActiveSheet()->getCell('B'.$index)->getValue();
	$email = $objPHPExcel->getActiveSheet()->getCell('C'.$index)->getValue(); 
}
$objPHPExcel->getActiveSheet()->setCellValue('B'.$index, $post['name']);
$objPHPExcel->getActiveSheet()->setCellValue('C'.$index, trim(strtolower($post['email'])));
$objPHPExcel->getActiveSheet()->setCellValue('D'.$index, "'".$post['phone']);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$index, $post['company']);
$objPHPExcel->getActiveSheet()->setCellValue('F'.$index, $post['position']);
$objPHPExcel->getActiveSheet()->setCellValue('G'.$index, $post['address']);
$objPHPExcel->getActiveSheet()->setCellValue('H'.$index, date("d/m/Y"));
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save($src_xls);

/*
$mail = new Mail();
$mail->setSubject('Dang ky Hoi thao ACTi');
$mail->addRecipient('sangtialia@gmail.com,sangtran@nsp.com.vn');
$mail->setBody($content);
$mail->send();
*/

// gửi email KH

$content2 = '
<table id="Table_01" width="648" height="963" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<img src="http://nsp.com.vn/email-marketing/ACTi/2013-8-Hoi-thao/thu-cam-on_01.jpg" width="648" height="113" alt=""></td>
	</tr>
	<tr>
		<td>
			<img src="http://nsp.com.vn/email-marketing/ACTi/2013-8-Hoi-thao/thu-cam-on_02.jpg" width="648" height="152" alt=""></td>
	</tr>
	<tr>
		<td height="150" bgcolor="#ebebeb" align="center"><p style="font-family:Arial, Helvetica, sans-serif; font-size:15pt; color:#58585a;"><strong>Kính gửi Anh/Chị: '.$post['name'].',</strong><br>
		  <br>
		  Công ty TNHH TM - DV Tin Học Nhân Sinh Phúc<br>
		  <br>
	    Xin gửi lời cảm ơn đến Quý Khách hàng đã đăng ký tham dự Hội thảo:</p></td>
	</tr>
	<tr>
		<td>
			<img src="http://nsp.com.vn/email-marketing/ACTi/2013-8-Hoi-thao/thu-cam-on_04.jpg" width="648" height="319" alt=""></td>
	</tr>
	<tr>
		<td align="center" bgcolor="#ebebeb" height="120">
			<p style="font-size:13pt; font-family:Arial, Helvetica, sans-serif; font-style:italic; color:#58585a;">Rất mong quý khách hàng đến tham dự đúng giờ để Hội thảo diễn ra tốt đẹp. <br>
              <br>
          Trân trọng,</p></td>
	</tr>
	<tr>
		<td>&nbsp;
			</td>
	</tr>
	<tr>
		<td>
			<img src="http://nsp.com.vn/email-marketing/ACTi/2013-8-Hoi-thao/thu-cam-on_07.jpg" width="648" height="116" alt=""></td>
	</tr>
</table>
';
/*
$mail2 = new Mail();
$mail2->setSubject('Thu cam on');
$mail2->addRecipient($post['email']);
$mail2->setBody($content2);
$mail2->send();
*/

//header("Location: ./index.php?send=1");
?>
<script language="javascript">
//alert("Ban da dang ky thanh cong!");
window.location.href='index.php?send=1';
</script>