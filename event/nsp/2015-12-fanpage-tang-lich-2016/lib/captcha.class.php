<?php
/*
form:
	require_once('classes/captcha.class.php');
	$captcha = new NSPCaptcha();
	echo $captcha->htmlCaptcha();
	
Progcess:
	// check captcha
	require_once('classes/captcha.class.php');
	$captcha = new NSPCaptcha();
	if( !$captcha->checkCaptcha(&$error) ){
		echo '<h3>'.$error.'</h3>';
	}else{
		....
	}
*/
class NSPCaptcha{
	
	var $key = null;
	var $__mycaptcha = null;

	function __construct(){
	}
	// ham de chen vao captcha
	function htmlCaptcha(){
		$this->setKey();
		$html = '<img src="captcha/?sess='.$this->setStrCaptcha().'" />&nbsp;';
		$html .='<input type="text" name="captcha" />';
		return $html;
	}
	
	// ham kiem  tra captcha nhap vao co dung hay ko
	// str captcha
	// str loi tra ve
	function checkCaptcha($error = '')
	{
		$submitStr = trim($_REQUEST['captcha']);
		if( strtolower($this->getKey()) <> strtolower($submitStr) ){
			$error = 'M&atilde; b&#7843;o v&#7879; kh&ocirc;ng tr&ugrave;ng kh&#7899;p, vui l&ograve;ng nh&#7853;p l&#7841;i';
			return false;
		}
		$_SESSION['__mycaptcha'] = '';
		return true;
	}
	
	function setKey()
	{
		$this->key = $this->getRandomKey();
		$_SESSION['__mycaptcha'] = $this->key;
		$this->__mycaptcha = $_SESSION['__mycaptcha'];
	}
	
	function getKey()
	{
		return $_SESSION['__mycaptcha'];
	}

	function getRandomKey()
	{
		$random = uniqid();
		$captcha = substr($random, count($random)-6, 5);
		return $captcha;
	}
	
	function setStrCaptcha( $strCaptcha = null )
	{
		if( !$strCaptcha ) $strCaptcha = $this->key;
		return base64_encode($strCaptcha);
	}
	
	
}
?>