<?php
require_once('lib/captcha.class.php');
require_once('lib/mysql.php');


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>NSP - Kỷ niệm 15 năm thành lập - gửi cảm nhận</title>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16757079-4']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<style type="text/css">
.primary{
	width:678px;
	margin:0 auto;
	border:1px solid #999;
	background:#eeeeee url(images/content-bg.jpg);
}
.primary > .inner{
	margin:15px;
}
.primary .logo{
	height:150px;
	padding:0 20px;
	text-align:right;
}
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	line-height:1.5;
}
table.frm{
	font-size:11px;
}
table.frm td.label{
	text-align:right;
	padding-right:3px;
}
li{margin-bottom:7px;}

table.frm .inputbox{
	width:180px;
}
td.h{
	font-weight:bold;
	color:#07539d;
	font-size:13px;
}
.button{
	background:url(images/bt.png) no-repeat;
	width:60px;
	height:43px;
	border:none;
	cursor:pointer;
}
.page{
	background:url(images/bongbong2.png) no-repeat;
}
</style>
<script language="javascript">
function checkfrm(){
	if(document.frmsend.company.value==''){
		alert('Nhập vào tên công ty');
		document.frmsend.company.focus();
		return false;
	}
	if(document.frmsend.name.value==''){
		alert('Nhập vào tên của bạn');
		document.frmsend.name.focus();
		return false;
	}
	if(document.frmsend.chucvu.value==''){
		alert('Nhập vào chức vụ của bạn');
		document.frmsend.chucvu.focus();
		return false;
	}
	if(document.frmsend.msg.value==''){
		alert('Nhập vào nội dung lời chúc');
		document.frmsend.msg.focus();
		return false;
	}
	
	document.getElementById('txt').style.display='block'; 
	document.frmsend.submit.style.display='none'
	return true;
}
</script>
</head>

<body>
<div class="primary">
	<div class="page">
  <div class="inner">
    <div class="logo">
        &nbsp;
    </div>
<?php
if(isset($_REQUEST['send']) ):
//include_once("send2.php");
?>
<div style="border-top:3px solid #0C0;border-bottom:3px solid #0C0; padding:10px; margin-top:5px; background:#CFF; font-size:15px; font-weight:bold; color:#0C0">Gửi thành công! Cảm ơn bạn đã gửi lời chúc đến NSP.</div>
<?php endif?>
<div style="height:10px;">&nbsp;</div>
<div style="float:left; width:49%;">

</div>
<div style="float:right; width:49%; border:1px solid #CCC; margin-right:10px;">
<form action="send2.php" method="post" name="frmsend" onsubmit="return checkfrm();">
<div style="background:#868686; padding:10px 0 10px 5px; font-size:11px; color:#FFF; text-align:center"><strong>Quý khách vui lòng điền đầy đủ thông tin dưới đây:</strong></div>
<p><i>&nbsp;Các trường có dấu (*) là bắt buộc nhập dữ liệu</i></p>
<table width="auto" border="0" cellspacing="0" cellpadding="5" class="frm">
  <tr>
    <td class="label">Tên công ty (*)</td>
    <td><input type="text" name="company" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Tên của bạn (*)</td>
    <td><input type="text" name="name" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Chức vụ (*)</td>
    <td><input type="text" name="chucvu" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ emai</td>
    <td><input type="text" name="email" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Điện thoại liên hệ</td>
    <td><input type="text" name="phone" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Nội dung<br />
lời chúc (*)</td>
    <td><textarea name="msg" class="inputbox" rows="3"></textarea></td>
  </tr>
  <tr>
    <td class="label">&nbsp;</td>
    <td><input type="submit" name="submit" value="&nbsp;" class="button" /> <p style="display:none;" id="txt">Đang xử lý, vui lòng chờ...</p></td>
  </tr>
</table>
</form>
<p>&nbsp;</p>
</div>
<div style="clear:both;">&nbsp;</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td style="padding:0.2in 0.14in 0.2in 0.14in" bgcolor="#868686">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        	  <tr>
        	    <td width="25%" valign="middle" align="left"><a href="http://nsp.com.vn"><img src="http://nsp.com.vn/logo/logo-132x45.png" border="0" width="132" height="45"></a></td>
        	    <td width="60%"><p style="font-size:9pt; color:#FFF">Công ty TNHH  TM-DV Tin học Nhân Sinh Phúc<br>
          Địa chỉ: 359  Võ Văn Tần, Phường 5, Quận 3, TP. Hồ Chí Minh<br>
          Điện thoại:  +84 8 3834 2108<br>
          Fax: +84 8  3834 2109</p></td>
          <td width="15%"><a href="http://www.nsp.com.vn" target="_blank"><img src="images/logo.jpg" width="60" height="75" border="0" style="" /></a> </td>
      	    </tr>
      	  </table>
      </tr>
</table>

</div>
</div>
</div>
</div>
</body>
</html>
