<?php
define( '_EXEC', 1 );

define( 'DS', DIRECTORY_SEPARATOR );
error_reporting(E_ALL ^ E_NOTICE);
include('lib/mail.php');
include('lib/captcha.php');

$post = $_POST; 

require_once('lib/phpxls/PHPExcel.php');
require_once('lib/phpxls/PHPExcel'.DS.'IOFactory.php');

$src_xls = dirname(__FILE__).DS.'DSKH2.xls';

$objReader = new PHPExcel_Reader_Excel5();
$objPHPExcel = $objReader->load($src_xls);
$objPHPExcel->setActiveSheetIndex(0);

$index = 3;
$name = $objPHPExcel->getActiveSheet()->getCell('B'.$index)->getValue();
while($name){
	$index++;
	$name = $objPHPExcel->getActiveSheet()->getCell('B'.$index)->getValue();
	$email = $objPHPExcel->getActiveSheet()->getCell('C'.$index)->getValue(); 
}
$objPHPExcel->getActiveSheet()->setCellValue('B'.$index, $post['name']);
$objPHPExcel->getActiveSheet()->setCellValue('C'.$index, trim(strtolower($post['email'])));
$objPHPExcel->getActiveSheet()->setCellValue('D'.$index, "'".$post['phone']);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$index, $post['company']);
$objPHPExcel->getActiveSheet()->setCellValue('F'.$index, $post['chucvu']);
$objPHPExcel->getActiveSheet()->setCellValue('G'.$index, $post['msg']);
$objPHPExcel->getActiveSheet()->setCellValue('H'.$index, date("d/m/Y"));
$objPHPExcel->getActiveSheet()->setCellValue('I'.$index, $post['mobilesv']);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save($src_xls);


header("Location: ./index.php?send=1");
?>
