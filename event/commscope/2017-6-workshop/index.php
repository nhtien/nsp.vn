<?php
require_once('lib/captcha.class.php');
require_once('lib/mysql.php');


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Đăng ký tham dự workshop</title>
<meta name="description" content="" />
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-16757079-4', 'nsp.com.vn');
  ga('send', 'pageview');

</script>
<style type="text/css">
.primary{
	width:648px;
	margin:0 auto;
	border:1px solid #999;
	background:#FFF;
}
.primary > .inner{
	margin:15px;
}
.primary .logo{
	margin:0 50px;
}
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	line-height:1.5;
}
table.frm{
	font-size:11px;
}
table.frm td.label{
	text-align:right;
	padding-right:3px;
}
li{margin-bottom:7px;}

table.frm .inputbox{
	width:150px;
}
td.h{
	font-weight:bold;
	color:#1193b7;
	font-size:12px;
}
.button{
	background:url(images/bt.png) no-repeat;
	width:90px;
	height:30px;
	border:none;
}
h4{
	font-size:13px;
}
</style>
<script language="javascript">
function checkfrm(){
	if(document.frmsend.name.value==''){
		alert('Nhập vào tên của bạn');
		document.frmsend.name.focus();
		return false;
	}
	if(document.frmsend.company.value==''){
		alert('Nhập vào tên công ty');
		document.frmsend.company.focus();
		return false;
	}
	if(document.frmsend.email.value==''){
		alert('Nhập vào địa chỉ email của bạn');
		document.frmsend.email.focus();
		return false;
	}
	if(document.frmsend.phone.value==''){
		alert('Nhập vào số điện thoại liên hệ của bạn');
		document.frmsend.phone.focus();
		return false;
	}
	
	document.getElementById('txt').style.display='block'; 
	document.frmsend.submit.style.display='none'
	return true;
}
</script>
</head>

<body>
<div class="primary">
  <div class="inner">
    <div class="logo">
        <a href="http://www.nsp.com.vn" target="_blank"><img src="images/logo-NSP.png" border="0" style="float:left" /></a> 
        <br />
    </div>
<br /><br />&nbsp;
<div>
</div>
<?php
if(isset($_REQUEST['send']) ):
//include_once("send2.php");
?>
<div style="border-top:2px solid #0C0;border-bottom:2px solid #0C0; padding:5px; margin-top:5px; background:#CFF; font-size:15px; color:#0C0">Thông tin đăng ký của bạn đã gửi thành công.</div>
<?php endif?>
<h1 align="center" style="text-transform:uppercase;">ĐĂNG KÝ THAM DỰ</h1>
<h3 align="center">WORKSHOP: DATA CENTER AND INTELLIGENT SOLUTION</h3>
<div style="height:10px;">&nbsp;</div>
<div style="float:left; width:49%;"><br />
  Thời gian: <strong>9:00AM - ngày 13/7/2017</strong><br />
Địa điểm: Liberty Central Saigon Centre Hotel, 179 Lê Thánh Tôn, Quận 1, TP.HCM<br />
<p><strong><u>Chi tiết chương trình:</u></strong></p>
  <table border="0" cellspacing="0" cellpadding="5" width="100%">
    <tr>
      <td width="120" valign="top">8.00am - 9.00am</td>
      <td width="170" valign="top">Registration </td>
    </tr>
    <tr>
      <td width="120" valign="top">9.00am - 9.15am</td>
      <td width="170" valign="top">Welcome Speech</td>
    </tr>
    <tr>
      <td width="120" valign="top">9.15am - 9.30am</td>
      <td width="170" valign="top">Introduction - NSP</td>
    </tr>
    <tr>
      <td width="120" valign="top">9.30am - 10.00am</td>
      <td width="170" valign="top">High Speed Migration -    Connecting Where You Are Today to Where You Are Going Tomorrow</td>
    </tr>
    <tr>
      <td width="120" valign="top">10.00am - 10.20am</td>
      <td width="170" valign="top">Coffee Break</td>
    </tr>
    <tr>
      <td width="120" valign="top">10.20am - 10.50am</td>
      <td width="170" valign="top">imVision Solution</td>
    </tr>
    <tr>
      <td width="120" valign="top">10.50am - 11.30am</td>
      <td width="170" valign="top">Q&amp;A &amp; closing remarks </td>
    </tr>
    <tr>
      <td width="120" valign="top">11.30am – 13.00pm</td>
      <td width="170" valign="top">Lunch</td>
    </tr>
  </table>
</div>
<?php if(!isset($_REQUEST['send']) ):?>
<div style="float:right; width:49%; border:1px solid #CCC;">
<form action="send.php" method="post" name="frmsend" onsubmit="return checkfrm();">
<div style="background:#c22431; padding:10px 0 10px 5px; font-size:11px; color:#FFF; text-align:center"><strong>Quý khách vui lòng điền đầy đủ thông tin dưới đây:</strong></div>
<p><i>&nbsp;Các trường có dấu (*) là bắt buộc nhập dữ liệu</i></p>
<table width="auto" border="0" cellspacing="0" cellpadding="5" class="frm">
  <tr>
    <td class="label" nowrap="nowrap">Tên người đăng ký (*)</td>
    <td><input type="text" name="name" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Tên công ty (*)</td>
    <td><input type="text" name="company" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Chức vụ</td>
    <td><input type="text" name="position" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ email (*)</td>
    <td><input type="text" name="email" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Điện thoại liên hệ (*)</td>
    <td><input type="text" name="phone" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ</td>
    <td><input type="text" name="address" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">&nbsp;</td>
    <td><input type="submit" name="submit" value="&nbsp;" class="button" /> <p style="display:none;" id="txt">Đang xử lý, vui lòng chờ...</p></td>
  </tr>
</table>
</form>

<p>&nbsp;</p>
</div>
<?php endif;?>

<div style="clear:both;">&nbsp;</div>
</div>
</div>

</div>
</body>
</html>
