<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PT-E550W</title>
<link rel="stylesheet" href="css.css" />
</head>
<?php
global $page;
$page = 'PT-E550W';
?>
<body>
<div class="main">
<?php include("includes/top.php");?>
<?php include("includes/nav.php");?>    
<div class="content navbar PT-E550W">
    	<div class="navbar-inner">
        <br class="break" /><br /><br /><br />
        	<div class="img"><img src="images/PT-E550W.png" alt="PT-E550W" /></div>
            <div class="info">
            	<h1>PT-E550W</h1>
                • Khổ rộng nhãn in từ 6-24 mm<br />
                • Màn hình LCD lớn<br />
                • Tích hợp 384 biểu tượng<br />
                • Chế độ cắt nhãn tự động<br />
                • Chức năng thiết kế nhãn chuyên dụng<br />
                • In nhãn từ dữ liệu sẵn có<br /><br />
                <p><a href="target="_blank" href="http://www.mediafire.com/view/v1qdrrv61yllnl2/Catalogue_2014-09.pdf""><img src="images/download.png" align="Download tài liệu" class="imglink" border="0" /></a></p>
            </div>
            <br class="break" /><br /><br />
        </div>
    </div><!-- content -->
    <?php include("includes/bottom.php");?>
    <?php include("includes/footer.php");?>
</div>
</body>
</html>
