<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
global $page;
$page = 'case';
$t = @$_REQUEST['t'];
if(!$t) $t = 'VNPT';

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Case study <?php echo $t?></title>
<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" href="css.css" />
<script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
		});
</script>
</head>
<body>
<div class="main">
<?php include("includes/top.php");?>
<div class="nav navbar">
      <div class="navbar-inner">
    	<a href="case.php?t=VNPT"  class="l1<?php if($t == 'VNPT') echo ' current'?>">VNPT</a>
        <a href="case.php?t=VENTECH"<?php if($t == 'VENTECH') echo ' class="current"'?>>VENTECH</a>
        <a href="case.php?t=NETCOM"<?php if($t == 'NETCOM') echo ' class="current"'?>>NET&COM</a>
      </div>
</div><!-- nav -->
<div class="content navbar image-content">
    	<div class="navbar-inner">
       		<br class="break" />
            <?php include("includes/case.$t.php");?>
            <br class="break" />
        </div>
    </div><!-- content -->
    <?php include("includes/bottom.php");?>
    <?php include("includes/footer.php");?>
</div>
</body>
</html>
