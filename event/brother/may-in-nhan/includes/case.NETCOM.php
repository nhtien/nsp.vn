<div style="float:left" class="list-image-left">
	<p align="center" class="typical"><img class="imglink" src="images/case7.PNG"></p>
    <div>
    	<ul>
            <li><a class="fancybox" href="images/case7.PNG"><img width="165" height="165" class="imglink" src="images/case7.PNG"></a></li>
            <li><a class="fancybox" href="images/case8.PNG"><img width="165" height="165" class="imglink" src="images/case8.PNG"></a></li>
            <li><a class="fancybox" href="images/case9.PNG"><img width="165" height="165" class="imglink" src="images/case9.PNG"></a></li>
            <li><a class="fancybox" href="images/case10.PNG"><img width="165" height="165" class="imglink" src="images/case10.PNG"></a></li>
            <li><a class="fancybox" href="images/case11.PNG"><img width="165" height="165" class="imglink" src="images/case11.PNG"></a></li>
            <li><a class="fancybox" href="images/case12.PNG"><img width="165" height="165" class="imglink" src="images/case12.PNG"></a></li>
        </ul>
    </div>
</div>
<div style="float:right; width:500px;" class="body-right">
    <p class="title-categories">Công Ty Cổ Phần Tích Hợp Truyền Thông Net&Com</p>
    <div>
        <ol>
            <li>Địa chỉ: 17 Đào Duy Anh, Đà Nẵng </li>
            <li>Máy đang sử dụng: PT-E100VP; PT-E300VP</li>
            <li>Website: netcomvn.com </li>
            <li>Nhãn sử dụng: Siêu dẻo</li>
            <li>Lĩnh vực: Cáp mạng viễn thông
</li>
            <li>Ứng dụng: Đánh dấu cáp server Vòng Xoay  Sun 
Wheel Đà Nẵng</li>
            <li>Thời gian sử dụng</li>
        </ol>
    </div>
    <p class="title-categories">Lý do sử dụng máy in nhãn của chúng tôi</p>
    <div>
    	<ol>
            <li>Chất lượng nhãn bền, phù hợp thực tế</li>
            <li>Đa dạng, chủ động trong thiết kế nội dung</li>
            <li>Độ chuyên nghiệp cao</li>
        </ol>
        
    </div>
    <p class="title-categories">Nhận xét của khách hàng</p>
    <div>
    	<ol>
            <li>“Nhãn có độ bền lâu. Thiết bị dễ sử dụng, in nhãn nhanh.  Giúp anh em kỹ thuật tiết kiệm thời gian, làm việc hiệu quả hơn.” (Mr Khoa - Trưởng Phòng Kỹ Thuật)</li> 
        </ol>
    </div>
    
</div>