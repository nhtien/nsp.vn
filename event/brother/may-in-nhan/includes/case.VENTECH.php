<div style="float:left" class="list-image-left">
	<p align="center" class="typical"><img class="imglink" src="images/case2.PNG"></p>
    <div>
    	<ul>
            <li><a class="fancybox" href="images/case1.PNG"><img width="165" height="165" class="imglink" src="images/case1.PNG"></a></li>
            <li><a class="fancybox" href="images/case2.PNG"><img width="165" height="165" class="imglink" src="images/case2.PNG"></a></li>
            <li><a class="fancybox" href="images/case3.PNG"><img width="165" height="165" class="imglink" src="images/case3.PNG"></a></li>
            <li><a class="fancybox" href="images/case4.PNG"><img width="165" height="165" class="imglink" src="images/case4.PNG"></a></li>
        </ul>
    </div>
</div>
<div style="float:right; width:500px;"  class="body-right">
    <p class="title-categories">Công ty TNHH Công Nghệ Mạng Sao Kim (Ventech)</p>
    <div>
        <ol>
            <li>Địa chỉ: Tầng 6, toàn nhà PL, 506 Nguyễn Đình Chiểu, 
P. 4, Q. 3, Tp. Hồ Chí Minh</li>
            <li>Máy đang sử dụng: PT-7600,  PT-9700PC</li>
            <li>Website: http://www.ventech.com.vn/</li>
            <li>Nhãn sử dụng: TZe Siêu dẻo</li>
            <li>Lĩnh vực: Thi công hạ tầng mạng</li>
            <li>Ứng dụng: Dán nhãn cho cáp và thiết bị viễn 
            thông</li>
            <li>Thời gian sử dụng: Năm 2012</li>
        </ol>
    </div>
    <p class="title-categories">Lý do sử dụng máy in nhãn của chúng tôi</p>
    <div>
    	<ol>
            <li>Chất lượng nhãn tốt, độ bền cao và có màng bảo vệ</li>
            <li>Máy in nhãn có công suất tốt, độ bền cao, có chế độ cắt tự động, half -cut tiện lợi</li>
            <li>Khách hàng của Ventech yêu cầu sử dụng nhãn Brother</li>
            <li>Brother là thương hiệu của Nhật Bản – yên tâm về dộ bền và chất lượng</li>
        </ol>
        
    </div>
    <p class="title-categories">Nhận xét của khách hàng</p>
    <div>
    	<ol>
            <li>“Khi thi công hạ tầng mạng, nhiều khách hàng của chúng tôi chỉ định phải sử dụng nhãn in Brother để đánh 
dấu cáp và thiết bị.” (Ông Nguyễn Minh Quốc – Vice Director)</li> 
        </ol>
    </div>
    
</div>