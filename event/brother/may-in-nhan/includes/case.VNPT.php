<div style="float:left" class="list-image-left">
    <div>
    	<p align="center" class="typical"><img  class="imglink" src="images/case5.PNG"></p>
    	<ul>
            <li><a class="fancybox" href="images/case5.PNG"><img width="165" height="165" class="imglink" src="images/case5.PNG"></a></li>
            <li><a class="fancybox" href="images/case6.PNG"><img width="165" height="165" class="imglink" src="images/case6.PNG"></a></li>
            
        </ul>
    </div>
</div>
<div style="float:right; width:500px;" class="body-right">
    <p class="title-categories">VNPT BIÊN HÒA ĐỒNG NAI</p>
    <div>
        <ol>
            <li>Địa chỉ: Số 1 Đường 30/4, Phường Trung Dũng, 
Thành Phố Biên Hòa, Tỉnh Đồng Nai</li>
            <li>Máy đang sử dụng: PT- E100</li>
            <li>Website: http://www.vnptdongnai.vn/</li>
            <li>Nhãn sử dụng: Tze tiêu chuẩn và siêu dẻo</li>
            <li>Lĩnh vực: Viễn thông </li>
            <li>Ứng dụng: Đánh dấu thông tin trên cáp quang</li>
            <li>Thời gian sử dụng</li>
        </ol>
    </div>
    <p class="title-categories">Lý do sử dụng máy in nhãn của chúng tôi</p>
    <div>
    	<ol>
            <li>Chất lượng nhãn in bền</li>
            <li>Đa dạng trong việc thiết kế kiểu nhãn, nội dung nhãn</li>
            <li>Nhanh gọn, tiện sử dụng và chuyên nghiêp</li>
        </ol>
        
    </div>
    <p class="title-categories">Nhận xét của khách hàng</p>
    <div>
    	<ol><li>“Nhãn in của Brother có độ bền rất tốt, giúp các anh em kỹ thuật nhanh chóng chủ động thiết kế nhiều kiểu nhãn in.” (Mr. Vui - Trưởng Phòng Mạng và Dịch Vụ)</li> 
        </ol>
    </div>
    
</div>