<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PT-P750W</title>
<link rel="stylesheet" href="css.css" />
</head>
<?php
global $page;
$page = 'PT-P750W';
?>
<body>
<div class="main">
<?php include("includes/top.php");?>
<?php include("includes/nav.php");?>    
<div class="content navbar PT-P750W">
    	<div class="navbar-inner">
        <br class="break" /><br /><br /><br />
        	<div class="img" align="center"><img src="images/PT-P750W.png" alt="PT-P750W" /></div>
            <div class="info">
            	<h1>PT-E550W</h1>
                • Khổ rộng nhãn in từ 6 - 24mm<br />
                • Ứng dụng thiết kế nhãn iPrint&Label miễn phí dành cho hệ điều hành iOS và Android<br />
                • Cắt nhãn tự động với chế độ "cắt một nửa" giúp dễ tháo nhãn<br />
                • Tích hợp Wi-F<br />
                • Cho phép chọn nhiều kiểu chữ<br />
                <p><a target="_blank" href="http://www.mediafire.com/view/v1qdrrv61yllnl2/Catalogue_2014-09.pdf"><img src="images/download.png" align="Download tài liệu" class="imglink" border="0" /></a></p>
            </div>
            <br class="break" /><br /><br />
        </div>
    </div><!-- content -->
    <?php include("includes/bottom.php");?>
    <?php include("includes/footer.php");?>
</div>
</body>
</html>
