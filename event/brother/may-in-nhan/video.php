<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
global $page;
$page = 'video';
$t = @$_REQUEST['t'];
if(!$t) $t = 'PT-E550W';

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Video <?php echo $t?></title>
<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" href="css.css" />
<script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
		});
</script>
</head>
<body>
<div class="main">
<?php include("includes/top.php");?>
<div class="nav navbar">
      <div class="navbar-inner">
    	<a href="video.php?t=PT-P750W"  class="l<?php if($t == 'PT-P750W') echo ' current'?>">PT-P750W</a>
        <a href="video.php?t=PT-E550W"<?php if($t == 'PT-E550W') echo ' class="current"'?>>PT-E550W</a>
      </div>
</div><!-- nav -->
<div class="content navbar image-content">
    	<div class="navbar-inner">
       		<br class="break" />
            <div style="float:left"><p align="center"><?php include("includes/video.$t.php");?></p></div>
            <div style="float:right; margin-right:100px;">
                <p style="font-weight:bold; font-size:16px;">Những video khác</p>
                <div>
                    <p>-&nbsp;&nbsp;&nbsp;<a target="_blank" href="https://www.youtube.com/watch?v=oON1C8jy4VQ ">Tổng quan về máy in nhãn</a></p>
                    <p>-&nbsp;&nbsp;&nbsp;<a target="_blank" href="https://www.youtube.com/watch?v=su2_3oy8-bA ">Mobile Transfer Express</a></p>
                    <p>-&nbsp;&nbsp;&nbsp;<a target="_blank" href="https://www.youtube.com/watch?v=HVdgB4V-SVA ">Phần mềm P-touch Editor 5.1</a></p>
                    <p>-&nbsp;&nbsp;&nbsp;<a target="_blank" href="https://www.youtube.com/watch?v=OrFfr1yewII ">Về Chất lượng nhãn in</a></p>
                    <br class="break" />
                </div>
            </div>
            <br class="break" />
        </div>
    </div><!-- content -->
    <?php include("includes/bottom.php");?>
    <?php include("includes/footer.php");?>
</div>
</body>
</html>
