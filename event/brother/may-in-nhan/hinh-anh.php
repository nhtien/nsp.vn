<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
global $page;
$page = 'image';
$folder = @$_REQUEST['t'];
if(!$folder) $folder = 'PT-E550W';
$path = dirname(__FILE__).'/hinh-anh/'.$folder; 
$images = array();
$handle = opendir($path);
while (($file = readdir($handle)) !== false)
{
	if ( ($file != '.') && ($file != '..') ) 
	{
		if(!is_dir($path.'/'.$file)) $images[] = $folder.'/'.$file;
	}
}
closedir($handle);

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hình ảnh <?php echo $folder?></title>
<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" href="css.css" />
<script type="text/javascript" src="js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
		});
</script>
</head>
<body>
<div class="main">
<?php include("includes/top.php");?>
<div class="nav navbar">
      <div class="navbar-inner">
    	<a href="hinh-anh.php?t=PT-P750W"  class="l<?php if($folder == 'PT-P750W') echo ' current'?>">PT-P750W</a>
        <a href="hinh-anh.php?t=PT-E550W"<?php if($folder == 'PT-E550W') echo ' class="current"'?>>PT-E550W</a>
      </div>
</div><!-- nav -->
<div class="content navbar image-content">
    	<div class="navbar-inner">
       		<br class="break" />
            	<ul>
            	<?php foreach($images as $i){?>
                	<li><a href="hinh-anh/<?php echo $i?>" class="fancybox"><img src="hinh-anh/<?php echo $i?>" width="165" height="165" class="imglink" /></a></li>
                <?php } ?>
                </ul>
            <br class="break" />
        </div>
    </div><!-- content -->
    <?php include("includes/bottom.php");?>
    <?php include("includes/footer.php");?>
</div>
</body>
</html>
