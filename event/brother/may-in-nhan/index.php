<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Brother PT-E550W - PT-P750W</title>
<link rel="stylesheet" href="css.css" />
</head>
<?php
global $page;
$page = 'home';
?>
<body>
<div class="main homepage">
<?php include("includes/tophome.php");?>
<div class="content navbar">
    	<div class="navbar-inner">
        	<div class="prod l">
            	<h1>PT-P750W</h1>
                <a href="PT-P750W.php" title="PT-P750W"><img src="images/PT-P750W-h.png" alt="PT-P750W" border="0" class="imglink" width="342" height="470" /></a>
            </div>
        	<div class="prod">
            	<h1>PT-E550W</h1>
                <a href="PT-E550W.php" title="PT-E550W"><img src="images/PT-E550W-h.png" alt="PT-E550W" border="0" class="imglink" width="357" height="456" /></a>
            </div>
            <br class="break" /><br />
        </div>
    </div><!-- content -->
    <?php include("includes/bottom.php");?>
    <?php include("includes/footer.php");?>
</div>
</body>
</html>
