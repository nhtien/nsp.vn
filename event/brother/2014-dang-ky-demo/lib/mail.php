<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

require_once(dirname(__FILE__).DS.'phpmailer'.DS.'class.phpmailer.php');
require_once(dirname(__FILE__).DS.'phpmailer'.DS.'class.smtp.php');

class Mail
{
	var $to 			= null;
	var $subject		= null;
	var $message 		= null;
	var $cc 			= null;
	var $bcc	 		= null;
	var $mailfrom		= null;
	var $fromname		= null;
	
	function __construct()
	{
		global $config;
		$this->mailfrom = $config->mailfrom;
		$this->fromname = $config->fromname;
	}
	
	function setSubject($string)
	{
		$this->subject = $string;
	}
	
	function setSender($mailfrom)
	{
		$this->mailfrom = $mailfrom;
	}
	
	function addRecipient($recipient)
	{
		if( is_array($recipient) )
		{
			$to = implode(",",$recipient);
		}
		else
		{
			$to = $recipient;
		}
		$this->to = $to;
	}
	
	function addCc( $cc )
	{
		$this->cc = $cc;
	}
	
	function addBcc( $bcc )
	{
		$this->bcc = $bcc;
	}
	
	function setBody($content)
	{
		$this->message = $content;
	}
	
	function setFromName( $name )
	{
		$this->fromname = $name;
	}
	
	function send()
	{
		$mail         	= new PHPMailer();
		$mail->IsSMTP();
		$mail->CharSet	= "utf-8";
		
		$body             = $this->message;
		$body             = eregi_replace("[\]",'',$body);
		
		
		$mail->SMTPAuth   = true;  
		$mail->SMTPSecure = ''; 
		$mail->Host       = 'mail.nsp.com.vn';
		$mail->Port       = 25;
		
		$mail->Username   = 'webmaster@nsp.com.vn';
		$mail->Password   = 'nsp12345678';
		
		$mail->From       = 'webmaster@nsp.com.vn';
		$mail->FromName   = 'NSP';
		$mail->Subject    = $this->subject;
		$mail->AltBody    = "";
		$mail->WordWrap   = 50; // set word wrap
		
		$mail->MsgHTML($body);
		
		$mail->AddReplyTo($this->mailfrom, $this->fromname);
		
		// ATTTACHMENT
		if($this->attachment)
		{
			foreach($this->attachment as $file)
				$mail->AddAttachment($file); 
		}
		
		// TO
		$recipient = explode(',',$this->to);
		foreach($recipient as $to)
		{
			$mail->AddAddress(trim($to),'');
		}
		
		// CC
		if($this->cc)
		{
			$cc = explode(',',$this->cc);
			foreach($cc as $c)
			{
				$mail->AddCC(trim($c),'');
			}
		}
		
		// BCC
		if($this->bcc)
		{
			$bcc = explode(',',$this->bcc);
			foreach($bcc as $c)
			{
				$mail->AddBCC(trim($c),'');
			}
		}
		
		$mail->IsHTML(true); // send as HTML
		//var_dump($mail->send());exit;
		if(!$mail->Send())
		{
		  echo 'Gửi email không thành công!';
		  return false;
		}
		return true;
		
	}
}

?>