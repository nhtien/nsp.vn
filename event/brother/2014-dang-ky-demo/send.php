<?php
define( '_EXEC', 1 );

define( 'DS', DIRECTORY_SEPARATOR );
error_reporting(E_ALL ^ E_NOTICE);
include('lib/mail.php');
include('lib/captcha.php');

$post = $_POST; 
$content = 'Có một khách hàng đăng ký demo sản phẩm Brother:<br />';
$content .= 'Công ty: '.$post['company'].'<br />';
$content .= 'Tên: '.$post['name'].'<br />';
$content .= 'Email: '.$post['email'].'<br />';
$content .= 'Điện thoại: '.$post['phone'].'<br />';
$content .= 'Website: '.$post['website'].'<br />';
$content .= 'Địa chỉ: '.$post['address'].'<br />';

require_once('lib/phpxls/PHPExcel.php');
require_once('lib/phpxls/PHPExcel'.DS.'IOFactory.php');

$src_xls = dirname(__FILE__).DS.'DSKH-brodther-demo.xls';

$objReader = new PHPExcel_Reader_Excel5();
$objPHPExcel = $objReader->load($src_xls);
$objPHPExcel->setActiveSheetIndex(0);

$index = 3;
$name = $objPHPExcel->getActiveSheet()->getCell('B'.$index)->getValue();
while($name){
	$index++;
	$name = $objPHPExcel->getActiveSheet()->getCell('B'.$index)->getValue();
	$email = $objPHPExcel->getActiveSheet()->getCell('C'.$index)->getValue(); 
}
$objPHPExcel->getActiveSheet()->setCellValue('B'.$index, $post['name']);
$objPHPExcel->getActiveSheet()->setCellValue('C'.$index, trim(strtolower($post['email'])));
$objPHPExcel->getActiveSheet()->setCellValue('D'.$index, "'".$post['phone']);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$index, $post['company']);
$objPHPExcel->getActiveSheet()->setCellValue('F'.$index, $post['address']);
$objPHPExcel->getActiveSheet()->setCellValue('G'.$index, date("d/m/Y"));
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save($src_xls);


$mail = new Mail();
$mail->setSubject('Dang ky demo SP Brother');
$mail->addRecipient('tinbui@nsp.com.vn,sangtran@nsp.com.vn');
$mail->setBody($content);
$mail->send();


// gửi email KH
/*
$content2 = '<p>Kính gửi Ông/Bà: <strong>'.$post['name'].'</strong><br />
Công ty: <strong>'. $post['company'].'</strong></p>
<p>Cảm ơn quý Khách hàng đã đăng ký tham dự buổi hội  thảo <strong>&ldquo;Trải nghiệm công nghệ đánh nhãn cầm tay thế hệ mới&rdquo;</strong>.</p>
<p><strong>Thông tin chi tiết về thời gian và địa điểm:</strong><br />
  <strong>Thời gian</strong>: 8:30 - 12:00<br />
  <strong>Địa điểm</strong>: Khách sạn Pullman Sài Gòn - 148 Trần  Hưng Đạo, Quận 1, Tp. HCM</p>
<p>Rất mong quý Khách hàng đến tham dự đúng giờ để buổi  hội thảo diễn ra tốt đẹp.</p>
<p>Trân trọng,</p>
<!-- NHAN SINH PHUC -->
    <div style="font-family: Arial, Helvetica, sans-serif; font-size: 9pt; line-height: 1.5;">
  <p style="margin-top: 12pt;"><strong>NHAN SINH PHUC CO., LTD.</strong><br />359 Vo Van Tan St., Dist. 3, HCMC, Vietnam<br />
    Tel: (84-8) 3834 2108<br />
    Fax: (84-8) 3834 2109<br /><a href="http://www.nsp.com.vn"><font face="Arial, Helvetica, sans-serif">www.nsp.com.vn</font></a> <br /><br /><a href="http://nsp.com.vn" style="border: 0;"><img src="http://www.nsp.com.vn/logo/logo NSP.jpg" alt="logo" style="border: 0;" width="125" height="43" /></a></p>
</div>

';
$mail2 = new Mail();
$mail2->setSubject('Đăng ký tham dự hội thảo');
$mail2->addRecipient($post['email']);
$mail2->setBody($content2);
$mail2->send();
*/

//header("Location: ./index.php?send=1");
?>
<script language="javascript">
alert("Ban da dang ky thanh cong!");
window.location.href='index.php?send=1';
</script>