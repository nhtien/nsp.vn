<?php
require_once('lib/captcha.class.php');
require_once('lib/mysql.php');


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Đăng ký demo sản phẩm Brother</title>
<style type="text/css">
.primary{
	width:678px;
	margin:0 auto;
	border:1px solid #999;
	background:#eeeeee;
}
.primary > .inner{
	margin:15px;
}
.primary .logo{
	margin:0 50px;
}
body{
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	line-height:1.5;
}
table.frm{
	font-size:11px;
}
table.frm td.label{
	text-align:right;
	padding-right:3px;
}
li{margin-bottom:7px;}

table.frm .inputbox{
	width:150px;
}
td.h{
	font-weight:bold;
	color:#07539d;
	font-size:13px;
}
.button{
	background:url(images/bt.png) no-repeat;
	width:90px;
	height:30px;
	border:none;
}
</style>
<script language="javascript">
function checkfrm(){
	if(document.frmsend.company.value==''){
		alert('Nhập vào tên công ty');
		document.frmsend.company.focus();
		return false;
	}
	if(document.frmsend.name.value==''){
		alert('Nhập vào tên của bạn');
		document.frmsend.name.focus();
		return false;
	}
	if(document.frmsend.email.value==''){
		alert('Nhập vào địa chỉ email của bạn');
		document.frmsend.email.focus();
		return false;
	}
	if(document.frmsend.phone.value==''){
		alert('Nhập vào số điện thoại liên hệ của bạn');
		document.frmsend.phone.focus();
		return false;
	}
	
	document.getElementById('txt').style.display='block'; 
	document.frmsend.submit.style.display='none'
	return true;
}
</script>
</head>

<body>
<div class="primary">
  <div class="inner">
    <div class="logo">
        <a href="http://www.nsp.com.vn" target="_blank"><img src="images/logo-NSP.png" border="0" style="float:right" /></a> 
        <a href="http://welcome.brother.com/" target="_blank"><img src="images/logo-Brother.png" border="0" style="float:left" /></a>
    </div>
<br /><br />&nbsp;
<div>
	<img src="images/banner.jpg" border="0" alt="Trải nghiệm giải pháp Giám sát hình ảnh toàn diện" />
</div>
<?php
if(isset($_REQUEST['send']) ):
//include_once("send2.php");
?>
<div style="border-top:2px solid #0C0;border-bottom:2px solid #0C0; padding:5px; margin-top:5px; background:#CFF; font-size:15px; color:#0C0">Thông tin đăng ký của bạn đã gửi thành công.</div>
<?php endif?>
<div style="height:10px;">&nbsp;</div>
<div style="float:left; width:49%;">
	<h3 style="color:#07539d;">ĐĂNG KÝ YÊU CẦU DEMO SẢN PHẨM</h3>
    <p style="line-height:1.7">Vui lòng điền đầy đủ thông tin vào mẫu đăng ký để được tư vấn, sử dụng demo MIỄN PHÍ giải pháp đánh nhãn THÔNG MINH NHẤT hiện nay!</p>
</div>
<div style="float:right; width:49%; border:1px solid #CCC;">
<form action="send.php" method="post" name="frmsend" onsubmit="return checkfrm();">
<div style="background:#07539d; padding:10px 0 10px 5px; font-size:11px; color:#FFF; text-align:center"><strong>Quý khách vui lòng điền đầy đủ thông tin dưới đây:</strong></div>
<p><i>&nbsp;Các trường có dấu (*) là bắt buộc nhập dữ liệu</i></p>
<table width="auto" border="0" cellspacing="0" cellpadding="5" class="frm">
  <tr>
    <td class="label">Tên công ty (*)</td>
    <td><input type="text" name="company" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Tên người yêu cầu(*)</td>
    <td><input type="text" name="name" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ email (*)</td>
    <td><input type="text" name="email" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label" nowrap="nowrap">Điện thoại liên hệ (*)</td>
    <td><input type="text" name="phone" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ</td>
    <td><input type="text" name="address" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">Website</td>
    <td><input type="text" name="website" class="inputbox" size="30" /></td>
  </tr>
  <tr>
    <td class="label">&nbsp;</td>
    <td><input type="submit" name="submit" value="&nbsp;" class="button" /> <p style="display:none;" id="txt">Đang xử lý, vui lòng chờ...</p></td>
  </tr>
</table>
</form>
</div>
<div style="clear:both;">&nbsp;</div>
<div style="clear:both;">&nbsp;</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td style="padding:0.14in 0.14in 0.14in 0.14in" bgcolor="#868686">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        	  <tr>
        	    <td width="27%" valign="middle" align="left"><a href="http://nsp.com.vn"><img src="http://nsp.com.vn/logo/logo-132x45.png" border="0" width="132" height="45"></a></td>
        	    <td width="73%"><p style="font-size:9pt; color:#FFF">Công ty TNHH  TM-DV Tin học Nhân Sinh Phúc<br>
          Địa chỉ: 359  Võ Văn Tần, Phường 5, Quận 3, TP. Hồ Chí Minh<br>
          Điện thoại:  +84 8 3834 2108<br>
          Fax: +84 8  3834 2109</p></td>
      	    </tr>
      	  </table>
      </tr>
</table>
</div>


</div>
</div>

</div>
</body>
</html>
