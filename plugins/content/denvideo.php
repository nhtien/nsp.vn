<?php
/**
* @version denVIDEO v2.9
* @copyright (C) 2007 3DEN StudiO
* @package Joomla!
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*/
// no direct access
defined( '_JEXEC' ) or die( 'Access Denied!' );
$mainframe->registerEvent( 'onPrepareContent', 'plgContentDenVideo' );
function plgContentDenVideo( &$row, &$params, $page=0 ){		
	// EXIT
	if ( JString::strpos( $row->text, '{denvideo' ) === false ){
		return false;
	}
	// PARAMs
	$plugin =& JPluginHelper::getPlugin('content', 'denvideo');
	$plgParams = new JParameter( $plugin->params );
	// PATH
	$paths['plg'] = JURI::base().'plugins/content/denvideo/';
	$paths['img'] = JURI::base().'images/'.$plgParams->get('defaultdir', 'stories');
	// Regular Expression
	$regex = '/\{denvideo(.*?)}/i';
	$total = preg_match_all( $regex, $row->text, $matches );
	//print_r( $matches );//text
	for( $x=0; $x < $total; $x++ ){
		// General Params			
		$parts = explode( ' ', trim($matches[1][$x]) );
		// Default Vaues
		$width = (int)$plgParams->get('width', 500);
		$height = (int)$plgParams->get('height', ($width * 0.7) );
		$autoplay = $plgParams->get('autostart', 0 );
		// Params
		$pcount = count($parts);
		if($pcount > 1){//Width
			$width = $parts[1];
			if($pcount > 2){//Height
				$height = $parts[2];				
				if($pcount > 3){//autoStart
					$autoplay = $parts[3];				
				}
			}else{
				$height = $width * 0.7;
			}
		}
		$width='width:'.$width.'px;'; $height='height:'.$height.'px;';
		// Video Display
		$video = $parts[0];
		$video = (strpos($video, 'http://')!==false)? $video: $paths['img'].'/'.$video;
		// Put Video inside the content
		$replace = showDenVideo( $video, $paths, $width, $height, $autoplay ).'<a href="http://www.3den.org/" class="small" style="display:none;">Powered by 3DEN<br /></a>';
		$row->text = str_replace( $matches[0][$x], $replace, $row->text );
	}
	return true;
}
/**
 * Show denVideo
 * @return str
 * @param $video str
 * @param $width int
 * @param $height int
 * @param $autoplay bool
 */
function showDenVideo( $video, &$paths, $width, $height, $autoplay = false ){	
		/* YouTube Video 
		*****************************************************/
		if( strpos( $video, 'youtube.com' ) ){
			$video = substr( strstr( $video, 'v=' ), 2 );
			$replace = '<object style="'.$width . $height.'">'.
				'<param name="movie" value="http://www.youtube.com/v/'.$video.'" />'.
				'<param name="wmode" value="transparent" />'.
				'<embed src="http://www.youtube.com/v/'.$video.'"  wmode="transparent" type="application/x-shockwave-flash" style="'.$width . $height.'"></embed>'.
			'</object>';
		}
		/* Yahoo Video 
		*****************************************************/
		elseif( strpos( $video, 'video.yahoo' ) ){
			$video = explode( '/', substr( strstr( $video, 'watch/' ), 6 ) );		
			$replace = '<object style="'.$width . $height.'"><embed src="http://us.i1.yimg.com/cosmos.bcst.yahoo.com/player/media/swf/FLVVideoSolo.swf" flashvars="id='.$video[1].'&b=2"  type="application/x-shockwave-flash" style="'.$width . $height.'"></embed></object>';
		}
		/* Google Video 
		*****************************************************/
		elseif( strpos( $video, 'video.google' ) ){
			$video = substr( stristr( $video, 'docid=' ), 6 );		
			$replace = '<object style="'.$width . $height.'"><embed style="'.$width . $height.'" id="VideoPlayback" type="application/x-shockwave-flash" src="http://video.google.com/googleplayer.swf?docid='.$video.'" flashvars="autoPlay='.$autoplay.'"></embed></object>';
		}
		/* Brightcove Video 
		*****************************************************/
		elseif( strpos( $video, 'brightcove.tv' ) ){
			$video = substr( stristr( $video, 'title=' ), 6 );	
			$replace = '<embed src="http://www.brightcove.tv/playerswf" bgcolor="#FFFFFF" flashVars="initVideoId='.$video.'&servicesURL=http://www.brightcove.tv&viewerSecureGatewayURL=https://www.brightcove.tv&cdnURL=http://admin.brightcove.com&autoStart='.$autoplay.'" base="http://admin.brightcove.com" name="bcPlayer" allowFullScreen="true" allowScriptAccess="always" seamlesstabbing="false" type="application/x-shockwave-flash" swLiveConnect="true" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" 
			style="'.$width . $height.'"</embed>';
		}
		/* Scribd
		*****************************************************
		elseif( strpos( $video, 'scribd.com' ) ){
			$video = substr( stristr( $video, 'doc/' ), 4 );
			$video = explode( '/', $video ); print_r($video);			
			$replace = '<object style="'.$width . $height.'" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" align="middle"><param name="flashvars" value="&document_id='.$video[0].'&access_key=xzmh9nlyosi1&page=&version=1"><param name="movie" value="http://documents.scribd.com/ScribdViewer.swf"><param name="quality" value="high"><param name="play" value="true"><param name="loop" value="true"><param name="scale" value="showall"><param name="wmode" value="opaque"><param name="devicefont" value="false"><param name="bgcolor" value="#ffffff"><param name="menu" value="true"><param name="allowFullScreen" value="true"><param name="allowScriptAccess" value="always"><param name="salign" value=""><embed flashvars="&document_id='.$video[0].'&access_key=xzmh9nlyosi1&page=&version=1" src="http://documents.scribd.com/ScribdViewer.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" play="true" loop="true" scale="showall" wmode="opaque" devicefont="false" bgcolor="#ffffff" menu="true" allowfullscreen="true" allowscriptaccess="always" salign="" type="application/x-shockwave-flash" align="middle" style="'.$width . $height.'" ></embed></object>';//124368&access_key=xzmh9nlyosi1&page=&version=1

		}		
		/* Video from files
		******************************************************/ 
		else{			
			$type = substr( $video, strrpos($video, '.') );			
			switch( $type ){
			/* Flash .SWF 
			*****************************************************/
			case '.swf':		    
				$replace = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0" style="'.$width . $height.'">'.
					'<param name="movie" value="'.$video.'" />'.
					'<param name="quality" value="high" />'.
					'<embed src="'.$video.'" quality="high" pluginspage="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" style="'.$width . $height.'"></embed>'.
				'</object>';
				break;				
			/* Video .FLV
			*****************************************************/
			case '.flv':
				//$player = $paths['plg'].'flvplayer.swf'; 	
				$player = JURI::base().'plugins/content/denvideo/flvplayer.swf';
				$play = $player.'?autoStart='.$autoplay.'&file='.$video;
				$replace = '<object type="application/x-shockwave-flash" data="'.$play.'" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" style="'.$width . $height.'" wmode="transparent"  width="'.$width.'" height="'.$height.'" >'.
					'<param name="movie" value="'.$play.'" />'.
					'<param name="wmode" value="transparent" />'.
					'<embed src="'.$play.'" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" style="'.$width . $height.'" width="'.$width.'" height="'.$height.'" ></embed>'.
				'</object>'; 
				break;				
			/* Quicktime .MOV
			************************************************/
			case '.mov':
			case '.mp4':
				$replace = '<object codebase="http://www.apple.com/qtactivex/qtplugin.cab" classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" style="'.$width . $height.'" >'.
					'<param name="src" value="'.$video.'" />'.
					'<param name="controller" value="True" />'.
					'<param name="cache" value="False" />'.
					'<param name="autoplay" value="'.$autoplay.'" />'.
					'<param name="kioskmode" value="False" />'.
					'<param name="scale" value="tofit" />'.
					'<embed src="'.$video.'" pluginspage="http://www.apple.com/quicktime/download/" scale="tofit" kioskmode="False" qtsrc="'.$video.'" cache="False" style="'.$width . $height.'" controller="True" type="video/quicktime" autoplay="'.$autoplay.'" />'.
				'</object>';	
				break;
				
			/* Realmedia .RM & .RAM
			*****************************************************/
			case '.rm':
			case '.ram': 	
				$replace = '<object classid="clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA" style="'.$width . $height.'" >'.
					'<param name="controls" value="ControlPanel" />'.
					'<param name="autostart" value="'.$autoplay.'" />'.
					'<param name="src" value="'.$video.'" />'.
					'<embed src="'.$video.'" type="audio/x-pn-realaudio-plugin" style="'.$width . $height.'" controls="ControlPanel" autostart="'.$autoplay.'" />'.
				'</object>';
				break;
				
			/* Applet .CLASS
			*****************************************************/
			case '.class':			
				$end = strrpos($video, '/');
				$code = substr( $video, 0, $end );
				$video = substr( $video, $end+1 );			
				$replace = '<applet codebase="'.$code.'" code="'.$video.'" style="'.$width . $height.'"></applet>';
				break;
				
			/* Error
			*****************************************************/
			default: 
				$replace = '<q>Invalid Video: '.$video.'</q>';
				break;
			}
		}
		return $replace;
}
?>