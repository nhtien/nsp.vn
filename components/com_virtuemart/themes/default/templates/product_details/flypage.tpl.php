<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
mm_showMyFileName(__FILE__);
 ?>
 <div class="vm_product_detail">
<?php echo $buttons_header // The PDF, Email and Print buttons ?>
<?php
global $Itemid,$sess;
if( $this->get_cfg( 'showPathway' )) {
	echo "<div class=\"pathway\">$navigation_pathway</div>";
}
if( $this->get_cfg( 'product_navigation', 1 )) {
	if( !empty( $previous_product )) {
		echo '<a class="previous_page" href="'.$previous_product_url.'">'.shopMakeHtmlSafe($previous_product['product_name']).'</a>';
	}
	if( !empty( $next_product )) {
		echo '<a class="next_page" href="'.$next_product_url.'">'.shopMakeHtmlSafe($next_product['product_name']).'</a>';
	}
}
?>
<?php
foreach( $category_list as $c){ 
	$category_link = $sess->url("page=shop.browse&amp;category_id=".$c['category_id']);
	$category_pathway[] = '<a href="'.$category_link.'">'.$c['category_name'].'</a>';
}
$category_pathware = implode("&nbsp;&raquo;&nbsp;",$category_pathway);
?>
	<script src="components/com_nsp/js/jquery-1.3.2.js"></script>
    <script type="text/javascript" src="components/com_nsp/js/jquery.lightbox-0.5.js"></script>
    <link rel="stylesheet" type="text/css" href="components/com_nsp/themes/jquery.lightbox-0.5.css" media="screen" />
	
    <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
<br style="clear:both;" />
<table border="0" style="width: 100%;">
  <tbody>
	<tr>
	  <td width="20%" rowspan="1" valign="top" align="center"><br/>
	  <div id="gallery">
	  	<?php echo $product_image ?><br/><br/><?php echo $this->vmlistAdditionalImages( $product_id, $images ) ?>
		</div>
		</td>
	  <td rowspan="1" colspan="">
	  <a href="index.php?option=com_nsp&view=brand&bid=<?php echo $manufacturer->manufacturer_id ?>&Itemid=72">
	  	<img src="<?php echo JURI::base()?>images/nsp/manufacturer/logos/<?php echo $manufacturer->mf_logo; ?>" alt="<?php echo $manufacturer->name?>" border="0" />
		</a>
	  <h4><?php echo $product_name ?><?php echo $edit_link ?></h4>
	  
	  <?php if( !$child_product ) :?>
	  <strong><?php echo $VM_LANG->_('PHPSHOP_CART_SKU');?>: <?php echo $product_sku;?></strong>
	  <?php endif;?>
	  <div class="pathway"><?php echo $VM_LANG->_('PHPSHOP_CATEGORY').': '.$category_pathware;?></div>
	  <?php
	  if( $this->get_cfg('showManufacturerLink')) {
	  	echo $manufacturer_link;
		}
		?>
		<div><?php echo $product_price_lbl . $product_price; ?></div>
		<?php echo $product_packaging; ?>
	   <div class="ask-question"><?php echo $ask_seller; ?></div>
	  <p>
		<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4bcfff15556b296d"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4bcfff15556b296d"></script>
		</p>
		<p><?php echo $product_description ?></p>
	  <?php 
	  		if( $this->get_cfg( 'showAvailability' )) {
	  			echo $product_availability; 
	  		}
	  		?>
		<div><?php echo $addtocart ?></div>
		<?php echo $product_type ?>
	  </td>
	</tr>
	
	<!--<tr>
	  <td colspan="2">
	  	<img src="<?php echo JURI::base()?>images/nsp/payments/visaCard.jpg" title="Visa Credit Card" />
		<img src="<?php echo JURI::base()?>images/nsp/payments/mastercard.jpg" title="MasterCard" />
		<img src="<?php echo JURI::base()?>images/nsp/payments/Paypal.jpg" title="Papal" />
		<img src="<?php echo JURI::base()?>images/nsp/payments/nganluong.jpg" title="Nganluong.vn" />
	  </td>
	</tr>-->
	<!-- Product panel -->
	<tr>
		<td colspan="2">
		<br />
		<div class="product_panel">
<script>
    $(document).ready(function(){
        $("dd:not(:first)").hide();
        $("dt a").click(function(){
            //$("dd:visible").slideUp("slow");
            //$(this).parent().next().slideDown("slow");
            return false;
        });
    });
	function arrowPanelProduct( idx ){
		var n = <?php echo (count($file_list) ? 6 : 5) ?>;
		var obj = document.getElementById('panelItemId_0'+idx);
		
		if(obj.className=='active')
		{
			obj.className='';
			$('#panelItemIdDD_0'+idx).slideUp('slow');
		}
		else{
			obj.className='active';
			$('#panelItemIdDD_0'+idx).slideDown('slow');
		}
			
		for(i=1 ; i < n ; i++){
			//document.getElementById('panelItemId_0'+i).className='';
		}
		
	}
	
	$(document).ready(function(){
		//var w = document.getElementById('nsp-content').offsetWidth;
		//document.getElementById('product_spes').style.width=(w-30)+'px';
	});
</script>
<dl>
	<dt><a href="javascript:void(0);" onclick="arrowPanelProduct(1);" id="panelItemId_01" class="active"><?php echo $VM_LANG->_('VM_PRODUCT_DETAIL_OVERIVEW');?></a></dt>
	<dd id="panelItemIdDD_01">
		<?php echo $product_overview; ?>
	</dd>
	<dt><a href="javascript:void(0);"  onclick="arrowPanelProduct(2);"  id="panelItemId_02" class=""><?php echo $VM_LANG->_('VM_PRODUCT_DETAIL_TECHNICAL');?></a></dt>
	<dd id="panelItemIdDD_02">
		<div id="product_spes">
		<?php echo $product_tech; ?>
		</div>
	</dd>
	<dt><a href="javascript:void(0);"  onclick="arrowPanelProduct(3);"  id="panelItemId_03" class="">
		<?php echo $VM_LANG->_('VM_PRODUCT_DETAIL_MODEL').' / '.$VM_LANG->_('VM_PRODUCT_DETAIL_ACCESSORIES');?>
	</a></dt>
	<dd id="panelItemIdDD_03"> 
		<?php
		if( $child_products_list ){
			$html = '<strong>'.$VM_LANG->_('VM_PRODUCT_DETAIL_MODEL').'</strong><hr size="1" />';
		
			$html .= '<table class="n-border" width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr class="title"><td>'.$VM_LANG->_('PHPSHOP_CART_SKU').'</td><td>'.$VM_LANG->_('PHPSHOP_CART_NAME').'</td>';
			//$html .= '<td>'.$VM_LANG->_('PHPSHOP_CART_PRICE').'</td>';
			$html .= '</tr>';
			foreach( $child_products_list as $item){
				$link_product = "index.php?page=shop.product_details&flypage=flypage.tpl&product_id=".
				$item->product_id."&category_id=".
				JRequest::getVar('category_id')."&option=com_virtuemart&Itemid=$Itemid";
				$link_ask = "index.php?option=com_virtuemart&page=shop.ask&product_id=$item->product_id&subject=".urlencode($VM_LANG->_('PHPSHOP_PRODUCT_CALL').$item->product_name)."&Itemid=92";
				
				$product_price = $item->product_price['product_price'] ? (number_format($item->product_price['product_price'],2).' '.$item->product_price['product_currency']) : '<a href="'.$link_ask.'">Call<a/>';
				$html .= '<tr>';
				$html .= '<td><a href="'.$link_product.'">'.$item->product_sku.'</a></td>';
				$html .= '<td><a href="'.$link_product.'">'.$item->product_name.'</a></td>';
				//$html .= '<td>'.$product_price.'</td>';
				$html .= '</tr>';
			}
			$html .= '</table><br /><br />';
			
			echo $html;
		}
		?>
		<?php if( $product_accessories ):?>
		<strong><?php echo $VM_LANG->_('VM_PRODUCT_DETAIL_ACCESSORIES');?></strong>
		<hr size="1" class="product_blog" />
		<?php echo $product_accessories;?>
		<?php endif;?>
	</dd>
	
	<dt><a href="javascript:void(0);"  onclick="arrowPanelProduct(4);"  id="panelItemId_04" class=""><?php echo $VM_LANG->_('VM_PRODUCT_DETAIL_SERVICES');?></a></dt>
	<dd id="panelItemIdDD_04">
		<?php echo $product_services;?>
	</dd>
	
		<?php
		if(count($file_list)):?>
	<dt><a href="javascript:void(0);"  onclick="arrowPanelProduct(5);"  id="panelItemId_05" class=""><?php echo JText::_('Attachment');?></a></dt>
	<dd id="panelItemIdDD_05">
		<h4><?php echo JText::_('Download for this Product');?></h4>
		<p><?php echo JText::_('Product download header')?></p>
		<table class="n-border" border="0" cellpadding="5" cellspacing="0" width="100%">
		<tr class="title">
		  <td><?php echo JText::_('File Name')?></td>
		  <td><?php echo JText::_('File Description')?></td>
		  <td><?php echo JText::_('File Size')?></td>
		  <td><?php echo JText::_('Downloaded')?></td>
		  <td><?php echo JText::_('View/Download')?></td>
		 </tr>
	  	<?php
		$file_extension = array(
					"pdf"	=> "pdf.gif",
					"PDF"	=> "pdf.gif",
					"zip"	=> "zip.gif",
					"rar"	=> "rar.gif",
					"doc"	=> "doc.gif",
					"docx"	=> "doc.gif",
					"xls"	=> "xls.gif",
					"xlsx"	=> "xls.gif",
					"txt"	=> "txt.gif",
					"exe"	=> "exe.gif",
					"jar"	=> "jar.gif",
					"ttf"	=> "ttf.gif",
					"jpg"	=> "img.gif",
					"png"	=> "img.gif",
					"gif"	=> "img.gif",
					"other"	=> "other.gif"
					);
		$html = '';
		foreach( $file_list as $f){ 
			if( array_key_exists($f->file_extension, $file_extension) ){
				$file_icon = $file_extension[strtolower($f->file_extension)];
			}else{
				$file_icon = $file_extension['other'];
			}
			$html .= '<tr>';
			$html .= '<td>'.$f->link_download.$f->filename.'</a></td>';
			$html .= '<td><strong>'.$f->filetitle.'</strong><p>'.$f->file_description.'</p></td>';
			$html .= '<td>'.$f->filesize.'</td>';
			$html .= '<td align="center">'.$f->count_download.'</td>';
			$html .= '<td align="center">'.$f->link_download.'<img src="images/file_icons/'.$file_icon.'" border="0" /></a></td>';
			$html .= '</tr>';
		}
		
		echo $html;
		echo '</table>';
		echo '</dd>';
		endif;
		?>
</dl>
</div>
			
		</td>
	</tr>
	<!-- end -->
	<tr>
	  <td colspan="2"><?php echo $product_reviews ?></td>
	</tr>
	<tr>
	  <td colspan="2"><?php echo $product_reviewform ?><br /></td>
	</tr>
	<tr>
	  <td colspan="2"><?php echo $related_products ?><br />
	   </td>
	</tr>
	<?php if( $this->get_cfg('showVendorLink')) { ?>
		<tr>
		  <td colspan="2"><div style="text-align: center;"><?php echo $vendor_link ?><br /></div><br /></td>
		</tr>
	<?php  } ?>
  </tbody>
</table>
<?php 
if( !empty( $recent_products )) { ?>
	<div class="vmRecent">
	<?php echo $recent_products; ?>
	</div>
<?php 
}
if( !empty( $navigation_childlist )) { ?>
	<?php echo $VM_LANG->_('PHPSHOP_MORE_CATEGORIES') ?><br />
	<?php echo $navigation_childlist ?><br style="clear:both"/>
<?php
} 
?>
</div><!--vm_product_detail-->

<div id="backgroundPopup"></div>
<?php
	
	$ses = JFactory::getSession();
	//$ses->set('__flag__',1);
	$flag = $ses->get('__flag__',0,'product');
	
/*Thay đổi sản phẩm DTX - > DSX */
if($_GET['product_id']==350 && $flag!=1)
{
	
	$ses->set('__flag__',1,'product');
	
	/*Hiện popup lên để biết sự thay đổi của sản phẩm*/
	?>
    
    <div id="popup" style="left: 25%; 
    top: 15%; 
    z-index:31 !important; 
    background:#FFF;
     display:none;
      border-radius: 3px;
    -moz-border-radius: 3px;
    -weblit-border-radius: 3px;
    -o-border-radius: 3px;
    box-shadow: 0px 0px 10px -1px rgba(0, 0, 0, 0.5) inset; padding:30px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td><h2 style="color:#2D548F">Sản phẩm phân tích cáp tiên tiến nhất trong lĩnh vực đo kiểm</h2></td>
          </tr>
          <tr>
            <td>
            <div style="float:left; width:400px;">
              <p><strong>Bạn đã từng gặp khó khăn:</strong></p>
              
              <ul type="disc" >
                <li>Đội ngũ kỹ thuật viên trình độ không đồng đều, sử         dụng nhiều công cụ với các công việc khác nhau (kiểm tra cáp đồng, kiểm         tra cáp quang, kiểm tra hệ thống Wireless…) </li>
                <li>Việc thi công mạng ngày một phức tạp hơn và không có         một chuyên gia hướng dẫn các lỗi thi công, làm tăng chi phí sửa chữa </li>
                <li>Thiếu kiến thức về thi công cáp và các tiêu chuẩn cáp         được áp dụng trên toàn thế giới.</li>
               
              </ul>
               <p><strong style="color:#2D548F">Hãy tham khảo thế hệ    sản phẩm phân tích cáp tiên tiến nhất-DSX-5000 Cable Analyzer</strong></p>
               <div class="btn_143w_2lines" style="float:left;margin-right:10px;"><a href="<?php echo JURI::base()?>vi/san-pham/cabling-certification-a-testing/copper-certification-a-testing.html?page=shop.product_details&flypage=flypage.tpl&product_id=1322&category_id=30&manufacturer_id=5" target="_blank">Xem sản phẩm DSX-5000 Cable Analyzer</a></div>
              </div>
              <img src="<?php echo JURI::base()?>images/DSX.jpg" style="float:right" />
              <div style="clear:both;"></div>
              </td>
              </tr>
        </table>
    </div>
    <a id="fancybox-close" style="display: inline;"></a>
    
    <style>
	.btn_143w_2lines a {
		display: block;
		width: 143px;
		height: 36px;
		padding: 7px 0 0 0;
		line-height: 14px;
		color: #000000;
		text-align: center;
		font-family: Arial,Helvetica,sans-serif;
		font-size: 13px;
		font-weight: bold;
		text-decoration: none;
		background-image: url(<?php echo JURI::base()?>images/btn_143w_2lines_yellow.gif);
		background-position: top left;
		background-repeat: no-repeat;
		border: 0;
		border-radius: 0;
		-webkit-box-sizing: content-box;
		-moz-box-sizing: content-box;
		box-sizing: content-box;
	}
	#backgroundPopup {
		position: fixed;
		width: 100%;
		height: 100%;
		top:0;
		left:0;
		opacity: 0.6;
		z-index: 30;
		display: none;
	}
	#fancybox-close {
		background: url("<?php echo JURI::base()?>images/fancybox.png") repeat scroll -40px 0 rgba(0, 0, 0, 0);
		cursor: pointer;
		display: none;
		height: 30px;
		position: fixed;
		right: 26.5%;
		top: 12.5%;
		width: 30px;
		z-index: 1103;
	}
	</style>
    <script type="text/javascript">
		var popupStatus_popup = 0;
		//loading popup with jQuery magic!_ add website
		function loadPopup_popup(){
			//loads popup only if it is disabled
			if(popupStatus_popup==0){
				$("#backgroundPopup").css({
					"background":"black"
				});
				$("#backgroundPopup").fadeIn(500);
				$("#popup").fadeIn(500);
				popupStatus_popup = 1;
			}
		}
		function loadbackgroundPopup(){
			if(popupStatus_popup==0){
				$("#backgroundPopup").css({
					"opacity": "0.5"
				});
				$("#backgroundPopup").fadeIn(500);
				setTimeout(function(){$("#backgroundPopup").hide(500)},3000)
			}
		}
		//disabling popup with jQuery magic!
		function disablePopup_popup(){
			//disables popup only if it is enabled
			if(popupStatus_popup==1){
				$("#backgroundPopup").fadeOut(500);
				$("#popup").fadeOut(500);
				$("#fancybox-close").fadeOut(500);
				popupStatus_popup = 0;
			}
		}
		//centering popup
		function centerPopup_popup(){
			//request data for centering
			//centering
			$("#popup").css({
				"position": "fixed",
		
			});
		}
		$(document).ready(function(e) {
			//centering with css
			var loaded = false;
			var time = 2000;
			$(function() {
				setTimeout(function() { 

						centerPopup_popup();
						//load popup
						loadPopup_popup();
				},time);
			});
			
			//Click out event!
			$("#backgroundPopup").live("click",function(){
				disablePopup_popup();
			});
			$("#fancybox-close").live("click",function(){
				disablePopup_popup();
			});
		
		});
	</script>
    <?php
	/*Hiện popup lên để biết sự thay đổi của sản phẩm*/
}
/*End Thay đổi sản phẩm DTX - > DSX */
