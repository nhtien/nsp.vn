<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );
mm_showMyFileName(__FILE__);
//JHTML::_('behavior.modal', 'a.modal-button');
include_once(dirname(__FILE__).DS.'includes'.DS.'a.modal.php'); 
 ?>
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="browseProductItem">
  <tr>
    <td class="thumb">
<!-- images -->
<a class="modal-button" href="<?php echo $product_full_image ?>" rel="{handler: 'image', size: {x: 200, y: 200}, overlayOpacity: 0.7, classWindow: 'escdownloadpreviewwindow', classOverlay: 'escdownloadpreviewoverlay'}" >
		<img src="product_image.php?style=0&filename=<?php echo urlencode($product_thumb_image)?>" border="0" title="<?php echo $product_name?>" alt="" class="browseProductImage" />
			<?php //echo ps_product::image_tag( $product_thumb_image, 'class="browseProductImage" border="0" title="'.$product_name.'" alt="'.$product_name .'"' ) ?>
		</a>
	        <noscript>
	            <a href="<?php echo $product_full_image ?>" target="_blank" title="<?php echo $product_name ?>">
	            <?php echo ps_product::image_tag( $product_thumb_image, 'class="browseProductImage" border="0" title="'.$product_name.'" alt="'.$product_name .'"' ) ?>
	            </a>
	        </noscript>
	</td>
    <td class="name">
        <a title="<?php echo $product_name ?>" href="<?php echo $product_flypage ?>"><strong><?php echo $product_name ?></strong></a>
		<?php
		// show part number
		if($product_parent_id > 0 || !$child_product):?>
		<br />
		<strong><?php echo $VM_LANG->_('PHPSHOP_CART_SKU');?>: <?php echo $product_sku ?></strong>
		<?php endif;?>
       <p>
            <?php echo $product_s_desc ?>&nbsp;
            
        </p>
	</td>
    <td class="rating">
        <?php 
		//echo $product_rating 
		
		?>
        <a href="<?php echo $product_flypage ?>" title="<?php echo $product_details ?>" class="_readmore">
			<img src="images/btn-grey-more-vi.jpg" border="0" /></a>
	</td>
	<?php if($product_price):?>
    <td width="" class="price">
           <?php echo $product_price ?>
        <br />
        <span class="browseAddToCartContainer">
        <?php echo $form_addtocart ?>
        </span>
	</td>
	<?php endif;?>
  </tr>
</table>
<hr size="1" class="product_blog" />


