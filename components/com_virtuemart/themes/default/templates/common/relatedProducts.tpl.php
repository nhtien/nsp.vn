<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); ?>

<hr size="1" class="product_blog" />
<h3><?php echo $VM_LANG->_('PHPSHOP_RELATED_PRODUCTS_HEADING') ?></h3>
 <?php
 	$columns = 4;
	$i = 1; 
	$total_replace_prod = count($products->record);
	$td_w = $total_replace_prod >= $columns ? floor(100 / $columns).'%' : floor(100/$total_replace_prod).'%';
 ?>
<table width="100%" align="left" border="0" cellspacing="0" cellpadding="5" class="related_products">
	<tr>
    <?php 
    while( $products->next_record() ) { ?>
      	<td valign="top" width="<?php echo $td_w?>">
      		<?php echo $ps_product->product_snapshot( $products->f('product_sku') ) ?>
      	</td>
	<?php 
	if( $i % $columns == 0) echo '</tr><tr>';
    }
	?>
    </tr>
</table> 
