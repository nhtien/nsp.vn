<?php if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' );

if($empty_cart) { ?>
    
    <div style="margin: 0 auto;">
    <?php if(!$vmMinicart) { ?>
        <img src="<?php echo JURI::base()?>documents/vm_images/ps_image/menu_logo.gif" alt="NSP Shopping" width="80" border="0" />
        <br />
    <?php }
    echo $VM_LANG->_('PHPSHOP_EMPTY_CART') ?>
    </div>
<?php } 
else {
    // Loop through each row and build the table
    foreach( $minicart as $cart ) { 		

		foreach( $cart as $attr => $val ) {
			// Using this we make all the variables available in the template
			// translated example: $this->set( 'product_name', $product_name );
			$this->set( $attr, $val );
		}
        if(!$vmMinicart) { // Build Minicart
            ?>
            <div style="float: left;">
            <?php echo $cart['quantity'] ?><strong class="x">&nbsp;x&nbsp;</strong><a href="<?php echo $cart['url'] ?>"><?php echo $cart['product_name'] ?></a>
            </div>
            <div style="float: right;">
            <?php echo $cart['price'] ?>
            </div>
            <br style="clear: both;" />
            <?php echo $cart['attributes'];
        }
    }
}
if(!$vmMinicart) { ?>
    <hr style="clear: both;" />
<?php } ?>
<div style="float: left;" >
<strong><?php echo $total_products ?></strong>
</div>
<div style="float: right;">
<strong><?php echo $total_price ?></strong>
</div>
<?php if (!$empty_cart && !$vmMinicart) { ?>
    <br/><br style="clear:both" /><div align="center">
    <?php echo $show_cart ?>
    </div><br/>

<?php } 
echo $saved_cart;
?>