<?php
/**
 * Comments Controller for Comments Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */

jimport('joomla.application.component.controller');

class CommentsController extends JController{
	/** @var String MultiController controll*/
	var $_ctr;
	
	/**
	 * constructor (registers additional tasks to methods)
	 * 
	 * @return void
	 */
	function __construct($config = array())
	{
		$this->addModelPath(JPATH_COMPONENT_ADMINISTRATOR.DS.'models');
		parent::__construct($config);
		
		$this->_ctr = JRequest::getCmd('c', 'comment');
		$this->registerTask( 'apply', 	'save' );
		$this->registerTask( 'add',		'edit' );
	}

	/**
	 * Save Comment
	 * 
	 * @return void
	 */
	function save()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		
		// get Model
		$model = $this->getModel($this->_ctr);
				
		// get Request data 
		$post	= JRequest::get('post');

		// captcha error
		if (!$this->_checkCaptcha()){
			$msg = JText::_( 'SECRETWORD_IS_WRONG' );
            $type= 'error';
		} 
		// save comment
		else{
			// Save Success or Error
			if ( $model->store($post) ) {// success
				$msg = JText::_( 'Successfully saved changes' );
				$type = 'message';
			}
			else{// error			
				$msg = JText::_( $model->getError() );
				$type = 'error';
			} 
		}	
		
		//Redirect
		$target = $post['target'];		
		$this->setRedirect($target, $msg, $type);				
	}
	
	
	/**
	 * Cancel Editing
	 * 
	 * @return void
	 */
	function cancel()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		// Checkin the weblink
		$model = $this->getModel($this->_ctr);
		$model->checkin();
		
		$target = JRequest::getVar('target');
		$this->setRedirect($target, $msg, $type);			
	}	
	
	/**
	 * Display a captcha image
	 * @return 
	 */
	function displayCaptcha() {
		global $mainframe;

        $Ok = null;
        $mainframe->triggerEvent('onCaptcha_Display', array($Ok));
        if (!$Ok) {
 			echo JText::_('Error displaying Captcha');
        }
	}
	
	/**
	 * Called inside save() to check the captcha word
	 * @return 
	 */
	function _checkCaptcha() {
		global $mainframe;
		
		if( !JRequest::getInt('use_captcha', 0 ) ){
			return true;
		}
		
		$return = false;
		$word = JRequest::getCmd('word', '');
 		$mainframe->triggerEvent('onCaptcha_confirm', array($word, &$return));
   	
        return $return;
	}
}
