<?php 
/**
 * Media Model for JMultimedia Component
 * 
 * @package    		JMultimedia Suite
 * @subpackage 	Components
 * @link				http://3den.org
 * @license		GNU/GPL
 */
defined('_JEXEC') or die('Restricted access'); // no direct access

// Set template vars
$action = JRoute::_('index.php?option=com_comments&view=comment');
$uri =& JFactory::getURI();	
$target = $uri->toString();
?>

<script language="javascript" type="text/javascript">

</script>
<style>

</style>

<form enctype="multipart/form-data" action="<?php echo $action; ?>" method="post" name="commentForm" id="commentForm">
<div class="comment">
	<h3><?php echo JText::_('Write your comment'); ?></h3>
	<table class="commentbox" cellspacing="4" >
	
	<!-- Title -->
	<?php if( $params->get('edit_title') ): ?>
	<tr>
		<td align="left">
			<input class="inputbox" type="text" name="title" id="title" size="32" maxlength="250" value="<?php echo $item->title;?>" />
			<label for="title" class="key">
				<?php echo JText::_( 'Title' ); ?>
			</label>
		</td>
	</tr>		
	<?php endif; ?>

	<!-- Author-->	
	<?php if( $params->get('edit_author') ): ?>
	<tr>
		<td align="left">
			<input class="inputbox" type="text" name="author" id="author" size="32" maxlength="250" value="<?php echo $item->params->get('author', $item->author); ?>" />
			<label for="author">
				<?php echo JText::_( 'Author' ); ?>
			</label>
		</td>
	</tr>		
	<?php endif; ?>

	<!-- Email -->	
	<?php if( $params->get('edit_email') ): ?>
	<tr>
		<td align="left">
			<input class="inputbox" type="text" name="email" id="email" size="32" maxlength="250" value="<?php echo $item->params->get('email'); ?>" />
			<label for="email">
				<?php echo JText::_( 'Email' ); ?>
			</label>
		</td>
	</tr>		
	<?php endif; ?>
		
	<!-- URL -->	
	<?php if( $params->get('edit_url') ): ?>
	<tr>
		<td align="left">
			<input class="inputbox" type="text" name="url" id="url" size="32" maxlength="250" value="<?php echo $item->params->get('url'); ?>" />
			<label for="url">
				<?php echo JText::_( 'URL' ); ?>
			</label>
		</td>
	</tr>		
	<?php endif; ?>
	

	<!-- Comment -->
	<tr>
		<td align="left">
			<textarea class="inputbox" cols="64" rows="5" name="comment" id="comment"><?php echo $item->comment; ?></textarea>
		</td>
	</tr>

	<!-- Button + captcha -->
	<tr>
		<td align="left">
			<div class="butons" align="left">
				<?php if( $params->get('use_captcha')): ?>	
					<img src='index.php?option=com_unicomments&task=displayCaptcha' />
					<input type="text" class="inputbox" name="word" id="word" size="10" />
					<input type="hidden" name="use_captcha" value="1" />				
				<?php endif; ?>	
				<input class="button" type="submit" id="save" name="save" value="<?php echo JText::_('Save'); ?>" >
			</div>
		</td>
	</tr>	
	</table>

</div>

	<input type="hidden" name="target" value="<?php echo $target; ?>" />
	<input type="hidden" name="published" value="<?php echo $item->published; ?>" />
	<input type="hidden" name="section" value="<?php echo $item->section; ?>" />
	<input type="hidden" name="content_id" value="<?php echo $item->content_id; ?>" />
	<input type="hidden" name="id" value="<?php echo $item->id; ?>" />
	<input type="hidden" name="userid" value="<?php echo $item->userid; ?>" />
	<input type="hidden" name="task" value="save" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>