<?php 
/**
 * Media Model for JMultimedia Component
 * 
 * @package    		JMultimedia Suite
 * @subpackage 	Components
 * @link				http://3den.org
 * @license		GNU/GPL
 */
defined('_JEXEC') or die('Restricted access'); // no direct access

// Set template vars
$action = JRoute::_('index.php?option=com_comments&view=comment');
$uri =& JFactory::getURI();	
$target = $uri->toString();
?>

<script language="javascript" type="text/javascript">

</script>
<style>
#commentForm .inputbox{
	width: 100%;
}	
	
</style>

<form enctype="multipart/form-data" action="<?php echo $action; ?>" method="post" name="commentForm" id="commentForm">
<div class="comment">
	<h3><?php echo JText::_('Write your comment'); ?></h3>
	<table class="commenttable" width="100%">
		<?php // Title
			if( $params->get('edit_title') ){
		?>		
		<tr>
			<td width="150" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'Title' ); ?>:
				</label>
			</td>
			<td>
				<input class="inputbox" type="text" name="title" id="title" maxlength="250" value="<?php echo $item->title;?>" />
			</td>
		</tr>
		<?php } ?>

<!-- Optional Params -->	
		<?php // Author Alias
			if( $params->get('edit_author') ){
		?>
		<tr>
			<td align="right" class="key">
				<label for="author">
					<?php echo JText::_( 'Author' ); ?>:
				</label>
			</td>
			<td>
				<input class="inputbox" type="text" name="author" id="author" maxlength="250" value="<?php echo $item->params->get('author'); ?>" />
			</td>
		</tr>
		<?php } ?>

		<?php	// Email
			if( $params->get('edit_email') ){
		?>
		<tr>
			<td align="right" class="key">
				<label for="email">
					<?php echo JText::_( 'Email' ); ?>:
				</label>
			</td>
			<td>
				<input class="inputbox" type="text" name="email" id="email" maxlength="250" value="<?php echo $item->params->get('email'); ?>" />
			</td>
		</tr>		
		<?php } ?>
		
		<?php	// URL 
			if( $params->get('edit_url') ){
		?>
		<tr>
			<td align="right" class="key">
				<label for="url">
					<?php echo JText::_( 'URL' ); ?>:
				</label>
			</td>
			<td>
				<input class="inputbox" type="text" name="url" id="url" maxlength="250" value="<?php echo $item->params->get('url'); ?>" />
			</td>
		</tr>		
		<?php } ?>

		<!-- Comment -->
		<tr>
			<td width="150" valign="center" align="right" class="key">
				<label for="comment">
					<?php echo JText::_( 'Message' ); ?>: 
				</label>
			</td>
			<td>
				<textarea class="inputbox" rows="5" name="comment" id="comment"><?php echo $item->comment; ?></textarea>
			</td>
		</tr>
		
		<!-- captcha -->
		<?php if( $params->get('use_captcha')): ?>
		<tr>
			<td width="150" valign="center" align="right" class="key">
				<label for="word">
					<img src='index.php?option=com_unicomments&task=displayCaptcha' />
				</label>
			</td>
			<td>
				<input type="text" class="input" name="word" id="word" size="10" />
				<input type="hidden" name="use_captcha" value="1" />
			</td>
		</tr>
		<?php endif; ?>	
		
		<tr>
			<td width="150" >&nbsp; </td>
			<td>
			<div class="butons">				
				<input class="button" type="submit" id="save" name="save" value="<?php echo JText::_('Save'); ?>" >
			</div>
			</td>
		</tr>	
	
	</table>		
	</fieldset>

</div>



	<input type="hidden" name="target" value="<?php echo $target; ?>" />
	<input type="hidden" name="published" value="<?php echo $item->published; ?>" />
	<input type="hidden" name="section" value="<?php echo $item->section; ?>" />
	<input type="hidden" name="content_id" value="<?php echo $item->content_id; ?>" />
	<input type="hidden" name="id" value="<?php echo $item->id; ?>" />
	<input type="hidden" name="userid" value="<?php echo $item->userid; ?>" />
	<input type="hidden" name="task" value="save" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>