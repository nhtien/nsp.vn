<?php
/**
 * Comment view for Comments Component
 * 
 * @package    Joomla.Tutorials
 * @subpackage Components
 * @link http://dev.joomla.org/component/option,com_jd-wiki/Itemid,31/id,tutorials:components/
 * @license		GNU/GPL
 */
defined('_JEXEC') or die( 'Restricted access' );
jimport( 'joomla.application.component.view');

class CommentsViewComment extends JView
{

	function display( $tpl = null)
	{
		$model = $this->getModel('comment');	
 		$item = $model->getData();
		
		$this->assignRef('item', $item);
				
		parent::display($tpl);
	}
	
}