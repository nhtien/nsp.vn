<?php
/**
* @version		$Id: helper.php 9764 2007-12-30 07:48:11Z ircmaxell $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
// no direct access
defined('_JEXEC') or die('Restricted access');

// Define Constants
define('PATH_COM_COMMENTS', JPATH_SITE.DS.'components'.DS.'com_comments'.DS);	
define('URI_COM_COMMENTS', JURI::base().'components/com_comments/');

/**
 * Comments Helper class very use
 * @return 
 * @param $layout Object[optional]
 * @param $params Object[optional]
 */	
class CommentsHelper
{
	/**
	 * display Comments List
	 * 
	 * @return String
	 * @param $code String
	 */
	function showList(&$row, $params=null)
	{

		// set variables
		$layout = $params->get('list_layout');		
		$limit = $params->get('count');		
		
		// layout vars
		$cotid = $row->id;
		$items = CommentsHelper::getList($cotid, $limit);
		$pagination = CommentsHelper::getPagination($cotid, $limit);
		
		// display	
		include(PATH_COM_COMMENTS.'views'.DS.'comments'.DS.'tmpl'.DS. $layout .'.php' );
	}
	
	/**
	 * display Comment Form
	 * 
	 * @return String
	 * @param $code String
	 */
	function showForm(&$row, $params=null)
	{

		// set variables
		$layout = $params->get('formlayout');
		$item = CommentsHelper::_initData();
		$item->content_id = $row->id;	
		
		// display
		include( PATH_COM_COMMENTS.'views'.DS.'comment'.DS.'tmpl'.DS.$layout .'.php' );
	}
	
	/**
	 * load Comment Form
	 * 
	 * @return String
	 * @param $code String
	 */
	function showLink(&$row, $link, $params=null){

		// set variables
		$layout = $params->get('link_layout');
		$count = (int)CommentsHelper::getTotal($row->id);
		$link .= "#COMMENTS_TOP";
		
		include( PATH_COM_COMMENTS.'views'.DS.'links'.DS.'tmpl'.DS.$layout.'.php' );
	}
	
	
/* ************************************************************************** */	

	/**
	 * Get list of comments
	 * 
	 * @static
	 * @return array Medias list
	 * @param JParameter $params Module parameters\
	 */
	function getList($cotid=0, $limit=10){
		global $option, $mainframe ;
		$db =& JFactory::getDBO();
							

		// Get the pagination request variables
		$limitstart = JRequest::getInt('limitstart', 0);
			
		// In case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		
		// Switch Mode / Order by
		$order = '1';
		switch($order){
			default:
			case 'newer':
				$orderby = ' ORDER BY added DESC ';
				break;					
		}
		
		// Get Content Where
		$where = CommentsHelper::_buildContentWhere($cotid);
		
		// Return rows 
		$query = CommentsHelper::_buildQuery(
			$where, 
			$orderby, 
			$limit,
			$limitstart );

			 
		$db->setQuery($query);
		return $db->loadObjectList();		
	}
	
	function getTotal($cotid){
		$db = JFactory::getDBO();
		$query = 'SELECT COUNT(id) FROM #__comments AS a '
			. CommentsHelper::_buildContentWhere($cotid);
		$db->setQuery($query);
		$total = $db->loadResult();

		return $total;
	}
	
	/**
	 * get pagination values
	 * @return 
	 * @param $cotid Object
	 */
	function getPagination($cotid, $limit=10)
	{
		global $mainframe;
				
		// Get the pagination request variables
		$limitstart = JRequest::getInt('limitstart', 0);
		
		// Lets load the content if it doesn't already exist
		jimport('joomla.html.pagination');
		$pagination = new JPagination( 
			CommentsHelper::getTotal($cotid), 
			$limitstart, 
			$limit );
	
		return $pagination;
	}
	
		
	/**
	 * Return parameters object
	 * 
	 * @return JParameter 
	 * @param $parameters Object[optional]
	 */
	function &getParams(&$config=array()){
		// Get default params
		$params =& getCommentsParams();
		
		// merge params
		$params->merge($config);

		return $params;	
	}
	
	
	/**
	 * Build List Query
	 * 
	 * @access private
	 * @return string Query
	 * @param string $where SQL query
	 * @param string $orderby SQL query
	 * @param int $limit Items max count
	 */	
	function _buildQuery($where, $orderby, $limit, $limitstart=0){
		
		$query = ' SELECT a.id AS id, a.content_id AS content_id, a.userid AS userid, '
				. ' a.section AS section, a.ip AS ip, '
				. ' a.title AS title, a.comment AS comment, '
				. ' a.hits AS hits, a.added AS added, '
				. ' a.published AS published, a.params AS params, ' 
				. ' a.checked_out AS checked_out, a.checked_out_time AS checked_out_time, '
				. ' u.name AS author '
			. ' FROM #__comments AS a '
			. ' LEFT JOIN #__users AS u ON u.id = a.userid '
			. $where .' '. $orderby
			. ' LIMIT '. $limitstart .','. $limit;

		return $query;
	}
	
	/**
	 * Build list Where
	 * @return 
	 */
	function _buildContentWhere($cotid, $section=''){
		global $mainframe, $option;
		
		$db =& JFactory::getDBO();
		
		($section) or $section = JRequest::getVar('option');
		
		$where = array();		
		$where[] = 'a.section = '. $db->Quote($section);		
		$where[] = 'a.content_id = '. (int) $cotid;
		$where[] = 'a.published = 1';
		
		// build where
		$where = ' WHERE '. implode( ' AND ', $where );
		
		return $where;
	}
	
	/**
	 * create new item
	 * 
	 * @return 
	 */
	function _initData(){
		global $option;
		$user =& JFactory::getUser();
		$item = new stdClass();
		$item->id = null;
		/** @var string user IP */
		$item->ip = null;
		/** @var int component foreign key */
		$item->section = $option;
		/** @var int categories foreign key */
		$item->content_id = null;
		/** @var int users foreign key */
		$item->userid = $user->get('id');
		
		/** @var string title */
		$item->title = null;
		/** @var string alias */
		$item->comment= null;
		/** @var datetime date */
		$item->added = null;
		/** @var string author */
		$item->author = null;
				
		/** @var int hits */
		$item->hits = 0;
		/** @var int checked by */
		$item->checked_out = 0;
		/** @var datetime checked time */
		$item->checked_out_time = 0;
		/** @var boolean True on published */
		$item->published = 1;
		/** @var string commenr parameters */
		$item->params = new JParameter(null);
		
		return $item;
	}
}