<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.controller' ); 

class NspVoucherController extends JController 
{
	function display() 
	{	
		$document =& JFactory::getDocument(); 
		$viewName = JRequest::getVar('view', 'default');
		$viewType = $document->getType(); 
		$view = &$this->getView($viewName, $viewType, 'NspVoucherView'); 
		$model =& $this->getModel( $viewName, 'NspVoucherModel' ); 
		if (!JError::isError( $model )) { 
			$view->setModel( $model, true ); 
		}

		$view->setLayout('default'); 
		$view->display();
	}
	function submitcode()
	{
		global $Itemid,$mainframe;
		$db = JFactory::getDBO();
		$user = &JFactory::getUser();
		$session = JFactory::getSession();
		$code = strtoupper(JRequest::getString('code','','post'));
		$link = 'index.php?option=com_nspvoucher&Itemid=129';
		JRequest::checkToken() or jexit( 'Invalid Token' );
		if(!$user->id){
			$mainframe->redirect($link,'Vui lòng đăng nhập','error');
			return;
		}
		if(!$code){
			$mainframe->redirect($link,'Nhập mã số voucher','error');
			return;
		}
		$sql = 'SELECT * FROM #__nspvoucher where code = '.$db->Quote($code);
		$db->setQuery($sql);
		$row = $db->loadObject();
		if( !$row->published || !$row->id){
			$count = $session->get('_countCode',1 ,'voucher');
			if($count >= 10){
				$mainframe->logout();
				$session->set('_countCode',1 ,'voucher');
				$mainframe->redirect('index.php', 'Bạn đã vượt quá số lần nhập cho phép, vui lòng đăng nhập để thử lại', 'error');
				return;
			}
			$session->set('_countCode',$count+1 ,'voucher');
			$mainframe->redirect($link, 'Mã số không hợp lệ, vui lòng thử lại', 'error');
			return;
		}
		
		if($row->checked){
			$mainframe->redirect($link,'Mã số này đã được sử dụng, vui lòng thử lại mã số khác','error');
			return;
		}
		
		$sql = 'SELECT * FROM #__nspvoucher_user_ref where voucher_code = '.$db->Quote($code);
		$db->setQuery($sql);
		$row2 = $db->loadObject();
		if( $row2->id){
			$mainframe->redirect($link,'Mã số đã được sử dụng, vui lòng thử lại','error');
			return;
		}
		
		$voucher = &JTable::getInstance('voucher_user_ref','Table');
		$voucher->id			= 0;
		$voucher->created 		=  	date("Y-m-d H:i:s");
		$voucher->voucher_id	= $row->id;
		$voucher->voucher_code	= $row->code;
		$voucher->voucher_value	= $row->value;
		$voucher->user_id		= $user->id;
		$voucher->username		= $user->username;
		
		if( !$voucher->store() ){
			JError::raiseError( 500, $row->getError() );
		}
		$sql = 'UPDATE #__nspvoucher SET checked = 1 where code = '.$db->Quote($code) .' LIMIT 1';
		$db->setQuery($sql);
		$db->query();
		
		$sql = 'SELECT * FROM #__nspvoucher_account where user_id = '.$user->get('id');;
		$db->setQuery($sql);
		$row3 = $db->loadObject();
		if($row3->user_id){
			$account = &JTable::getInstance('account','Table');
			$account->load($user->id);
			$account->total_value = $account->total_value + $row->value;
			if( !$account->store() ){
				JError::raiseError( 500, $row->getError() );
			}
		}else
		{
			$sql = 'INSERT INTO #__nspvoucher_account (
					 `user_id` ,
					 `total_value` 
					)
					VALUES (
					 '.$user->get('id').','.$row->value.'
					);
					';
			$db->setQuery($sql);
			$db->query();
		}
		

		$msg = 'Tài khoản của bạn đã được cộng thêm '.vpriceFormat($row->value);
		$session->set('_countCode',1 ,'voucher');
		$mainframe->redirect($link,$msg);
	}
	// cart function
	function addtocart()
	{
		global $mainframe;
		$user = &JFactory::getUser();
		if(!$user->id){
			$mainframe->redirect('index.php?option=com_nspvoucher','Vui lòng đăng nhập trước khi bạn tham gia mua sản phẩm','error');
			return;
		}
		$model =& $this->getModel( 'default', 'NspVoucherModel' );
		$value = $model->getUserValue($user->id);
		 
		require_once(JPATH_COMPONENT . DS . 'libraries' . DS . 'cart.php');
		$pid = JRequest::getInt('pid',0,'post');
		$quantity = max(JRequest::getInt('quantity',1,'post'),1);
		$product = &JTable::getInstance('product','Table');
		$product->load( $pid );

		$cart = new Cart();
		$products = $cart->products;
		$total_price = 0;
		for($i=0 ; $i<count($products); $i++)
		{
			$p = $products[$i];
			$total_price += ($p->quantity*$p->price);
		}
		$total_price +=  ($product->price * $quantity);
		if($total_price > $value->total_value){
			$mainframe->redirect('index.php?option=com_nspvoucher','Số tiền trong tài khoản của bạn không đủ để mua sản phẩm, vui lòng nạp mã số trong phiếu khuyến mãi.','error');
			return;
		}
		$cart->add($product->id, $product->code, $product->name, $product->price, $quantity, $product->thumb_url, $link);
		$mainframe->redirect('index.php?option=com_nspvoucher&task=cart','Sản phẩm đã thêm vào giỏ hàng thành công');
	}
	
	function updatecart()
	{
		global $mainframe;
		$user = &JFactory::getUser();
		require_once(JPATH_COMPONENT . DS . 'libraries' . DS . 'cart.php');
		$pid = JRequest::getInt('pid');
		$quantity = (int)JRequest::getInt('quantity',1);
		$cart = new Cart();
		$cart->update($pid, $quantity);
		$mainframe->redirect('index.php?option=com_nspvoucher&task=cart','Cập nhật số lượng sản phẩm thành công');
	}
	
	function removefromcart()
	{
		global $mainframe;
		$user = &JFactory::getUser();
		require_once(JPATH_COMPONENT . DS . 'libraries' . DS . 'cart.php');
		$pid = JRequest::getInt('pid');
		$cart = new Cart();
		$cart->remove($pid);
		$mainframe->redirect('index.php?option=com_nspvoucher&task=cart','Xóa sản phẩm ra khỏi giỏ hàng thành công');
	}
	
	function cart()
	{
		global $mainframe;
		$user = &JFactory::getUser();
		if(!$user->id){
			$mainframe->redirect('index.php?option=com_nspvoucher','Vui lòng đăng nhập trước khi bạn tham gia mua sản phẩm','error');
			return;
		}
		
		$model =& $this->getModel( 'default', 'NspVoucherModel' );
		$value = $model->getUserValue($user->id);
		
		require_once(JPATH_COMPONENT . DS . 'libraries' . DS . 'cart.php');
		$cart = new Cart();
		$products = $cart->products;
		
		$tmpl = new NspVoucherTemplate('cart');
		$tmpl->set('products', $products );
		$tmpl->set('cart', $cart);
		$tmpl->set('value', $value);
		$tmpl->set('cmn', $cmn);
		
		$tmpl->display();
	}
	
	function checkout()
	{
		global $mainframe;
		$user = &JFactory::getUser();
		require_once(JPATH_COMPONENT . DS . 'libraries' . DS . 'cart.php');
		
		$model =& $this->getModel( 'default', 'NspVoucherModel' );
		$value = $model->getUserValue($user->id);
		
		$cart = new Cart();
		$products = $cart->products;
		
		$total_price = 0;
		for($i=0 ; $i<count($products); $i++)
		{
			$p = $products[$i];
			$total_price += ($p->quantity*$p->price);
		}
		
		if($total_price > $value->total_value){
			$mainframe->redirect('index.php?option=com_nspvoucher&task=cart','Số tiền trong tài khoản của bạn không đủ để mua sản phẩm, vui lòng nạp mã số trong phiếu khuyến mãi.','error');
			return;
		}

		$tmpl = new NspVoucherTemplate('checkout');
		$tmpl->set('products', $products );
		$tmpl->set('cart', $cart);
		$tmpl->set('user', $user);
		$tmpl->display();
	}
	function save_cart()
	{
		require_once(JPATH_COMPONENT . DS . 'libraries' . DS . 'cart.php');
		global $mainframe, $nspVoucher_config;
		$jconfig = new jconfig();
		$post = JRequest::get('post');
		$user = &JFactory::getUser();
		$db = &JFactory::getDBO();
		
		if(!$user->id){
			$mainframe->redirect('index.php?option=com_nspvoucher','Vui lòng đăng nhập trước khi bạn tham gia mua sản phẩm','error');
			return;
		}
		
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model =& $this->getModel( 'default', 'NspVoucherModel' );
		$value = $model->getUserValue($user->id);
		
		$cart = new Cart();
		$products = $cart->products;
		
		if(!count($products)){
			$mainframe->redirect('index.php?option=com_nspvoucher&task=products','Giỏ hàng của bạn trống.','error');
			return;
		}
		$total_price = 0;
		for($i=0 ; $i<count($products); $i++)
		{
			$p = $products[$i];
			$total_price += ($p->quantity*$p->price);
		}
		
		if($total_price > $value->total_value){
			$mainframe->redirect('index.php?option=com_nspvoucher&task=cart','Số tiền trong tài khoản của bạn không đủ để mua sản phẩm, vui lòng nạp mã số trong phiếu khuyến mãi.','error');
			return;
		}
		$order =& JTable::getInstance('order', 'Table');
		if (!$order->bind( $post )) {
			JError::raiseError(500, $row->getError() );
		}
		$datenow =& JFactory::getDate();
		$order->id 		= 0;
		$order->total 	= $total_price;
		$order->status	= 0;
		$order->user_id	= $user->id;
		$order->created	= $datenow->toMySQL();
		$order->user_crvalue = $value->total_value;
		
		if (!$order->store()) {
			JError::raiseError(500, $row->getError() );
		}
		
		$product =& JTable::getInstance('order_product', 'Table');
		foreach($products as $p){
			$product->id = 0;
			$product->order_id = $order->id;
			$product->product_id	= $p->id;
			$product->product_name	= $p->name;
			$product->product_price	= $p->price;
			$product->quantity		= $p->quantity;
			$product->created		= $datenow->toMySQL();
			$product->store();
		}
		reset($products);
		if($nspVoucher_config->sendmail2admin){
			$tmpl = new NspVoucherTemplate('cart.mail.html');
			$tmpl->set('order', $order);
			$tmpl->set('products', $products);
			$content = $tmpl->fetch();
			//$recipient	= $nspVoucher_config->sendmailto;
			$recipient	= explode(",",$nspVoucher_config->sendmailto);
			
			//jimport('joomla.mail.mail');
			//$mail = new JMail();
			$mail = JFactory::getMailer();
			$mail->ContentType = 'text/html';
			$mail->setSubject('[NSP] - Don hang - '.$order->name);
			$mail->setSender(array($jconfig->mailfrom, $jconfig->sitename));
			$mail->FromName = $jconfig->sitename;
			foreach($recipient as $m) {
				$mail->addRecipient($m);
			}
			//$mail->addRecipient($recipient);
			//$mail->addRecipient('sangtran@nsp.com.vn');
			//$mail->addCC('sangtran@nsp.com.vn');
			
			//$mail->ReplyTo = "";
			$mail->setBody($content);
			$mail->send(); //var_dump($mail);exit;
		}
		
		$cart->removeAll();
		
		$sql = 'UPDATE #__nspvoucher_account SET total_value = total_value - '.$total_price .' where user_id = '.$user->id.' LIMIT 1';
		$db->setQuery($sql);
		$db->query();
		
		
		$mainframe->redirect('index.php?option=com_nspvoucher','Đơn hàng của bạn đã được gửi thành công, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất.');
	}
}
?>