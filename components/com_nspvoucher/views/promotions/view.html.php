<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

class NspVoucherViewPromotions extends JView
{
	function display($tpl = null)
	{
		global $nspVoucher_config;
		$user = &JFactory::getUser();
		$doc = &JFactory::getDocument();
		$model =& $this->getModel();
		//$doc->setTitle();
		$doc->setDescription('Chuyên trang khuyến mãi - các chương trình khuyến mãi của công ty NSP');
		//$doc->setMetaData('keywords',$nspVoucher_config->meta_key);
		$promotions = $model->getPromotions();
		$this->assignRef('promotions', $promotions);
		parent::display($tpl);
	}
}

?>


