<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<h1 class="componentheading">Khuyến mãi</h1>
<div style="width:635px; margin:0 auto;">
  <table cellpadding="5" cellspacing="0" border="0" width="100%">
    <tr>
	<?php
	$i = 1;
    foreach($this->promotions as $p){
		$thumb = 'documents/nspvoucher/promotions/'.($p->thumb ? $p->thumb : 'none_thumb.jpg');
		$url = $p->url ? $p->url : 'index.php?option=com_nspvoucher&view=promotion&pid='.$p->id.'&itemid=131' ;
		?>
    	<td align="center"><a href="<?php echo $url?>" title="<?php echo $p->title?>" target="_blank"><img src="<?php echo $thumb?>" alt="<?php echo $p->title?>" border="0" /></a></td>
    <?php 
	if($i%2 == 0) echo '</tr><tr>';
	$i++;
	} ?>
 </tr>
 </table>
</div>
