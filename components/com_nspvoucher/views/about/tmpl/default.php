<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
global $Itemid;
?>
<h1 class="componentheading"><?php echo JText::_('NT_TRAINING_TITLE')?></h1>
<div class="view-nsptraining-about">
	<p align="justify"><?php echo JText::_('NT_ABOUT_INFOMATION');?></p>
  <h2 align="center">LỊCH KHAI GIẢNG CÁC KHÓA HỌC TRONG NĂM 2012</h2>  
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="n-border">
  <tr class="title">
    <td>Khóa đào tạo</td>
    <td>Khai giảng</td>
    <td>Thời lượng</td>
    <td>Giờ học</td>
    <td>Học phí</td>
  </tr>
  <tr>
    <td><strong>AMP ACT 1</strong><br />
    <a href="index.php?&option=com_nsptraining&view=register&catid=1&cid=1&Itemid=17">Đăng ký online</a></td>
    <td align="center">23/02<br />      
      16/04<br />
      21/06<br />
    15/11   <br /></td>
    <td align="center">2 ngày</td>
    <td align="center">08:30 - 17:00</td>
    <td align="center">3.150.000 đ</td>
  </tr>
  <tr>
    <td><strong>AMP ACT 2</strong><br />
    <a href="index.php?&option=com_nsptraining&view=register&catid=1&cid=3&Itemid=17">Đăng ký online</a></td>
    <td align="center">05/2012 (dự kiến)<br />      
    23/08      <br /></td>
    <td align="center">2 ngày</td>
    <td align="center">08:30 - 17:00</td>
    <td align="center">3.150.000 đ</td>
  </tr>
  <tr>
    <td><strong>AMP ACT 3</strong><br />
    <a href="index.php?&option=com_nsptraining&view=register&catid=1&cid=4&Itemid=17">Đăng ký online</a></td>
    <td align="center">05/2012 (dự kiến)<br />      
    23/08      <br /></td>
    <td align="center">2 ngày</td>
    <td align="center">08:30 - 17:00</td>
    <td align="center">3.150.000 đ</td>
  </tr>
  <tr>
    <td><strong>AMP ACT 4</strong><br />
    <a href="index.php?&option=com_nsptraining&view=register&catid=1&cid=5&Itemid=17">Đăng ký online</a></td>
    <td align="center">Cập nhật sau</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">3.150.000 đ</td>
  </tr>
  <tr>
    <td><strong>AMP ACT 5</strong><br />
    <a href="index.php?&option=com_nsptraining&view=register&catid=1&cid=6&Itemid=17">Đăng ký online</a></td>
    <td align="center">Cập nhật sau</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">3.150.000 đ</td>
  </tr>
  <tr>
    <td><strong>AMP ACT 6</strong><br />
    <a href="index.php?&option=com_nsptraining&view=register&catid=1&cid=7&Itemid=17">Đăng ký online</a></td>
    <td align="center">Cập nhật sau</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">3.150.000 đ</td>
  </tr>
  <tr>
    <td><strong>CCTT</strong><br />
    <a href="index.php?option=com_nsptraining&view=register&catid=2&cid=8&Itemid=17">Đăng ký online</a></td>
    <td align="center">23/05<br />
      27/09</td>
    <td align="center">2 ngày</td>
    <td align="center">09:00 - 17:30</td>
    <td align="center">4.180.000 đ</td>
  </tr>
</table>
    
    
    
	<P><?php echo JText::_('NT_COURSES');?></P>
	<?php
		//echo '<ul class="arrow-orange"><li><a href="index.php?option=com_nsptraining&view=ampact1">AMP ACT 1 - Installing Premises Cabling System</a></li></ul>';
		foreach( $this->data as $d){
			echo '<h4> From '.$d->name.'</h4>';
			if( $d->courses ){
				echo '<ul class="arrow-orange">';
				
				foreach( $d->courses as $r){
					echo '<li><a href="index.php?option=com_nsptraining&view=course&cid='.$r->id.'&Itemid='.$Itemid.'" title="'.$r->name.'">'.$r->name.'</a></li>';
				}
				echo '</ul>';
			}
		}
	?>
</div>
<!-- view -->