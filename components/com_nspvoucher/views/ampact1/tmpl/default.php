<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
global $Itemid;
$link = NRoute::_('index.php?option=com_nsptraining');
JHTML::_('behavior.formvalidation' );
?>
<p><strong>Khóa 1 </strong><a name="1"></a><br />Khóa đào tạo kỹ thuật AMPACT 1 (Lắp đặt hệ thống cáp mạng)</p>
<p align="justify">Học phí: Xin mời liên hệ các <a href="http://www.ampnetconnect.com.vn/distributor.php">Nhà Phân Phối của AMP NETCONNECT</a> để được huớng dẫn đăng ký. <br /><br />Địa điểm học:<br />   + Thành phố Hồ Chí Minh: Tòa nhà ACBR Building, Lầu 4, số 249, đường Cộng Hòa, Quận Tân Bình, Thành phố Hồ Chí Minh, Điện thoại : 84-8 62966992 Fax : 84-8 62966993<br />     + Hà nội: Toà nhà HITTC Tower, P 507, lầu5, số 185 Giảng Võ, Hà Nội, Điện thoại : 84-4 35122801/802 Fax : 84-4 35122803</p>
<p align="justify">Thời gian khóa học: 2 ngày<br />Giờ: 09:00-16:00 vào các ngày làm việc trong tuần<br />Thời khóa biểu: Xin xem chi tiết dưới đây</p>
<p align="justify"><strong>Nội dung:</strong></p>
<p align="justify"><strong>Ngày 1 </strong></p>
<p align="justify"><strong>Phần Giới Thiệu</strong></p>
<ul>
<li>Tổng Quan về Khóa Học : Giới thiệu Tyco Electronics, hệ thống chứng chỉ,... </li>
<li>Làm quen các tiêu chuẩn : Giới thiệu hệ thống kết nối cáp, các tiêu chuẩn cho hệ thống </li>
<li>7 yếu tố của chuẩn TIA/EIA-568B, các chỉ định của TIA/EIA &amp; ISO : loại cáp, loại đầu nối &amp; khoảng cách kết nối. </li>
<li>Các loại cáp hiện hành : Cáp đồng đôi xoắn , cáp quang , cấu trúc, thuật ngữ, phân loại,.. </li>
</ul>
<p align="justify"><strong>Phần Hoạch Định &amp; Chuẩn Bị Thi Công</strong></p>
<ul>
<li>Các Bước Hoạch Định &amp; Chuẩn Bị Thi Công : Phạm vi công việc, bản vẽ, dự toán vật tư,.. </li>
<li>Các Vật Tư Phụ cho Khu Vực Làm Việc ( Work Area ): Các loại đế và Outlet </li>
<li>Lắp Đặt Vật Tư &amp; Các Thiết Bị Phụ Trợ : Rack &amp; các thiết bị treo tường, </li>
</ul>
<p align="justify"><strong>Phần Triển Khai Lắp Đặt</strong></p>
<ul>
Các chỉ định lắp đặt cáp đôi xoắn : Các thông số kiểm định và các yếu tố ảnh hưởng đến thông số kiểm định.
<li>Các chỉ định lắp đặt cáp sợi quang : Độ Mất Tín Hiệu (Fiber Loss) &amp; các yếu tố ảnh hưởng đến độ mất tín hiệu </li>
<li>Các ghi chú trước khi thi công : Quản lý cáp, cáp chùng dự phòng, cách buộc dây mồi,.. </li>
<li>Lắp đặt hệ thống ống máng đi cáp </li>
<li>Lắp đặt cáp trục chính (Backbone) </li>
</ul>
<p align="justify"><strong>Ngày 2 </strong></p>
<p align="justify"><strong>Kỹ Thuật Bấm Đầu </strong></p>
<div>
<ul>
<li>Cáp đôi xoắn : Phương pháp IDC, Mã màu , Nguyên tắc bấm đầu </li>
<li>Thực hành bấm đầu cáp đồng : RJ45 Cat 5e (UTP, FTP), Cat 6 SL cùng các modular tương ứng,.. </li>
<li>Cáp Quang : Các phương pháp bấm đầu cáp quang như : Epoxy, LightCrimp, LightCrimp Plus </li>
<li>Thực hành bấm đầu cáp quang : ST, SC, LightCrimp, LightCrimp Phus trên cáp ngoài trời, cáp trong nhà. </li>
</ul>
</div>
<p align="justify"><strong>Thi cuối khóa:</strong> Học viên nào đạt kết quả cao trong kỳ thi cuối khóa sẽ được cấp giấy chứng nhận là “Nhà lắp đặt hệ thống chính thức của AMP NETCONNECT”. <a href="http://www.ampnetconnect.com.vn/images/Training/cer-ampact1.jpg">Bấm vào đây</a> để xem mẫu chứng nhận.</p>
<h3> Đăng ký</h3>
<p><?php echo JText::_('NT_TRAINING_REQUIRED_FIELD');?><p>
<form action="index.php" method="post" class="form-validate">
<table width="auto" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><label for="msgcomname">Tên công ty (*) </label></td>
    <td><input name="name" type="text" value="<?php echo $row->name?>" class="inputbox required" id="msgcomname"  size="50"/></td>
  </tr>
  <tr>
    <td><label for="msgaddress">Địa chỉ (*) </label></td>
    <td><input name="address" type="text" value="<?php echo $row->address?>" class="inputbox required" id="msgaddress" size="50" /></td>
  </tr>
  <tr>
    <td><label for="msgname">Tel (*)</label></td>
    <td><input name="phone" type="text" value="<?php echo $row->phone?>" class="inputbox required" id="msgname" /></td>
  </tr>
  <tr>
    <td>Fax</td>
    <td><input name="fax" type="text" value="<?php echo $row->fax?>" class="inputbox" /></td>
  </tr>
  <tr>
    <td><label for="msgemail">Email (*)</label></td>
    <td><input name="mail" type="text" value="<?php echo $row->mail?>" class="inputbox required validate-email" id="msgemail" /></td>
  </tr>
</table>
<p> Danh sách tham dự:</p>
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="n-border">
  <tr class="title">
    <td>STT</td>
    <td>Họ Tên </td>
    <td>Chức vụ </td>
    <td>Điện thoại </td>
    <td>Email</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td align="center"><input name="name_reg[]" type="text" value="" class="inputbox" size="15" /></td>
    <td align="center"><input name="chucvu_reg[]" type="text" value="" class="inputbox" size="10" /></td>
    <td align="center"><input name="phone_reg[]" type="text" value="" class="inputbox" size="15" /></td>
    <td align="center"><input name="email_reg[]" type="text" value="" class="inputbox" size="15" /></td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td align="center"><input name="name_reg[]" type="text" value="" class="inputbox" size="15" /></td>
    <td align="center"><input name="chucvu_reg[]" type="text" value="" class="inputbox" size="10" /></td>
    <td align="center"><input name="phone_reg[]" type="text" value="" class="inputbox" size="15" /></td>
    <td align="center"><input name="email_reg[]" type="text" value="" class="inputbox" size="15" /></td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td align="center"><input name="name_reg[]" type="text" value="" class="inputbox" size="15" /></td>
    <td align="center"><input name="chucvu_reg[]" type="text" value="" class="inputbox" size="10" /></td>
    <td align="center"><input name="phone_reg[]" type="text" value="" class="inputbox" size="15" /></td>
    <td align="center"><input name="email_reg[]" type="text" value="" class="inputbox" size="15" /></td>
  </tr>
</table>
<p>Khi cần xin liên hệ với Anh/ chị: <input name="" type="text" class="inputbox" /> Email: <input name="" type="text" class="inputbox" /></p>
<?php $mainframe->triggerEvent('onShowOSOLCaptcha', array(false)); ?>
<br />
<input type="submit" name="s" value="<?php echo JText::_('Send Register')?>" class="button validate" />
<input type="button" name="back" value="<?php echo JText::_('Cancel Register')?>" class="button" onclick="window.location.href='<?php echo $link?>';" />
<input type="hidden" name="option" value="com_nsptraining" />
<input type="hidden" name="task" value="save_regcourse" />
<?php echo JHTML::_( 'form.token' ); ?>

</form>