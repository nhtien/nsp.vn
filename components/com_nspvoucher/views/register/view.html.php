<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

class NspTrainingViewRegister extends JView
{
	function display($tpl = null)
	{
		$layout = JRequest::getVar('layout') ? JRequest::getVar('layout') : $this->getLayout();
		if( $layout !='default'){
			$this->$layout();
			return;
		}
		$catid = JRequest::getVar('catid');
		
		$model =& $this->getModel( );
		$courses = $model->getCourses($catid);
		$this->assignRef('courses', $courses);
		$this->assignRef('lists',$this->_lists() );
		parent::display($tpl);
	}
	
	function thank_you( $tpl = null){
		$this->setLayout('thank_you'); 
		parent::display($tpl);
		return;
	}
	
	function _lists(){
		include( JPATH_COMPONENT.DS.'libraries'.DS.'common.php');
		
		/* Partner Type */
		$type_option[] = JHTML::_('select.option','', JText::_('Please Select...'));
		while( list($k,$v) = each($arr_partner_type) ){
			$type_option[] = JHTML::_('select.option', $k,$v);
		}
		$lists['partner_type'] = JHTML::_('select.genericList', $type_option,'partner_type','class=""','value','text');
		/* Location */
		
		//$location_option[] = JHTML::_('select.option','', JText::_('Please Select...'));
		while( list($k,$v) = each($arr_location) ){
			$location_option[] = JHTML::_('select.option', $k,$v);
		}
		$lists['location'] = JHTML::_('select.genericList', $location_option,'location','class=""','value','text');
		
		return $lists;
	}
}

?>


