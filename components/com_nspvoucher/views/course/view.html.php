<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

class NspTrainingViewCourse extends JView
{
	function display($tpl = null)
	{
		$doc = &JFactory::getDocument();
		$model =& $this->getModel();
		$data = $model->getCourse();
		$doc->setTitle($data->name);
		$this->assignRef('data', $data);
		parent::display($tpl);
	}
}

?>


