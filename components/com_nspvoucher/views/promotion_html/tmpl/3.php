<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

?>
<h2 align="center">Danh sách  khách hàng trúng thưởng: </h2>
<h3><strong>1. Giải đặc biệt</strong>: Xe    máy Honda Airblade</h3>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="n-border">
  <tr class="title">
    <td>STT</td>
    <td>Tên khách hàng</td>
    <td>Số CMND</td>
    <td>Địa chỉ</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td align="center">............................................</td>
    <td align="center">...................................</td>
    <td align="center">...................................</td>
  </tr>
</table>
<p>&nbsp;</p>
<h3><strong>2. Giải nhất</strong>: Tivi    Samsung 32&quot;</h3>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="n-border">
  <tr class="title">
    <td>STT</td>
    <td>Tên khách hàng</td>
    <td>Số CMND</td>
    <td>Địa chỉ</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td align="center">............................................</td>
    <td align="center">...................................</td>
     <td align="center">...................................</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td align="center">............................................</td>
    <td align="center">...................................</td>
     <td align="center">...................................</td>
  </tr>
</table>

<h3><strong>3. Giải nhì</strong>: Tủ    lạnh Panasonic</h3>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="n-border">
  <tr class="title">
    <td>STT</td>
    <td>Tên khách hàng</td>
    <td>Số CMND</td>
    <td>Địa chỉ</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td align="center">............................................</td>
    <td align="center">...................................</td>
     <td align="center">...................................</td>
  </tr>
  <tr>
    <td align="center">2</td>
    <td align="center">............................................</td>
    <td align="center">...................................</td>
     <td align="center">...................................</td>
  </tr>
  <tr>
    <td align="center">3</td>
    <td align="center">............................................</td>
    <td align="center">...................................</td>
     <td align="center">...................................</td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
