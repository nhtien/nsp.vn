<h2 align="center">THỂ LỆ CHƯƠNG TRÌNH KHUYẾN MÃI</h2>
<h3 align="center" style="margin-top:0; padding-top:0;"><em>THẺ CÀO TRAO TAY - RINH NGAY QUÀ TẶNG</em></h3>
  <br />
<h3 >1. TÊN CHƯƠNG TRÌNH:</h3>
<p><strong>&ldquo;</strong><strong>THẺ CÀO TRAO TAY - RINH NGAY QUÀ TẶNG</strong><strong>&rdquo; </strong></p>
<h3>2. THỜI GIAN CHƯƠNG TRÌNH</h3>
<p>Chương trình có  hiệu lực từ ngày 01/06/2012 tới hết ngày 30/9/2012 hoặc cho tới khi hết số  lượng giải thưởng khuyến mãi. </p>
<h3>3. PHẠM VI VÀ ĐỐI TƯỢNG KHUYẾN MÃI</h3>
<p>Chương trình áp dụng cho tất cả khách hàng cuối cùng  (End-user) mua hàng tại hệ thống phân phối của công ty TNHH TM-DV Tin Học Nhân  Sinh Phúc (NSP) trong phạm vi Tp. Hồ Chí Minh.</p>
<h3>3. HÌNH THỨC</h3>
<p>Cào trúng thưởng.</p>
<h3>5. SẢN PHẨM KHUYẾN MÃI</h3>
<p>Áp dụng cho các sản phẩm   cáp đồng 305m thương hiệu AMP Netconnect:</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="n-border">
  <tr>
    <td width="71" nowrap="nowrap"><p>1427254-6</p></td>
    <td width="444"><p>AMP    Category 6 UTP Cable, 4-Pair, 23AWG, Solid, CM, 305m, Blue 
    (CABLE, CAT6, 4UTP, 23AWG, CM, 75C, BLUE)</p></td>
  </tr>
  <tr>
    <td width="71" nowrap="nowrap"><p>6-219590-2</p></td>
    <td width="444"><p>AMP    Category 5e UTP Cable (200MHz), 4-Pair, 24AWG, Solid, CM, 305m, White 
    (CA CAT5E 4UTP 24AWG CM 75C GWHT)</p></td>
  </tr>
</table>
<h3>6. ĐIỀU KIỆN THAM DỰ</h3>
<ul>
  <li>Chương trình chỉ áp dụng cho khách hàng cuối cùng  (End-user).</li>
  <li>Chương trình chỉ áp dụng cho các hóa đơn thanh toán đúng hạn.</li>
  <li>Chương trình không áp dụng cho nhân viên công ty NSP, các  hóa đơn mua hàng được hỗ trợ giá. Chương trình không áp dụng chung với các  chương trình khuyến mãi khác của công ty NSP.</li>
  
</ul>
<h3>7. GIẢI THƯỞNG VÀ CÁCH THỨC THAM DỰ</h3>
<p>Trong thời gian diễn ra chương trình khuyến mãi, với mỗi  thùng cáp đồng 305m CAT 5E và CAT 6 thương hiệu AMP Netconnect mua tại hệ thống  đại lý của công ty TNHH TM-DV Tin Học Nhân Sinh Phúc khách hàng sẽ nhận được 1  thẻ cào để có cơ hội nhận được một trong những giải thưởng hấp dẫn dưới đây: </p>
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="n-border">
  <tr class="title">
    <td width="23%">
      <strong>Giải    thưởng</strong></td>
    <td width="29%" nowrap="nowrap"><p><strong>Hạng    mục</strong></p></td>
    <td width="17%" nowrap="nowrap"><p><strong>Giá    (đồng)</strong></p></td>
    <td width="7%" nowrap="nowrap"><p><strong>SL    (cái)</strong></p></td>
    <td width="22%" nowrap="nowrap"><p><strong>Chi    phí (đồng)</strong></p></td>
  </tr>
  <tr>
    <td width="23%"><p>Giải    đặc biệt</p></td>
    <td width="29%" nowrap="nowrap"><p>Xe    máy Honda Airblade</p></td>
    <td width="17%" nowrap="nowrap"><p>38,000,000 </p></td>
    <td width="7%" nowrap="nowrap"><p>1</p></td>
    <td width="22%" nowrap="nowrap"><p>38,000,000 </p></td>
  </tr>
  <tr>
    <td width="23%"><p>Giải    nhất</p></td>
    <td width="29%" nowrap="nowrap"><p>Tivi    Samsung 32&quot;</p></td>
    <td width="17%" nowrap="nowrap"><p>8,900,000 </p></td>
    <td width="7%" nowrap="nowrap"><p>2</p></td>
    <td width="22%" nowrap="nowrap"><p>17,800,000 </p></td>
  </tr>
  <tr>
    <td width="23%"><p>Giải    nhì</p></td>
    <td width="29%" nowrap="nowrap"><p>Tủ    lạnh Panasonic</p></td>
    <td width="17%" nowrap="nowrap"><p>4,100,000 </p></td>
    <td width="7%" nowrap="nowrap"><p>3</p></td>
    <td width="22%" nowrap="nowrap"><p>12,300,000 </p></td>
  </tr>
  <tr>
    <td width="23%"><p>Giải    ba</p></td>
    <td width="29%" nowrap="nowrap"><p>Áo    thun cao cấp NSP</p></td>
    <td width="17%" nowrap="nowrap"><p>100,000 </p></td>
    <td width="7%" nowrap="nowrap"><p>200</p></td>
    <td width="22%" nowrap="nowrap"><p>20,000,000 </p></td>
  </tr>
  <tr>
    <td width="23%"><p>Giải    tư </p></td>
    <td width="29%" nowrap="nowrap"><p>Ly    thủy tinh</p></td>
    <td width="17%" nowrap="nowrap"><p>60,000 </p></td>
    <td width="7%" nowrap="nowrap"><p>500</p></td>
    <td width="22%" nowrap="nowrap"><p>30,000,000 </p></td>
  </tr>
  <tr>
    <td width="23%"><p>Giải    khuyến khích</p></td>
    <td width="29%" nowrap="nowrap"><p>Phiếu    giảm giá cáp</p></td>
    <td width="17%" nowrap="nowrap"><p>50,000 </p></td>
    <td width="7%" nowrap="nowrap"><p>1000</p></td>
    <td width="22%" nowrap="nowrap"><p>50,000,000 </p></td>
  </tr>
  <tr>
    <td width="77%" colspan="4"><p align="center"><strong>TỔNG    CỘNG</strong></p></td>
    <td width="22%" nowrap="nowrap"><p><strong>167,100,000</strong></p></td>
  </tr>
</table>
<h3>8. QUY ĐỊNH ĐỔI GIẢI THƯỞNG</h3>
<ul>
  
  <li>Tất cả  giải thưởng đều không có giá trị quy đổi thành tiền mặt.<strong> </strong></li>
  <li>Hình ảnh  trên website hoặc áp phích chỉ mang tính minh họa, quà tặng thực tế có thể khác  so với trong hình ảnh nhưng có cùng nội dung và giá trị. </li>
  <li>Khách  hàng trúng thưởng phải thanh toán tất cả mọi khoản thuế, phí, lệ phí có liên  quan đến giải thưởng theo quy định pháp luật Việt Nam hiện hành, kể cả thuế thu  nhập (nếu có) khi nhận giải. Ngoài ra, người nhận giải phải tự trả mọi khoản  phí tổn như đi lại, ăn ở, vận chuyển giải thưởng...(nếu có) cho việc nhận giải  của mình.</li>
  <li>Tất cả  các thẻ cào trúng thưởng được khách hàng đem đổi, gửi trả về cho NSP đương  nhiên trở thành tài sản của NSP.</li>
  <li>Khách  hàng trúng thưởng phải xuất trình giấy CMND (hoặc hộ chiếu) và thẻ cào trúng giải  khi làm thủ tục nhận giải. Trường hợp người khác nhận thay giải thưởng cho người  trúng thưởng thì người nhận phải xuất trình CMND (hoặc hộ chiếu), thẻ cào trúng  giải do công ty NSP phát hành và giấy ủy quyền theo quy định của pháp luật.</li>
  <li>Tất cả  thẻ cào trúng thưởng sẽ được kiểm tra và thẩm định. Các phiếu giả mạo không  đúng ký hiệu, mã số và không đúng với mẫu thẻ cào trúng thưởng đã đăng ký sẽ bị  xử lý theo quy định pháp luật Việt Nam hiện hành.</li>
</ul>
<p><strong><u>Quy định đổi giải thưởng cho các giải đặc biệt, giải nhất, giải nhì:</u></strong><u> </u></p>
<ul>
    <li>Tất cả  khách hàng trúng thưởng phải có nghĩa vụ đăng ký thông tin với công ty NSP trước  ngày 30/9/2012. Sau ngày 30/9/2012, tất cả thẻ cào trúng thưởng chưa được đăng  ký với công ty NSP sẽ được xem như mất giá trị đổi giải thưởng.</li>
    <li>Lễ trao  giải thưởng sẽ được tổ chức tại trụ sở chính công ty NSP – 359 Võ Văn Tần, Quận  3, TP.HCM vào ngày 05/10/2012. Thời gian và địa điểm có thể thay đổi để phù hợp  với tình hình thực tế. Mọi thay đổi sẽ được công ty NSP công bố trên website <a href="http://www.nsp.com.vn">www.nsp.com.vn</a> và thông báo trực tiếp tới khách hàng trúng giải thưởng.</li>
    <li>Trong  vòng 30 ngày kể từ ngày tổ chức lễ trao giải thưởng, nếu khách hàng trúng thưởng  không đến làm thủ tục nhận giải thì xem như người đó mất quyền nhận giải thưởng.</li>
    <li>Danh  sách khách hàng nhận được giải thưởng sẽ được công bố trên website: <a href="http://www.nsp.com.vn">www.nsp.com.vn</a> vào sau ngày 05/10/2012.</li>
</ul>
<p><strong><u>Quy định đổi giải thưởng cho các giải ba, giải tư, giải khuyến khích:</u></strong><strong><u> </u></strong>(CHỖ NÀY CÓ THỂ CHO KHÁCH HÀNG ĐỔI Ở TẠI ĐẠI  LÝ CỦA NSP)</p>
<ul>
    <li>Giải  thưởng sẽ được trao trực tiếp tại trụ sở chính của công ty NSP – 359 Võ Văn Tần,  Quận 3, TP.HCM.</li>
    <li>Đối với  các khách hàng ở xa, giải thưởng sẽ được  trao qua đường bưu điện với mức phí được trả bởi người nhận giải.</li>
    <li>Khách  hàng trúng thưởng phải có trách nhiệm liên hệ với công ty NSP ít nhất 01 ngày  trước khi làm thủ tục nhận giải.</li>
    
</ul>
<h3>9. CÁC QUY ĐỊNH KHÁC</h3>
<ul>
  <li>Khi  tham gia chương trình khuyến mãi này, khách hàng trúng giải đã mặc nhiên chấp nhận cho NSP quyền sử dụng  tên, hình ảnh và địa chỉ của mình vào mục đích quảng cáo mà NSP không phải trả  cho người này bất kỳ một khoản chi phí nào.</li>
  <li>NSP có  quyền thay đổi điều khoản và điệu kiện này hoặc hủy bỏ ưu đãi vào bất cứ thời  điểm nào mà không cần phải thông báo hay đưa ra lý do. Trong trường hợp có xảy  ra bất kỳ tranh chấp nào liên quan tới chương trình, quyết định của NSP sẽ là  quyết định cuối cùng. </li>
  <li>Thuế  thu nhập cá nhân không thường xuyên của người trúng giải được tính căn cứ trên  giá thực mua của NSP được thể hiện rõ trên hóa đơn do đơn vị cung cấp phát  hành. </li>
</ul>
<p><strong><u>M</u></strong><strong><u>ẫu thẻ cào trúng thưởng</u></strong><strong><u>:</u></strong></p>
<p align="center"><img src="images/stories/khuyen-mai/2012/5-AMP/the-cao-AIR-2_02-03.jpg" border="0" alt="Mẫu thẻ cào mặt trước" /></p>
<p align="center"><img src="images/stories/khuyen-mai/2012/5-AMP/the-cao-AIR-2_06.jpg" border="0" alt="Mẫu thẻ cào mặt sau" /></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Mọi thắc mắc xin vui lòng  liên hệ đường dây nóng:</strong><br />
  Điện  thoại: (08) 3834 2108 để được tư vấn thêm.<br />
  Web:  www.nsp.com.vn     Email: info@nsp.com.vn<br />
  <strong>            </strong></p>
<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

?>
