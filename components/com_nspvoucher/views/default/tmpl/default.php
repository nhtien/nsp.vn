<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<div class="nspvouchercom">
    <img src="images/voucher-nsp.jpg" border="0" align="Chuong trình ưu đãi - Đổi voucher nhận ngay quà tặng" />
    <div>&nbsp;</div>
    <div class="nav">
    	<div class="item<?php if(!$this->task2 || $this->task2=='account') echo ' current';?>"><a href="index.php?option=com_nspvoucher&Itemid=129">Thông tin cá nhân</a></div>
        <div class="item<?php if($this->task2=='2') echo ' current';?>"><a href="<?php echo JRoute::_('index.php?option=com_nspvoucher&task2=2');?>">Nội dung chương trình</a></div>
        <div class="item<?php if($this->task2=='products') echo ' current';?>"><a href="index.php?option=com_nspvoucher&task2=products&Itemid=129">Danh sách quà tặng</a></div>
    </div>
    <div class="content"><?php echo $this->content?></div>
</div>
