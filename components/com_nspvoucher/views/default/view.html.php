<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

class NspVoucherViewDefault extends JView
{
	function display($tpl = null)
	{
		global $nspVoucher_config;
		$user = &JFactory::getUser();
		$doc = &JFactory::getDocument();
		$model =& $this->getModel();
		$doc->setTitle($nspVoucher_config->meta_title);
		$doc->setDescription($nspVoucher_config->meta_desc);
		$doc->setMetaData('keywords',$nspVoucher_config->meta_key);
		
		$task2 = JRequest::getVar('task2');
		switch($task2)
		{
			// nội dung chuong trình
			case '2':
				$content = $nspVoucher_config->content_text;
				break;
			case 'products':
					$products = $model->getProducts(); //var_dump($products);exit;
					$tmpl = new NspVoucherTemplate('products');
					$tmpl->set('products', $products);
					$content = $tmpl->fetch();
				break;
			// thong tin ca nhan
			default:
				if($user->get('id')){
					$value = $model->getUserValue($user->id);
					$tmpl = new NspVoucherTemplate('account');
					$tmpl->set('user', $user);
					$tmpl->set('value', $value);
					$content = $tmpl->fetch();
				}else{
					$tmpl = new NspVoucherTemplate('login');
					$content = $tmpl->fetch();
				}
				break;
		}
		
		$this->assignRef('content', $content);
		$this->assignRef('task2', $task2);
		parent::display($tpl);
	}
}

?>


