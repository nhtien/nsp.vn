<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_JEXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class NspVoucherTemplate
{

	var $vars 	= array();
	
	var $file 	= null;
	
	var $folder	= null;
	
	function __construct( $filename = null )
	{
		
		if( !$filename ) $filename = 'index';
		
		$file = JPATH_COMPONENT.DS.'tpl'.DS.$filename.'.php';
			
		$this->file = $file;
		
	}
	
	function set( $name, $value )
	{
		$this->vars[$name] = $value;
	}
	
    function setRef($name, &$value) 
	{
	
        $this->vars[$name] =& $value;
		
    }
	function fetch( $file = null )
	{
		global $debug;
	
		if(!$file)
		{
			$file = $this->file;
		}
		
		if( !file_exists( $file ) )
		{
			return;
		}

		if($this->vars)
        	extract($this->vars, EXTR_REFS);

        ob_start();
        require($file);
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
	}
	function display( $file = null )
	{
		$content = $this->fetch( $file );
		echo $content;
	}

}
?>
