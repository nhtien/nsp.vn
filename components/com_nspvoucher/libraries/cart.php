<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_JEXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class Cart extends JObject
{
	 var $products 	= array();

	function __construct()
	{
		$cart = $this->getCart();
		$this->products = $cart['products'];
		return $cart;
	}
	
	
	// addtocart
	function add($productID, $productCode, $productName, $productPrice, $quantity, $thumbURL, $productURL)
	{
		$obj = new StdClass();
		$obj->id 		= $productID;
		$obj->code 		= $productCode;
		$obj->name 		= $productName;
		$obj->price 	= $productPrice;
		$obj->quantity	= $quantity;
		$obj->thumb_url = $thumbURL;
		$obj->link		= $productURL;
		$cart = $this->getCart();
		$products = $cart['products'];

		for($i=0 ; $i<count($products); $i++)
		{
			$p = $products[$i];
			if($p->id == $obj->id)
			{
				$products[$i]->quantity += $obj->quantity;
				break;
			}
		}
		if($i==count($products))
			$products[] = $obj;
		
		$cart['products'] = $products;
		$this->setCart($cart);
		
		return $obj;
		
	}
	function remove($productID)
	{
		$cart = $this->getCart();
		$cart = new Cart();
		$pcart = $cart->getCart();
		for($i=0 ; $i< count($pcart['products']) ; $i++)
		{ 
			if($pcart['products'][$i]->id == $productID){
				unset($pcart['products'][$i]);
			}
		}
		foreach($pcart['products'] as $p) $products[] = $p;
		$pcart['products'] = $products;
		$cart->setCart($pcart);
	}
	
	function update($productID, $quantity)
	{
		if($quantity <=0) $quantity=1;
		$cart = $this->getCart();
		for($i=0 ; $i< count($cart['products']) ; $i++)
		{
			if($cart['products'][$i]->id == $productID){
				$cart['products'][$i]->quantity = $quantity;
			}
		}
		$this->setCart($cart);
	}
	
	function getCart()
	{
		$session = JFactory::getSession();
		return $session->get('_cart',array(),'product');
	}
	
	function setCart($cart)
	{
		$session = JFactory::getSession();
		$session->set('_cart',$cart,'product');//var_dump($_SESSION);exit;
	}
	
	function removeAll()
	{
		$session = JFactory::getSession();
		$session->clear('_cart','product');
	}
	
	
}
	
?>