<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class NspVoucherModelPromotions extends JModel
{
	function __construct()
	{
		parent::__construct();
		
	}
	
	function getPromotions()
	{
		$db = &JFactory::getDBO();
		$query = "SELECT * FROM #__nspvoucher_promotions AS p WHERE p.published = 1 ORDER BY p.created DESC";
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		return $rows;
	}
	
}

?>