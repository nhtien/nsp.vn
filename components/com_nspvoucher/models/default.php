<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class NspVoucherModelDefault extends JModel
{
	function __construct()
	{
		parent::__construct();
		
	}
	
	function getUserValue( $user_id ){
		$db = &JFactory::getDBO();
		$query = "SELECT * FROM #__nspvoucher_account WHERE user_id = ".(int)$user_id;
		$db->setQuery($query);
		$row = $db->loadObject();
		return $row;
	}
	function getProducts()
	{
		$db = &JFactory::getDBO();
		$query = "SELECT * FROM #__nspvoucher_products AS p WHERE p.published = 1 ORDER BY p.price";
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		return $rows;
	}
	
}

?>