<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_JEXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );
?>
<h1 class="componentheading">Giỏ hàng của bạn</h1>
Tài khoản của bạn hiện có: <strong><?php echo number_format($value->total_value,0,',','.')?>đ</strong>
<div class="pcart">
<table class="n-border" width="100%">
<tr class="title">
  <td>Tên sản phẩm</td>
  <td>Giá</td>
  <td>Số lượng / cập nhật</td>
  <td>Tổng thành tiền</td>
 </tr>
<?php
$totalPrice = '';
if(!count($products))
{
	echo '<tr><td colspan="5"><p><i>Không có sản phẩm nào trong giỏ hàng của bạn.</i></p></td></tr>';
}
else
{
	foreach( $products as $p)
	{
	$subTotalPrice = '';
	if($p->price){
		$subTotalPrice = $p->quantity * $p->price;
		$totalPrice += $subTotalPrice;
	}else{
		$totalPrice = '';
	}
	?>
	<tr>
	  <td><a href="<?php echo $p->link?>"><?php echo $p->name?></a></td>
	  <td nowrap="nowrap"><?php echo vpriceFormat($p->price) ?></td>
	  <td nowrap="nowrap">
	  <form action="index.php?option=com_nspvoucher&task=updatecart" method="post">
		<input type="text" name="quantity" value="<?php echo $p->quantity?>" size="3" class="inputbox" align="absmiddle" />&nbsp;
		<input type="image" src="components/com_nspvoucher/images/update_cart.png" title="Thay đổi giỏ hàng" align="absmiddle" />
		<a href="index.php?option=com_nspvoucher&task=removefromcart&pid=<?php echo $p->id?>" title="Xóa sản phẩm ra khỏi giỏ hàng"><img src="components/com_nspvoucher/images/delete_cart.png" align="absmiddle" title="" /></a>
		<input type="hidden" name="pid" value="<?php echo $p->id?>" />
		</form>
		
	  </td>
	  <td nowrap="nowrap"><?php echo vpriceFormat($subTotalPrice)?></td>
	 </tr>
	<?php
	}
}
?>
</table>
<p><strong><?php echo 'Tổng thành tiền: '.vpriceFormat($totalPrice)?></strong></p>
<p align="center">
	<img src="components/com_nspvoucher/images/back.png" align="absmiddle" />&nbsp;<a href="index.php?option=com_nspvoucher&task2=products">Quay lại trang sản phẩm</a> &nbsp; &nbsp;	<a href="index.php?option=com_nspvoucher&task=checkout">Mua hàng</a>&nbsp;<img src="components/com_nspvoucher/images/forward.png" align="absmiddle" />
</p>
</div>
