<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Trần Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

// products
$ncolumns = 2;
$percent = (100/$ncolumns);

?>
<table class="productList" width="100%" cellspacing="5" cellpadding="4">
<tr>
<?php
	$i=1;
	$ncount = count($products);
	foreach( $products as $p):
		$link_product = '#';
		$lastClassName = ($i%$ncolumns == 0) ? 'last' : '';
		$thumb =  $p->thumb ? $p->thumb : 'none_thumb.jpg';
		$thumb_url = JURI::base().'documents/nspvoucher/products/'.$thumb;
	?>
	<td width="<?php echo $percent?>%" class="item">
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td rowspan="2" align="left" width="138"><div class="thumb setting5"><img src="<?php echo $thumb_url ?>" border="0" alt="<?php echo htmlspecialchars($p->name)?>" /></div></td>
        <td>
        <h4 class="red"><?php echo $p->name ?></h4>
        <span class="price">Giá: <strong><?php echo number_format($p->price,0,',','.')?>đ</strong></span>
        <form action="index.php" method="post">
        <input type="text" name="quantity" value="1" size="3" class="inputbox quantity" maxlength="5" />&nbsp;<input type="submit" value="Mua" class="button" />
        <input type="hidden" name="option" value="com_nspvoucher" />
        <input type="hidden" name="task" value="addtocart" />
        <input type="hidden" name="pid" value="<?php echo $p->id?>" />
        </form>
        </td>
      </tr>
      <tr>
        <td><?php echo $p->short_desc?></td>
      </tr>
    </table>
            
	</td>
<?php
$brline = ($i%$ncolumns == 0 && $i < $ncount) ? '<tr><td colspan="'.$ncolumns.'"><hr /></td></tr>' : '';
if( $i%$ncolumns == 0 ) echo '</tr>'.$brline.'<tr>';
$i++;
 endforeach;
 ?>
</tr>
<tr><td width="25%"></td><td width="25%"></td><td width="25%"></td><td width="25%"></td></tr>
</table>

