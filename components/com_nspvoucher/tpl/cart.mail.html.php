<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Trần Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/ 
?>
<html>
<body>
<p><b>Có một khách hàng đặt mua sản phẩm tại website <?php echo JURI::base()?></b></p>
Thông tin khách hàng:<br />
<table border="0" cellpadding="5" cellspacing="0" width="auto">
  <tr>
    <td class="label">Tên</td>
    <td><?php echo $order->name; ?></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ</td>
    <td><?php echo $order->address?></td>
  </tr>
  <tr>
    <td class="label">Email </td>
    <td><?php echo $order->email?></td>
  </tr>
  <tr>
    <td class="label">Điện thoại</td>
    <td><?php echo $order->phone?></td>
  </tr>
  <tr>
    <td class="label">Ghi chú</td>
    <td><?php echo $order->note?></td>
  </tr>
  <tr>
    <td class="label">Tổng tiền hiện có</td>
    <td><?php echo vpriceFormat($order->user_crvalue)?></td>
  </tr>
</table>


<table class="list" width="100%" border="1" cellpadding="5" cellspacing="0">
<tr class="title">
  <td align="center"><strong>Tên sản phẩm</strong></td>
  <td align="center"><strong>Giá sản phẩm</strong></td>
  <td align="center"><strong>Số lượng</strong></td>
  <td align="center"><strong>Thành tiền</strong></td>
 </tr>
<?php
$totalPrice = '';
if(!count($products))
{
	echo '<tr><td colspan="5">&nbsp;</td></tr>';
}
else
{
	foreach( $products as $p){
	$subTotalPrice = '';
	if($p->price){
		$subTotalPrice = $p->quantity * $p->price;
		$totalPrice += $subTotalPrice;
	}else{
		$totalPrice = '';
	}
	?>
	<tr>
	  <td><?php echo $p->name?></td>
	  <td nowrap="nowrap"><?php echo vpriceFormat($p->price)?></td>
	  <td nowrap="nowrap">
		<?php echo $p->quantity?>
	  </td>
	  <td nowrap="nowrap"><?php echo vpriceFormat($subTotalPrice)?></td>
	 </tr>
	<?php
	}
}
?>
</table>
<p><strong><?php echo 'Tổng giá trị đơn hàng: '.vpriceFormat($totalPrice)?></strong></p>
</body>
</html>

