<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_JEXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );
JHTML::_('behavior.formvalidation' );
?>
<h1 class="componentheading">Giỏ hàng</h1>
<p>Bạn vui lòng nhập chính xác các thông tin cá nhân của bạn để xác nhận đơn hàng.</p>
<form action="index.php?option=com_nspvoucher&task=save_cart" method="post" class="form-validate">
<table class="siteForm">
  <tr>
    <td class="label">Tên của bạn (*)</td>
    <td><input type="text" name="name" class="required" value="<?php echo $user->name?>" size="35" /></td>
  </tr>
  <tr>
    <td class="label">Địa chỉ (*)</td>
    <td><input type="text" name="address" class="required" value="<?php echo $user->address?>" size="55" /></td>
  </tr>
  <tr>
    <td class="label">Email (*)</td>
    <td><input type="text" name="email" class="required validate-email" value="<?php echo $user->email?>" size="35" /></td>
  </tr>
  <tr>
    <td class="label">Điện thoại (*)</td>
    <td><input type="text" name="phone" class="required" value="<?php echo $user->phone?>" size="35" /></td>
  </tr>
  <tr>
    <td class="label">Tên công ty</td>
    <td><input type="text" name="company_name" class="" value="" size="35" /></td>
  </tr>
  <tr>
    <td class="label">Ghi chú</td>
    <td><textarea class="inputbox" cols="50" rows="7" name="note"></textarea></td>
  </tr>
  <tr>
    <td class="label"></td>
    <td><?php echo $captcha; ?></td>
  </tr>
  <tr>
    <td class="label"></td>
    <td><input type="submit" value="Gửi" class="button validate" /></td>
  </tr>
</table>

<input type="hidden" name="tep" value="2" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>