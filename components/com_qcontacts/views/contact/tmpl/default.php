<?php
/**
 * QContacts Contact manager component for Joomla! 1.5
 *
 * @version 1.0.3
 * @package qcontacts
 * @author Massimo Giagnoni
 * @copyright Copyright (C) 2008 Massimo Giagnoni. All rights reserved.
 * @copyright Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */
 /*
This file is part of QContacts.
QContacts is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
defined( '_JEXEC' ) or die( 'Restricted access' );
$cparams = JComponentHelper::getParams ('com_media');
?>
<?php if ($this->params->get('show_page_title')) { ?>
<h1 class="componentheading<?php echo $this->params->get('pageclass_sfx'); ?>">
	<?php echo $this->escape($this->params->get('page_title')); ?>
</h1>
<?php } ?>

<div id="qcontacts<?php echo $this->params->get('pageclass_sfx'); ?>">

<?php if ($this->params->get('show_contact_list') && count($this->contacts) > 1) { ?>
	<form method="post" name="selectForm" id="selectForm">
		<?php echo JText::_('Select Contact'); ?>
		<br />
		<?php echo JHTML::_('select.genericlist', $this->contacts, 'id', 'class="inputbox" onchange="this.form.submit()"', 'id', 'name', $this->contact->id); ?>
		<input type="hidden" name="option" value="com_qcontacts" />
	</form>
<?php } ?>

<?php if($this->contact->name && $this->params->get('show_name', 1)) { ?>
	<p id="contact-name">
		<?php echo $this->contact->name; ?>
	</p>
<?php } ?>

<?php if($this->contact->con_position && $this->params->get('show_position', 1)) { ?>
	<p id="contact-position">
		<?php echo $this->contact->con_position; ?>
	</p>
<?php } ?>
<?php if ($this->contact->image && $this->params->get('show_image')) { ?>
	<div id="contact-image" class="<?php echo ($this->params->get('cimage_align') ? 'aright' : 'aleft'); ?>">
		<?php echo JHTML::_('image', $cparams->get('image_path').'/'.$this->contact->image, JText::_( 'Contact' )); ?>
	</div>
<?php } ?>

<?php echo $this->loadTemplate('address'); ?>
			
<?php if($this->params->get('allow_vcard')) { ?>
	<p>
		<?php echo JText::_('Download information as a'); ?>
		<a href="index.php?option=com_qcontacts&amp;controller=vcard&amp;contact_id=<?php echo $this->contact->id; ?>&amp;format=raw">
		<?php echo JText::_('VCard'); ?></a>
	</p>
<?php }
if ( $this->params->get('show_email_form') && ($this->contact->email_to || $this->contact->user_id)) {
	echo $this->loadTemplate('form');
}
?>
</div>
<p align="center">

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7838.967231786468!2d106.6872343070054!3d10.774223345054967!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9676ccf7b8202cf6!2zQ8O0bmcgdHkgVE5ISCBUTS1EViBUaW4gSOG7jWMgTmjDom4gU2luaCBQaMO6Yw!5e0!3m2!1svi!2s!4v1409977085940" width="600" height="450" frameborder="0" style="border:0"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=vi&amp;geocode=&amp;q=359+V%C3%B5+V%C4%83n+T%E1%BA%A7n,+5,+H%E1%BB%93+Ch%C3%AD+Minh,+Vi%E1%BB%87t+Nam&amp;aq=0&amp;oq=359&amp;sll=37.0625,-95.677068&amp;sspn=36.042042,79.013672&amp;ie=UTF8&amp;hq=&amp;hnear=359+V%C3%B5+V%C4%83n+T%E1%BA%A7n,+5,+H%E1%BB%93+Ch%C3%AD+Minh,+Vi%E1%BB%87t+Nam&amp;t=m&amp;ll=10.772603,106.685679&amp;spn=0.007378,0.012875&amp;z=16&amp;iwloc=A" style="color:#0000FF;text-align:left">Xem Bản đồ cỡ lớn hơn</a></small>


<!--
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?q=NSP&amp;hl=vi&amp;sll=10.768981,106.683266&amp;sspn=0.006295,0.006295&amp;ie=UTF8&amp;view=map&amp;cid=4498449364836292790&amp;hq=NSP&amp;hnear=&amp;ll=10.770116,106.684821&amp;spn=0.007378,0.00912&amp;z=16&amp;output=embed"></iframe><br /><small>
</small>
-->