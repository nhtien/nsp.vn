<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.controller' ); 

class NspTrainingController extends JController 
{
	function display() 
	{	
		$document =& JFactory::getDocument(); 
		$viewName = JRequest::getVar('view', 'categories'); 
		$viewType = $document->getType(); 
		$view = &$this->getView($viewName, $viewType, 'NspTrainingView'); 
		$model =& $this->getModel( $viewName, 'NspTrainingModel' ); 
		if (!JError::isError( $model )) { 
			$view->setModel( $model, true ); 
		}

		$view->setLayout('default'); 
		$view->display();
	}
	
	function save_regcourse(){
		include( JPATH_COMPONENT.DS.'libraries'.DS.'common.php');
		
		$user = &JFactory::getUser();
		$db = &JFactory::getDBO();
		$config = new JConfig();
		global $Itemid,$mainframe, $nspTraining_config;
		$post = JRequest::get('post');
		$post['published'] = 0;
		$post['create_by'] = $user->id;
		
		$row = &JTable::getInstance('regcourse','Table');

		$post['created_date'] =  date("Y-m-d H:i:s");
	
		
		if( !$row->bind( $post ) ){
			JError::raiseError( 500, $row->getError() );
		}
		if( !$row->store() ){
			JError::raiseError( 500, $row->getError() );
		}
		/*tinbui@nsp.com.vn;sangtran@nsp.com.vn;
		$document = &JFactory::getDocument();
		$viewType = $document->getType(); 
		$view = &$this->getView('register',$viewType,'NspTrainingView');
		$view->assignRef('data', $row);
		$view->setLayout('thank_you');
		$view->display();
		return;
		*/
		
		/* Send Email */
		if($nspTraining_config->send_reg_to_mail == '1'){
			$arr_body = array(
								'Loại đối tác'	=> $arr_partner_type[$row->partner_type],
								'Tên công ty'	=> $row->company,
								'Họ Tên'		=> $row->name,
								'Email'			=> $row->mail,
								'ĐTDĐ'			=> $row->mobile,
								'Điện Thoại'	=> $row->phone,
								'Fax'			=> $row->fax,
								'Địa chỉ'		=> $row->address,
								'Chức danh'		=> $row->position,
								'Tổng số người'	=> $row->total_attending,
								'Địa điểm'		=> $arr_location[$row->location],
								'Ngày đăng ký'	=> JHTML::_('date', date("Y-m-d H:i:s"), JText::_('DATE_FORMAT_LC2'))
			);
			
			$course = &JTable::getInstance('course','Table');
			$course->load( $post['course_id']);
			
			$recipient	= explode(";",$nspTraining_config->contact_email);
			$cc			= explode(";",$nspTraining_config->contact_cc_email);
			
			$body = 'Có một ứng viên đã đăng ký khóa học '.$course->name ." trực tuyến tại website http://www.nsp.com.vn.\n\nThông tin ứng viên:\n";
			
			while( list($k,$v) = each($arr_body) ){
				$body .= $k.": ".$v."\n";
			}
			jimport('joomla.mail.mail');
			
			//$mail = new JMail();
			$mail = JFactory::getMailer();
			$mail->setSender( $config->mailfrom );
			$mail->addCC($cc);
			$mail->setSubject('Dang ky khoa hoc '.$course->name);
			$mail->addRecipient( $recipient );
			$mail->setBody($body);
			$mail->set('FromName','NHAN SINH PHUC CO., LTD.');
			//var_dump( $mail);exit;
			$mail->send();
		}
		
		$msg = JText::_('Your Registration was sent successfully!');
		$mainframe->redirect(NRoute::_("index.php?option=com_nsptraining&view=register&layout=thank_you"), $msg);
		return;
	}
	
	function popup_report_form(){
		global $mosConfig_live_site;
		$db = &JFactory::getDBO();
		$id = JRequest::getVar('sid');
		$act = JRequest::getVar('act');
		$user = &JFactory::getUser();
		if( !$act){
			$sql = "SELECT * FROM #__escmobile_software WHERE id=".$id;
			$db->setQuery( $sql );
			$row = $db->loadObject();
			include(JPATH_COMPONENT."/helpers/popup_report.php");
			PopupReport::showForm( $row );
			return;
		}
		$post = JRequest::get( 'post' );
		$post['post_date'] = date("Y-m-d H:i:s");
		$post['author'] = $user->id;
		$post['link'] = $mosConfig_live_site.'/index.php?option=com_nsptraining&view=software&sid='.$id.'&Itemid=22';
		$row = &JTable::getInstance('report','Table');
		if( !$row->bind( $post ) ){
			JError::raiseError( 500, $row->getError() );
		}
		if( !$row->store() ){
			JError::raiseError( 500, $row->getError() );
		}
		$msg = "<h3>Thank for post report!</h3>";
		echo $msg;
	}
	
		
	function download(){
		global $nspTraining_config,$mainframe; 
		$db = &JFactory::getDBO();
		$fid = JRequest::getVar('fid','0');
		if( JRequest::getVar('return') ){
			$return = base64_decode( JRequest::getVar('return'));
		}
		$sql = "SELECT f.* FROM #__escmobile_files AS f INNER JOIN #__escmobile_software AS p ON p.id = f.software_id 
								WHERE f.id = $fid AND p.published=1";
		$db->setQuery( $sql );
		$row = $db->loadObject();
		
		$folder = $row->folder ? $row->folder.'/' : '';
		
		$filename = $nspTraining_config->path_project.$folder.$row->filename;
		
		$obj = new stdClass();
		$obj->id = $row->id;
		$obj->filename = $filename;
		$obj->software_id = $row->software_id;
		$obj->directlink = 0;
		
		/**/
		$paramsTmpl['allowed_file_types'] = "
			{hqx=application/mac-binhex40} 
			{cpt=application/mac-compactpro} 
			{csv=text/x-comma-separated-values} 
			{bin=application/macbinary} 
			{dms=application/octet-stream} 
			{lha=application/octet-stream} 
			{lzh=application/octet-stream} 
			{exe=application/octet-stream} 
			{class=application/octet-stream} 
			{psd=application/x-photoshop} 
			{so=application/octet-stream} 
			{sea=application/octet-stream} 
			{dll=application/octet-stream} 
			{oda=application/oda} 
			{pdf=application/pdf} 
			{ai=application/postscript} 
			{eps=application/postscript} 
			{ps=application/postscript} 
			{smi=application/smil} 
			{smil=application/smil} 
			{mif=application/vnd.mif} 
			{xls=application/vnd.ms-excel} 
			{ppt=application/powerpoint} 
			{wbxml=application/wbxml} 
			{wmlc=application/wmlc} 
			{dcr=application/x-director} 
			{dir=application/x-director} 
			{dxr=application/x-director} 
			{dvi=application/x-dvi} 
			{gtar=application/x-gtar} 
			{gz=application/x-gzip} 
			{php=application/x-httpd-php} 
			{php4=application/x-httpd-php} 
			{php3=application/x-httpd-php} 
			{phtml=application/x-httpd-php} 
			{phps=application/x-httpd-php-source} 
			{js=application/x-javascript} 
			{swf=application/x-shockwave-flash} 
			{sit=application/x-stuffit} 
			{tar=application/x-tar} 
			{tgz=application/x-tar} 
			{xhtml=application/xhtml+xml} 
			{xht=application/xhtml+xml} 
			{zip=application/x-zip} 
			{mid=audio/midi} 
			{midi=audio/midi} 
			{mpga=audio/mpeg} 
			{mp2=audio/mpeg} 
			{mp3=audio/mpeg} 
			{aif=audio/x-aiff} 
			{aiff=audio/x-aiff} 
			{aifc=audio/x-aiff} 
			{ram=audio/x-pn-realaudio} 
			{rm=audio/x-pn-realaudio} 
			{rpm=audio/x-pn-realaudio-plugin} 
			{ra=audio/x-realaudio} 
			{rar=application/x-rar} 
			{rv=video/vnd.rn-realvideo} 
			{wav=audio/x-wav} 
			{bmp=image/bmp} 
			{gif=image/gif} 
			{jpeg=image/jpeg} 
			{jpg=image/jpeg} 
			{jpe=image/jpeg}
			{png=image/png} 
			{tiff=image/tiff} 
			{tif=image/tiff} 
			{css=text/css} 
			{html=text/html} 
			{htm=text/html} 
			{shtml=text/html} 
			{txt=text/plain} 
			{text=text/plain} 
			{log=text/plain} 
			{rtx=text/richtext} 
			{rtf=text/rtf} 
			{xml=text/xml} 
			{xsl=text/xml} 
			{mpeg=video/mpeg} 
			{mpg=video/mpeg} 
			{mpe=video/mpeg} 
			{qt=video/quicktime} 
			{mov=video/quicktime} 
			{avi=video/x-msvideo} 
			{flv=video/x-flv} 
			{movie=video/x-sgi-movie} 
			{doc=application/msword} 
			{xl=application/excel} 
			{eml=message/rfc822}" ;
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'download.php');
		escProjectDownload::download( $obj,$currentLink , $paramsTmpl);
		exit;
	}
	/* download to phone */
	function mdownload(){
		global $mainframe;
		$dcode = (int)JRequest::getVar('dcode');
		$file = &JTable::getInstance('file', 'Table');
		if( !$file->load( $dcode ) ){
			$msg = "The code incorrect";
			$mainframe->redirect("index.php?option=com_nsptraining&task=d2p&tmpl=component", $msg);
			exit;
		}
		/* download link */
		$mainframe->redirect("index.php?option=com_nsptraining&task=download&fid=".$file->id);
	}
	
	function check_phone(){
		$mid = JRequest::getVar('model_id');
		$bid = JRequest::getVar('manufacture');
		$return = JRequest::getVar('return','index.php');
		
		$_SESSION['user_brand_id'] = $bid;
		$_SESSION['user_model_id'] = $mid;
		
		$this->setRedirect( $return );
	}	
/* */	

/* ajax function */
	
	function ajax_check_compatible(){
		$db = &JFactory::getDBO();
		$software_id = JRequest::getVar('sid');
		$model_id = JRequest::getVar('mid');
		
		$sql = "SELECT f.* FROM #__escmobile_files AS f INNER JOIN #__escmobile_software AS s ON s.id = f.software_id 
		WHERE s.id = $software_id 
		ORDER BY f.created_date DESC ";
		$db->setQuery( $sql ); 
		$files = $db->loadObjectList();
		foreach( $files as $f){
			$arr = explode(",",$f->model_id);
			if( in_array( $model_id,$arr) ){
				echo "The software is available for your phone. 
					<a href=\"javascript:void(0);\" onclick=\"eDownloadWin('".$f->id."');\" >Download it now</a>";
				exit;
			}
		}
		echo "Sorry! The software is not available for your phone";
		
	}
	function ajax_model_selectbox( ){ 
		$db = &JFactory::getDBO();
		$cid = JRequest::getVar('manf_id');
		$select_id = JRequest::getVar('slid','model_id');
		$selected = JRequest::getVar('selected','0');
		$where = array();
		$where[] = "mf.id=$cid";
		$row = &JTable::getInstance('model','Table');
		
		$rows = $row->getList(1,999,'m.name', $where); 
		$model_option = array();
		$model_option[] = JHTML::_('select.option','','- select once -');
		
		foreach( $rows as $r){
			$model_option[] = JHTML::_('select.option',$r->id,$r->name);
		}
		$lists['model'] = JHTML::_('select.genericList', $model_option, 'model_id','class="inputbox"','value','text', $selected, $select_id);
		
		echo $lists['model'];
		
		exit;
		
	}
	
	
	function ajaxRemoveFavorite(){
		$user = &JFactory::getUser();
		$db = &JFactory::getDBO();
		if( !$user->id ){
			return;
		}
		$bid = JRequest::getVar("bid",0);
		$sql = "SELECT id FROM #__mbook_favorites WHERE book_id=$bid AND user_id = $user->id LIMIT 0,1";
		$db->setQuery( $sql ); 
		$id = $db->loadResult();
		require(JPATH_ADMINISTRATOR.DS."components/com_mbook/tables/favorite.php");
		$library =& JTable::getInstance('favorite', 'Table'); 
		if (!$library->delete( $id )) {
			$msg = "Delete is not sucessful";
		}else{
			$msg = "Delete is successful";
		}
		echo $msg;
	}	

/* pop up */
	function popup_location(){
		
		$language_option=array();
		$language_option[] = JHTML::_('select.option','',' Select Language ');
		$lists['language'] = JHTML::_('select.genericList', $language_option, 'language','class="inputbox"','value','text',null);
		
		$country_option=array();
		$country_option[] = JHTML::_('select.option','',' Select Country ');
		$lists['location'] = JHTML::_('select.genericList', $country_option, 'country','class="inputbox"','value','text',null);
		
		require_once(JPATH_COMPONENT.DS.'helpers'.DS.'popup.php');
		EscMobilePopup::location($lists);
	}
	
	function popup_download(){
		$db = &JFactory::getDBO();
		$fid = JRequest::getVar('fid','0');
		$sql = "SELECT f.*, s.name as software_name FROM #__escmobile_files AS f INNER JOIN #__escmobile_software AS s ON s.id = f.software_id WHERE f.id=".$fid;
		$db->setQuery( $sql );
		$row = $db->loadObject();
		require_once(JPATH_COMPONENT.DS.'helpers'.DS.'popup.php');
		EscMobilePopup::download($row, $lists);
	}
	
	function popup_bookmark(){
		$bid = JRequest::getVar('id',0);
		$itemid = JRequest::getVar('Itemid',0);
		require_once(JPATH_COMPONENT.DS.'helpers'.DS.'popup_bookmark.php');
		Boobmark::show($bid, $itemid);
	}
	function popup_adtoFavorite(){
		$user = &JFactory::getUser();
		$db = &JFactory::getDBO();
		$bid = JRequest::getVar('id');
		$act = JRequest::getVar('act');
		if( !$user->get('id') ){
			require_once(JPATH_COMPONENT.DS.'helpers'.DS.'popup_adfavorite.php');
			Favorite::loginRequest( );
			return;
		}
		if( (int)$bid <= 0){
			echo "Please select a book";
			return;
		}
		if( $act == "save" ){
			$date = date("Y-m-d H:i:s");
			$sql = "SELECT * FROM #__mbook_favorites WHERE user_id = $user->id AND book_id = $bid";
			$db->setQuery( $sql );
			$row = $db->loadObjectList();
			if( count( $row) > 0){
				echo '<script language="javascript">window.close();</script>';
				return;
			}
			$sql = "INSERT INTO #__mbook_favorites(`id` ,`user_id` ,`book_id` ,`created_date` )VALUES (NULL , $user->id, $bid, '".$date."')";
			$db->setQuery( $sql );
			$db->query();
			
			echo '<script language="javascript">window.close();</script>';
			return;
		}
		require_once(JPATH_COMPONENT.DS.'helpers'.DS.'popup_adfavorite.php');
		Favorite::showForm( $gid );
	}

}
?>