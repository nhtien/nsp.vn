<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
$lists = $this->lists;
JHTML::_('behavior.formvalidation' );

$link = NRoute::_('index.php?option=com_nsptraining');

?>
<div class="componentheading"><?php echo JText::_('NT_TRAINING_TITLE')?></div>
<div class="view-nsptraining-register">
<form action="index.php" name="adminForm" method="post" class="form-validate">
<p><?php echo JText::_('NT_TRAINING_REQUIRED_FIELD');?><p>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="contentpane">
<tr>
	<td height="40">
		<label id="" for="partner_type">
			<?php echo JText::_( 'Partner Type' ); ?>:
		</label>
	</td>
  	<td>
  		<?php echo $lists['partner_type']?>
  	</td>
</tr>
<tr>
	<td width="30%" height="40">
		<label id="namemsg" for="company">
			Tên công ty:
		</label>
	</td>
  	<td>
  		<input type="text" name="company" id="company" size="40" value="" class="required" maxlength="50" /> *
  	</td>
</tr>
<tr>
	<td width="30%" height="40">
		<label id="namemsg" for="name">
			<?php echo JText::_( 'Your Name' ); ?>:
		</label>
	</td>
  	<td>
  		<input type="text" name="name" id="name" size="40" value="" class="required" maxlength="50" /> *
  	</td>
</tr>
<tr>
	<td height="40">
		<label id="position" for="position">
			<?php echo JText::_( 'Position' ); ?>:
		</label>
	</td>
	<td>
		<input type="text" id="position" name="position" size="40" value="" class="required" maxlength="25" /> *
	</td>
</tr>
<tr>
	<td height="40">
		<label id="phone" for="phone">
			<?php echo JText::_( 'Phone' ); ?>:
		</label>
	</td>
	<td>
		<input type="text" id="phone" name="phone" size="40" value="" class="" maxlength="25" />
	</td>
</tr>
<tr>
	<td height="40">
		<label id="faxmsg" for="fax">
			<?php echo JText::_( 'Fax' ); ?>:
		</label>
	</td>
	<td>
		<input type="text" id="" name="fax" size="40" value="" class="" maxlength="25" />
	</td>
</tr>
<tr>
	<td height="40">
		<label id="mobilemgs" for="mobile">
			<?php echo JText::_( 'Mobile Phone' ); ?>:
		</label>
	</td>
	<td>
		<input type="text" id="mobile" name="mobile" size="40" value="" class="required" maxlength="25" /> *
	</td>
</tr>
<tr>
	<td height="40">
		<label id="address" for="address">
			<?php echo JText::_( 'Address' ); ?>:
		</label>
	</td>
	<td>
		<input type="text" id="address" name="address" size="40" value="" class="required" maxlength="25" /> *
	</td>
</tr>
<tr>
	<td height="40">
		<label id="mailmsg" for="mail">
			<?php echo JText::_( 'Email' ); ?>:
		</label>
	</td>
	<td>
		<input type="text" id="mail" name="mail" size="40" value="" class="required validate-email" maxlength="100" /> *
	</td>
</tr>

<tr>
	<td height="40">
		<label id="locationmsg" for="location">
			<?php echo JText::_( 'Location' ); ?>:
		</label>
	</td>
	<td>
		<?php echo $lists['location'];?>
	</td>
</tr>
<tr>
	<td height="40">
		<label id="total_msg" for="total_attending">
			<?php echo JText::_( 'Total Attending' ); ?>:
		</label>
	</td>
	<td>
		<input type="text" id="total_attending" name="total_attending" size="40" value="" class="required " maxlength="100" /> *
	</td>
</tr>
<tr>
	<td colspan="2">
		<strong><?php echo JText::_('Choose a Course')?>:</strong>
		<ul style="list-style:none;">
		<?php
		$current_value = JRequest::getVar('cid', $this->courses[0]->id);
		foreach( $this->courses as $c){
			$check = $c->id == $current_value ? 'checked="checked"' : '';
			echo '<li> <input type="radio" name="course_id" value="'.$c->id.'" '.$check.' />'.$c->name.'</li>';
		}
		?>
		</ul>
	</td>
</tr>

</table>
<?php $mainframe->triggerEvent('onShowOSOLCaptcha', array(false)); ?>
<input type="submit" name="Submit" value="<?php echo JText::_('Send Register')?>"  class="button validate" onclick="" /> &nbsp; 
<input type="button" name="back" value="<?php echo JText::_('Cancel Register')?>" class="button" onclick="window.location.href='<?php echo $link?>';" />
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="com_nsptraining" />
<input type="hidden" name="task" value="save_regcourse" />
</form>
</div>
<!-- view -->