<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die('Restricted access');

global $nspTraining_config;

$arr_partner_type = array(
						'SI' => JText::_('Systems Intergrators'),
						'CD' => JText::_('Consultants - Design'),
						'ME' => JText::_('Contractors Electrical Engineering'),
						'CC' => JText::_('Construction Contructors'),
						'EU' => JText::_('End User'),
						'SV' => 'Sinh viên',
						'OT' => JText::_('Other')
						
					);
					
$arr_location = array(
						'HCM' => JText::_('Ho Chi Minh City'),
						'HN' => JText::_('Ha Noi'),
						
					);


?>