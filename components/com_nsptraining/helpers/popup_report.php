<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined('_JEXEC') or die("Direct Access Is Not Allowed");

class PopupReport{
	
	function showForm( $row ){
	?>
<h3 class="titlebar">Report Inappropriate Project: <?php echo $row->name;?></h3>
 
<p>Tell us why you believe this project is inappropriate.</p>
<form method="post" action="">
    <input type="hidden" name="project_id" value="<?php echo $row->id;?>" />
	 <input type="hidden" name="option" value="com_nsptraining" />
    <input type="hidden" name="task" value="popup_report_form" />
	<input type="hidden" name="act" value="save" />
    <label>Reason: </label><p><textarea name="report_text" rows="8" cols="80" style="width: 400px;"></textarea><p>
    <p><input type="submit" value="Submit" name="submitInappropriate" class="button" /></p>
</form>	<?php
	}
	
}	
?>
