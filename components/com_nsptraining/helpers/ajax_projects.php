<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
		<table border="0" cellpadding="5" cellspacing="0">
		<?php
		foreach( $projects as $r){
			$link = EPRoute::_("index.php?option=com_nsptraining&view=project&id=$r->id");
			$percent = ($r->total_rate ? (100*$r->rate_good/$r->total_rate ) : 0 );
			$percent = round( $percent,2);
		?>
		<tr>
			<td valign="top">
				<a class="name" href="<?php echo $link;?>" title="View detail"><?php echo $r->name;?></a>
				<div class="des"><?php echo $r->short_des;?></div>
				<a href="<?php echo $link;?>" class="download" title="download">Download now!<?php echo $r->filesize;?></a>
			</td>
			<td width="100" valign="top" align="center">
			<div><img src="modules/mod_escproject_for_wind/tmpl/images/icon-commentadd.png" />  &nbsp;&nbsp; <img src="modules/mod_escproject_for_wind/tmpl/images/icon-commentminus.png" /></div>
			<span class="recommend"><?php echo $percent;?>% recommend</span><br/>
			<span class="vote">(<?php echo $r->total_rate;?> ratings)</span>
			</td>
		</tr>
		<?php } ?>
		</table>
