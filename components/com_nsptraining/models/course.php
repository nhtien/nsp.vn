<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class NspTrainingModelCourse extends JModel
{
	var $cid = null;
	
	function __construct(){
		$this->cid = JRequest::getVar('cid',0);
		parent::__construct();
		
	}
	
	function getCourse( $cid= null ){
		if( !$cid ) $cid = $this->cid;
		$db = &JFactory::getDBO();
		$query = "SELECT * FROM #__nsptraining_courses AS cr WHERE published=1 AND cr.id=$cid";
		$db->setQuery($query);
		$row = $db->loadObject();
		return $row;
	}
	
}

?>