<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class NspTrainingModelAbout extends JModel
{
	var $id = null;
	
	function __construct(){
		$this->_id = JRequest::getVar('bid',0);
		parent::__construct();
		
	}
	
	function getData(){
		$categories = $this->_getCategories();
		$data = array();
		foreach( $categories as $c){
			$obj = new stdClass();
			$obj = $c;
			$obj->courses = $this->_getCourses( $c->id );
			$data[] = $obj;
		}
		return $data;
	}
		
	function _getCategories( ){
		$db = &JFactory::getDBO();
		$query = "SELECT * FROM #__nsptraining_categories AS c WHERE published=1 ORDER BY c.ordering ";
		$db->setQuery($query);
		$row = $db->loadObjectList();
		return $row;
	}
	function _getCourses( $cat_id ){
		$db = &JFactory::getDBO();
		$query = "SELECT * FROM #__nsptraining_courses AS cr WHERE published=1 AND cr.category_id=$cat_id ORDER BY cr.ordering ";
		$db->setQuery($query);
		$row = $db->loadObjectList();
		return $row;
	}
	
}

?>