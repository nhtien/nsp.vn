<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
global $Itemid;
$job = $this->job;
?>
<div class="view_brand view-jobs">

	<div class="featured">
		<div class="header t_r_setting7">
			<h3><?php echo JText::_('Careers')?></h3>
			<span class="view_all"><a href="index.php?option=com_nsp&view=jobs"><?php echo JText::_('View all')?> </a></span>
		</div><!--header-->
		<br />
		<div class="list_job">
				<div class="item">
					<h3><?php echo $job->job_title?></h3>
					<table width="90%" border="0" cellspacing="0" cellpadding="3" class="tbl">
					  <tr>
						<td width="120" class="lb">Số lượng:</td>
						<td><?php echo $job->number > 0 ? $job->number : '&nbsp;' ?></td>
					  </tr>
					  <tr>
						<td class="lb">Địa điểm làm việc:</td>
						<td><?php echo $job->location?></td>
					  </tr>
					  <tr>
						<td class="lb">Mô tả việc làm:</td>
						<td><?php echo $job->job_description?></td>
					  </tr>
					  <tr>
						<td class="lb">Kỹ năng tối thiểu:</td>
						<td><?php echo $job->job_skills?></td>
					  </tr>
					  <tr>
						<td class="lb">Trình độ tối thiểu:</td>
						<td><?php echo $job->mini_edu_level?></td>
					  </tr>
					  <tr>
						<td class="lb">Kinh nghiệm yêu cầu:</td>
						<td><?php echo $job->experience?></td>
					  </tr>
					  <tr>
						<td class="lb">Yêu cầu giới tính:</td>
						<td><?php echo $job->gender?></td>
					  </tr>
					  <tr>
						<td class="lb">Hình thức làm việc:</td>
						<td><?php echo $job->job_type?></td>
					  </tr>
					  <tr>
						<td class="lb">Mức lương:</td>
						<td><?php echo $job->salary?></td>
					  </tr>
					  <tr>
						<td class="lb">Thời gian thử việc:</td>
						<td><?php echo $job->probationary?></td>
					  </tr>
					  <tr>
						<td class="lb">Các chế độ khác:</td>
						<td><?php echo $job->regimes?></td>
					  </tr>
					  <tr>
						<td class="lb">Yêu cầu hồ sơ:</td>
						<td><?php echo $job->profile?></td>
					  </tr>
					  <tr>
						<td class="lb">Hạn nộp hồ sơ:</td>
						<td><?php echo date("d/m/Y",strtotime($job->end_date));?></td>
					  </tr>
					  <tr>
						<td></td>
						<td>
							<a href="index.php?option=com_nsp&view=jobs" class="more">Trang tuyển dụng</a> &nbsp; &nbsp; 
							<a href="index.php?option=com_nsp&view=jobapply&jid=<?php echo $job->id?>" class="apply">Gửi đơn trực tuyến</a>
						</td>
					  </tr>
					</table>
				</div>
		<br />
		<p><strong><em>Liên hệ nộp hồ sơ & phỏng vấn trực tiếp tại:</em></strong></p>
		<strong>Địa chỉ:</strong> 54 Hoa Hồng, phường 02, quận Phú Nhuận, TP HCM<br />
		<strong>Điện thoại:</strong> (028) 3834 2108 – (+84) 77 571 3635 <br/>
		<strong>Email:</strong> hr@nsp.com.vn
			
		</div>
	</div><!--featured-->
</div><!-- view-brand -->


