<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
$agents = $this->xml;
$pane =& JPane::getInstance('sliders');
$flash_path = JURI::base().'images/stories/flash/agents-map.swf';
?>
<div class="nsp-view-agents">
<div class="componentheading"><?php echo JText::_('Agents')?></div>
<br />
<?php
echo $pane->startPane("nsp-agents-panel");
foreach($agents as $item){ 
	echo $pane->startPanel($item['title'], $item['tab']."-tab");
	//echo '<h4>'.$item['title'].'</h4>';
	echo '<ol>';
	foreach( $item as $a){
		?>
		<li>
			<strong><?php echo $a->name ?></strong>
			<p>
			<?php 
			echo JText::_('Address').': '.$a->address.'<br />';
			echo JText::_('Tel').': '.$a->phone.'<br />';
			if($a->fax) echo JText::_('Fax').': '.$a->fax.'<br />';
			if($a->email) 'Email'.': '.$a->email.'<br />';
			if($a->website) echo JText::_('website').': <a href="'.$a->website.'" target="_blank">'.$a->website.'</a><br />';
			?>
			</p>
		</li>
		<?php
	}
	echo '</ol>';
	echo $pane->endPanel();
}
echo $pane->endPane();
?>
</div>
<div id="flashMapAgents"><p>To view the flash, you need the latest <a href="http://adobe.com/go/getflashplayer">Flash player</a>.</p></div>
<script language="javascript">
	var swfAgentsMap = new SWFObject("<?php echo $flash_path?>", "agent-map", "730", "600", "8", "#FFFFFF");
	swfAgentsMap.write('flashMapAgents');
	
</script>