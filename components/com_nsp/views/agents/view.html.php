<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

class NspViewAgents extends JView
{
	function display($tpl = null)
	{
		$doc = &JFactory::getDocument();
		$doc->addScript(JURI::base().'components/com_nsp/js/swfobject.js');
			
		$model =& $this->getModel();
		$xml = simplexml_load_file(JURI::base().'components/com_nsp/xml/agents.xml');
		
		$this->assignRef('xml', $xml);
		parent::display($tpl);
	}
}

?>