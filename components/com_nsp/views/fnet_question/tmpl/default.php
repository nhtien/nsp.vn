<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$doc = JFactory::getDocument();
$doc->setTitle('Chương trình tìm hiểu cách bảo vệ hệ thống cáp DTX-1800');
$doc->setDescription('Chương trình tìm hiểu cách bảo vệ hệ thống cáp DTX-1800. Bằng cách tham gia trả lời các câu hỏi trắc nghiệm trong chương trình, khách hàng sẽ có cơ hội sở hữu bộ kiểm tra cáp đồng trục nhỏ gọn Pocket Toner NX8 hay áo thun thời trang NSP.');
$doc->setMetaData('keywords','DTX-1800, trac nghiem DTX');

if(isset($_SESSION['__show_result'])){
	echo' <style type="text/css">
ol li.t{ color:#F00 !important;}
</style>';
	unset($_SESSION['__show_result']);

}

JHTML::_('behavior.formvalidation');
?>
<img src="fnet/FNET-DTX-1800-bang-cau-hoi/banner-chuong-trinh.jpg" alt="Chương trình tìm hiểu cách bảo vệ hệ thống cáp DTX-1800" />
<h3>Để tham dự chương trình, vui lòng chọn câu trả lời đúng và điền đầy đủ thông tin bên dưới:</h3>
<ol type="I" class="heading">
<!--<li>Mục tiêu: đẩy mạnh sản phẩm DTX-1800, tăng độ hiểu  biết về sản phẩm DTX, <br />mở rộng nhu cầu đo kiểm</li>
<li>Chủ đề: &ldquo;Bảo hiểm&rdquo; hệ thống cáp với DTX-1800</li>
<li>Câu hỏi:</li>-->
</ol>
<form action="index.php" method="post" class="form-validate">
<ol class="question">
  <li>Chức năng CHÍNH của thiết bị DTX CableAnalyzer là:</li>
  <ol type="a">
    <li class="t"><input type="radio" name="q1" value="a" /> Đo chứng nhận và báo cáo hiệu suất kết nối cáp theo tiêu chuẩn</li>
    <li>
      <input type="radio" name="q1" value="b" />
      Kiểm tra tính thông mạch của kết nối cáp</li>
    <li>
      <input type="radio" name="q1" value="c" />
      Đo chiều dài của kết nối cáp</li>
    <li>
      <input type="radio" name="q1" value="d" />
      Đo điện áp trên cổng thiết bị mạng</li>
  </ol>
  <li>Lĩnh vực nào bên dưới KHÔNG thuộc phạm vi đo của các dòng máy DTX CableAnalyzer?</li>
  <ol type="a">
    <li>
      <input type="radio" name="q2" value="a" /> Hiệu suất cáp đồng trục (CATV, CCTV)</li>
    <li>
      <input type="radio" name="q2" value="b" /> Hiệu suất sợi quang tier 1 (Loss/Length) và tier 2 (OTDR)</li>
    <li class="t"><input type="radio" name="q2" value="c" /> Chất lượng sóng wifi</li>
    <li><input type="radio" name="q2" value="d" /> Hiệu suất cáp đôi xoắn (Cat-5e, Cat-6, Cat-6A, Cat-7...)</li>
  </ol>
  <li>Các dòng máy DTX CableAnalyzer có những đặc trưng cơ bản nào?</li>
  <ol type="a">
    <li><input type="radio" name="q3" value="a" /> Chứng nhận hiệu suất kết nối cáp theo tiêu chuẩn với độ chính xác cao nhất hiện nay (Level IV)</li>
    <li><input type="radio" name="q3" value="b" /> Hỗ trợ xử lý sự cố, chỉ ra chính xác vị trí và nguyên nhân gây ra lỗi </li>
    <li><input type="radio" name="q3" value="c" /> Kiểm tra cáp đồng và sợi quang cùng lúc, không cần thay đổi module</li>
    <li class="t"><input type="radio" name="q3" value="d" /> Tất cả các đặc trưng trên</li>
  </ol>
  <li>Máy DTX-1800 đo chứng nhận một kết nối Cat-6 mất bao lâu?</li>
  <ol type="a">
    <li class="t"><input type="radio" name="q4" value="a" /> 9 giây</li>
    <li><input type="radio" name="q4" value="b" /> 15 giây</li>
    <li><input type="radio" name="q4" value="c" /> 30 giây</li>
    <li><input type="radio" name="q4" value="d" /> 40 giây</li>
  </ol>
  <li>Nguy cơ của việc hệ thống kết nối cáp không đạt chuẩn hoặc không được đo chứng nhận theo tiêu chuẩn:</li>
  <ol type="a">
    <li><input type="radio" name="q5" value="a" /> Không được cấp chứng nhận bảo hành từ nhà sản xuất</li>
    <li><input type="radio" name="q5" value="b" /> Có thể gây nhiều lỗi "bí hiểm", mất nhiều thời gian tìm kiếm và khắc phục</li>
    <li><input type="radio" name="q5" value="c" /> Không đảm bảo hỗ trợ các ứng dụng mạng hiện tại cũng như tương lai</li>
    <li class="t"><input type="radio" name="q5" value="d" /> Tất cả các nguy cơ trên</li>
  </ol>
  <li>Khách hàng mua kèm dịch vụ GOLD SUPPORT cho các dòng máy DTX CableAnalyzer được hưởng những đặc quyền nào?</li>
  <ol type="a">
    <li><input type="radio" name="q6" value="a" /> Miễn phí sửa chữa máy và cân chỉnh máy hằng năm tại hãng</li>
    <li><input type="radio" name="q6" value="b" /> Được hỗ trợ cước vận chuyển và cung cấp máy dùng tạm trong thời gian cân chỉnh, sửa chữa</li>
    <li><input type="radio" name="q6" value="c" /> Cập nhật software/firmware miễn phí</li>
    <li class="t"><input type="radio" name="q6" value="d" /> Tất cả các quyền lợi trên</li>
  </ol>
 </ol>
 <ul class="question">
  <li><strong class="red">CÂU HỎI PHỤ</strong>: có bao nhiêu người tham gia chương trình?</li>
  		<p><label id="subquestion" for="subquestion">Nhập con số dự đoán của bạn vào ô bên cạnh:</label> <input id="subquestion" type="text" class="inputbox required digits" value="" name="subquestion" maxlength="10" />
   </p>
 </ul>

<hr size="1" />
<p><strong>Cảm ơn bạn đã tham gia chương trình</strong>.</p>
<p class="note">Vui lòng nhập vào đầy đủ thông tin bên dưới và nhấn nút "Gửi" để gửi câu trả lời của bạn đến chúng tôi.
<br />
Các trường có dấu (*) là bắt buột phải nhập dữ liệu.</p>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td><label id="namemsg" for="name">Họ tên</label></td>
    <td><input type="text" name="name" id="name" class="inputbox required" value="" size="40" maxlength="200" /> (*)</td>
  </tr>
  <tr>
    <td><label id="" for="email">Email</label></td>
    <td><input type="text" name="email" id="email" class="inputbox required validate-email" value="" size="40" maxlength="100" /> (*)</td>
  </tr>
  <tr>
    <td><label for="phone">Điện thoại</label></td>
    <td><input type="text" name="phone" id="phone" class="inputbox required" value="" size="40" maxlength="100" /> (*)</td>
  </tr>
  <tr>
    <td><label for="company">Công ty</label></td>
    <td><input type="text" name="company" id="company" class="inputbox required" value="" size="40" maxlength="100" /> (*)</td>
  </tr>
  <tr>
    <td><label for="position">Chức vụ</label></td>
    <td><input type="text" name="position" id="position" class="inputbox required" value="" size="40" maxlength="50" /> (*)</td>
  </tr>
  <tr>
    <td></td>
    <td><input type="submit" value="&nbsp;Gửi&nbsp;" class="button validate" /></td>
  </tr>
</table>
<input type="hidden" name="option" value="com_nsp" />
<input type="hidden" name="task" value="save_fnet_question" />
</form>
<br />
<div style="border:1px dotted #CCC; background:#f2f2f2; font-size:11px; padding:10px; font-style:italic">
  <p><strong>Nhận ngay các phần thưởng giá trị khi tham gia trả lời các  câu hỏi trắc nghiệm về sản phẩm đo kiểm chứng nhận hệ thống cáp Fluke Networks  DTX-1800</strong>:<br />
  &nbsp; • 1 bộ Pocket Toner NX8 cho người trả lời đúng tất cả  các câu hỏi và trả lời câu hỏi phụ gần đúng nhất.<br />
  &nbsp; • 10 áo thun NSP cho 10 người trả lời đúng tất cả các  câu hỏi và trả lời câu hỏi phụ gần đúng tiếp theo.</p>
  <p><strong>Thể lệ chương trình:</strong><br />
  &nbsp; • Kéo dài từ ngày 01/10/2012 đến hết ngày 31/10/2012<br />
  &nbsp; • Người tham gia phải trả lời toàn bộ các câu hỏi  trong chương trình<br />
  &nbsp; • Giải thưởng sẽ được trao vào cuối chương trình vào  ngày 01/11/2012 tại trụ sở chính công ty NSP (359 Võ Văn Tần, P.5, Q.3, Tp. HCM)</p>
</div>

<style type="text/css">
ol.question > li{
	
	margin:7px 0;
	
}
ol.question > li, ul.question > li{
	font-size:14px;
	color:#000;
}
ol.question ol li{
	font-size:13px;
	color:#1E1E1E;
}
ol.heading li{
	font-weight:bold;
	font-size:14px;
	margin:10px 0;
}
</style>
