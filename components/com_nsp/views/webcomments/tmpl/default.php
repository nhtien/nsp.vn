<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JHTML::_('behavior.formvalidation' );
?>

<div class=""><h4>Chia sẽ cảm nghĩ về website của bạn cho chúng tôi.</h4></div>
<p>Vui lòng điền đầy đầy đủ thông tin bên dưới để góp ý về website cho chúng tôi.</p>
<form action="index.php?option=com_nsp&view=webcomments" method="post" class="form-validate">
<p>Tên của bạn<br /><input type="text" class="inputbox" name="name" /></p>
<p>
Địa chỉ Email<br />
<input type="text" value="<?php echo $user->email?>" name="email" class="inputbox" /></p>
<p>
<label for="lbmsg">Nội dung góp ý (*)</label><br />
<textarea name="message" class="inputbox required" rows="7" cols="70" id="lbmsg"></textarea>
</p>
<?php $mainframe->triggerEvent('onShowOSOLCaptcha', array(false)); ?>
<br />
<input type="submit" class="buttom validate" value="-Gửi-" />
<?php echo JHTML::_( 'form.token' ); ?>
<input type="hidden" name="option" value="com_nsp" />
<input type="hidden" name="task" value="save_webcommnents" />
</form>

