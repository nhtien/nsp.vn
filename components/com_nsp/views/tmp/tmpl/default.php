<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
?>
<div class="tmp">
	<h1 class="componentheading">ĐO KIỂM VÀ CHỨNG NHẬN HỆ THỐNG CÁP <br />
	  VỚI DTX CABLEANALYZER&#8482;</h1>
    <table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td><img src="images/tmp/2011-11-giam-gia-cuoi-nam/DTX-1800.jpg" border="0" hspace="10" /></td>
    <td style="padding-left:30px;">
    <h2>DTX-1800</h2>
    Đo chứng nhận cáp đồng, đảm bảo hệ thống cáp được thi công,<br />
      lắp đặt phù hợp với tiêu chuẩn TIA/ISO.<br />
      <ul>
      <li>Giảm chi phí đo chứng nhận hằng năm lên đến 33%.</li>
      <li>Được hầu hết các đơn vị đo kiểm sử dụng và tin tưởng</li>
      <li>Xác nhận độc lập bởi ETL đảm bảo đáp ứng tiêu chuẩn ISO cấp độ IV và TIA <br />
        cấp độ IIIe về độ chính xác.</li>
      <li>Phân tích kết quả đo kiểm và tạo bảng báo cáo chứng nhận chuyên nghiệp.</li>
      <li>Được xác nhận bởi hơn 20 công ty sản xuất cáp trên toàn thế giới.</li>
      <li>Thực hiện chứng nhận Tier-1 cho sợi quang nhanh gấp 5 lần với mô-đun quang DTX.</li>
       <li>Thực hiện chứng nhận Tier-2 cho sợi quang .</li>
       </ul>
       <h3>Thông tin chi tiết sản phẩm: </h3>
       <a href="http://www.flukenetworks.com/datacom-cabling/copper-testing/dtx-cableanalyzer-series" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/english_button2.jpg" border="0" /></a> <a href="http://nsp.com.vn/index.php/vi/san-pham/cabling-certification-a-testing/copper-certification-a-testing.html?page=shop.product_details&flypage=flypage.tpl&product_id=350&category_id=30" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/vietnam_button2.jpg" border="0" /></a>
            
            </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td><div class="giahotro"><span style="font-size:15px;"><strong>GIÁ HỖ TRỢ:</strong> </span>
        <br />
        <span style="font-size:19px;"><strong>182.000.000đ</strong></span><br />
Tặng kèm 1 bộ <a href="http://vietrack.com.vn/tmp/MMP.PDF" target="_blank" style="color:#FFF; text-decoration:underline;">MMP-KIT </a>
<br />
+ 1 suất học <a href="http://nsp.com.vn/index.php/vi/dao-tao-huan-luyen-ky-thuat.html?view=course&cid=8" style="color:#FFF; text-decoration:underline;">CCTT v2.1</a></div></td>
    <td>
    <h3>Điều kiện khuyến mãi:</h3>
    	<em>Chương trình có hiệu lực từ 15/11/2011 đến 29/02/2012 <br />
      hoặc khi hết số lượng sản phẩm hỗ trợ.</em></td>
  </tr>
</table>
<br />
<br />
<img src="images/tmp/2011-11-giam-gia-cuoi-nam/WEB-FUKLE4_33.jpg" border="0" />
<h2 style="margin-top:9px;">THANH LÝ HÀNG TRƯNG BÀY</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listPro">
  <tr>
    <td width="23%" align="center"><a href="http://www.flukenetworks.com/enterprise-network/network-testing/EtherScope-Series-II-Network-Assistant" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/EtherScope-Series-II-Network-Assistant.jpg" border="0" /></a></td>
    <td width="77%"><a href="http://www.flukenetworks.com/enterprise-network/network-testing/EtherScope-Series-II-Network-Assistant" target="_blank"><h5 style="padding-left:20px;">EtherScope&#8482; Series II <br />
      <span>Network Assistant</span></h5></a>
        <div class="prod_price">
        Giảm 50%<br />
        <span>75.000.000đ</span>
        </div>
      </td>
  </tr>
  <tr>
    <td colspan="2" style="padding-left:10px;">
    <ul>
    <li>Phân tích mạng LAN với kết nối đồng, kết nối quang và mạng wireless. Hỗ trợ Gigabit.</li>
    <li>Chẩn đoán "sức khỏe" hệ thống mạng LAN</li>
    <li>Khắc phục sự cố nhanh chóng với các hướng dẫn phân tích.</li>
    <li>Đo và đánh giá hiệu suất kết nối LAN/WAN, đảm bảo SLA.</li>
    <li>Kiểm tra tính sẵn sàng và khả năng đáp ứng của các dịch vụ mạng trong LAN </li></ul>
    </td>
    </tr>
</table>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="listPro">
  <tr>
    <td width="19%" align="center"><a href="http://vietrack.com.vn/tmp/Data_Sheet_1675522-SimpliFiber_72.pdf" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/EtherScope-Series-II-Network-Assistant-21.jpg" border="0" /></a></td>
    <td width="32%"><a href="http://vietrack.com.vn/tmp/Data_Sheet_1675522-SimpliFiber_72.pdf" target="_blank"><h4>SimpliFiber&#8482;<br />
  <span>Optical Loss Test Kit</span></h4></a>
      <div class="prod_price"> Giảm 35%<br />
      <span>11.700.000đ</span> </div></td>
    <td width="19%" align="center"><a href="http://www.flukenetworks.com/datacom-cabling/copper-testing/dtx-cableanalyzer-series" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/EtherScope-Series-II-Network-Assistant-23.jpg" border="0" /></a></td>
    <td width="30%"><a href="http://www.flukenetworks.com/datacom-cabling/copper-testing/dtx-cableanalyzer-series" target="_blank"><h4>DTX Patch Cord<br />
      <span>Test Adapter Set</span></h4></a>
      <div class="prod_price"> Giảm 35%<br />
      <span>11.000.000đ</span> </div></td>
  </tr>
  <tr>
    <td colspan="2"></td>
    <td colspan="2"></td>
  </tr>
  <tr>
    <td colspan="2" valign="top"><ul class="left">
      <li>Đo năng lượng và suy hao tín hiệu quang tại bước sóng 850nm và 1300nm.</li>
      <li>Khả năng lưu trữ lên  đến 100 kết quả đo, xuất ra máy tính với phần mềm ScanLink Tools</li>
      <li>Có khả năng chuyển đổi giữa giao diện ST và SC</li></ul></td>
    <td colspan="2" valign="top"><ul class="right">
      <li>Bộ chuyển đổi Giao diện DTX chứng nhận hiệu suất Patch Cord</li>
  <li>Chứng nhận theo tiêu chuẩn TIA CAT6/5e và ISO Class D/E</li>
    </ul></td>
  </tr>
  <!--
  <tr>
    <td><img src="images/tmp/2011-11-giam-gia-cuoi-nam/english_button.jpg" border="0" /></td>
    <td><img src="images/tmp/2011-11-giam-gia-cuoi-nam/english_button.jpg" border="0" /></td>
    <td><img src="images/tmp/2011-11-giam-gia-cuoi-nam/english_button.jpg" border="0" /></td>
  </tr>
  -->
</table>
<br /><br />
<img src="images/tmp/2011-11-giam-gia-cuoi-nam/WEB-FUKLE4_33.jpg" border="0" />
<h2 style="margin-top:9px;">GIẢM GIÁ CUỐI NĂM</h2>


<table width="100%" border="0" cellspacing="0" cellpadding="5" class="listPro">
  <tr>
    <td width="50%" valign="top">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" >
      <tr>
        <td align="center" class="thumb"><a href="http://www.flukenetworks.com/enterprise-network/network-testing/NetTool-Series-II-Inline-Network-Tester" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/NetTool-Series-II.jpg" border="0" /></a></td>
        <td><a href="http://www.flukenetworks.com/enterprise-network/network-testing/NetTool-Series-II-Inline-Network-Tester" target="_blank"><h4>NetTool&#8482; Series II<br /><span>Inline Network Tester</span>
        </h4></a>
        <div class="prod_price">
        Giảm 35%<br />
        <span>39.300.000đ</span>
        </div>
        </td>
      </tr>
      <tr>
        <td colspan="2"><ul class="left">
          <li>Phân tích NetProve: nhanh chóng cô lập thiết bị và ứng dụng có vấn đề về kết nối. </li>         
          <li>Khả năng Inline Gigabit: Xử lý sự cố mạng nhanh chóng với khả năng xen giữa và thấy được traffic giữa Switch, máy tính, IP phone và các thiết bị khác.</li>
          <li>Xác định spyware, malware, virus với tính năng giám sát port. Xử lý vấn đề chứng thực với log 802.1x </li>
          <li>Hỗ trợ xử lý sự cố VoIP          </li>
          <li>Hỗ trợ đo PoE          </li>
          <li>Hiển thị MAC, địa chỉ IP, subnet và dịch vụ được cung cấp bởi các máy chủ, router, switch đang hoạt động</li></ul></td>
        </tr>
    </table>
    </td>
    <td width="50%" valign="top">
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" class="thumb"><a href="http://www.flukenetworks.com/telecom-tools/network/Fiber-OneShot-Optical-Test-Set" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/Filber-Oneshot.jpg" border="0" /></a></td>
        <td><a href="http://www.flukenetworks.com/telecom-tools/network/Fiber-OneShot-Optical-Test-Set"><h4>Filber Oneshot&#8482;<br /> <span>Optical Test Set</span></h4></a>
        <div class="prod_price">
        Giảm 35%<br />
        <span>24.000.000đ</span>
        </div>
        </td>
      </tr>
      <tr>
        <td colspan="2"><ul class="right">
          <li>Xác định chiều dài của đường cáp nhanh chóng 
            <br />
            và chính xác.</li>
          <li>Cảnh báo khi có tín hiệu quang trên sợi quang.</li>
          <li>Hoạt động tức thì, không cần thời gian khởi động.</li>
          <li>Thao tác đơn giản và nhanh chóng với 1 nút TEST            </li>
          <li>Xác định lỗi trong đoạn cáp tối thiểu 1 mét </li>
        </ul></td>
        </tr>
    </table>
    </td>
  </tr>
  
  <tr><td colspan="2"><hr /></td></tr>
  
  <tr>
    <td width="50%" valign="top">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" >
      <tr>
        <td align="center" class="thumb"><a href="http://www.flukenetworks.com/datacom-cabling/copper-testing/MicroScanner-Cable-Verifier" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/MicroScanner2.jpg" border="0" /></a></td>
        <td><a href="http://www.flukenetworks.com/datacom-cabling/copper-testing/MicroScanner-Cable-Verifier"><h4>MicroScanner<sup>2</sup> <br />
          <span>Cable Verifier</span></h4></a>
        <div class="prod_price">
        Giảm 10%<br />
        <span>9.400.000đ</span>
        </div>
        </td>
      </tr>
      <tr>
        <td colspan="2"><ul class="left">
          <li>Hiển thị sơ đồ đấu dây (wiremap), chiều dài, ID cáp và khoảng cách đến điểm lỗi.</li>
          <li>Định vị sợi cáp trong bó cáp với tính năng phát âm digital và analog            </li>
          <li>Đa dụng với màn hình LCD rộng, hiệu ứng chiếu sáng sau (Backlit).</li>
        </ul></td>
        </tr>
    </table>
    </td>
    <td width="50%" valign="top">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" style="display:none;">
      <tr>
        <td align="center" class="thumb"><a href="http://vietrack.com.vn/tmp/MMP.PDF" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/MicroMapper-Pro-Kit.jpg" border="0" /></a></td>
        <td><a href="http://vietrack.com.vn/tmp/MMP.PDF" target="_blank"><h4>MicroMapper&#8482; <br />
          PRO Kit</h4></a>
        <div class="prod_price">
        Giảm 45%<br />
        <span>5.000.000đ</span>
        </div>
        </td>
      </tr>
      <tr>
        <td colspan="2"><ul class="right"><li>Kiểm tra sơ đồ đấy dây (Wire-map) cho cáp đôi xoắn và cáp đồng trục bao gồm chéo cặp và khoảng cách đến điểm đứt gãy.</li>
        <li>
Có chức năng phát tone analog</li></ul></td>
        </tr>
    </table>
    </td>
  </tr>

  <tr><td colspan="2"><hr /></td></tr>

  <tr>
    <td width="50%" valign="top">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" >
      <tr>
        <td align="center" class="thumb"><a href="http://www.flukenetworks.com/datacom-cabling/installation-tools/TS-90-Cable-Fault-Finder" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/TS-90-Cable.jpg" border="0" /></a></td>
        <td><a href="http://www.flukenetworks.com/datacom-cabling/installation-tools/TS-90-Cable-Fault-Finder"><h4>TS<sup>&reg;</sup>90 <br />
          <span>Cable Fault Finder</span></h4></a>
        <div class="prod_price">
        Giảm 30%<br />
        <span>3.200.000đ</span>
        </div>
        </td>
      </tr>
      <tr>
        <td colspan="2"><ul class="left">
          <li>Hoạt động với loại cáp có từ 2 lõi trở lên với chiều dài cáp lên đến 2500 ft (762 m).</li>
          <li>Kiểm tra tính liên tục, xác định khoảng cách đến điểm hở mạch hoặc điểm lỗi.            </li>
          <li>Có khả năng chịu độ ẩm cao </li>
        </ul></td>
        </tr>
    </table>
 
    </td>
    <td width="50%" valign="top">
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" class="thumb"><a href="http://www.flukenetworks.com/datacom-cabling/copper-testing/IntelliTone-Pro-Toner-and-Probe" target="_blank"><img src="images/tmp/2011-11-giam-gia-cuoi-nam/IntelliTone-Pro-Toner-and-Probe.jpg" border="0" /></a></td>
        <td><a href="http://www.flukenetworks.com/datacom-cabling/copper-testing/IntelliTone-Pro-Toner-and-Probe" target="_blank"><h4>IntelliTone&#8482; Pro <br />
          <span>Toner and Probe</span></h4></a>
        <div class="prod_price">
        Giảm 35%<br />
        <span>3.000.000đ</span>
        </div>
        </td>
      </tr>
      <tr>
        <td colspan="2"><ul class="right">
          <li>Cô lập, định vị sợi cáp trong bó, ngay cả khi cáp đang truyền dữ liệu. </li>
          <li>Xác định vị trí cáp, kiểm tra tính liên tục, phát hiện lỗi (hở mạch, ngắn mạch, đảo cặp).            </li>
          <li>Thay đổi âm thanh khi phát hiện ngắn mạch.</li>
        </ul></td>
        </tr>
    </table>
    </td>
  </tr>

</table>

</div>
<p>&nbsp;</p>
<style type="text/css">
.tmp ul{
	margin-left:0;
	padding-left:0;
}
.tmp ul li{
	margin-left:15px;
	padding-left:1px;
	text-align:justify;
}
.tmp h1{
	color:#0f3f6e;
	line-height:1.7;
	font-size:22px;
	text-align:center;
	background:#ffc313;
	padding:15px 0;
	margin-top:0;
}
.tmp h1, .tmp h2, .tmp h3, .tmp h4, .tmp h5{color:#0f3f6e;}
.tmp .giahotro{
	color:#FFF;
	background:url(<?php echo JURI::base()?>images/tmp/2011-11-giam-gia-cuoi-nam/WEB-FUKLE4_11.jpg) no-repeat;
	width:266px;
	height:95px;
	padding-top:10px;
	text-align:center;
}
.tmp .prod_price{
	color:#FFF;
	background:url(<?php echo JURI::base()?>images/tmp/2011-11-giam-gia-cuoi-nam/WEB-FUKLE4_42.jpg) no-repeat;
	width:120px;
	padding-left:15px;
	height:52px;
	padding-top:10px;
	text-align:center;
}
.tmp .prod_price span{
	font-size:17px;
	font-weight:bold;
}
.tmp table.listPro h4{
	padding-left:20px;
}
.tmp table.listPro td.thumb{
	width:35%;
	height:135px;
}
.tmp table.listPro ul{
	margin:5px 10px;
}
.tmp table.listPro ul.left{
	margin-right:20px;
}
.tmp table.listPro ul.right{
	margin-left:20px;
}

.tmp table.listPro h4 span, h5 span{
	font-weight:normal;
}
.tmp hr{
	border-top:none;
	border-bottom:1px dotted #999;
	
}

</style>
