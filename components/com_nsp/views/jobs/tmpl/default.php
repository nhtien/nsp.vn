<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
global $Itemid;
$show_detail = 1;
?>
<div class="view_brand view-jobs">

	<div class="featured">
		<div class="header t_r_setting7">
			<h3><?php echo JText::_('Careers')?></h3>
			<span class="view_all"><a href="index.php?option=com_nsp&view=jobs"><?php echo JText::_('View all')?> </a></span>
		</div><!--header-->
		<br />
		<div class="list_job">
			<?php
			$nColumns = 1;
			$i = 1;
				foreach( $this->jobs as $r){
				$link_job = "index.php?option=com_nsp&view=jobdetail&jid=".$r->id."&Itemid=$Itemid";
				
				?>
				<div class="item">
					<p><a class="title" href="<?php echo $link_job?>" title="<?php echo JText::_('View more...')?>"><?php echo $i .' . '. $r->job_title?></a></p>
					<?php if( $show_detail == 1 ):?>
					<table width="90%" border="0" cellspacing="0" cellpadding="3" class="tbl">
					  <tr>
						<td class="">Số lượng: <?php echo $r->number > 0 ? $r->number : '&nbsp;' ?></td>
					  </tr>
					  <tr>
						<td>
						<?php echo $r->job_short_desc?></td>
					  </tr>
					  <tr>
						<td>
                        	<p><b>Hạn nộp hồ sơ:</b> <?php echo date("d/m/Y",strtotime($r->end_date));?></p>
							<a href="<?php echo $link_job?>" class="more">Xem chi tiết</a> &nbsp; &nbsp; 
							<a href="index.php?option=com_nsp&view=jobapply&jid=<?php echo $r->id?>" class="apply">Gửi đơn trực tuyến</a>
						</td>
					  </tr>
					</table>
					
				<?php endif;?>
					
				</div>
				<?php
				$i++;
				}
			?>
			
		</div>
		<br />
		<p><strong><em>Liên hệ nộp hồ sơ & phỏng vấn trực tiếp tại:</em></strong></p>
		<strong>Địa chỉ:</strong> 54 Hoa Hồng, phường 02, quận Phú Nhuận, TP HCM<br />
		<strong>Điện thoại:</strong> (028) 3834 2108 – (+84) 77 571 3635 <br/>
		<strong>Email:</strong> hr@nsp.com.vn

	</div><!--featured-->
</div><!-- view-brand -->


