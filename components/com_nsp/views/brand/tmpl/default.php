<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
$tempimg = '<img src="images/spacer.gif" border="0" />';
$b = $this->brand;
?>
<div class="view_brand">
	<div class="top">
		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="border">
		<tr>
			<td class="top-left corner"><?php echo $tempimg;?></td><td class="top-middle"><?php echo $tempimg;?></td><td class="top-right corner"><?php echo $tempimg;?></td>
		</tr>
		<tr>
			<td class="body-left"><?php echo $tempimg;?></td>
			<td class="body-middle">
		<!-- body -->
			<!--<div class="logo">
				<div class="img">
					<img src="images/nsp/manufacturer/full_logos/<?php echo $b->mf_full_logo?>" title="<?php echo $b->mf_name?>" />
				</div>
				<div class="title">
					<h2><?php printf(JText::_('WELCOME TO THE BRAND SHOWCASE'), $b->mf_name) ?></h2>
					<span class="title"><?php echo $b->mf_title ?></span>
				</div>
			</div><!-- logo -->
			<div class="break">&nbsp;</div>
			<div class="description">
				<?php echo $b->mf_desc; ?>
			</div>
		
		<!-- end body -->
			</td>
			<td class="body-right"><?php echo $tempimg;?></td>
		</tr>
		<tr>
			<td class="bottom-left corner"><?php echo $tempimg;?></td><td class="bottom-middle"><?php echo $tempimg;?></td><td class="bottom-right corner"><?php echo $tempimg;?></td>
		</tr>
		</table>
	</div><!-- top -->
	
	<div class="featured">
		<div class="header t_r_setting7">
			<h3><?php printf(JText::_('FEATURED BRAND PRODUCTS'), $b->mf_name)?></h3>
			<span class="view_all"><a href="index.php?option=com_virtuemart&page=shop.browse&manufacturer_id=<?php echo $b->manufacturer_id?>&Itemid=127">
			<?php printf( JText::_('VIEW ALL BRAND PRODUCTS') , $b->mf_name) ?> </a></span>
		</div><!--header-->
		<br />
		<div class="list_p">
			<table cellpadding="5" cellspacing="0" border="0">
			<tr>
			<?php
			$nColumns = 3;
			$i = 1;
				foreach( $this->featuredProducts as $r){
					$imgFile = $r->product_thumb_image ? $r->product_thumb_image : '___1noimage.gif';
					$link_pro = "index.php?page=shop.product_details&flypage=flypage.tpl&product_id=$r->product_id&option=com_virtuemart&Itemid=26";
				?>
				<td class="item" width="<?php echo round(100/$nColumns,2)?>%" valign="top">
					<div class="img">
						<a href="<?php echo $link_pro?>">
							<img src="product_image.php?style=0&filename=<?php echo urlencode($imgFile)?>" title="<?php echo $r->product_name?>" />
						</a>
					</div>
					<div class="bt-more"><a href="<?php echo $link_pro?>"><img src="<?php echo JText::_('MORE_SRC_IMG')?>" /></a></div>
					<p>
						<a href="<?php echo $link_pro?>" class="name"><?php echo $r->product_name;?></a>
						<div class="des"><?php echo $r->product_s_desc ?></div>
					
				</td>
				<?php
				if( $i%$nColumns ==0 ) echo '</tr><tr>';
				$i++;
				}
			?>
			</tr></table>
		</div>
	</div><!--featured-->
</div><!-- view-brand -->