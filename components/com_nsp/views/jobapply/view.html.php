<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

class NspViewJobApply extends JView
{
	function display($tpl = null)
	{
		$user = &JFactory::getUser();
		JHTML::_('behavior.formvalidation' );
		
		$layout = JRequest::getVar('layout');
		if( $layout == 'completed' ){
			$this->setLayout('completed');
			parent::display($tpl);
			return;
		}
			
		$model =& $this->getModel();
		$row = &JTable::getInstance('jobapply','Table');
		if( $user->id ){
			$row->name = $user->name;
			$row->email = $user->email;
		}
		//$row->load(186);
		$this->assignRef('row', $row);
		$this->assignRef('lists', $this->getLists($row) );
		parent::display($tpl);
	}
	
	function getLists( &$row ){
		$model =& $this->getModel();
		$jid = $row->job_id ? $row->job_id : JRequest::getVar('jid',0);
		require(JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_nsp'.DS.'common.php');
		$birthday_month[] = JHTML::_('select.option','','---');
		for($i = 1 ; $i<=12 ; $i++)
			$birthday_month[] = JHTML::_('select.option',$i, $i);
			
		$lists['birthday_month']	= JHTML::_('select.genericList', $birthday_month,'birthday_month','class="inputbox required"','value','text', $row->birthday_month);
		
		$birthday_day[] = JHTML::_('select.option','','---');
		for( $i = 1 ; $i <= 31 ; $i++) $birthday_day[] = JHTML::_('select.option',$i, $i);
		$lists['birthday_day'] 		= JHTML::_('select.genericList', $birthday_day, 'birthday_day','class="inputbox required"', 'value', 'text', $row->birthday_day);
		
		$birthday_year[] = JHTML::_('select.option','','---');
		$start_y = (int)(date("Y") - 40);
		for( $i = $start_y ; $i <= date("Y") ; $i++) $birthday_year[] = JHTML::_('select.option',$i, $i);
		$lists['birthday_year'] 	= JHTML::_('select.genericList',$birthday_year, 'birthday_year','class="inputbox required"','value','text', $row->birthday_year);
		
		$gender_option[] = JHTML::_('select.option','1','Nam');
		$gender_option[] = JHTML::_('select.option','0','N&#7919;');
		$lists['gender'] = JHTML::_('select.radiolist',$gender_option,"gender",'class=""',"value","text", $row->gender,'gender');
		
		while( list($k,$v) = each($exp_company_type) ){
			$exp_company_type_op[] = JHTML::_('select.option',$k, $v);
		}
		$lists['exp_company_type'] = JHTML::_('select.radiolist',$exp_company_type_op,'exp_company_type[]','class=""','value','text','','');
		
		while(list($k,$v) = each($exp_time_type)){
			$exp_time_type_op[] = JHTML::_('select.option',$k, $v);
		}
		$lists['exp_time_type'] = JHTML::_('select.radiolist',$exp_time_type_op,'exp_time_type[]','class=""','value','text','','');
		
		/* Job Title */
		$row_jobs = $model->getJobs(); 
		$job_title_option[] = JHTML::_('select.option','','');
		foreach( $row_jobs as $r){
			$job_title_option[] = JHTML::_('select.option',$r->id, $r->job_title);
		}
		$lists['job_title'] = JHTML::_('select.genericList', $job_title_option,'job_id','class="inputbox required" onchange="getJobTitle();"','value','text',$jid);
		
		/* capcha */
		$key = substr( md5(microtime() * mktime()), 0, 5);
		$temp_session = $_SESSION; 
		session_write_close();
		ini_set('session.save_handler','files'); 
		//session_start();
		$_SESSION['flipping'] = $key; 
		session_write_close();
		ini_set('session.save_handler','user'); 
		$jd = new JSessionStorageDatabase();
		$jd->register('flippingkeyreset');//
		session_start();
		$_SESSION = $temp_session;
		$e = session_id();
		$se = &JFactory::getSession();
		$se->set('flippingkeyreset',$key);
		$cap_path = JURI::root() . 'components/com_flippingcomic/helpers/captcha.php?sid=' . $e;
		
		return $lists;
	}
}
//document.getElementById(\'hdposition\').value = 
?>