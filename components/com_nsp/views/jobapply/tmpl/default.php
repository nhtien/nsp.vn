<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
$lists = $this->lists; 
$row = $this->row;
?>
<script language="javascript">
function addField(what,index) { 
	if(document.getElementById) {
		tr = what;
		while (tr.tagName != 'TR') tr = tr.parentNode;
		tr = tr.previousSibling;
		var newTr = tr.parentNode.insertBefore(tr.cloneNode(true),tr.nextSibling);
		checkForLast(index);
		checkForMax(index);
	}
}
function show (what) {
	what.style.display = "block"; 
}

function hide (what) {
	what.style.display = "none";
}

function checkForMax(index){
	btnsminus_f = document.getElementsByName('minus_f'+index);
	document.getElementsByName('plus_f'+index)[0].disabled = (btnsminus_f.length > 5) ? true : false;
}

function checkForLast(index){
	btnsminus_f = document.getElementsByName('minus_f'+index);
	for (i = 0; i < btnsminus_f.length; i++){
		btnsminus_f[i].className = "addField";
		if (btnsminus_f.length > 1){
			btnsminus_f[i].disabled = false;
			show(btnsminus_f[i]);
		}
		else{
			btnsminus_f[i].disabled = true;
			hide(btnsminus_f[i]);
		}
	}
	btnsminus_l = document.getElementsByName('minus_l'); 
	for (i = 0; i < btnsminus_l.length; i++){
		btnsminus_l[i].className = "addfield";
		if (btnsminus_l.length > 1){
			btnsminus_l[i].disabled = false;
			show(btnsminus_l[i]);
		}
		else{
			btnsminus_l[i].disabled = true;
			hide(btnsminus_l[i]);
		}
	}
}

function dropField(what,index){
	tr = what;
	while (tr.tagName != 'TR') tr = tr.parentNode;
	tr.parentNode.removeChild(tr);
	checkForLast(index);
	checkForMax(index);
}
function checkApplyForm(){
	if(document.getElementById('birthday_day').value==''){
		document.getElementById('msgbirthday').className='invalid';
		return false;
	}

}
function onSubmitApplyForm(){
	if( document.getElementsByName('gender').value==''){
		return false;
	}
	
	var f_listen = document.getElementsByName('foreign_lang_listen[]'); 
	var f_talk = document.getElementsByName('foreign_lang_talk[]'); 
	var f_read = document.getElementsByName('foreign_lang_read[]'); 
	var f_write = document.getElementsByName('foreign_lang_write[]'); 
	for( i=0 ; i < f_listen.length ; i++){ 
		if( f_listen[i].checked == false ) { 
			f_listen[i].value='0';
			f_listen[i].checked = true;
		}
		if( f_talk[i].checked == false ) { 
			f_talk[i].value='0';
			f_talk[i].checked = true;
		}
		if( f_read[i].checked == false ) { 
			f_read[i].value='0';
			f_read[i].checked = true;
		}
		if( f_write[i].checked == false ) { 
			f_write[i].value='0';
			f_write[i].checked = true;
		}
	}
	
	return true;
}
function getJobTitle(){
	var text = document.getElementById('job_id').options[document.getElementById('job_id').selectedIndex].text;
	document.getElementById('hdposition').value = text; 
}
</script>
<h2>Thông tin ứng viên</h2>
<div class="jobapply-form">
<p class="note"><label class="">Vui lòng điền vào đầy đủ thông tin bên dưới để gửi thông tin của bạn cho chúng tôi. Những trường có dấu (*) là bắt buộc phải nhập dữ liệu.</label></p>
<form action="" method="post" enctype="multipart/form-data" class="form-validate" onsubmit="return onSubmitApplyForm();">
	<div class="Private-info">
	<p> 
	<table class="tbl" width="auto" border="0" cellpadding="2" cellspacing="0">
	  <tr>
	    <td class="lb"><label for="job_id">Chức danh dự tuyển: (*) &nbsp; </label></td>
		<td><?php echo $lists['job_title']?></td>
	  </tr>
	    <td><label for="jbexpected-salary">Mức lương mong muốn </label></td>
		<td> <input type="text" name="expected_salary" id="jbexpected-salary" class="inputbox" maxlength="50" value="<?php echo $row->expected_salary;?>" /></td>
	  </table>
	</table>
		<fieldset>
			<legend>Thông tin cá nhân</legend>
			<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tbl">
			  <tr>
				<td class="lb"><label id="lbname" for="jbname">Họ và Tên (*) </label></td>
				<td><label>
				  <input type="text" name="name" id="jbname" class="inputbox required" maxlength="50" value="<?php echo $row->name;?>" />
				</label></td>
				<td class="lb"><label for="gender">Giới tính (*)</label> </td>
				<td><?php echo $lists['gender']?></td>
			  </tr>
			  <tr>
				<td class="lb"><label id="msgbirthday">Ngày sinh (*)</label> </td>
				<td><label for="birthday_day">Ngày</label> 
				<?php echo $lists['birthday_day']?> <label for="birthday_month">Tháng </label><?php echo $lists['birthday_month']?> <label for="birthday_year">Năm</label> 
				<?php echo $lists['birthday_year']?></td>
				<td class="lb"><label for="jbbirthplace">Nơi sinh (*)</label></td>
				<td><input type="text" name="birth_place" id="jbbirthplace" class="inputbox required" maxlength="50" value="<?php echo $row->birth_place;?>" /></td>
			  </tr>
			  <tr>
				<td class="lb"><label id="emailmsg" for="jbemail">Email (*)</label></td>
				<td><input type="text" name="email" id="jbemail" class="inputbox required validate-email" maxlength="50" value="<?php echo $row->email;?>" /></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			  
			  <tr>
				<td class="lb"><label id="lbaddress" for="jbaddress">Địa chỉ thường trú (*)</label></td>
				<td><input type="text" name="address" id="jbaddress" class="inputbox required" maxlength="200" size="35" value="<?php echo $row->address?>" /></td>
				<td class="lb"><label id="lbmobilephone" for="jbmobilephone">Điện thoại di động (*)</label></td>
				<td><input type="text" name="mobilephone" id="jbmobilephone" class="inputbox required" maxlength="50" value="<?php echo $row->mobilephone?>" /></td>
			  </tr>
			  <tr>
				<td class="lb"><label id="lbaddress2" for="jbaddress2">Địa chỉ liên lạc (*)</label></td>
				<td><input type="text" name="address2" id="jbaddress2" maxlength="255" class="inputbox required" size="35" value="<?php echo $row->address2?>" /></td>
				<td class="lb">Điện thoại </td>
				<td><input type="text" name="phone" maxlength="50" class="inputbox" value="<?php echo $row->phone?>" /></td>
			  </tr>
			  <tr>
				<td class="lb">hình ảnh</td>
				<td colspan="3"><input name="imagefile" type="file" /><label class="note"> (các định dạng file chấp nhận: .jpg, .gif, .png, .bmp)</label></td>
				</tr>
				<tr>
				<td class="lb">File đính kèm</td>
				<td colspan="3"><input name="attachment" type="file" /> <label class="note">(các định dạng file chấp nhận: .rar, .zip, .doc, .pdf, txt)</label></td>
			  </tr>
			</table>

		</fieldset>
	</div><!--private-info-->
	
	<div class="school-info">
		<fieldset>
			<legend>Thông tin học vấn - Kỹ năng</legend>
			<h4>Quá trình học tập</h4>
			<div id="id_learning">
				<table border="0" cellpadding="0" cellspacing="0" class="middle_form">
					<tr height="30">
						<td valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tbl">
							  <tr>
								<td class="lb">Thời gian đào tạo (Từ ...đến...)</td>
								<td><input type="text" name="learning_time[]" class="inputbox" maxlength="50" /></td>
								<td class="lb">Trường/Đơn vị đào tạo </td>
								<td><input type="text" name="learing_names[]" class="inputbox" maxlength="255" size="35" /></td>
							  </tr>
							  <tr>
								<td class="lb">Chuyên ngành</td>
								<td><input type="text" name="learning_subjects[]" class="inputbox" maxlength="255" /></td>
								<td class="lb">Bằng cấp</td>
								<td><input type="text" name="learning_qualifications[]" class="inputbox" maxlength="255" /></td>
							   </tr>
							   <tr><td colspan="4">&nbsp;</td></tr>
							 </table>
						</td>
						<td width="50" align="center" valign="top">
							<input type="button" name="minus_f1" value="&#8212;" onClick="dropField(this,1);" class="addfield" style="display:none;" title="Xóa">
						</td>
					</tr>
					<tr height="">
						<td colspan="2" valign="top">
						
							<!--<input type="button"  name="plus_f1" value="Thêm..." onClick="addField(this,1);" class="bt_more">-->
						</td>
					</tr>
				</table>
				</div>
				<p class="note"><i>Để tiếp tục nhập các dự liệu tương tự cho mục quá trình học tập, bạn vui lòng click vào nút <strong>Thêm</strong> bên dưới.</i></p>
				<input type="button"  name="plus_f1" value="Thêm..." onClick="addField1('id_learning',id_learning, 1);" class="bt_more">

			<h4>Các khóa đào tào</h4>
				<div id="id_training">
				<table border="0" cellpadding="0" cellspacing="0" class="middle_form">
					<tr height="30">
						<td valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tbl">
							  <tr>
								<td class="lb">Thời gian đào tạo (Từ ...đến...)</td>
								<td><input type="text" name="training_time[]" class="inputbox" maxlength="50" /></td>
								<td class="lb">Trường/Đơn vị đào tạo </td>
								<td><input type="text" name="training_names[]" class="inputbox" maxlength="255" size="35" /></td>
							  </tr>
							  <tr>
								<td class="lb">Chuyên ngành</td>
								<td><input type="text" name="training_subjects[]" class="inputbox" maxlength="255" /></td>
								<td class="lb">Bằng cấp</td>
								<td><input type="text" name="training_qualifications[]" class="inputbox" maxlength="255" /></td>
							   </tr>
							   <tr><td colspan="4">&nbsp;</td></tr>
							 </table>
						</td>
						<td width="50" align="center" valign="top">
							<input type="button" name="minus_f2" value="&#8212;" onClick="dropField(this,2);" class="addfield" style="display: none;" title="Xóa">
						</td>
					</tr>
					<tr height="">
						<td colspan="2" valign="top">
						
							<!--<input type="button"  name="plus_f2" value="Thêm..." onClick="addField(this,2);" class="bt_more">-->
						</td>
					</tr>
				</table>
				</div>
				<p class="note"><i>Để tiếp tục nhập các dự liệu tương tự cho mục Các khóa đào tạo, bạn vui lòng click vào nút <strong>Thêm</strong> bên dưới.</i></p>
				<input type="button"  name="plus_f2" value="Thêm..." onClick="addField1('id_training',id_training, 2);" class="bt_more">


			<h4>Ngoại ngữ</h4>
				<div id="foreign_lang">
				<table border="0" cellpadding="0" cellspacing="0" class="middle_form">
					<tr height="30">
						<td valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tbl">
							  <tr>
								<td class="lb">Ngoại ngữ</td>
								<td><input type="text" name="foreign_lang[]" class="inputbox" maxlength="255" size="30" /></td>
								<td class="lb">Trình độ </td>
								<td><input type="text" name="foreign_lang_level[]" class="inputbox" maxlength="255" /></td>
							  </tr>
							  <tr>
								<td class="lb">Nơi cấp</td>
								<td><input type="text" name="foreign_lang_school[]" class="inputbox" maxlength="255" size="30" /></td>
								<td class="lb">Khả năng</td>
								<td>
								<input type="checkbox" name="foreign_lang_listen[]" value="1"  /> Nghe
								<input type="checkbox" name="foreign_lang_talk[]" value="1"  /> Nói
								<input type="checkbox" name="foreign_lang_read[]" value="1"  /> Đọc
								<input type="checkbox" name="foreign_lang_write[]" value="1"  /> Viết
								</td>
							   </tr>
							   <tr><td colspan="4">&nbsp;</td></tr>
							 </table>
						</td>
						<td width="50" align="center" valign="top">
							<input type="button" name="minus_f3" value="&#8212;" onClick="dropField(this,3);" class="addfield" style="display: none;" title="Xóa">
						</td>
					</tr>
					<tr height="0">
						<td colspan="2" valign="top">
						</td>
					</tr>
				</table>
				</div>
				<p class="note"><i>Để tiếp tục nhập các dự liệu tương tự cho mục Ngoại ngữ, bạn vui lòng click vào nút <strong>Thêm</strong> bên dưới.</i></p>
					<input type="button"  name="plus_f3" value="Thêm..." onClick="addField1('foreign_lang',foreign_lang, 3);" class="bt_more">
				
				<h4>Kỹ năng</h4>
				<p>
					<label class="lb">Vi tính</label><br />
					Nêu các phần mềm vi tính biết sử dụng thành thạo:<br />
					<textarea name="computure_software" rows="3" cols="70"><?php echo $row->computure_software;?></textarea>
				</p>
				<p>
					<label class="lb">Máy móc văn phòng: các máy móc văn phòng biết sử dụng:</label><br />
					<textarea name="office_machine" rows="3" cols="70"><?php echo $row->office_machine?></textarea>
				</p>
				<p>
					<label class="lb">Các kỹ năng khác:</label><br />
					<textarea name="other_skills" rows="3" cols="70"><?php echo $row->other_skills;?></textarea>
				</p>


			<h4>Kinh nghiệm làm việc <span class="note">(Liệt kê theo thứ tự những công việc gần nhất)</span></h4>
			  <div id="experience">
				<table border="0" cellpadding="0" cellspacing="0" class="middle_form">
					<tr height="30">
						<td valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tbl">
							  <tr>
								<td class="lb">Thời gian (Từ ...đến...)</td>
								<td><input type="text" name="exp_time[]" class="inputbox" maxlength="255" /></td>
								<td class="lb">Tên đơn vị </td>
								<td><input type="text" name="exp_company_name[]" class="inputbox" maxlength="255" size="30" /></td>
							  </tr>
							  <tr>
								<td class="lb">Ngành hoạt động</td>
								<td><input type="text" name="exp_activity[]" class="inputbox" maxlength="255" size="30" /></td>
								<td class="lb">Điện thoại/Fax liên lạc:</td>
								<td><input type="text" name="exp_phone_fax[]" class="inputbox" maxlength="255" />
								
								</td>
							   </tr>
							  <tr>
								<td class="lb">Loại hình</td>
								<td colspan=3""><?php echo $lists['exp_company_type']?></td>
							  </tr>
							  <tr>
								<td class="lb">Chức danh</td>
								<td><input type="text" name="exp_position[]" class="inputbox" maxlength="255" value="" /></td>
								<td class="lb">Mô  tả ngắn công việc:</td>
								<td><textarea name="exp_description[]" class="inputbox" rows="3" cols="30"></textarea></td>
							  </tr>
							  <tr>
								<td class="lb">Loại hình</td>
								<td colspan="3">
								<?php echo $lists['exp_time_type'];?>
								</td>
							  </tr>
							  
							  <tr>
								<td class="" colspan="4">Tên & chức vụ cấp quản lý trực tiếp &nbsp; 
<input type="text" name="exp_boss[]" class="inputbox" maxlength="255" value="" size="30" /></td>
							  </tr>
							  <tr>
								<td class="lb">Số nhân viên phụ trách</td>
								<td><input type="text" name="exp_total_staff[]" class="inputbox" /></td>
								<td class="lb">Thu nhập</td>
								<td><input type="text" name="exp_loan[]" class="inputbox" maxlength="255" value="" /></td>
							  </tr>
							  
							  <tr>
								<td class="lb">Lý do thôi việc:</td>
								<td colspan="2"><textarea name="exp_discontinue[]" class="inputbox" rows="3" cols="35"></textarea></td>
								<td>&nbsp;</td>
							  </tr>
							   <tr><td colspan="4">&nbsp;</td></tr>
							 </table>
						</td>
						<td width="50" align="center" valign="top">
							<input type="button" name="minus_f4" value="&#8212;" onClick="dropField(this,4);" class="addfield" style="display: none;" title="Xóa">
						</td>
					</tr>
					<tr height="">
						<td colspan="2" valign="top">
						<hr />
						</td>
					</tr>
				</table>
			  </div>
			  <p class="note"><i>Để tiếp tục nhập các dự liệu tương tự cho mục Kinh nghiệm làm việc, bạn vui lòng click vào nút <strong>Thêm</strong> bên dưới.</i></p>
				<input type="button"  name="plus_f4" value="Thêm..." onClick="addField1('experience',experience,4);" class="bt_more">
		</fieldset>
		</div><!--school-info-->
		<div class="other-info">
			<fieldset>
				<legend>Các thông tin khác</legend>
				Cho biết về người quen (họ hàng, bạn bè,...) đang làm việc tại NSP:
				<div id="reference">
				<table border="0" cellpadding="0" cellspacing="0" class="middle_form">
					<tr height="30">
						<td valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tbl">
							  <tr>
								<td class="lb">Quan hệ</td>
								<td><input type="text" name="reference[]" class="inputbox" maxlength="50" /></td>
								<td class="lb">Họ và tên</td>
								<td><input type="text" name="ref_name[]" class="inputbox" maxlength="255" size="" /></td>
							  </tr>
							  <tr>
								<td class="lb">chức vụ</td>
								<td><input type="text" name="ref_position[]" class="inputbox" maxlength="255" /></td>
								<td class="lb">Bộ phận</td>
								<td><input type="text" name="ref_part[]" class="inputbox" maxlength="255" /></td>
							   </tr>
							   <tr><td colspan="4">&nbsp;</td></tr>
							 </table>
						</td>
						<td width="50" align="center" valign="top">
							<input type="button" name="minus_f5" value="&#8212;" onClick="dropField(this,5);" class="addfield" style="display: none;" title="Xóa">
						</td>
					</tr>
					<tr height="">
						<td colspan="2" valign="top">
							
						</td>
					</tr>
				</table>
				</div>
						<p class="note"><i>Để tiếp tục nhập các dự liệu tương tự cho mục người quen, bạn vui lòng click vào nút <strong>Thêm</strong> bên dưới.</i></p>
							<input type="button"  name="plus_f5" value="Thêm..." onClick="addField1('reference',reference,5);" class="bt_more">
				
				<br />
				Cho biết về người quen (họ hàng, bạn bè,...) đang làm việc tại công ty khác:
				<div id="reference1">
				<table border="0" cellpadding="0" cellspacing="0" class="middle_form">
					<tr height="30">
						<td valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="2" class="tbl">
							  <tr>
								<td class="lb">Quan hệ</td>
								<td><input type="text" name="reference1[]" class="inputbox" maxlength="50" /></td>
								<td class="lb">Họ và tên</td>
								<td><input type="text" name="ref_name1[]" class="inputbox" maxlength="255" size="" /></td>
							  </tr>
							  <tr>
								<td class="lb">chức vụ</td>
								<td><input type="text" name="ref_position1[]" class="inputbox" maxlength="255" /></td>
								<td class="lb">Bộ phận</td>
								<td><input type="text" name="ref_part1[]" class="inputbox" maxlength="255" /></td>
							   </tr>
							   <tr><td colspan="4">&nbsp;</td></tr>
							 </table>
						</td>
						<td width="50" align="center" valign="top">
							<input type="button" name="minus_f6" value="&#8212;" onClick="dropField(this,6);" class="addfield" style="display: none;" title="Xóa">
						</td>
					</tr>
					<tr height="30">
						<td colspan="2" valign="top">
						</td>
					</tr>
				</table>
				</div>
				<p class="note"><i>Để tiếp tục nhập các dự liệu tương tự cho mục người quen, bạn vui lòng click vào nút <strong>Thêm</strong> bên dưới.</i></p>
				<input type="button"  name="plus_f6" value="Thêm..." onClick="addField1('reference1',reference1,6);" class="bt_more">
				
				<br />
				<p><label for="reference1_detail">Nêu tên, chức vụ, nơi công tác của 2 người (không phải người thân) biết rõ về quá trình của bạn để NSP tham khảo nếu cần thiết.(*)</label></p>
				<table cellpadding="3" cellspacing="0" border="0">
				<tr><td>1.</td><td>2.</td></tr>
				<tr><td valign="top">
					<textarea name="reference1_detail" id="reference1_detail" rows="4" cols="35"  class="inputbox required"><?php echo $row->reference1_detail?></textarea>
				</td><td><textarea name="reference2_detail" rows="4" cols="35" class="inputbox"><?php echo $row->reference2_detail?></textarea></td>
				</tr>
				</table>
				<br />
				<p>Tự nhập xét về những điểm mạnh và điểm yếu (*) </p>
				<table cellpadding="3" cellspacing="0" border="0">
				<tr><td><label for="strength">Điểm mạnh (*) </label></td><td><label for="weakness">Điểm yếu (*) </label></td></tr>
				<tr><td valign="top">
					<textarea name="strength" id="strength" rows="4" cols="35"  class="inputbox required"><?php echo $row->strength?></textarea>
				</td><td><textarea name="weakness" id="weakness" rows="4" cols="35" class="inputbox required"><?php echo $row->weakness?></textarea></td>
				</tr>
				</table>
			</fieldset>
		</div>
		<?php $mainframe->triggerEvent('onShowOSOLCaptcha', array(false)); ?>
		<br />
		<div class=""><input type="checkbox" name="confirm" id="user_confirm" value="1" class="required" /> <label id="" for="user_confirm">Tôi cam đoan những thông tin trên đây là hoàn toàn đứng với sự thật và chịu hoàn toàn trách nhiệm về những điều đã khai.(*)</label> 
		</div>
		<br />
		

	<input type="submit" name="submit" value="Gửi" class="button validate" />
	<input type="reset" name="reset" value="Hủy" class="button" />
	
	<input type="hidden" name="option" value="com_nsp" />
	<input type="hidden" name="task" value="save_jobapply" />
	<input type="hidden" name="position" id="hdposition" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
</div><!-- jobapply-form -->
			<script language="javascript"> 
			var id_learning = document.getElementById('id_learning').innerHTML;
			var id_training = document.getElementById('id_training').innerHTML;
			var foreign_lang = document.getElementById('foreign_lang').innerHTML;
			var experience = document.getElementById('experience').innerHTML;
			var reference = document.getElementById('reference').innerHTML;
			var reference1 = document.getElementById('reference1').innerHTML;
			
			//alert(document.getElementById('id_learning').inner);
			
			function addField1(id, content, index ){
				var obj = document.getElementById(id);
				obj.innerHTML = obj.innerHTML + content;
				checkForLast(index);
				checkForMax(index);

			}
			
			</script>
			
				
				
				
