<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
?>
<p>Bạn đã nộp đơn thành công! Chúng tôi sẽ xem hồ sơ và liên hệ với bạn trong thời gian sớm nhất!<br />
</p>