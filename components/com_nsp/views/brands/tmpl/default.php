<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
?>
<div class="view_brand">

	<div class="featured">
		<div class="header t_r_setting7">
			<h3><?php echo JText::_('Featured Brand Showcases')?></h3>
			<span class="view_all"><a href="index.php?option=com_nsp&view=browsecategories&Itemid=18"><?php echo JText::_('Browse All Categories')?> </a></span>
		</div><!--header-->
		<br class="break" />&nbsp;
		<div class="list_p">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
			<?php
			$nColumns = 3;
			$i = 1;
				foreach( $this->brands as $r){
				$link_brand = "index.php?option=com_nsp&view=brand&bid=".$r->manufacturer_id."&Itemid=72";
				
				?>
				<td class="item" width="<?php echo round(100/$nColumns,2)?>%" valign="top">
					<div class="img">
						<a href="<?php echo $link_brand?>">
							<img src="images/nsp/manufacturer/full_logos/<?php echo $r->mf_full_logo?>" title="<?php echo $r->mf_name?>" />
						</a>
					</div>
					<p>
						<a href="<?php echo $link_brand?>" class="name"><?php echo $r->product_name;?></a>
						<div class="des"><?php echo $r->product_s_desc ?></div>
					
				</td>
				<?php
				if( $i%$nColumns ==0 ) echo '</tr><tr>';
				$i++;
				}
			?>
			</tr></table>
		</div>
	</div><!--featured-->
</div><!-- view-brand -->


