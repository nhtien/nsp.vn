<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.view');

class NspViewBrowseCategories extends JView
{
	function display($tpl = null)
	{
			
		$model =& $this->getModel();
		$categories = $model->getCategories();	
		//var_dump($categories); exit;		
		$this->assignRef('categories', $categories);
		parent::display($tpl);
	}
}

?>