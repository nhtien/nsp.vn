<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
?>
<div id="browse_all_categories">

<?php if (count($this->categories) > 0){?>
<h3 class="header"><?php echo JText::_('Browse All Categories');?></h3>
<?php
/* the number of column  */
$numColumn = 3; 
$iColumn = 0;
?>
<div class="inner">
<table width="100%" height="100%" border="0" cellpadding="4" class="content" cellspacing="0"><tr>
<?php foreach($this->categories as $c):
	 if($iColumn%$numColumn==0&&$iColumn!=0) {
	 	echo '</tr><tr>';
	 }
	 ?>
	 	<td  width="<?php echo (100/$numColumn); ?>%" align="left" valign="top">
			<a href="index.php?option=com_virtuemart&page=shop.browse&category_id=<?php echo $c->category_id?>"><h4 class="title title_<?php echo $c->category_id?>" style="background-image:url(images/category-icons/<?php echo $c->category_id?>.png);"><?php echo $c->category_name ?></h4></a>
			<?php
			if( $c->child ){
				echo '<ul>';
				foreach( $c->child as $ch ){
					echo '<li><a href="index.php?option=com_virtuemart&page=shop.browse&category_id='.$ch->category_id.'&Itemid=26">'.$ch->category_name.'</a></li>';
				}
				echo '</li>';
			}
			?>
		</td>
	<?php
		$iColumn++;
	 endforeach; ?>
</tr></table>
<?php }else{
	echo "There is not any categories";
}
?>
</div>
</div>

