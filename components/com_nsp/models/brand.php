<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class NspModelBrand extends JModel
{
	var $id = null;
	
	function __construct(){
		$this->_id = JRequest::getVar('bid',0);
		parent::__construct();
		
	}
		
	function getBrand( ){
		$db = &JFactory::getDBO();
		$query = "SELECT mf.* FROM #__vm_manufacturer AS mf WHERE mf.manufacturer_id = ".$this->_id;
		$db->setQuery( $query ); 
		$rows = $db->loadObject();
		return $rows;
	}
	function getFeaturedProducts(){
		$db = &JFactory::getDBO();
		$where[] = "p.product_publish='Y'";
		$where[] = "p.product_special = 'Y'";
		$where[] = "mf.manufacturer_id = ".$this->_id;
		$where = " WHERE ".implode(" AND ", $where);
		$sql = "SELECT * FROM #__vm_product AS p 
					INNER JOIN #__vm_product_mf_xref AS mf ON mf.product_id = p.product_id"
					.$where
					;
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		return $rows;
	}
	
}

?>