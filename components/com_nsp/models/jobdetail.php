<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class NspModelJobDetail extends JModel
{
	var $id = null;
	
	function __construct(){
		$this->_id = JRequest::getVar('jid',0);
		parent::__construct();
		
	}
		
	function getJob( ){
		$db = &JFactory::getDBO();
		$query = "SELECT * FROM #__nsp_job AS j WHERE j.id=".$this->_id;
		$db->setQuery( $query ); 
		$row = $db->loadObject();
		return $row;
	}
	
}

?>