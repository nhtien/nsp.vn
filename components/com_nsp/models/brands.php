<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class NspModelBrands extends JModel
{
	var $id = null;
	
	function __construct(){
		$this->_id = JRequest::getVar('bid',0);
		parent::__construct();
		
	}
		
	function getBrands( ){
		$db = &JFactory::getDBO();
		$query = "SELECT mf.* FROM #__vm_manufacturer AS mf WHERE mf.published=1 ORDER BY mf.mf_name";
		$db->setQuery( $query ); 
		$rows = $db->loadObjectList();
		return $rows;
	}
	
}

?>