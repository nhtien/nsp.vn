<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );

class NspModelBrowseCategories extends JModel
{
	var $_Categories = null;
		
		
	function getCategories()
	{
		if(!$this->_Categories)
		{
			$orderby = "ORDER BY c.list_order";
			$query = "SELECT c.* FROM #__vm_category_xref AS cx
						INNER JOIN #__vm_category AS c ON c.category_id = cx.category_child_id
						WHERE cx.category_parent_id = 0 AND category_publish = 'Y' ".$orderby;
			$this->_db->setQuery($query);
			$this->_Categories = $this->_getList($query, 0, 0);		
		}
		$rows = $this->_Categories;
		$data = array();
		foreach( $rows as $r){
			$obj = new stdClass();
			$obj = $r;
			$obj->child = $this->_getSubCategories( $obj->category_id);
			
			$data[] = $obj;
		}
		return $data;
	}
	
	function _getSubCategories( $catid ){
		$db = &JFactory::getDBO();
		$orderby = "ORDER BY c.list_order";
		$query = "SELECT c.* FROM #__vm_category_xref AS cx
					INNER JOIN #__vm_category AS c ON c.category_id = cx.category_child_id
					WHERE cx.category_parent_id = $catid ".$orderby;
		$db->setQuery( $query ); 
		$rows = $db->loadObjectList();
		return $rows;
	}
	
}

?>