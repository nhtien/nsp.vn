function dates(){ 
	var now = new Date();
	var yr = now.getYear();
	var dayarray=new Array("Ch&#7911; nh&#7853;t","Th&#7913; hai","Th&#7913; ba","Th&#7913; t&#432;","Th&#7913; n&#259;m","Th&#7913; s&#225;u","Th&#7913; b&#7843;y");
	
	if (yr <= 1000) { yr += 1900 }

	var month = now.getMonth() + 1;
	var day = now.getDay()
	var dayNr = now.getDate();
	var hours = now.getHours();
	var minutes = ((now.getMinutes() < 10) ? ":0" : ":") + now.getMinutes();

	if(month < 10)	month="0" + month;
	if(dayNr < 10)	dayNr="0" + dayNr;

	// String to display month, day, & year.
	var MonthDayYear =(dayarray[day] + " ng&agrave;y " + dayNr + "/" + month + "/" + yr);

	// String to display current time.
	var TimeValue =(hours + minutes);
	//return MonthDayYear + " " + TimeValue;
	document.write(MonthDayYear + " - " + TimeValue);
}