/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/

function nspOpenWin(url,title,w,h,menubar,scrollbars,tatusbar){
	/*title is not space*/
	var xxx='menubar='+menubar+',status='+tatusbar+',resizable=yes,width='+w+',height='+h;
	var win = window.open(url,title,xxx);
	if( window.focus){
			win.focus();
	}
}

document.getElementsByClassName = function(clsName){    
	var retVal = new Array();    
	var elements = document.getElementsByTagName("*");    
	for(var i = 0;i < elements.length;i++){        
		if(elements[i].className.indexOf(" ") >= 0){            
		var classes = elements[i].className.split(" ");            
			for(var j = 0;j < classes.length;j++){                
				if(classes[j] == clsName)                    
					retVal.push(elements[i]);            
			}        
		}       
		else if(elements[i].className == clsName)            
			retVal.push(elements[i]);    
	}    
	return retVal;
}
/* get Max heigh (all items) */
function getMaxHeight( classNameStr ){
	var lis = document.getElementsByClassName(classNameStr);
	var h = lis[0].offsetHeight;
	for(i = 0; i < lis.length; i++)
	{
		var li = lis[i];
		if( li.offsetHeight > h) 
		{
			h = li.offsetHeight ; 
		}
	}
	
	return h;

}

function nspCompareItemsHeight( strClassName ){
	var lis = document.getElementsByClassName(strClassName); 
	
	var h = getMaxHeight(strClassName);
	for(i = 0; i < lis.length; i++)
	{
		var li = lis[i];
		li.style.height = h+'px';
	}

}


