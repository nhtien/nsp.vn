<?php
class NRoute 
{
	var $menuname = 'mainmenu';

	/**
	 * Method to wrap around getting the correct links within the email
	 * 
	 * @return string $url
	 * @param string $url
	 * @param boolean $xhtml
	 */
	function emailLink( $url , $xhtml = false )
	{
		$uri	=& JURI::getInstance();
		$base	= $uri->toString( array('scheme', 'host', 'port'));
		
		return $base . EPRoute::_( $url , $xhtml );
	}
	
	function getURI()
	{
		$url		= '';
		
		// In the worst case scenario, QUERY_STRING is not defined at all.
		$url		.= 'index.php?';
		$segments	=& $_GET;
			
		$i			= 0;
		$total		= count( $segments );
		foreach( $segments as $key => $value )
		{
			++$i;
			$url	.= $key . '=' . $value;
			
			if( $i != $total )
			{
				$url	.= '&';
			}					
		}

		// @rule: clean url
		$url	= urldecode( $url );
		$url 	= str_replace('"', '&quot;',$url);
		$url 	= str_replace('<', '&lt;',$url);
		$url	= str_replace('>', '&gt;',$url);
		$url	= preg_replace('/eval\((.*)\)/', '', $url);
		$url 	= preg_replace('/[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']/', '""', $url);
 		
		return EPRoute::_( $url );
	}
	
	/**
	 * Wrapper to JRoute to handle itemid
	 * We need to try and capture the correct itemid for different view	 
	 */	 	
	function _($url, $xhtml = true, $ssl = null) 
	{
		
		static $itemid = array();

		parse_str($url);
		if(empty($view))
			$view = 'category';
		
		if(empty($itemid[$view])) {
			global $Itemid;
			$isValid = false;
			
			// If the current Itemid match the expected Itemid based on view
			// we'll just use it 
			$db		=& JFactory::getDBO();
			$viewId =EPRoute::_getViewItemid($view);
			if($viewId === $Itemid && !is_null($viewId)) {
				$itemid[$view] = $Itemid;
				$isValid = true;
			} else if($viewId !== 0 && !is_null($viewId)){
				$itemid[$view] = $viewId;
				$isValid = true;
			}
			
// 			if(!$isValid){
// 				// See if current itemid is for com_escsocial component, if yes, use it
// 				$query  = "SELECT count(*) FROM #__menu WHERE `link` LIKE '%option=com_escsocial%' AND `id`={$db->quote($Itemid)}";					
// 				$db->setQuery($query);
// 				$isValid = $db->loadResult();
// 			}
// 			
// 			if($isValid) {
// 				$itemid[$view] = $Itemid;
// 			} 
			
			if(!$isValid){
				$id = EPRoute::_getDefaultItemid();
				if($id !== 0 && !is_null($id)) {
					$itemid[$view] =$id;
				}
				$isValid = true;
			}
			
			// Search the mainmenu for the 1st itemid of jomsocial we can find
			if(!$isValid){
				$query  = "SELECT `id` FROM #__menu WHERE "
					." `link` LIKE '%option=com_nsp%' "
					." AND `published`='1' "
					." AND `menutype`='{EPRoute::menuname}' ";					
				$db->setQuery($query);
				$isValid = $db->loadResult();
				if(!empty($isValid))
					$itemid[$view] = $isValid;
			}
			
			
			
			
			// If not in mainmenu, seach in any menu
			if(!$isValid){
				$query  = "SELECT `id` FROM #__menu WHERE "
					." `link` LIKE '%option=com_nsp%' "
					." AND `published`='1' ";					
				$db->setQuery($query);
				$isValid = $db->loadResult();	
				if(!empty($isValid))
					$itemid[$view] = $isValid;
			}
			
			
		}
		
		$pos = JString::strpos($url, '#'); 
		if ($pos === false)
		{
			if( isset( $itemid[$view] ) )
				$url .= '&Itemid='.$itemid[$view];
		}
		else 
		{
			if( isset( $itemid[$view] ) )
				$url = JString::str_ireplace('#', '&Itemid='.$itemid[$view].'#', $url);
		}		
		
		return JRoute::_($url, $xhtml, $ssl); 
	}
	
	/**
	 * Return the Itemid specific for the given view. 
	 */	 	
	function _getViewItemid($view) {
		$db		=& JFactory::getDBO();
		
		$url = $db->quote('%option=com_nsp&view=' . $view . '%');
		
		$query  = 'SELECT id FROM #__menu WHERE `link` LIKE ' . $url . ' AND `published`=1 ORDER BY id';					
		$db->setQuery($query);
		$val = $db->loadResult();
		return $val;
	}
	
	/**
	 * Return the Itemid for default view, frontpage
	 */	 	
	function _getDefaultItemid() {
		$db		=& JFactory::getDBO();
		
		$url = $db->quote("%option=com_nsp&view=category%");
		
		$query  = "SELECT id FROM #__menu WHERE `link` LIKE {$url} AND `published`=1";					
		$db->setQuery($query);
		$val = $db->loadResult();
		return $val;
	}
}
?>