<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

jimport( 'joomla.application.component.controller' ); 

class NspController extends JController 
{
	function display() 
	{	
		$document =& JFactory::getDocument();
		$viewName = JRequest::getVar('view', 'categories');
		$viewType = $document->getType();
		$view = &$this->getView($viewName, $viewType, 'NspView');
		$model =& $this->getModel( $viewName, 'NspModel' );
		if (!JError::isError( $model )) { 
			$view->setModel( $model, true ); 
		}

		$view->setLayout('default'); 
		$view->display();
	}
	function save_jobapply(){
		global $mainframe,$Itemid, $nsp_config;
		$jconfig = new jconfig();
		$datenow = &JFactory::getDate();
		
		$post = &JRequest::get('post');
		
		$link_edit = 'index.php?option=com_nsp&view=jobapply&Itemid='.$Itemid;
		$row = &JTable::getInstance('jobapply','Table');
		$job = &JTable::getInstance('job','Table');
		$job->load($post['job_id']); 
		
		$profile_name = nspconvert2Alias( $post['name'] );
		$profile_name = trim(strtolower($profile_name));
		$profile_name = substr(str_replace(" ","_",$profile_name),0,70);
		$profile_name = $post['job_id']."_".$profile_name."_".date("d_m_Y");
		
		$post['position']	= $post['position'] ? $post['position'] : $job->job_title;
		$post['post_date']	= $datenow->toMySQL();
		$post['name']		= ucwords(trim($post['name']));
		$post['email']		= strtolower($post['email']);

		$arr_input_data = array(
				'learning_time','learing_names','learning_subjects','learning_qualifications',
				'training_time','training_names','training_subjects','training_qualifications',
				'foreign_lang','foreign_lang_level','foreign_lang_school',
				'exp_time','exp_company_name','exp_activity','exp_phone_fax','exp_company_type','exp_position','exp_description','exp_time_type','exp_boss','exp_total_staff','exp_loan','exp_discontinue',
				'reference','ref_name','ref_position','ref_part',
				'reference1','ref_name1','ref_position1','ref_part1'
			
		);

		/* upload image */
		$file = JRequest::getVar( 'imagefile', '', 'files', 'array' );
		$filename =  $file['name'];
		if( $filename ){
			jimport("joomla.filesystem.file");
			$ext	= strtolower(JFile::getExt( $filename ) );
			$filename = $profile_name.".".$ext;
			$fillter = array("png","gif","jpg","bmp","jepg");
			if( !in_array($ext, $fillter) ){
				$mainframe->redirect( $link_edit, "Dinh dang file anh khong ho tro",'error');
				return;
			}
			$dir_file	= $nsp_config->path_piture.DS;
			$dir_avatar = $nsp_config->path_avatar.DS;
			
			if( !JFolder::exists($dir_file)){
				JFolder::create($dir_file,'0755');
			}
			
			if( !JFolder::exists($dir_avatar)){
				JFolder::create($dir_avatar,'0755');
			}
			
			$tmp_dest 	 = $dir_file.$filename;
			$tmp_src 	 = $file['tmp_name'];
			if(file_exists($tmp_dest)){
				$name = JFile::stripExt( $filename );
				$filename = uniqid(substr( $name,0,70)."_" ).".".$ext;
				$tmp_dest 	= $dir_file.$filename;
			}
			JFile::upload($tmp_src, $tmp_dest);
			$post['picture'] = $filename;
			
			$thumb_name = 't_'.$profile_name.'.jpg';
			$thumb_width = 80;
			$thumb_height = 80;
			createThumbnail_Item($tmp_dest, $thumb_name, $dir_avatar, $thumb_width, $thumb_height);
			$post['avatar'] = $thumb_name;
		}
		/* attachments */
		$file = JRequest::getVar( 'attachment', '', 'files', 'array' );
		$filename =  $file['name'];
		if( $filename ){
			jimport("joomla.filesystem.file");
			$ext	= strtolower(JFile::getExt( $filename ) );
			$filename = $profile_name.".".$ext;
			$fillter = array("rar","zip","doc","docx","pdf","txt");
			if( !in_array($ext, $fillter) ){
				$mainframe->redirect( $link_edit, "Dinh dang file dinh kem khong ho tro",'error');
				return;
			}
			
			$dir_file = $nsp_config->path_attachment.DS;
			if( !JFolder::exists($dir_file)){
				JFolder::create($dir_file,'0755');
			}
			$tmp_dest 	 = $dir_file.$filename;
			$tmp_src 	 = $file['tmp_name'];
			if(file_exists($tmp_dest)){
				$name = JFile::stripExt( $filename );
				$filename = uniqid(substr( $name,0,70)."_" ).".".$ext;
				$tmp_dest 	= $dir_file.$filename;
			}
			JFile::upload($tmp_src, $tmp_dest);
			$post['attachment'] = $filename;
		}

		if( !$row->bind( $post ) ){
			JError::raiseError( 500, $row->getError() );
		}
		/* bind data */
		while(list($k,$v)=each($arr_input_data)){
			$row->put_data_to_property($post[$v], $v);
		}
		/* foreign language skill */
		//var_dump( $post['foreign_lang_listen']);exit;
		$foreign_lang = $post['foreign_lang'];
		$str_skills = array();
		for( $i = 0 ; $i<count($foreign_lang) ; $i++ ){
			if( !$post['foreign_lang_listen'][$i] )  $post['foreign_lang_listen'][$i] = 0;
			if( !$post['foreign_lang_talk'][$i] )  $post['foreign_lang_talk'][$i] = 0;
			if( !$post['foreign_lang_read'][$i] )  $post['foreign_lang_read'][$i] = 0;
			if( !$post['foreign_lang_write'][$i] )  $post['foreign_lang_write'][$i] = 0;
			
			$str_skills[] = $post['foreign_lang_listen'][$i].';'.$post['foreign_lang_talk'][$i].';'.$post['foreign_lang_read'][$i].';'.$post['foreign_lang_write'][$i];
			
		}
		$row->foreign_skills = implode("\n",$str_skills);

		if( !$row->store() ){
			JError::raiseError( 500, $row->getError() );
		}

		$link = 'index.php?option=com_nsp&view=jobapply&layout=completed&Itemid='.$Itemid;
		/* send mail to manager */
		if( $nsp_config->job_sendmail2admin == '1'){
			$profile_link = JURI::base().'administrator/index.php?option=com_nsp&task=jobapply_detail&cid[]='.$row->id;
			$str_rec = $nsp_config->job_mailto . ($job->mail_admin ? ';'.$job->mail_admin : '');
			$recipient	= explode(";",$str_rec);
			$cc			= explode(";",$nsp_config->job_mailcc);
			$content  = JText::_('HAVE_A_NEW_PROFILE_ALERT')."\n";
			$content .= JText::_('PROFILE_LINK'). ' '. $profile_link."\n\n";
			$content .= JText::_('APPLICANT_INFOMATION')."\n";
			$content .='- ' . JText::_('FULL_NAME_TITLE') . $row->name . " \n";
			$content .='- ' . JText::_('POSITION_APPLY') . $row->position . " \n";
			$content .='- ' . JText::_('DATE_APPLY') . JHTML::_('date', date("Y-m-d H:i:s"), JText::_('DATE_FORMAT_LC2')) ;
			jimport('joomla.mail.mail');
			//$mail = new JMail();
			$mail = JFactory::getMailer();
			$mail->setSubject($nsp_config->job_mail_subject);
			$mail->setSender($jconfig->mailfrom);
			$mail->FromName = $jconfig->sitename;
			$mail->addRecipient($recipient);
			$mail->addCC($cc);
			$mail->ReplyTo = "";
			$mail->setBody($content);
			$mail->send();
			//var_dump( $mail);exit;
		}
		/* send alert email to applier */
		if( $nsp_config->job_sendmail2applier == '1'){
			$content2 = sprintf(JText::_("SMS_CONTENT_TO_APPLIER"), $post['name'] );
			//$mail2 = new JMail();
			$mail2 = JFactory::getMailer();
			$mail2->setSubject('Don tuyen dung');
			$mail2->setSender($jconfig->mailfrom);
			$mail2->addRecipient($post['email']);
			$mail2->FromName = $jconfig->sitename;
			$mail2->setBody($content2);
			$mail2->send();
			
		}
		//end

		$mainframe->redirect( $link, JText::_("Send Successful"));
		
	}
	
	function save_webcommnents(){
		jimport('joomla.utilities.date');
		$jconfig = new jconfig();
		$date = new JDate();
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$post = JRequest::get('post');
		$post['date'] = $date->toMySQL();
		$row = &JTable::getInstance('webcomments','Table');
		
		/*send mail */
		jimport('joomla.mail.mail');
		$content = "Có một người dùng đã góp ý về website nsp.com.vn:\n\n Tên:".$post['name']."\n Email:".$post['email']."\nTin nhắn:".$post['message'] ;
		$mail = new JMail();
		$mail->setSubject('Thư góp ý về website nsp.com.vn');
		$mail->setSender($jconfig->mailfrom);
		$mail->addRecipient('sangtialia@gmail.com');
		$mail->FromName = $jconfig->sitename;
		$mail->setBody($content);
		$mail->send();

		if( !$row->bind( $post ) ){
			JError::raiseError( 500, $row->getError() );
		}
		if( !$row->store() ){
			JError::raiseError( 500, $row->getError() );
		}
		$msg = "Cám ơn bạn đã đóng ý về website. Thông tin góp ý của bạn đã được gửi thành công!";
		$link = "index.php?option=com_nsp&view=webcomments";
		$this->setRedirect( $link, $msg);
	}
/*  */	
	
function downloadToPhone(){
	global $nsp_config;
	include(JPATH_COMPONENT.DS.'helpers'.DS.'mdownload.php');
	EscMobileDownload::showform( $row );
}
	
	function save_review(){
		$user = &JFactory::getUser();
		$db = &JFactory::getDBO();
		global $Itemid;
		$post = JRequest::get('post');
		$post['published'] = 1;
		$post['create_by'] = $user->id;
		
		if( $post['return'] ){
			$link = base64_decode($post['return']);
		}
		if( !$user->id ){
			$this->setRedirect($link,"Please log in to post your review!");
			return;
		}
		
		$review = &JTable::getInstance('review','Table');

		if( $post['id'] ) {
			$post['modify_date'] = date("Y-m-d H:i:s");
		}else{
			$post['created_date'] =  date("Y-m-d H:i:s");
		}
	
		
		if( !$review->bind( $post ) ){
			JError::raiseError( 500, $rate->getError() );
		}
		if( !$review->store() ){
			JError::raiseError( 500, $rate->getError() );
		}
		$msg = "Thank for post your review";
		$this->setRedirect( $link, $msg);
	}
	function popup_report_form(){
		global $mosConfig_live_site;
		$db = &JFactory::getDBO();
		$id = JRequest::getVar('sid');
		$act = JRequest::getVar('act');
		$user = &JFactory::getUser();
		if( !$act){
			$sql = "SELECT * FROM #__escmobile_software WHERE id=".$id;
			$db->setQuery( $sql );
			$row = $db->loadObject();
			include(JPATH_COMPONENT."/helpers/popup_report.php");
			PopupReport::showForm( $row );
			return;
		}
		$post = JRequest::get( 'post' );
		$post['post_date'] = date("Y-m-d H:i:s");
		$post['author'] = $user->id;
		$post['link'] = $mosConfig_live_site.'/index.php?option=com_nsp&view=software&sid='.$id.'&Itemid=22';
		$row = &JTable::getInstance('report','Table');
		if( !$row->bind( $post ) ){
			JError::raiseError( 500, $row->getError() );
		}
		if( !$row->store() ){
			JError::raiseError( 500, $row->getError() );
		}
		$msg = "<h3>Thank for post report!</h3>";
		echo $msg;
	}
		
	function download(){
		global $nsp_config, $mainframe;
		$db = &JFactory::getDBO();
		$fid = JRequest::getVar('fid','0');
		if( JRequest::getVar('return') ){
			$return = base64_decode( JRequest::getVar('return'));
		}
		$sql = "SELECT f.* FROM #__escmobile_files AS f INNER JOIN #__escmobile_software AS p ON p.id = f.software_id 
								WHERE f.id = $fid AND p.published=1";
		$db->setQuery( $sql );
		$row = $db->loadObject();
		
		$folder = $row->folder ? $row->folder.'/' : '';
		
		$filename = $nsp_config->path_project.$folder.$row->filename;
		
		$obj = new stdClass();
		$obj->id = $row->id;
		$obj->filename = $filename;
		$obj->software_id = $row->software_id;
		$obj->directlink = 0;
		
		/**/
		$paramsTmpl['allowed_file_types'] = "
			{hqx=application/mac-binhex40} 
			{cpt=application/mac-compactpro} 
			{csv=text/x-comma-separated-values} 
			{bin=application/macbinary} 
			{dms=application/octet-stream} 
			{lha=application/octet-stream} 
			{lzh=application/octet-stream} 
			{exe=application/octet-stream} 
			{class=application/octet-stream} 
			{psd=application/x-photoshop} 
			{so=application/octet-stream} 
			{sea=application/octet-stream} 
			{dll=application/octet-stream} 
			{oda=application/oda} 
			{pdf=application/pdf} 
			{ai=application/postscript} 
			{eps=application/postscript} 
			{ps=application/postscript} 
			{smi=application/smil} 
			{smil=application/smil} 
			{mif=application/vnd.mif} 
			{xls=application/vnd.ms-excel} 
			{ppt=application/powerpoint} 
			{wbxml=application/wbxml} 
			{wmlc=application/wmlc} 
			{dcr=application/x-director} 
			{dir=application/x-director} 
			{dxr=application/x-director} 
			{dvi=application/x-dvi} 
			{gtar=application/x-gtar} 
			{gz=application/x-gzip} 
			{php=application/x-httpd-php} 
			{php4=application/x-httpd-php} 
			{php3=application/x-httpd-php} 
			{phtml=application/x-httpd-php} 
			{phps=application/x-httpd-php-source} 
			{js=application/x-javascript} 
			{swf=application/x-shockwave-flash} 
			{sit=application/x-stuffit} 
			{tar=application/x-tar} 
			{tgz=application/x-tar} 
			{xhtml=application/xhtml+xml} 
			{xht=application/xhtml+xml} 
			{zip=application/x-zip} 
			{mid=audio/midi} 
			{midi=audio/midi} 
			{mpga=audio/mpeg} 
			{mp2=audio/mpeg} 
			{mp3=audio/mpeg} 
			{aif=audio/x-aiff} 
			{aiff=audio/x-aiff} 
			{aifc=audio/x-aiff} 
			{ram=audio/x-pn-realaudio} 
			{rm=audio/x-pn-realaudio} 
			{rpm=audio/x-pn-realaudio-plugin} 
			{ra=audio/x-realaudio} 
			{rar=application/x-rar} 
			{rv=video/vnd.rn-realvideo} 
			{wav=audio/x-wav} 
			{bmp=image/bmp} 
			{gif=image/gif} 
			{jpeg=image/jpeg} 
			{jpg=image/jpeg} 
			{jpe=image/jpeg}
			{png=image/png} 
			{tiff=image/tiff} 
			{tif=image/tiff} 
			{css=text/css} 
			{html=text/html} 
			{htm=text/html} 
			{shtml=text/html} 
			{txt=text/plain} 
			{text=text/plain} 
			{log=text/plain} 
			{rtx=text/richtext} 
			{rtf=text/rtf} 
			{xml=text/xml} 
			{xsl=text/xml} 
			{mpeg=video/mpeg} 
			{mpg=video/mpeg} 
			{mpe=video/mpeg} 
			{qt=video/quicktime} 
			{mov=video/quicktime} 
			{avi=video/x-msvideo} 
			{flv=video/x-flv} 
			{movie=video/x-sgi-movie} 
			{doc=application/msword} 
			{xl=application/excel} 
			{eml=message/rfc822}" ;
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'helpers'.DS.'download.php');
		escProjectDownload::download( $obj,$currentLink , $paramsTmpl);
		exit;
	}
	/* download to phone */
	function mdownload(){
		global $mainframe;
		$dcode = (int)JRequest::getVar('dcode');
		$file = &JTable::getInstance('file', 'Table');
		if( !$file->load( $dcode ) ){
			$msg = "The code incorrect";
			$mainframe->redirect("index.php?option=com_nsp&task=d2p&tmpl=component", $msg);
			exit;
		}
		/* download link */
		$mainframe->redirect("index.php?option=com_nsp&task=download&fid=".$file->id);
	}
	
	function check_phone(){
		$mid = JRequest::getVar('model_id');
		$bid = JRequest::getVar('manufacture');
		$return = JRequest::getVar('return','index.php');
		
		$_SESSION['user_brand_id'] = $bid;
		$_SESSION['user_model_id'] = $mid;
		
		$this->setRedirect( $return );
	}	
/* */	

/* ajax function */
	
	function ajax_check_compatible(){
		$db = &JFactory::getDBO();
		$software_id = JRequest::getVar('sid');
		$model_id = JRequest::getVar('mid');
		
		$sql = "SELECT f.* FROM #__escmobile_files AS f INNER JOIN #__escmobile_software AS s ON s.id = f.software_id 
		WHERE s.id = $software_id 
		ORDER BY f.created_date DESC ";
		$db->setQuery( $sql ); 
		$files = $db->loadObjectList();
		foreach( $files as $f){
			$arr = explode(",",$f->model_id);
			if( in_array( $model_id,$arr) ){
				echo "The software is available for your phone. 
					<a href=\"javascript:void(0);\" onclick=\"eDownloadWin('".$f->id."');\" >Download it now</a>";
				exit;
			}
		}
		echo "Sorry! The software is not available for your phone";
		
	}
	function ajax_model_selectbox( ){ 
		$db = &JFactory::getDBO();
		$cid = JRequest::getVar('manf_id');
		$select_id = JRequest::getVar('slid','model_id');
		$selected = JRequest::getVar('selected','0');
		$where = array();
		$where[] = "mf.id=$cid";
		$row = &JTable::getInstance('model','Table');
		
		$rows = $row->getList(1,999,'m.name', $where); 
		$model_option = array();
		$model_option[] = JHTML::_('select.option','','- select once -');
		
		foreach( $rows as $r){
			$model_option[] = JHTML::_('select.option',$r->id,$r->name);
		}
		$lists['model'] = JHTML::_('select.genericList', $model_option, 'model_id','class="inputbox"','value','text', $selected, $select_id);
		
		echo $lists['model'];
		
		exit;
		
	}
	
	
	function ajaxRemoveFavorite(){
		$user = &JFactory::getUser();
		$db = &JFactory::getDBO();
		if( !$user->id ){
			return;
		}
		$bid = JRequest::getVar("bid",0);
		$sql = "SELECT id FROM #__mbook_favorites WHERE book_id=$bid AND user_id = $user->id LIMIT 0,1";
		$db->setQuery( $sql ); 
		$id = $db->loadResult();
		require(JPATH_ADMINISTRATOR.DS."components/com_mbook/tables/favorite.php");
		$library =& JTable::getInstance('favorite', 'Table'); 
		if (!$library->delete( $id )) {
			$msg = "Delete is not sucessful";
		}else{
			$msg = "Delete is successful";
		}
		echo $msg;
	}	

/* pop up */
	function popup_location(){
		
		$language_option=array();
		$language_option[] = JHTML::_('select.option','',' Select Language ');
		$lists['language'] = JHTML::_('select.genericList', $language_option, 'language','class="inputbox"','value','text',null);
		
		$country_option=array();
		$country_option[] = JHTML::_('select.option','',' Select Country ');
		$lists['location'] = JHTML::_('select.genericList', $country_option, 'country','class="inputbox"','value','text',null);
		
		require_once(JPATH_COMPONENT.DS.'helpers'.DS.'popup.php');
		EscMobilePopup::location($lists);
	}
	
	function popup_download(){
		$db = &JFactory::getDBO();
		$fid = JRequest::getVar('fid','0');
		$sql = "SELECT f.*, s.name as software_name FROM #__escmobile_files AS f INNER JOIN #__escmobile_software AS s ON s.id = f.software_id WHERE f.id=".$fid;
		$db->setQuery( $sql );
		$row = $db->loadObject();
		require_once(JPATH_COMPONENT.DS.'helpers'.DS.'popup.php');
		EscMobilePopup::download($row, $lists);
	}
	
	function popup_bookmark(){
		$bid = JRequest::getVar('id',0);
		$itemid = JRequest::getVar('Itemid',0);
		require_once(JPATH_COMPONENT.DS.'helpers'.DS.'popup_bookmark.php');
		Boobmark::show($bid, $itemid);
	}
	function popup_adtoFavorite(){
		$user = &JFactory::getUser();
		$db = &JFactory::getDBO();
		$bid = JRequest::getVar('id');
		$act = JRequest::getVar('act');
		if( !$user->get('id') ){
			require_once(JPATH_COMPONENT.DS.'helpers'.DS.'popup_adfavorite.php');
			Favorite::loginRequest( );
			return;
		}
		if( (int)$bid <= 0){
			echo "Please select a book";
			return;
		}
		if( $act == "save" ){
			$date = date("Y-m-d H:i:s");
			$sql = "SELECT * FROM #__mbook_favorites WHERE user_id = $user->id AND book_id = $bid";
			$db->setQuery( $sql );
			$row = $db->loadObjectList();
			if( count( $row) > 0){
				echo '<script language="javascript">window.close();</script>';
				return;
			}
			$sql = "INSERT INTO #__mbook_favorites(`id` ,`user_id` ,`book_id` ,`created_date` )VALUES (NULL , $user->id, $bid, '".$date."')";
			$db->setQuery( $sql );
			$db->query();
			
			echo '<script language="javascript">window.close();</script>';
			return;
		}
		require_once(JPATH_COMPONENT.DS.'helpers'.DS.'popup_adfavorite.php');
		Favorite::showForm( $gid );
	}
	
	function save_fnet_question(){
		global $mainframe;
		$config = new JConfig();
		$post = JRequest::get('post');
		
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_nspeximpro'.DS.'classes'.DS.'PHPExcel.php');
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_nspeximpro'.DS.'classes'.DS.'PHPExcel'.DS.'IOFactory.php');
		
		$src_xls = JPATH_ROOT.DS.'fnet'.DS.'FNET_contest _info.xls';
		
		$objReader = new PHPExcel_Reader_Excel5();
		$objPHPExcel = $objReader->load($src_xls);
		$objPHPExcel->setActiveSheetIndex(0);
		
		$questionArr = array();
		$questionArr[1] = 'a';
		$questionArr[2] = 'c';
		$questionArr[3] = 'd';
		$questionArr[4] = 'a';
		$questionArr[5] = 'd';
		$questionArr[6] = 'd';
		
		$rightAnwser = 0;
		while(list($k,$v)=each($questionArr)){
			if($post['q'.$k] == $questionArr[$k]) $rightAnwser++;
		}
		//var_dump($rightAnwser);exit;
		$index = 3;
		$name = $objPHPExcel->getActiveSheet()->getCell('B'.$index)->getValue(); //get first part number
		while($name){
			$index++;
			$name = $objPHPExcel->getActiveSheet()->getCell('B'.$index)->getValue();
			$email = $objPHPExcel->getActiveSheet()->getCell('C'.$index)->getValue(); 
			if(strtolower(trim($email)) == strtolower(trim($post['email']))){
				$mainframe->redirect('index.php', 'Đăng ký không thành công. Bạn đã tham dự chương trình','error');
				return;
			}
		}//var_dump($post['email']);exit;
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$index, $post['name']);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$index, trim(strtolower($post['email'])));
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$index, "'".$post['phone']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$index, $post['company']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$index, $post['position']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$index, $post['q1']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$index, $post['q2']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$index, $post['q3']);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$index, $post['q4']);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$index, $post['q5']);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$index, $post['q6']);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$index, (int)$post['subquestion']);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$index, $rightAnwser);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save($src_xls);
		
		$content = "Có một KH trả lời của hỏi tại web NSP.COM.VN:\nHọ tên: ".$post['name']." \n Điện thoại: ".$post['phone']."\n Email: ".$post['email'];
		$content .= "\n\n Số câu trả lời đúng: ".$rightAnwser.'/6';
		//jimport('joomla.mail.mail');
		include_once(JPATH_LIBRARIES.DS.'joomla'.DS.'mail'.DS.'mail.php');
		include_once(JPATH_LIBRARIES.DS.'joomla'.DS.'mail'.DS.'helper.php');
		$mail = JFactory::getMailer();
		$mail->setSender( $config->mailfrom );
		//$mail->addCC('sangtialia@gmail.com');
		$mail->setSubject('[FNET] - '.$post['name']);
		//$mail->addRecipient('minhvu@nsp.com.vn');
		$mail->addRecipient('sangtialia@gmail.com');
		$mail->setBody($content);
		$mail->addAttachment($src_xls);
		$mail->set('FromName','NHAN SINH PHUC CO., LTD.');
		$mail->send();
		$_SESSION['__show_result'] = 1;
		
		
		$mainframe->redirect('index.php?option=com_nsp&view=fnet_question&result='.$rightAnwser,'Số câu trả lời đúng của bạn là: '.$rightAnwser.'/6<br>Thông tin của bạn đã được gửi thành công. Cảm ơn bạn đã quan tâm đến chương trình này!');
		return;
	}

}
?>