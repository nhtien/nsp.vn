<?php 
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined('_JEXEC') or die('Restricted access'); 

require_once( JPATH_COMPONENT.DS.'libraries'.DS.'route.php');

JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR.DS.'tables');

require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'functions.php');
loadNSPConfig();

$document = &JFactory::getDocument();
$document->addStyleSheet(JURI::base().'components/com_nsp/themes/nsp.css');

require_once( JPATH_COMPONENT.DS.'controller.php' );

$controller = new NspController(); 

$controller->registerTask( 'bookmark', 'popup_bookmark' );


$controller->execute( JRequest::getVar( 'task' ) ); 
$controller->redirect(); 

?> 
