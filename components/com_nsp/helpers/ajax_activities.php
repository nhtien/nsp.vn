<?php
/***************************************************************************
*  @EcsProjec Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2009 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			October 2009
***************************************************************************/

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<?php
if ( count($data) == 0){
	echo "No recent activity ";
}else
foreach( $data as $r ) : 
	$link_profile = "index.php?option=com_escsocial&view=profile&userid=".$r->author;
?>
	<div class="item icon_<?php echo $r->title_id;?>">
		<span class="name"><?php echo $r->title;?></span>
		<p><?php echo $r->content;?></p>
		<span class="date"><?php echo date("Y-m-d H:i:s",strtotime($r->created_date));?> by</span>  <a href="<?php echo $link_profile;?>"><?php echo $r->name;?></a>
	</div>
<?php
 endforeach; ?>
