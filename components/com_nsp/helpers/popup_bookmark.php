<?php
/**********************************************
*  Manage Book Joomla! Component.
*  Released under
* Sang Tran Thanh
* Date july 2009
**********************************************/
defined( '_JEXEC' ) or die( 'Restricted access' );
class Boobmark{
	function show( $bid, $itemid ){
?>
<style type="text/css">
	body{
	margin:0px 0px 0px 0px;
	font-size:12px;
	font-weight:bold;
	}
	div.game-bookmark a{
		color:#333333;
		text-decoration:none;
	}
	div.game-bookmark{
		border:1px solid #000000;
		text-align:left;
	}
	div.game-bookmark div.bookmark-inner{
		border:2px solid #937b35;
		padding:5px 5px 5px 5px;
	}
	div.game-bookmark div.item{
		border:1px solid #CCCCCC;
		margin:2px 0px 2px 0px;
		padding:3px 5px 3px 5px;
	}
	div.game-bookmark div.bookmark-title{
		text-transform:uppercase;
		background:#93361d;
		color:#FFFFFF;
		padding:3px 3px 3px 5px;
		}
</style>
	<div class="game-bookmark" id="game-bookmark">
		<div class="bookmark-title">
			<img src="components/com_nsp/themes/images/bookmark_03.png" align="left" />&nbsp;Book-mark
		</div>
		<div class="bookmark-inner">
			
		<?php 
			echo Boobmark::WriteBookmarks2($bid,$itemid);
		?>
		</div>
	</div>
<?php
}
	function WriteBookmarks2($bid,$Itemid,$title = ''){
		global $mosConfig_live_site, $mosConfig_sitename;
		
		$url = $mosConfig_live_site."/index.php?option=com_nsp%26view=project%26Itemid=".$Itemid."%26id=".$bid;
		
		$purandom   = mt_rand(1000, 9999);
		echo "<script type=\"text/javascript\" language=\"JavaScript\">";
		echo "var url = '".$url."';";
		echo "var title = 'Game From ".$mosConfig_sitename."';";
		echo "</script>";
		$img_src = $mosConfig_live_site."/components/com_nsp/themes/images/";
		
		$facebook   = '<div class="item"><a onclick="window.open(\'http://www.facebook.com/share.php?u=\'+url'.$purandom.'+\'&t=\'+title'.$purandom.');return false;" href="http://www.facebook.com/share.php?u='. $url .'&t='. $title .'" title="Facebook!" target="_blank">';
		$facebook  .= '<img src="'. $img_src .'facebook.png" alt="Facebook!" title="Facebook!" border=0 /> Facebook</a></div>';	
		
		$digg       = '<div class="item"><a onclick="window.open(\'http://digg.com/submit?phase=2&url=\'+url'.$purandom.'+\'&title=\'+title'.$purandom.');return false;" href="http://digg.com/submit?phase=2&url='. $url .'&title='. $title .'" title="Digg!" target="_blank">';
		$digg      .= '<img src="'. $img_src .'digg.png" alt="Digg!" title="Digg!" border=0  /> Digg </a></div>';
		
		$reddit     = '<div class="item"><a onclick="window.open(\'http://reddit.com/submit?url=\'+url'.$purandom.'+\'&title=\'+title'.$purandom.');return false;" href="http://reddit.com/submit?url='. $url .'&title='. $title .'" title="Reddit!" target="_blank">';
		$reddit    .= '<img src="'. $img_src .'reddit.png" alt="Reddit!" title="Reddit!" border=0 /> Reddit</a></div>';
		
		$delicious  = '<div class="item"><a onclick="window.open(\'http://del.icio.us/post?url=\'+url'.$purandom.'+\'&title=\'+title'.$purandom.');return false;" href="http://del.icio.us/post?url='. $url .'&title='. $title .'" title="Del.icio.us!" target="_blank">';
		$delicious .= '<img src="'. $img_src .'delicious.png" alt="Del.icio.us!" title="Del.icio.us!" border=0 /> Delicious</a></div>';	
		
		$google     = '<div class="item"><a onclick="window.open(\'http://www.google.com/bookmarks/mark?op=add&bkmk=\'+url'.$purandom.'+\'&title=\'+title'.$purandom.');return false;" href="http://www.google.com/bookmarks/mark?op=add&bkmk='. $url .'&title='. $title .'" title="Google!" target="_blank">';
		$google    .= '<img src="'. $img_src .'google.png" alt="Google!" title="Google!" border=0 /> Google</a></div>';	
		
		$slashdot   = '<div class="item"><a onclick="window.open(\'http://slashdot.org/bookmark.pl?url=\'+url'.$purandom.'+\'&title=\'+title'.$purandom.');return false;" href="http://slashdot.org/bookmark.pl?url='. $url .'&title='. $title .'" title="Slashdot!" target="_blank">';
		$slashdot  .= '<img src="'. $img_src .'slashdot.png" alt="Slashdot!" title="Slashdot!" border=0 /> Slashdot</a></div>';
		
		$stumble    = '<div class="item"><a onclick="window.open(\'http://www.stumbleupon.com/submit?url=\'+url'.$purandom.'+\'&title=\'+title'.$purandom.');return false;" href="http://www.stumbleupon.com/submit?url='. $url .'&title='. $title .'" title="StumbleUpon!" target="_blank">';
		$stumble   .= '<img src="'. $img_src .'stumbleupon.png" alt="StumbleUpon!" title="StumbleUpon!" border=0 /> StumbleUpon</a></div>';
			
		$yahoo	    = '<div class="item"><a onclick="window.open(\'http://myweb2.search.yahoo.com/myresults/bookmarklet?u=\'+url'.$purandom.'+\'&t=\'+title'.$purandom.');return false;" href="http://myweb2.search.yahoo.com/myresults/bookmarklet?u='. $url .'&t='. $title .'" title="Yahoo!" target="_blank">';
		$yahoo	   .= '<img src="'. $img_src .'yahoo.png" alt="Yahoo!" title="Yahoo!" border=0 /> Yahoo</a></div>';	
		
		$techno	    = '<div class="item"><a onclick="window.open(\'http://technorati.com/faves/?add=\'+url'.$purandom.'+\'&title=\'+title'.$purandom.');return false;" href="http://technorati.com/faves/?add='. $url .'" title="Technorati!" target="_blank">';
		$techno    .= '<img src="'. $img_src .'technorati.png" alt="Technorati!" title="Technorati!" border=0 /> Technorati</a></div>';
				
		return  $facebook . $digg . $reddit . $delicious . $google . $slashdot . $stumble . $yahoo . $techno;		
	}
}
?>