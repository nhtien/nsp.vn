<?php
/***************************************************************************
*  @NSP Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2010 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/
// no direct access
defined('_JEXEC') or die("Direct Access Is Not Allowed");
$document = &JFactory::getDocument();
$document->addStyleSheet(JURI::base().'/templates/open_start/css/popup.css');

class EscMobilePopup{
	
	function location( $lists ){
	?>
	
<div class="popup_location">
<div class="inner">
<div class="border">
<form method="post" action="">
<table width="auto" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td><?php echo JText::_('Location');?>:</td>
    <td><?php echo $lists['location'];?></td>
  </tr>
  <tr>
    <td><?php echo JText::_('Language');?>:</td>
    <td><?php echo $lists['language'];?></td>
  </tr>
</table>
<br />
<br />
<table width="auto" border="0" cellspacing="0" cellpadding="2">
  <tr>
    <td width="auto"><?php echo JText::_('Network Provider');?>:</td>
    <td align="left"><input type="text" name="net_provider" value="" size="" class="inputbox" maxlength="255" /></td>
  </tr>
  <tr><td colspan="2"><span class="note">Type your mobile network provider in the blank</span></td></tr>
  <tr>
    <td><?php echo JText::_('Mobile Number');?>:</td>
    <td align="left"><input type="text" name="phone_number" value="" class="inputbox" maxlength="255" /></td>
	<tr><td colspan="2"><span class="note">Enter your cellphone number with the country code. For example: +1 800 888 222</span></td></tr>
  </tr>
</table>
	
	<input type="hidden" name="option" value="com_escproject" />
    <input type="hidden" name="task" value="popup_report_form" />
    <p><input type="submit" value="Enter Website" name="submitInappropriate" class="button" /></p>
</form>	
</div>
</div>
</div><!-- popup-location-->
<?php
	}
	
	function download( $file, $lists ){
	$link_download="index.php?option=com_nsp&task=download&fid=".$file->id;
	$link_mdownload = wordwrap(JURI::base()."index.php?option=com_nsp&task=d2p&tmpl=component",30,"<br />",true);
	
	if( strlen($file->id) < 5 ){
		$i = strlen( $file->id );
		$str_id = $file->id;
		while( $i < 5){
			$str_id = '0'.$str_id;
			$i++;
		}
	}
	?>
<div class="popup_location popup_download">
<div class="inner">
<div class="border">
<div style="padding:10px 10px 10px 20px;">
	<h3>Download <?php echo $file->software_name;?></h3>
	<div class="left">
		<h4>To Download to Your Phone:</h4>
		<ol>
		<li>Go to <?php echo $link_mdownload; ?> on your phone</li>

		<li>Select Quick Download code on the bottom of the page.</li>
		
		<li>Enter Code: <strong><?php echo $str_id;?> </strong>to begin download. </li>
		
		</ol>
	</div>
	<div class="right">
	<div class="border_dw_pc">
		<h4>Download to Computer</h4>
		<p>If you are an advanced user, you can also <a href="<?php echo $link_download;?>" title="download">download this application to your computer </a>, then transfer it to your phone.</p><br class="break" />
		</div>
		<br />
		<button class="button" onclick="window.close();"> OK </button>
	</div>
	</div>
	
</div><!--border-->
</div>
</div><!-- popup-location-->
<script language="javascript">
	addEvent(window, 'load', function(){curvyCorners(setSettings( 9 ), "div.border_dw_pc"); });
</script>

<?php
	}
	
}	
?>
