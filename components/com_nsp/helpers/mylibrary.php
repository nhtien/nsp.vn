<?php
/**********************************************
*  Manage Book Joomla! Component.
*  Released under
* Sang Tran Thanh
* Date july 2009
**********************************************/
global  $mosConfig_absolute_path;
defined( '_JEXEC' ) or die( 'Restricted access' );
$user = &JFactory::getUser();
?>
<link href="components/com_flippingcomic/themes/flippingbook.css" rel="stylesheet" type="text/css" />
<?php
	function getBook($bookId){
		
		$db = &JFactory::getDBO();
		$sql = "SELECT * FROM #__flippingcomic_books AS b INNER JOIN #__users AS u ON u.id = b.created_by WHERE b.id IN (".$bookId.")";
		$db-> setQuery($sql);
		$rowBook = $db->loadObject();
		return $rowBook;
	}
?>
<?php
class library {

	function showform( ) {
		JHTML::_('behavior.tooltip');
		global $mosConfig_absolute_path;
		$bookId = JRequest::getVar('id',0);
		//var_dump($bookId);
		$book = getBook($bookId);
		//var_dump($book);
		$getLink = JRequest::getVar('linkbook','');
		if(!$getLink){
			$uri =&JFactory::getURi();
			$path ="http://".$uri->_host.$uri->_path.'?option=com_flippingcomic&view=book&id='.JRequest::getVar('id',0);
			$getLink = $path;
			}
			$strMail = array();
			foreach($book as $r){$strMail[]=$r->email;}
				
		?>
<form action="" method="post" enctype="" id="fc_frmuploadfile" class="fc_frmuploadfile">
<fieldset class="input">
<legend>Add book to my library </legend>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="21%" class="demo"><?php echo JText::_('Book id');?></td>
    <td width="79%"><?php echo JRequest::getVar('id',0);?> </td>
  </tr>

  <tr>
    <td width="21%" class="demo"><?php echo JText::_('Book link');?></td>
    <td width="79%"><input name="book_url" type="text" id="to" size="50" value="<?php echo $path;?>"/></td>
  </tr>
  <tr>
    <td width="21%" class="demo"><?php echo JText::_('Book title');?></td>
    <td width="79%"><input name="book_title" type="text" id="subject" size="40" value="<?php echo $book->title;?>" /></td>
  </tr>
  <tr>
    <td><?php echo JText::_('description');?> </td>
    <td><textarea name="book_description" cols="37" rows="5" id="body"><?php echo $book->description;?></textarea></td>
  </tr>
    <tr>
    <td width="21%" class="demo"><?php echo JText::_('');?></td>
    <td width="79%">&nbsp;</td>
    </tr>
</table>
<input type="submit" name="submit" value="<?php echo JText::_('- Add -');?>" class="button"/>
<br />
</fieldset>
	<input name="folder" value="" type="hidden">
	<input type="hidden" name="option" value="com_flippingcomic" />
	<input type="hidden" name="task" value="library" />
	<input type="hidden" name="act" value="save" />
	<input type="hidden" name="section" value="add_mylibrary" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="book_id" value="<?php echo JRequest::getVar('id',0);?>" />
</form>
<br>
<?php
	}
}
?>
