<?php
/***************************************************************************
*  @EcsProjec Joomla! Component.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2009 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			October 2009
***************************************************************************/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class FileManger {
	function showFiles( &$rows, &$pageNav, $option, &$lists ) {
		$user =& JFactory::getUser();
		$ordering = ($lists['order'] == 'p.id' || $lists['order'] == 'p.name' || $lists['order'] == 'p.ordering');
		//echo $lists['order'];
		JHTML::_('behavior.tooltip');
		?>
		<form action="index.php?option=com_nsp" method="post" name="adminForm">

		<table width="100%">
		<tr>
			<td align="left">
				<?php echo JText::_( 'Filter (File Name)' ); ?>:
				<input type="text" name="search" id="search" value="<?php echo $lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_( 'Go' ); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.submit();"><?php echo JText::_( 'Reset' ); ?></button></td>
			<td align="center" nowrap="nowrap">
				<?php echo JText::_( 'Project Filter' ); ?>:<?php
				echo $lists['project'];
				?></td>
			<td align="center" nowrap="nowrap">
				</td>	
			<td align="right" nowrap="nowrap"><?php echo JText::_( 'State Filter' ); ?>:
				<?php
				echo $lists['state'];
				?></td>
		</tr>
		</table>

		<div id="tablecell">
			<table class="adminlist">
			<thead>
				<tr>
<th width="5" nowrap="nowrap"><?php echo JText::_( 'NUM' ); ?></th>
<th width="20" nowrap="nowrap"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $rows ); ?>);" /></th>
<th width="1%" nowrap="NOWRAP"><?php echo JHTML::_('grid.sort', 'ID', 'p.id', @$lists['order_Dir'], @$lists['order'], 'list_project' ); ?></th>
<th nowrap="nowrap" class="title"><?php echo JHTML::_('grid.sort', 'File Name', 'f.name', @$lists['order_Dir'], @$lists['order'], 'list_file' ); ?></th>
<th align="center" nowrap="nowrap"><?php echo JText::_( 'Platform'); ?></th>
<th align="center" nowrap="nowrap"><?php echo JText::_( 'Folder Name'); ?></th>
<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'File', 'f.filename', @$lists['order_Dir'], @$lists['order'], 'list_file' ); ?></th>
<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'File Size', 'f.filesize', @$lists['order_Dir'], @$lists['order'], 'list_file' ); ?></th>

<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Author', 'author', @$lists['order_Dir'], @$lists['order'], 'list_file' ); ?></th>
<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Project Name', 'project_name', @$lists['order_Dir'], @$lists['order'], 'list_file' ); ?></th>					<th align="center" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Added Date', 'f.created_date', @$lists['order_Dir'], @$lists['order'], 'list_file' ); ?></th>
<th colspan="3" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Published', 'm.published', @$lists['order_Dir'], @$lists['order'], 'list_file' ); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="13">
						<?php echo $pageNav->getListFooter(); ?>					</td>
				</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0; 
			for ($i=0, $n=count( $rows ); $i < $n; $i++) {
				$row = &$rows[$i];

				$link 		= 'index.php?option=com_nsp&task=edit_file&cid[]='. $row->id ;

				$checked 	= JHTML::_('grid.checkedout', $row, $i );
				$published 	= JHTML::_('grid.published', $row, $i );
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td><?php echo $pageNav->getRowOffset( $i ); ?></td>
					<td><?php echo $checked; ?></td>
					<td align="center"><?php echo $row->id; ?></td>
					<td>
						<a href="<?php echo JRoute::_( $link ); ?>" title="<?php echo JText::_( 'Edit File' ); ?>"><?php echo $row->name; ?></a>
					</td>
					<td  align="center"><?php echo $row->platform;?></td>
					<td  align="center"><?php echo $row->folder;?></td>
					<td align="left"><?php echo $row->filename;?></td>
					<td align="center"><?php echo $row->filesize;?></td>
					<td align="left"><?php echo $row->author;?></td>
					<td align="left"><?php echo $row->project_name;?></td>
					<td align="center"><?php echo $row->created_date;?></td>
					<td align="center"><?php echo $published;?></td>
					
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>
		</div>

		<input type="hidden" name="option" value="<?php echo $option;?>" />
		<input type="hidden" name="task" value="list_file" />
		<input type="hidden" name="section" value="manage_file" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $lists['order']; ?>" />
		<input type="hidden" name="filter_order_Dir" value="" />
		</form>
		<?php
	}
	
	function editFile( &$row, &$lists ) {
		global $option;
		if (JRequest::getCmd('act') == 'new_file') {
			$row->published = '1';
			$row->platform = '';
			$row->description = '';
			$row->ordering = '0';
			$row->project_id = JRequest::getVar('project_id');
		}
?>
    <form action="" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" cellpadding="5" cellspacing='0' class="editView adminform">
        <tr>
			<td width="150" class="key" id="filename_label"><?php echo JText::_('Folder name'); ?></td>
			<td><?php echo $lists['folder'];?></td>
		</tr>
		</tr>
        <tr>
			<td align="" class='<?php echo $row->filelocation ? 'fieldNameRequiredActive' : 'fieldNameRequired';?>' id="filelocation_label"><?php echo JText::_('File'); ?></td>
            <td class='fieldValue'>
            <?php if($row->filelocation) { 
				echo $row->filelocation;
				echo "<input type='hidden' name='filelocation' id='filelocation' value='".$row->filelocation."' >";
			} else {?>
				<input type='file' name='filelocation' id='filelocation' value="<?php echo $row->filelocation; ?>" />
			<?php } ?>
			</td>
		</tr>
        <tr>
          <td class='fieldName' valign='top'><?php echo JText::_("Description"); ?></td>
          <td colspan='3' align="left" class='fieldValue'><textarea name="description" rows="4" cols="40"></textarea></td>
        </tr>	
    </table>
<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
<input type="hidden" name="project_id" value="<?php echo $row->project_id; ?>" />
<input type="hidden" name="published" value="1" />
<input type="hidden" name="option" value="<?php echo $option; ?>" />
<input type="hidden" name="task" value="popup_manage_project" />
<input type="hidden" name="act" value="save_file" />
<div><input type="submit" name="submit" value="Upload" />&nbsp;<input type="button" name="reset" value="Cancel" onclick="window.parent.closeModalBox();" /></div>

</form> 
		<?php
	}
	
	function newFolder( $row, $lists){
	?>
    <form action="" method="post" name="adminForm" enctype="multipart/form-data">
    <table width="100%" cellpadding="5" cellspacing='0' class="editView adminform">
        <tr>
			<td width="150" class="key" id="filename_label"><?php echo JText::_('Folder Parent'); ?></td>
			<td><?php echo $lists['folder'];?></td>
		</tr>
		</tr>
        <tr>
          <td><?php echo JText::_("Folder Name"); ?></td>
          <td><input type="text" name="foldername" size="20" /></td>
        </tr>	
    </table>
					<input type="hidden" name="option" value="com_nsp" />
					<input type="hidden" name="task" value="popup_manage_project" />
					<input type="hidden" name="act" value="save_folder" />
					<input type="hidden" name="project_id" value="<?php echo JRequest::getVar('project_id');?>" />
					<div><input type="submit" name="submit" value="OK" />&nbsp;<input type="button" name="reset" value="Cancel" onclick="window.parent.closeModalBox();" /></div>

</form> 
	<?php
	}
}
?>
