<?php
/**********************************************
*  Manage Book Joomla! Component.
*  Released under
* Sang Tran Thanh
* Date july 2009
**********************************************/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<style type="text/css">
	body{
	margin:0px 0px 0px 0px;
	font-size:12px;
	font-weight:bold;
	color:#424242;
	font-family:Verdana, Arial, Helvetica, sans-serif;
	}
	div.game-add-to-favorite{border:1px solid #ff6512; padding:2px 2px 2px 2px;
		min-height:110px;
	}
	div.game-add-to-favorite div.inner{
		text-align:center;
		
		}
	.button{
		border:1px;
		width:50px;
	}
	div.title{
		color:#FFFFFF;
		text-transform:uppercase;
		font-weight:bold;
		background:#ff6512;
		padding:5px 0px 5px 10px;
		text-align:left;
		margin-bottom:5px;
	}
</style>

<?php
class Favorite{
	function showForm( $gid ){
?>
	<div class="game-add-to-favorite">
		<div class="inner">
		<div class="title"><?php echo _MB_ADD_TO_MY_FAVORITES;?></div>
		<span><?php echo _MB_CONFIRM_ADD_TO_FAVORITES;?></span><br/>&nbsp;
		<form action="index2.php" method="post">
			<input type="submit" name="submit" value=" <?php echo _MB_YES;?> " class="button" />&nbsp;
			<button onclick="window.close();" class="button"> <?php echo _MB_NO;?>&nbsp; </button>
			<input type="hidden" name="act" value="save" />
			<input type="hidden" name="task" value="adtofavorite" />
			<input type="hidden" name="option" value="com_mbook" />
			<input type="hidden" name="id" value="<?php echo JRequest::getVar('id');?>" />
		</form>
		</div>
	</div>
<?php
	}
	function loginRequest(){
	?>
	<div class="game-add-to-favorite">
		<div class="inner">
		<div class="title"><?php echo _MB_ADD_TO_MY_FAVORITES;?></div>
		<br/>
		<span><?php echo _MB_LOGIN_ADD_TO_FAVORITES;?></span>
		<br/><br/>
		<a href="javascript:window.close();">Close window</a><br/><br/>
		</div>
	</div>
	<?php
	}
}
?>