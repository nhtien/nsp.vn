<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
 
// no direct access
defined('_JEXEC') or die('Restricted access');

class modSpecialProductsHelper
{

	function getSpecialProducts( $params, $type ){
		$str_id = $params->get('product_list_id');
		$not_in_bid = $params->get('not_in_bid');
		if( $str_id )
			$filter[] = 'p.product_id IN ('. $str_id . ')';
		else{
			$filter[] = "p.product_special = 'Y'";
			$catid = JRequest::getVar('category_id');
			$bid = JRequest::getVar('bid');
			
			if( $catid && !$bid ){
				$filter[] = "cf.category_id = ".$catid;
			}
			
			if( $bid ){
				$filter[] = 'mf.manufacturer_id = '.$bid;
			}
			
			if($not_in_bid){
				$filter[]='mf.manufacturer_id NOT IN( '.$not_in_bid .' )';
			}
		}
		
		$order = $params->get('orderby',0);
		switch( $order ){
			case 1:
				$orderby = "p.cdate DESC";
				break;
			case 2:
				$orderby = "RAND()";
				break;
			default:
				$orderby = "p.product_name ASC";
				break;
		}

		$results = modSpecialProductsHelper::_getProducts( $params->get('category_id'),$orderby , $filter );
		if( count( $results) == 0){
			$results = modSpecialProductsHelper::_getProducts( null,$orderby , "p.product_special = 'Y'" );
		}
		return $results;
	}
	
	function _getProducts( $cat_id = null,$orderby = null, $filter = null, $limitstart = 0, $limit = 3  )
	{
		$db = &JFactory::getDBO();
		$user = &JFactory::getUser();
		
		$sql = 'SELECT s.shopper_group_id FROM #__vm_shopper_group as s WHERE s.default=1';
		$db->setQuery( $sql );
		$shopper_group_id = $db->loadResult();
		$shopper_default = $shopper_group_id;

		if( $user->id ){
			$sql = 'SELECT * FROM #__vm_shopper_vendor_xref WHERE user_id = '.$user->id;
			$db->setQuery( $sql );
			$row = $db->loadObject();
			if( $row->shopper_group_id){
				$shopper_group_id = $row->shopper_group_id;
			} //echo $shopper_group_id ;
		} 
		
		$where[] = "p.product_publish = 'Y'";
		
		if( $cat_id ){
			$where[] = 'cf.category_id = '.$params->get('category_id');
		}
		
		$where = array_merge($where, (array)$filter );
		
		
		$where = implode(" AND ", $where);
		
		$orderby = ' ORDER BY '.($orderby ? $orderby : 'p.product_name ASC');
		
		$limit = ' LIMIT 0,'.$limit;
		
		$query = "SELECT p.*, mf.*,cf.category_id, pr.product_price, pr1.product_price AS product_price_shopper
					FROM #__vm_product AS p 
					LEFT JOIN #__vm_product_price AS pr ON pr.product_id = p.product_id AND pr.shopper_group_id = $shopper_default
					LEFT JOIN #__vm_product_price AS pr1 ON pr1.product_id = p.product_id AND pr1.shopper_group_id = $shopper_group_id
					
					
					LEFT JOIN #__vm_product_category_xref AS cf ON cf.product_id = p.product_id
					LEFT JOIN #__vm_product_mf_xref AS mfx ON mfx.product_id = p.product_id
					LEFT JOIN #__vm_manufacturer AS mf ON mf.manufacturer_id = mfx.manufacturer_id
					WHERE ".$where.' GROUP BY p.product_id'.$orderby.$limit;
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		$data = array();
		foreach( $rows as $r){
			$obj = new stdClass();
			$obj = $r; 
			if( $r->product_price_shopper ) $obj->product_price = $r->product_price_shopper;
			
			// truong hop sp con, cần lấy lại ảnh, category ID của sp cha
			if( $r->product_parent_id <> 0){ 
				$sql = "SELECT p.*, cf.category_id FROM #__vm_product AS p
						LEFT JOIN #__vm_product_category_xref AS cf ON cf.product_id = p.product_id 
						WHERE p.product_id = ".$r->product_parent_id;
				$db->setQuery($sql);
				$p = $db->loadObject();
				if( !$r->product_thumb_image )$obj->product_thumb_image = $p->product_thumb_image;
				if( !$r->product_full_image )$obj->product_full_image = $p->product_full_image;
				if( !$r->product_s_desc ) $obj->product_s_desc = $p->product_s_desc;
				if( !$r->product_desc ) $obj->product_s_desc = $p->product_desc;
				$obj->category_id = $p->category_id;
			}
			if( !$r->product_thumb_image ) $obj->product_thumb_image = '___1noimage.gif';
			$data[] = $obj;
		}
		return $data;
	}
}

?>
