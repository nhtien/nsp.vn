<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$user = &JFactory::getUser();
$call_pricing = 'Call';
?>
<link href="<?php echo JURI::base()?>modules/mod_special_products/tmpl/images/style.css" rel="stylesheet" type="text/css" />
<div class="mod_special_products">
	<div class="row">
		<ul class="list">
		<?php
			$i=1;
			foreach( $specialProducts as $r){
				$pro_link = 'index.php?page=shop.product_details&flypage=flypage.tpl&product_id='.$r->product_id.'&category_id='.$r->category_id.'&option=com_virtuemart&Itemid=26';
				$brand_blink = "index.php?option=com_nsp&view=brand&bid=".$r->manufacturer_id."&Itemid=72";
				
				$subject = urlencode("Call: ".$r->product_name);
				$ask_link = JRoute::_('index.php?option=com_virtuemart&page=shop.ask&product_id='.$r->product_id.'&subject='.$subject.'&Itemid=92');
				$ask_price = '<a href="'.$ask_link.'" >'.$call_pricing.'</a>';
				
		?>
			<li class="item <?php echo $i == count($specialProducts) ? 'last' : '' ?>">
				<div class="info">
					<a href="<?php echo $pro_link;?>" class="name"><?php echo $r->product_name;?></a>
					<p class="short_des"><?php //echo $r->product_s_desc ?> </p>
					<div class="thumb">
						
						 <a href="<?php echo $pro_link;?>">
							<img src="<?php echo JURI::base().'product_image.php?filename='.urlencode($r->product_thumb_image).'&style=0';?>" border="0" title="<?php echo $r->product_name?>" align="<?php echo $r->product_name?>" />
						</a>
					</div>
					<div class="left">
					<!--
						<?php echo JText::_('Price')?>: <br />
					
						<span class="price"><?php echo ($r->product_price) ? '$'.number_format($r->product_price,2) : $ask_price ;?></span>
						-->
						<div class="btn-more"><a href="<?php echo $pro_link;?>"><img src="images/<?php echo JText::_('MORE_IMG_SRC')?>" /></a></div>
					</div>
				</div>
				<div class="break">&nbsp;</div>
			</li>
		<?php
		$i++;
		 } ?>
		</ul>
		<div class="break">&nbsp;</div>
	</div><!-- row-->
	
</div>
