<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$width	= $params->get('width');
$link	= $params->get('link');

$session = JFactory::getSession();
if( $session->isNew() ) :
?>
<div class="stl_promotion setting5" id="stl_promotion" style="width:<?php echo $params->get('width')?>px; height:<?php echo $params->get('height')?>px; top:15%;">
	<img src="modules/mod_popup_promotion/tmpl/images/close.png" style="position:absolute; z-index:10005; margin-left:<?php echo $width-15 ?>px; margin-top:-10px;" onclick="document.getElementById('stl_promotion').style.display='none';" title="Close" />
	<img src="<?php echo $params->get('image')?>" width="<?php echo $params->get('width')?>" height="<?php echo $params->get('height')?>" onclick="window.location.href='<?php echo $link?>';" />
</div>
<script language="javascript">
	var pwidth = <?php echo $params->get('width','300')?>;
	var percent = 100 - ((pwidth/document.body.clientWidth)*100);
	document.getElementById('stl_promotion').style.left = percent/2+'%';
	if( document.getElementById('stl_promotion').style.display == 'none' ){
		document.body.style.display='none';
	}
</script>
<?php
endif;
?>
