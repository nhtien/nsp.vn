<?php
/// $Id: mod_modulename.php, v1.0 June 2007 DART Creations Exp $
/**
* Joomla 1.5 module
* @ package Joomla
* @ joomla Open Source is Free Software
* @ Released under GNU/GPL License : http://www.gnu.org/copyleft/gpl.html

* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@28cth.com
* @ version $Revision: 1.0 $
**/

// no direct access
defined('_JEXEC') or die('Restricted access');
// Include the syndicate functions only once

$doc = JFactory::getDocument();
$doc->addStyleSheet( JURI::base().'modules/mod_popup_promotion/tmpl/images/style.css');
require_once (dirname(__FILE__).DS.'helper.php');

$params->def('greeting', 1);

$var02	= modPopupPromotionHelper::function01($params, $type);
require(JModuleHelper::getLayoutPath('mod_popup_promotion'));