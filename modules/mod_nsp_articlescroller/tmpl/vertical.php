<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<script type=text/javascript src="<?php echo JURI::base()?>modules/mod_nsp_articlescroller/tmpl/js/js_scrolling.js"></script>
<style type="text/css">
.nspnewscroller{
	background:#e6e1e1;
	height:20px;
	
	line-height:20px;
	vertical-align:middle;
	font-size:12px;
	margin-top:2px;
	margin-bottom:2px;
}
.nspnewscroller .title{
	float:left;
	width:15%;
	background:url(<?php echo JURI::base()?>modules/mod_nsp_articlescroller/tmpl/images/bg.png) no-repeat left;
	height:20px;
	line-height:20px;
	vertical-align:middle;
	padding-left:10px;
	color:#ffffff;
	}
.newsContainerScroll {
	FLOAT: left;
	width:75%;
}
#pscroller2 {
	PADDING-BOTTOM: 1px; PADDING-LEFT: 10px; WIDTH: 500px; PADDING-RIGHT: 1px;
	PADDING-TOP: 0px; height:20px;
	line-height:20px;
	vertical-align:middle;
}
#pscroller2 A, .nspnewscroller a {
	TEXT-DECORATION: none;
	color:#000000 !important;
}


</style>
<div class="nspnewscroller">
<div class="title png_bg">Tin túc & Sự kiện</div>
<DIV class="newsContainerScroll">
<SCRIPT type=text/javascript>
	    var pausecontent2=new Array();
			<?php
			$i=0;
			foreach( $rows as $r){
				$r->title = str_replace("'","&acute;",$r->title);
				echo "pausecontent2[$i]='<a href=".JRoute::_("index.php?option=com_content&view=article&id=$r->id&Itemid=60").">$r->title</a>';\n";
				$i++;
			}
			?>
	  //new pausescroller(name_of_message_array, CSS_ID, CSS_classname, pause_in_miliseconds)
	  new pausescroller(pausecontent2, "pscroller2", "someclass", 5000)
	    </SCRIPT>
</DIV>
</div>
