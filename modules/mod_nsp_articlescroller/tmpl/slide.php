<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<style type="text/css">
.nspnewscroller{
	height:15px;
	line-height:15px;
	vertical-align:middle;
	font-size:12px;
	margin-bottom:10px;
}
.nspnewscroller .title{
	float:left;
	width:11.5%;
	height:15px;
	line-height:15px;
	vertical-align:middle;
	padding-left:10px;
	color:#b21f24;
	border-right:1px solid #666666;
	}
.nspnewscroller .scroller{
	width:84.5%;
	float:right;
	text-align:left;
	}
</style>
<script language="javascript">
function setupLinks() {
	<?php
	$i=0;
	foreach( $rows as $r){
		$r->title = str_replace("'","&acute;",$r->title);
		echo "arrLinks[$i]='".JRoute::_("index.php?option=com_content&view=article&id=$r->id:$r->alias&Itemid=60")."';\n";
		echo "arrTitles[$i]='$r->title';\n";
		$i++;
	}
	?>

}


	
</script>
<div class="nspnewscroller">
<div class="title">Tin tức & Sự kiện</div>
<div class="scroller">
<?php
	$i = 0;
	foreach( $rows as $r):?>
	<a href="<?php echo JRoute::_("index.php?option=com_content&view=article&id=$r->id:$r->alias&Itemid=60")?>" id="newid<?php echo $i++?>"><?php echo $r->title?> &nbsp; ></a>
	<?php endforeach?>
	
</div>
</div>
<script language="javascript">
var nnews = <?php echo count($rows)?>;
var crrindex = 0;
var crrcolor = 0;
var colorarr = new Array();
colorarr[0] = '#666666';
colorarr[1] = '#c2bcbc';
colorarr[2] ='#dbd6d6';
colorarr[3] ='#eee9e9';
colorarr[4] ='#fbf7f8';
colorarr[5] ='#fdfdfd';
colorarr[6] ='#fefefe';
colorarr[7] ='#ffffff';
<?php
for( $i = 1; $i<count($rows) ; $i++){
	echo 'document.getElementById("newid'.$i.'").style.display="none";';
}
?>
document.getElementById('newid0').style.color = colorarr[0];

function showNew()
{
	hideText();
}
function showText(id){
	
	var id  = document.getElementById(id);
	id.style.display = 'block';
	id.style.color = colorarr[0];
}
function hideText(){
	var id = 'newid'+crrindex;
	var sid  = document.getElementById(id);
	if(crrcolor == 8){crrcolor = 0;
		sid.style.display = 'none';
		crrindex++;
		if(crrindex >= nnews ) crrindex = 0;
		showText('newid'+crrindex);
		
	}else{
		sid.style.color = colorarr[crrcolor];
		crrcolor++;
		setTimeout("hideText()", 100);
	}
}
setInterval("showNew()",3000);
</script>
