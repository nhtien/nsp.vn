<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
 
// no direct access
defined('_JEXEC') or die('Restricted access');

class modNspArticleScrollerHelper
{
	function getArticles($params, $type)
	{
		$db = &JFactory::getDBO();
		$catid = $params->get('category');
		$sesid= $params->get('section');
		
		$where[] = 'c.state=1';
		if( $catid ){
			$where[] = 'c.catid = '.$catid;
		}
		if( $sesid ){
			$where[] = 'c.sectionid='.$sesid;
		}
		
		$where = ' WHERE '.implode(' AND ', $where);
		$orderby = ' ORDER BY '.$params->get('news_sort_value','created') .' '. $params->get('news_sort_order','ASC');
		$limit = ' LIMIT 0,'.$params->get('limit',5);
		$query = "SELECT c.* FROM  #__content AS c ".$where.$orderby.$limit;
		$db->setQuery($query);
		$rows = $db->loadObjectList(); 
		return $rows;
	}
}
