<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
 
// no direct access
defined('_JEXEC') or die('Restricted access');

class modFeaturedBrandsHelper
{
	function getFeaturedProducts( $params, $type){
		$str_id = $params->get('featured_product_list_id');
		$filter[] = 'p.product_id IN ('. $str_id . ')';
		$rows = modSpecialProductsHelper::_getProducts($params->get('category_id'), null, $filter, 0, 5);
		$array_id = explode(",", $str_id); 
		while(list($k,$v) = each( $array_id) ){
			foreach( $rows as $r){
				if($r->product_id == $v)
					$r_rows[] = $r;
			}
		} 
		return $r_rows;
		//return modFeaturedBrandsHelper::_getProducts( $str_id, $params);
	}

	function getSpecialProducts( $params, $type){
		$str_id = $params->get('product_list_id');
		$filter[] = 'p.product_id IN ('. $str_id . ')';
		$rows = modSpecialProductsHelper::_getProducts($params->get('category_id'), null, $filter, 0, 8);
		
		$array_id = explode(",", $str_id); 
		while(list($k,$v) = each( $array_id) ){
			foreach( $rows as $r){
				if($r->product_id == $v){
					$str = $r->product_s_desc;
					$news_limit = 25;
					if(strlen($str) > $news_limit)
					{
						$start_strpos = 0;
						$j = 0;
						//
						for($i = 0; $i < $news_limit && $start_strpos < strlen($str); $i++)
						{
							//
							if(strpos($str, ' ', $start_strpos) !== FALSE)
							{
								//
								$start_strpos = strpos($str, ' ', $start_strpos) + 1;
								$j++;
							}	
						}
						
						$news_textt = trim($str);
						$news_textt = substr($news_textt, 0, $start_strpos);
						$news_textt .= "...";
					}
					if( $j >= $news_limit )
						$r->product_s_desc = $news_textt;
					$r_rows[] = $r;
				}
			}
		} 
		return $r_rows;
		//return modFeaturedBrandsHelper::_getProducts( $str_id, $params);
	}
	/*
	function _getProducts( $str_id, $params )
	{
		$db = &JFactory::getDBO();
		
		$where[] = '1';
		
		if( $params->get('category_id') ){
			$where[] = 'cf.category_id = '.$params->get('category_id');
		}
		
		$where[] = 'p.product_id IN ('. $str_id . ')';
		
		$where = implode(" AND ", $where);
		
		$query = "SELECT p.*, mf.*,cf.category_id, pr.product_price FROM #__vm_product AS p 
					LEFT JOIN #__vm_product_price AS pr ON pr.product_id = p.product_id
					INNER JOIN #__vm_product_category_xref AS cf ON cf.product_id = p.product_id
					INNER JOIN #__vm_product_mf_xref AS mfx ON mfx.product_id = p.product_id
					INNER JOIN #__vm_manufacturer AS mf ON mf.manufacturer_id = mfx.manufacturer_id
					WHERE ".$where.' GROUP BY p.product_id';
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		return $rows;
	}
	
	*/
	function getLists( $params , $stype){
		$lists = array();
		
		$db = &JFactory::getDBO();
		$sql = "SELECT * FROM #__vm_manufacturer AS m WHERE m.published=1 ORDER BY m.mf_name ASC";
		$db->setQuery( $sql );
		$rows = $db->loadObjectList();
		
		$option_brand[] = JHTML::_('select.option',' ','- '. JText::_('SELECT_BRAND') . ' - ' );
		foreach( $rows as $r){
			$option_brand[] = JHTML::_('select.option', $r->manufacturer_id , $r->mf_name );
		}
		$lists['brands'] = JHTML::_("select.genericList", $option_brand, 'brands','class="inputbox" onchange="goBrandPage(this.value);"','value','text', $value);
		
		return $lists;
	}
}
