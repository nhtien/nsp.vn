<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$param	= $params->get('param1',0);
$user = &JFactory::getUser();
$img = JURI::base()."images/";
$call_pricing = 'Call';
$ItemIdCallLayout = 92;
?>
<?php 
$document	= &JFactory::getDocument();
$document->addStyleSheet(JURI::base().'components/com_nsp/themes/style_tooltips.css');
$document->addScript(JURI::base().'components/com_nsp/js/tooltip.js');
$document->addScript(JURI::base().'components/com_nsp/js/nsp.js');
?>

<script language="javascript">
function goBrandPage( bid ){
	var url = 'index.php?option=com_nsp&view=brand&bid='+bid+'&Itemid=59';
	window.location.href = url;
}

addEvent(window, 'load', function(){
	nspCompareItemsHeight('compare_top_item');
	nspCompareItemsHeight('compare_row1_top');
	
});
addEvent(window, 'load', function(){
	nspCompareItemsHeight('compare_img_thumb');
	nspCompareItemsHeight('compare_logo');
	
});

</script>
<link href="<?php echo JURI::base()?>modules/mod_featured_brands/tmpl/images/style.css" rel="stylesheet" type="text/css" />
<div class="mod_featured_brands">
	<div class="header">
		<h3 class="png_bg"><?php echo JText::_('Featured Brands')?></h3>
		<?php echo $lists['brands']?>
		<span class="brows_cat"><a href="index.php?option=com_nsp&view=browsecategories&Itemid=18"><?php echo JText::_('Browse All Categories');?></a></span>
	</div>
	<div class="row01">
		<ul class="list">
		<?php
			foreach( $featuredProduct as $r){
				$full_img_path = 'documents'.DS.'vm_images'.DS.'product'.DS.$r->product_full_image;
				$subject = urlencode("Call: ".$r->product_name);
				$ask_link = JRoute::_('index.php?option=com_virtuemart&page=shop.ask&product_id='.$r->product_id.'&subject='.$subject.'&Itemid='.$ItemIdCallLayout);
				$ask_price = '<a href="'.$ask_link.'" >'.$call_pricing.'</a>';
				
				if( file_exists($full_img_path) ){ 
					$imginfo = getimagesize( JPATH_SITE.DS.$full_img_path );
				}
				$old_w =  $imginfo[0];
				$old_h =  $imginfo[1];
				$max_w = 250;
				$max_h = 250;
				if( $old_w > $max_w){
					$old_h = $old_h - ( $old_h * ( ($old_w - $max_w)/$old_w ) );
					$old_w = $max_w;
				}
				if( $old_h > $max_h){
					$old_w = $old_w - ( $old_w * ( ($old_h - $max_h)/$old_h) );
					$old_h = $max_h;
				}
				
				$full_img_w = ceil($old_w);
				$full_img_h = ceil($old_h); 
				
				//$full_img_w = $imginfo[0] < 200 ? 200 : $imginfo[0];
				//if( $full_img_w > 300 ) $full_img_w = 300;

				$pro_link = 'index.php?page=shop.product_details&flypage=flypage.tpl&product_id='.$r->product_id.'&category_id='.$r->category_id.'&option=com_virtuemart&Itemid=26';
				$brand_blink = "index.php?option=com_nsp&view=brand&bid=".$r->manufacturer_id."&Itemid=72";
		?>
			<li class="item">
				<div class="thumb">
					<div class="logo">
						<a href="<?php echo $brand_blink?>" title="<?php echo $r->mf_name?> Showcase">
							<img src="<?php echo JURI::base().'images/nsp/manufacturer/logos/'.$r->mf_logo?>" />
						</a>
					</div>
					<div class="thumb">
					
						<?php
						
						 $html_tip = '<div align=center><img src='.JURI::base().'product_image.php?style=1&filename='.urlencode($r->product_full_image).' width='.$full_img_w.' height='.$full_img_h.' /> </div>';
						 
						 ?>
						 <br />
						 <a href="<?php echo $pro_link?>">
						<img src="<?php echo JURI::base().'product_image.php?filename='.urlencode($r->product_thumb_image).'&style=0';?>" 
							onmouseover="ddrivetip('<?php echo $html_tip?>',<?php echo $max_w?>)" 
							onmouseout=" hideddrivetip();" 
						/>
						</a>
					</div>
				</div>
				<div class="info">
				  <div class="compare_row1_top">
					<a href="<?php echo $pro_link?>" class="name"><?php echo $r->product_name;?></a><br />
					<!--<span class="title">Product title here</span><br />-->
					<p class="short_des"><?php echo $r->product_s_desc?></p>
				  </div><!--compare_row1_top-->
					<?php echo JText::_('Price')?>: <br />
					
					<span class="price"><?php echo ($r->product_price) ? '$'.number_format($r->product_price,2) : $ask_price ;?></span>
					<div class="btn-more">
						<a href="<?php echo $pro_link?>" title="<?php echo $r->product_name?>">
							<img src="<?php echo $img.JText::_('MORE_IMG_SRC')?>" />
						</a>
					</div>
					<ul class="">
						<li><a href="index.php?page=shop.browse&category_id=<?php echo $r->category_id?>&option=com_virtuemart&Itemid=26">
						<?php echo JText::_('More Products')?></a> </li>
						<li><a href="<?php echo $brand_blink?>" title="<?php echo $r->mf_name?> Showcase"><?php printf(JText::_('BRAND SHOWCASE'),$r->mf_name)?></a> </li>
					</ul>

				</div>
			</li>
			<?php
			}
			?>

		</ul>
		
		<div class="break">&nbsp;</div>
	</div><!--row01-->
	
	<div class="row02">
		<ul class="list">
		<?php
			$i=1;
			$item_per_row = 4;
			foreach( $specialProducts as $r){
				$full_img_path = 'documents'.DS.'vm_images'.DS.'product'.DS.$r->product_full_image;
				$subject = urlencode("Call: ".$r->product_name);
				$ask_link = JRoute::_('index.php?option=com_virtuemart&page=shop.ask&product_id='.$r->product_id.'&subject='.$subject.'&Itemid='.$ItemIdCallLayout);
				$ask_price = '<a href="'.$ask_link.'" >'.$call_pricing.'</a>';
				if( file_exists($full_img_path) ){  
					$imginfo = getimagesize( JPATH_SITE.DS.$full_img_path );
				}
				$old_w =  $imginfo[0];
				$old_h =  $imginfo[1];
				$max_w = 250;
				$max_h = 250;
				if( $old_w > $max_w){
					$old_h = $old_h - ( $old_h * ( ($old_w - $max_w)/$old_w ) );
					$old_w = $max_w;
				}
				if( $old_h > $max_h){
					$old_w = $old_w - ( $old_w * ( ($old_h - $max_h)/$old_h) );
					$old_h = $max_h;
				}
				
				$full_img_w = ceil($old_w);
				$full_img_h = ceil($old_h); 
				
				$pro_link = 'index.php?page=shop.product_details&flypage=flypage.tpl&product_id='.$r->product_id.'&category_id='.$r->category_id.'&option=com_virtuemart&Itemid=26';
				$brand_blink = "index.php?option=com_nsp&view=brand&bid=".$r->manufacturer_id."&Itemid=72";
		?>
			<li class="item">
			  <div class="inner <?php if( $i%$item_per_row == 0) echo 'last';?>">
				<div class="logo compare_logo">
					<a href="<?php echo $brand_blink?>" title="<?php echo $r->mf_name?> Showcase">
						<img src="<?php echo JURI::base().'images/nsp/manufacturer/logos/'.$r->mf_logo?>" />
					</a>
				</div>
				<div class="info">
				  <div class="compare_top_item">
					<a href="<?php echo $pro_link;?>" class="name"><?php echo $r->product_name;?></a><br />
					<!--<span class="title">Enter text here.</span><br />-->
					<p class="short_des"><?php echo $r->product_s_desc;?> </p>
				  </div>
					
					<div class="thumb compare_img_thumb">
						
						<?php
						
						 $html_tip = '<div align=center><img src='.JURI::base().'product_image.php?style=1&filename='.urlencode($r->product_full_image).' width='.$full_img_w.' height='.$full_img_h.' /> </div>';
						 
						 ?>
						 <a href="<?php echo $pro_link?>">
						<img src="<?php echo JURI::base().'product_image.php?filename='.urlencode($r->product_thumb_image).'&style=0'; ?>" 
							onmouseover="ddrivetip('<?php echo $html_tip?>',<?php echo $max_w ?>)" 
							onmouseout=" hideddrivetip();" vspace="2"
						/>
						</a>
					</div>
					<div style="float:right;">
						<?php echo JText::_('Price')?>: <br />
					
						<span class="price"><?php echo ($r->product_price) ? '$'.number_format($r->product_price,2) : $ask_price ;?></span>
						<div class="btn-more"><a href="<?php echo $pro_link;?>"><img src="<?php echo $img.JText::_('MORE_IMG_SRC')?>" /></a></div>
					</div>
					
					<ul class="">
						<li><a href="index.php?page=shop.browse&category_id=<?php echo $r->category_id?>&option=com_virtuemart&Itemid=26">
						<?php echo JText::_('More Products')?></a> </li>
						<li><a href="<?php echo $brand_blink?>" title="<?php echo $r->mf_name?> Showcase"><?php printf(JText::_('BRAND SHOWCASE'),$r->mf_name)?></a> </li>
					</ul>

				</div>
			  </div>
			</li>
		<?php
		if($i%4==0 && $i<count($specialProducts)) echo '<li class="break"><hr class="break" size="1" /><li>';
		$i++;
		 } ?>
		</ul>
		<div class="break">&nbsp;</div>
	</div><!-- row02-->
	
</div>

