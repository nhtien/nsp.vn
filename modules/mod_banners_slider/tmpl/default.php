<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$param	= $params->get('param1',0);
$numColumn = $params->get('columns',2);
$rows = $project;
$tempimg = '<img src="images/spacer.gif" border="0" />';
$m_width = 955;// width of site
$m_height = 125;

?>
<style type="text/css">
.appsCarousel , .btnRight , .divDashLeft , .btnLeft , .divDashRight{
	height:<?php echo $m_height?>px;
	}
</style>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="border">
<tr>
	<td class="top-left corner"><?php echo $tempimg;?></td><td class="top-middle"><?php echo $tempimg;?></td><td class="top-right corner"><?php echo $tempimg;?></td>
</tr>
<tr>
	<td class="body-left"><?php echo $tempimg;?></td>
	<td class="body-middle">
<!-- body -->

<div class="inner">
<?php
$img_folder = JURI::base()."modules/mod_banners_slider/tmpl/images";
?>

<link rel=stylesheet type=text/css href="<?php echo $img_folder;?>/navigation.css">
<script language=javascript type=text/javascript src="<?php echo $img_folder;?>/navigation.js"></script>


<div >
  <div >
  <div >
<div class="pMid">

<div class="appsCarousel">
<div id="nav" class="navMenu">
<div id="btnLeftDiv" class="btnLeft colorE" onmousemove="javascript:onMove(this.id);" onmouseout="javascript:onOut(this.id);" onclick="javascript:goPrev('<?php echo $img_folder;?>/');" align=center>
	<IMG id="imgPrev" onclick="javascript:goPrev('<?php echo $img_folder;?>/');" border=0 alt="" vspace="0" src="<?php echo $img_folder;?>/previuos_dis.png"> 
</div>
<div class="divDashLeft"></div>
<div style="BORDER-BOTTOM: red 0px solid; POSITION: absolute; BORDER-LEFT: red 0px solid; WIDTH: 19500px; HEIGHT: <?php echo $m_height?>px; BORDER-TOP: red 0px solid; BORDER-RIGHT: red 0px solid; LEFT: 51px" id="sourceDiv">
	<?php
		$t_width = $params->get('t_width');
		$t_height = $params->get('t_height');
	$i=0;
	$n = count( $rows );

foreach($list as $item) :

	?>
	<div style="Z-INDEX: 1; WIDTH: 179px; FLOAT: left; HEIGHT: <?php echo $m_height?>px; LEFT: 0px" id="div_<?php echo $i;?>">
	  <div class="appCar">
		<div class="banneritem<?php echo $params->get( 'moduleclass_sfx' ) ?>">
		<?php echo modBannersSliderHelper::renderBanner($params, $item);?>
		</div>
	  </div>
	</div>
<?php
$i++;
 endforeach; ?>




</div>



<div class="divDashRight"></div>
<div id="btnRightDiv" class="btnRight colorE" onmousemove="javascript:onMove(this.id);" onmouseout="javascript:onOut(this.id);" onclick="javascript:goNext('<?php echo $img_folder;?>/');" align="right">
<img id="imgNext" onclick="javascript:goNext('<?php echo $img_folder;?>/');" border=0 alt="" vspace="0" src="<?php echo $img_folder;?>/next.png"> 
</div></div></div></div>
<div id="carBotLeft" class="pBotLeftCar"></div>
<div class="pBotMidCar"></div>

<div id="carBotRight" class="pBotRightCar"></div></div>
<div style="MARGIN-LEFT: 42px" class="left"><!-- YB: m_rectangle_b (300x250) -->
</div>

<div style="MARGIN-LEFT: 10px" class="left"><!-- YB: m_rectangle_b (300x250) -->
</div>

<div style="CLEAR: both"></div> </div>
  <div style="CLEAR: both"></div> </div>
  
<SCRIPT type=text/javascript>var imgPlaces=5; initButtons('<?php echo $img_folder;?>/');</SCRIPT>
</div><!--inner-->

<!-- end body -->
	</td>
	<td class="body-right"><?php echo $tempimg;?></td>
</tr>
<tr>
	<td class="bottom-left corner"><?php echo $tempimg;?></td><td class="bottom-middle"><?php echo $tempimg;?></td><td class="bottom-right corner"><?php echo $tempimg;?></td>
</tr>
</table>
