function changeImage( id, newSrc )
{
	var block = document.getElementById(id);
	block.src = newSrc;
}

function addImage( id, imgSrc, w, h )
{
	var block = document.getElementById(id);
	block.innerHTML = '<img src="' + imgSrc + '" width="' + w + '" height="' + h + '" />';
}

function changeStyle( id, styleStr )
{
	var block = document.getElementById(id);
	if( !block.originalStyle ) block.originalStyle=block.style.cssText;
	block.style.cssText = block.style.cssText + '; ' + styleStr;
}

function restoreStyle( id )
{
	var block = document.getElementById(id);
	if( block.originalStyle ) block.style.cssText=block.originalStyle;
}

function changeClass( id, newClass )
{
	var block = document.getElementById(id); //if( block == null ) alert( id +","+ newClass);
	if( !block.originalStyleClass ) block.originalStyleClass=block.className;
	block.className = newClass;	
}

function restoreClass( id )
{
	var block = document.getElementById(id);
	if( block.originalStyleClass ) block.className=block.originalStyleClass;	
}

function showSub(){
	var sub = document.getElementById('sub');
	sub.style.display = 'block';
	//changeClass( 'sep6', 'seperatorSel' ); changeClass( 'Item7', 'menuItemExpandSel' ); changeClass( 'icon', 'expandIconSel' );
}

function hideSub() {
	var sub = document.getElementById('sub');
	sub.style.display = 'none';
	//restoreClass( 'sep6' ); restoreClass( 'Item7' ); restoreClass( 'icon' );
}

function mouseBottomRightOver() {
	var item = document.getElementById('bottom_right');
	item.className = 'bottom_right_sel'; 
}

function mouseBottomRightOut() {
	var item = document.getElementById('bottom_right');
	item.className = 'bottom_right'; 	
}

function mouseBottomLeftOver() {
	var item = document.getElementById('bottom_left');
	item.className = 'bottom_left_sel'; 
}

function mouseBottomLeftOut() {
	var item = document.getElementById('bottom_left');
	item.className = 'bottom_left'; 	
}


//-------------------------------------------------

function AJAX_GET( link ) {
	var http_request = false; 
	if(window.XMLHttpRequest) http_request=new XMLHttpRequest();
	else if (window.ActiveXObject) http_request = new ActiveXObject("Microsoft.XMLHTTP");	
	if( !http_request ) return false; 
	
	window.status = "Please wait...";
	link += '&' + Math.floor( Math.random() * 100000000 ); /* IE caching AJAX */
	http_request.open('GET', link, false); 
	http_request.send("empty"); 
	window.status = ""; 
	
	if (http_request.readyState == 4 && http_request.status == 200 ) return http_request.responseText; 
	else return false;
}

function AJAX_POST( url, parameters ) {
	var http_request = false; 
	if(window.XMLHttpRequest) http_request=new XMLHttpRequest();
	else if (window.ActiveXObject) http_request = new ActiveXObject("Microsoft.XMLHTTP");	
	if( !http_request ) return false;
 
	http_request.open('POST', url, false);
	http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_request.setRequestHeader("Content-length", parameters.length);
	http_request.setRequestHeader("Connection", "close");
	http_request.send(parameters);
	
	if (http_request.readyState == 4 && http_request.status == 200 ) return http_request.responseText; 
	else return false;	
}

function findPosX( obj ) {
	var curleft = 0;
	if( obj.offsetParent ) {
	    while( 1 ) {
	      curleft += obj.offsetLeft;
	      if( !obj.offsetParent ) break;
	      obj = obj.offsetParent;
	    }
	} else if( obj.x ) curleft += obj.x;
	return curleft;
}		  
				  
function findPosY( obj ) {
	var curtop = 0;
	if( obj.offsetParent ) {
	    while( 1 ) {
	      curtop += obj.offsetTop;
	      if( !obj.offsetParent ) break;
	      obj = obj.offsetParent;
	    }
	} else if( obj.y ) curtop += obj.y;
	return curtop;
}

function addEvent( obj, evType, fn ) {
	if( obj.addEventListener ) {
		obj.addEventListener( evType, fn, false );
		return true;
	} else if( obj.attachEvent ) return obj.attachEvent("on"+evType, fn);
	else return false;
}

function getScroll(){
	if( self.pageYOffset ) return posY = self.pageYOffset;
	if( document.documentElement && document.documentElement.scrollTop ) return posY = document.documentElement.scrollTop;
	if( document.body ) return posY = document.body.scrollTop;
}

function setScrollTo( x, y ) {
	window.scrollTo( x, y ); 
}

function browserIsIE6() {
	var version = parseFloat(navigator.appVersion.split('MSIE')[1])
	return version <= 6;
}

function changeOpacity ( opacity, id ) {
	var object = document.getElementById( id ).style;
	object.opacity = (opacity / 100);
	object.MozOpacity = (opacity / 100);
	object.KhtmlOpacity = (opacity / 100);
}

function trimString( str ) {
	return str.replace( /^\s+|\s+$/g, '' );
}

function setFocus( elemId ) {
	var elem = document.getElementById( elemId );
	if( elem ) elem.focus();
}


/*limit textarea chars and count*/
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

function hideSortBox(hide) {
	if( document.getElementById('sortBox') ) 
	{
		if(hide) document.getElementById('sortBox').style.visibility = 'hidden';
		else document.getElementById('sortBox').style.visibility = 'visible';
	}
}