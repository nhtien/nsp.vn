var divScroll = 0;
var gCalloutTimer = '';
var gCalloutTimerOn = false;

function startCloseCalloutTimer() {
	gCalloutTimerOn = true;
	gCalloutTimer = setTimeout('callout.hide(); gCalloutTimerOn = false;', 10000);
}

function clearCloseCalloutTimer() {
	if( gCalloutTimerOn !== false ) {
		clearTimeout( gCalloutTimer );
		gCalloutTimerOn = false;
	}
}

function setSearchResult( result ) {
	document.getElementById( 'searchPhoneResultsContainer' ).innerHTML = result;
	var searchResultsDiv = document.getElementById( 'searchPhoneResults' );
	if( searchResultsDiv !== null ) searchResultsDiv.scrollTop = divScroll;
}

var lasttimeout = 0;
function enterPhoneModel( keyword, event ) {
	divScroll = 0;
	keyword = trimString( keyword );
	if( !keyword || keyword.length < 2 || event.keyCode == 32 ) return setSearchResult( '' );
	if( lasttimeout ) clearTimeout(lasttimeout);
	lasttimeout = setTimeout('displayMatchedPhonesDialog(\"'+keyword+'\", "15")', 300 );		
}

/* STEP 1 - Show callout to select a type of phone */
function displayPhoneCallout( skipUserSession ) {
	dialog.hide();
	callout.show( 'phoneCallout', skipUserSession ? {'skipUserSession':true} : '', 'assignCallout' );
	if( document.getElementById( 'myScript' ) !== null ) eval( document.getElementById( 'myScript' ).innerHTML );
}

/* STEP 2 - Show window with search phone results */
function displayMatchedPhonesDialog( keyword, displayItemsCount ) {
	var result = AJAX_GET( '/php/web2/ajax.php?action=dialog&type=searchPhones&keyword='+keyword+'&itemsCount='+displayItemsCount );
	if( result ) setSearchResult( result );
}

function saveScrollPos(){
    divScroll = document.getElementById("searchPhoneResults").scrollTop;
}

/* STEP 3 - Show modal dialog with selected phone */
function displaySelectedPhoneDialog( deviceID ) {
	callout.hide();
	dialog.show( 'showSelectedDevice', {'deviceID':deviceID} );
}

/* STEP 4 - Set phone */
function setUserDevice( deviceID ) {
	var url = '/php/web2/ajax.php?action=dialog&type=setUserDevice&deviceID='+deviceID+'&download='+startedDownloadChain()+'&url='+encodeURIComponent(document.location.href);
	var result = AJAX_GET( url );
	if( result ) onPhoneFlowCompleted( result );
}

function onPhoneFlowCompleted( redirectURL ) {
	gPageMustReload = true;
	if( gDlgCallBackFunc ) eval( gDlgCallBackFunc );
	if( gDlgType ) callDialogChain( true, gDlgType, gDlgParams );
	else setTimeout(function(){window.location.href = redirectURL;}, 0); //IE6 is full of many wonderful surprises that require equally wonderful work-arounds.
}

var gDlgType = '';
var gDlgParams = new Array();
var gPageMustReload = false;
var gDlgCallBackFunc = '';

function callDialogChain( deviceIsSet, type, params, callBackFunc ) {
	gDlgType = type;
	gDlgParams = params;
	gDlgCallBackFunc = callBackFunc;
	
	if( deviceIsSet ) {
		callout.hide();
		dialog.show( gDlgType, gDlgParams );
	} else {
		dialog.hide();
		callout.show( gDlgType, gDlgParams, 'assignCallout' );
	}
	if( document.getElementById( 'myScript' ) !== null ) eval( document.getElementById( 'myScript' ).innerHTML );
}

function breakDialogChain() {
	gDlgType = '';
	gDlgParams = new Array();
	dialog.hide( gPageMustReload );
}

function startedDownloadChain() {
	return gDlgType ? 1 : 0;
}

function submitCountryChange( ) {
	var result = AJAX_GET( '/php/web2/ajax.php?action=dialog&type=setUserCountry&countryID='+document.getElementById('userCountry').value );
	dialog.hide( true );
}

function submitLanguageChange( ) {
	var result = AJAX_GET( '/php/web2/ajax.php?action=dialog&type=setUserLanguage&languageCode='+document.getElementById('userLanguage').value );
	dialog.hide( true );
}

function cleanPhoneBox( obj ) {
	if( obj.value == obj.defaultValue ) {
		obj.value = '';
		obj.className = '';
	}
}

function fillPhoneBox( obj ) {
	if( obj.value == '' ) {
		obj.value = obj.defaultValue;
		obj.className = 'phoneTypeDefaultValue';
	}
}