
var source = new Array();
var selItem = 1;
var oldItem = '';
var actItem = 1;
var actSubItem = 1;
var hideItem = 1;
var hideSubItem = 1;
var timer='';
var timerI = 0;

function getObj( id ) {
	return document.getElementById( id );
}

function setSource( listID, itemID ) {	
	if( getObj( 'div_'+hideItem+'_'+hideSubItem ).style.display != '' ) getObj( 'div_'+hideItem+'_'+hideSubItem ).style.display = '';
	var contentOld = 'div_'+actItem+'_'+actSubItem; 
	var contentNew = 'div_'+listID+'_'+itemID; 	
	var contentImg = 'img_'+listID+'_'+itemID; 	
	
	if( listID != actItem || itemID != actSubItem ) {	
		hideItem = actItem;		
		hideSubItem = actSubItem;					
		
		zxcBAnimator('opacity#',contentOld,100,0,750);			
		getObj( contentImg ).src = getObj( contentNew ).lang;
		getObj( contentNew ).style.display = 'block';		
		zxcBAnimator('opacity#',contentNew,0,100,750);	
		
		actItem = listID;
		actSubItem = itemID;
	} else {
		if( getObj( contentNew ).style.display != 'block' ) 
			getObj( contentNew ).style.display = 'block';			
		getObj( contentImg ).src = getObj( contentNew ).lang;
	}
}

function getActiveParent() {
	for(var i=1;i<=10;i++) {
		var obj = getObj( 'parent'+i );		
		if( obj.className == 'sel' ) return i;
	}		
	return false;
}
	
function deactivateParents() {
	for(var i=1;i<=10;i++) {
		var obj = getObj( 'parent'+i );
		obj.className = '';
	}	
}

function setActiveParent( id ) {
	var obj = getObj( 'parent'+id );
	obj.className = 'sel';	
}

function deactivateItems() {
	for( var i=1;i<=10;i++ ) {
		var obj = getObj( 'ulist'+i );		
		obj.style.display = 'none';		
		//obj.style.top = -((i-1) * 27)+'px';				
	}			
}

function setActivateItems( id ) {
	var obj = getObj( 'ulist'+id );
	obj.style.display = 'block';	
}

function parentClick( id, sub ) {
	clearTimeout(timer);
	deactivateParents();	
	setActiveParent( id );
	deactivateItems();
	setActivateItems( id );		
	deactivateSubItems( id );
	setActivateSubItem( id, sub );
	setSource( id, sub );			
}

function showItem( listID, itemID ) {
	deactivateItems();
	setActivateItems( listID );	
	deactivateSubItems( listID );
	setActivateSubItem( listID, itemID );
	setSource( listID, itemID );	
}

function offHighliteParent() {
	for(var i=1;i<=10;i++) {
		var obj = getObj( 'parent'+i );
		if( obj.className == 'highliteParent' ) obj.className = '';
	}		
}

function onHighliteParent( listID ) {
	var obj = getObj( 'parent'+listID );
	if( obj.className == '' ) obj.className = 'highliteParent';	
}
	
function deactivateSubItems( listID ) {
	for(var i=1;i<=10;i++) {
		var obj = getObj( 'list'+listID+'_item'+i );
		if( obj != null) obj.className = '';
	}	
}

function setActivateSubItem( listID, itemID ) {
	var obj = getObj( 'list'+listID+'_item'+itemID );
	if( obj != null) obj.className = 'selItem';	
}

function itemMouseMove( listID, itemID ) {
	oldItem = listID;
	selItem = itemID;	
}

function initializeVertMenu() {
	deactivateParents();	
	setActiveParent( 1 );
	deactivateItems();
	setActivateItems( 1 );		
	setSource( 1, 1 );
	scrollHIS3();	
}

function getSoftsCount( item ) {	
	var count = 0;
	for(var j=1;j<=10;j++) {
		if( getObj('div_'+item+'_'+j) == null ) break;
		count ++;
	}		
	return count;
}

function nextItem( item, sub ) {	
	setItem = item+1;
	setSubItem = sub;	
	if( setItem == 11 )	{
		setItem = 1; 									
		setSubItem = setSubItem + 1;			
		if( setSubItem == 11 ) setSubItem = 1;
	}				
	if( setSubItem > getSoftsCount( setItem )) nextItem( setItem, setSubItem );	
	deactivateParents();	
	setActiveParent( setItem );
	deactivateItems();
	setActivateItems( setItem );	
	deactivateSubItems( setItem );
	setActivateSubItem( setItem, setSubItem );
	setSource( setItem, setSubItem );				
}

function scrollHIS3(){
	clearTimeout(timer);
	timerI = timerI + 1;
	if( timerI == 20 ) {
		timerI = 0;				
		nextItem( actItem, actSubItem );
	}			
	timer=setTimeout("scrollHIS3()", 500 );
}