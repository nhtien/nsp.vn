function overlayObject_IEOverlay6( height, overflow ) { 
	this.body.style.height = height;
	this.body.style.overflow = overflow;
	html = document.getElementsByTagName('html')[0];
	html.style.height = height;
	html.style.overflow = overflow;
}

function overlayObject_opacity( id, opacStart, opacEnd, millisec ) {
	var speed = Math.round( millisec / 100 );
	var timer = 0;

	if( opacStart > opacEnd ) {
		for( i = opacStart; i >= opacEnd; i-- ) {
			setTimeout("changeOpacity(" + i + ",'" + id + "', true)",(timer * speed));
			timer++;
		}
	} else if(opacStart < opacEnd) {
		for( i = opacStart; i <= opacEnd; i++ ) {
			setTimeout("changeOpacity(" + i + ",'" + id + "')",(timer * speed));
			timer++;
		}
	}
}

function overlayObject_isChildOfOverlay( elem ) {
	var parent = elem.parentNode;
	if( !parent ) return false;
	if( parent.id == this.overlayContainerId ) return true;
	return this.isChildOfOverlay( parent );
}

function overlayObject_changeDropDownElementsVisibility( visibility ) {
	var elem = document.getElementsByTagName( 'select' );
	for( i = 0; i < elem.length; i++ )
		if( !this.isChildOfOverlay(elem[i]) ) elem[i].style.visibility = visibility;
}

function overlayObject_initialize() {	
	if( !this.body ) this.body = document.getElementsByTagName('body')[0];
	
	this.overlayContainer = document.getElementById( this.overlayId );
	if( this.overlayContainer == null ) {
		this.overlayContainer = document.createElement('div');
		this.overlayContainer.id = this.overlayId;
		this.body.appendChild( this.overlayContainer );
	}
	
	this.overlayContentContainer = document.getElementById( this.overlayContainerId );
	if( this.overlayContentContainer == null ) {
		this.overlayContentContainer = document.createElement('div');
		this.overlayContentContainer.id = this.overlayContainerId;
		if( this.assignToDivId ) {
			var assignDiv = document.getElementById( this.assignToDivId );
			assignDiv.insertBefore( this.overlayContentContainer, assignDiv.firstChild );
		} else this.body.appendChild( this.overlayContentContainer );
	}
}

function overlayObject_changeVisibility( visibility ) {
	this.overlayContainer.style.display = visibility;
	this.overlayContentContainer.style.display = visibility;
}

function overlayObject_show() {
	var url = '/php/web2/ajax.php?action=dialog&type='+this.ajaxCallType;
	if( this.ajaxArguments )
		for( key in this.ajaxArguments ) url += '&'+key+'='+this.ajaxArguments[key];
	
	var result = AJAX_GET( url );
	this.overlayContentContainer.innerHTML = result;
	
	this.opacity( this.overlayId, 0, 20, 2000 );
	this.opacity( this.overlayContainerId, 0, 100, 600 );
	this.changeVisibility( 'block' );
	
	if( browserIsIE6() ) {
		getScroll();
		this.IEOverlay6( '100%', 'hidden' );
		setScrollTo( 0, 0 );
		this.changeDropDownElementsVisibility( 'hidden' );
	}
}

function overlayObject_hide() {
	if( this.overlayContentContainer == null ) return false;

	if( browserIsIE6() ) {
		setScrollTo( 0, posY );
		this.IEOverlay6( 'auto', 'auto' );
		this.changeDropDownElementsVisibility( 'visible' );
	}
	
	this.changeVisibility( 'none' );
	changeOpacity( 0, this.overlayContainerId );
	changeOpacity( 0, this.overlayId );
	this.overlayContentContainer.innerHTML = '';
}

function overlayObject() {
	this.body = '',
	this.overlayContainerId = '',
	this.overlayId = '',
	this.overlayContentContainer = '',
	this.overlayContainer = '',
	this.ajaxCallType = '',
	this.ajaxArguments = '',
	this.assignToDivId = '',
	this.visible = false;
	this.initialize = overlayObject_initialize;
	this.IEOverlay6 = overlayObject_IEOverlay6;
	this.opacity = overlayObject_opacity;
	this.changeVisibility = overlayObject_changeVisibility;
	this.show = overlayObject_show;
	this.hide = overlayObject_hide;
	this.changeDropDownElementsVisibility = overlayObject_changeDropDownElementsVisibility;
	this.isChildOfOverlay = overlayObject_isChildOfOverlay;
}

callout = {
	
	overlayObject : '',
		
	show : function( type, arguments, assignToDivId ) {
		this.overlayObject = new overlayObject();
		if( !dialog.visible ) {
			window.scroll(0,0);
			this.overlayObject.overlayId = 'popupContainerOverlay';
			this.overlayObject.overlayContainerId = 'popupContainer';
			this.overlayObject.ajaxCallType = type;
			this.overlayObject.ajaxArguments = arguments;
			this.overlayObject.assignToDivId = assignToDivId;
			this.overlayObject.initialize();
			this.overlayObject.overlayContainer.onclick = function() { clearCloseCalloutTimer(); callout.hide(); }
			this.overlayObject.show();
		}
	},
		
	hide : function( reload ) {
		if( this.overlayObject ) {
			this.overlayObject.hide();
			if( reload ) window.location.reload( true );
		}
	}
	
}

dialog = {
	
	overlayObject : '',
	visible : false,
	
	show : function( type, arguments ) {
		window.scroll(0,0);
		this.overlayObject = new overlayObject();
		this.overlayObject.overlayId = 'popupContainerOverlay';
		this.overlayObject.overlayContainerId = 'popupContainerCentered';
		this.overlayObject.ajaxCallType = type;
		this.overlayObject.ajaxArguments = arguments;
		this.overlayObject.initialize();
		this.overlayObject.overlayContainer.onclick = null;
		this.overlayObject.show();
		this.visible = true;
	},
	
	hide : function( reload ) {
		if( this.overlayObject ) {
			this.overlayObject.hide();
			if( reload ) window.location.reload( true );
			this.visible = false;
		}
	}
	
}
