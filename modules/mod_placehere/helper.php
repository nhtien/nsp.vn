<?php
/**
 * @version		mod_placehere alpha
 * @package		Joomla
 * @copyright	Copyright (C) 2007 Eike Pierstorff eike@diebesteallerzeiten.de
 * @license		GNU/GPL, see LICENSE.php
 *
 * 11/18/07 Line  72 Correct date routine
 * 08/03/08 Sql for section/category display, added ordering options
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

class modPlaceHereHelper
{
	function getList(&$params)
	{
		global $mainframe;
		JPluginHelper::importPlugin('content');

		// $dispatcher	   =& JEventDispatcher::getInstance();
		$dispatcher	   =& JDispatcher::getInstance();
		$db			=& JFactory::getDBO();
		$user		=& JFactory::getUser();
		$userId		= (int) $user->get('id');
		$show_front	= $params->get('show_front', 1);
		$aid		= $user->get('aid', 0);
		$count		= (int) $params->get('count', false);
		// Id of currently displayed article
		$curid		= JRequest::getVar('id', 0, '', 'int');
		$hide_current	= $params->get('hide_current', 0);

		//Item, cat, section
		$type		= trim( $params->get('type') );
		//  ID or comma separated lists of ids
		$showbyid		= trim( $params->get('showbyid') );

		// map id to type
		$cimid = $catid = $secid = false;
		switch($type) {
	  // Items
			// Cats
	  case '3':
	  	$secid = true;
	  	if($params->get("show_section") == 1) {
	  		$params->set('section',1);
	  	}
	  	break;
	  case '2':
	  	$catid = true;
	  	if($params->get("show_category") == 1) {
	  		$params->set('category',1);
	  	}
	  	break;
	  case '1':
	  default:
	  	$cimid = true;
	  	break;
	 }

		$contentConfig = &JComponentHelper::getParams( 'com_content' );
		$access		= !$contentConfig->get('shownoauth');

		$nullDate	= $db->getNullDate();

		// $now		= date('Y-m-d H:i:s', time());
		// 18/11/07 Correct date routine as suggested by eathaiku
		// http://diebesteallerzeiten.de/blog/module-for-15-alpha/#comment-1275
		$date = new JDate();
		$now = $date->toMySQL();

		$where		= 'a.state = 1'
		. ' AND ( a.publish_up = '.$db->Quote($nullDate).' OR a.publish_up <= '.$db->Quote($now).' )'
		. ' AND ( a.publish_down = '.$db->Quote($nullDate).' OR a.publish_down >= '.$db->Quote($now).' )'
		;

		if(	$hide_current ) {
		 $where .= " AND a.id != '" . $curid . "' ";
		}
			
		// User Filter
		switch ($params->get( 'user_id' ))
		{
			case 'by_me':
				$where .= ' AND (created_by = ' . (int) $userId . ' OR modified_by = ' . (int) $userId . ')';
				break;
			case 'not_me':
				$where .= ' AND (created_by <> ' . (int) $userId . ' AND modified_by <> ' . (int) $userId . ')';
				break;
		}

		// Ordering
		switch ($params->get( 'ordering' ))
		{
			case 'o_dsc':
				$ordering = 'a.ordering DESC';
				break;
			case 'o_asc':
				$ordering = 'a.ordering ASC';
				break;
			case 'm_dsc':
				$ordering		= 'a.modified DESC, a.created DESC';
				break;
			case 's_asc':	
				$ordering		= 'a.sectionid ASC';
				break;
			case 's_dsc':
				$ordering		= 'a.sectionid DESC';
				break;	
			case 'cy_asc':	
				$ordering		= 'a.catid ASC, a.sectionid ASC';
				break;
			case 'cy_dsc':
				$ordering		= 'a.catid DESC, a.sectionid DESC';
				break;					
			case 'random':
				$ordering = 'RAND()';
				break;
			case 'c_dsc':
			default:
				$ordering		= 'a.created DESC';
				break;
		}
		
		switch ($params->get( 'flip_frontpage' ))
		{
		 case(1):
		  $flip = " NOT ";
		 break;
		 default:
		  $flip = "  ";
		 break;
		}
		
		// Ordering
		switch ($params->get( 'sec_ordering' ))
		{
			case 'o_dsc':
				$ordering .= ',a.ordering DESC';
				break;
			case 'o_asc':
				$ordering .= ',a.ordering ASC';
				break;
			case 'm_dsc':
				$ordering	 .= ',a.modified DESC, a.created DESC';
				break;
			case 's_asc':	
				$ordering	 .= ',a.sectionid ASC';
				break;
			case 's_dsc':
				$ordering		.= ',a.sectionid DESC';
				break;	
			case 'cy_asc':	
				$ordering		.= ',a.catid ASC, a.sectionid ASC';
				break;
			case 'cy_dsc':
				$ordering		.= ',a.catid DESC, a.sectionid DESC';
				break;					
			case 'random':
				$ordering .= ',RAND()';
				break;
			case 'c_dsc':
				$ordering		= 'a.created DESC';
				break;
			case 'none':
			default:
			break;
		}

		$Condition = "";
		if ($cimid)
		{
			$ids = explode( ',',  $showbyid );
			JArrayHelper::toInteger( $ids );
			$Condition = ' AND (a.id=' . implode( ' OR a.id=', $ids ) . ')';
		}

		if ($catid)
		{
			$ids = explode( ',',  $showbyid );
			JArrayHelper::toInteger( $ids );
			$Condition = ' AND (cc.id=' . implode( ' OR cc.id=', $ids ) . ')';
		}
		if ($secid)
		{
			$ids = explode( ',',  $showbyid );
			JArrayHelper::toInteger( $ids );
			$Condition = ' AND (s.id=' . implode( ' OR s.id=', $ids ) . ')';
		}

		// Content Items only
		// John Woelfel - +18 lines - Change query to allow for  static content items in Joomla 1.5
		// 04/04/08 Mike Bronner fixed sql to display author names
		if($cimid)
		{
			$query = 'SELECT a.*, ' .
			' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'.
			' \'\' as catslug,'.
			' 0 as catid, \'\' as category, ' .
			' 0 as sectionid, \'\' as section' .	
			' , u.name AS author '	.						
			' FROM #__content AS a' .
			($show_front == '0' ? ' LEFT JOIN #__content_frontpage AS f ON f.content_id = a.id' : '') .
			' LEFT JOIN #__users AS u ON u.id = a.created_by '.
			' WHERE '. $where .
			($access ? ' AND a.access <= ' .(int) $aid. ' ':'').
			$Condition .
			($show_front == '0' ? ' AND f.content_id IS  ' . $flip . ' NULL ' : '').
			' ORDER BY '. $ordering;
		}
		else
		{
		// 08/03/08 Sql Error corrected by Maurice
		// http://diebesteallerzeiten.de/blog/module-for-15-alpha/#comment-2634
			$query = 'SELECT a.*, ' .
			' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'.
			' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug'.
			' , cc.id as catid, cc.title as category ' .
			' , s.id as sectionid, s.title as section' .
			' , u.name AS author '	.		
			' FROM #__content AS a' .
			($show_front == '0' ? ' LEFT JOIN #__content_frontpage AS f ON f.content_id = a.id' : '') .
			' INNER JOIN #__categories AS cc ON cc.id = a.catid' .
			' INNER JOIN #__sections AS s ON s.id = a.sectionid' .
			' LEFT JOIN #__users AS u ON u.id = a.created_by '.
			' WHERE '. $where .' AND s.id > 0' .
			($access ? ' AND a.access <= ' .(int) $aid. ' AND cc.access <= ' .(int) $aid. ' AND s.access <= ' .(int) $aid : '').
			$Condition .
			($show_front == '0' ? ' AND f.content_id IS  ' . $flip . ' NULL ' : '').
			' AND s.published = 1' .
			' AND cc.published = 1' .
			' ORDER BY '. $ordering;

		}
		
		if($count) {
		 $db->setQuery($query, 0, $count);
		} else {
		 $db->setQuery($query);
		}
		$rows = $db->loadObjectList();

		for($i=0;$i<count($rows);$i++) {
		 $rows[$i]->readmore_link = "";
		 $rows[$i]->readmore_text = "";
		 $rows[$i]->url = ContentHelperRoute::getArticleRoute($rows[$i]->slug, $rows[$i]->catslug, $rows[$i]->sectionid);
		 
		 
		 if($params->get( 'show_intro' )) {   
		 	$rows[$i]->text = $rows[$i]->introtext;		
		  	if($rows[$i]->fulltext) {
		  		$rows[$i]->parameters	= new JParameter( $rows[$i]->attribs );	      		
				$rows[$i]->readmore_text = $rows[$i]->parameters->get('readmore') ? $rows[$i]->parameters->get('readmore') : $params->get( 'readmoretext' );			
				$rows[$i]->readmore_link = $rows[$i]->url;		   
		  	}		  	
		 } else {
		  $rows[$i]->text = $rows[$i]->introtext . $rows[$i]->fulltext;
		 }

		 $plugins	= $params->get('plugins', 0);
		 switch($plugins) {
			 case(1):
			 	$rows[$i]->event = new stdClass();
			 	$row->event->afterDisplayTitle = NULL;
			 	$rows[$i]->event->afterDisplayTitle = NULL;
			 	$rows[$i]->event->beforeDisplayContent = NULL;
				$rows[$i]->event->afterDisplayContent = NULL;
			 break;
			 case(0):
			 default:
		    	$rows[$i]->event = new stdClass();
		    	$results = $dispatcher->trigger('onPrepareContent', array (& $rows[$i], & $params));
		    	$results = $dispatcher->trigger('onAfterDisplayTitle', array ($rows[$i], &$params));
		    	$row->event->afterDisplayTitle = trim(implode("\n", $results));
		    	$results = $dispatcher->trigger('onBeforeDisplayContent', array (& $rows[$i], & $params));
		    	$rows[$i]->event->beforeDisplayContent = trim(implode("\n", $results));
		    	$results = $dispatcher->trigger('onAfterDisplayContent', array (& $rows[$i], & $params));
		    	$rows[$i]->event->afterDisplayContent = trim(implode("\n", $results));
		   break;
		 }	
		 	

		}
	return $rows;
	}
}
