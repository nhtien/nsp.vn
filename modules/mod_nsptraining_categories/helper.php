<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
 
// no direct access
defined('_JEXEC') or die('Restricted access');

class modNspTrainingCatHelper
{
	function getCategories($params, $type)
	{
		$db = &JFactory::getDBO();
		$query = "SELECT * FROM #__nsptraining_categories AS c WHERE published=1 ORDER BY c.ordering ";
		$db->setQuery($query);
		$row = $db->loadObjectList();
		return $row;
	}
}
