<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$param	= $params->get('param1',0);
global $Itemid;
?>
<link href="<?php echo JURI::base()?>modules/mod_nsptraining_categories/tmpl/images/style.css" rel="stylesheet" type="text/css" />
<div class="mod_nsptraining_cat">
<?php
	foreach( $categories as $r){
?>
	<div class="item">
		<a href="index.php?option=com_nsptraining&view=register&catid=<?php echo $r->id?>&Itemid=<?php echo $Itemid?>" title="<?php echo $r->name?>">
			<img src="components/com_nsptraining/images/categories/<?php echo $r->thumbnail;?>" />
		</a>
	</div>
<?php 
}
?>
</div>
