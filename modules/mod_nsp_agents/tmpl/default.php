<?php
/**
* @ Author : Trần Thanh Sang
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$param	= $params->get('param1',0);
$user = &JFactory::getUser();
$pane =& JPane::getInstance('sliders');
?>
<link rel=stylesheet type=text/css href="<?php echo JURI::base()?>modules/mod_nsp_agents/tmpl/images/style.css">
<div class="mod_nsp_agents">
<?php
echo $pane->startPane("agents-panel");
echo $pane->startPanel('Thành phố Hồ Chí Minh', "hcm-tab");
?>
<ol>
	<li>Công ty Cổ Phần TM-DV Phong Vũ</li>
	<li>Công ty TNHH Tin Học Thành Nhân</li>
	<li>công ty TNHH TM&DV Viễn Thông Vân Nguyễn</li>
	<li>công ty TNHH TM DV Tin học Anh Phương</li>
</ol>
<?php 
echo $pane->endPanel();
echo $pane->startPanel('Hà nội', "hahoi-tab");
?>
<ol>
	<li>Công ty TNHH Hệ Thống Tin Học - Viễn Thông NDS</li>
	<li>Công ty TNHH Mạng Kim Ngân</li>
	<li>Công ty TNHH Mạng, Truyền thông và Điều khiển (NCC)</li>
	<li>Công ty TNHH Công Nghệ Mạng - Viễn Thông INET</li>
	<li>Công ty CP Giải Pháp và Truyền Thông</li>
</ol>
<?php
echo $pane->endPanel();
echo $pane->startPanel('Đà Nẵng', "danang-tab");
?>
<ol>
	<li>Công ty Công Nghệ Tin Học Phương Tùng</li>
	<li>Công ty Điện Tử Tin Học Phi Long</li>
</ol>
<?php
echo $pane->endPanel();
echo $pane->startPanel('Cần Thơ', "cantho-tab");
?>
<ol>
	<li>Trung Tâm Điện Tử Tin Học Cần Thơ</li>
	<li>Công ty TNHH Tin Học  Á Châu Cần Thơ</li>
	<li>Trung Tâm Tin Học Viễn THông Cần Thơ-Hậu Giang</li>
</ol>
<?php
echo $pane->endPanel();
echo $pane->startPanel('Đồng Nai', "dongnai-tab");
?>
<ol>
	<li>Công ty TNHH Đại Hùng Anh</li>
	<li>Công ty TNHH Một thành viên Tương Lai Việt</li>
</ol>
<?php
echo $pane->endPanel();
echo $pane->endPane();
?>
	
</div>





