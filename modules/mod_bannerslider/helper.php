<?php
defined('_JEXEC') or die('Restricted access');

class modBannerSliderHelper
{
	// get the parameters
	function getparam($params)
	{
		$var = array();

		$var['moduleclass_sfx'] = $params->get('moduleclass_sfx');

		$var['banners']			= $params->get('banners', '');
		$var['categories']		= $params->get('categories', '');
		$var['clients']			= $params->get('clients', '');
		$var['type'] 			= $params->get('type', 1);

		$var['effect']			= $params->get('effect', 'fade');

		$var['width']			= $params->get('width', 180);
		$var['height']			= $params->get('height', 150);
		$var['delay']			= $params->get('delay', 3000);

		$var['random']			= $params->get('random', 1);
		
		$var['resize']			= $params->get('resize', 0);
		$var['center']			= $params->get('center', 0);
		
		$var['window']			= $params->get('window', 1);

		return $var;
	}
	
	function display($conf)
	{
		$where = array();
		if ($conf['banners'] != '')
		{
			$where[] = 'bid IN (' . modBannerSliderHelper::clean($conf['banners']) . ')';
		}
		
		if ($conf['categories'] != '')
		{
			$where[] = 'catid IN ('.modBannerSliderHelper::clean($conf['categories']) . ')';
		}

		if ($conf['clients'] != '')
		{
			$where[] = 'cid IN (' . modBannerSliderHelper::clean($conf['clients']) . ')';
		}
		
		$where = (count($where) > 0) ? ' AND ('.implode(' OR ', $where).')' : '';

		$query  = "SELECT bid, imageurl, custombannercode, description FROM #__banner WHERE showBanner = '1'".$where." ORDER BY bid";
		
		$db =& JFactory::getDBO();
		$db->setQuery($query);
		
		$data = $db->loadObjectList();
		
		require(JModuleHelper::getLayoutPath('mod_bannerslider'));
	}
	
	function clean($in)
	{
		return preg_replace(
			array('#^[^\d]+|[^\d,]|[^\d]+$#', '#,{2,}#'), 
			array('', ','),
			$in
		);
	}
	
	function htmlsafe($in)
	{
		if (empty($in))
		{
			return '';
		}
		$in = htmlspecialchars($in, ENT_COMPAT, 'UTF-8');
		return preg_replace('#\s+#', ' ', $in);
	}
}
?>
