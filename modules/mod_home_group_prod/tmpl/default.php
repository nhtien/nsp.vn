<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$param	= $params->get('param1',0);
$user = &JFactory::getUser();
$img = JURI::base().'modules/mod_home_group_prod/tmpl/images/';
?>
<script language="javascript">function curvyCorners(a,b){ return;}</script>
<link rel=stylesheet type=text/css href="<?php echo JURI::base()?>modules/mod_home_group_prod/tmpl/images/style.css" />
<script type="text/javascript" src="<?php echo $img?>jquery.min.js"></script>
<script src="<?php echo $img?>jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $img?>jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo $img?>jquery.mousewheel.min.js"></script>
<div class="mod_group_product">

<div id="mcs5_container">
	<div class="customScrollBox">
		<div class="container">
    		<div class="content">
			  <table cellpadding="0" cellspacing="0" border="0" width="auto">
			  <tr>
			  	<td><a href="index.php?option=com_virtuemart&Itemid=19" title="Hệ thống cáp cấu trúc"><img src="<?php echo $img?>cap_cau_truc.png" width="220" height="80" alt="Hệ thống cáp cấu trúc" /></a></td>
			  	<td><a href="index.php?option=com_virtuemart&Itemid=47" title="Thiết bị đo kiểm kết nối cáp"><img src="<?php echo $img?>thiet_bi_do_kiem.png" width="220" height="80" alt="Thiết bị đo kiểm kết nối cáp" /></a></td>
			  	<td><a href="index.php?option=com_virtuemart&Itemid=100" title="Quản lý hạ tầng data center"><img src="<?php echo $img?>datacenter.png" width="220" height="80" alt="Quản lý hạ tầng data center" /></a></td>
				<td><a href="index.php?option=com_virtuemart&Itemid=56" title="Rack thiết bị và phụ kiện"><img src="<?php echo $img?>rack.png" width="220" height="80" alt="Rack thiết bị và phụ kiện" /></a></td>
				
			  	<td><a href="index.php?option=com_virtuemart&Itemid=21" title="Quản trị kết nối vật lý"><img src="<?php echo $img?>quantrivatly.png" width="220" height="80" alt="Quản trị kết nối vật lý" /></a></td>
			  	<td><a href="index.php?option=com_virtuemart&Itemid=74" title="Giám sát và phân tích mạng"><img src="<?php echo $img?>giamsatmang.png" width="220" height="80" alt="Giám sát và phân tích mạng" /></a></td>
				
				</tr>
				</table>
			</div>
		</div>
		<div class="dragger_container">
    		<div class="dragger"></div>
		</div>
	</div>
    <a href="#" class="scrollUpBtn"><img src="<?php echo $img?>left.png" /></a> <a href="#" class="scrollDownBtn"><img src="<?php echo $img?>right.png" /></a>
</div>

</div>

<script>
$(window).load(function() {
	mCustomScrollbars();
});

function mCustomScrollbars(){
	/* 
	malihu custom scrollbar function parameters: 
	1) scroll type (values: "vertical" or "horizontal")
	2) scroll easing amount (0 for no easing) 
	3) scroll easing type 
	4) extra bottom scrolling space for vertical scroll type only (minimum value: 1)
	5) scrollbar height/width adjustment (values: "auto" or "fixed")
	6) mouse-wheel support (values: "yes" or "no")
	7) scrolling via buttons support (values: "yes" or "no")
	8) buttons scrolling speed (values: 1-20, 1 being the slowest)
	*/
	$("#mcs5_container").mCustomScrollbar("horizontal",500,"easeOutCirc",1,"fixed","no","yes",440);
}

/* function to fix the -10000 pixel limit of jquery.animate */
$.fx.prototype.cur = function(){
    if ( this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null) ) {
      return this.elem[ this.prop ];
    }
    var r = parseFloat( jQuery.css( this.elem, this.prop ) );
    return typeof r == 'undefined' ? 0 : r;
}

/* function to load new content dynamically */
function LoadNewContent(id,file){
	$("#"+id+" .customScrollBox .content").load(file,function(){
		mCustomScrollbars();
	});
}
</script>
<script src="<?php echo $img?>jquery.mCustomScrollbar.js"></script>
