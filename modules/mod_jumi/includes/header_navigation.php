<?php
/***************************************************************************
*  @Nhan Sinh Phuc Joomla! Module.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2009 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
global $option; 
if( substr(strtolower(trim(JRequest::getVar('page'))),0,7)=='account' && $option='com_virtuemart') {
	$className2='active';
	$className1 = '';
}else{
	$className1='active';
	$className2 = '';
}

$option = array();
$option[] = JHTML::_('select.option','','All Products');
$list['pro_filter'] = JHTML::_('select.genericList', $option, 'searchscope','class="inputbox"','value','text');


?>
<div class="navigation">
	<div class="top">
		<div class="item <?php echo $className1;?> nv_menu_item"><a href="index.php">NSP</a></div>  
		<div class="item <?php echo $className2;?> nv_menu_item">
			<a href="index.php?page=account.index&option=com_virtuemart&Itemid=71"> <?php echo JText::_('MY_ACCOUNT')?></a>
		</div>
	</div>
	<div class="search navi_search">
<form id="searchform" class="gh-form" onsubmit="if(searchform.searchbox2.value == '<?php echo $search_text?>'){searchform.searchbox2.value=''}" method="get" action="index.php">
<?php 
	$search_text = JText::_('SEARCH_FOR');
?>
	<INPUT onBlur="if(this.value==''){this.value='<?php echo $search_text?>'}" class="inputbox" onfocus="if(this.value=='<?php echo $search_text?>'){this.value=''}" value="<?php echo $search_text?>" 	maxLength="75" name="keyword" size="35"> 
	<?php //echo $list['pro_filter']?>&nbsp;
	<INPUT title="Find it" alt="Find it" src="images/btn-findit.png" type="image" align="absmiddle" name="Find it">
	<span class="browse_cat"><A href="index.php?option=com_nsp&view=browsecategories&Itemid=18">Browse All Categories</A></span>
	<input type="hidden" name="option" value="com_virtuemart" />
	<input type="hidden" name="page" value="shop.browse" />
</form>
	</div>
	
	<div class="main_menu">
		<?php
			$attribs = array();
			echo JModuleHelper::renderModule(JModuleHelper::getModule('superfishmenu','Main menu'), $attribs);
		?>
	</div>
	
</div>