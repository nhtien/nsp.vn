<?php
/***************************************************************************
*  @Nhan Sinh Phuc Joomla! Module.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2009 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
?>
<style type="text/css">
#nsp-right div.nsprdmod, #nsp-right div.nsprdmodagents{
	display:none;
}
</style>