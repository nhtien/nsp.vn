<?php
/***************************************************************************
*  @Nhan Sinh Phuc Joomla! Module.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2009 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

?>
<div class="search">
<?php
$user =&JFactory::getUser();
if( $user->id ){
	printf(JText::_('HINAME'), $user->name) ;
	echo ' &nbsp;|&nbsp; '. '<a href="index.php?option=com_user&task=logout&return='.base64_encode('index.php').'">'.JText::_('Thoát').'</a> &nbsp;';
}
else{
	echo '<a href="index.php?option=com_user&view=login">Đăng nhập</a> &nbsp;';//&nbsp; | &nbsp; <a href="index.php?option=com_user&view=register">Đăng ký</a>';
}
?>
<br />
<form id="searchform" class="gh-form" onsubmit="if(searchform.searchbox2.value == '<?php echo $search_text?>'){searchform.searchbox2.value=''}" method="get" action="index.php">
<?php 
	$search_text = JText::_('SEARCH_FOR');
?>
	<INPUT onBlur="if(this.value==''){this.value='<?php echo $search_text?>'; this.style.fontStyle='italic';}" class="inputbox" onfocus="if(this.value=='<?php echo $search_text?>'){this.value=''; this.style.fontStyle='normal';}" value="<?php echo $search_text?>" 	maxLength="75" name="keyword" size="30">
	<?php //echo $list['pro_filter']?>&nbsp;
	<!--
	<INPUT title="Find it" alt="Find it" src="<?php echo JText::_('IMG_SRC_FINDIT')?>" type="image" align="absmiddle" name="Find it">
	-->
	<input type="hidden" name="option" value="com_virtuemart" />
	<input type="hidden" name="page" value="shop.browse" />
</form>
</div>

