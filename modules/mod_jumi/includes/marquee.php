<?php
/***************************************************************************
*  @Nhan Sinh Phuc Joomla! Module.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2009 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<table class="" cellpadding="0" cellspacing="0" width="100%" bgcolor="#E9E9E9">
<tr>
<td class="date" nowrap="nowrap" width="20%" style="border-right:1px solid #FFFFFF;">
	&nbsp;&nbsp;
	<SCRIPT language="javascript" src="<?php echo JURI::base()?>components/com_nsp/js/daytime.js"></SCRIPT>
	<SCRIPT language="JavaScript" >dates();</SCRIPT>
</td>
<td class="" width="70%">
<?php
	$attribs = array();
	$module = JModuleHelper::getModule('nsp_articlescroller','Article Scroller');
	echo JModuleHelper::renderModule($module, $attribs);
?>
</td>
<td class="last" nowrap="nowrap" width="10%" style="border-left:1px solid #FFFFFF;" align="center">
	<a href="index.php?option=com_content&view=article&id=1&Itemid=87" target="_blank"><strong><?php echo JText::_('Rates');?></strong></a>
</td>
</tr>
</table>
