<?php
/***************************************************************************
*  @Nhan Sinh Phuc Joomla! Module.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2009 Sang Tran Thanh. All rights reserved.
*  @Released under 	Sang Tran Thanh
*  @Email			sangtialia@gmail.com
*  @Date			March 2010
***************************************************************************/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
$user = &JFactory::getUser();

$cart = $_SESSION['cart'];
//var_dump( $cart );exit;
if( count( $cart ) > 0 )
 	$n = count( $cart ) - 1;
else
	$n = 0;
	
	
$segments = $_GET;
$url = JURI::base()."index.php";
if( count( $segments) > 0 ){
	$url .= "?";
}
$i = 1;
foreach( $segments as $key=>$value ){
	$url .= $key."=".$value;
	if( $i< count( $segments ) ){
		$url .= "&";
	}
	$i++;
}

function addLangToLink( $language ){
	$segments = $_GET;
	$url = "index.php";
	if( count( $segments) > 0 ){
		$url .= "?";
	}
	$i = 1;
	foreach( $segments as $key=>$value ){
		if( $key != 'lang')
			$url .= $key."=".$value;
		if( $i< count( $segments ) ){
			$url .= "&";
		}
		$i++;
	}
	$url .= '&lang='.$language;
	$url = JURI::base().$url;
	return $url;
}

$return = base64_encode( $url );
			
/* count visitor */
include (dirname(__FILE__).DS.'counter.php');
/* end count visitor */

?>
<div class="cart">
	<?php if( $user->id ) printf(JText::_("HINAME"),$user->name); ?>
	<a href="index.php?option=com_virtuemart&page=shop.cart&Itemid=" class="shopping png_bg"><?php echo JText::_('Shopping Cart')?></a>  &nbsp; &nbsp; 
	<a href="javascript:void(0);" onclick="document.getElementById('top_shopping_cart').style.display='block';" class="item"><?php echo $n?> <?php echo JText::_('TOP_ITEMS')?></a>  &nbsp; | &nbsp;  <a href="index.php?option=com_content&view=article&id=19&Itemid=8"><?php echo JText::_('Support')?></a>  &nbsp; | &nbsp;  
	
	<?php if( $user->id ){?>
		<a href="index.php?page=account.index&option=com_virtuemart&Itemid=71"> <?php echo JText::_('MY_ACCOUNT')?></a> &nbsp; | &nbsp; 
		<a href="<?php echo JRoute::_("index.php?option=com_user&task=logout&return=$return");?>"><?php echo JText::_('Log out')?></a>
		
	<?php }else{ ?>
		<a href="javascript:void(0);" onclick="document.getElementById('top_login_form').style.display='block';"><?php echo JText::_('Log On')?></a>
	<?php } ?>
<br class="break" />

	<!-- popup cart -->
	<div class="w_popup shopping_cart" id="top_shopping_cart" style="display:none;">
		<div class="title t_r_setting7">
			<span class="t"><?php echo JText::_('Shopping Cart')?></span>
			<span class="close" onclick="document.getElementById('top_shopping_cart').style.display='none';" title="Close window">X</span>
		</div>
		<div class="inner">
		<?php
			$module = JModuleHelper::getModule('virtuemart_cart','shopping cart') ;
			echo(JModuleHelper::renderModule($module, $attribs));

		?>
		</div>
	</div><!--w_popup-->
	
	<!-- logon cart -->
	<div class="w_popup login_form" id="top_login_form" style="display:none;">
		<div class="title t_r_setting7">
			<span class="t"><?php echo JText::_('Log On')?></span>
			<span class="close" onclick="document.getElementById('top_login_form').style.display='none';" title="Close window">X</span>
		</div>
		<div class="inner">
			<form action="index.php" method="post" name="top_login">
			<p>
				<label><?php echo JText::_('Username')?>:</label><br />
				<input type="text" name="username" maxlength="100" size="25" class="inputbox" />
			</p>
			<p>
				<label><?php echo JText::_('Password')?>:</label><br />
				<input type="password" name="passwd" maxlength="100" size="25" class="inputbox" />
			</p>
			<p><input type="submit" value="<?php echo JText::_('Log On')?>" name="submit" /></p>
			<ul>
				<li><A href="<?php echo JRoute::_( 'index.php?option=com_user&view=remind' ); ?>"><?php echo JText::_('Forgot User Name')?>?</A></li>
				<li><A href="<?php echo JRoute::_( 'index.php?option=com_user&view=reset' ); ?>"><?php echo JText::_('Forgot Password?')?></A></li>
				<li><A href="index.php?option=com_virtuemart&page=shop.registration&vmcchk=1<?php //echo JRoute::_( 'index.php?option=com_user&view=register' ); ?>">
				
				<?php echo JText::_('New Register')?></A> </li>
			</ul>
			<input type="hidden" name="option" value="com_user" />
			<input type="hidden" name="task" value="login" />
			<input type="hidden" name="return" value="<?php echo $return; ?>" />
			<?php echo JHTML::_( 'form.token' ); ?>
			<input type="hidden" name="return" value="<?php echo $return;?>" />
		</div><!--- inner -->
		</form>
	</div><!--w_popup-->

</div>
<div class="language" >
	<span class="browse_cat"><A href="index.php?option=com_nsp&view=browsecategories&Itemid=18"><?php echo JText::_('Browse All Categories')?></A></span>
	<a href="<?php echo addLangToLink('vi');?>" title="Ti&#7871;ng vi&#7879;t" style="display:none;" >
		<img src="<?php echo JURI::base();?>images/lang_vn.png" align="middle" class="png_bg" /></a>
	<a href="<?php echo addLangToLink('en');?>" title="English">&nbsp;<img src="<?php echo JURI::base();?>images/lang_en.png" align="middle" class="png_bg" style="display:none" /> </a> 
</div>
	<div class="search">
<form id="searchform" class="gh-form" onsubmit="if(searchform.searchbox2.value == '<?php echo $search_text?>'){searchform.searchbox2.value=''}" method="get" action="index.php">
<?php 
	$search_text = JText::_('SEARCH_FOR');
?>
	<INPUT onBlur="if(this.value==''){this.value='<?php echo $search_text?>'}" class="inputbox" onfocus="if(this.value=='<?php echo $search_text?>'){this.value=''}" value="<?php echo $search_text?>" 	maxLength="75" name="keyword" size="30">
	<?php //echo $list['pro_filter']?>&nbsp;
	<INPUT title="Find it" alt="Find it" src="<?php echo JText::_('IMG_SRC_FINDIT')?>" type="image" align="absmiddle" name="Find it">
	<input type="hidden" name="option" value="com_virtuemart" />
	<input type="hidden" name="page" value="shop.browse" />
</form>
	</div>

