<?php
/**
* @ Author : Sang Tran Thanh
* @ Email  : sangtialia@gmail.com
* @ version $Revision: 1.0 $
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$user = &JFactory::getUser();

	$module = JModuleHelper::getModule('news_show_gk3','Lastest News');

$img_path = JURI::base() .'modules/mod_homehtml/tmpl/images/';

?>
<script language="javascript">
	document.getElementById('nsp-right').style.display = 'none';
	document.getElementById('nsp-content').style.width = '960px';
</script>
<style type="text/css">
</style>
<LINK rel=stylesheet type=text/css href="<?php echo JURI::base()?>modules/mod_homehtml/tmpl/images/style.css">
<div class="homehtml">

<div class="t-content">
	<div class="title"><img src="<?php echo $img_path?>gioithieu.png" class="png_bg" alt="Giới thiệu NSP" /></div>
	<div class="border">
	<div class="t"><a href="/vi/gioi-thieu.html"><img src="modules/mod_homehtml/tmpl/images/about-us.png" alt="Công ty Nhân Sinh Phúc (NSP)" /></a></div>
	<div class="m">
	
	<p align="justify" style="text-align:justify; line-height:1.4">Thành lập từ năm 1999 với vai trò là một trong số ít những công ty tiên phong trong lĩnh vực hạ tầng hệ thống cáp cấu trúc tại Việt Nam. Qua hơn 18 năm phát triển, NSP đã từng bước phát triển và hoàn thiện mình để trở thành một trong những công ty hàng đầu trong lĩnh vực hạ tầng hệ thống cáp cấu trúc nói riêng và thị trường hạ tầng hệ thống CNTT Việt Nam nói chung.</p>
</div>
<div class="b"> <a href="/vi/gioi-thieu.html"> >> </a></div>
</div><!--border-->
</div><!--t-content-->

<!-- dich vu -->
<div class="t-content t-giaiphap">
	<div class="title"><img src="<?php echo $img_path?>giaiphap.png" class="png_bg" alt="Các giải pháp" /></div>
	<div class="border">
	<div class="t"><img src="modules/mod_homehtml/tmpl/images/solutions.png" alt="Các giải pháp công ty NSP" /></div>
	<div class="m giaiphap">
		<p><a href="index.php?option=com_virtuemart&Itemid=144">Quản lý kết nối vật lý QUAREO  &nbsp;  ></a></p>
		<p><a href="index.php?option=com_virtuemart&Itemid=100">Quản lý cơ sở hạ tầng trung tâm dữ liệu &nbsp;  ></a></p>
		<p><a href="index.php?option=com_virtuemart&Itemid=94">Quản lý cơ sở hạ tầng công nghệ thông tin  &nbsp; ></a></p>
		<p><a href="index.php?option=com_virtuemart&Itemid=48">Đo chứng nhận kết nối sợi quang  &nbsp; ></a></p>
</div>

<div class="b"> <a href="index.php?option=com_nsp&view=browsecategories&Itemid=2"> >> </a> </div>
</div><!--border-->
</div><!--t-content-->

<!-- tin tức sự kiện -->
<div class="t-content t-last">
	<div class="title"><img src="<?php echo $img_path?>tinmoinhat.png" class="png_bg" alt="Tin tức - sự kiện NSP" /></div>
	<div class="border">
	<div class="t"><img src="images/lastest-news/<?php echo $params->get('news_filename','lastst-news.jpg')?>" alt="Tin tức - sự kiện, chia sẻ kiến thức công nghệ" width="291" height="160" /></div>
	<div class="m">
		<p><?php echo(JModuleHelper::renderModule($module, $attribs)); ?></p>
	</div>

<div class="b"> <a href="index.php?option=com_content&view=category&layout=blog&id=8&Itemid=60"> >> </a></div>
</div><!--border-->
</div><!--t-content-->

<br class="break" />
</div><!-- homehtml-->


