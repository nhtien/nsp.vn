<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SALES TRAINEE PROGRAM 2018</title>
<meta name="robots" content="index, follow" />
<meta name="keywords" content="tuyển nhân viên kinh doanh; Công ty nhân sinh phúc tuyển dụng" />
<meta name="description" content="Công ty Tin Học Nhân Sinh Phúc cần tuyển dụng các vị trí: Nhân viên kinh doanh. Ứng viên quan tâm nộp hồ sơ..." />
<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600,600i,700&amp;subset=vietnamese" rel="stylesheet">
<link href="css/style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-3.2.1.min.js"></script>
<script src="css/bootstrap/js/bootstrap.min.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-16757079-4', 'auto');
  ga('send', 'pageview');

</script>

</head>
<?php
$page = $_REQUEST['page'];
?>

<body>
<div class="premary">
    <section class="banner">
    	<a href="index.html"><img src="images/banner.jpg" alt="NSP tuyển dụng"></a>
    </section>
    <section class="content">
        <div class="container"></div>
    	<br>
        <?php include("pages/".$page.'.php');?>
    </section>
    
    <footer><p>Bản quyền thuộc về Công ty TNHH TM-DV Tin Học Nhân Sinh Phúc</p></footer>
</div>
</body>
</html>
