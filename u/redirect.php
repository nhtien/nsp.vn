<?php 
require_once('includes/config.php');
require('includes/Short.class.php');

$short = new Short;

	if(isset($_GET["id"])){
		$url=$short->get($_GET['id']);
		if($url){
			$short->update($_GET['id']);
			if($config["frame"]){
				include("template/frame.php");
				exit;
			}else{
				header('Location: '.$url);
				exit;
			}
		}else{
			include("404.php");
			exit;
		}
	}
?>