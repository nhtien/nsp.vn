<?php 
error_reporting(0);
require_once('includes/config.php');
	if($config["maintenance"]) {
		include("template/maintenance.php");
		exit;
	}

require('includes/User.class.php');
require('includes/Short.class.php');
require_once("includes/Recaptcha.php");

$short = new Short;
$user = new User;
$check=$user->check();

	if(isset($_GET["url"])){
		$url=$short->add($_POST);
		if($url["error"]){
			$_SESSION["msg"]="<div class='message error'><span class='close'>x</span>{$url["msg"]}</div>";
		}else{
			$_SESSION["msg"]='<div class="message success"><span class="close">x</span>URL has been successfully shortened.</div>';
		}
	}
	include('template/header.php');
?>
<body>	
<header>
	<div class="info">
		<ul class="menu">
			<?php if($check["status"]): ?>
			<li><a href="<?php echo $config["url"];?>/user/logout">Logout</a></li>		
			<li><a href="<?php echo $config["url"];?>/user/">My Account</a></li>	
			<?php else: ?>	
			<li><a href="<?php echo $config["url"];?>/user/login">Login</a></li>		
			<?php if($config["user"]): ?>		
			<li><a href="<?php echo $config["url"];?>/user/register">Register</a></li>	
			<?php endif;?>						
			<?php endif;?>			
			<li><a href="<?php echo $config["url"];?>">Home</a></li>						
			<?php if($check["status"] && $check["admin"]):?>		
			<li><a href="<?php echo $config["url"];?>/admin">Admin</a></li>	
			<?php endif;?>				
		</ul>		
	</div>
</header>
<?php if($config["ads"]) echo "<div class='ads'>{$config["ad728"]}</div>" ?>
<?php message(); ?>
<div class="ajax"></div>
<section id="main">
	<form action="?url" method="post" class="form" id="shortner">
		<div class="container">
			<input type="text" name="url" id="url" placeholder="Paste Long URL" value="<?php if(isset($url)) echo $url["short"];?>">
			<button type="submit" class="button" id="button">Shorten</button>
			<button type="button" class="button copyurl" id="copyurl">Copy</button>			
		</div>
		<div class="inner">
			<div class="request">
				<?php if($config["captcha"]){
					echo recaptcha_get_html($config["captcha_public"]);
				}?>
			</div>			
			<div class="advanced">
				<h2>Advanced Options</h2>
				<div class="options first">
					<h3>Custom Alias</h3>
					<span>If you need a custom alias, you can enter it below.</span>
					<input type="text" class="subtext" name="custom" placeholder="e.g. thedarkknight">
				</div>
				<?php if($config["geotarget"]):?>
				<div class="options middle">
					<h3>Geotargeting</h3>
					<span>If you have different pages for different countries then it is possible to redirect users to that page using the same URL. Simply choose the <b>country</b> and enter the <b>URL</b>.</span>
					<input type="text" class="subtext country" name="country[]" placeholder="e.g. Canada">
					<small>>></small>
					<input type="text" class="subtext" name="target[]" placeholder="e.g. http://mysite.ca">
					<a href="#" class="more-fields">+</a>			
				</div>	
				<?php endif;?>
				<div class="options last">
					<h3>Description</h3>
					<span>This can be used to identify URLs on your account, which is <a href="<?php echo $config["url"]?>/user/register"><strong>free</strong></a>.</span>
					<textarea class="subtext" name="description" placeholder="e.g. My favorite video"></textarea>
				</div>					
			<br class="clear" />			
			</div>	
		</div>
	<a href="#" class="config">Advanced Options</a>			
	</form>	
		
</section>
<?php include('template/footer.php'); ?>