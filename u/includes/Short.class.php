<?php 
if(!isset($config["included"])) exit();
class Short {

		public function add($array,$ajax=FALSE,$api=FALSE){
			global $db,$config;
			$error="";
			$url=$this->validate($this->clean($array["url"]));
			$domain=$this->domain($url);

			if(empty($array["url"]) || !$url) $error = array('error' => 1, 'msg' => 'Please enter a valid URL.');

			//if($domain==$this->domain($config["url"]) && !$error) $error = array('error' => 1, 'msg' => 'You cannot shorten URLs of this website.');		

			if($this->adult($domain) && !$config["adult"] && !$error) $error = array('error' => 1, 'msg' => 'Adult and warez websites are not allowed.');	

			if(!empty($array["custom"]) && strlen($array["custom"])<3 && !$error) $error = array('error' => 1, 'msg' => 'Custom alias must be at least 3 characters.');	
			
			$userid="0";
			if(isset($_SESSION["login"])){
				$email=base64_decode($_SESSION["login"]["key"]);				
				$query = "SELECT * FROM user WHERE email=:email";
				$result = $db->prepare($query);
				$result->execute(array(":email"=>$email));

				$result->setFetchMode(PDO::FETCH_ASSOC);
				$user = $result->fetch();
				if($user) $userid=$user["id"];
			}
			if($config["captcha"] && !$userid){
				require_once("Recaptcha.php");

				if(empty($array["recaptcha_response_field"]) && !$error) {
					$error = array('error' => 1, 'msg' => 'Please enter the reCAPTCHA','html'=>'captcha');	
				}else{
				  $resp = recaptcha_check_answer ($config["captcha_private"],$_SERVER["REMOTE_ADDR"],$_POST["recaptcha_challenge_field"],$_POST["recaptcha_response_field"]);
				  if (!$resp->is_valid && !$error) {
				    $error = array('error' => 1, 'msg' => "The reCAPTCHA wasn't entered correctly. Please try it again. <script>Recaptcha.reload();</script>",'html'=>'captcha');	
				  }				
				}
			}
			if(!$error){	
				// Validate & Encode Countries
				if(!empty($array['country'][0]) && !empty($array['target'][0])){
					foreach ($array['country'] as $i => $country) {
						$countries[strtolower($this->clean($country))]=$this->clean($array['target'][$i]);
					}
					$countries=json_encode($countries);
				}else{
					$countries='';
				}
				// Validate API
				if($api){
					$result = $db->prepare("SELECT id FROM user WHERE api=:api");
					$result->execute(array(":api"=>$api));
					$user=$result->fetch(PDO::FETCH_ASSOC);
					if($user){
						$userid=$user["id"];
					}
				}
				// Determine if user is registered
				if(!$userid){
					if(!empty($countries)){
						$query="SELECT * FROM url WHERE url='{$url}' AND userid='0' AND location='{$countries}'";	
					}else{
						$query="SELECT * FROM url WHERE url='{$url}' AND userid='0' AND location=''";
					}
				}else{
					if(!empty($countries)){
						$query="SELECT * FROM url WHERE url='{$url}' AND userid='{$userid}' AND location='{$countries}'";	
					}else{					
						$query="SELECT * FROM url WHERE url='{$url}' AND userid={$userid} AND location=''";
					}
				}					
				$check = $db->query($query);
				if($check->rowCount() && empty($array["custom"])){
					$check->setFetchMode(PDO::FETCH_ASSOC);
					$url=$check->fetch();
					return array('error' => 0, 'short'=>"{$config["url"]}/".$url['alias']);
				}else{
					$alias="";
					$custom="";
					if(empty($array["custom"])){
						$unique=FALSE;
						while (!$unique) {
							$alias=$this->alias();
							$check = $db->query("SELECT * FROM url WHERE alias='{$alias}'");	
							if($check->rowCount()==0) $unique=TRUE;
						}							
					}else{
						$custom=$array["custom"];
						$check = $db->query("SELECT * FROM url WHERE custom='{$custom}'");	
						if($check->rowCount()) {
							return array('error' => 1, 'msg' => 'That alias is taken. Please choose another one.');	
						}
					}

					$description = (isset($array["description"])?$array["description"]:'');
					$values=array(
						":alias" => $this->clean($alias),
						":custom" => $this->clean($custom),
						":url" => $url,
						":description" => $this->clean($description),
						":location"	=> $countries,
						":id"	=> $userid
						);				
					$query="INSERT INTO url (alias,custom,url,description,location,date,userid) VALUES (:alias,:custom,:url,:description,:location,NOW(),:id)";					
					$result = $db->prepare($query);					
					if($result->execute($values)) return array('error' => 0, 'short'=>"{$config["url"]}/".$alias.$custom);
				}	
				return FALSE;
			}else{
				return $error;
			}
		}
		public function get($id){
			global $db;
				$country='';
				if($this->country($_SERVER['REMOTE_ADDR'])) $country=$this->country($_SERVER['REMOTE_ADDR']);
				$query = "SELECT * FROM url WHERE alias=:a OR custom=:a LIMIT 1";
				$result = $db->prepare($query);
				$result->execute(array(":a"=>$id));

				$result->setFetchMode(PDO::FETCH_ASSOC);
				$url = $result->fetch();
				$location=json_decode($url["location"],TRUE);

				if(!empty($url["location"]) && isset($location[$country])){
					return $location[$country];
				}else{
					return $url["url"];
				}
		}
		public function update($id){
			global $db;
				$query = "UPDATE url SET click=click+1 WHERE alias=:a OR custom=:a";
				$result = $db->prepare($query);
				$result->execute(array(":a"=>$id));
			return TRUE;
		}

		// Private Functions
		private function alias($l='5'){ 
			$alias="";
			$r= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"; 
				srand((double)microtime()*1000000); 
				for($i=0; $i<$l; $i++) { 
					$alias.= $r[rand()%strlen($r)]; 
				} 
			return $alias; 
		}
		private function clean($text){
			$text=preg_replace('/<script[^>]*>([\s\S]*?)<\/script[^>]*>/i', '', $text);			
			$text=strip_tags($text);
			return trim($text);
		}		
		public function country($ip=NULL,$api=''){
			global $config;
			if(is_null($ip)) $ip=$_SERVER['REMOTE_ADDR'];

			$content=@file_get_contents("http://api.ipinfodb.com/v3/ip-city/?key={$config["apikey"]}&ip=$ip");
			if($content){
				$content=explode(';',$content);
				return strtolower($content["4"]);				
			}
			return FALSE;
		}
		private function validate($url){
				if(empty($url)) return FALSE;
				if(!preg_match('((http://|https://|www.)([\w-\d]+\.)+[\w-\d]+)', $url)) return FALSE;
				if(!filter_var($url, FILTER_VALIDATE_URL)) return FALSE;
				$url=rtrim($url, '/');
			return $url;
		}
		private function domain($url){
			   if(!$this->validate($url)) return FALSE;
				if(preg_match('((http://|https://|www.)([\w-\d]+\.)+[\w-\d]+)', $url, $domain)) {
					return str_replace(".", "", $domain[2]);
				}
		}
		private function adult($url){
			$array = array('porn','adult','sex','porno','redtube','4tube','spankwire','xshare','ziporn','naked','pornstar','tits','pussy','fuck','suck','porntube','warez');
				foreach ($array as $adult) {
				  if (preg_match("~$adult~i", $url)) {
				  	return TRUE;
				  }
				}
			return FALSE;
		}					
	}
?>
