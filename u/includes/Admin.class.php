<?php 
if(!isset($config["included"])) exit();

class Admin{
	public function count($table,$where='',$sum=FALSE){
		global $db;

			if($sum) {
		  	$query = $db->query("SELECT SUM($sum) FROM $table  $where");
		  	$query->setFetchMode(PDO::FETCH_ASSOC);
				while($row = $query->fetch()){
					$count=$row['SUM('.$sum.')'];
					if($count==FALSE) $count="0";
				}
			}else{
				$query=$db->query("SELECT * FROM $table $where");
				$count=$query->rowCount();
				if($count==FALSE) $count="0";
			}
			return $count;
	}

	public function check(){
		global $db;
		if(isset($_SESSION["login"])){
			if($_SESSION["login"]["loggedin"]){
				$query = "SELECT * FROM user WHERE email=:email AND admin='1'";
				$result = $db->prepare($query);
				$result->execute(array(":email"=>base64_decode($_SESSION["login"]["key"])));
				if($result->rowCount()){										
					return TRUE;
				}
			}
		}
		return FALSE;
	}

	public function get($page='1'){
		global $db,$config;
		$content="<h2>URLs</h2>";
		$content.='<ul class="list">';		
			$query="SELECT * FROM url";
			$result = $db->prepare($query);			
			$result->execute();
			$result->setFetchMode(PDO::FETCH_ASSOC);
					while($url = $result->fetch()){
						$content.="<li>";						
							$content.= "<div class='container'> <a href='{$url["url"]}' class='link' target='_blank'>{$url["url"]}</a><a href='{$config["url"]}/admin/url?do=delete&amp;id={$url["id"]}' title='Are you sure you want to delete this?' data-content='This URL will be deleted forever, however you may easily regenerate it.' class='delete'>Delete</a></div>";
							$content.="<span>{$config["url"]}/{$url["alias"]}</span>";							
							$content.= "<span> {$url["click"]} Clicks</span>";						
							$content.= "<span class='type'> ".(($url["userid"])?"Registered User":"Anonymous")." </span>";							
							$content.= "<span class='date'> {$this->timeago($url["date"])} </span>";	
						$content.="</li>";
					}
				$content.="</ul>";
		return $content;
	}
	public function search($array){
		global $db,$config;	
			$q=$array["q"];
			$content='<ul class="stat">
						<li class="form full">
							<form action="'.$config["url"].'/admin/search" method="get">
								<label for="q">Enter a term and Press Enter</label>
								<input type="text" placeholder="Enter a term and Press Enter" id="q" name="q" value="'.$q.'"/>
								'.(isset($array["do"])?'<input type="hidden" name="do" value="'.$array["do"].'"/>':'').'
							</form>
						</li>
					</ul>';
			if(empty($q)){
				$content.="<h2>No query to search to with...</h2>";
				return $content;
			}							
			if(isset($array["do"]) && $array["do"]=="user"){
				$query="SELECT * FROM user WHERE email LIKE \"%$q%\"ORDER BY date DESC";
			}else{
				$query="SELECT * FROM url WHERE url LIKE \"%$q%\" OR alias LIKE \"%$q%\" ORDER BY date DESC";
			}
			$result = $db->prepare($query);			
			$result->execute();
				if(!$result->rowCount()){
					$content.="<h2>No Results found for \"$q\"</h2>";
					return $content;
				}			
			$content.="<h2>Results found for \"$q\"</h2>";
			$result->setFetchMode(PDO::FETCH_ASSOC);								

			if(isset($array["do"]) && $array["do"]=="user"){
					while($user = $result->fetch()){
						$content.='<div class="user-ui">';	
							$content.='<div class="user">';
							$content.= '<span class="count">'.$this->count("url",'WHERE userid='.$user['id'].'').' urls</span><img src="http://1.gravatar.com/avatar/'.md5($user["email"]).'?s=200&r=pg&d=mm" alt="" />';
							$content.= "<span class='email'> {$user["email"]}</span>";					
							$content.= "</div>";
							$content.="<a href='{$config["url"]}/admin/user?delete&amp;id={$user["id"]}' title='Are you sure you want to delete this user?' data-content='This user, along with its bookmarks (not the urls), will be deleted forever.' class='delete'>Delete</a>";
						$content.="</div>";
					}
			}else{
					$content.='<ul class="list">';	
					while($url = $result->fetch()){
						$content.="<li>";						
							$content.= "<div class='container'><a href='{$url["url"]}' class='link' target='_blank'>{$url["url"]}</a><a href='{$config["url"]}/admin/url?do=delete&amp;id={$url["id"]}' title='Are you sure you want to delete this?' data-content='This URL will be deleted forever, however you may easily regenerate it.' class='delete'>Delete</a></div>";
							$content.="<span>{$config["url"]}/{$url["alias"]}</span>";							
							$content.= "<span> {$url["click"]} Clicks</span>";
							$content.= "<span class='type'> ".(($url["userid"])?"Registered User":"Anonymous")." </span>";							
							$content.= "<span class='date'> {$this->timeago($url["date"])} </span>";	
						$content.="</li>";
					}
					$content.="</ul>";
			}			

		return $content;
	}	
	public function user($page='1'){
			global $db,$config;
			$content="<h2>Latest Users (Total ".$this->count("user","WHERE admin='0'")." users)</h2>";
				$query="SELECT * FROM user WHERE admin='0' ORDER by id DESC";
				$result = $db->prepare($query);			
				$result->execute();
				$result->setFetchMode(PDO::FETCH_ASSOC);
						while($user = $result->fetch()){
							$content.='<div class="user-ui">';	
								$content.='<div class="user">';
								$content.= '<span class="count">'.$this->count("url",'WHERE userid='.$user['id'].'').' urls</span><img src="http://1.gravatar.com/avatar/'.md5($user["email"]).'?s=200&r=pg&amp;d=mm" alt="" />';
								$content.= "<span class='email'> {$user["email"]}</span>";					
								$content.= "</div>";
								$content.="<a href='{$config["url"]}/admin/user?delete&amp;id={$user["id"]}' title='Are you sure you want to delete this user?' data-content='This user, along with its bookmarks (not the urls), will be deleted forever.' class='delete'>Delete</a>";
							$content.="</div>";
						}
		return $content;
	}
	public function page($array=FALSE,$update=FALSE,$add=FALSE){
			global $db,$config;
			if($update){
				$query="UPDATE page SET name=:name,seo=:seo,content=:content,menu=:menu WHERE id=:id";					
				$result = $db->prepare($query);			
				$result->execute(array(":name"=>$array["name"],":seo"=>$this->slug($array["seo"]),":content"=>$array["content"],":id"=>$array["id"],":menu"=>$array["menu"]));
				return TRUE;
			}
			if($add){
				$query="INSERT INTO page (name,seo,content,menu) VALUES (:name,:seo,:content,:menu)";			
				$result = $db->prepare($query);			
				$result->execute(array(":name"=>$array["name"],":seo"=>$this->slug($array["seo"]),":content"=>$array["content"],":menu"=>$array["menu"]));
				return TRUE;
			}
			if(!$array){
			$content="<h2>Pages <a href='{$config["url"]}/admin/page?do=add' class='blue right'>Add Page</a></h2>";
				$content.='<ul class="list">';		
				$query="SELECT * FROM page";
				$result = $db->prepare($query);			
				$result->execute();
				$result->setFetchMode(PDO::FETCH_ASSOC);
						while($page = $result->fetch()){
						$content.="<li>";						
							$content.= "<div class='container'> <a href='{$config["url"]}/page/{$page["seo"]}' class='link' target='_blank'>{$page["name"]}</a><a href='{$config["url"]}/admin/page?do=delete&amp;id={$page["id"]}' title='Are you sure you want to delete this?' data-content='This page and its content will be deleted forever.' class='delete'>Delete</a><a href='{$config["url"]}/admin/page?do=edit&amp;id={$page["id"]}' class='green'>Edit</a><a href='{$config["url"]}/page/{$page["seo"]}' class='blue' target='new'>View</a></div>";
							$content.=$this->truncate($this->clean($page["content"]),210);
						$content.="</li>";
						}
				$content.="</ul>";
			}else{
				$query="SELECT * FROM page WHERE id=:id";
				$result = $db->prepare($query);			
				$result->execute(array(":id"=>$array));
				$result->setFetchMode(PDO::FETCH_ASSOC);
				$page = $result->fetch();
				$content="<h2>Edit Page <a href='{$config["url"]}/page/{$page["seo"]}' class='blue right' target='_blank'>View Page</a></h2>";
				$content.='<form action="'.$config["url"].'/admin/page?do=update" method="post" class="form" onsubmit="editor.post()">';			

				$content.='<label for="name">Name/Title</label>';
				$content.='<input type="text" id="name" name="name" value="'.$page["name"].'" class="input" />';			
				$content.='<label for="seo">Alias (used for URL)</label>';
				$content.='<input type="text" id="seo" name="seo" value="'.$page["seo"].'" class="input" />';

				$content.='<label for="content">Content (HTML allowed)</label>';
				$content.='<p class="message info">
					You can use some short-tags to dynamically insert information. For example you automatically insert the url of the site by adding {URL}. Below is a list of tags you can use:<br />
						{URL} - Generates the URL of this site <br />
						{AD728} - Insert the advertisement code 1 of width 728 that you saved in the settings<br />
						{AD468} - Insert the advertisement code 2 of width 468 that you saved in the settings<br />
						{AD300} - Insert the advertisement code 3 of width 300 that you saved in the settings<br />
				</p>';				
				$content.='<textarea name="content" id="content" class="content">'.$page["content"].'</textarea>';
		$content.='<ul class="form_opt" data-id="menu">
					<li class="label">Show in Menu <small>This page will automatically be linked from the footer of all pages.</small></li>
					<li><a href="" class="last'.(!$page["menu"]?' current':'').'" data-value="0">No</a></li>
					<li><a href="" class="first '.($page["menu"]?' current':'').'" data-value="1">Yes</a></li>
				</ul>';
				$content.="<input type='hidden' name='menu' id='menu' value='{$page["menu"]}' />";	
				$content.="<br />";
				$content.="<input type='hidden' name='id' value='{$page["id"]}' />";
				$content.="<button type='submit' class='button-form'>Change</button>";								
				$content.="</form>";					
			}
		return $content;
	}	

	public function delete($type,$id){
		global $db;
		if($type=="url"){
			$query="DELETE FROM url WHERE id=:id";
			$result = $db->prepare($query);			
			$status=$result->execute(array(":id"=>$id));
				if($status){										
					return TRUE;
				}	
		}
		return FALSE;
	}

	public function stats($option=""){
		global $db,$config;
		if($option=="last10"){
		$content="<h2>Latest URLs</h2>";
		$content.='<ul class="list">';		
			$query="SELECT * FROM url ORDER BY id DESC LIMIT 10";
			$result = $db->prepare($query);			
			$result->execute();
			$result->setFetchMode(PDO::FETCH_ASSOC);
					while($url = $result->fetch()){
						$content.="<li>";						
							$content.= "<div class='container'> <a href='{$url["url"]}' class='link' target='_blank'>{$url["url"]}</a><a href='{$config["url"]}/admin/url?do=delete&amp;id={$url["id"]}' title='Are you sure you want to delete this?' data-content='This URL will be deleted forever, however you may easily regenerate it.' class='delete'>Delete</a></div>";
							$content.="<span>{$config["url"]}/{$url["alias"]}</span>";	
							$content.= "<span> {$url["click"]} Clicks</span>";			
							$content.= "<span class='type'> ".(($url["userid"])?"Registered User":"Anonymous")." </span>";												
							$content.= "<span class='date'> {$this->timeago($url["date"])} </span>";	
						$content.="</li>";
					}
				$content.="</ul>";
		return $content;
		}else{
			$var= "";
			$date="";
			$query="SELECT COUNT(DATE(date)) as count,DATE(date) as date FROM url GROUP BY DATE(date) DESC LIMIT 0,7";
			$result = $db->prepare($query);			
			$result->execute();
			$result->setFetchMode(PDO::FETCH_ASSOC);
				$i=0;
				while($data = $result->fetch()){
					$i++;
					$var.="[".(7-$i).",{$data["count"]}],";
					$date.="[".(7-$i).",\"".date("d-M y",strtotime($data["date"]))."\"],";
				}	
			return array($var,$date);
		}
	}	
	public function setting($update=FALSE,$array=""){
		global $db,$config;

		if($update){
			
			$query="UPDATE settings SET var=? WHERE config=?";					
			$result = $db->prepare($query);	
			foreach($array as $var=>$opt) {				
				$result->execute(array($opt,$var));
			}
			return TRUE;

		}else{
			$content='<form action="'.$config["url"].'/admin/setting?do=update" method="post" class="form">';			
			$content.="<h2>Site Information</h2>";

			$content.='<label for="url">Main URL (with http:// and without trailing slash)</label>';
			$content.='<input type="text" id="url" name="url" value="'.$config["url"].'" class="input" />';			
			$content.='<label for="title">Site Name/Title</label>';
			$content.='<input type="text" id="title" name="title" value="'.$config["title"].'" class="input" />';

			$content.='<label for="keywords">Site Keywords (Seperated by a comma)</label>';
			$content.='<input type="text" id="keywords" name="keywords" value="'.$config["keywords"].'" class="input" />';			
			$content.='<label for="description">Description</label>';
			$content.='<textarea name="description" id="description" class="input">'.$config["description"].'</textarea>';
			
			$content.='<label for="email">Admin Email (For Contact)</label>';
			$content.='<input type="text" id="email" name="email" value="'.$config["email"].'" class="input" />';

			$content.='<label for="apikey">Geotargeting API (View Documentation)</label>';
			$content.='<input type="text" id="apikey" name="apikey" value="'.$config["apikey"].'" class="input" />';

			$content.='<label for="facebook">Facebook Page</label>';
			$content.='<input type="text" id="facebook" name="facebook" value="'.$config["facebook"].'" class="input" />';

			$content.='<label for="twitter">Twitter Page</label>';
			$content.='<input type="text" id="twitter" name="twitter" value="'.$config["twitter"].'" class="input" />';			
			$content.="<h2>Advertisement</h2>";
			$content.='<ul class="form_opt" data-id="ads">';
			$content.='<li class="label">Advertisement <small>Enable or disable advertisement throughout the site.</small></li>';
			$content.='<li><a href="" class="last'.((!$config["ads"])?' current':'').'" data-value="0">Disable</a></li>
						<li><a href="" class="first'.(($config["ads"])?' current':'').'" data-value="1">Enable</a></li>
						</ul><input type="hidden" name="ads" id="ads" value="'.$config["ads"].'">';			
			$content.="<br />";	

			$content.='<label>Advertisement Code 1 (728x90 recommended)</label>';
			$content.='<textarea name="ad728" class="input">'.$config["ad728"].'</textarea>';
			$content.='<label>Advertisement Code 2 (468x60 recommended)</label>';
			$content.='<textarea name="ad468" class="input">'.$config["ad468"].'</textarea>';
			$content.='<label>Advertisement Code 3 (300x250 recommended)</label>';
			$content.='<textarea name="ad300" class="input">'.$config["ad300"].'</textarea>';

			$content.="<br /><h2>Site Configuration</h2>";
			$content.='<ul class="form_opt" data-id="theme">';
			$content.='<li class="label">Theme <small>Themes files are located in the static folder. Each theme has its associated css (e.g. orange.css).</small></li>';
			$content.='<li><a href="" class="last'.(($config["theme"]=="custom")?' current':'').'" data-value="custom">Custom</a></li>
						<li><a href="" class="'.(($config["theme"]=="orange")?' current':'').'" data-value="orange">Orange</a></li>
						<li><a href="" class="'.(($config["theme"]=="pink")?' current':'').'" data-value="pink">Pink</a></li>
					   <li><a href="" class="first'.(($config["theme"]=="default")?' current':'').'" data-value="default">Default</a></li>
						</ul><input type="hidden" name="theme" id="theme" value="'.$config["theme"].'">';			


			$content.='<ul class="form_opt" data-id="maintenance">';
			$content.='<li class="label">Site Online/Offline <small>Setting offline will make your website inaccessible.</small></li>';
			$content.='<li><a href="" class="last'.(($config["maintenance"])?' current':'').'" data-value="1">Offline for Maintenance</a></li>
					   <li><a href="" class="first'.((!$config["maintenance"])?' current':'').'" data-value="0">Online</a></li>
						</ul><input type="hidden" name="maintenance" id="maintenance" value="'.$config["maintenance"].'">';			


			$content.='<ul class="form_opt" data-id="frame">';
			$content.='<li class="label">Frame (Redirection)<small>Enable/Disable the frame at upon redirection.</small></li>';
			$content.='<li><a href="" class="last'.((!$config["frame"])?' current':'').'" data-value="0">Disable</a></li>
					   <li><a href="" class="first'.(($config["frame"])?' current':'').'" data-value="1">Enable</a></li>
						</ul><input type="hidden" name="frame" id="frame" value="'.$config["frame"].'">';			

			$content.='<ul class="form_opt" data-id="geotarget">';
			$content.='<li class="label">Geotargeting (API Key Requried)<small>Redirects user according to their country (if set by author).</small></li>';
			$content.='<li><a href="" class="last'.((!$config["geotarget"])?' current':'').'" data-value="0">Disable</a></li>
					   <li><a href="" class="first'.(($config["geotarget"])?' current':'').'" data-value="1">Enable</a></li>
						</ul><input type="hidden" name="geotarget" id="geotarget" value="'.$config["geotarget"].'">';			
			
			$content.='<ul class="form_opt" data-id="api">';
			$content.='<li class="label">Developer API <small>Allow registered users to shorten URLs from their site using an API.</small></li>';
			$content.='<li><a href="" class="last'.((!$config["api"])?' current':'').'" data-value="0">Disable</a></li>
					   <li><a href="" class="first'.(($config["api"])?' current':'').'" data-value="1">Enable</a></li>
						</ul><input type="hidden" name="api" id="api" value="'.$config["api"].'">';			

			$content.='<ul class="form_opt" data-id="user">';
			$content.='<li class="label">User Registration <small>Allow users to register and to bookmark their URLs.</small></li>';
			$content.='<li><a href="" class="last'.((!$config["user"])?' current':'').'" data-value="0">Disable</a></li>
					   <li><a href="" class="first'.(($config["user"])?' current':'').'" data-value="1">Enable</a></li>
						</ul><input type="hidden" name="user" id="user" value="'.$config["user"].'">';			

			$content.='<ul class="form_opt" data-id="adult">';
			$content.='<li class="label">Adult/Warez URLs <small>Once disabled, any url containing adult/warez keywords will not be allowed.</small></li>';
			$content.='<li><a href="" class="last'.((!$config["adult"])?' current':'').'" data-value="0">Disable</a></li>
					   <li><a href="" class="first'.(($config["adult"])?' current':'').'" data-value="1">Enable</a></li>
						</ul><input type="hidden" name="adult" id="adult" value="'.$config["adult"].'">';			

			$content.='<ul class="form_opt" data-id="captcha">';
			$content.='<li class="label">reCaptcha (View Documentation)<small>Users will be prompted to answer a captcha before processing their request.</small></li>';
			$content.='<li><a href="" class="last'.((!$config["captcha"])?' current':'').'" data-value="0">Disable</a></li>
					   <li><a href="" class="first'.(($config["captcha"])?' current':'').'" data-value="1">Enable</a></li>
						</ul><input type="hidden" name="captcha" id="captcha" value="'.$config["captcha"].'">';			

			$content.='<ul class="form_opt" data-id="sharing">';
			$content.='<li class="label">Sharing <small>Allow users to share their shorten URL through social networks such as facebook and twitter.</small></li>';
			$content.='<li><a href="" class="last'.((!$config["sharing"])?' current':'').'" data-value="0">Disable</a></li>
					   <li><a href="" class="first'.(($config["sharing"])?' current':'').'" data-value="1">Enable</a></li>
						</ul><input type="hidden" name="sharing" id="sharing" value="'.$config["sharing"].'">';					

			$content.="<br />";
			$content.="<button type='submit' class='button-form'>Submit</button>";								
			$content.="</form>";	
			return $content;	

		}
		return FALSE;
	}
	private function timeago($time){
	   $time=strtotime($time);
	   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
	   $lengths = array("60","60","24","7","4.35","12","10");
	   $now = time();
		   $difference = $now - $time;
		   $tense= "ago";
		   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			   $difference /= $lengths[$j];
		   }
		   $difference = round($difference);
		   if($difference != 1) {
			   $periods[$j].= "s";
		   }
	   return "$difference $periods[$j] $tense ";
	}
	private function truncate($string, $limit) {
	  $len = strlen($string);
		if ($len > $limit) return substr($string,0,$limit)."...";
		else return $string; 
	}
	private function clean($text){
		$text=preg_replace('/<script[^>]*>([\s\S]*?)<\/script[^>]*>/i', '', $text);			
		$text=strip_tags($text);	
		return $text;
	}			
	private function slug($str){
		$slug=preg_replace('/[^_0-9a-zA-Z ]/', '', strtolower($str));
		$slug=preg_replace('/\s\s+/', ' ', $slug);
		$slug=str_replace(' ','-',$slug);
	return $slug;
	}			
}
?>