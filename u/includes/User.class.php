<?php 
if(!isset($config["included"])) exit();
class User {

	public function login($email,$pass){
		global $db;

		if(!$this->validate($email)) return array("status"=>FALSE, "msg"=>"Please enter a valid email.", "type"=>"error");

			$query = "SELECT * FROM user WHERE email=:email AND password=:pass";
			$result = $db->prepare($query);
			$result->execute(array(":email"=>$email,":pass"=>$this->encode($pass)));

			if($result->rowCount()){
				$_SESSION["login"]=array("loggedin"=>TRUE,"key"=>base64_encode($email));
				return array("status"=>TRUE);

			}else{
				return array("status"=>FALSE, "msg"=>"Wrong email/password combination.", "type"=>"error");
			}	
		return FALSE;	
	}	
	public function register($array){
		global $db;
		$error="";
		if(!$this->validate($array["email"])) $error.="Please enter a valid email.<br />";
		$result = $db->prepare("SELECT * FROM user WHERE email=:email");
		$result->execute(array(":email"=>$array["email"]));
		if($result->rowCount()) $error.="An account is already associated with this email.<br />";		
		if(empty($array["pass"]) OR strlen($array["pass"])<6) $error.="Password has to be at least 6 characters.<br />";
		if($array["pass"]!=$array["cpass"]) $error.="Passwords don't match.";
		if(!isset($array["terms"]) OR $array["terms"]!=1) $error.="You must agree to the terms and conditions.<br />";


		if($error) return array("error"=>1, "msg"=>$error, "type"=>"error");
		$unique=FALSE;
		while (!$unique) {
			$api=$this->api();
			$check = $db->query("SELECT * FROM user WHERE api='{$api}'");	
			if($check->rowCount()==0) $unique=TRUE;
		}			
		$values=array(
			":email" => strip_tags($array["email"]),
			":pass" => $this->encode($array["pass"]),
			":api"	=> $api
			);				
		$query="INSERT INTO user (email,password,date,api) VALUES (:email,:pass,NOW(),:api)";					
		$result = $db->prepare($query);					
		if($result->execute($values)) return array('error' => 0, 'msg'=>"Congratulations! You have been successfully registered. You may now login");		
	}
	public function check(){
		global $db;
		if(isset($_SESSION["login"])){
			if($_SESSION["login"]["loggedin"]){
					$query = "SELECT * FROM user WHERE email=:email AND admin='1'";
					$result = $db->prepare($query);
					$result->execute(array(":email"=>base64_decode($_SESSION["login"]["key"])));
					if($result->rowCount()){										
						return array('status' => TRUE, 'admin'=>TRUE);
					}
				return array('status' => TRUE, 'admin'=>FALSE);
			}
		}
		return FALSE;
	}
	public function logout(){
		unset($_SESSION["login"]);
	}
	public function home(){
		global $db,$config;
			$email=base64_decode($_SESSION["login"]["key"]);
			$content='<ul class="list">';	
			$query = "SELECT * FROM user WHERE email=:email";
				$result = $db->prepare($query);
				$result->execute(array(":email"=>$email));

				$result->setFetchMode(PDO::FETCH_ASSOC);
				$user = $result->fetch();
			$query = "SELECT * FROM url WHERE userid=:id ORDER BY date DESC";
				$result = $db->prepare($query);
				$result->execute(array(":id"=>$user["id"]));

				$result->setFetchMode(PDO::FETCH_ASSOC);
					while($url = $result->fetch()){
						$content.="<li>";						
							$content.= "<div class='container'> <a href='{$url["url"]}' class='link' target='_blank'>{$url["url"]}</a></div>";
							if(!empty($url["description"])) $content.="<div>{$url["description"]}</div>";
							$content.="<span>{$config["url"]}/{$url["alias"]}{$url["custom"]}</span>";
							$content.= "<span> {$url["click"]} Clicks</span>";						
							$content.= "<span class='date'> {$this->timeago($url["date"])} </span>";
							if($config["sharing"]){
							}								
						$content.="</li>";
					}
				$content.="</ul>";
		return $content;
	}
	public function info(){
		global $db,$config;
			$email=base64_decode($_SESSION["login"]["key"]);
			$query = "SELECT * FROM user WHERE email=:email";
				$result = $db->prepare($query);
				$result->execute(array(":email"=>$email));
				$result->setFetchMode(PDO::FETCH_ASSOC);
				$user = $result->fetch();
				$content = '<div class="half blue"><h3>My Profile <a href="" class="edit" title="Edit Account Information" data-content=".updateform" data-inline="true">(edit)</a> </h3>';
				$content.="<strong>Email: {$user["email"]}</strong> <br />";
				$content.="<strong>Registered: {$this->timeago($user["date"])}</strong>";
				$content.="<div class='updateform hide'>";
				$content.="<form action='{$config["url"]}/user/update' method='post' id='account'>";
				$content.="<label>Update Email</label>";
				$content.="<input type='text' value='{$user["email"]}' name='email' class='input' />";	
				$content.="<label>Current Password</label>";
				$content.="<input type='password' value='' name='opass' class='input' />";					
				$content.="<label>Change Password <small>Minimum 6 Characters</small></label>";
				$content.="<input type='password' value='' name='pass' class='input' />";	
				$content.="<label>Confirm Password</label>";
				$content.="<input type='password' value='' name='cpass' class='input' />";		
				$content.="<button type='submit' class='green'>Update</button>";							
				$content.="<button type='button' class='delete close'>Cancel</button>";
				$content.="</form>";	
				$content.="</div>";			
				$content.="</div>";

				$content.= '<div class="half right black"><h3>Developer Key <a href="'.$config["url"].'/user/regenerateapi">(Regenerate)</a> </h3>';
				$content.="<p>If you want use our developer API, you will need to authenticate with this key. <a href='{$config["url"]}/page/developer'>(Learn More)</a> <br />";
				$content.="<strong>API Key: {$user["api"]}</strong>";
				$content.="</div>";	

				$content.='<div class="clear"></div>';
		return $content;
	}
	public function update($action,$array=""){
		global $db;
			$email=base64_decode($_SESSION["login"]["key"]);
			if($action=="api"){		
				$query="UPDATE user SET api=:api WHERE email=:email";
				$unique=FALSE;
				while (!$unique) {
					$api=$this->api();
					$check = $db->query("SELECT * FROM user WHERE api='{$api}'");	
					if($check->rowCount()==0) $unique=TRUE;
				}					
				$values=array(
				":api" => $api,
				":email" => $email
				);						
			}elseif($action=="user"){
				$error="";

				if(!$this->validate($array["email"])) $error.="Please enter a valid email.<br />";			
				if(!empty($array["opass"]) && (empty($array["pass"]) OR strlen($array["cpass"])<6)) $error.="New password has to be at least 6 characters.<br />";
				if(!empty($array["opass"]) && $array["opass"]===$array["pass"]) $error.="You new password has to be different from your current one.<br />";		
				if(!empty($array["opass"]) && $array["pass"]!=$array["cpass"]) $error.="Passwords don't match.";
				if($error) return array("status"=>FALSE, "msg"=>$error, "type"=>"error");

				$query = "SELECT * FROM user WHERE email=:email";
				$result = $db->prepare($query);
				$result->execute(array(":email"=>$email));
				$result->setFetchMode(PDO::FETCH_ASSOC);
				$user = $result->fetch();
				if($array["email"]!=$user["email"]) $email=$array["email"];
				if(!empty($array["opass"]) && $this->encode($array["opass"])!=$user["password"]) return array("status"=>FALSE, "msg"=>"Your current password doesn't the password we have in our database.", "type"=>"error");
				if(empty($array["opass"])) $pass=$user["password"];
				else $pass=$this->encode($array["pass"]);
				$query="UPDATE user SET email=:email,password=:pass WHERE id={$user["id"]}";
				$values=array(
					":email"=>$email,
					":pass"=>$pass);
			}
			$_SESSION["login"]["key"]=base64_encode($email);
			if($query){
				$result = $db->prepare($query);					
				if($result->execute($values)) return array("status"=>TRUE);	
			}
		return FALSE;
	}
	public function forgot($array,$update=FALSE){
		global $db,$config;
		$error="";
			if(!$update){
				if(!$this->validate($array)) return FALSE;
				$check = $db->prepare("SELECT api,email FROM user WHERE email=:email LIMIT 1");	
				$check->execute(array(":email"=>$array));
				$pass=$check->fetch(PDO::FETCH_ASSOC);
				if($pass){
					$forgot_url=$config["url"]."/user/forgot/?token=".$pass["api"].".".md5(strtotime(date('Y-m-d')));  
		     		$var["email"] =$pass["email"];
		        	$var["subject"] = "[{$config["title"]}] Password Reset Instructions";
					$var["message"] = "
		          <p><b>A request to reset your password was made.</b> If you <b>didn't</b> make this request, please ignore and delete this email otherwise click the link below to reset your password.</p>
		          <a href=\"$forgot_url\" class='link'><b>Click here to reset your password.</b></a>
		          <p>If you cannot click on the link above, simply copy &amp; paste the following link into your browser.</p>
		          <a href=\"$forgot_url\" class='link'>$forgot_url</a>
		          <p><b>Note:This link is only valid for one day. If it expires, you can request another one.</b></p>
		          Best regards, <br />
		          <b>{$config["title"]}</b> Team <br />
		          <a href=\"{$config["url"]}\">{$config["url"]}</a>";		
		          if($this->mail($var)) return TRUE;
				}
			}else{
				if((empty($array["pass"]) OR strlen($array["pass"])<6)) $error.="New password has to be at least 6 characters.<br />";		
				if($array["pass"]!=$array["cpass"]) $error.="Passwords don't match.";
				if(!$error){
					$query="UPDATE user SET password=:pass WHERE api=:api";
					$values=array(
					":api"=>$_SESSION["token"],
					":pass"=>$this->encode($array["pass"]));
					$result = $db->prepare($query);					
					if($result->execute($values)) return array("error"=>'0');	
				}else{
					return array("error"=>'1',"msg"=>$error);
				}
			}		
		return FALSE;
	}
	public function contact($array){
		global $config;
		$error="";
		if(empty($array["name"])) $error.="Please enter you name.<br />";
		if(empty($array["email"]) || !$this->validate($array["email"])) $error.="Please enter a valid email.<br />";
		if(empty($array["message"])) $error.="Please enter a brief message so we can contact you back.";
		if(!$error){
     		$var["email"] = $config["email"];
     		$var["from"]="From: ".strip_tags($array["name"])." <".strip_tags($array["email"]).">";
        	$var["subject"] = "[{$config["title"]}] You have been contacted: ".strip_tags($array["email"])."";
			$var["message"] = "You been contacted by ".strip_tags($array["email"])." with the following subject <b>".strip_tags($array["subject"])."</b><br /> <br /> ".strip_tags($array["message"])."";

          if($this->mail($var)) return TRUE;	
		}else{
			return array("error"=>'1',"msg"=>$error);
		}
	}
	//Private Functions - Editing not recommended
	private function encode($password, $type='MD5', $salt=''){
		return hash($type,$password.$salt);
	}		
	private function validate($email){
		if (!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]{2,})+$/i', $email) && strlen($email)<=50){
			return FALSE;
		} 
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			return FALSE;
		}
		return TRUE;
	}
	private function timeago($time){
	   $time=strtotime($time);
	   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
	   $lengths = array("60","60","24","7","4.35","12","10");
	   $now = time();
		   $difference = $now - $time;
		   $tense= "ago";
		   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			   $difference /= $lengths[$j];
		   }
		   $difference = round($difference);
		   if($difference != 1) {
			   $periods[$j].= "s";
		   }
	   return "$difference $periods[$j] $tense ";
	}	
	private function api(){
			$l='12';
			$api="";
			$r= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
				srand((double)microtime()*1000000); 
				for($i=0; $i<$l; $i++) { 
					$api.= $r[rand()%strlen($r)]; 
				} 
			return $api; 		
	}
	private function mail($email){
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		if($email["from"]) $headers .= $email["from"]. "\r\n";
		$content="
		<html>
		<head><title>{$email["subject"]}</title>
		<style>body{padding:5px; margin:5px} a{color: #2185F3;text-decoration:none;}a:hover{color: #000;}.link{display:inline-block; background:#2185F3; color:#fff;padding:5px 10px;font-weight:700;}
		</style>
		</head>
		<body>
		{$email["message"]}
		</body>
		</html>
		";
		if(mail($email["email"], $email["subject"], $content, $headers)) return TRUE;
	return FALSE;
	}
}
?>