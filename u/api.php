<?php
	require_once('includes/config.php');

	if(!$config['api']) exit(json_encode(array("error"=>"1", "msg"=>"Service have been disabled.")));
	if(!isset($_GET['api'])) exit(json_encode(array("error"=>"1", "msg"=>"Invalid API Key.")));
	if(!isset($_GET['url'])) exit(json_encode(array("error"=>"1", "msg"=>"Invalid URL")));

	require_once('includes/Short.class.php');
	$short = new Short;

	$query = "SELECT * FROM user WHERE api=:api";
	$result = $db->prepare($query);
	$result->execute(array(":api"=>$_GET['api']));
	if($result->rowCount()){
		echo json_encode($short->add($_GET,TRUE,$_GET['api']));
	}else{
		echo json_encode(array('error' => "1",'msg'=> 'Your API key cannot be validated.'));
	}
?>