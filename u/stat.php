<?php 
require_once('includes/config.php');
require('includes/User.class.php');
$user= new User;
$check= $user->check();

	$query = "SELECT * FROM url WHERE alias=:id OR custom=:id LIMIT 1";
	$result = $db->prepare($query);
	$result->execute(array(":id"=>$_GET['id']));
	$url=$result->fetchAll(PDO::FETCH_ASSOC);
	$url=$url[0];
	$act="Statistics For {$url["url"]}";
	include('template/header.php');	
?>
<header>
	<div class="info">
		<ul class="menu">
			<?php if($check["status"]): ?>
			<li><a href="<?php echo $config["url"];?>/user/logout">Logout</a></li>		
			<li><a href="<?php echo $config["url"];?>/user/">My Account</a></li>	
			<?php else: ?>	
			<li><a href="<?php echo $config["url"];?>/user/login">Login</a></li>
			<?php if($config["user"]): ?>		
			<li><a href="<?php echo $config["url"];?>/user/register">Register</a></li>	
			<?php endif;?>			
			<?php endif;?>	
			<li><a href="<?php echo $config["url"];?>">Home</a></li>		
			<?php if($check["status"] && $check["admin"]):?>		
			<li><a href="<?php echo $config["url"];?>/admin">Admin</a></li>	
			<?php endif;?>		
			<li class="logo"><?php echo $config["title"]?><span><?php echo $config["description"]?></span></li>								
		</ul>		
	</div>
</header>	
<section>
	<div class="ads"><?php if($config["ads"]) echo $config["ad728"]; ?></div>	
	<div class="thumb">
		<img src="http://pagepeeker.com/thumbs.php?size=x&url=<?php echo $url["url"];?>" alt="">
	</div>
	<div class="stats">
		<div class="social">
			<img src="http://chart.apis.google.com/chart?chs=150x150&chld=L|0&choe=UTF-8&cht=qr&chl=<?php echo urlencode($config["url"])."/".$url["alias"];?>" alt="QR" />
			<?php if($config["sharing"]):?>
			<iframe src="//www.facebook.com/plugins/like.php?href=<?php echo urlencode($config["url"]."/".$url["alias"].$url["custom"]);?>&amp;send=false&amp;layout=button_count&amp;width=160&amp;show_faces=false&amp;font=arial&amp;colorscheme=light&amp;action=like&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:160px; height:21px;" allowTransparency="true"></iframe>
			<br />
			<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $config["url"]."/".$url["alias"].$url["custom"];?>" data-text="Check out this website! <?php echo $config["url"]."/".$url["alias"].$url["custom"];?>">Tweet</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			<!-- Place this tag where you want the +1 button to render. -->
			<br />
			<div class="g-plusone" data-href="<?php echo $config["url"]."/".$url["alias"].$url["custom"];?>"></div>

			<!-- Place this tag after the last +1 button tag. -->
			<script type="text/javascript">
			  (function() {
			    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			    po.src = 'https://apis.google.com/js/plusone.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			  })();
			</script>
			<?php endif;?>
		</div>	
		<div class="info">
			<h3>Target URL</h3>
				<a href="<?php echo $url["url"];?>" target="_blank"><?php echo $url["url"];?></a>
			<h3>Short URL</h3>
				<a href="<?php echo $config["url"]."/".$url["alias"].$url["custom"];?>" target="_blank"><?php echo $config["url"]."/".$url["alias"].$url["custom"];?></a>
			<h3>Total Number of Clicks</h3>
				<?php echo $url["click"];?> Clicks
			<h3>Date of Creation</h3>
				<?php echo date("F d, Y",strtotime($url["date"]));?>
		</div>					
	</div>
</section>
<?php include('template/footer.php');?>