<?php 
include('includes/config.php');
include('includes/User.class.php');

$user = new User();
$check=$user->check();
$action = array('forgot','login','logout','register','update','regenerateapi');
$act="home";

	if(isset($_GET['action']) && in_array($_GET['action'], $action)) $act=$_GET["action"];
//var_dump($act);exit;
	switch ($act) {
		case 'login':
			if(isset($_POST["email"]) && isset($_POST["pass"])){
				$login=$user->login($_POST['email'], $_POST['pass']);
				if(!$login["status"]){
					$_SESSION["msg"]="<div class='message {$login["type"]}'><span class='close'>x</span>{$login["msg"]}</div>";
				}else{
					exit_status("You have been successfully logged in.","success","user");
				}
			}
			break;
		case 'register':
			if(!$config["user"]) exit_status("Registration has been disabled.","notice","");	
			if($_POST){
				$register=$user->register($_POST);
				if(!$register["error"]){
					exit_status("Registration has been successful. You may now login.","success","user/login");	
				}else{
					$_SESSION["msg"]="<div class='message {$register["type"]}'><span class='close'>x</span>{$register["msg"]}</div>";
				}
			}
			break;
		case 'logout':
			if($check["status"]){
				$user->logout();
				exit_status("You have been logged out. See you later!","notice","");	
			}
			break;
		case 'regenerateapi':
			if($check["status"]){
				$user->update("api");
				exit_status("Developer key has been regenerated.","success","user");
			}else{
				exit_status("You need to be logged in to use this feature.","error","user/login");
			}
			break;
		case 'update':
			if($check["status"] && isset($_POST)) {
				$update=$user->update("user",$_POST);
				if(!$update["status"]){
					exit_status($update["msg"],$update["type"],"user");
				}else{
					exit_status("Your account information has been updated.","success","user");
				}
			}
			break;
		case 'forgot':
			if(isset($_POST["email"])){
				if($user->forgot($_POST["email"])) {
					exit_status("Please check your email for furthur instructions.","success","user/forgot");
				}else{
					$_SESSION["msg"]="<div class='message error'><span class='close'>x</span>Please enter a valid email.</div>";
				}		
			}
			if(isset($_GET["token"])){ 
		        $i=explode('.',$_GET['token']);
		        if(md5(strtotime(date('Y-m-d')))==$i[1]) {
		        	$_SESSION["token"]=$i[0];
		        }else{
		         exit_status("Link has expired. You can request a new link below.","error","user/forgot");
		        }
			}
			if(isset($_POST["update"])){
				$update=$user->forgot($_POST,TRUE);
				if(!$update["error"]){
					exit_status("Your password has been successfully changed. You may now login.","success","user/login");
				}else{
					$_SESSION["msg"]="<div class='message error'><span class='close'>x</span>{$update["msg"]}</div>";
				}
			}
			break;			
	}
	include('template/header.php');
?>
<body>	
    <div id="alert"></div>
    <div id="shadow"></div> 	
<header>
	<div class="info">		
		<ul class="menu">
			<?php if($check["status"]): ?>
			<li><a href="<?php echo $config["url"];?>/user/logout">Logout</a></li>		
			<li><a href="<?php echo $config["url"];?>/user/">My Account</a></li>	
			<?php else: ?>	
			<li><a href="<?php echo $config["url"];?>/user/login">Login</a></li>
			<?php if($config["user"]): ?>		
			<li><a href="<?php echo $config["url"];?>/user/register">Register</a></li>	
			<?php endif;?>			
			<?php endif;?>	
			<li><a href="<?php echo $config["url"];?>">Home</a></li>		
			<?php if($check["status"] && $check["admin"]):?>		
			<li><a href="<?php echo $config["url"];?>/admin">Admin</a></li>	
			<?php endif;?>										
			<li class="logo"><?php echo $config["title"]?><span><?php echo $config["description"]?></span></li>		
		</ul>	
	</div>
</header>	
<section>
<?php
	if($user->check()):?>
		<?php message();?>
		<?php echo $user->info();?>	
	<?php if($config["ads"]) echo "<div class='ads'>{$config["ad728"]}</div>" ?>
	<h2>My Last URLs</h2>
          <?php echo $user->home();?>
          
	<?php else: ?>	
		<div class="center">
			<?php message();?> 		
		<?php if($act=="register"):?>
			<h3>Create an account</h3>
			<p class="promotion">
				By becoming a member, you can <b>bookmark</b> your favorite sites and <b>share</b> them with your friends. You can also access our <a href="">API</a> and request our service from <b>your own website.</b>
			</p>
			<form action="<?php echo $config["url"]?>/user/register" method="post" class="form">
				<label>Email <a>Email must be valid</a></label>
				<input type="text" value="" class="input" name="email"/>

				<label>Password  <a>Minimum 6 characters</a> </label>
				<input type="password" value="" class="input" name="pass"/>

				<label>Confirm Password </label>
				<input type="password" value="" class="input" name="cpass"/>
				<p class="box"><input type="checkbox" name="terms" value="1"> <small>I agree to the</small> <a href="<?php echo $config["url"]?>/page/terms" target="_blank"><small>terms and conditions.</small></a></p>
				<button type="submit" class="button-form">Register</button>
			</form>	
		<?php elseif($act=="forgot"):?>
			<?php if(isset($_GET["token"])):?>
				<h3>Reset your Password</h3>
				<p class="promotion">You can now change your password by simply entering a <b>new one below</b>.</p>
				<form action="" method="post" class="form">
					<label>New Password</label>
					<input type="password" value="" class="input" name="pass"/>
					<label>Confirm Password</label>
					<input type="password" value="" class="input" name="cpass"/>
					<input type="hidden" name="update"/>
					<button type="submit" class="button-form">Reset Password</button>
				</form>				
			<?php else:?>
				<h3>Recover your Password</h3>
				<p class="promotion">Forgot your password? Don't worry it happens to all of us. Simply enter the <b>email you used to register here</b> and we will send you another one right way!</p>
				<form action="<?php echo $config["url"]?>/user/forgot" method="post" class="form">
					<label>Email</label>
					<input type="text" value="" class="input" name="email"/>
					<button type="submit" class="button-form">Reset Password</button>
				</form>	
			<?php endif;?>
		<?php else:?>		
			<h3>Login to your account</h3>
			<form action="<?php echo $config["url"]?>/user/login" method="post" class="form">
				<label>Email <?php echo ''.($config["user"]?'<a href="'.$config["url"].'/user/register">Don\'t have an account?</a>':'').''?></label>
				<input type="text" value="" class="input" name="email"/>

				<label>Password  <a href="<?php echo $config["url"]?>/user/forgot">Forgot password?</a> </label>
				<input type="password" value="" class="input" name="pass"/>

				<button type="submit" class="button-form">Login</button>
			</form>	
		<?php endif;?>		
		</div>
	<?php endif;?>
</section>	
<?php include('template/footer.php'); ?>