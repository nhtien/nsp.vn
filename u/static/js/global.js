$(document).ready(function () {

	// Ajax request: URL shortening and error handeling
	$("form#shortner").live('submit',function(e) {
		e.preventDefault();
		var form=$(this);
		var url=form.find("#url");
		if(url.hasClass('error')) url.removeClass('error');
		if(!url.val()){
			$('.ajax').hide().html('<div class="message error"><span class="close">x</span>Please enter a valid URL.</div>').fadeIn('slow');
			$("input#url").addClass('error');

		}else{			
			$.ajax({
				type: "POST",
				url: "ajax.php",
				data: $(this).serialize(),
				dataType:"json",
				beforeSend: function() {
				    $('.ajax').html("<img class='loader' src='static/loader.gif' style='margin:5px 50%;border:0;' />");
				    form.find('#button').text('Processing...');
				},
				complete: function() {
				    form.find('#button').text('Shorten');					
				    $('.ajax').find('.loader').fadeOut("fast");
				},		        
				success: function (html) {												
				    if(html.error){
				    	if(html.html=="captcha"){
				    		$('.request').fadeIn('slow');				    		
				    		$('.inner').slideDown('slow');	
				    	}
				    	$('.ajax').hide().html('<div class="message error"><span class="close">x</span>'+html.msg+'</div>').fadeIn('slow');	
				    	$("input#url").addClass('error');
				    }else{
				    	$('.advanced').fadeOut('slow');
				    	$('.request').fadeOut('slow');				    	
				    	$('.inner').slideUp('slow');
						$("#button").hide();
				    	$("#copyurl").show();				    	
						$('.ajax').hide().html('<div class="message success"><span class="close">x</span>URL has been successfully shortened. Click "Copy" or CRTL+C to Copy it. <br /><br /> You can access the statistic page via this like '+html.short+'/stat </div>').fadeIn('slow');
						$('.subtext').val('');
				    	url.val(html.short);	
				    	url.select();
						$('#copyurl').zclip({
						    path:'static/js/zclip.swf',
						    copy:$('input#url').val(),
						    afterCopy:function(){
						    	$('.ajax').hide().html('<div class="message notice"><span class="close">x</span>Copied to clipboard.</div>').fadeIn('slow');	
						    	$(this).hide();
						    	$("#button").show();
						    	$('#copyurl').zclip('remove');
						    }        
						});				    					    	
				    } 
				    
				}
			},'json');	
		}
	});

	// Hide advanced options: Homepage
	$('.inner').fadeOut();
	$('.advanced').fadeOut();
	$('.request').fadeOut();
	// Show advanced options: Homepage
	$('a.config').click(function(){
		$('.advanced').fadeToggle('slow');		
		$('.inner').slideToggle();
		return false;
	});
	// Close Message by click .close
	$(".message span.close").live('click',function(){ 
		$(this).parent(".message").fadeOut("slow");
	});	
	// User page: Edit info in Modal
    $("a.edit").click(function(){
       	$(this).modal();
        return false;
    });
    // Add more country fields: Home Page	
    $(document).on('click',"a.more-fields",function(){
    	$(this).parent('div').last(".subtext").append('<input type="text" class="subtext country" name="country[]"> >> <input type="text" class="subtext" name="target[]"> <a href="#" class="more-fields">+</a>');
    	return false;
    });
    // Autocomplete list of Countries
	var countries = ["Afghanistan","Aland Islands","Albania","Algeria","American Samoa","Andorra","Angola","Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Asia/Pacific Region","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos (Keeling) Islands","Colombia","Comoros","Congo","Congo  The Democratic Republic of the","Cook Islands","Costa Rica","Cote d'Ivoire","Croatia","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Europe","Falkland Islands (Malvinas)","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia","French Southern Territories","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guernsey","Guinea","Guinea-Bissau","Guyana","Haiti","Heard Island and McDonald Islands","Holy See (Vatican City State)","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran  Islamic Republic of","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Korea  Democratic People's Republic of","Korea  Republic of","Kuwait","Kyrgyzstan","Lao People's Democratic Republic","Latvia","Lebanon","Lesotho","Liberia","Libyan Arab Jamahiriya","Liechtenstein","Lithuania","Luxembourg","Macao","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia  Federated States of","Moldova  Republic of","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauru","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Niue","Norfolk Island","Northern Mariana Islands","Norway","Oman","Pakistan","Palau","Palestinian Territory","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn","Poland","Portugal","Puerto Rico","Qatar","Reserved","Reunion","Romania","Russian Federation","Rwanda","Saint Helena","Saint Kitts and Nevis","Saint Lucia","Saint Pierre and Miquelon","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia and the South Sandwich Islands","Spain","Sri Lanka","Sudan","Suriname","Svalbard and Jan Mayen","Swaziland","Sweden","Switzerland","Syrian Arab Republic","Taiwan","Tajikistan","Tanzania  United Republic of","Thailand","Timor-Leste","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","United States Minor Outlying Islands","Uruguay","Uzbekistan","Vanuatu","Venezuela","Vietnam","Virgin Islands  British","Virgin Islands  U.S.","Wallis and Futuna","Western Sahara","Yemen","Zambia","Zimbabwe"];
	$(document).on("keydown.autocomplete",".country",function(e){
    	$(this).autocomplete({
     	  source : countries
    	});
  	});
});
// Modal Function
(function($){
    $.fn.extend({ 
        modal: function(settings) {
            var defaults = {
            	title:$(this).attr("title"),
                content:$(this).attr("data-content"),
                link:$(this).attr("href")
            };
         var s = $.extend(defaults, settings);
        var hide="";
        if($(this).attr("data-inline")){
        	s.content=$(s.content).html();
        	var hide="hide";
        }
        $("#shadow").css("height", $(document).height()).hide();
        $("#shadow").show();
        $("#alert").slideDown();
        $("#alert").html("<div class='title'>"+s.title+"</div><p>"+ s.content +"</p><div class='action "+hide+"'><a href='" + s.link + "' class='green "+ hide +"'>Proceed</a> <a href='' class='delete close'>Cancel</a></div>");
        $(".close,#shadow").click(function() {
            $("#shadow").fadeOut();
            $("#alert").slideUp();
            return false;
        });
        $(document).keyup(function(e) {
          if (e.keyCode == 27) {       
            $("#shadow").fadeOut();
            $("#alert").slideUp();
            return false;
          }   
        });                 
        }
    });
})(jQuery);