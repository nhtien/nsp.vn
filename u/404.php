<?php
   require_once('includes/config.php');
   include('template/header.php');
?>
<body>
<section id="notfound">
   <h1>Zut! Page Not Found</h1>
   <p>The page you were looking for is not available or has been deleted.</p>
   <a href="<?php echo $config["url"]?>/?ref=404" class="fulllink">Take me back to the Homepage!</a>
</section>
</body>
</html>