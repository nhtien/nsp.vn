<?php 
require_once('includes/config.php');
	if($config["maintenance"]) {
		include("template/maintenance.php");
		exit;
	}
function replace($text){
	global $config;
	$text=str_replace("{URL}",$config["url"],$text);
	if($config["ads"]){
		$text=str_replace("{AD728}",$config["ad728"],$text);
		$text=str_replace("{AD468}",$config["ad468"],$text);
		$text=str_replace("{AD300}",$config["ad300"],$text);
	}else{
		$text=str_replace("{AD728}","",$text);
		$text=str_replace("{AD468}","",$text);
		$text=str_replace("{AD300}","",$text);
	}
	return $text;
}	
require('includes/User.class.php');
$user = new User;
$check=$user->check();

	$query = "SELECT * FROM page WHERE seo=:seo LIMIT 1";
	$result = $db->prepare($query);
	$result->execute(array(":seo"=>$_GET['page']));
	$result->setFetchMode(PDO::FETCH_ASSOC);
	$page = $result->fetch();
	if(!$page) {
		include('404.php');
		exit;
	}	
	$act=$page["name"];
	include('template/header.php');
?>
<body>
<header>
	<div class="info">
		<ul class="menu">
			<?php if($check["status"]): ?>
			<li><a href="<?php echo $config["url"];?>/user/logout">Logout</a></li>		
			<li><a href="<?php echo $config["url"];?>/user/">My Account</a></li>	
			<?php else: ?>	
			<li><a href="<?php echo $config["url"];?>/user/login">Login</a></li>
			<?php if($config["user"]): ?>		
			<li><a href="<?php echo $config["url"];?>/user/register">Register</a></li>	
			<?php endif;?>			
			<?php endif;?>	
			<li><a href="<?php echo $config["url"];?>">Home</a></li>		
			<?php if($check["status"] && $check["admin"]):?>		
			<li><a href="<?php echo $config["url"];?>/admin">Admin</a></li>	
			<?php endif;?>	
			<li class="logo"><?php echo $config["title"]?><span><?php echo $config["description"]?></span></li>								
		</ul>		
	</div>
</header>
<?php if($config["ads"]) echo "<div class='ads'>{$config["ad728"]}</div>" ?>
<section>
	<h1><?php echo $page["name"]?></h1>
	<?php echo replace($page["content"])?>
</section>

<?php include('template/footer.php');?>