<?php 
require_once('../includes/config.php');
require_once('../includes/Admin.class.php');

$admin = new Admin;

if(!$admin->check()){
	exit_status("You are not authorized to access this location.", "error");
}
$action = array('url','user','page','setting','search');
	if(isset($_GET["action"]) && isset($_GET["do"]) && in_array($_GET["action"], $action) && $_GET["do"]=="delete" && isset($_GET["id"]) && is_numeric($_GET["id"])){
		if($admin->delete($_GET["action"],$_GET["id"])) exit_status("URL has been successfully deleted.","success","admin");
	}
	if(isset($_GET["action"]) && $_GET["action"]=="setting" && isset($_GET["do"])){
		if($_GET["do"]=="update"){
			if($admin->setting(TRUE,$_POST)) exit_status("Configuration has been updated.","success","admin/setting");
		}
	}elseif(isset($_GET["action"]) && $_GET["action"]=="page" && isset($_GET["do"])){

		if($_GET["do"]=="update"){
			if(!empty($_POST["name"]) && !empty($_POST["seo"]) && !empty($_POST["content"])){
				if($admin->page($_POST,TRUE)) exit_status("Page has been updated.","success","admin/page?do=edit&id={$_POST["id"]}");
			}else{
				exit_status("All fields must be filled out.", "error","admin/page?do=add");
			}
		}elseif($_GET["do"]=="added"){
			if(!empty($_POST["name"]) && !empty($_POST["seo"]) && !empty($_POST["content"])){
				if($admin->page($_POST,FALSE,TRUE)) exit_status("Page has been updated.","success","admin/page");
			}else{
				exit_status("All fields must be filled out.", "error","admin/page?do=add");
			}
		}
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><?php echo $config["title"]?> Admin Dashboard</title>
    <meta name="description" content="URL Shortner" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0" />
	<link rel="stylesheet" href="<?php echo $config["url"]?>/admin/static/style.css" type="text/css" media="screen" />     
	<link rel="stylesheet" href="<?php echo $config["url"]?>/admin/static/js/tiny.css" type="text/css" media="screen"/>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>	
	<![endif]-->	
</head>
<body>
	<div id="alert"></div>
    <div id="shadow"></div> 
<header>
	<ul class="menu">
		<li><a href="<?php echo $config["url"];?>/user/logout">Logout</a></li>		
		<li><a href="<?php echo $config["url"];?>/user/">My Account</a></li>	
		<li><a href="<?php echo $config["url"];?>">Home</a></li>	
	</ul>	
	<ul class="nav">
		<li><a href="<?php echo $config["url"]?>/admin">Dashboard</a></li>
		<li><a href="<?php echo $config["url"]?>/admin/url">Manage URLs</a></li>
		<li><a href="<?php echo $config["url"]?>/admin/user">Manage Users</a></li>
		<li><a href="<?php echo $config["url"]?>/admin/page">Manage Pages</a></li>
		<li><a href="<?php echo $config["url"]?>/admin/setting">Settings</a></li>
		<li><a href="<?php echo $config["url"]?>" target="_blank" class="right">View Site</a></li>
	</ul>
</header>
<section>
<?php message(); ?>	
<?php if(isset($_GET["action"]) && in_array($_GET["action"], $action)):?>
		<?php if($_GET["action"]=="url"): ?>
		<ul class="stat">
			<li class="form full">
				<form action="<?php echo $config["url"]?>/admin/search" method="get">
					<label for="q">Search Using URL or Alias and Press Enter</label>
					<input type="text" placeholder="Enter the URL or Alias and Press Enter" name="q"  id="q" />
				</form>
			</li>
		</ul>
		<?php echo $admin->get(); ?>
		<?php elseif($_GET["action"]=="user"): 
		?>
		<ul class="stat">
			<li class="form full">
				<form action="<?php echo $config["url"]?>/admin/search" method="get">
					<label for="q">Search Users Using Email and Press Enter</label>
					<input type="text" placeholder="Enter the email of the user and Press Enter" name="q" id="q" />
					<input type="hidden" name="do" value='user'/>
				</form>
			</li>
		</ul>
		<?php echo $admin->user();?>	
		<?php elseif($_GET["action"]=="page"): 
			if(isset($_GET["do"]) && $_GET["do"]=="edit" && is_numeric($_GET["id"])){
				echo $admin->page($_GET["id"]);
			}elseif(isset($_GET["do"]) && $_GET["do"]=="add"){
			?>
			<form action="<?php echo $config["url"]?>/admin/page?do=added" method="post" class="form" onsubmit="editor.post()">
				<label for="name">Name/Title</label>
					<input type="text" id="name" name="name" placeholder="e.g. Terms and Conditions" class="input" />
				<label for="seo">Alias (used for URL)</label>
					<input type="text" id="seo" name="seo" placeholder="e.g. terms-condition" class="input" />
				<label for="content">Content (HTML allowed)</label>
				<p class="message info">
					You can use some short-tags to dynamically insert information. For example you automatically insert the url of the site by adding {URL}. Below is a list of tags you can use:<br />
						{URL} - Generates the URL of this site <br />
						{AD728} - Insert the advertisement code 1 of width 728 that you saved in the settings<br />
						{AD468} - Insert the advertisement code 2 of width 468 that you saved in the settings<br />
						{AD300} - Insert the advertisement code 3 of width 300 that you saved in the settings<br />
				</p>
					<textarea name="content" id="content" class="input content" style="width:98%;height:540px;">
					</textarea>
					<br />
					<ul class="form_opt" data-id="menu">
						<li class="label">Show in Menu <small>This page will automatically be linked from the footer of all pages.</small></li>
						<li><a href="" class="last" data-value="0">No</a></li>
						<li><a href="" class="first current" data-value="1">Yes</a></li>
					</ul>	
					<br />	
				<input type="hidden" id="menu" name="menu" value="1">
				<button type='submit' class='button-form'>Add Page</button>
			</form>			
			<?php
			}else{
				echo $admin->page();
			}
		?>				
		<?php elseif($_GET["action"]=="setting"): 
			echo $admin->setting();
		?>
		<?php elseif($_GET["action"]=="search"): 
			echo $admin->search($_GET);
		?>		
		<?php endif; ?>	
<?php else:?>
	<ul class="stat">
		<li><?php echo $admin->count("url")?> <span>URLs Shortened</span></li>
 		<li><?php echo $admin->count("url",'','click')?> <span>Clicks Generated</span></li>		
		<li><?php echo $admin->count("user",'WHERE admin="0"')?> <span>Users Registered</span></li>
		<li class="form">
			<form action="<?php echo $config["url"]?>/admin/search" method="get">
				<label for="q">Search Using URL and Press Enter</label>
				<input type="text" placeholder="Enter the URL and Press Enter" name="q" id="q" />
			</form>
		</li>
	</ul>
	<br />
	<h2>Short URLs Statistics</h2>
	<div id="chart" style="width:950px;height:300px"></div>
	<?php echo $admin->stats("last10")?>
<?php endif; ?>	
</section>
<footer>
	2012 &copy; KBRmedia - All Rights Reserved.
</footer>
	<script type="text/javascript" src="<?php echo $config["url"];?>/static/js/jquery.min.js"></script>	
	<script type="text/javascript" src="<?php echo $config["url"];?>/admin/static/js/excanvas.js"></script>		
	<script type="text/javascript" src="<?php echo $config["url"];?>/admin/static/js/jquery.flot.js"></script>
	<script type="text/javascript" src="<?php echo $config["url"];?>/admin/static/js/tiny.js"></script>	
	<script type="text/javascript" src="<?php echo $config["url"];?>/admin/static/js/effect.js"></script>
	<?php if(!isset($_GET["action"])): ?>
	<script type="text/javascript">
		<?php $data= $admin->stats();?>
		$(function () {
		    var data = [<?php echo $data[0]?>];
			$.plot($("#chart"),[data], { 
						xaxis: { ticks: [<?php echo $data[1]?>], color:["#999"]},		
						yaxis: { color:["#999"]},
						series: {
						   color:["#0C67CD"],
						   shadowSize: 2,
		                   lines: { show: true, fill:true, color:["#ccc"]},                
		                   points: { show: true},               
		               },
		               grid: { hoverable: true, borderColor: { colors: ["#eee"] },borderWidth:1} });
		});
	</script>	
	<?php endif;?>
</body>
</html>