$(document).ready(function () {
    $(".message span.close").live('click',function(){ 
        $(this).parent(".message").fadeOut("slow");
    }); 
    $("a.delete").click(function(){
        $(this).modal();
        return false;
    }); 
    $(".form_opt li a").click(function() {
        var name = $(this).parent("li").parent("ul").attr("data-id");
        var to = $(this).attr("data-value");
        $("input#" + name).val(to);
        $(this).parent("li").parent("ul").find("a").removeClass("current");
        $(this).addClass("current");
        return false;
    });  
// Editor Start
if($("#content").length>0){
    var editor = new TINY.editor.edit('editor', {
        id: 'content',
        width: 936,
        height: 575,
        cssclass: 'tinyeditor',
        controlclass: 'tinyeditor-control',
        rowclass: 'tinyeditor-header',
        dividerclass: 'tinyeditor-divider',
        controls: ['bold', 'italic', 'underline', 'strikethrough', '|', 'subscript', 'superscript', '|',
            'orderedlist', 'unorderedlist', '|', 'outdent', 'indent', '|', 'leftalign',
            'centeralign', 'rightalign', 'blockjustify', '|', 'unformat', '|', 'undo', 'redo', 'n',
            'font', 'size', 'style', '|', 'image', 'hr', 'link', 'unlink', '|', 'print'],
        footer: true,
        fonts: ['Verdana','Arial','Georgia','Trebuchet MS'],
        xhtml: true,
        bodyid: 'editor',
        footerclass: 'tinyeditor-footer',
        toggle: {text: 'source', activetext: 'wysiwyg', cssclass: 'toggle'},
        resize: {cssclass: 'resize'}
    });
}
// Editor End
// Charts Start
    function showTooltip(x, y, contents) {
        $('<div id="tooltip" class="chart-tip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y - 25,
            left: x + 5,
            color: '#fff',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#chart").bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;
                    $("#tooltip").remove();
                    showTooltip(item.pageX, item.pageY, item.datapoint[1] +" urls");
                }
        }
    });
// Charts End  
});
(function($){
    $.fn.extend({ 
        modal: function(settings) {
            var defaults = {
                title:$(this).attr("title"),
                content:$(this).attr("data-content"),
                link:$(this).attr("href")
            };
            var s = $.extend(defaults, settings);

        $("#shadow").css("height", $(document).height()).hide();
        $("#shadow").show();
        $("#alert").slideDown();
        $("#alert").html("<div class='title'>"+s.title+"</div><p>"+ s.content +"</p><center><a href='" + s.link + "' class='green'>Proceed</a> <a href='' class='delete close'>Cancel</a></center>");
        $(".close,#shadow").click(function() {
            $("#shadow").fadeOut();
            $("#alert").slideUp();
            return false;
        });
        $(document).keyup(function(e) {
          if (e.keyCode == 27) {       
            $("#shadow").fadeOut();
            $("#alert").slideUp();
            return false;
          }   
        });                 
        }
    });
})(jQuery);