<?php if(!$config["included"]) exit(); 
	ob_start('compress');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title><?php echo $config["title"]?> - <?php echo (isset($act))?ucfirst($act):"URL Shortner";?></title>
    <meta name="description" content="<?php echo $config["description"]?>" />
    <meta name="keywords" content="<?php echo $config["keywords"]?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0" />
    <!--Open Graph Tags-->
	<meta property="og:title" content="<?php echo $config["title"]?> - <?php echo (isset($act))?ucfirst($act):"URL Shortner";?>"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="<?php echo $config["url"]?>"/>
	<meta property="og:description" content="<?php echo $config["description"]?>"/>
	<!--Stylesheets-->
	<link rel="stylesheet" href="<?php echo $config["url"];?>/static/style.css" />
	<?php if($config["theme"]!=='default'):?>
	<link rel="stylesheet" href="<?php echo $config["url"];?>/static/<?php echo $config["theme"]?>.css" />
    <link href="<?php echo $config["url"];?>/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<?php endif;?>
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>	
	<![endif]-->	
</head>