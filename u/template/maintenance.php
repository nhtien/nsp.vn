<?php
if (!isset($config["included"])) exit;
?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
		<title><?=$config["title"]?> - We'll be back soon</title>
		<style type="text/css" media="screen">
				body{
				    background: #fff;
				    font-family: Helvetica, "Arial Rounded MT Bold", Arial;
					width: 740px;
				    margin: 150px auto;
				    max-width: 98%;    				
				}
				section {
					width:740px;
				}
				footer{
					width:740px;
					font:bold 0.8em arial;
					float:left;						
				}
				h1{
					font:bold 3.0em arial;
				}
				span,a {
					color:#000;
				}
				::selection {
					background: #2185F3;
					color:#fff;
				 }
				::-moz-selection {
					background: #2185F3;
					color:#fff;	
				}
				::-webkit-scrollbar {
					width: 10px;
					border: none;
				}

				::-webkit-scrollbar-track {
					background: transparent;
					border: none;
				}

				::-webkit-scrollbar-thumb {
					background: #2185F3;
					border: 1px solid #0A55A9;
				}
				a,a:visited{
					color: #2185F3;
					text-decoration:none;
				}
				a:hover{
					color: #000;
				}
				p{
					font:bold 1.5em arial;
				}
		</style>
</head>
<body>
		<section>
			<h1>Site is offline for <span>maintenance</span>.</h1>
			<p>Currently, we are working on some <span>major updates</span>, please check us back another time. Meanwhile, you can check our <a href="<?php echo $config["facebook"]?>">Facebook Page</a> or our <a href="<?php echo $config["twitter"]?>">Twitter Profile</a>. Thanks for your patience.</p>
		</section>
		<footer>
			<?php echo date("Y")?> &copy <?php echo $config["title"]?> - All Rights Reserved.
		</footer>
</body>
</html>