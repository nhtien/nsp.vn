<?php if(!$config["included"]) exit(); 
	ob_start('compress');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><?php echo $config["title"]?></title>
    <meta name="description" content="<?php echo $config["description"]?>" />
    <meta name="keywords" content="<?php echo $config["keywords"]?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0" />
    <!--Open Graph Tags-->
	<meta property="og:title" content="<?php echo $config["title"]?> - <?php echo (isset($act))?ucfirst($act):"URL Shortner";?>"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="<?php echo $config["url"]?>"/>
	<meta property="og:description" content="<?php echo $config["description"]?>"/>
	<!--Stylesheets-->
	<link rel="stylesheet" href="<?php echo $config["url"];?>/static/style.css" />
	<style type="text/css">
		body{margin:0; padding:0; width: 100%;max-width: 100%;overflow:hidden}									
	</style>	
	<?php if($config["theme"]!=='default'):?>
	<link rel="stylesheet" href="<?php echo $config["url"];?>/static/<?php echo $config["theme"]?>.css" />
	<?php endif;?>	
</head>
<body>
<div class="frame">
	<div class="logo">
		<h3><?php echo $config["title"];?></h3>
		<span><?php echo $config["description"];?></span>
	</div>
	<div class="ad">
		<?php if($config["ads"]) echo $config["ad468"];?>
	</div>
	<div class="close">
		<a href="<?php echo $url;?>">Blank Screen?</a>
	</div>	
</div>
<iframe id="site" src="<?php echo $url;?>" frameborder="0" style="width:100%;height:100%;" scrolling="yes"></iframe>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>	
	<script type="text/javascript" src="<?php echo $config["url"];?>/static/js/global.js"></script>
	<script type="text/javascript">
     $("iframe").height($(document).height());
	</script>
</body>
</html>