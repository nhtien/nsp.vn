<?php 
require_once('includes/config.php');
require('includes/User.class.php');
$user = new User;
$check=$user->check();

if(isset($_POST["contact"])){
	$contact=$user->contact($_POST);
	if($contact["error"]){
		$_SESSION["msg"]="<div class='message error'><span class='close'>x</span>{$contact["msg"]}</div>";
	}else{
		exit_status("You message has been sent. We will reply as soon as possible.","success","contact");
	}
}
	$act="Contact Us";
	include('template/header.php');
?>
<body>
<header>
	<div class="info">
		<ul class="menu">
			<?php if($check["status"]): ?>
			<li><a href="<?php echo $config["url"];?>/user/logout">Logout</a></li>		
			<li><a href="<?php echo $config["url"];?>/user/">My Account</a></li>	
			<?php else: ?>	
			<li><a href="<?php echo $config["url"];?>/user/login">Login</a></li>
			<?php if($config["user"]): ?>		
			<li><a href="<?php echo $config["url"];?>/user/register">Register</a></li>	
			<?php endif;?>			
			<?php endif;?>	
			<li><a href="<?php echo $config["url"];?>">Home</a></li>		
			<?php if($check["status"] && $check["admin"]):?>		
			<li><a href="<?php echo $config["url"];?>/admin">Admin</a></li>	
			<?php endif;?>	
			<li class="logo"><?php echo $config["title"]?><span><?php echo $config["description"]?></span></li>								
		</ul>		
	</div>
</header>
<section>	
	<h1>Contact Us</h1>
<?php message();?>	
	<form action="" class="form" method="post">
		<label for="name">Ful Name</label>
		<input type="text" value="" class="input" name="name" id="name" />
		<label for="email">Email</label>
		<input type="text" value="" class="input" name="email" id="email" />
		<label for="subject">Subject</label>
		<input type="text" value="" class="input" name="subject" id="subject" />
		<label for="message">Message</label>
		<textarea name="message" id="message" cols="30" rows="25" class="input"></textarea>
		<button type="submit" class="button-form" name="contact">Send Message</button>
	</form>
</section>
<?php include('template/footer.php');?>