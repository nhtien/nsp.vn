<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );
$i=1;
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list">
  <tr class="title">
  	<td>STT</td>
    <td>Họ tên</td>
    <td>Công ty</td>
    <td>Điện thoại </td>
    <td>Email</td>
    <td>Thực hiện</td>
  </tr>
 <?php foreach($results as $r){?>
  <tr>
  	<td align="center"><?php echo $i++?></td>
    <td><?php echo $r->name?></td>
    <td><?php echo $r->company?></td>
    <td><?php echo $r->phone?></td>
    <td><?php echo $r->email?></td>
    <td><?php echo $r->sale?></td>
  </tr>
  <?php } ?>
</table>
