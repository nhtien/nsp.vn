<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chương trình: Tạo nhãn in cho Bản tin Tầm Nhìn Mạng</title>
<link href="css/style.css" type="text/css" rel="stylesheet" />
<script language="javascript" src="js/ajax.js"></script>
<script language="javascript" src="js/jquery.js"></script>
</head>
<body>
<h2>Chương trình: Tạo nhãn in cho Bản tin Tầm Nhìn Mạng</h2>
<fieldset>
<legend><h3 class="red">Chọn tập tin danh sách khách hàng (.xlsx)</h3></legend>
<form action="index.php" enctype="multipart/form-data" method="post">
	<br />
    <h4>Tuỳ chọn các trường</h4>
<p>
	<input type="checkbox" name="phone" value="1" checked="checked" /> Điện thoại<br /> 
    <input type="checkbox" name="company" value="1" checked="checked" /> Tên công ty<br /> 
    <input type="checkbox" name="title" value="1" checked="checked" /> Chức vụ<br /> 
    <input type="checkbox" name="address" value="1" checked="checked" /> Địa chỉ<br />
</p>
	<input type="file" name="file" /> <input type="submit" value="Bắt đầu" />
    <input type="hidden" name="task" value="result" />
   </form>
</fieldset>

</body>
</html>
