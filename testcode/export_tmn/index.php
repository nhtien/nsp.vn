<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/
define( '_EXEC', 1 );

define( 'DS', DIRECTORY_SEPARATOR );

define('LIVE_SITE','');
define('BASE_URL','');

require_once("config.php");
include('lib'.DS.'object.php');
include('lib'.DS.'phpxls.php');
include('lib'.DS.'request.php');
include('lib'.DS.'string.php');
include('lib'.DS.'html.php');
include('lib'.DS.'table.class.php');
global $config, $db;
$config = new Config();

//$db 	= new Database();

$task = Request::getVar('task');
switch($task){
	case 'result':
			$file 		= $_FILES['file']; 
			$addphone 	= @$_REQUEST['phone'];
			$addcom 	= @$_REQUEST['company'];
			$addtitle 	= @$_REQUEST['title'];
			$addresss 	= @$_REQUEST['address'];
			
			$srcfile = dirname(__FILE__).DS.'data'.DS.$file['name'];
			move_uploaded_file($file['tmp_name'], $srcfile);
			$filename = $file['name']; //var_dump($filename);exit;
			//$srcfile = dirname(__FILE__).DS.'data'.DS.'demo.xlsx';
			
			$objxls = PHPExcel_IOFactory::load($srcfile); 
			$index = 1;
			$anpha = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q');
			foreach($anpha as $a){
				$title = $objxls->getActiveSheet()->getCell($a.$index)->getValue();
				if($title){
					$title = str_replace(" ","",trim(strtolower($title)));
					$arrTitle[$title] = $a;
				}
			} 
			//var_dump($arrTitle);exit;
			//(!array_key_exists(
			$index++;
			$row = $objxls->getActiveSheet()->getCell('A'.$index)->getValue(); //var_dump($row);exit;
			$rows = array();
			while($row){ 
				$o = new StdClass();
				$o->lastName = '';
				$o->firstName = '';
				$o->company = '';
				$o->title = '';
				$o->address = '';
				$o->phone = '';
				
				if(isset($arrTitle['lastname']))
					$o->lastName 	= $objxls->getActiveSheet()->getCell($arrTitle['lastname'].$index)->getValue();
					
				if(isset($arrTitle['firstname']))
					$o->firstName 	= $objxls->getActiveSheet()->getCell($arrTitle['firstname'].$index)->getValue();
				
				if(isset($arrTitle['company']))	
					$o->company 	= $objxls->getActiveSheet()->getCell($arrTitle['company'].$index)->getValue();
					
				if(isset($arrTitle['title']))	
					$o->title 		= $objxls->getActiveSheet()->getCell($arrTitle['title'].$index)->getValue();
					
				if(isset($arrTitle['mailingstreet']))
					$o->address		= $objxls->getActiveSheet()->getCell($arrTitle['mailingstreet'].$index)->getValue();
				
				if(isset($arrTitle['mobile']))
					$o->phone		= $objxls->getActiveSheet()->getCell($arrTitle['mobile'].$index)->getValue();
				$rows[] = $o;
				$index++;
				$row = $objxls->getActiveSheet()->getCell('A'.$index)->getValue(); 
			}
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getProperties()->setCreator("Sang Tran")
										 ->setLastModifiedBy("Sang Tran")
										 ->setTitle("Office 2007 XLSX Test Document")
										 ->setSubject("Office 2007 XLSX Test Document")
										 ->setDescription("Tao nhan in cho Ban tin Tam Nhin Mang")
										 ->setKeywords("office 2007 openxml php")
										 ->setCategory("Tam Nhin Mang");	
			$objPHPExcel->setActiveSheetIndex(0);
			$index = 1;
			$col = 3; $i = 1; $j = 0;
			foreach($rows as $r){
				$objRichText = new PHPExcel_RichText();
				
				$objPayable = $objRichText->createTextRun("Kính gửi: Ông/Bà $r->lastName $r->firstName");
				$objPayable->getFont()->setBold(true);
				
				if($addphone && $r->phone)
					$objRichText->createText(" ($r->phone)");
					
				if($r->title && $addtitle)
					$objRichText->createText("\n$r->title");
				if($addcom){
					$objPayable = $objRichText->createTextRun("\n$r->company");
					$objPayable->getFont()->setBold(true);
				}
				if($addresss){
					$objRichText->createText("\n$r->address");
				}
				
				$objPHPExcel->getActiveSheet()->setCellValue( $anpha[$j].$i , $objRichText );
				$j++;
				if($index % $col == 0){$i++; $j = 0;}
				$index++;
			}
			
			$styleThinBlackBorderOutline = array(
				'borders' => array(
					'outline' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('argb' => 'FF000000'),
					),
					'inside' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN,
						'color' => array('argb' => 'FF000000'),
					),

				),
				'alignment' => array(
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
				),
			);
			$styleTitle = array(
					'fill'=>array(
						'type'=>PHPExcel_Style_Fill::FILL_SOLID,
						'startcolor'=>array('argb'=>'FFd1f6f6')
					),
					'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER),
					'font'    => array(	'bold'    => true)
			);
			$range = 'A1:C'.$i;
			$objPHPExcel->getActiveSheet()->getStyle($range)->applyFromArray($styleThinBlackBorderOutline);
			$objPHPExcel->getActiveSheet()->getStyle($range)->getAlignment()->setWrapText(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(55);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(55);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(55);
			$objPHPExcel->getActiveSheet()->getStyle($range)->getFont()->setName('Arial');
			
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$resultfile = dirname(__FILE__).DS.'data'.DS.$filename;
			$objWriter->save($resultfile); //exit;
			header("Cache-Control: public, must-revalidate");
			header('Cache-Control: pre-check=0, post-check=0, max-age=0');
			header("Pragma: no-cache");
			header("Expires: 0"); 
			header("Content-Description: File Transfer");
			header("Expires: Sat, 30 Dec 1990 07:07:07 GMT");
			header("Content-Type: application/excel");
			header("Content-Length: ".filesize($resultfile));
			header('Content-Disposition: attachment; filename="'.$filename.'"');
			header("Content-Transfer-Encoding: binary\n");
			
			@readfile($resultfile);
			
		
		break;
	default:
		include('html'.DS.'index.php');
		break;
}

?>