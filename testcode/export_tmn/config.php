<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class Config
{
	var $db_host 		= 'localhost';
	
	var $db_user		= 'root';
	
	var $db_name		= 'naivebyes';
	
	var $db_prefix		= '';
	
	var $db_pw	 		= '';
	
}

?>