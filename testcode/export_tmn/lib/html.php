<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Trần Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class HTML
{

	function select($option , $name, $atribute = 'class="inputbox"', $id = null ,$current_value = null){
		$html = '<select name="'.$name.'" id = "'.$id.'" '.$atribute.' >';
		for($i=0 ; $i < count($option) ; $i++)
		{
			$value = $option[$i]->value;
			if( strtolower($value) == '<optgroup>' )
			{
				$html .= '<optgroup label="'.$option[$i]->text.'">';
			}
			elseif( strtolower($value) == '</optgroup>' )
			{
				$html .= '</optgroup>';
			}
			else
			{
				$html .=HTML::makeHtmlOption( $option[$i]->value , $option[$i]->text , $current_value, $option[$i]->attribs );
			}
		}
		$html .="</select>";
		return $html;
	}
	
	function makeHtmlOption($value, $name , $current_value = null, $atribute = null)
	{ 
		if(is_array($current_value) )
		{
			$select = in_array($value, $current_value) ? 'selected' : '';
		}
		else
		{
			$select = (($value == $current_value) ? 'selected' : '');
		}
		if($atribute) $atribute = ' '.$atribute;
		$str = '<option value="'.$value.'" '.$select.$atribute.'>'.$name.'</option>';
		return $str;
	}
	
	function option($value, $text = '', $attribs = null)
	{
		if( !$text ) $text = $value;
		$obj = new stdClass;
		$obj->value		= trim($value);
		$obj->text		= trim($text);
		$obj->attribs 	= $attribs;
		return $obj;
	}
	
}

?>