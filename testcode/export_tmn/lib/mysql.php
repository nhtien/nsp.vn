<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class Database
{
	var $db_host          = "";
	
	var $db_name          = "";
	
	var $db_prefix		  = "";
	
	var $db_username      = "";
	
	var $db_password      = "";
	
	var $serverconn		  = 0;
	
	var $db_conn		  = 0;
	
	var $sql			  ='';
	
	
	function __construct( $options = null )
	{
		if($options)
			$this->db_mysql_option($options);
		else
			$this->db_mysql();

		$this->db_connect();
		$this->db_select();
	}
	
	function db_mysql()
	{
		$config = new Config(); 
		$this->db_host		= $config->db_host;
		$this->db_name    	= $config->db_name;
		$this->db_username 	= $config->db_user;
		$this->db_password 	= $config->db_pw;
		$this->db_prefix	= $config->db_prefix;
	}
	
	function db_mysql_option($options)
	{
		$this->db_host     	= $options['db_host'];
		$this->db_name     	= $options['db_name'];
		$this->db_username 	= $options['db_user'];
		$this->db_password 	= $options['db_pw'];
		$this->db_prefix	= $options['db_prefix'];
	}
	
	function _set_db_prefix( $sql = '' )
	{
		if(!$sql) $sql = $this->sql;
		$sql = str_replace('#__', $this->db_prefix, $sql);
		return $sql;
	}
	
	function db_connect()
	{
		$this->serverconn = @mysql_connect($this->db_host,$this->db_username,$this->db_password, true);
		if(!$this->serverconn) die ("Could not connect to MySQL ".mysql_error());
		mysql_query("SET NAMES 'utf8'");
	}
	
	function db_select()
	{
		$this->db_conn = mysql_select_db($this->db_name,$this->serverconn) or die("Could not connect to database ".mysql_error());
	}
	
	function setQuery($sql)
	{
		global $config, $debug;
		$this->sql = $this->_set_db_prefix($sql);
		if( $config->debug)
			$debug->_( $this->getQuery() );
	}
	
	function getQuery(){
		return $this->sql;
	}
	
	function loadObject($sql = null)
	{
		if(!$sql) 
			$sql = $this->sql;
		else {
			$sql = $this->_set_db_prefix($sql);
			$this->sql = $sql;
		}
		$result=mysql_query($sql,$this->serverconn );
		if(!$result) die ("Could not execute SQL: ".$this->_showError( $sql) );
		$row = mysql_fetch_object( $result );
		mysql_free_result($result);
		return $row;
	}
	
	function loadObjectList($sql = '')
	{
		if(!$sql) 
			$sql = $this->sql;
		else{
			$sql = $this->_set_db_prefix($sql);
			$this->sql = $sql;
		}
		if($sql)
		{
			$result = mysql_query($sql,$this->serverconn );
			if(!$result) die ("Could not execute SQL: ".$this->_showError( $sql) );
			$rows = array();
			while( $row = mysql_fetch_object($result) )
			{
				 $rows[] = $row;
			}
			mysql_free_result($result);
			return $rows;
		}
	}
	
	function loadResult($sql = '')
	{
		if(!$sql) 
			$sql = $this->sql;
		else{
			$sql = $this->_set_db_prefix($sql);
			$this->sql = $sql;
		}
		$result=mysql_query($sql,$this->serverconn ); 
		if(!$result) die ("Could not execute SQL: ".$this->_showError( $sql) );
		if ($row = mysql_fetch_row( $result )) {
			$r = $row[0];
		}
		mysql_free_result($result);
		return $r;
	}
	
	function query($sql = '')
	{
		if( !$sql ) 
			$sql = $this->sql;
		else{
			$sql = $this->_set_db_prefix($sql);
			$this->sql = $sql;
		}
		$result = mysql_query($sql,$this->serverconn); 
		if ( !$result )
			die("Could not execute SQL: ". $this->_showError( $sql) );
		return true;

	}
	
	function quote( $text, $escaped = true )
	{
		return '\''.($escaped ? $this->escapeString( $text ) : $text).'\'';
	}
	
	function escapeString( $text )
	{
		return mysql_real_escape_string( $text );
	}
	
	
}
	

?>
