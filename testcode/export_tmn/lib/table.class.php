<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class Table extends Object 
{
	
	var $_tbl		= null;
	var $_tbl_key	= null;

	function __construct($table, $key)
	{
		$this->_tbl = $table;
		$this->_tbl_key = $key; 
	}
	
	function _($type, $prefix = 'Table')
	{
		require_once('tables'.DS.strtolower($type).'.'.'tbl');
		$className = $prefix.$type;
		$class = new $className;
		return $class;
	}
	
	function update( $key_value = 0 )
	{
		if( !$key_value)
		{
			$key_n = $this->_tbl_key;
			$key_value = $this->$key_n;
		}

		$db = new Database;
		$var = get_object_vars($this);
		$ignore = array("_tbl","_tbl_key");
		$fieldName 	= array();
		$value		= array();
		while(list($key,$val) = each($var))
		{
			if( !in_array( $key, $ignore ) && isset($val) )
			{
				$value[]	 = '`'.$key.'` = '.$db->quote($val);
			}
		}
		$value	   = implode(",",$value); 
		$sql = "UPDATE ".$this->_tbl."  SET $value WHERE ".$this->_tbl_key." = ".$key_value;
		$db->query($sql); //echo $sql;exit;
		return true;
	}
	// update record
	function delete( $key_value = 0 )
	{
		if( !$key_value)
		{
			$key_n = $this->_tbl_key;
			$key_value = $this->$key_n;
		}
		
		if( !$key_value )
		{
			return false;
		}

		$db = new Database;
		$sql = "DELETE FROM ". $this->_tbl."  WHERE ".$this->_tbl_key." = ".$key_value;
		$db->query($sql);
		return true;
	}
	//save new record
	function save()
	{
		$db = new Database;
		$ignore = array("_tbl","_tbl_key");
		$var = get_object_vars($this);
		$fieldName 	= array();
		$value		= array();
		while(list($key,$val) = each($var))
		{
			if( !in_array( $key, $ignore ) )
			{
				$fieldName[] = $key;
				$value[]	 = $db->quote($val);
			}
		}
		$fieldName = '`'.implode('` , `',$fieldName).'`';
		$value	   = implode(",",$value);
		$sql = "INSERT INTO ".$this->_tbl."  ($fieldName) VALUES ($value)";
		$db->query($sql);
		
		$key = $this->_tbl_key ;
		if(!$this->$key)
		{
			$this->$key = mysql_insert_id(); // lay ID vua duoc insert vao DB
		}
		
		return true;
	}
	
	// save a new record or update a record
	function store()
	{
		if($this->isNew()) {
			if($this->save()) return true;
		}
		else if($this->update()) return true;
		return false;
	}
	
	
	function load( $id )
	{
		$db = new Database;
		$sql ="SELECT * FROM ".$this->_tbl."  WHERE ".$this->_tbl_key." = ".(int)$id; 
		$row = $db->loadObject($sql);
		$tbl_key = $this->_tbl_key; 
		if ( !$row->$tbl_key )
		{
			 return;
		}
		$ignore = array("_tbl","_tbl_key");
		
		$var = get_object_vars( $this );
		while(list($key,$value) = each($var))
		{
			if( !in_array( $key, $ignore ) )
				$this->$key = $row->$key;
		}
		return true;
	}
	
	function saveorder($ordering, $key_value = null)
	{
		global $database;
		$key = $this->_tbl_key ;
		$key_value  = (int) ($key_value ? $key_value : $this->$key);
		if( !$key_value ) return;
		$sql = "UPDATE ".$this->_tbl." SET ordering = ".(int)$ordering." WHERE ".$this->_tbl_key."=". (int)$key_value; 
		if( $database->query( $sql ) ) return true;
		
		return false;
	}
	
	function isNew()
	{
		$key = $this->_tbl_key;
		if($this->$key)
		{
			$db = new Database();
			$sql ="SELECT * FROM ".$this->_tbl."  WHERE ".$this->_tbl_key." = ".$db->quote($this->$key); 
			$db->setQuery($sql);
			if($db->loadObject()) return false;
		}
		return true;
	}
}

?>