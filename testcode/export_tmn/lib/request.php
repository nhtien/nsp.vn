<?php
/***************************************************************************
*  @PHP Web Application.
*  @version			1.0.0 
*  @copyright		Copyright (C) 2011 Tran Thanh Sang. All rights reserved.
*  @Released under 	Tran Thanh Sang
*  @Email			sangtialia@gmail.com
*  @Date			Jan 2011
***************************************************************************/

defined( '_EXEC' ) or die( 'Quyen truy cap nay khong cho phep.' );

class Request
{
	function getVar($name, $val = null, $hash = '' )
	{
		$hash = strtoupper( $hash );
		switch ($hash)
		{
			case 'GET' :
				$input = &$_GET;
				break;
			case 'POST' :
				$input = &$_POST;
				break;
			case 'FILES' :
				$input = &$_FILES;
				break;
			case 'COOKIE' :
				$input = &$_COOKIE;
				break;
			case 'ENV'    :
				$input = &$_ENV;
				break;
			case 'SERVER'    :
				$input = &$_SERVER;
				break;
			default:
				$input = &$_REQUEST;
				break;
		}
	
		if(isset($input[$name])) $value = $input[$name];
		if ( !isset($value) ) $value = $val;
		return $value;
	}
	function getInt($name, $val = null, $hash = '')
	{
		$value = Request::getVar($name, $val = null, $hash = '');
		$value = (int) $value;
		return $value;
	}
	
	function getString($name, $val = null, $hash = '')
	{
		$value = trim( Request::getVar($name, $val = null, $hash = '') );
		$value = str_replace("\\\"","\"", $value); // don't remove
		return $value;
	}
	
	function getTextarea($name, $val = null, $hash = 'post', $allowHTMLCode = true, $breakLine = true)
	{
		$string = Request::getVar( $name, $val,$hash);
		if($breakLine) str_replace("\n",'<br />',$string);
		if(!$allowHTMLCode) $string = htmlspecialchars($string);
		
		return $string;
		
			
	}
	
	function setVar($name, $value = null, $hash = 'method', $overwrite = true)
	{
		if(!$overwrite && array_key_exists($name, $_REQUEST)) {
			return $_REQUEST[$name];
		}
		$hash = strtoupper($hash);
		if ($hash === 'METHOD') {
			$hash = strtoupper($_SERVER['REQUEST_METHOD']);
		}

		$previous	= array_key_exists($name, $_REQUEST) ? $_REQUEST[$name] : null;

		switch ($hash)
		{
			case 'GET':
				$_GET[$name] = $value;
				$_REQUEST[$name] = $value;
				break;
			case 'POST':
				$_POST[$name] = $value;
				$_REQUEST[$name] = $value;
				break;
			case 'COOKIE':
				$_COOKIE[$name] = $value;
				$_REQUEST[$name] = $value;
				break;
			case 'FILES':
				$_FILES[$name] = $value;
				break;
			case 'ENV':
				$_ENV['name'] = $value;
				break;
			case 'SERVER':
				$_SERVER['name'] = $value;
				break;
		}

		return $previous;
	}
	
	function get($hash = 'default')
	{
		$hash = strtoupper($hash);

		if ($hash === 'METHOD')
		{
			$hash = strtoupper( $_SERVER['REQUEST_METHOD'] );
		}

		switch ($hash)
		{
			case 'GET' :
				$input = $_GET;
				break;

			case 'POST' :
				$input = $_POST;
				break;

			case 'FILES' :
				$input = $_FILES;
				break;

			case 'COOKIE' :
				$input = $_COOKIE;
				break;

			case 'ENV'    :
				$input = &$_ENV;
				break;

			case 'SERVER'    :
				$input = &$_SERVER;
				break;

			default:
				$input = $_REQUEST;
				break;
			}

		return $input;
	}
	
}
?>